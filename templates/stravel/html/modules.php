<?php

defined('_JEXEC') or die;
/*
 * Module chrome for rendering the module in a submenu
 */
function modChrome_no($module, &$params, &$attribs)
{
	if ($module->content)
	{
		echo $module->content;
	}
}

function modChrome_well($module, &$params, &$attribs)
{
	if ($module->content)
	{
		echo "<div class=\"well " . htmlspecialchars($params->get('moduleclass_sfx')) . "\">";
		if ($module->showtitle)
		{
			echo "<h3 class=\"page-header\">" . $module->title . "</h3>";
		}
		echo $module->content;
		echo "</div>";
	}
}
function modChrome_slide($module, &$params, &$attribs) 
{
    if ($module->content)
	{
		echo "<div class=\"container-fluid\">";
        echo '<div class="row">';
		if ($module->showtitle)
		{
			echo "<h3 class=\"page-header\">" . $module->title . "</h3>";
		}
		echo '<div class="module-content slide-section">'.$module->content.'</div>';
		echo "</div></div>";
	}        
}
?>
