<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Note. It is important to remove spaces between elements.
$class = $item->anchor_css ? 'class="' . $item->anchor_css . '" ' : '';
$title = $item->anchor_title ? 'title="' . $item->anchor_title . '" ' : '';
$icon = json_decode(htmlspecialchars_decode($item->title));
$attribs = '';
if(!empty($icon->onclick)) {
    $attribs .= ' onclick="'.$icon->onclick.'" ';
}
if(!empty($icon->datatoggle)) {
    $attribs .= ' data-toggle="'.$icon->datatoggle.'" ';
}
if(!empty($icon->datatarget)) {
    $attribs .= ' data-target="'.$icon->datatarget.'" ';
}
if ($item->menu_image)
	{
		$item->params->get('menu_text', 1) ?
		$linktype = '<img src="' . $item->menu_image . '" alt="' . $item->title . '" /><span class="image-title">' . $item->title . '</span> ' :
		$linktype = '<img src="' . $item->menu_image . '" alt="' . $item->title . '" />';
}
else
{
    if(!empty($icon->icon)) {
        $linktype = '<i class="fa '. $icon->icon .'"></i>';
        if(!empty($icon->title)) {
            $linktype .= '<span class="icon-title-tpl">'.$icon->title.'</span>';
        }   
    }else{
        $linktype = $item->title;    
    }
	
}

$flink = $item->flink;
$flink = JFilterOutput::ampReplace(htmlspecialchars($flink));

switch ($item->browserNav) :
	default:
	case 0:
?><a <?php echo $attribs. ' ' .$class; ?> href="<?php echo $flink; ?>" <?php echo $title; ?>><?php echo $linktype; ?></a><?php
		break;
	case 1:
		// _blank
?><a <?php echo $attribs. ' ' .$class; ?> href="<?php echo $flink; ?>" target="_blank" <?php echo $title; ?>><?php echo $linktype; ?></a><?php
		break;
	case 2:
		// window.open
		$options = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,'.$params->get('window_open');
			?><a <?php echo $attribs. ' ' .$class; ?> href="<?php echo $flink; ?>" onclick="window.open(this.href,'targetWindow','<?php echo $options;?>');return false;" <?php echo $title; ?>><?php echo $linktype; ?></a><?php
		break;
endswitch;
