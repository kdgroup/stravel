<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
$lang = JFactory::getLanguage()->getTag();
$pagename = JRequest::getWord('generalpage','aboutus');
?>
<div id="generalpage-<?php echo $pagename; ?>">
    <div class="container">
        <div class="row <?php echo $lang; ?>">
            <div class="col-md-12">
                <ul class="cbreadcrumb">
                    <li><a href="<?php echo JUri::root(); ?>"><i class="fa fa-home"></i><?php echo JText::_('COM_STRAVEL_HOME_TEXT_BREADCRUMBS') ?></a></li>
                    <li><a href="#" onclick="return false;"><?php echo $this->navigation->Content ?></a></li>
                </ul>
            </div>
        </div>
        <div class="row <?php echo $lang; ?>">
            <div class="col-md-12">
                <div class="detailcontent">
                <?php echo $this->content->Content; ?>
                </div>
            </div>    
        </div>
    </div>
</div>
<?php if(!empty($this->branches)) : 
    foreach($this->branches as $branch) :
?>
    <div class="modal fade" id="contactPopup<?php echo $branch->Id; ?>" tabindex="-1" role="dialog" aria-labelledby="contactPopup" aria-hidden="true">
      <div class="modal-dialog modal-contactus-size">
        <div class="modal-content">
          <div class="modal-body">
            <div data-dismiss="modal" class="closebutton2">x</div>
            <?php echo $branch->Content; ?>
          </div>
        </div>
      </div>
    </div>    
<?php endforeach; endif; ?>
<?php if($pagename=='contactus') : ?>
<!-- Google Code for Google 10-20 Conversion Page -->
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 965813877;
            var google_conversion_language = "en";
            var google_conversion_format = "2";
            var google_conversion_color = "ffffff";
            var google_conversion_label = "Ckb4CLOC-1YQ9czEzAM";
            var google_remarketing_only = false;
            /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
            <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/965813877/?label=Ckb4CLOC-1YQ9czEzAM&amp;guid=ON&amp;script=0"/>
            </div>
            </noscript>
        </div>
    </div>
</div>
<?php endif; ?>