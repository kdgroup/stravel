<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
$lang = str_replace('-','_',strtolower(JFactory::getLanguage()->getTag()));
?>
<div id="fb-root"></div> 
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/<?php echo str_replace('-','_',JFactory::getLanguage()->getTag()); ?>/sdk.js#xfbml=1&appId=194378270738290&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<section itemscope itemtype="http://schema.org/Product" id="cruiseflyer" class="<?php echo $lang; ?> productpage">
    <!-- Structure data -->
    <span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" class="hide">
        <span itemprop="ratingValue"><?php echo round((rand(45,50)/10),1) ?></span>
        <span itemprop="reviewCount"><?php echo rand(333,888); ?></span>
    </span>
    <span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="hide">
        <span itemprop="priceCurrency"><?php echo empty($this->cruise->Currency)?'HKD':$this->cruise->Currency ?></span>
        <span itemprop="price"><?php echo empty($this->cruise->Price)?'300':(int)$this->cruise->Price ?></span>
    </span>
    <!-- Structure data end -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="cbreadcrumb">
                    <li><a href="<?php echo JURI::root(); ?>"><i class="fa fa-home"></i><?php echo JText::_('COM_STRAVEL_HOME_TEXT_BREADCRUMBS') ?></a></li>
                    <li><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=cruisehome'); ?>"><?php echo JText::_('COM_STRAVEL_SEARCH_CRUISE') ?></a></li>
                    <?php if(!empty($this->cruise->CruiseLineName)) : ?>
                        <li><a href="<?php $urlname = apiHelper::convertUrl($this->cruise->CruiseLineName); echo JRoute::_('index.php?option=com_stravel&view=cruisehome&cruisecpn='.$this->cruise->CruiseLineId.'&cruisecpnname='.$urlname); ?>"><?php echo $this->cruise->CruiseLineName; ?></a></li>
                    <?php endif; ?>
                    <?php if(!empty($this->cruise->VesselName)) : ?>
                        <li><a href="<?php $urlname = apiHelper::convertUrl($this->cruise->VesselName); echo JRoute::_('index.php?option=com_stravel&view=cruisehome&cruisevessel='.$this->cruise->VesselId.'&cruisevesselname='.$urlname); ?>"><?php echo $this->cruise->VesselName; ?></a></li>
                    <?php endif; ?>
                    <?php if(!empty($this->cruise->Title)) : ?>
                        <li><a href="#" onclick="return false;"><?php echo $this->cruise->Title; ?></a></li>
                    <?php endif; ?>
                </ul>
            </div>        
        </div>
        <div class="row">
            <!-- Start left pane -->
            <div class="col-md-3">
                <div class="left-pane">
                    <div class="panetitle">
                        <h3><?php echo JText::_('COM_STRAVEL_CRUISEHOME') ?></h3>
                    </div>
                    
                    <?php
                        $presetHelper = new presetHelper(); 
                        echo $presetHelper->getSearchPanel(4);
                    ?>
                    
                    <!-- Filter on left -->
                    <div class="filterleft hide">
                        <div class="selectroute">
                            <h4><?php echo JText::_('COM_STRAVEL_SELECT_ROUTE') ?></h4>
                            <div class="dropdown">
                                <span data-toggle="dropdown" data-target="#" class="gradientgray"><span class="fakeselect form-control"><?php echo JText::_('COM_STRAVEL_SELECT_ROUTE') ?></span></span>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                    <?php foreach($this->destination as $item) : ?>
                                        <li><a href="<?php $urlname = apiHelper::convertUrl($item->DstName); echo JRoute::_('index.php?option=com_stravel&view=cruisehome&cruisedst='.$item->Id.'&cruisedstname='.$urlname);  ?>"><?php echo $item->DstName ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                        <div class="selectroute">
                            <div class="companyselect">
                                <h4><?php echo JText::_('COM_STRAVEL_SELECT_YEAR') ?></h4>
                                <div class="dropdown">
                                    <span data-toggle="dropdown" data-target="#" class="gradientgray"><span class="fakeselect form-control">2015</span></span>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                        <?php foreach(array('2014','2015') as $year) : ?>
                                            <li><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=cruisehome&year='.$year); ?>"><?php echo $year ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="companyselect hide">
                                <h4><?php echo JText::_('COM_STRAVEL_SELECT_MONTH') ?></h4>
                                <div class="dropdown">
                                    <span data-toggle="dropdown" data-target="#" class="gradientgray"><span class="fakeselect form-control">3</span></span>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                        <?php 
                                            for($i=1;$i<=12;$i++) {
                                                echo '<li><a href="'.JRoute::_('index.php?option=com_stravel&view=cruisehome&month='.$i).'">'.$i.'</a></li>';
                                            }
                                        ?>
                                    </ul>
                                </div>                                                                
                            </div>
                            <div class="companyselect hide">
                                <h4><?php echo JText::_('COM_STRAVEL_NIGHTS') ?></h4>
                                <div class="dropdown">
                                    <span data-toggle="dropdown" data-target="#" class="gradientgray"><span class="fakeselect form-control">&lt;6</span></span>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                        <li><a href="#" data-id="1" onclick="return false;">&lt;6</a></li>
                                        <li><a href="#" data-id="2" onclick="return false;">7-9</a></li>
                                        <li><a href="#" data-id="3" onclick="return false;">10-14</a></li>
                                        <li><a href="#" data-id="4" onclick="return false;">15-25</a></li>
                                        <li><a href="#" data-id="5" onclick="return false;">&gt;26</a></li>
                                    </ul>
                                </div>                                
                            </div>
                        </div>
                        <div class="selectroute">
                            <div class="companyselect">
                                <h4><?php echo JText::_('COM_STRAVEL_SELECT_CRUISE') ?></h4>
                                <div class="dropdown cruiselineselect">
                                    <span data-toggle="dropdown" data-target="#" class="gradientgray"><span class="fakeselect form-control"><?php echo $this->cruisecpn->CruiseLineName ?></span></span>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                        <?php foreach($this->cruisecpn as $item) : ?>
                                        <li><a href="<?php $urlname = apiHelper::convertUrl($item->CruiseLineName);echo JRoute::_('index.php?option=com_stravel&view=cruisehome&cruisecpn='.$item->Id.'&cruisecpnname='.$urlname); ?>"><?php echo $item->CruiseLineName; ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="companyselect">
                                <h4><?php echo JText::_('COM_STRAVEL_SELECT_CRUISE2') ?></h4>
                                <div class="dropdown">
                                    <span data-toggle="dropdown" data-target="#" class="gradientgray"><span class="fakeselect form-control"><?php echo $this->cruisevessel->VesselName ?></span></span>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                        <?php foreach($this->cruisevessel as $item) : ?>
                                        <li><a href="<?php $urlname = apiHelper::convertUrl($item->VesselName); echo JRoute::_('index.php?option=com_stravel&view=cruisehome&cruisevessel='.$item->Id.'&cruisevesselname='.$urlname); ?>"><?php echo $item->VesselName; ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="selectroute hide">
                            <div class="dragprice companyselect">
                                <h4><?php echo JText::_('COM_STRAVEL_PRICE') ?></h4>
                                <p class="text-center">
                                    <?php echo JText::_('COM_STRAVEL_HK') ?>
                                    <span id="pricefrom">$500</span>
                                    <?php echo JText::_('COM_STRAVEL_TO') ?>
                                    <span id="priceto">$19999</span>
                                </p>
                                <input id="greendragprice" type="slider" name="price" value="500;19999" />
                            </div>
                        </div>
                    </div>
                    <!-- End filter --> 
                </div>                
            </div>
            <!-- End left pane -->
            
            <!-- Start right pane -->
            <div class="col-md-9">
                <div class="right-pane">
                    <!-- Basic sort -->
                    <div class="basicsort">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="row">
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_SORTBY') ?></p></div>
                                            <div class="col-md-3">
                                                <div class="dropdown">
                                                    <span data-direction="1" data-toggle="dropdown" data-target="#" class="fakesortvalue fakeselectgray form-control"><?php echo JText::_('COM_STRAVEL_PRICE_LOW_TO_HIGH') ?></span>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                                        <li><a data-direction="1" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_PRICE_LOW_TO_HIGH') ?></a></li>
                                                        <li><a data-direction="2" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_PRICE_HIGH_TO_LOW') ?></a></li>
                                                        <li><a data-direction="3" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_LASTEST_OFFERS') ?></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                    <!-- end basic sort -->
                    <!-- Filter -->
                    <div class="filter">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="row">
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_HOT_CRUISE_COMPANY') ?></p></div>
                                            <div class="col-md-10">
                                                <div class="linklist">
                                                <?php 
                                                    foreach($this->hotcruisecpn as $cruise) {
                                                        $urlname = apiHelper::convertUrl($cruise->Content);
                                                        echo '<a href="'.JRoute::_('index.php?option=com_stravel&view=cruisehome&cruisecpn='.$cruise->Id.'&cruisecpnname='.$urlname).'">'.$cruise->Content.'</a>';
                                                    }
                                                ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_HOT_ROUTE') ?></p></div>
                                            <div class="col-md-10">
                                                <div class="linklist">
                                                <?php 
                                                    foreach($this->hotcruisedst as $cruise) {
                                                        $urlname = apiHelper::convertUrl($cruise->Content);
                                                        echo '<a href="'.JRoute::_('index.php?option=com_stravel&view=cruisehome&cruisedst='.$cruise->Id.'&cruisedstname='.$urlname).'">'.$cruise->Content.'</a>';
                                                    }
                                                ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End fitler -->
                    
                    <!-- Item List -->
                    <div class="itemlist">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row">
                                    <!-- Product content -->
                                    <div class="packageFlyerInfo">
                                        <div class="col-md-5">
                                            <div class="packageInfoLeft">
                                                <p><span class="infoLabel"><?php echo JText::_('COM_STRAVEL_CRUISE_PACKAGE_NAME') ?></span><span class="sep">:</span><span itemprop="name" class="infoValue"><?php if(!empty($this->cruise->Title)) echo $this->cruise->Title; ?></span></p>
                                                <p><span class="infoLabel"><?php echo JText::_('COM_STRAVEL_CRUISE_COMPANY') ?></span><span class="sep">:</span><span class="infoValue"><?php if(!empty($this->cruise->CruiseLineName)) { echo $this->cruise->CruiseLineName;} ?></span></p>
                                                <p><span class="infoLabel"><?php echo JText::_('COM_STRAVEL_SEARCH_CRUISE') ?></span><span class="sep">:</span><span class="infoValue"><?php if(!empty($this->cruise->VesselName)) {echo $this->cruise->VesselName;} ?></span></p>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="packageInfoRight">
                                                <p><span class="infoLabel"><?php echo JText::_('COM_STRAVEL_RELATED_PACKAGE_NIGHT') ?></span><span class="sep">:</span><span class="infoValue"><?php echo sprintf(JText::_('COM_STRAVEL_PACKAGE_DAYNIGHT_VARRIBLE'),$this->cruise->Days,$this->cruise->Nights); ?></span></p>
                                                <p><span class="infoLabel"><?php echo JText::_('COM_STRAVEL_DEPARTURE_DATE') ?></span><span class="sep">:</span><span class="infoValue"><?php if(!empty($this->cruise->TravelPeriodFr)) echo array_shift(explode('T',$this->cruise->TravelPeriodFr)); if(!empty($this->cruise->TravelPeriodTo)) echo JText::_('COM_STRAVEL_TO'). array_shift(explode('T',$this->cruise->TravelPeriodTo)); ?></span></p>
                                                <p><span class="infoLabel"><?php echo JText::_('COM_STRAVEL_PRICE') ?></span><span class="sep">:</span><span class="infoValue"><?php if(!empty($this->cruise->Price)) {echo '$'.(int)$this->cruise->Price;if($this->cruise->PriceInd=='1') echo '+'; } ?></span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="packageFlyerImage">
                                        <div class="graysep">&nbsp;</div>
                                        <div class="col-md-8">
                                            <p class="greenclr"><?php echo JText::_('COM_STRAVEL_PACKAGE_NAME2') ?></p>
                                            <p><?php if(!empty($this->cruise->Title)) echo $this->cruise->Title; ?></p>
                                        </div>
                                        <div class="col-md-4 text-right">
                                            <div class="shareProduct">
                                                <div class="fb-like" data-href="<?php echo JFactory::getURI()->toString(); ?>" data-width="20" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div>
                                                <a href="<?php if(!empty($this->cruise->RefCodePdfUrl)) { echo preg_replace('/(zh_tw|zh_cn)/','en_us',$this->cruise->RefCodePdfUrl); } ?>" target="_blank" title="Download this Package Flyer"><img src="images/PDF.png" width="19" height="19" alt="Download this Package" /></a>
                                                <a href="#"
                                                    data-frompage="cruise" 
                                                    data-productname="<?php echo $this->cruise->Title; ?>" 
                                                    data-productprice="$<?php echo (int)$this->cruise->Price; if($this->cruise->PriceInd==1) echo '+'; ?>" 
                                                    data-productid="<?php echo $this->cruise->Id ?>"
                                                    data-productship="<?php echo $this->cruise->VesselName; ?>"
                                                    data-productdate="<?php if(!empty($this->cruise->TravelPeriodFr)) echo array_shift(explode('T',$this->cruise->TravelPeriodFr)); if(!empty($this->cruise->TravelPeriodTo)) echo JText::_('COM_STRAVEL_TO'). array_shift(explode('T',$this->cruise->TravelPeriodTo)); ?>"
                                                    data-productnights="<?php echo sprintf(JText::_('COM_STRAVEL_PACKAGE_DAYNIGHT_VARRIBLE'),$this->cruise->Days,$this->cruise->Nights); ?>"  
                                                    data-toggle="modal" data-target="#enquiryModal" title="Enquiry this Package Flyer"><img src="images/email.png" width="18" height="13" alt="Enquiry this Package" /></a>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="packageFlyerImageList">
                                                <?php 
                                                    if(!empty($this->cruise->PicUrl->string)) { 
                                                        foreach($this->cruise->PicUrl->string as $img) {  
                                                            echo '<div class="packageFlyerImageItem"><img itemprop="image" class="img-responsive" src="'.preg_replace('/(zh_tw|zh_cn)/','en_us',$img).'" alt="'.$this->cruise->Title.'"/></div>'; 
                                                        }
                                                    } 
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!-- Related package -->
                                    <?php if(!empty($this->allcruises)) : ?>
                                    <div class="packageRelated">
                                        <div class="graysep">&nbsp;</div>
                                        <div class="col-md-12">
                                            <h5><?php echo JText::_('COM_STRAVEL_RELATED_CRUISE'); ?></h5>
                                            <div class="row">
                                                <div class="relatedheader">
                                                    <div class="col-xs-3"><?php echo JText::_('COM_STRAVEL_CRUISE_PACKAGE_NAME') ?></div>
                                                    <div class="col-xs-1"><?php echo JText::_('COM_STRAVEL_RELATED_PACKAGE_NIGHT') ?></div>
                                                    <div class="col-xs-2"><?php echo JText::_('COM_STRAVEL_CRUISE_COMPANY') ?></div>
                                                    <div class="col-xs-2"><?php echo JText::_('COM_STRAVEL_AVG_PRICE') ?></div>
                                                    <div class="col-xs-4"><?php echo JText::_('COM_STRAVEL_DEPARTURE_DATE') ?></div>
                                                </div>
                                            </div>
                                            <?php $i=1; foreach($this->allcruises as $item) : ?>
                                                <?php 
                                                    $urlname=apiHelper::convertUrl($item->Title);
                                                    if((string) $item->Id !== (string) $this->cruise->Id && (int)$this->cruise->CruiseLineId==(int)$item->CruiseLineId) :
                                                        $i++;
                                                ?>
                                                <div class="row">
                                                    <div class="relateditem">
                                                        <div class="col-xs-3 twolineonly"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=cruiseflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php echo $item->Title; ?></a></div>
                                                        <div class="col-xs-1"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=cruiseflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php echo sprintf(JText::_('COM_STRAVEL_PACKAGE_DAYNIGHT_VARRIBLE'),$item->Days,$item->Nights); ?></a></div>
                                                        <div class="col-xs-2"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=cruiseflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php if(!empty($item->CruiseLineName)) {echo $item->CruiseLineName;} ?></a></div>
                                                        <div class="col-xs-2"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=cruiseflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php echo '$'.(int)$item->Price;if($item->PriceInd=='1') echo '+'; ?></a></div>
                                                        <div class="col-xs-4"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=cruiseflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php if(!empty($item->TravelPeriodFr)) echo array_shift(explode('T',$item->TravelPeriodFr)); if(!empty($item->TravelPeriodTo)) echo JText::_('COM_STRAVEL_TO'). array_shift(explode('T',$item->TravelPeriodTo)); ?></a></div>
                                                    </div>
                                                </div>
                                            <?php 
                                                endif;
                                                if($i>3) {
                                                    break;
                                                }
                                                endforeach; 
                                            ?>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <!-- End related package -->
                                    
                                    <!-- Other package -->
                                    <?php if(!empty($this->allcruises)) : ?>
                                    <div class="packageRelated otherPackage">
                                        <div class="graysep">&nbsp;</div>
                                        <div class="col-md-12">
                                            <h5><?php echo JText::_('COM_STRAVEL_OTHER_CRUISE'); ?></h5>
                                            <div class="row">
                                                <div class="relatedheader">
                                                    <div class="col-xs-3"><?php echo JText::_('COM_STRAVEL_CRUISE_PACKAGE_NAME') ?></div>
                                                    <div class="col-xs-1"><?php echo JText::_('COM_STRAVEL_RELATED_PACKAGE_NIGHT') ?></div>
                                                    <div class="col-xs-2"><?php echo JText::_('COM_STRAVEL_CRUISE_COMPANY') ?></div>
                                                    <div class="col-xs-2"><?php echo JText::_('COM_STRAVEL_AVG_PRICE') ?></div>
                                                    <div class="col-xs-4"><?php echo JText::_('COM_STRAVEL_DEPARTURE_DATE') ?></div>
                                                </div>
                                            </div>
                                            <?php $i=1; foreach($this->allcruises as $item) : ?>
                                                <?php 
                                                    $urlname=apiHelper::convertUrl($item->Title);
                                                    if((string)$item->Id !== (string)$this->cruise->Id && (int)$this->cruise->CruiseLineId!=(int)$item->CruiseLineId) :
                                                        $i++;
                                                ?>
                                                <div class="row">
                                                    <div class="relateditem">
                                                        <div class="col-xs-3 twolineonly"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=cruiseflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php echo $item->Title; ?></a></div>
                                                        <div class="col-xs-1"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=cruiseflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php echo sprintf(JText::_('COM_STRAVEL_PACKAGE_DAYNIGHT_VARRIBLE'),$item->Days,$item->Nights); ?></a></div>
                                                        <div class="col-xs-2"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=cruiseflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php if(!empty($item->CruiseLineName)) {echo $item->CruiseLineName;} ?></a></div>
                                                        <div class="col-xs-2"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=cruiseflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php echo '$'.(int)$item->Price;if($item->PriceInd=='1') echo '+'; ?></a></div>
                                                        <div class="col-xs-4"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=cruiseflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php if(!empty($item->TravelPeriodFr)) echo array_shift(explode('T',$item->TravelPeriodFr)); if(!empty($item->TravelPeriodTo)) echo JText::_('COM_STRAVEL_TO').array_shift(explode('T',$item->TravelPeriodTo)); ?></a></div>
                                                    </div>
                                                </div>
                                            <?php 
                                                endif;
                                                if($i>3) {
                                                    break;
                                                }
                                                endforeach; 
                                            ?>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <!-- End other package -->
                                    
                                    <!-- End product content -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Item List -->    
                </div>
            </div>
            <!-- End right pane -->
        </div>
    </div>
</section>