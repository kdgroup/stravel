<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
$lang = JFactory::getLanguage()->getTag();
$cityname = JRequest::getVar('destination','');
//var_dump($this->hotellist);
?>
<section id="hotelresult" class="<?php echo $lang; ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="cbreadcrumb">
                    <li><a href="<?php echo JURI::root(); ?>"><i class="fa fa-home"></i><?php echo JText::_('COM_STRAVEL_HOME_TEXT_BREADCRUMBS') ?></a></li>
                    <li><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=hotelhome'); ?>"><?php echo JText::_('COM_STRAVEL_SEARCH_HOTEL') ?></a></li>
                </ul>
            </div>        
        </div>
        <div class="row">
            <!-- Start left pane -->
            <div class="col-md-3">
                <div class="left-pane">
                    <div class="panetitle">
                        <?php if(empty($cityname)) : ?>
                            <h3><?php echo JText::_('COM_STRAVEL_HOTELHOME') ?></h3>
                        <?php else: ?>
                            <h3><?php echo sprintf(JText::_('COM_STRAVEL_HOTELRESULT_TITLE_VARRIBLE'),$cityname,$this->lowestprice); ?></h3>
                        <?php endif; ?>
                    </div>
                    
                    <?php
                        $presetHelper = new presetHelper(); 
                        echo $presetHelper->getSearchPanel(3);
                    ?>
                    
                    <!-- Start filter of hotelresult -->
                    <div class="filterleft">
                        <div class="selectroute hotelnamefilter">
                            <h4><?php echo JText::_('COM_STRAVEL_HOTELNAME_FILTER') ?></h4>
                            <div class="dropdown">
                                <input data-target="#" data-toggle="dropdown" id="hotelName" type="text" class="form-control" placeholder="<?php echo JText::_('COM_STRAVEL_HOTELNAME_FILTER_PLACEHOLDER') ?>" />
                                <ul class="dropdown-menu">
                                    <?php 
                                        $i=0; 
                                        foreach($this->hotelnamelist as $hotel) :
                                        if($i>14) {
                                            $class = 'displaynone';
                                        }else{
                                            $class = '';
                                        }
                                        $i++; 
                                    ?>
                                    <li class="<?php echo $class ?>" data-id="<?php echo $hotel[0] ?>" data-name="<?php echo $hotel[1] ?>"><a href="#" onclick="return false;"><?php echo $hotel[1] ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                        <div class="selectroute starfilter">
                            <h4><?php echo JText::_('COM_STRAVEL_STAR') ?></h4>
                            <?php for($i=5;$i>=1;$i--) : ?>
                            <div class="route-item"><input id="star-<?php echo $i ?>" data-id="<?php echo $i ?>" type="checkbox" /><label for="star-<?php echo $i ?>"><?php for($j=1;$j<=$i;$j++) { echo '<i class="fa fa-star"></i>'; } ?></label></div>
                            <?php endfor; ?>
                        </div>
                        <div class="selectroute">
                            <div class="companyselect areaselection">
                                <h4><?php echo JText::_('COM_STRAVEL_AREA_SELECTION') ?></h4>
                                <div class="dropdown">
                                    <span data-toggle="dropdown" data-target="#" class="gradientgray"><span class="fakeselect form-control"><?php echo JText::_('COM_STRAVEL_ALL_AREA') ?></span></span>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                        <?php
                                            foreach($this->arealist as $key => $item) {
                                                echo '<li><a href="#" data-id="'.$key.'" onclick="return false;">'.$item.'</a></li>';
                                            }
                                        ?>
                                    </ul>
                                </div>                                                                
                            </div>
                        </div>
                        <div class="selectroute">
                            <div class="dragprice companyselect">
                                <h4><?php echo JText::_('COM_STRAVEL_PRICE') ?></h4>
                                <p class="text-center">
                                    <?php echo JText::_('COM_STRAVEL_HK') ?>
                                    <span id="pricefrom">$0</span>
                                    <?php echo JText::_('COM_STRAVEL_TO') ?>
                                    <span id="priceto">$19999</span>
                                </p>
                                <input id="greendragprice" type="slider" name="price" value="0;19999" />
                            </div>
                        </div>
                    </div>
                    <!-- End filter of hotelresult -->
                    
                </div>                
            </div>
            <!-- End left pane -->
            
            <!-- Start right pane -->
            <div class="col-md-9">
                <div class="right-pane">
                    <div class="basicsort">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="row">
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_SORTBY') ?></p></div>
                                            <div class="col-md-3">
                                                <div class="dropdown">
                                                    <span data-direction="1" data-toggle="dropdown" data-target="#" class="fakesortvalue fakeselectgray form-control"><?php echo JText::_('COM_STRAVEL_PRICE_LOW_TO_HIGH') ?></span>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                                        <li><a data-direction="1" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_PRICE_LOW_TO_HIGH') ?></a></li>
                                                        <li><a data-direction="2" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_PRICE_HIGH_TO_LOW') ?></a></li>
                                                        <li><a data-direction="3" class="sortbyprice hide" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_LASTEST_OFFERS') ?></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                    <!-- end basic sort -->
                    <!-- Filter -->
                    <div class="filter">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="row">
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_POPULAR_CITY_FILTER') ?></p></div>
                                            <div class="col-md-9">
                                                <div class="linklist hothtlcity">
                                                    <?php
                                                        foreach($this->hothtlcities as $item) {
                                                            $infos = explode("\n",$item->Content);
                                                            $urlname = apiHelper::convertUrl($infos[0]);
                                                            echo '<a href='.JRoute::_('index.php?option=com_stravel&view=hotelresult&city='.$item->Keyword.'&destination='.$urlname).'>'.$infos[0].'</a>';
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <!-- End fitler -->
                    
                    <!-- Item List -->
                    <div class="itemlist">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1 listcontainer">
                                <div class="hotelresultheader row">
                                    <div class="col-xs-5 col-md-4"><?php echo JText::_('COM_STRAVEL_SEARCH_HOTEL'); ?></div>
                                    <div class="col-xs-3 hidden-xs"><?php echo JText::_('COM_STRAVEL_HOTEL_CITY_LOCATION'); ?></div>
                                    <div class="col-xs-1"><?php echo JText::_('COM_STRAVEL_STAR'); ?></div>
                                    <div class="col-xs-2"><?php echo JText::_('COM_STRAVEL_HOTEL_AVG_PRICE'); ?></div>
                                    <div class="col-xs-2 col-md-1"><span class="htinfo"><?php echo JText::_('COM_STRAVEL_HOTEL_INFOMATION'); ?></span></div>
                                    <div class="col-xs-2 col-md-1 text-center"><?php echo JText::_('COM_STRAVEL_QUERY'); ?></div>
                                </div>
                                    <?php 
                                        $i=0;$j=1;
                                        foreach($this->hotellist as $item) :
                                        if((int) $item->RqtPrice != 0) :
                                        if($i%15==0) {
                                            echo '<div class="page-sep page-sep'.$j.'">';
                                            $j++;
                                        } 
                                        switch($lang) {
                                            case 'en-GB':
                                                if(!empty($item->HotelName)) {
                                                    $hotelname = $item->HotelName;    
                                                }else{
                                                    if(!empty($item->HotelName2)) {
                                                        $hotelname = $item->HotelName2;
                                                    }else{
                                                        $hotelname = $item->HotelName1;
                                                    }    
                                                }                                                
                                                break;
                                            case 'zh-CN':
                                                if(!empty($item->HotelName2)) {
                                                    $hotelname = $item->HotelName2;    
                                                }else{
                                                    if(!empty($item->HotelName1)) {
                                                        $hotelname = $item->HotelName1;
                                                    }else{
                                                        $hotelname = $item->HotelName;
                                                    }    
                                                } 
                                                break;
                                            case 'zh-TW':
                                                if(!empty($item->HotelName1)) {
                                                    $hotelname = $item->HotelName1;    
                                                }else{
                                                    if(!empty($item->HotelName2)) {
                                                        $hotelname = $item->HotelName2;
                                                    }else{
                                                        $hotelname = $item->HotelName;
                                                    }    
                                                }
                                                break;
                                            default:
                                                if(!empty($item->HotelName2)) {
                                                    $hotelname = $item->HotelName2;    
                                                }else{
                                                    if(!empty($item->HotelName1)) {
                                                        $hotelname = $item->HotelName1;
                                                    }else{
                                                        $hotelname = $item->HotelName;
                                                    }    
                                                }
                                                break;    
                                        }
                                        $locationId = array_search((string)$item->Location,$this->arealist);                                        
                                    ?>
                                        <div class="row">
                                            <div data-hotelcode="<?php echo $item->HotelCode; ?>" data-locationid="<?php echo $locationId ?>" data-area="<?php echo $item->Location; ?>" data-price="<?php echo (int)$item->RqtPrice ?>" data-hotelstar="<?php echo $item->Star ?>" class="product-container col-md-12">
                                                <div class="hotelresult-item row">
                                                    <div class="col-xs-5 col-md-4"><h5><?php echo $hotelname ?></h5></div>
                                                    <div class="col-xs-3 hidden-xs"><h6><?php echo $cityname.'<br />'.$item->Location; ?></h6></div>
                                                    <div class="col-xs-1"><span class="hotelresult-txt"><?php echo $item->Star ?></span></div>
                                                    <div class="col-xs-2"><span class="hotelresult-txt">$<?php echo (int) $item->RqtPrice; ?>+</span></div>
                                                    <div class="col-xs-2 col-md-1"><span class="zoomhotel" data-hotelcode="<?php echo $item->HotelCode ?>" data-toggle="modal" data-target="#myLargeHotelResultModal" class="pull-right">&nbsp;</span></div>
                                                    <div class="col-xs-2 col-md-1 text-center"><a class="envelophotel" href="#enquiryModal"
                                                                    data-frompage="hotel"
                                                                    data-productid="<?php echo $item->Id; ?>" 
                                                                    data-productname="<?php echo $hotelname; ?>" 
                                                                    data-productprice="$<?php echo (int)$item->RqtPrice; ?>+" 
                                                                    data-productdst="<?php echo $cityname.' - '.$item->Location; ?>"
                                                                    data-productdate="<?php echo JRequest::getVar('checkinDate'); ?>"
                                                                    data-productnights="<?php echo JRequest::getVar('nights'); ?>" 
                                                                    data-toggle="modal" data-target="#enquiryModal">&nbsp;</a>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                        if($i%15==14 || ($i+1)>=count($this->hotellist)) {echo '</div>';} 
                                        $i++; 
                                        endif;endforeach; 
                                    ?>
                            </div>
                        </div>
                    <!-- End Item List --> 
                        
                        <!-- Disclaimer & pagination -->                        
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row disclaimer noborder">
                                    <?php if(!empty($this->disclaimer)) : ?>
                                    <div class="col-md-7"><?php echo str_replace("\n",'<br />',$this->disclaimer->Content) ?></div>
                                    <?php endif; ?>
                                    <?php if($i>15) : ?>
                                    <div class="col-md-5 text-right">
                                        <ul class="pagination">                                  
                                          <?php 
                                            $n = $i%15;
                                            if($n==0) {
                                                $n = $i/15;
                                            }else{
                                                $n = floor($i/15)+1;
                                            }
                                            echo '<li data-totalpage="'.$n.'" data-currentpage="1" data-page="prev" class="first disabled"><a href="#">&laquo;</a></li>'; 
                                            for($i=1;$i<=$n;$i++) {
                                                if($i==1) {
                                                    $cls = "disabled";
                                                }else{
                                                    $cls = '';
                                                }
                                                if($i>=1 && $i<=5) {
                                                    $cls .= " showed";
                                                }else{
                                                    $cls .= '';
                                                }
                                                echo '<li data-totalpage="'.$n.'" data-currentpage="1" class="pagsseitem pagsseitem'.$i.' '.$cls.'" data-page="'.$i.'"><a href="#">'.$i.'</a></li>';
                                            }
                                            echo '<li data-totalpage="'.$n.'" data-currentpage="1" data-page="next" class="last"><a href="#">&raquo;</a></li>'; 
                                          ?>
                                        </ul>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>                        
                        <!-- Disclaimer -->
                    </div>   
                </div>
            </div>
            <!-- End right pane -->
        </div>
    </div>
</section>