<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
$lang = JFactory::getLanguage()->getTag();
?>
<section id="hotelhome" class="<?php echo $lang; ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="cbreadcrumb">
                    <li><a href="<?php echo JURI::root(); ?>"><i class="fa fa-home"></i><?php echo JText::_('COM_STRAVEL_HOME_TEXT_BREADCRUMBS') ?></a></li>
                    <li><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=hotelhome'); ?>"><?php echo JText::_('COM_STRAVEL_SEARCH_HOTEL') ?></a></li>
                </ul>
            </div>        
        </div>
        <div class="row">
            <!-- Start left pane -->
            <div class="col-md-3">
                <div class="left-pane">
                    <div class="panetitle">
                        <h3><?php echo JText::_('COM_STRAVEL_HOTELHOME') ?></h3>
                    </div>
                    
                    <?php
                        $presetHelper = new presetHelper(); 
                        echo $presetHelper->getSearchPanel(3);
                    ?>
                    
                    <div class="topten">
                        <h3><?php echo JText::_('COM_STRAVEL_TOPTEN_HOTEL') ?></h3>
                        <div class="topten-info">
                            <?php
                                $i = 1; 
                                foreach($this->hotellist as $item) : 
                                if($i>10) {break;}
                                $top10 = false;
                                foreach($item->AdvIdeaList->AdvIdea as $preference) {
                                    if($preference->Id==191) {
                                        $top10 = true;
                                    }
                                }
                                if($top10) :
                                $i++;
                                $urlname = apiHelper::convertUrl($item->CityName);
                            ?>
                            <div class="topten-item" data-toggle="modal" data-target="#myHotelSearchModal">
                                <div class="pull-left name"><?php echo $item->CityName ?></div>
                                <div class="pull-left time"><?php echo $item->HotelName ?></div>
                                <div class="pull-left price text-right">$<?php echo (int)$item->Price; ?>+</div>
                            </div>
                            <?php endif;endforeach; ?>
                        </div>
                    </div>
                </div>                
            </div>
            <!-- End left pane -->
            
            <!-- Start right pane -->
            <div class="col-md-9">
                <div class="right-pane">
                    <div class="basicsort">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="row">
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_SORTBY') ?></p></div>
                                            <div class="col-md-3">
                                                <div class="dropdown">
                                                    <span data-direction="1" data-toggle="dropdown" data-target="#" class="fakesortvalue fakeselectgray form-control"><?php echo JText::_('COM_STRAVEL_PRICE_LOW_TO_HIGH') ?></span>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                                        <li><a data-direction="1" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_PRICE_LOW_TO_HIGH') ?></a></li>
                                                        <li><a data-direction="2" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_PRICE_HIGH_TO_LOW') ?></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                    <!-- end basic sort -->
                    <!-- Filter -->
                    <div class="filter">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="row">
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_POPULAR_CITY_FILTER') ?></p></div>
                                            <div class="col-md-9">
                                                <div class="linklist hothtlcity">
                                                    <?php
                                                        foreach($this->hothtlcities as $item) {
                                                            $infos = explode("\n",$item->Content);
                                                            $urlname = apiHelper::convertUrl($infos[0]);
                                                            echo '<a href='.JRoute::_('index.php?option=com_stravel&view=hotelresult&city='.$item->Keyword.'&destination='.$urlname).'>'.$infos[0].'</a>';
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <!-- End fitler -->
                    
                    <!-- Item List -->
                    <div class="itemlist">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1 listcontainer">
                                <div class="row">
                                    <?php
                                        $usedHtl = array(); 
                                        $i = 1;
                                        foreach($this->hotellist26 as $item) :
                                        if($i>6) {break;}
                                        $usedHtl[] = $item->hotel_code;
                                        $i++;
                                        switch($lang) {
                                            case 'en-GB':
                                                if(!empty($item->hotel_en)) {
                                                    $hotelname = $item->hotel_en;
                                                }else{
                                                    if(!empty($item->hotel_zh_tw)) {
                                                        $hotelname = $item->hotel_zh_tw;    
                                                    }else{
                                                        $hotelname = $item->hotel_zh_cn;
                                                    }
                                                }
                                                break;
                                            case 'zh-TW':
                                                if(!empty($item->hotel_zh_tw)) {
                                                    $hotelname = $item->hotel_zh_tw;
                                                }else{
                                                    if(!empty($item->hotel_zh_cn)) {
                                                        $hotelname = $item->hotel_zh_cn;    
                                                    }else{
                                                        $hotelname = $item->hotel_en;
                                                    }
                                                }
                                                break;
                                            case 'zh-CN':
                                                if(!empty($item->hotel_zh_cn)) {
                                                    $hotelname = $item->hotel_zh_cn;
                                                }else{
                                                    if(!empty($item->hotel_zh_tw)) {
                                                        $hotelname = $item->hotel_zh_tw;    
                                                    }else{
                                                        $hotelname = $item->hotel_en;
                                                    }
                                                }
                                                break;
                                            default:
                                                if(!empty($item->hotel_zh_tw)) {
                                                    $hotelname = $item->hotel_zh_tw;
                                                }else{
                                                    if(!empty($item->hotel_zh_cn)) {
                                                        $hotelname = $item->hotel_zh_cn;    
                                                    }else{
                                                        $hotelname = $item->hotel_en;
                                                    }
                                                }
                                                break;
                                        }
                                    ?>
                                        <div data-price="<?php echo $item->hotel_price ?>" class="product-container col-md-4">
                                            <div class="hotel-item product-item">
                                                <div data-toggle="modal" data-target="#myHotelSearchModal" class="hotel-image product-image text-center">
                                                    <img class="img-responsive lazy" data-original="<?php echo empty($item->hotel_img)?'images/Hotel4.jpg':$item->hotel_img ?>" alt="package img" />
                                                    <div class="caption">
                                                        <h6 class="pull-left"><?php echo $hotelname; ?></h6>
                                                        <span class="pull-right"><img src="images/GreyMagnifierIcon.png" alt="find"/></span>
                                                    </div>
                                                    <?php if(!empty($item->Description)) : ?>
                                                    <div class="package-feature">
                                                        <?php echo $item->Description; ?>
                                                    </div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="hotel-info product-info">
                                                    <h6 data-toggle="modal" data-target="#myHotelSearchModal"><?php echo $item->cityname ?></h6>
                                                    <div class="hotel-bottom-info">
                                                        <div data-toggle="modal" data-target="#myHotelSearchModal" class="hotel-price"><span>$<?php echo $item->hotel_price; ?>+</span></div>
                                                        <div class="hotel-link text-right">
                                                            <a href="#enquiryModal"
                                                                data-frompage="hotel"
                                                                data-productid="<?php echo $item->hotel_code; ?>" 
                                                                data-productname="<?php echo $hotelname; ?>" 
                                                                data-productprice="$<?php echo $item->hotel_price; ?>+" 
                                                                data-productdst="<?php echo $item->cityname ?>"
                                                                data-productdate="<?php echo JRequest::getVar('checkindate'); ?>"
                                                                data-productnights="<?php echo JRequest::getVar('nights'); ?>" 
                                                                data-toggle="modal" data-target="#enquiryModal">&nbsp;</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    <!-- End Item List --> 
                        
                    <!-- Table -->
                        <div class="row margintop">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row noborder">
                                    <!-- Left table -->
                                    <div class="col-md-6">
                                        <div class="headrow">
                                            <div class="colum-1">城市</div>
                                            <div class="colum-2">酒店</div>
                                            <div class="colum-3">平均房價(起)</div>
                                            <div class="colum-4">詳情</div>
                                            <div class="colum-5">查詢</div>
                                        </div>
                                        <?php
                                            $i=1;
                                            foreach($this->hotellist26 as $item) :
                                            if($i>10) {break;}
                                            if(array_search($item->hotel_code,$usedHtl)===false) :
                                            $usedHtl[] = $item->hotel_code;
                                            $i++;
                                            switch($lang) {
                                                case 'en-GB':
                                                    if(!empty($item->hotel_en)) {
                                                        $hotelname = $item->hotel_en;
                                                    }else{
                                                        if(!empty($item->hotel_zh_tw)) {
                                                            $hotelname = $item->hotel_zh_tw;    
                                                        }else{
                                                            $hotelname = $item->hotel_zh_cn;
                                                        }
                                                    }
                                                    break;
                                                case 'zh-TW':
                                                    if(!empty($item->hotel_zh_tw)) {
                                                        $hotelname = $item->hotel_zh_tw;
                                                    }else{
                                                        if(!empty($item->hotel_zh_cn)) {
                                                            $hotelname = $item->hotel_zh_cn;    
                                                        }else{
                                                            $hotelname = $item->hotel_en;
                                                        }
                                                    }
                                                    break;
                                                case 'zh-CN':
                                                    if(!empty($item->hotel_zh_cn)) {
                                                        $hotelname = $item->hotel_zh_cn;
                                                    }else{
                                                        if(!empty($item->hotel_zh_tw)) {
                                                            $hotelname = $item->hotel_zh_tw;    
                                                        }else{
                                                            $hotelname = $item->hotel_en;
                                                        }
                                                    }
                                                    break;
                                                default:
                                                    if(!empty($item->hotel_zh_tw)) {
                                                        $hotelname = $item->hotel_zh_tw;
                                                    }else{
                                                        if(!empty($item->hotel_zh_cn)) {
                                                            $hotelname = $item->hotel_zh_cn;    
                                                        }else{
                                                            $hotelname = $item->hotel_en;
                                                        }
                                                    }
                                                    break;
                                            } 
                                        ?>
                                            <div class="itemrow">
                                                <div data-toggle="modal" data-target="#myHotelSearchModal" class="colum-1"><?php echo $item->cityname; ?></div>
                                                <div data-toggle="modal" data-target="#myHotelSearchModal" class="colum-2"><?php echo $hotelname; ?></div>
                                                <div data-toggle="modal" data-target="#myHotelSearchModal" class="colum-3">$<?php echo $item->hotel_price; ?>+</div>
                                                <div class="colum-4"><img data-toggle="modal" data-target="#myHotelSearchModal" class="img-responsive" src="images/GreyMagnifierIcon.png" alt="hotel stravel" /></div>
                                                <div class="colum-5"><img data-frompage="hotel" data-toggle="modal" data-target="#enquiryModal" data-productid="<?php echo $item->hotel_code ?>" data-productname="<?php echo $hotelname; ?>" data-productprice="$<?php echo $item->hotel_price ?>+" data-productdst="<?php echo $item->cityname ?>" data-productdate="<?php echo JRequest::getVar('checkindate'); ?>" data-productnights="<?php echo JRequest::getVar('nights'); ?>" src="images/GreyEnvelop.png" alt="hotel stravel os" /></div>
                                            </div>
                                        <?php endif;endforeach; ?>
                                    </div>
                                    <!-- End left table -->
                                    <!-- Left table -->
                                    <div class="col-md-6">
                                        <div class="headrow headrowhastop">
                                            <div class="colum-1">城市</div>
                                            <div class="colum-2">酒店</div>
                                            <div class="colum-3">平均房價(起)</div>
                                            <div class="colum-4">詳情</div>
                                            <div class="colum-5">查詢</div>
                                        </div>
                                        <?php 
                                            $i = 1;
                                            foreach($this->hotellist26 as $item) :
                                            if($i>10) {break;}
                                            if(array_search($item->hotel_code,$usedHtl)===false) :
                                            $i++;
                                            switch($lang) {
                                                case 'en-GB':
                                                    if(!empty($item->hotel_en)) {
                                                        $hotelname = $item->hotel_en;
                                                    }else{
                                                        if(!empty($item->hotel_zh_tw)) {
                                                            $hotelname = $item->hotel_zh_tw;    
                                                        }else{
                                                            $hotelname = $item->hotel_zh_cn;
                                                        }
                                                    }
                                                    break;
                                                case 'zh-TW':
                                                    if(!empty($item->hotel_zh_tw)) {
                                                        $hotelname = $item->hotel_zh_tw;
                                                    }else{
                                                        if(!empty($item->hotel_zh_cn)) {
                                                            $hotelname = $item->hotel_zh_cn;    
                                                        }else{
                                                            $hotelname = $item->hotel_en;
                                                        }
                                                    }
                                                    break;
                                                case 'zh-CN':
                                                    if(!empty($item->hotel_zh_cn)) {
                                                        $hotelname = $item->hotel_zh_cn;
                                                    }else{
                                                        if(!empty($item->hotel_zh_tw)) {
                                                            $hotelname = $item->hotel_zh_tw;    
                                                        }else{
                                                            $hotelname = $item->hotel_en;
                                                        }
                                                    }
                                                    break;
                                                default:
                                                    if(!empty($item->hotel_zh_tw)) {
                                                        $hotelname = $item->hotel_zh_tw;
                                                    }else{
                                                        if(!empty($item->hotel_zh_cn)) {
                                                            $hotelname = $item->hotel_zh_cn;    
                                                        }else{
                                                            $hotelname = $item->hotel_en;
                                                        }
                                                    }
                                                    break;
                                            } 
                                        ?>
                                            <div class="itemrow">
                                                <div data-toggle="modal" data-target="#myHotelSearchModal" class="colum-1"><?php echo $item->cityname; ?></div>
                                                <div data-toggle="modal" data-target="#myHotelSearchModal" class="colum-2"><?php echo $hotelname; ?></div>
                                                <div data-toggle="modal" data-target="#myHotelSearchModal" class="colum-3">$<?php echo $item->hotel_price; ?>+</div>
                                                <div class="colum-4"><img data-toggle="modal" data-target="#myHotelSearchModal" class="img-responsive" src="images/GreyMagnifierIcon.png" alt="hotel stravel" /></div>
                                                <div class="colum-5"><img data-frompage="hotel" data-toggle="modal" data-target="#enquiryModal" data-productid="<?php echo $item->hotel_code ?>" data-productname="<?php echo $hotelname; ?>" data-productprice="$<?php echo $item->hotel_price ?>+" data-productdst="<?php echo $item->cityname ?>" data-productdate="<?php echo JRequest::getVar('checkindate'); ?>" data-productnights="<?php echo JRequest::getVar('nights'); ?>" src="images/GreyEnvelop.png" alt="hotel stravel os" /></div>
                                            </div>
                                        <?php endif;endforeach; ?>
                                    </div>
                                    <!-- End left table -->
                                </div>
                            </div>    
                        </div>
                    <!-- End table -->
                        <!-- Disclaimer -->
                        <?php if(!empty($this->disclaimer)) : ?>
                            <div class="row">
                                <div class="col-md-11 col-md-offset-1">
                                    <div class="row disclaimer noborder"><div class="col-md-12"><?php echo str_replace("\n",'<br />',$this->disclaimer->Content) ?></div></div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <!-- Disclaimer -->
                    </div>   
                </div>
            </div>
            <!-- End right pane -->
        </div>
    </div>
</section>