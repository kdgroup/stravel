<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
$lang = str_replace('-','_',strtolower(JFactory::getLanguage()->getTag()));
//var_dump($this->packageList);
?>
<section id="packagehome" class="<?php echo $lang; ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="cbreadcrumb">
                    <li><a href="<?php echo JURI::root(); ?>"><i class="fa fa-home"></i><?php echo JText::_('COM_STRAVEL_HOME_TEXT_BREADCRUMBS') ?></a></li>
                    <li><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packagehome'); ?>"><?php echo JText::_('COM_STRAVEL_SEARCH_PACKAGE') ?></a></li>
                </ul>
            </div>        
        </div>
        <div class="row">
            <!-- Start left pane -->
            <div class="col-md-3">
                <div class="left-pane">
                    <div class="panetitle">
                        <h3><?php echo JText::_('COM_STRAVEL_PACKAGEHOME') ?></h3>
                    </div>
                    
                    <?php
                        $presetHelper = new presetHelper(); 
                        echo $presetHelper->getSearchPanel(1); 
                    ?>
                    
                    <?php if(!empty($this->top10)) : ?>
                    <div class="topten">
                        <h3><?php echo JText::_('COM_STRAVEL_TOPTEN_PACKAGE') ?></h3>
                        <div class="topten-info">
                            <?php $i=0;
                                foreach($this->top10 as $topten) :
                                    $isFront2Item = false;
                                    foreach($topten->AdvIdeaList->AdvIdea as $idea) {
                                        if($idea->Id==185) {
                                            $isFront2Item = true;
                                        }
                                    }
                                    if($isFront2Item) :
                                    if($i>9) break; $i++;
                                    $urlname=apiHelper::convertUrl($topten->Abstract); 
                            ?>
                            <div class="topten-item">
                                <div class="pull-left name"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packageflyer&id='.$topten->Id.'&name='.$urlname) ?>"><?php echo $topten->CityList->City->CityName; ?></a></div>
                                <div class="pull-left time"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packageflyer&id='.$topten->Id.'&name='.$urlname) ?>"><?php echo $topten->Abstract ?></a></div>
                                <div class="pull-left price text-right">$<?php echo $topten->Price;if($topten->PriceInd=='Fr') echo '+'; ?></div>
                            </div>
                            <?php endif;endforeach; ?>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>                
            </div>
            <!-- End left pane -->
            
            <!-- Start right pane -->
            <div class="col-md-9">
                <div class="right-pane">
                    <div class="basicsort">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="row">
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_SORTBY') ?></p></div>
                                            <div class="col-md-3">
                                                <div class="dropdown">
                                                    <span data-direction="1" data-toggle="dropdown" data-target="#" class="fakesortvalue fakeselectgray form-control"><?php echo JText::_('COM_STRAVEL_PRICE_LOW_TO_HIGH') ?></span>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a data-direction="1" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_PRICE_LOW_TO_HIGH') ?></a></li>
                                                        <li><a data-direction="2" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_PRICE_HIGH_TO_LOW') ?></a></li>
                                                        <li><a data-direction="3" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_LASTEST_OFFERS') ?></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="dropdown">
                                                    <span data-toggle="dropdown" data-target="#" class="fakeselectgray form-control">主題精選</span>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <?php
                                                            if(!empty($this->featuredPackage)) { 
                                                                foreach($this->featuredPackage as $cat) {
                                                                    //add language filter 
                                                                    if((string) $cat->Lang == (string) $lang){
                                                                        $urlname = apiHelper::convertUrl($cat->Name);
                                                                        echo '<li><a href="'.JRoute::_('index.php?option=com_stravel&view=packagehome&ideaid='.$cat->Id.'&ideaname='.$urlname).'">'.$cat->Name.'</a></li>';
                                                                    }
                                                                }
                                                            }
                                                        ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                    <!-- end basic sort -->
                    <!-- Filter -->
                    <div class="filter">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-11">
                                        <!-- Featured Package -->
                                        <?php if(!empty($this->featuredPackage)) :?>
                                        <div class="row">                                            
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_CATEGORY_FILTER') ?></p></div>
                                            <div class="col-md-10">
                                                <div class="linklist">
                                                <?php 
                                                    foreach($this->featuredPackage as $cat) {
                                                         //add language filter 
                                                        if((string) $cat->Lang == (string) $lang && $cat->Type != 12 && $cat->Type!=13){
                                                            $urlname = apiHelper::convertUrl($cat->Name);
                                                            echo '<a href="'.JRoute::_('index.php?option=com_stravel&view=packagehome&ideaid='.$cat->Id.'&ideaname='.$urlname).'">'.$cat->Name.'</a>';
                                                        }
                                                    }
                                                ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <!-- End featured Package -->
                                        
                                        <!-- Hot cities -->
                                        <?php if(!empty($this->mergecities)) :?>
                                        <div class="row">
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_POPULAR_CITY_FILTER') ?></p></div>
                                            <div class="col-md-10">
                                                <div class="linklist">
                                                <?php
                                                    $i = 1;
                                                    //$cityName = 'CityName_'.strtolower(str_replace('-','_', (string) $lang)); 
                                                    foreach($this->mergecities as $city) {
                                                        //if($i>10) {break;} $i++;
                                                        //$urlname = apiHelper::convertUrl($city->$cityName);
                                                        $infos = explode("\n",$city->Content);
                                                        $urlname = apiHelper::convertUrl($infos[0]);
                                                        //echo '<a href="'.JRoute::_('index.php?option=com_stravel&view=packagehome&city='.$city->CityCode.'&cityname='.$urlname).'">'.$city->$cityName.'</a>';
                                                        echo '<a href="'.JRoute::_('index.php?option=com_stravel&view=packagehome&city='.$city->Keyword.'&cityname='.$urlname).'">'.$infos[0].'</a>';
                                                    }
                                                ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <!-- End hot cities -->
                                    </div>
                                </div>
                            </div>                            
                        </div> 
                    </div>
                    <!-- End fitler -->
                    
                    <!-- Item List -->
                    <div class="itemlist">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1 listcontainer">
                                    <?php $urlArsoe = array(); $i=0;$j=1; if(!empty($this->packageList)) : foreach($this->packageList as $package) : ?>
                                        <?php
                                            if($this->packagehome && $i>=12) {break;} 
                                            $urlname=apiHelper::convertUrl($package->Abstract);
                                            $urlArsoe[] = $urlname;
                                            $isPromotion = false;
                                            $isTour = false;
                                            foreach($package->AdvIdeaList->AdvIdea as $idea) {
                                                if($idea->Id==183) {
                                                    $isPromotion = true;
                                                }
                                                if($idea->Type==12) {
                                                    $isTour = true;
                                                }    
                                            }
                                            if($isPromotion || ($this->packagehome==false && $isTour==false)) : 
                                                if($i%3==0) {
                                                    if($i%12==0) {
                                                        echo '<div class="page-sep page-sep'.$j.'">';
                                                        $j++;
                                                    }
                                                    if($i>=9 && $this->packagehome) {
                                                        echo '<div class="row noborder">';
                                                    }else{
                                                        echo '<div class="row">';    
                                                    }                                                    
                                                }
                                                $imgurl = 'images/Taipei7.jpg';
                                                if(empty($package->ImgUrls->string)) {
                                                    if(!empty($package->CityList->City->ImgUrls->string)) {
                                                        $imgurl = $package->CityList->City->ImgUrls->string;
                                                        if(count($imgurl)>1) {
                                                            $index = rand(0,count($imgurl)-1);
                                                            $imgurl = $imgurl[$index];
                                                        }
                                                    }
                                                }else{
                                                    $imgurl = $package->ImgUrls->string;
                                                } 
                                        ?>
                                        <div data-keyword="<?php echo $package->Keyword ?>" data-abstract="<?php echo $package->Abstract ?>" data-effectfrom="<?php echo strtotime($package->TravelPeriodFr); ?>" data-price="<?php echo $package->Price ?>" class="product-container col-md-4">
                                            <div class="package-item product-item">
                                                <div class="package-image product-image text-center">
                                                    <img class="img-responsive lazy" data-original="<?php echo $imgurl; ?>" alt="package img" />
                                                    <?php if(!empty($package->AdvIdeaList->AdvIdea)) : ?>
                                                        <div class="advidea">
                                                            <?php
                                                                $jk = 0; 
                                                                foreach($package->AdvIdeaList->AdvIdea as $idea) {
                                                                    if($jk>=2) break;
                                                                    if($idea->Type==11) {
                                                                       echo '<span class="ideaicon">'.$idea->Content.'</span>'; 
                                                                       $jk++; 
                                                                    }
                                                                }                                                           
                                                            ?>
                                                        </div>
                                                    <?php endif; ?>
                                                    <div class="package-feature" onclick="location.href='<?php echo JRoute::_('index.php?option=com_stravel&view=packageflyer&id='.$package->Id.'&name='.$urlname) ?>';">
                                                        <?php if(!empty($package->Content)) echo $package->Content ?>
                                                    </div>
                                                </div>
                                                <div class="package-info product-info">
                                                    <h6><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packageflyer&id='.$package->Id.'&name='.$urlname) ?>"><?php echo apiHelper::__wordProcess($package->Abstract); ?></a></h6>
                                                    <div class="row">
                                                        <div class="package-price product-price col-xs-4"><h4>$<?php echo $package->Price; if($package->PriceInd=='Fr') echo '+'; ?></h4></div>
                                                        <div class="package-link product-link col-xs-8 text-right">
                                                            <?php if(!empty($package->CarrierList->Carrier->CarrierCode) && !empty($package->CarrierList->Carrier->CarrierName)) : ?>
                                                            <div class="airlineinfo <?php if(strlen($package->CarrierList->Carrier->CarrierCode)>2) echo 'extended'; ?>"><span><?php echo $package->CarrierList->Carrier->CarrierCode; ?></span><span class="infoicon" data-toggle="tooltip" data-placement="bottom" title="<?php echo $package->CarrierList->Carrier->CarrierName; ?>">&nbsp;</span></div>
                                                            <?php endif; ?>
                                                            <div class="imgbtnbg">
                                                                <a href="<?php if(!empty($package->AttachmentList->Attachment)) { foreach($package->AttachmentList->Attachment as $attach) {  if($attach->Type=='Flyer') echo $attach->Url; } } ?>" target="_blank">&nbsp;</a>
                                                                <a href="#enquiryModal"
                                                                    data-frompage="package" 
                                                                    data-productname="<?php echo $package->Abstract; ?>" 
                                                                    data-productprice="$<?php echo $package->Price; if($package->PriceInd=='Fr') echo '+'; ?>" 
                                                                    data-productid="<?php echo $package->Id ?>"
                                                                    data-productdate="<?php echo $this->departuredate; ?>"
                                                                    data-productnights="<?php echo sprintf(JText::_('COM_STRAVEL_PACKAGE_DAYNIGHT_VARRIBLE'),$package->Days,$package->Nights); ?>" 
                                                                    data-toggle="modal" 
                                                                    data-target="#enquiryModal">&nbsp;
                                                                </a>                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php 
                                                if($i%3==2 || ($i+1)>=($this->totalhomelist)) {echo '</div>';}
                                                if($i%12==11 || ($i+1)>=($this->totalhomelist)) {echo '</div>';} 
                                                $i++; 
                                            endif; 
                                        ?>
                                    <?php endforeach;endif; ?>
                            </div>
                        </div>
                        <!-- Disclaimer & pagination -->                        
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row disclaimer noborder">
                                    <?php 
                                        if(!empty($this->disclaimer)) :
                                        if($this->packagehome) {
                                            $cols = 12;
                                        }else{
                                            $cols = 7;
                                        } 
                                    ?>
                                    <div class="col-md-<?php echo $cols ?>"><?php echo str_replace("\n",'<br />',$this->disclaimer->Content) ?></div>
                                    <?php endif; ?>
                                    <?php if($i>12) : ?>
                                    <div class="col-md-5 text-right">
                                        <ul class="pagination">                                  
                                          <?php 
                                            $n = $i%12;
                                            if($n==0) {
                                                $n = $i/12;
                                            }else{
                                                $n = floor($i/12)+1;
                                            }
                                            echo '<li data-totalpage="'.$n.'" data-currentpage="1" data-page="prev" class="first disabled"><a href="#">&laquo;</a></li>'; 
                                            for($i=1;$i<=$n;$i++) {
                                                if($i==1) {
                                                    $cls = "disabled";
                                                }else{
                                                    $cls = '';
                                                }
                                                if($i>=1 && $i<=5) {
                                                    $cls .= " showed";
                                                }else{
                                                    $cls .= '';
                                                }
                                                echo '<li data-totalpage="'.$n.'" data-currentpage="1" class="pagsseitem pagsseitem'.$i.' '.$cls.'" data-page="'.$i.'"><a href="#">'.$i.'</a></li>';
                                            }
                                            echo '<li data-totalpage="'.$n.'" data-currentpage="1" data-page="next" class="last"><a href="#">&raquo;</a></li>'; 
                                          ?>
                                        </ul>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>                        
                        <!-- Disclaimer -->
                    </div>
                    <!-- End Item List -->    
                </div>
            </div>
            <!-- End right pane -->
        </div>
    </div>
</section>
<?php 
    $namecount = count($urlArsoe);
    $uniquename = array_unique($urlArsoe);
    $namecount2 = count($uniquename);
    echo '<div class="hide">Total: '.count($this->packageList).'Origin:'.$namecount.'. After filter: '.$namecount2.'</div>';
?>