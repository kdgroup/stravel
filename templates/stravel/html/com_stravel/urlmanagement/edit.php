<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
//var_dump($this->items);
?>
<section id="urledit">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="cbreadcrumb">
                    <li><a href="<?php echo JURI::root(); ?>"><i class="fa fa-home"></i><?php echo JText::_('COM_STRAVEL_HOME_TEXT_BREADCRUMBS') ?></a></li>
                    <li><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=urlmanagement'); ?>"><?php echo JText::_('COM_STRAVEL_URLMANAGEMENT') ?></a></li>
                </ul>
            </div>        
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="urlmanagmentedit">
                    <form class="form-horizontal" role="form" action="<?php echo JRoute::_('index.php?option=com_stravel&view=urlmanagement'); ?>" method="post" name="adminForm">
                        <div class="form-group">
                            <label for="inputPagetitle" class="col-sm-2 control-label"><?php echo JText::_('COM_STRAVEL_URLMANAGEMENT_PAGE_TITLE') ?></label>
                            <div class="col-sm-10">
                                <textarea name="pagetitle" class="form-control" id="inputPagetitle" placeholder="<?php echo JText::_('COM_STRAVEL_URLMANAGEMENT_PAGE_TITLE') ?>"><?php echo $this->url->page_title ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPagekeyword" class="col-sm-2 control-label"><?php echo JText::_('COM_STRAVEL_URLMANAGEMENT_PAGE_KEYWORD') ?></label>
                            <div class="col-sm-10">
                                <textarea name="pagekeyword" class="form-control" id="inputPagekeyword" placeholder="<?php echo JText::_('COM_STRAVEL_URLMANAGEMENT_PAGE_KEYWORD') ?>"><?php echo $this->url->page_keyword ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPagedesc" class="col-sm-2 control-label"><?php echo JText::_('COM_STRAVEL_URLMANAGEMENT_PAGE_DESC') ?></label>
                            <div class="col-sm-10">
                                <textarea name="pagedesc" class="form-control" id="inputPagedesc" placeholder="<?php echo JText::_('COM_STRAVEL_URLMANAGEMENT_PAGE_DESC') ?>"><?php echo $this->url->page_description ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="hidden" name="id" value="<?php echo $this->url->id ?>" />
                                <input type="hidden" name="layout" value="edit" />
                                <input type="hidden" name="action" value="0" />
                                <button onclick="document.adminForm.layout.value='edit';document.adminForm.action.value='save';" type="submit" class="btn btn-success"><?php echo JText::_('COM_STRAVEL_SAVE') ?></button>
                                <button onclick="document.adminForm.layout.value='default';document.adminForm.action.value='save';" type="submit" class="btn btn-info"><?php echo JText::_('COM_STRAVEL_SAVE_CLOSE') ?></button>
                                <a href="<?php echo JRoute::_('index.php?option=com_stravel&view=urlmanagement'); ?>" class="btn btn-warning"><?php echo JText::_('COM_STRAVEL_CANCEL') ?></a>
                            </div>
                        </div>    
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>