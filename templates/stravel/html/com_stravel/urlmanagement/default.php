<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
//var_dump($this->items);
?>
<section id="urlmanagement">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="cbreadcrumb">
                    <li><a href="<?php echo JURI::root(); ?>"><i class="fa fa-home"></i><?php echo JText::_('COM_STRAVEL_HOME_TEXT_BREADCRUMBS') ?></a></li>
                    <li><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=urlmanagement'); ?>"><?php echo JText::_('COM_STRAVEL_URLMANAGEMENT') ?></a></li>
                </ul>
            </div>        
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="urlmanagment table-responsive">
                    <div class="filterurl text-right">
                        <form role="form" name="filterform" action="<?php echo JRoute::_('index.php?option=com_stravel&view=urlmanagement'); ?>" method="post">
                            <input type="hidden" name="filterurl" value="0" />
                            <input type="hidden" name="filtertitle" value="0" />
                            <input type="hidden" name="filterkeyword" value="0" />
                            <input type="hidden" name="filterdesc" value="0" />
                            
                            <button type="submit" class="btn btn-xs btn-info">
                                <i class="fa fa-chain-broken"></i> All <span class="badge"><?php echo $this->totalUrl ?></span>
                            </button>
                            
                            <?php if(!empty($this->dupurl)) : ?>
                            <button type="submit" onclick="document.filterform.filterurl.value='1';" data-toggle="tooltip" data-title="Urls might have same content." href="#" class="btn btn-xs btn-danger">
                                <i class="fa fa-ban"></i> Duplicated Content <span class="badge"><?php echo $this->dupurl ?></span>
                            </button>
                            <?php endif; ?>
                            <?php if(!empty($this->duptitle)) : ?>
                            <button type="submit" onclick="document.filterform.filtertitle.value='1';" data-toggle="tooltip" data-title="Urls have same page title." href="#" class="btn btn-xs btn-warning">
                                <i class="fa fa-times-circle"></i> Duplicated Title <span class="badge"><?php echo $this->duptitle ?></span>
                            </button>
                            <?php endif; ?>
                            <?php if(!empty($this->dupkeyword)) : ?>
                            <button type="submit" onclick="document.filterform.filterkeyword.value='1';" data-toggle="tooltip" data-title="Urls have same page keywords." href="#" class="btn btn-xs btn-warning">
                                <i class="fa fa-exclamation-circle"></i> Duplicated Keywords <span class="badge"><?php echo $this->dupkeyword ?></span>
                            </button>
                            <?php endif; ?>
                            <?php if(!empty($this->dupdesc)) : ?>
                            <button type="submit" onclick="document.filterform.filterdesc.value='1';" data-toggle="tooltip" data-title="Urls have same page description." href="#" class="btn btn-xs btn-warning">
                                <i class="fa fa-exclamation-triangle"></i> Duplicated Description <span class="badge"><?php echo $this->dupdesc ?></span>
                            </button>
                            <?php endif; ?>
                        </form>                        
                    </div>
                    <table class="table table-hover table-bordered">
                        <form action="<?php echo JRoute::_('index.php?option=com_stravel&view=urlmanagement'); ?>" method="post" name="adminForm">
                            <colgroup>
                                <col span="1" style="width: 35%;">
                                <col span="1" style="width: 20%;">
                                <col span="1" style="width: 20%;">
                                <col span="1" style="width: 20%;">
                                <col span="1" style="width: 5%;">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th><?php echo JText::_('COM_STRAVEL_URLMANAGEMENT_PAGE_URL') ?></th>
                                    <th><?php echo JText::_('COM_STRAVEL_URLMANAGEMENT_PAGE_TITLE') ?></th>
                                    <th><?php echo JText::_('COM_STRAVEL_URLMANAGEMENT_PAGE_KEYWORD') ?></th>
                                    <th><?php echo JText::_('COM_STRAVEL_URLMANAGEMENT_PAGE_DESC') ?></th>
                                    <th><?php echo JText::_('COM_STRAVEL_URLMANAGEMENT_PAGE_ACTION') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php  
                                foreach($this->items as $item) {
                                    $edit_link = JRoute::_('index.php?option=com_stravel&view=urlmanagement&layout=edit&id='.$item->id);
                                    echo '<tr>';                                
                                    echo '<td><a target="_blank" href="'.$item->sef_url.'"><span class="tbltext">'.urldecode($item->sef_url).'</span></a></td>';
                                    echo '<td><span class="tbltext">'.$item->page_title.'</span></td>';
                                    echo '<td><span class="tbltext">'.$item->page_keyword.'</span></td>';
                                    echo '<td><span class="tbltext">'.$item->page_description.'</span></td>';
                                    echo '<td valign="middle" class="vcenter"><a href="'.$edit_link.'" class="btn btn-success btn-xs text-center"><i class="fa fa-pencil-square-o"></i> '.JText::_('COM_STRAVEL_URLMANAGEMENT_URL_EDIT').'</a></td>';
                                    echo '</tr>';
                                }
                            ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <?php echo $this->pagination->getListFooter(); ?>
                                    </td>
                                </tr>
                            </tfoot>                    
                        </form>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>