<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
$lang = str_replace('-','_',strtolower(JFactory::getLanguage()->getTag()));
$cityName = 'CityName_'.strtolower(str_replace('-','_', (string) $lang));
$presetHelper = new presetHelper();
if(!empty($this->package->CityList->City->CountryCode)) {
    $coutry = $presetHelper->getCoutryByCoutryCode((string) $this->package->CityList->City->CountryCode);
}
$chinese = true;
switch($lang) {
    case 'en_us':
        $chinese = false;
        break;
    case 'zh_tw':
        $chinese = true;
        break;
    case 'zh_cn':
        $chinese = true;
        break;
    default:
        $chinese = true;
        break;
}
?>
<div id="fb-root"></div> 
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/<?php echo str_replace('-','_',JFactory::getLanguage()->getTag()); ?>/sdk.js#xfbml=1&appId=194378270738290&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<section itemscope itemtype="http://schema.org/Product" id="tourflyer" class="<?php echo $lang; ?> productpage tourflyerpage">
    <!-- Structure data -->
    <span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" class="hide">
        <span itemprop="ratingValue"><?php echo round((rand(45,50)/10),1) ?></span>
        <span itemprop="reviewCount"><?php echo rand(333,888); ?></span>
    </span>
    <span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="hide">
        <span itemprop="priceCurrency"><?php echo empty($this->package->Currency)?'HKD':$this->package->Currency ?></span>
        <span itemprop="price"><?php echo empty($this->package->Price)?'300':(int)$this->package->Price ?></span>
    </span>
    <!-- Structure data end -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="cbreadcrumb">
                    <li><a href="<?php echo JURI::root(); ?>"><i class="fa fa-home"></i><?php echo JText::_('COM_STRAVEL_HOME_TEXT_BREADCRUMBS') ?></a></li>
                    <li><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=tourhome'); ?>"><?php echo JText::_('COM_STRAVEL_PACKAGE_TOUR') ?></a></li>
                    <?php 
                        if(!empty($this->package->Abstract)) {
                            echo '<a href="#" onclick="return false;">'.$this->package->Abstract.'</a>';
                        } 
                    ?>
                </ul>
            </div>        
        </div>
        <div class="row">         
            <!-- Start right pane -->
            <div class="col-md-12">
                <div class="right-pane">
                    <div class="basicsort hide">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="row">
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_SORTBY') ?></p></div>
                                            <div class="col-md-3">
                                                <div class="dropdown">
                                                    <span data-toggle="dropdown" data-target="#" class="fakeselectgray form-control"><?php echo JText::_('COM_STRAVEL_PRICE_LOW_TO_HIGH') ?></span>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                                        <li><a data-direction="1" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_PRICE_LOW_TO_HIGH') ?></a></li>
                                                        <li><a data-direction="2" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_PRICE_HIGH_TO_LOW') ?></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="dropdown">
                                                    <span data-toggle="dropdown" data-target="#" class="fakeselectgray form-control">主題精選</span>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                                        <?php 
                                                            foreach($this->featuredPackage as $cat) {
                                                                //add language filter 
                                                                if((string) $cat->Lang == (string) $lang){
                                                                    $urlname = apiHelper::convertUrl($cat->Name);
                                                                    echo '<li><a href="'.JRoute::_('index.php?option=com_stravel&view=tourhome&ideaid='.$cat->Id.'&ideaname='.$urlname).'">'.$cat->Name.'</a></li>';
                                                                }
                                                            }
                                                        ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                    <!-- end basic sort -->
                    <!-- Filter -->
                    <div class="filter">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-11">
                                        <!-- Featured Package -->
                                        <?php if(!empty($this->featuredPackage)) :?>
                                        <div class="row">                                            
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_CATEGORY_FILTER') ?></p></div>
                                            <div class="col-md-10">
                                                <div class="linklist">
                                                <a href="<?php echo JRoute::_('index.php?option=com_stravel&view=tourhome') ?>"><?php echo JText::_('STRAVEL_SHOW_ALL') ?></a>
                                                <?php 
                                                    foreach($this->featuredPackage as $cat) {
                                                         //add language filter 
                                                        if((string) $cat->Lang == (string) $lang){
                                                            $urlname = apiHelper::convertUrl($cat->Name);
                                                            echo '<a href="'.JRoute::_('index.php?option=com_stravel&view=tourhome&ideaid='.$cat->Id.'&ideaname='.$urlname).'">'.$cat->Name.'</a>';
                                                        }
                                                    }
                                                ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <!-- End featured Package -->
                                        
                                        <!-- Hot cities -->
                                        <?php if(!empty($this->hotCities)) :?>
                                        <div class="row">
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_POPULAR_CITY_FILTER') ?></p></div>
                                            <div class="col-md-10">
                                                <div class="linklist">
                                                <a href="<?php echo JRoute::_('index.php?option=com_stravel&view=tourhome') ?>"><?php echo JText::_('STRAVEL_SHOW_ALL') ?></a>
                                                <?php
                                                    $i = 1;
                                                    //$cityName = 'CityName_'.strtolower(str_replace('-','_', (string) $lang)); 
                                                    foreach($this->hotCities as $city) {
                                                        if($i>10) {break;} $i++;
                                                        $urlname = apiHelper::convertUrl($city->CityName);
                                                        echo '<a href="'.JRoute::_('index.php?option=com_stravel&view=tourhome&city='.$city->CityCode.'&cityname='.$urlname).'">'.$city->CityName.'</a>';
                                                        //$infos = explode("\n",$city->Content);
                                                        //$urlname = apiHelper::convertUrl($infos[0]);
                                                        //echo '<a href="'.JRoute::_('index.php?option=com_stravel&view=packagehome&city='.$city->CityCode.'&cityname='.$urlname).'">'.$city->$cityName.'</a>';
                                                        //echo '<a href="'.JRoute::_('index.php?option=com_stravel&view=tourhome&city='.$city->Keyword.'&cityname='.$urlname).'">'.$infos[0].'</a>';
                                                    }
                                                ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <!-- End hot cities -->
                                    </div>
                                </div>
                            </div>                            
                        </div> 
                    </div>
                    <!-- End fitler -->
                    
                    <!-- Item List -->
                    <div class="itemlist">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row">
                                    <!-- Product content -->
                                    <div class="packageFlyerInfo">
                                        <div class="col-md-5">
                                            <div class="packageInfoLeft">
                                                <p><span class="infoLabel"><?php echo JText::_('COM_STRAVEL_TOUR_NAME') ?></span><span class="sep">:</span><span itemprop="name" class="infoValue"><?php if(!empty($this->package->Abstract)) echo $this->package->Abstract; ?></span></p>
                                                <p><span class="infoLabel"><?php echo JText::_('COM_STRAVEL_CITY') ?></span><span class="sep">:</span><span class="infoValue"><?php if(!empty($this->package->CityList->City)) {$citystring = array(); foreach($this->package->CityList->City as $city) { $citystring[]= $city->CityName; } echo implode(', ',$citystring);} ?></span></p>
                                                <p><span class="infoLabel"><?php echo JText::_('COM_STRAVEL_AIRLINE') ?></span><span class="sep">:</span><span class="infoValue"><?php if(!empty($this->package->CarrierList->Carrier)) {$carrierstring = array(); foreach($this->package->CarrierList->Carrier as $carrier) { $carrierstring[]= $carrier->CarrierName; } echo implode(', ',$carrierstring);} ?></span></p>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="packageInfoRight">
                                                <p><span class="infoLabel"><?php echo JText::_('COM_STRAVEL_DAY_NIGHT') ?></span><span class="sep">:</span><span class="infoValue"><?php echo sprintf(JText::_('COM_STRAVEL_PACKAGE_DAYNIGHT_VARRIBLE'),$this->package->Days,$this->package->Nights); ?></span></p>
                                                <p><span class="infoLabel"><?php echo JText::_('COM_STRAVEL_DEPARTURE_DATE') ?></span><span class="sep">:</span><span class="infoValue"><?php if(!empty($this->package->TravelPeriodFr)) echo array_shift(explode('T',$this->package->TravelPeriodFr)); if(!empty($this->package->TravelPeriodTo)) echo JText::_('COM_STRAVEL_TO'). array_shift(explode('T',$this->package->TravelPeriodTo)); ?></span></p>
                                                <p><span class="infoLabel"><?php echo JText::_('COM_STRAVEL_PRICE') ?></span><span class="sep">:</span><span class="infoValue"><?php if(!empty($this->package->Price)) {echo '$'.$this->package->Price;if($this->package->PriceInd=='Fr') echo '+'; } ?></span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="packageFlyerImage">
                                        <div class="col-md-8">
                                            <p class="greenclr"><?php echo JText::_('COM_STRAVEL_PACKAGE_NAME2') ?></p>
                                            <p><?php if(!empty($this->package->Abstract)) echo $this->package->Abstract; ?></p>
                                        </div>
                                        <div class="col-md-4 text-right">
                                            <div class="shareProduct">
                                                <div class="fb-like" data-href="<?php echo JFactory::getURI()->toString(); ?>" data-width="20" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div>
                                                <a href="<?php if(!empty($this->package->AttachmentList->Attachment)) { foreach($this->package->AttachmentList->Attachment as $attach) {  if($attach->Type=='Flyer') echo $attach->Url; } } ?>" target="_blank" title="Download this Package Flyer"><img src="images/PDF.png" width="19" height="19" alt="Download this Package" /></a>
                                                <a href="#"
                                                    data-frompage="package" 
                                                    data-productname="<?php echo sprintf(JText::_('COM_STRAVEL_PACKAGE_NAME_VARRIBLE'),$this->package->Title,$this->package->Days,$this->package->Nights); ?>" 
                                                    data-productprice="$<?php echo $this->package->Price; if($package->PriceInd=='Fr') echo '+'; ?>" 
                                                    data-productid="<?php echo $this->package->Id ?>"
                                                    data-productdate=""
                                                    data-productnights="<?php echo sprintf(JText::_('COM_STRAVEL_PACKAGE_DAYNIGHT_VARRIBLE'),$this->package->Days,$this->package->Nights); ?>" 
                                                    data-toggle="modal" 
                                                    data-target="#enquiryModal" title="Enquiry this Package Flyer"><img src="images/email.png" width="18" height="13" alt="Enquiry this Package" /></a>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="packageFlyerImageList">
                                                <?php 
                                                    if(!empty($this->package->AttachmentList->Attachment)) { 
                                                        foreach($this->package->AttachmentList->Attachment as $attach) {  
                                                            if($attach->Type=='Image') echo '<div class="packageFlyerImageItem tourpackage"><img itemprop="image" class="img-responsive" src="'.$attach->Url.'" alt="'.$this->package->Title.'"/></div>'; 
                                                        }
                                                    } 
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!-- Related package -->
                                    <?php if(!empty($this->relatedPackage)) : ?>
                                    <div class="packageRelated">
                                        <div class="col-md-12">
                                            <h5><?php echo JText::_('COM_STRAVEL_RELATED_PACKAGE'); ?></h5>
                                            <div class="row">
                                                <div class="relatedheader">
                                                    <div class="col-xs-3"><?php echo JText::_('COM_STRAVEL_RELATED_PACKAGE_NAME') ?></div>
                                                    <div class="col-xs-1"><?php echo JText::_('COM_STRAVEL_RELATED_PACKAGE_NIGHT') ?></div>
                                                    <div class="col-xs-2"><?php echo JText::_('COM_STRAVEL_AIRLINE') ?></div>
                                                    <div class="col-xs-2"><?php echo JText::_('COM_STRAVEL_AVG_PRICE') ?></div>
                                                    <div class="col-xs-4"><?php echo JText::_('COM_STRAVEL_DEPARTURE_DATE') ?></div>
                                                </div>
                                            </div>
                                            <?php $i=1; foreach($this->relatedPackage as $item) : ?>
                                                <?php 
                                                    $urlname=apiHelper::convertUrl($item->Title);
                                                    if((string) $item->Id !== (string) $this->package->Id) :
                                                        $i++;
                                                ?>
                                                <div class="row">
                                                    <div class="relateditem">
                                                        <div class="col-xs-3"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packageflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php echo $item->Title; ?></a></div>
                                                        <div class="col-xs-1"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packageflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php echo sprintf(JText::_('COM_STRAVEL_PACKAGE_DAYNIGHT_VARRIBLE'),$item->Days,$item->Nights); ?></a></div>
                                                        <div class="col-xs-2"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packageflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php if(!empty($item->CarrierList->Carrier)) {$carrierstring = array(); foreach($item->CarrierList->Carrier as $carrier) { $carrierstring[]= $carrier->CarrierName; } echo implode(', ',$carrierstring);} ?></a></div>
                                                        <div class="col-xs-2"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packageflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php echo '$'.$item->Price;if($item->PriceInd=='Fr') echo '+'; ?></a></div>
                                                        <div class="col-xs-4"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packageflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php if(!empty($item->TravelPeriodFr)) echo array_shift(explode('T',$item->TravelPeriodFr)); if(!empty($item->TravelPeriodTo)) echo JText::_('COM_STRAVEL_TO'). array_shift(explode('T',$item->TravelPeriodTo)); ?></a></div>
                                                    </div>
                                                </div>
                                            <?php 
                                                endif;
                                                if($i>3) {
                                                    break;
                                                }
                                                endforeach; 
                                            ?>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <!-- End related package -->
                                    
                                    <!-- Other package -->
                                    <?php if(!empty($this->otherPackage)) : ?>
                                    <div class="packageRelated otherPackage">
                                        <div class="col-md-12">
                                            <h5><?php echo JText::_('COM_STRAVEL_OTHER_PACKAGE'); ?></h5>
                                            <div class="row">
                                                <div class="relatedheader">
                                                    <div class="col-xs-3"><?php echo JText::_('COM_STRAVEL_RELATED_PACKAGE_NAME') ?></div>
                                                    <div class="col-xs-1"><?php echo JText::_('COM_STRAVEL_RELATED_PACKAGE_NIGHT') ?></div>
                                                    <div class="col-xs-2"><?php echo JText::_('COM_STRAVEL_AIRLINE') ?></div>
                                                    <div class="col-xs-2"><?php echo JText::_('COM_STRAVEL_AVG_PRICE') ?></div>
                                                    <div class="col-xs-4"><?php echo JText::_('COM_STRAVEL_DEPARTURE_DATE') ?></div>
                                                </div>
                                            </div>
                                            <?php $i=1; foreach($this->otherPackage as $item) : ?>
                                                <?php 
                                                    $urlname=apiHelper::convertUrl($item->Title);
                                                    if((string)$item->Id !== (string)$this->package->Id) :
                                                        $i++;
                                                ?>
                                                <div class="row">
                                                    <div class="relateditem">
                                                        <div class="col-xs-3"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packageflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php echo $item->Title; ?></a></div>
                                                        <div class="col-xs-1"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packageflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php echo sprintf(JText::_('COM_STRAVEL_PACKAGE_DAYNIGHT_VARRIBLE'),$item->Days,$item->Nights); ?></a></div>
                                                        <div class="col-xs-2"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packageflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php if(!empty($item->CarrierList->Carrier)) {$carrierstring = array(); foreach($item->CarrierList->Carrier as $carrier) { $carrierstring[]= $carrier->CarrierName; } echo implode(', ',$carrierstring);} ?></a></div>
                                                        <div class="col-xs-2"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packageflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php echo '$'.$item->Price;if($item->PriceInd=='Fr') echo '+'; ?></a></div>
                                                        <div class="col-xs-4"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packageflyer&id='.$item->Id.'&name='.$urlname) ?>"><?php if(!empty($item->TravelPeriodFr)) echo array_shift(explode('T',$item->TravelPeriodFr)); if(!empty($item->TravelPeriodTo)) echo JText::_('COM_STRAVEL_TO').array_shift(explode('T',$item->TravelPeriodTo)); ?></a></div>
                                                    </div>
                                                </div>
                                            <?php 
                                                endif;
                                                if($i>3) {
                                                    break;
                                                }
                                                endforeach; 
                                            ?>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <!-- End other package -->
                                    
                                    <!-- End product content -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Item List -->    
                </div>
            </div>
            <!-- End right pane -->
        </div>
    </div>
</section>