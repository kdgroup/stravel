<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
$lang = JFactory::getLanguage()->getTag();
$presetHelper = new presetHelper();
?>
<section id="airtickethome" class="<?php echo $lang; ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="cbreadcrumb">
                    <li><a href="<?php echo JURI::root(); ?>"><i class="fa fa-home"></i><?php echo JText::_('COM_STRAVEL_HOME_TEXT_BREADCRUMBS') ?></a></li>
                    <li><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=airtickethome'); ?>"><?php echo JText::_('COM_STRAVEL_SEARCH_AIRTICKET') ?></a></li>
                </ul>
            </div>        
        </div>
        <div class="row">
            <!-- Start left pane -->
            <div class="col-md-3">
                <div class="left-pane equalheight">
                    <div class="panetitle">
                        <h3><?php echo JText::_('COM_STRAVEL_AIRTICKETHOME') ?></h3>
                    </div>
                    
                    <?php echo $presetHelper->getSearchPanel(2);  ?>
                    
                    <div class="topten">
                        <h3><?php echo JText::_('COM_STRAVEL_TOPTEN_AIRTICKET') ?></h3>
                        <div class="topten-info">
                            <?php if(!empty($this->top10airmanual)) : 
                               $i=1; foreach($this->top10airmanual as $item) :
                               $info = explode("\n",$item->Content);
                               $urlname = apiHelper::convertUrl($info[0]); 
                            ?>
                                <div class="topten-item">
                                    <div class="pull-left name"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=airsearchresult&citycode='.$item->Keyword.'&cityname='.$urlname) ?>"><?php echo empty($item->DstName)?$item->Dst:$item->DstName; ?></a></div>
                                    <div class="pull-left time"><?php echo $info[1] ?></div>
                                    <div class="pull-left price text-right">$<?php echo $info[2] ?></div>
                                </div>    
                            <?php if($i>9) {break;}else {$i++;} endforeach; else : ?>
                            <?php 
                                $j=0; 
                                foreach($this->airlist as $item) : 
                                    $j++; 
                                    if($j>10) {break;}
                                    $urlname = apiHelper::convertUrl($item->DstName); 
                            ?>
                            <div class="topten-item">
                                <div class="pull-left name"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=airsearchresult&citycode='.$item->Dst.'&cityname='.$urlname) ?>"><?php echo empty($item->DstName)?$item->Dst:$item->DstName; ?></a></div>
                                <div class="pull-left time"><?php echo $item->AirlineName ?></div>
                                <div class="pull-left price text-right">$<?php echo (int) $item->Adult; ?>+</div>
                            </div>
                            <?php endforeach;endif; ?>
                        </div>
                    </div>
                </div>                
            </div>
            <!-- End left pane -->
            
            <!-- Start right pane -->
            <div class="col-md-9">
                <div class="right-pane equalheight">
                    <!-- basic sort -->
                    <div class="basicsort">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="row">
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_SORTBY') ?></p></div>
                                            <div class="col-md-3">
                                                <div class="dropdown">
                                                    <span data-direction="1" data-toggle="dropdown" data-target="#" class="fakesortvalue fakeselectgray form-control"><?php echo JText::_('COM_STRAVEL_PRICE_LOW_TO_HIGH') ?></span>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                                        <li><a data-direction="1" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_PRICE_LOW_TO_HIGH') ?></a></li>
                                                        <li><a data-direction="2" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_PRICE_HIGH_TO_LOW') ?></a></li>
                                                        <li><a data-direction="3" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_LASTEST_OFFERS') ?></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                    <!-- end basic sort -->
                    <!-- Filter -->
                    <div class="filter">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="row">
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_POPULAR_CITY_FILTER') ?></p></div>
                                            <div class="col-md-10">
                                                <div class="linklist aircitylist">
                                                    <?php 
                                                        foreach($this->hottestcity as $city) :
                                                        $infos = explode("\n",$city->Content);
                                                        $urlname = apiHelper::convertUrl($infos[0]); 
                                                    ?>
                                                        <a href="<?php echo JRoute::_('index.php?option=com_stravel&view=airsearchresult&citycode='.$city->Keyword.'&cityname='.$urlname) ?>"><?php echo $infos[0] ?></a>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div> 
                    </div>
                    <!-- End fitler -->
                    
                    <!-- Item List -->
                    <div class="itemlist">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1 listcontainer">
                                    <?php
                                        $i=0;$j=1;
                                        if(!empty($this->airlist)) :
                                        foreach($this->airlist as $item) :
                                        if($i%12==0) {
                                            if($i>0) {
                                                break;
                                            }
                                            //echo '<div class="page-sep page-sep'.$j.'">';
                                            $j++;
                                        } 
                                        if($i%3==0) {
                                            if($i>=9) {
                                                echo '<div class="row noborder">';
                                            }else{
                                                echo '<div class="row vloz'.$i.'">';    
                                            }
                                        }
                                        switch(strtoupper($item->FareClass)) {
                                            case 'Y':
                                                $pclass = '經濟艙';
                                                break;
                                            case 'F':
                                                $pclass = '頭等艙';
                                                break;
                                            case 'C':
                                                $pclass = '商務艙';
                                                break;
                                            default:
                                                $pclass = '經濟艙';
                                                break;
                                        }
                                        switch($item->TripType) {
                                            case 1:
                                                $ptrip = '單程';
                                                break;
                                            case 2:
                                                $ptrip = '往返';
                                                break;
                                            default:
                                                $ptrip = '往返';
                                                break;
                                        }
                                    ?>
                                        <div data-dateeffect="<?php echo $item->EffectiveFrom; ?>" data-effectfrom=<?php echo strtotime($item->EffectiveFrom); ?>" data-price="<?php echo (int)$item->Adult; ?>" class="product-container col-md-4">
                                        <?php //var_dump(strtotime($item->EffectiveFrom)); ?>
                                            <div class="airticket-item product-item">
                                                <div class="airticket-image product-image text-center">
                                                    <img class="img-responsive lazy"   data-original="http://www.adholidays.com/adh/cms/Carrier/<?php echo $item->Airline ?>/1.jpg" alt="package img" />
                                                    <div class="caption">
                                                        <h6 class="pull-left"><img src="<?php echo 'http://www.westminstertravel.com/hk/Airfare/al/'.$item->Airline.'.png'; ?>" alt="logo cx"/><?php echo $item->AirlineName; ?></h6>
                                                        <span data-productid="<?php echo $item->DetailID; ?>" data-target="#myLargeAirTicketModal" data-toggle="modal" class="pull-right"><img src="images/GreyMagnifierIcon.png" alt="find"/></span>
                                                    </div>
                                                </div>
                                                <div class="airticket-info product-info">
                                                    <h6><a href="<?php $urlname = apiHelper::convertUrl($item->DstName);echo JRoute::_('index.php?option=com_stravel&view=airsearchresult&citycode='.$item->Dst.'&cityname='.$urlname); ?>"><?php echo $item->DstName; ?></a></h6>
                                                    <div class="airticket-bottom-info">
                                                        <div class="airticket-price"><span>$<?php echo (int)$item->Adult; ?>+</span></div>
                                                        <div class="airticket-link text-right">
                                                            <a href="#enquiryModal" 
                                                                data-frompage="air"
                                                                data-productid="<?php echo $item->DetailID; ?>" 
                                                                data-productname="<?php echo $item->DstName .' - '. $item->AirlineName ?>" 
                                                                data-productprice="$<?php echo (int)$item->Adult; ?>" 
                                                                data-productclass="<?php echo $pclass; ?>"
                                                                data-productdate=""
                                                                data-producttrip="<?php echo $ptrip; ?>"
                                                                data-toggle="modal" data-target="#enquiryModal">&nbsp;</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                        
                                        if($i%3==2 || ($i+1)>=count($this->airlist)) {echo '</div>';}
                                        //if($i%12==11 || $i==count($this->airlist)) {echo '</div>';}
                                        $i++; 
                                        endforeach;endif; 
                                    ?>
                            </div>
                        </div>
                        <!-- Disclaimer -->
                        <?php if(!empty($this->disclaimer)) : ?>
                            <div class="row">
                                <div class="col-md-11 col-md-offset-1">
                                    <div class="row disclaimer noborder"><div class="col-md-12"><?php echo str_replace("\n",'<br />',$this->disclaimer->Content) ?></div></div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <!-- Disclaimer -->
                    </div>
                    <!-- End Item List -->    
                </div>
            </div>
            <!-- End right pane -->
        </div>
    </div>
</section>