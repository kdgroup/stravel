<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
$lang = JFactory::getLanguage()->getTag();
//var_dump($this->airresultlist);
?>
<section id="airtickethome" class="<?php echo $lang; ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="cbreadcrumb">
                    <li><a href="<?php echo JURI::root(); ?>"><i class="fa fa-home"></i><?php echo JText::_('COM_STRAVEL_HOME_TEXT_BREADCRUMBS') ?></a></li>
                    <li><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=airtickethome'); ?>"><?php echo JText::_('COM_STRAVEL_SEARCH_AIRTICKET') ?></a></li>
                </ul>
            </div>        
        </div>
        <div class="row">
            <!-- Start left pane -->
            <div class="col-md-3">
                <div class="left-pane">
                    <div class="panetitle twoline">
                        <h3>
                            <?php 
                                if(!empty($this->cityName)) echo sprintf(JText::_('COM_STRAVEL_AIRSEARCHRESULT_TITLE_VARRIBLE'),$this->cityName,$this->lowestPrice);
                            ?>
                        </h3>
                    </div>
                    
                    <?php
                        $presetHelper = new presetHelper(); 
                        echo $presetHelper->getSearchPanel(2);
                    ?>
                    
                    <!-- Filter on left -->
                    <div class="filterleft">
                        <div class="selectroute viaselect">
                            <h4><?php echo JText::_('COM_STRAVEL_STOP_OVER') ?></h4>
                            <div class="route-item"><input type="checkbox" id="directflight" /><label for="directflight"><?php echo JText::_('COM_STRAVEL_DIRECT_FLIGHT') ?></label></div>
                            <div class="route-item"><input type="checkbox" id="onestop" /><label for="onestop"><?php echo JText::_('COM_STRAVEL_1STOP') ?></label></div>
                        </div>
                        <div class="selectroute airlineselect">
                            <h4><?php echo JText::_('COM_STRAVEL_AIRLINE') ?></h4>
                            <?php foreach($this->airCo as $item) : ?>
                                <div class="route-item"><input type="checkbox" data-id="<?php echo $item->airline_code; ?>" id="airline-<?php echo $item->airline_code; ?>" /><label for="airline-<?php echo $item->airline_code; ?>"><?php if($this->presethelper->_lang=='en-US') { echo $item->airline_english; } else { echo $item->airline_chinese; } ?></label></div>
                            <?php endforeach; ?>
                        </div>
                        <div class="selectroute">
                            <div class="dragprice companyselect">
                                <h4><?php echo JText::_('COM_STRAVEL_PRICE_DRAG') ?></h4>
                                <p class="text-center">
                                    <?php echo JText::_('COM_STRAVEL_HK') ?>
                                    <span id="pricefrom">$500</span>
                                    <?php echo JText::_('COM_STRAVEL_TO') ?>
                                    <span id="priceto">$19999</span>
                                </p>
                                <input id="greendragprice" type="slider" name="price" value="500;19999" />
                            </div>
                        </div>
                    </div>
                    <!-- End filter --> 
                    
                </div>                
            </div>
            <!-- End left pane -->
            
            <!-- Start right pane -->
            <div class="col-md-9">
                <div class="right-pane">
                    <!-- basic sort -->
                    <div class="basicsort">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="row">
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_SORTBY') ?></p></div>
                                            <div class="col-md-3">
                                                <div class="dropdown">
                                                    <span data-direction="1" data-toggle="dropdown" data-target="#" class="fakesortvalue fakeselectgray form-control"><?php echo JText::_('COM_STRAVEL_PRICE_LOW_TO_HIGH') ?></span>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                                        <li><a data-direction="1" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_PRICE_LOW_TO_HIGH') ?></a></li>
                                                        <li><a data-direction="2" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_PRICE_HIGH_TO_LOW') ?></a></li>
                                                        <li><a data-direction="3" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_LASTEST_OFFERS') ?></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                    <!-- end basic sort -->
                    <!-- Filter -->
                    <div class="filter">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="row">
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_POPULAR_CITY_FILTER') ?></p></div>
                                            <div class="col-md-10">
                                                <div class="linklist aircitylist">
                                                    <?php 
                                                        foreach($this->hottestcity as $city) :
                                                        $infos = explode("\n",$city->Content);
                                                        $urlname = apiHelper::convertUrl($infos[0]); 
                                                    ?>
                                                        <a href="<?php echo JRoute::_('index.php?option=com_stravel&view=airsearchresult&citycode='.$city->Keyword.'&cityname='.$urlname) ?>"><?php echo $infos[0] ?></a>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div> 
                    </div>
                    <!-- End fitler -->
                    
                    <!-- Item List -->
                    <div class="itemlist">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1 listcontainer">
                                    <?php
                                        $i=0;$j=1;
                                        if(!empty($this->airresultlist)) :
                                        foreach($this->airresultlist as $item) :
                                        if($i%12==0) {
                                            echo '<div class="page-sep page-sep'.$j.'">';
                                            $j++;
                                        } 
                                        if($i%3==0) {echo '<div class="row">';}
                                        if($this->presethelper->_lang=='en-US') {
                                            $item->AirlineName=$this->presethelper->getAirlineByCode((string)$item->airline_code)->airline_english;
                                        }else{
                                            $item->AirlineName=$this->presethelper->getAirlineByCode((string)$item->airline_code)->airline_chinese;    
                                        }
                                        switch(strtoupper($item->fare_class)) {
                                            case 'Y':
                                                $pclass = '經濟艙';
                                                break;
                                            case 'F':
                                                $pclass = '頭等艙';
                                                break;
                                            case 'C':
                                                $pclass = '商務艙';
                                                break;
                                            default:
                                                $pclass = '經濟艙';
                                                break;
                                        }
                                        switch(JRequest::getVar('triptype',2)) {
                                            case 1:
                                                $ptrip = '單程';
                                                break;
                                            case 2:
                                                $ptrip = '往返';
                                                break;
                                            default:
                                                $ptrip = '往返';
                                                break;
                                        }
                                    ?>
                                        <div data-dateeffect="<?php echo $item->begin_date; ?>" data-effectfrom="<?php echo strtotime($item->begin_date); ?>" data-airline="<?php echo $item->airline_code ?>" data-stop="<?php echo empty($item->via_points)?'directflight':'onestop'; ?>" data-price="<?php echo (int)$item->PriceList->FarePrice->adult; ?>" class="product-container col-md-4">
                                        <?php //var_dump($item); ?>
                                            <div class="airticket-item product-item">
                                                <div class="airticket-image product-image text-center">
                                                    <img data-remark="<?php echo htmlentities($item->fare_term_summary); ?>" data-productid="<?php echo $item->fare_id; ?>" data-airline="<?php echo $item->airline_code ?>" data-target="#myLargeAirTicketModal" data-toggle="modal" class="img-responsive lazy" data-original="http://www.adholidays.com/adh/cms/Carrier/<?php echo $item->airline_code ?>/1.jpg" alt="package img" />
                                                    <div class="caption">
                                                        <h6 class="pull-left"><img src="<?php echo 'http://www.westminstertravel.com/hk/Airfare/al/'.$item->airline_code.'.png'; ?>" alt="logo"/><?php echo $item->AirlineName; ?></h6>
                                                        <span data-remark="<?php echo htmlentities($item->fare_term_summary); ?>" data-productid="<?php echo $item->fare_id; ?>" data-airline="<?php echo $item->airline_code ?>" data-target="#myLargeAirTicketModal" data-toggle="modal" class="pull-right"><img src="images/GreyMagnifierIcon.png" alt="find"/></span>
                                                    </div>
                                                </div>
                                                <div class="airticket-info product-info">
                                                    <h6>
                                                        <?php 
                                                            $citynametmp = JRequest::getVar('cityname');
                                                            if(!empty($citynametmp)) {
                                                                echo $citynametmp;
                                                            }else {
                                                                echo JText::_('COM_STRAVEL_FROM_AVG');
                                                            } 
                                                        ?>
                                                    </h6>
                                                    <div class="airticket-bottom-info">
                                                        <div class="airticket-price"><span>$<?php echo (int)$item->PriceList->FarePrice->adult; ?>+</span></div>
                                                        <div class="airticket-link text-right">
                                                            <a href="#enquiryModal"
                                                                data-frompage="air" 
                                                                data-productid="<?php echo $item->fare_id; ?>" 
                                                                data-productname="<?php if(!empty($this->cityName)) echo $this->cityName.' - '; echo $item->AirlineName ?>" 
                                                                data-productprice="$<?php echo (int)$item->PriceList->FarePrice->adult; ?>" 
                                                                data-productclass="<?php echo $pclass; ?>"
                                                                data-productdate="<?php echo $this->departuredate; ?>"
                                                                data-producttrip="<?php echo $ptrip; ?>"
                                                                data-toggle="modal" data-target="#enquiryModal">&nbsp;</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                         
                                        if($i%3==2 || ($i+1)>=count($this->airresultlist)) {echo '</div>';}
                                        if($i%12==11 || ($i+1)>=count($this->airresultlist)) {echo '</div>';}
                                        $i++;
                                        endforeach;endif; 
                                    ?>
                            </div>
                        </div>
                        
                        <!-- Disclaimer & pagination -->                        
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row disclaimer noborder">
                                    <?php if(!empty($this->disclaimer)) : ?>
                                    <div class="col-md-7"><?php echo str_replace("\n",'<br />',$this->disclaimer->Content) ?></div>
                                    <?php endif; ?>
                                    <?php if($i>12) : ?>
                                    <div class="col-md-5 text-right">
                                        <ul class="pagination">                                  
                                          <?php 
                                            $n = $i%12;
                                            if($n==0) {
                                                $n = $i/12;
                                            }else{
                                                $n = floor($i/12)+1;
                                            }
                                            echo '<li data-totalpage="'.$n.'" data-currentpage="1" data-page="prev" class="first disabled"><a href="#">&laquo;</a></li>'; 
                                            for($i=1;$i<=$n;$i++) {
                                                if($i==1) {
                                                    $cls = "disabled";
                                                }else{
                                                    $cls = '';
                                                }
                                                if($i>=1 && $i<=5) {
                                                    $cls .= " showed";
                                                }else{
                                                    $cls .= '';
                                                }
                                                echo '<li data-totalpage="'.$n.'" data-currentpage="1" class="pagsseitem pagsseitem'.$i.' '.$cls.'" data-page="'.$i.'"><a href="#">'.$i.'</a></li>';
                                            }
                                            echo '<li data-totalpage="'.$n.'" data-currentpage="1" data-page="next" class="last"><a href="#">&raquo;</a></li>'; 
                                          ?>
                                        </ul>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>                        
                        <!-- Disclaimer -->
                        <!-- Top 20 air -->
                        <?php if(!empty($this->top20air)) : ?>
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1 noborder">
                                <div class="row fktbl">
                                    <div class="col-md-6">
                                        <div class="row headtbl">
                                            <div class="col-xs-4"><?php echo JText::_('COM_STRAVEL_AIRLINE') ?></div>
                                            <div class="col-xs-4"><?php echo JText::_('COM_STRAVEL_LOCATION_ROUNDTRIP') ?></div>
                                            <div class="col-xs-2"><?php echo JText::_('COM_STRAVEL_TICKET_STARTING') ?></div>
                                            <div class="col-xs-1"><?php echo JText::_('COM_STRAVEL_TERMS') ?></div>
                                            <div class="col-xs-1"><?php echo JText::_('COM_STRAVEL_QUERY') ?></div>
                                        </div>    
                                    </div>
                                    <div class="col-md-6 hidden-xs hidden-sm">
                                        <div class="row headtbl">
                                            <div class="col-xs-4"><?php echo JText::_('COM_STRAVEL_AIRLINE') ?></div>
                                            <div class="col-xs-4"><?php echo JText::_('COM_STRAVEL_LOCATION_ROUNDTRIP') ?></div>
                                            <div class="col-xs-2"><?php echo JText::_('COM_STRAVEL_TICKET_STARTING') ?></div>
                                            <div class="col-xs-1"><?php echo JText::_('COM_STRAVEL_TERMS') ?></div>
                                            <div class="col-xs-1"><?php echo JText::_('COM_STRAVEL_QUERY') ?></div>
                                        </div>    
                                    </div>
                                </div>
                                <div class="row">
                                    <?php
                                        $i=0; 
                                        foreach($this->top20air as $item):
                                        $i++;
                                        if($i%21==0) {break;}
                                        if(($i-1)%10==0) {
                                            echo '<div class="col-md-6">';
                                        } 
                                    ?>
                                        <div class="row">
                                            <div class="col-xs-4"><a href="<?php $urlname = apiHelper::convertUrl($item->AirlineName);echo JRoute::_('index.php?option=com_stravel&view=airsearchresult&airlinecode='.$item->Airline.'&airlinename='.$urlname); ?>"><?php echo $item->AirlineName; ?></a></div>
                                            <div class="col-xs-4"><a href="<?php $urlname = apiHelper::convertUrl($item->DstName);echo JRoute::_('index.php?option=com_stravel&view=airsearchresult&citycode='.$item->Dst.'&cityname='.$urlname); ?>"><?php echo $item->DstName; ?></a></div>
                                            <div class="col-xs-2">$<?php echo (int)$item->Adult ?>+</div>
                                            <div class="col-xs-1 text-center"><span data-productid="<?php echo $item->DetailID; ?>" data-airline="<?php echo $item->Airline ?>" data-target="#myLargeAirTicketModal" data-toggle="modal" class="text-center"><img width="14" src="images/GreyMagnifierIcon.png" alt="find"/></span></div>
                                            <div class="col-xs-1 text-center"><a data-productid="<?php echo $item->DetailID; ?>" data-productname="<?php echo $item->DstName .' - '. $item->AirlineName ?>" data-productprice="$<?php echo (int)$item->Adult; ?>" href="#enquiryModal" data-toggle="modal" data-target="#enquiryModal"><img width="14" src="/images/GreyEnvelop.png" alt="download" /></a></div>
                                        </div>
                                    <?php
                                        if(($i-1)%10==9 || $i==count($this->top20air)) {echo '</div>';} 
                                        endforeach; 
                                    ?>
                                </div>        
                            </div>
                        </div>
                        <?php endif; ?>
                        <!-- top20air -->
                    </div>
                    <!-- End Item List -->    
                </div>
            </div>
            <!-- End right pane -->
        </div>
    </div>
</section>