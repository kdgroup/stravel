<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
$lang = str_replace('-','_',strtolower(JFactory::getLanguage()->getTag()));
$doc = JFactory::getDocument();
$doc->addScriptDeclaration("
    jQuery(document).ready(function() {
        jQuery('#stravel-slideshow').carousel({interval:5000});
    });    
");
$citycode = JRequest::getVar('city');
$ideaid = JRequest::getVar('ideaid');
?>
<script type="text/javascript" src="<?php  echo JUri::root() ?>templates/stravel/js/mansonry.js"></script>
<script type="text/javascript">
    var items = [];
    <?php
        $total_res = 0;
        foreach($this->packageList as $package) :
        $shouldAdd = false;
        if(!empty($package->AdvIdeaList->AdvIdea)) {
            $ideahtml = '<div class="advidea">';
            $jk = 0; 
            foreach($package->AdvIdeaList->AdvIdea as $idea) {
                if($jk>=2) break;
                if($idea->Type==11) {
                   $ideahtml .= '<span class="ideaicon">'.$idea->Content.'</span>'; 
                   $jk++; 
                }                
            }
            $ideahtml .= '</div>';
            foreach($package->AdvIdeaList->AdvIdea as $idea) {
                if($idea->Type==12) {
                    $shouldAdd = true;
                }    
            }
            if(!empty($ideaid) && $shouldAdd) {
                $curClist = array();
                foreach($package->AdvIdeaList->AdvIdea as $idea) {
                    $curClist[] = $idea->Id;
                }
                if(array_search($ideaid,$curClist) === false) {
                    $shouldAdd = false;
                }    
            }            
            if($shouldAdd && !empty($citycode)) {
                $curClist = array();
                foreach($package->CityList->City as $city) {
                    $curClist[] = $city->CityCode;         
                }
                if(array_search($citycode,$curClist) === false) {
                    $shouldAdd = false;
                }
            }            
        }else{
            $ideahtml = '';
        }
        if($shouldAdd) :
        $total_res++;
        $imgurl = 'images/Taipei7.jpg';
        if(empty($package->ImgUrls->string)) {
            if(!empty($package->CityList->City->ImgUrls->string)) {
                $imgurl = $package->CityList->City->ImgUrls->string;
                if(count($imgurl)>1) {
                    $index = rand(0,count($imgurl)-1);
                    $imgurl = $imgurl[$index];
                }
            }
        }else{
            $imgurl = $package->ImgUrls->string;
        }
        $urlname=apiHelper::convertUrl($package->Abstract);
        $pdfUrl = '';
        if(!empty($package->AttachmentList->Attachment)) { 
            foreach($package->AttachmentList->Attachment as $attach) {
                if($attach->Type=='Flyer') $pdfUrl = $attach->Url; 
            } 
        }
    ?>
    items.push({
        'id': '<?php echo $package->Id ?>',
        'txtabstract' : '<?php echo str_replace("'","\'",$package->Abstract) ?>',
        'link':'<?php echo JRoute::_('index.php?option=com_stravel&view=tourflyer&id='.$package->Id.'&name='.$urlname) ?>',
        'image':'<?php echo $imgurl ?>',
        'daynight':'<?php echo sprintf(JText::_('COM_STRAVEL_PACKAGE_DAYNIGHT_VARRIBLE'),$package->Days,$package->Nights); ?>',
        'price':'$<?php echo $package->Price; if($package->PriceInd=='Fr') echo '+'; ?>',
        'productdate':'<?php echo $this->departuredate; ?>',
        'departuredate':'<?php echo date('Y-m-d',strtotime($package->TravelPeriodFr)) ?>',
        'content':'<?php echo str_replace("'","\'",$package->Content) ?>',
        'ideahtml':'<?php echo $ideahtml ?>',
        'pdf':'<?php echo $pdfUrl ?>',
        'refcode':'<?php echo $package->RefCode ?>',
    });
    <?php endif;endforeach; ?>
    var index = 0;
    jQuery(document).ready(function($) {
        $('body').on('mouseover','.touritem-wrapper',function() {
            $(this).addClass('hover');
        });
        $('body').on('mouseleave','.touritem-wrapper',function() {
            $(this).removeClass('hover');
        });
        $('body').on('click','.loadmore',function() {
            //index = appendItems(index);
        });
        
        var $container = $('#msrcontainer');
        masonryLoad($container);
        index = appendItems(index,7);    
    });
    var appendItems = function(i,k) {
        i = i || 0;
        k = k || 3;
        var $container = $('#msrcontainer').masonry();
        for(j=i;j<=i+k && j<items.length;j++) {
            var elem = document.createElement('div');
            elem.className = 'mansonryitem col-md-3';
            elem.innerHTML = '<div class="touritem-wrapper"><div class="tourimg">'+
                    '<a href="'+items[j].link+'"><img class="img-responsive" src="'+items[j].image+'" /></a>' +
                    items[j].ideahtml +
                    '<div class="package-feature" onclick="window.location.href=\''+items[j].link+'\'">'+items[j].content+'</div></div>' +
                    '<div class="caption">'+
                        '<div class="line1 borderbottom"><div class="abstract"><a href="'+items[j].link+'">'+items[j].txtabstract+'</a></div></div>'+
                        '<div class="line1"><div class="col2 colref"><span class="refcode">'+items[j].refcode+'</span><span class="price">'+items[j].price+'</span></div><div class="col2 text-right"><div class="imgbtn">'+
                            '<a href="'+items[j].pdf+'" target="_blank">&nbsp;</a>'+
                            '<a href="#enquiryModal" data-frompage="package" data-productname="'+items[j].txtabstract+'" data-productprice="'+items[j].price+'" data-productid="'+items[j].price+'" data-productdate="" data-productnights="'+items[j].daynight+'" data-toggle="modal" data-target="#enquiryModal">&nbsp;</a>'+
                        '</div></div></div>'+
                    '</div>'+
                '</div>';
            $container.append( elem ).masonry( 'appended', elem );
        }
        if(j>=items.length) {
            $('body .loadmore').hide();
        }
        return j;
    };
    function masonryLoad($container) {
        $container.masonry({"transitionDuration": 0});
        setTimeout(function () {masonryLoad($container);},100);    
    };
    jQuery(window).scroll(function() {
        var loadmorebtn = jQuery('body .loadmore');
        if(isElementInViewport(loadmorebtn)) {
            index = appendItems(index);
        }
    });
    function isElementInViewport (el) {
        //special bonus for those using jQuery
        if (typeof jQuery === "function" && el instanceof jQuery) {
            el = el[0];
        }    
        var rect = el.getBoundingClientRect();    
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
            rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
        );
    };
</script>
<?php if(!empty($this->slideshowContent)) : ?>
<div id="stravel-slideshow" class="tour carousel carousel-fade slide">
    <ol class="carousel-indicators">
        <?php
            $i = 0; 
            foreach($this->slideshowContent as $slide) {
                if($i==0) {
                    $cls = 'active';
                }else{
                    $cls = 'inactive';
                }
                echo '<li data-target="#stravel-slideshow" data-slide-to="'.$i.'" class="'.$cls.'"></li>';
                $i++;
            }
        ?>
    </ol>            
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <?php $i=0;foreach($this->slideshowContent as $slide) : ?>
    <div class="item <?php  if($i===0) echo 'active';$i++; ?>">
      <a href="<?php echo $slide->Attachments->ContentAttachment->AttDescription; ?>">
      <img  class="lazy"   src="<?php echo 'http://www.adholidays.com/STravel/cms/Content/'.$slide->Attachments->ContentAttachment->AttType.'/'.$slide->Attachments->ContentAttachment->AttId.'_'.$slide->Attachments->ContentAttachment->AttFileName; ?>" alt="<?php echo $slide->Attachments->ContentAttachment->AttName; ?>">
      </a>
      <div class="carousel-caption hide"><a href="<?php echo $slide->Attachments->ContentAttachment->AttDescription; ?>"><?php echo $slide->Attachments->ContentAttachment->AttName; ?></a></div>
    </div>
    <?php endforeach; ?>
  </div>
</div>    
<?php endif; ?>

<section id="tourhome" class="<?php echo $lang; ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="linklist">
                    <span class="active"><?php echo JText::_('COM_STRAVEL_CATEGORY_FILTER') ?></span>
                    <a href="<?php echo JRoute::_('index.php?option=com_stravel&view=tourhome') ?>"><?php echo JText::_('STRAVEL_SHOW_ALL') ?></a>
                    <?php 
                        foreach($this->featuredPackage as $cat) {
                            if((int) $cat->Id == (int) $ideaid) {
                                $active_cls = 'active';    
                            }else{
                                $active_cls = '';
                            }
                             //add language filter 
                            if((string) $cat->Lang == (string) $lang){
                                $urlname = apiHelper::convertUrl($cat->Name);
                                echo '<a class="'.$active_cls.'" href="'.JRoute::_('index.php?option=com_stravel&view=tourhome&ideaid='.$cat->Id.'&ideaname='.$urlname).'">'.$cat->Name.'</a>';
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <div class="linklist ds">
                    <span class="active"><?php echo JText::_('COM_STRAVEL_POPULAR_CITY_FILTER') ?></span>
                    <a href="<?php echo JRoute::_('index.php?option=com_stravel&view=tourhome') ?>"><?php echo JText::_('STRAVEL_SHOW_ALL') ?></a>
                    <?php
                        $i = 1;
                        $cityName = 'CityName_'.strtolower(str_replace('-','_', (string) $lang)); 
                        foreach($this->hotCities as $city) {
                            if($i>10) {break;} $i++;
                            if($city->CityCode==$citycode) {
                                $active_cls = 'active';    
                            }else{
                                $active_cls = '';
                            }
                            $urlname = apiHelper::convertUrl($city->CityName);
                            //$infos = explode("\n",$city->Content);
                            //$urlname = apiHelper::convertUrl($infos[0]);
                            echo '<a class="'.$active_cls.'" href="'.JRoute::_('index.php?option=com_stravel&view=tourhome&city='.$city->CityCode.'&cityname='.$urlname).'">'.$city->CityName.'</a>';
                            //echo '<a href="'.JRoute::_('index.php?option=com_stravel&view=tourhome&city='.$city->Keyword.'&cityname='.$urlname).'">'.$infos[0].'</a>';
                        }
                    ?>
                </div>
            </div>
            <div class="col-md-2">
                <div class="text-right">
                    <span class="totalitem"><?php echo sprintf(JText::_('STRAVEL_TOTAL_NUMBER'),$total_res); ?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="msrcontainer"></div>
            <div class="col-md-2 col-md-offset-5"><button class="loadmore btn btn-success"><?php echo JText::_('STRAVEL_LOAD_MORE') ?></button></div>
        </div>
    </div>
</section>