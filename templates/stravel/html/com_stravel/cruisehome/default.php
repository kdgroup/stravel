<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
$lang = JFactory::getLanguage()->getTag();
?>
<section id="cruisehome" class="<?php echo $lang; ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="cbreadcrumb">
                    <li><a href="<?php echo JURI::root(); ?>"><i class="fa fa-home"></i><?php echo JText::_('COM_STRAVEL_HOME_TEXT_BREADCRUMBS') ?></a></li>
                    <li><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=cruisethome'); ?>"><?php echo JText::_('COM_STRAVEL_SEARCH_CRUISE') ?></a></li>
                </ul>
            </div>        
        </div>
        <div class="row">
            <!-- Start left pane -->
            <div class="col-md-3">
                <div class="left-pane">
                    <div class="panetitle">
                        <h3><?php echo JText::_('COM_STRAVEL_CRUISEHOME') ?></h3>
                    </div>
                    
                    <?php
                        $presetHelper = new presetHelper(); 
                        echo $presetHelper->getSearchPanel(4);
                    ?>
                    
                    <!-- Filter on left -->
                    <div class="filterleft">
                        <div class="selectroute cruisedestinationselect hide">
                            <h4><?php echo JText::_('COM_STRAVEL_SELECT_ROUTE') ?></h4>
                            <div class="dropdown">
                                <span data-toggle="dropdown" data-target="#" class="gradientgray"><span class="fakeselect form-control"><?php echo JText::_('COM_STRAVEL_SELECT_ROUTE') ?></span></span>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                    <?php foreach($this->destination as $item) : ?>
                                        <li><a href="#" data-id="<?php echo $item->Id ?>" onclick="return false;"><?php echo $item->DstName ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                        <div class="selectroute hide">
                            <div class="companyselect cruiseyearselect">
                                <h4><?php echo JText::_('COM_STRAVEL_SELECT_YEAR') ?></h4>
                                <div class="dropdown">
                                    <span data-toggle="dropdown" data-target="#" class="gradientgray"><span class="fakeselect form-control">2015</span></span>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                        <?php foreach($this->yearlist as $year) : ?>
                                            <li><a href="#" data-id="<?php echo $year ?>" onclick="return false;"><?php echo $year ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="companyselect cruisemonthselect">
                                <h4><?php echo JText::_('COM_STRAVEL_SELECT_MONTH') ?></h4>
                                <div class="dropdown">
                                    <span data-toggle="dropdown" data-target="#" class="gradientgray"><span class="fakeselect form-control">3</span></span>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                        <?php 
                                            for($i=1;$i<=12;$i++) {
                                                echo '<li><a href="#" data-id="'.$i.'" onclick="return false;">'.$i.'</a></li>';
                                            }
                                        ?>
                                    </ul>
                                </div>                                                                
                            </div>
                            <div class="companyselect cruisenightsselect">
                                <h4><?php echo JText::_('COM_STRAVEL_NIGHTS') ?></h4>
                                <div class="dropdown">
                                    <span data-toggle="dropdown" data-target="#" class="gradientgray"><span class="fakeselect form-control">&lt;6</span></span>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                        <li><a href="#" data-id="1" onclick="return false;">&lt;6</a></li>
                                        <li><a href="#" data-id="2" onclick="return false;">7-9</a></li>
                                        <li><a href="#" data-id="3" onclick="return false;">10-14</a></li>
                                        <li><a href="#" data-id="4" onclick="return false;">15-25</a></li>
                                        <li><a href="#" data-id="5" onclick="return false;">&gt;26</a></li>
                                    </ul>
                                </div>                                
                            </div>
                        </div>
                        <div class="selectroute hide">
                            <div class="companyselect">
                                <h4><?php echo JText::_('COM_STRAVEL_SELECT_CRUISE') ?></h4>
                                <div class="dropdown cruiselineselect">
                                    <span data-toggle="dropdown" data-target="#" class="gradientgray"><span class="fakeselect form-control"><?php echo $this->cruisecpn->CruiseLineName ?></span></span>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                        <?php foreach($this->cruisecpn as $item) : ?>
                                        <li><a href="#" onclick="return false;" data-id="<?php echo $item->Id; ?>"><?php echo $item->CruiseLineName; ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="companyselect">
                                <h4><?php echo JText::_('COM_STRAVEL_SELECT_CRUISE2') ?></h4>
                                <div class="dropdown cruisevesselselect">
                                    <span data-toggle="dropdown" data-target="#" class="gradientgray"><span class="fakeselect form-control"><?php echo $this->cruisevessel->VesselName ?></span></span>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                        <?php foreach($this->cruisevessel as $item) : ?>
                                        <li><a href="#" onclick="return false;" data-id="<?php echo $item->Id; ?>"><?php echo $item->VesselName; ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="selectroute hide">
                            <div class="dragprice companyselect">
                                <h4><?php echo JText::_('COM_STRAVEL_PRICE') ?></h4>
                                <p class="text-center">
                                    <?php echo JText::_('COM_STRAVEL_HK') ?>
                                    <span id="pricefrom">$500</span>
                                    <?php echo JText::_('COM_STRAVEL_TO') ?>
                                    <span id="priceto">$19999</span>
                                </p>
                                <input id="greendragprice" type="slider" name="price" value="500;19999" />
                            </div>
                        </div>
                    </div>
                    <!-- End filter --> 
                    
                </div>                
            </div>
            <!-- End left pane -->
            
            <!-- Start right pane -->
            <div class="col-md-9">
                <div class="right-pane">
                    <!-- Basic sort -->
                    <div class="basicsort">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="row">
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_SORTBY') ?></p></div>
                                            <div class="col-md-3">
                                                <div class="dropdown">
                                                    <span data-direction="1" data-toggle="dropdown" data-target="#" class="fakesortvalue fakeselectgray form-control"><?php echo JText::_('COM_STRAVEL_PRICE_LOW_TO_HIGH') ?></span>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownSelect">
                                                        <li><a data-direction="1" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_PRICE_LOW_TO_HIGH') ?></a></li>
                                                        <li><a data-direction="2" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_PRICE_HIGH_TO_LOW') ?></a></li>
                                                        <li><a data-direction="3" class="sortbyprice" href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_LASTEST_OFFERS') ?></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                    <!-- end basic sort -->
                    <!-- Filter -->
                    <div class="filter">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="row">
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_HOT_CRUISE_COMPANY') ?></p></div>
                                            <div class="col-md-10">
                                                <div class="linklist">
                                                <?php 
                                                    foreach($this->hotcruisecpn as $cruise) {
                                                        $urlname = apiHelper::convertUrl($cruise->Content);
                                                        echo '<a href="'.JRoute::_('index.php?option=com_stravel&view=cruisehome&cruisecpn='.$cruise->Id.'&cruisecpnname='.$urlname).'">'.$cruise->Content.'</a>';
                                                    }
                                                ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"><p><?php echo JText::_('COM_STRAVEL_HOT_ROUTE') ?></p></div>
                                            <div class="col-md-10">
                                                <div class="linklist">
                                                <?php 
                                                    foreach($this->hotcruisedst as $cruise) {
                                                        $urlname = apiHelper::convertUrl($cruise->Content);
                                                        echo '<a href="'.JRoute::_('index.php?option=com_stravel&view=cruisehome&cruisedst='.$cruise->Id.'&cruisedstname='.$urlname).'">'.$cruise->Content.'</a>';
                                                    }
                                                ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End fitler -->
                    
                    <!-- Item List -->
                    <div class="itemlist">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1 listcontainer">
                                    <?php 
                                        $i=0;$j=1;foreach($this->cruises as $cruise) : 
                                        if($i%12==0) {
                                            echo '<div class="page-sep page-sep'.$j.'">';
                                            $j++;
                                        }
                                        if($i%3==0) {
                                            echo '<div class="row">';
                                        }
                                    ?>
                                        <?php $urlname = apiHelper::convertUrl($cruise->Title); ?>
                                        <div data-cruiseid="<?php echo $cruise->Id ?>" data-nights="<?php echo $cruise->Nights; ?>" data-cruisevesselid="<?php echo $cruise->VesselId ?>" data-cruiselineid="<?php echo $cruise->CruiseLineId ?>" data-destinationid="<?php echo $cruise->DestinationId; ?>" data-month="<?php echo date('m',strtotime($cruise->TravelPeriodFr)); ?>" data-year="<?php echo date('Y',strtotime($cruise->TravelPeriodFr)); ?>" data-effectfrom="<?php echo strtotime($cruise->TravelPeriodFr); ?>" data-price="<?php echo (int)$cruise->Price; ?>" class="product-container col-md-4">
                                            <div class="cruise-item product-item">
                                                <div class="cruise-image product-image text-center">
                                                    <a href="<?php echo JRoute::_('index.php?option=com_stravel&view=cruiseflyer&id='.$cruise->Id.'&name='.$urlname);?>">
                                                    <img class="img-responsive lazy" data-original="<?php echo $cruise->VesselImageUrl->string; ?>" alt="cruise img" />
                                                    </a>
                                                    <div class="caption">
                                                        <h6><?php echo $cruise->CruiseLineName ?></h6>
                                                        <h6><?php echo $cruise->VesselName ?></h6>
                                                    </div>
                                                    <?php if(!empty($cruise->AdvIdeaList->AdvIdea)) : ?>
                                                        <div class="advidea">
                                                            <?php
                                                                $jk = 0; 
                                                                foreach($cruise->AdvIdeaList->AdvIdea as $idea) {
                                                                    if($jk>=2) break;
                                                                    if($idea->Type==3) {
                                                                       echo '<span class="ideaicon">'.$idea->Content.'</span>'; 
                                                                       $jk++; 
                                                                    }
                                                                }                                                           
                                                            ?>
                                                        </div>
                                                    <?php endif; ?>
                                                    <?php if(!empty($cruise->Content)) : ?>
                                                    <div class="package-feature" onclick="location.href='<?php echo JRoute::_('index.php?option=com_stravel&view=cruiseflyer&id='.$cruise->Id.'&name='.$urlname)  ?>';"><?php echo $cruise->Content ?></div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="cruise-info product-info">
                                                    <h6><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=cruiseflyer&id='.$cruise->Id.'&name='.$urlname);?>"><?php echo $cruise->Title ?></a></h6>
                                                    <div class="cruise-bottom-info">
                                                        <div class="cruise-price"><span>$<?php echo (int)$cruise->Price; if($cruise->PriceInd==1) echo '+'; ?></span></div>
                                                        <div class="cruise-link text-right">
                                                            <a href="#enquiryModal"
                                                                data-frompage="cruise"
                                                                data-productid="<?php echo $cruise->VesselId; ?>" 
                                                                data-productname="<?php echo $cruise->Title; ?>" 
                                                                data-productprice="$<?php echo (int)$cruise->Price; ?>+"
                                                                data-productship="<?php echo $cruise->VesselName; ?>"
                                                                data-productdate="<?php if(!empty($cruise->TravelPeriodFr)) echo array_shift(explode('T',$cruise->TravelPeriodFr)); if(!empty($cruise->TravelPeriodTo)) echo JText::_('COM_STRAVEL_TO'). array_shift(explode('T',$cruise->TravelPeriodTo)); ?>"
                                                                data-productnights="<?php echo sprintf(JText::_('COM_STRAVEL_PACKAGE_DAYNIGHT_VARRIBLE'),$cruise->Days,$cruise->Nights); ?>"  
                                                                data-toggle="modal" data-target="#enquiryModal">&nbsp;</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                        if($i%3==2 || ($i+1)==count($this->cruises)) {echo '</div>';}
                                        if($i%12==11 || ($i+1)>=count($this->cruises)) {echo '</div>';}
                                        $i++; 
                                        endforeach; 
                                    ?>
                            </div>
                        </div>
                        <!-- Disclaimer & pagination -->                        
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="row disclaimer noborder">
                                    <?php if(!empty($this->disclaimer)) : ?>
                                    <div class="col-md-7"><?php echo str_replace("\n",'<br />',$this->disclaimer->Content) ?></div>
                                    <?php endif; ?>
                                    <?php if($i>12) : ?>
                                    <div class="col-md-5 text-right">
                                        <ul class="pagination">                                  
                                          <?php 
                                            $n = $i%12;
                                            if($n==0) {
                                                $n = $i/12;
                                            }else{
                                                $n = floor($i/12)+1;
                                            }
                                            echo '<li data-totalpage="'.$n.'" data-currentpage="1" data-page="prev" class="first disabled"><a href="#">&laquo;</a></li>'; 
                                            for($i=1;$i<=$n;$i++) {
                                                if($i==1) {
                                                    $cls = "disabled";
                                                }else{
                                                    $cls = '';
                                                }
                                                if($i>=1 && $i<=5) {
                                                    $cls .= " showed";
                                                }else{
                                                    $cls .= '';
                                                }
                                                echo '<li data-totalpage="'.$n.'" data-currentpage="1" class="pagsseitem pagsseitem'.$i.' '.$cls.'" data-page="'.$i.'"><a href="#">'.$i.'</a></li>';
                                            }
                                            echo '<li data-totalpage="'.$n.'" data-currentpage="1" data-page="next" class="last"><a href="#">&raquo;</a></li>'; 
                                          ?>
                                        </ul>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>                        
                        <!-- Disclaimer -->
                    </div>
                    <!-- End Item List -->    
                </div>
            </div>
            <!-- End right pane -->
        </div>
    </div>
</section>