<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
$nItemId = JFactory::getApplication()->getMenu()->getItems('link','index.php?option=com_enmasse&view=deallisting',true)->id;
$lang = strtolower(str_replace('-','_',JFactory::getLanguage()->getTag()));
$usedId = array();
$doc = JFactory::getDocument();
$doc->addScriptDeclaration("
jQuery(document).ready(function() {
    jQuery('#stravel-slideshow,#packagebannertext').carousel({interval:5000});
});
");
/*
$doc->addScriptDeclaration('jQuery(document).ready(function() {

			if (jQuery.fn.cssOriginal!=undefined)
				jQuery.fn.css = jQuery.fn.cssOriginal;

				var revapi = jQuery(".fullscreenbanner").revolution(
					{
						delay:6000,
						startwidth:1170,
						startheight:400,

						onHoverStop:"on",						// Stop Banner Timet at Hover on Slide on/off

						thumbWidth:100,							// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
						thumbHeight:50,
						thumbAmount:3,

						hideThumbs:0,
						navigationType:"bullet",				// bullet, thumb, none
						navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none

						navigationStyle:"round",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom


						navigationHAlign:"center",				// Vertical Align top,center,bottom
						navigationVAlign:"bottom",					// Horizontal Align left,center,right
						navigationHOffset:15,
						navigationVOffset:16,

						soloArrowLeftHalign:"left",
						soloArrowLeftValign:"center",
						soloArrowLeftHOffset:20,
						soloArrowLeftVOffset:0,

						soloArrowRightHalign:"right",
						soloArrowRightValign:"center",
						soloArrowRightHOffset:20,
						soloArrowRightVOffset:0,

						touchenabled:"on",						// Enable Swipe Function : on/off


						stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
						stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic

						hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
						hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
						hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value


						fullWidth:"on",							// Same time only Enable FullScreen of FullWidth !!
						fullScreen:"off",						// Same time only Enable FullScreen of FullWidth !!


						shadow:0								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)

					});
                
                revapi.bind("revolution.slide.onloaded", function() {
                    jQuery(".fullscreen-container").css("opacity","1");            		
            	});


		});');
        */
        
?>
<?php if(!empty($this->slideshowContent)) : ?>
<div class="container-fluid">
    <div class="row">
        <div id="stravel-slideshow" class="carousel carousel-fade slide">
            <ol class="carousel-indicators">
                <?php
                    $i = 0; 
                    foreach($this->slideshowContent as $slide) {
                        if($i==0) {
                            $cls = 'active';
                        }else{
                            $cls = 'inactive';
                        }
                        echo '<li data-target="#stravel-slideshow" data-slide-to="'.$i.'" class="'.$cls.'"></li>';
                        $i++;
                    }
                ?>
            </ol>            
          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <?php $i=0;foreach($this->slideshowContent as $slide) : ?>
            <div class="item <?php  if($i===0) echo 'active';$i++; ?>">
              <a href="<?php echo $slide->Attachments->ContentAttachment->AttDescription; ?>">
              <img   src="<?php echo 'http://www.adholidays.com/STravel/cms/Content/'.$slide->Attachments->ContentAttachment->AttType.'/'.$slide->Attachments->ContentAttachment->AttId.'_'.$slide->Attachments->ContentAttachment->AttFileName; ?>" alt="<?php echo $slide->Attachments->ContentAttachment->AttName; ?>">
              </a>
              <div class="carousel-caption hide"><a href="<?php echo $slide->Attachments->ContentAttachment->AttDescription; ?>"><?php echo $slide->Attachments->ContentAttachment->AttName; ?></a></div>
            </div>
            <?php endforeach; ?>
          </div>
        </div>    
    </div>
</div>
<?php endif; ?>

<div class="container">
    <div class="row <?php echo $lang; ?>">
        <div class="col-md-4">
            <!-- Search panel -->
            <div class="search">
                <ul class="nav searchtab" role="tablist">
                  <li class="active"><a class="<?php echo $lang ?>" href="#package-search" role="tab" data-toggle="tab"><span class="package-icon">&nbsp;</span><?php echo JText::_('COM_STRAVEL_SEARCH_PACKAGE') ?></a></li>
                  <li><a class="<?php echo $lang ?>" href="#airticket-search" role="tab" data-toggle="tab"><span class="airticket-icon"></span><?php echo JText::_('COM_STRAVEL_SEARCH_AIRTICKET') ?></a></li>
                  <li><a class="<?php echo $lang ?>" href="#hotel-search" role="tab" data-toggle="tab"><span class="hotel-icon"></span><?php echo JText::_('COM_STRAVEL_SEARCH_HOTEL') ?></a></li>
                  <li><a class="<?php echo $lang ?>" href="#cruise-search" role="tab" data-toggle="tab"><span class="cruise-icon"></span><?php echo JText::_('COM_STRAVEL_SEARCH_CRUISE') ?></a></li>
                  <li class="hide"><a class="<?php echo $lang ?>" href="#jettour-search" role="tab" data-toggle="tab"><span class="jettour-icon"></span><?php echo JText::_('COM_STRAVEL_SEARCH_JETTOUR') ?></a></li>
                </ul>
                <div class="tab-content">
                  <!-- package search -->
                  <div class="tab-pane fade in active" id="package-search">
                    <form action="index.php" name="packageSearch" method="get" role="form" autocomplete="off">
                        <input type="hidden" name="option" value="com_stravel" />
                        <input type="hidden" name="view" value="packagehome" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label><?php echo JText::_('COM_STRAVEL_DEPARTURE_CITY_HONGKONG') ?></label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="arrivedCity"><?php echo JText::_('COM_STRAVEL_ARRIVED_CITY') ?></label>
                                    <div class="hotelcitylist dropdown">
                                        <input data-target="#" data-toggle="dropdown" id="arrivedCity" type="text" class="form-control" name="cityname" placeholder="<?php echo JText::_('COM_STRAVEL_CITY_SELECT_PLACEHOLDER') ?>">
                                        <ul class="packagecitylistitem dropdown-menu">
                                            <?php
                                                echo $this->mergecity;
                                                $i=1;
                                                $citynamelang = 'CityName_'.$lang;
                                                $cls = 'displaynone normalcity';
                                                foreach($this->packagehotcities as $city) {
                                                    //if($i>10) $cls = 'displaynone'; else $cls = '';
                                                    //if($i==1) {$extratxt = '<span class="hottxt">'.JText::_('COM_STRAVEL_HOTCITY_LABEL').'</span>';}else{ $extratxt = ''; }
                                                    echo '<li class="'.$cls.'" data-cityname="'.$city->$citynamelang.'" data-citycode="'.$city->CityCode.'" data-name="'.$city->CityName_zh_tw.' ('.$city->CityCode.') '.$city->CityName_zh_cn.' '.$city->CityName_en_us.'" data-order="'.$i.'"><a href="#" onclick="return false;">'.$city->CityName_zh_tw.' ('.$city->CityCode.') '.$city->CityName_zh_cn.' '.$city->CityName_en_us.'</a></li>';
                                                    $i++;            
                                                }    
                                            ?>
                                        </ul>
                                        <input type="hidden" name="city" />
                                    </div>                                    
                                </div>    
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="departureDate"><?php echo JText::_('COM_STRAVEL_DEPARTURE_DATE') ?></label>
                                    <input name="departureDate" type="text" class="form-control datepicker" id="departureDate">
                                </div>    
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="arrivedDate"><?php echo JText::_('COM_STRAVEL_ARRIVED_DATE') ?></label>
                                    <input name="arrivedDate" type="text" class="form-control datepicker" id="arrivedDate">
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><?php echo JText::_('COM_STRAVEL_THEME_PACKAGE') ?></label>
                                    <div class="dropdown themepackage">
                                        <span data-toggle="dropdown" data-target="#" class="fakeselect form-control"><?php echo JText::_('COM_STRAVEL_ALL_PACKAGE_FEATURE') ?></span>
                                        <ul class="dropdown-menu">
                                            <li data-name="<?php echo JText::_('COM_STRAVEL_ALL_PACKAGE_FEATURE') ?>" data-id="0"><a href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_ALL_PACKAGE_FEATURE') ?></a></li>
                                            <?php 
                                                foreach($this->featuredPackage as $item) : 
                                                    if($item->Lang==$lang) :
                                            ?>
                                                <li data-name="<?php echo ($item->Name) ?>" data-id="<?php echo $item->Id ?>"><a href="#" onclick="return false;"><?php echo $item->Name; ?></a></li>
                                            <?php endif;endforeach; ?>
                                        </ul>
                                        <input type="hidden" name="ideaid">
                                        <input type="hidden" name="ideaname">
                                    </div>
                                </div>    
                            </div>    
                        </div>
                        <div class="row search_button_row">
                            <div class="col-md-12 search_button">
                                <button class="searchbtn"><?php echo JText::_('SEARCH') ?></button>
                            </div>
                        </div>
                    </form>  
                  </div>
                  <!-- End package search -->
                  <!-- Search air ticket -->
                  <div class="tab-pane fade" id="airticket-search">
                    <form action="index.php" name="airticketSearch" method="get" role="form" autocomplete="off">
                        <input type="hidden" name="option" value="com_stravel" />
                        <input type="hidden" name="view" value="airsearchresult" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label><?php echo JText::_('COM_STRAVEL_DEPARTURE_CITY_HONGKONG') ?></label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="arrivedCity2"><?php echo JText::_('COM_STRAVEL_ARRIVED_CITY') ?></label>
                                    <div class="aircitylist dropdown">
                                        <input data-target="#" data-toggle="dropdown" placeholder="<?php echo JText::_('COM_STRAVEL_CITY_SELECT_PLACEHOLDER') ?>" id="arrivedCity2" type="text" class="form-control showname" name="cityname">
                                        <ul class="aircitylistitem dropdown-menu"></ul>
                                        <input type="hidden" name="citycode" />
                                    </div>                                    
                                </div>    
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="departureDate2"><?php echo JText::_('COM_STRAVEL_DEPARTURE_DATE') ?></label>
                                    <input name="departureDate" type="text" class="form-control datepicker" id="departureDate2">
                                </div>    
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="arrivedDate2"><?php echo JText::_('COM_STRAVEL_ARRIVED_DATE') ?></label>
                                    <input name="arrivedDate" type="text" class="form-control datepicker" id="arrivedDate2">
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group airclass">
                                    <label><?php echo JText::_('COM_STRAVEL_CLASS') ?></label>
                                    <div class="dropdown">
                                        <span data-toggle="dropdown" data-target="#" class="fakeselect form-control"><?php echo JText::_('COM_STRAVEL_ECONOMY') ?></span>
                                        <ul class="dropdown-menu" role="menu">
                                            <li data-id="Y"><a href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_ECONOMY') ?></a></li>
                                            <li data-id="C"><a href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_BUSSINESS') ?></a></li>
                                            <li data-id="F"><a href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_FIRST_CLASS') ?></a></li>
                                            <li data-id="P"><a href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_PREMIUM_ECONOMY_CLASS') ?></a></li>
                                        </ul>
                                    </div> 
                                    <input type="hidden" name="type">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group triptype">
                                    <label>&nbsp;</label>
                                    <div class="dropdown">
                                        <span data-toggle="dropdown" data-target="#" class="fakeselect form-control"><?php echo JText::_('COM_STRAVEL_SINGLETRIP') ?></span>
                                        <ul class="dropdown-menu" role="menu">
                                            <li data-id="1"><a href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_SINGLETRIP') ?></a></li>
                                            <li data-id="2"><a href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_ROUNDTRIP') ?></a></li>
                                        </ul>
                                    </div>
                                    <input type="hidden" name="triptype">
                                </div>    
                            </div>    
                        </div>
                        <div class="row search_button_row">
                            <div class="col-md-12 search_button">
                                <button class="searchbtn"><?php echo JText::_('SEARCH') ?></button>
                            </div>
                        </div>
                    </form>
                  </div>
                  <!-- End search air ticket -->
                  
                  <!-- Hotel search -->
                  <div class="tab-pane fade" id="hotel-search">
                    <form action="index.php" name="hotelSearch" method="get" role="form" autocomplete="off">
                        <input type="hidden" name="option" value="com_stravel" />
                        <input type="hidden" name="view" value="hotelresult" />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="destinationht"><?php echo JText::_('COM_STRAVEL_DESTINATION') ?></label>
                                    <div class="hotelcitylist dropdown">
                                        <input id="destinationht" data-target="#" data-toggle="dropdown" type="text" class="form-control" name="destination" placeholder="<?php echo JText::_('COM_STRAVEL_CITY_SELECT_PLACEHOLDER') ?>">
                                        <ul class="hotelcitylistitem dropdown-menu">
                                        </ul>
                                        <input type="hidden" name="city" />
                                    </div>                                    
                                </div>    
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="checkinDate"><?php echo JText::_('COM_STRAVEL_CHECKIN_DATE') ?></label>
                                    <input name="checkinDate" type="text" class="form-control datepicker" id="checkinDate">
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><?php echo JText::_('COM_STRAVEL_NIGHTS') ?></label>
                                    <div class="dropdown hotelnights">
                                        <span data-target="#" data-toggle="dropdown" class="fakeselect form-control">1</span>
                                        <ul class="dropdown-menu">
                                            <?php 
                                                for($i=1;$i<=14;$i++) {
                                                    echo '<li><a href="#" data-id="'.$i.'" onclick="return false;">'.$i.'</a></li>';
                                                } 
                                            ?>
                                        </ul>
                                    </div>
                                    <input type="hidden" name="nights" value="1">
                                </div> 
                            </div>
                        </div>
                        <div class="row search_button_row">
                            <div class="col-md-12 search_button">
                                <button class="searchbtn"><?php echo JText::_('SEARCH') ?></button>
                            </div>
                        </div>
                    </form>
                  </div>
                  <!-- End hotel search -->
                  <!-- Cruise search -->
                  <div class="tab-pane fade" id="cruise-search">
                    <form action="index.php" name="cruiseSearch" method="get" role="form" autocomplete="off">
                        <input type="hidden" name="option" value="com_stravel" />
                        <input type="hidden" name="view" value="cruisehome" />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><?php echo JText::_('COM_STRAVEL_SELECT_ROUTE') ?></label>
                                    <div class="dropdown selectroutecruise">
                                        <span data-target="#" data-toggle="dropdown" class="fakeselect form-control"><?php echo JText::_('COM_STRAVEL_SELECT_ROUTE') ?></span>
                                        <ul class="dropdown-menu">
                                        <?php foreach($this->destination as $item) : ?>
                                            <li><a href="#" data-id="<?php echo $item->Id ?>" onclick="return false;"><?php echo $item->DstName ?></a></li>
                                        <?php endforeach; ?>
                                        </ul>
                                    </div>                                    
                                    <input type="hidden" name="cruisedst">
                                    <input type="hidden" name="cruisedstname">
                                </div>    
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><?php echo JText::_('COM_STRAVEL_SELECT_YEAR') ?></label>
                                    <div class="dropdown cruiseyear">
                                        <span data-target="#" data-toggle="dropdown" class="fakeselect form-control">2014</span>
                                        <ul class="dropdown-menu">
                                            <li><a data-id="2014" href="#" onclick="return false;">2014</a></li>
                                            <li><a data-id="2015" href="#" onclick="return false;">2015</a></li>
                                        </ul>
                                    </div>
                                    <input type="hidden" name="year">
                                </div> 
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><?php echo JText::_('COM_STRAVEL_SELECT_MONTH') ?></label>
                                    <div class="dropdown">
                                        <span data-target="#" data-toggle="dropdown" class="fakeselect form-control">8月</span>
                                        <ul class="dropdown-menu">
                                        <?php 
                                            for($i=1;$i<=12;$i++) {
                                                echo '<li><a data-id="'.$i.'" href="#" onclick="return false;">'.$i.JText::_('COM_STRAVEL_MONTH').'</a></li>';
                                            }
                                        ?>
                                        </ul>
                                    </div>                                    
                                    <input type="hidden" name="month">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><?php echo JText::_('COM_STRAVEL_NIGHTS') ?></label>
                                    <div class="dropdown">
                                        <span data-toggle="dropdown" data-target="#" class="fakeselect form-control">&lt;6</span>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a data-id="1" href="#" onclick="return false;">&lt;6</a></li>
                                            <li><a data-id="2" href="#" onclick="return false;">7-9</a></li>
                                            <li><a data-id="3" href="#" onclick="return false;">10-14</a></li>
                                            <li><a data-id="4" href="#" onclick="return false;">15-25</a></li>
                                            <li><a data-id="5" href="#" onclick="return false;">&gt;26</a></li>
                                        </ul>
                                    </div>                                            
                                    <input type="hidden" name="numday">
                                </div>
                            </div>
                                
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><?php echo JText::_('COM_STRAVEL_SELECT_CRUISE') ?></label>
                                    <div class="dropdown selectcruisecpn">
                                        <span data-target="#" data-toggle="dropdown" class="fakeselect form-control"><?php echo JText::_('COM_STRAVEL_SELECT_CRUISE') ?></span>
                                        <ul class="dropdown-menu">
                                        <?php foreach($this->cruisecpn as $item) : ?>
                                            <li><a data-id="<?php echo $item->Id ?>" href="#" onclick="return false"><?php echo $item->CruiseLineName ?></a></li>
                                        <?php endforeach; ?>
                                        </ul>
                                    </div>
                                    <input type="hidden" name="cruisecpn">
                                    <input type="hidden" name="cruisecpnname">
                                </div>    
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><?php echo JText::_('COM_STRAVEL_SELECT_CRUISE2') ?></label>
                                    <div class="dropdown selectcruisevessel">
                                        <span data-target="#" data-toggle="dropdown" class="fakeselect form-control"><?php echo JText::_('COM_STRAVEL_SELECT_CRUISE2') ?></span>
                                        <ul class="dropdown-menu">
                                        <?php foreach($this->cruisevessel as $item) : ?>
                                            <li><a data-id="<?php echo $item->Id ?>" href="#" onclick="return false"><?php echo $item->VesselName ?></a></li>
                                        <?php endforeach; ?>
                                        </ul>
                                    </div>                                    
                                    <input type="hidden" name="cruisevessel">
                                    <input type="hidden" name="cruisevesselname">
                                </div>    
                            </div>
                        </div>
                        <div class="row search_button_row">
                            <div class="col-md-12 search_button">
                                <button class="searchbtn"><?php echo JText::_('SEARCH') ?></button>
                            </div>
                        </div>
                    </form>
                  </div>
                  <!-- End cruise search -->
                  
                  <!-- Jettour search -->
                  <div class="tab-pane fade" id="jettour-search">
                        <form action="index.php" name="jettourSearch" method="get" role="form" autocomplete="off">
                            <input type="hidden" name="option" value="com_stravel" />
                            <input type="hidden" name="task" value="" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><?php echo JText::_('COM_STRAVEL_CHOOSE_COUNTRY') ?></label>
                                        <span class="fakeselect form-control">台中</span>
                                        <input type="hidden" name="jettourChooseCountry">
                                    </div> 
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><?php echo JText::_('COM_STRAVEL_CHOOSE_CITY') ?></label>
                                        <span class="fakeselect form-control">台中</span>
                                        <input name="jettourChooseCity" type="hidden" />
                                    </div> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jettourDepartureDate"><?php echo JText::_('COM_STRAVEL_DEPARTURE_DATE') ?></label>
                                        <input name="jettourDepartureDate" type="text" class="form-control datepicker" id="jettourDepartureDate">
                                    </div> 
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jettourArrivedDate"><?php echo JText::_('COM_STRAVEL_ARRIVED_DATE') ?></label>
                                        <input name="jettourArrivedDate" type="text" class="form-control datepicker" id="jettourArrivedDate">
                                    </div> 
                                </div>
                            </div>
                            <div class="row search_button_row">
                                <div class="col-md-12 search_button">
                                    <button class="searchbtn"><?php echo JText::_('SEARCH') ?></button>
                                </div>
                            </div>
                        </form>
                  </div>  
                  <!-- End jettour search -->
                </div>
            </div>
            <!-- End search panel -->
        </div>
        <?php 
            if(!empty($this->hot1stitem)) {
                echo '<div class="col-md-4">
                    <div class="hotproduct">
                        <div class="hot-product-image">
                            <a href="'.$this->hot1stitem->Attachments->ContentAttachment->AttDescription.'">
                            <img class="bannerimg img-responsive lazy" data-original="http://www.adholidays.com/STravel/cms/Content/'.$this->hot1stitem->Attachments->ContentAttachment->AttType.'/'.$this->hot1stitem->Attachments->ContentAttachment->AttId.'_'.$this->hot1stitem->Attachments->ContentAttachment->AttFileName.'" alt="package top 2 items" />
                            </a>
                        </div>
                    </div>
                </div>';
            }else{
                $i = 1; 
                foreach($this->top10package as $item) :
                $isFront2Item = false;
                foreach($item->AdvIdeaList->AdvIdea as $idea) {
                    if($idea->Id==182) {
                        $isFront2Item = true;
                    }
                } 
                if($isFront2Item) :
                if($i>1) break; $i++;
                $usedId[] = $item->Id;
                $imgurl = 'images/Taipei7.jpg';
                if(empty($item->ImgUrls->string)) {
                    if(!empty($item->CityList->City->ImgUrls->string)) {
                        $imgurl = $item->CityList->City->ImgUrls->string;
                    }
                }else{
                    $imgurl = $item->ImgUrls->string;
                }
            ?>
                <div class="col-md-4">
                    <div class="hotproduct">
                        <div class="hot-product-image">
                            <a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packagehome&city='.$item->CityList->City->CityCode.'&cityname='.$item->CityList->City->CityName); ?>">
                            <img class="tempimg img-responsive lazy" data-original="<?php echo $imgurl; ?>" alt="<?php echo $item->Abstract; ?>" />
                            </a>
                            <?php if(!empty($item->AdvIdeaList->AdvIdea)) : ?>
                                <div class="advidea">
                                    <?php
                                        $jk = 0; 
                                        foreach($item->AdvIdeaList->AdvIdea as $idea) {
                                            if($jk>=2) break;
                                            if($idea->Id==197) {
                                               echo '<span class="ideaicon">'.$idea->Content.'</span>'; 
                                               $jk++; 
                                            }
                                        }                                                           
                                    ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="hot-product-info">
                            <h4><?php echo $item->Abstract; ?></h4>
                            <div class="priceandicon">
                                <div class="pull-left"><span class="price">$<?php echo $item->Price; if($item->PriceInd=='Fr') echo '+'; ?></span></div>
                                <div class="pull-right">
                                    <a href="<?php if(!empty($item->AttachmentList->Attachment)) { foreach($item->AttachmentList->Attachment as $attach) {  if($attach->Type=='Flyer') echo $attach->Url; } } ?>" target="_blank"><img src="images/download.png" alt="Download" width="15" /></a>
                                    <a href="#enquiryModal"
                                        data-frompage="package" 
                                        data-productname="<?php echo $item->Abstract; ?>" 
                                        data-productprice="$<?php echo $item->Price; if($item->PriceInd=='Fr') echo '+'; ?>" 
                                        data-productid="<?php echo $item->Id ?>" 
                                        data-productdate="<?php if(!empty($item->TravelPeriodFr)) echo array_shift(explode('T',$item->TravelPeriodFr)); if(!empty($item->TravelPeriodTo)) echo JText::_('COM_STRAVEL_TO'). array_shift(explode('T',$item->TravelPeriodTo)); ?>"
                                        data-productnights="<?php echo sprintf(JText::_('COM_STRAVEL_PACKAGE_DAYNIGHT_VARRIBLE'),$item->Days,$item->Nights); ?>" 
                                        data-toggle="modal" 
                                        data-target="#enquiryModal"><img src="images/email.png" alt="Enquiry" width="18" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <?php
                endif;
                endforeach;    
            }
        ?>
        <?php                
            if(!empty($this->hot2nditem)) {
                echo '<div class="col-md-4">
                    <div class="hotproduct">
                        <div class="hot-product-image">
                            <a href="'.$this->hot2nditem->Attachments->ContentAttachment->AttDescription.'">
                            <img class="bannerimg img-responsive lazy" data-original="http://www.adholidays.com/STravel/cms/Content/'.$this->hot2nditem->Attachments->ContentAttachment->AttType.'/'.$this->hot2nditem->Attachments->ContentAttachment->AttId.'_'.$this->hot2nditem->Attachments->ContentAttachment->AttFileName.'" alt="package top 2 items" />
                            </a>
                        </div>
                    </div>
                </div>';
            }else{
                $i = 1; 
                foreach($this->top10package as $item) :
                $isFront2Item = false;
                foreach($item->AdvIdeaList->AdvIdea as $idea) {
                    if($idea->Id==182) {
                        $isFront2Item = true;
                    }
                } 
                if($isFront2Item) :
                if($i>1) break; $i++;
                $usedId[] = $item->Id;
                $imgurl = 'images/Taipei7.jpg';
                if(empty($item->ImgUrls->string)) {
                    if(!empty($item->CityList->City->ImgUrls->string)) {
                        $imgurl = $item->CityList->City->ImgUrls->string;
                    }
                }else{
                    $imgurl = $item->ImgUrls->string;
                }
        ?>
        <div class="col-md-4">
            <div class="hotproduct">
                <div class="hot-product-image">
                    <a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packagehome&city='.$item->CityList->City->CityCode.'&cityname='.$item->CityList->City->CityName); ?>">
                    <img class="tempimg img-responsive lazy" data-original="<?php echo $imgurl; ?>" alt="<?php echo $item->Abstract; ?>" />
                    </a>
                    <?php if(!empty($item->AdvIdeaList->AdvIdea)) : ?>
                        <div class="advidea">
                            <?php
                                $jk = 0; 
                                foreach($item->AdvIdeaList->AdvIdea as $idea) {
                                    if($jk>=2) break;
                                    if($idea->Id==197) {
                                       echo '<span class="ideaicon">'.$idea->Content.'</span>'; 
                                       $jk++; 
                                    }
                                }                                                           
                            ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="hot-product-info">
                    <h4><?php echo $item->Abstract; ?></h4>
                    <div class="priceandicon">
                        <div class="pull-left"><span class="price">$<?php echo $item->Price; if($item->PriceInd=='Fr') echo '+'; ?></span></div>
                        <div class="pull-right">
                            <a href="<?php if(!empty($item->AttachmentList->Attachment)) { foreach($item->AttachmentList->Attachment as $attach) {  if($attach->Type=='Flyer') echo $attach->Url; } } ?>" target="_blank"><img src="images/download.png" alt="Download" width="15" /></a>
                            <a href="#enquiryModal"
                                data-frompage="package" 
                                data-productname="<?php echo $item->Abstract; ?>" 
                                data-productprice="$<?php echo $item->Price; if($item->PriceInd=='Fr') echo '+'; ?>" 
                                data-productid="<?php echo $item->Id ?>" 
                                data-productdate="<?php if(!empty($item->TravelPeriodFr)) echo array_shift(explode('T',$item->TravelPeriodFr)); if(!empty($item->TravelPeriodTo)) echo JText::_('COM_STRAVEL_TO'). array_shift(explode('T',$item->TravelPeriodTo)); ?>"
                                data-productnights="<?php echo sprintf(JText::_('COM_STRAVEL_PACKAGE_DAYNIGHT_VARRIBLE'),$item->Days,$item->Nights); ?>" 
                                data-toggle="modal" 
                                data-target="#enquiryModal"><img src="images/email.png" alt="Enquiry" width="18" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
            endif;
            endforeach;
            } 
        ?>
    </div>
    
    <!-- Top 10 -->
    <div class="row">
        <div class="col-md-4">
            <?php if(!empty($this->top10package)) : ?>
            <div class="topten topten-package">
                <h3><?php echo JText::_('COM_STRAVEL_TOPTEN_PACKAGE') ?></h3>
                <div class="topten-info">
                    <?php $i=0;
                        foreach($this->top10package as $item) : 
                        $isFront2Item = false;
                        foreach($item->AdvIdeaList->AdvIdea as $idea) {
                            if($idea->Id==185) {
                                $isFront2Item = true;
                            }
                        } 
                        if($isFront2Item) :
                        if($i>9) break; $i++;
                    ?>
                    <div class="topten-item <?php echo 'top10item-'.$i; ?>" onclick="location.href='<?php echo JRoute::_('index.php?option=com_stravel&view=packagehome&city='.$item->CityList->City->CityCode.'&cityname='.$item->CityList->City->CityName) ?>';">
                        <div class="pull-left name"><?php echo $item->CityList->City->CityName ?></div>
                        <div class="pull-left time"><?php echo $item->Abstract ?><br /><?php echo sprintf(JText::_('COM_STRAVEL_PACKAGE_DAYNIGHT_VARRIBLE'),$item->Days,$item->Nights) ?></div>
                        <div class="pull-left cx"><?php if($item->CarrierList->Carrier->CarrierCode != '--') : ?><img class="img-responsive" src="http://www.westminstertravel.com/hk/Airfare/al/<?php echo $item->CarrierList->Carrier->CarrierCode ?>.png" alt="transporter logo"/><?php endif; ?><?php echo $item->CarrierList->Carrier->CarrierCode ?></div>
                        <div class="pull-left price">$<?php echo $item->Price; if($item->PriceInd=='Fr') echo '+'; ?></div>
                    </div>
                    <?php endif;endforeach; ?>
                </div>
            </div>
            <?php endif; ?>            
        </div>
        <div class="col-md-4">
            <?php if(!empty($this->top10airmanual)) : ?>
                <div class="topten topten-airticket">
                    <h3><?php echo JText::_('COM_STRAVEL_TOPTEN_AIRTICKET') ?></h3>
                    <div class="topten-info">
                        <?php $i=1; foreach($this->top10airmanual as $item) : 
                            $info = explode("\n",$item->Content);
                        ?>
                        <div class="topten-item <?php echo 'top10item-'.$i; ?>" onclick="location.href='<?php echo JRoute::_('index.php?option=com_stravel&view=airtickethome') ?>';">
                            <div class="pull-left name"><?php echo empty($info[0])?'--':$info[0] ?></div>
                            <div class="pull-left cx"><img class="img-responsive" src="http://www.westminstertravel.com/hk/Airfare/al/<?php echo $info[3] ?>.png" alt="transporter logo"/><?php echo $info[3] ?></div>
                            <div class="pull-left time"><?php echo $info[1] ?></div>
                            <div class="pull-left price">$<?php echo $info[2] ?></div>
                        </div>
                        <?php if($i>9) {break;}else {$i++;} endforeach; ?>
                    </div>
                </div>        
            <?php else : ?>
                <?php if(!empty($this->top10air)) : ?>
                <div class="topten topten-airticket">
                    <h3><?php echo JText::_('COM_STRAVEL_TOPTEN_AIRTICKET') ?></h3>
                    <div class="topten-info">
                        <?php $i=1; foreach($this->top10air as $item) : ?>
                        <div class="topten-item <?php echo 'top10item-'.$i; ?>" onclick="location.href='<?php echo JRoute::_('index.php?option=com_stravel&view=airtickethome') ?>';">
                            <div class="pull-left name"><?php echo empty($item->DstName)?'&nbsp;':$item->DstName ?></div>
                            <div class="pull-left cx"><img class="img-responsive" src="http://www.westminstertravel.com/hk/Airfare/al/<?php echo $item->Airline ?>.png" alt="transporter logo"/><?php echo $item->Airline ?></div>
                            <div class="pull-left time"><?php echo $item->AirlineName ?></div>
                            <div class="pull-left price">$<?php echo (int) $item->Adult ?>+</div>
                        </div>
                        <?php if($i>9) {break;}else {$i++;} endforeach; ?>
                    </div>
                </div>
            <?php endif;endif; ?>
        </div>
        <div class="col-md-4">
            <?php if(!empty($this->top10hotel)) : ?>
            <div class="topten topten-hotel">
                <h3><?php echo JText::_('COM_STRAVEL_TOPTEN_HOTEL') ?></h3>
                <div class="topten-info">
                    <?php 
                        $i=0; 
                        foreach($this->top10hotel as $item) :
                        if($i>=10) {break;}
                        $top10 = false;
                        foreach($item->AdvIdeaList->AdvIdea as $preference) {
                            if($preference->Id==191) {
                                $top10 = true;
                            }
                        }
                        if($top10) :
                        $i++;
                    ?>
                    <div class="topten-item <?php echo 'top10item-'.$i; ?>" onclick="location.href='<?php echo JRoute::_('index.php?option=com_stravel&view=hotelhome') ?>';">
                        <div class="pull-left name"><?php echo $item->CityName ?></div>
                        <div class="pull-left time"><?php echo empty($item->HotelName)?'--':$item->HotelName; ?></div>
                        <div class="pull-left price">$<?php echo (int) $item->Price ?>+</div>
                    </div>
                    <?php endif;endforeach; ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <!-- End top 10 -->
</div>
<!-- Extra view more for top 10 -->
<div class="container">
    <div  class="row">
        <div class="col-md-12">
            <div class="viewmore text-center"><a href="#" onclick="return false;"><?php echo JText::_('COM_STRAVEL_MORE') ?></a></div>
        </div>
    </div>
</div>
<!-- End extra view more for top 10 -->

<?php  ?>
<?php 
    if(!empty($this->packagebanner)) :
?>
<!-- full width section -->
<div class="middletext">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div id="packagebannertext" class="carousel carousel-fade slide">
                    <ol class="carousel-indicators">
                        <?php
                            $i = 0; 
                            foreach($this->packagebanner as $banner) :
                            if($i>2) break;
                            if($i==0) { 
                                $cls = 'active';
                            }else{
                                $cls = 'sn';
                            }
                        ?>
                        <li data-target="#packagebannertext" data-slide-to="<?php echo $i ?>" class="<?php echo $cls ?>"></li>
                        <?php $i++;endforeach; ?>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <?php
                            $i = 0; 
                            foreach($this->packagebanner as $banner) :
                            if($i>2) break;
                            $infos = explode("\n",$banner->Content);
                            $url = array_shift($infos);
                            $text = array_shift($infos);
                            if($i==0) { 
                                $cls = 'active';
                            }else{
                                $cls = '';
                            }
                            $i++;
                        ?>
                        <div class="item <?php echo $cls ?>">            
                        <div class="text"><?php echo $text ?></div>
                        <div class="buttonst"><a class="btn" href="<?php echo $url ?>"><?php echo JText::_('COM_STRAVEL_MORE_DETAIL') ?></a></div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End full width section -->
<?php endif; ?>

<!--buy discount --> 
<div class="container discount">
    <div class="row enmasse_deal">
        <div class="col-md-2">
            <h3><?php echo JText::_('COM_STRAVEL_DEAL_FEATURED') ?></h3>
            <p><?php echo JText::_('COM_STRAVEL_DEAL_FEATURE_DESC') ?></p>
        </div>
        <div class="col-md-10">
            <div class="row">
                <?php foreach($this->latestBlogPosts as $post) : ?>
                        <div class="col-md-4">
                            <div class="discount-item">
                                <div class="discount-item-image">
                                    <a href="<?php echo $post['permalink']; ?>">
                                    <img class="maxwidth img-responsive lazy" data-original="<?php echo $post['Image'][0] ?>" alt="<?php echo $post['post_title'] ?>" />
        							</a>
                                </div>
                                <div class="discount-item-info">
                                    <h5><a href="<?php echo $post['permalink'];?>"><?php echo $post['post_title'] ?></a></h5>
                                    <div class="price text-right hide"><a href="<?php echo $post['permalink'];?>"><?php echo JText::_('COM_STRAVEL_MORE') ?></a></div>
                                </div>
                            </div>
                            
                        </div>
                <?php endforeach; ?>
				<?php foreach($this->front_deal as $deal) : ?>
                <div class="col-md-4 hide">
                    <div class="discount-item">
                        <div class="discount-item-image">
							<?php 
								//contruct deal image url 
								$imageUrlArr = array();
								if(EnmasseHelper::is_urlEncoded($deal->pic_dir)){
									$imageUrlArr = unserialize(urldecode($deal->pic_dir));
								}else{
									$imageUrlArr[0] = $deal->pic_dir;
								}
								$imageUrl = JURI::root().str_replace("\\","/",$imageUrlArr[0]);
								$link = 'index.php?option=com_enmasse&controller=deal&task=view&id=' . $deal->id ."&slug_name=" .$deal->slug_name ."&Itemid=$nItemId";
							?>
							<a href="<?php echo JRoute::_($link);?>">
                            <img class="maxwidth img-responsive lazy" data-original="<?php echo $imageUrl ?>" alt="<?php echo $deal->name ?>" />
							</a>
                        </div>
                        <div class="discount-item-info">
                            <h5><?php echo $deal->name ?></h5>
                            <h6><?php echo apiHelper::__wordProcess($deal->subTitle) ?></h6>
                            <div class="price text-right"><a href="<?php echo JRoute::_($link);?>">$<?php echo (int) $deal->price ?></a></div>
                        </div>
                    </div>
                </div>						
				<?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <h3><?php echo JText::_('COM_STRAVEL_TODAY_PROMOTION') ?></h3>
            <p><?php echo JText::_('COM_STRAVEL_TODAY_PROMOTION_EXP') ?></p>
        </div>
        <div class="col-md-10">
            <div class="row">
                <?php 
                    $i = 0;
                    foreach($this->top10package as $item) : 
                    $isFront2Item = false;
                    foreach($item->AdvIdeaList->AdvIdea as $idea) {
                        if($idea->Id==184) {
                            $isFront2Item = true;
                        }
                    } 
                    if($isFront2Item) :
                    if($i>=3) break; $i++;
                    $imgurl = 'images/Taipei7.jpg';
                    if(empty($item->ImgUrls->string)) {
                        if(!empty($item->CityList->City->ImgUrls->string)) {
                            $imgurl = $item->CityList->City->ImgUrls->string;
                        }
                    }else{
                        $imgurl = $item->ImgUrls->string;
                    }
                ?>
                <div data-id="<?php echo $item->Id ?>" class="col-md-4">
                    <div class="discount-item">
                        <div class="discount-item-image">
                            <a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packagehome&city='.$item->CityList->City->CityCode.'&cityname='.$item->CityList->City->CityName); ?>">
                            <img class="maxwidth img-responsive lazy" data-original="<?php echo $imgurl; ?>" alt="today recommend" />
                            </a>
                            <?php if(!empty($item->AdvIdeaList->AdvIdea)) : ?>
                                <div class="advidea">
                                    <?php
                                        $jk = 0; 
                                        foreach($item->AdvIdeaList->AdvIdea as $idea) {
                                            if($jk>=2) break;
                                            if($idea->Id==197) {
                                               echo '<span class="ideaicon">'.$idea->Content.'</span>'; 
                                               $jk++; 
                                            }
                                        }                                                           
                                    ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="discount-item-info">
                            <h5><?php echo apiHelper::__wordProcess($item->Abstract) ?></h5>
                            <h6><?php echo sprintf(JText::_('COM_STRAVEL_REQUIRED_TO_VAR'),date('m',strtotime($item->SalesPeriodTo)),date('d',strtotime($item->SalesPeriodTo))) ?></h6>
                            <div class="price text-right"><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packagehome&city='.$item->CityList->City->CityCode.'&cityname='.$item->CityList->City->CityName); ?>">$<?php echo $item->Price; ?>+</a></div>
                        </div>
                    </div>
                </div>
                <?php endif;endforeach; ?>  
            </div>
        </div>
    </div>
</div>
<!-- end buy discount -->