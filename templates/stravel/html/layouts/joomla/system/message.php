<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Template.Isis
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$msgList = $displayData['msgList'];
if (is_array($msgList) && $msgList) {
	foreach ($msgList as $type => $msgs) {
		if($type=='warning') {
			foreach ($msgs as $key=>$msg) {
				if($msg=="載入元件錯誤：、找不到元件" || $msg=="Error loading component: , Component not found") {
					unset($msgList['warning'][$key]);
				}
			}
		}
	}
	if(isset($msgList['warning']) && count($msgList['warning'])==0) {
		unset($msgList['warning']);
	}
}
$alert = array('error' => 'alert-danger', 'warning' => 'alert-warning', 'notice' => 'alert-info', 'message' => 'alert-success');
?>
<div id="system-message-container">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php if (is_array($msgList) && $msgList) : ?>
            		<?php foreach ($msgList as $type => $msgs) : ?>
            			<div class="alert <?php echo $alert[$type]; ?>" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
            				<h4 class="alert-heading"><?php echo JText::_($type); ?></h4>
            				<?php if ($msgs) : ?>
            					<?php foreach ($msgs as $msg) : ?>
            						<p><?php echo $msg; ?></p>
            					<?php endforeach; ?>
            				<?php endif; ?>
            			</div>
            		<?php endforeach; ?>
            	<?php endif; ?>            
            </div>
        </div>
    </div>
</div>
