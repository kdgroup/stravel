var stravel = {
    missing_name_en_us : "Please enter your name.",
    missing_name_zh_tw : "請輸入你的名字.",
    missing_name_zh_cn : "請輸入你的名字.",
    
    missing_email_en_us : "Please enter your email.",
    missing_email_zh_tw : "請輸入正確電郵.",
    missing_email_zh_cn : "請輸入正確電郵.",
    
    invalid_email_en_us : "Please enter a valid email.",
    invalid_email_zh_tw : "請輸入正確電郵.",
    invalid_email_zh_cn : "請輸入正確電郵.",
    
    default_error_en_us : "An error has happened, please try again!",
    default_error_zh_tw : "An error has happened, please try again!",
    default_error_zh_cn : "An error has happened, please try again!",
    
    no_city_found_en    : "No city found.",
    no_city_found_zh_tw : "No city found.",
    no_city_found_zh_cn : "No city found.",
    
    hotel_address_label_zh_tw: "地址:",
    hotel_address_label_zh_cn: "地址:",
    hotel_address_label_en_us: "Address:",
    
    hotcity_text_en:"Hot cities",
    hotcity_text_zh_tw:"熱門城市",
    hotcity_text_zh_cn:"熱門城市",
    
    package_name_label_en : "Package name: ",
    package_name_label_zh_tw : "旅遊套票: ",
    package_name_label_zh_cn : "旅遊套票: ",
    package_date_label_en : "Departure date: ",
    package_date_label_zh_tw : "出發日期: ",
    package_date_label_zh_cn : "出發日期: ",
    package_nights_label_en : "Number of nights: ",
    package_nights_label_zh_tw : "晚數: ",
    package_nights_label_zh_cn : "晚數: ",
    package_price_label_en : "Price: ",
    package_price_label_zh_tw : "價格(起)#: ",
    package_price_label_zh_cn : "價格(起)#: ",
    package_remark_label_en : "# All prices are in Hong Kong dollars and subject to change without notice.",
    package_remark_label_zh_tw : "# 所有價格均以港幣為單位及如有更新，恕不另行通知。",
    package_remark_label_zh_cn : "# 所有價格均以港幣為單位及如有更新，恕不另行通知。",
    
    air_name_label_en : "Fare: ",
    air_name_label_zh_tw : "機票資料: ",
    air_name_label_zh_cn : "機票資料: ",
    air_class_label_en : "Class: ",
    air_class_label_zh_tw : "客艙等級: ",
    air_class_label_zh_cn : "客艙等級: ",
    air_trip_label_en : "Trip: ",
    air_trip_label_zh_tw : "航程: ",
    air_trip_label_zh_cn : "航程: ",
    
    hotel_dest_label_en : 'Destination: ',
    hotel_dest_label_zh_tw : '目的地: ',
    hotel_dest_label_zh_cn : '目的地: ',
    hotel_name_label_en : 'Hotel Name: ',
    hotel_name_label_zh_tw : '酒店名稱: ',
    hotel_name_label_zh_cn : '酒店名稱: ',
    hotel_checkin_label_en : 'Check-in: ',
    hotel_checkin_label_zh_tw : '入住日期: ',
    hotel_checkin_label_zh_cn : '入住日期: ',
    
    cruise_name_label_en : 'Cruise Name: ',
    cruise_name_label_zh_tw : '郵輪套票: ',
    cruise_name_label_zh_cn : '郵輪套票: ',
    cruise_shipname_label_en : 'Vessel Name: ',
    cruise_shipname_label_zh_tw : '郵輪名稱: ',
    cruise_shipname_label_zh_cn : '郵輪名稱: ',
    
    staffnumber_empty_en : '職員編號: 填寫内容不全!',
    staffnumber_empty_zh_tw : '職員編號: 填寫内容不全!',
    staffnumber_empty_zh_cn : '職員編號: 填寫内容不全!'
    
};
var conditions = new Object();
var sortby = { direction:1 };
var totalitems;
jQuery(document).ready(function() {
    jQuery('[data-toggle="tooltip"]').tooltip({
        delay: { "show": 100, "hide": 2000 }
    });
    //equalheight for elements
    equalHeightElement(jQuery('.discount-item-image .maxwidth'));
    //slideshow center
    centerSlideshow();
    //package submenu hover
    jQuery('.mainnav .item-161').hover(function() {
        jQuery(this).find('.packagesubmenu').stop().show();
    },function() {
        jQuery(this).find('.packagesubmenu').stop().animate({opacity:1},2000,'swing',function() {
            jQuery(this).hide();
        });
    });
    //Hotelname filter
    jQuery('.hotelnamefilter').on('click','.dropdown li',function() {
        jQuery(this).parent().parent().find('input').val(jQuery(this).attr('data-name'));
        conditions.hotelcode = jQuery(this).attr('data-id');
        processFilterData();
    });
    jQuery('.hotelnamefilter').on('keyup','#hotelName',function(e) {
        e.preventDefault();
        var thisel = jQuery(this);
        var keyword = thisel.val();
        var keystrokecode = e.which;
        if(keystrokecode==13) {
            if(keyword.length>0) {
                keyword = keyword.toString().toLowerCase();
                var gotitem = false;
                thisel.next().find('li').each(function() {
                    if(gotitem==true) {
                        return false;
                    }
                    var dataname = jQuery(this).attr('data-name');
                    var hotelcode = jQuery(this).attr('data-id');
                    var thisdata = dataname.toString().toLowerCase();
                    var searchresult = thisdata.search(keyword);
                    if(searchresult>=0) {
                        gotitem = true;
                        jQuery(this).parent().prev().val(dataname);
                        conditions.hotelcode = hotelcode;
                        processFilterData();
                        thisel.dropdown("toggle");   
                    }           
                });    
            }    
        }else{
            if(keyword.length>0) {
                keyword = keyword.toString().toLowerCase();
                jQuery('.hotelnamefilter .dropdown li').hide();
                count = 0;            
                jQuery('.hotelnamefilter .dropdown li').each(function() {
                    if(count>14) {return false;}
                    var dataname = jQuery(this).attr('data-name');
                    var datacode = jQuery(this).attr('data-id');
                    var thisdata = dataname.toString().toLowerCase();
                    var searchresult = thisdata.search(keyword);
                    if(searchresult>=0) {
                        var replacetxt = dataname.substr(searchresult,keyword.length); 
                        thisdata = dataname.replace(replacetxt,'<span class="changecolor">'+replacetxt+'</span>');
                        jQuery(this).html('<a href="#" onclick="return false;">'+thisdata+'</a>');
                        jQuery(this).show();
                        count += 1;
                    }else{
                        jQuery(this).hide();
                    }
                });
            }else{
                delete conditions.hotelcode;
                processFilterData();
            }
        }        
    });
    //loading modal
    jQuery('.search').on('click','.searchbtn',function() {
        jQuery('#loadingModal').modal({
            keyboard: false,
            backdrop: 'static'
        });   
    });
    jQuery('#abovefooter ul.menu li,.footermenu,.linklist').on('click','a',function() {
        if(jQuery(this).attr('data-toggle')!='modal') {
            jQuery('#loadingModal').modal({
                keyboard: false,
                backdrop: 'static'
            });    
        }   
    });
    //init filterable items
    totalitems = getTotalItems();    
    //pagination click
    jQuery('.pagination').on('click','li',function(e) {
        e.preventDefault();
        if(jQuery(this).hasClass('disabled')) {
            return false;
        }
        jQuery('.pagination li').removeClass('disabled');
        var currentpage = jQuery(this).attr('data-currentpage');
        var totalpage = jQuery(this).attr('data-totalpage');
        var thispage = jQuery(this).attr('data-page');
        if(thispage=='next' || thispage=='prev') {
            if(thispage=='next') {
                thispage = parseInt(currentpage)+1;
                if(thispage>=totalpage) {
                    thispage = totalpage;
                    jQuery(this).addClass('disabled');
                }
            }
            if(thispage=='prev') {
                thispage = parseInt(currentpage)-1;
                if(thispage<=1) {
                    thispage = 1;
                    jQuery(this).addClass('disabled');
                }
            }
            jQuery('.pagsseitem'+thispage).addClass('disabled');
        }else{
            jQuery(this).addClass('disabled');    
        }
        jQuery('.pagination li').attr('data-currentpage',thispage);
        thispage = parseInt(thispage);
        currentpage = parseInt(currentpage);
        totalpage = parseInt(totalpage);
        jQuery('.pagination li.pagsseitem').each(function() {
                if(thispage>2 && thispage<(totalpage-1)) {
                    if(jQuery(this).attr('data-page')>=(thispage-2) && jQuery(this).attr('data-page')<=(thispage+2)) {
                        jQuery(this).addClass('showed');
                    }else{
                        jQuery(this).removeClass('showed');
                    }
                }
        });       
        jQuery('.page-sep').hide();
        jQuery('.page-sep'+thispage).fadeIn('slow');
    });
    //Call function equaheight
    equalHeight();
    //Top10item
    jQuery('.front .viewmore a').click(function() {
        jQuery(this).hide();
        jQuery('.front .topten .topten-info').toggleClass('moreitems');    
    });
    //Hover slideshow item
    /*
    jQuery('#stravel-slideshow .item').hover(function() {
        jQuery(this).addClass('hover');
    },function() {
        jQuery(this).removeClass('hover');
    }); 
    */ 
    //Select theme package
    jQuery('.themepackage').on('click','ul li',function() {
        var parent = jQuery(this).parentsUntil('.form-group');
        var name = jQuery(this).find('a').html();
        parent.find('span').html(name);
        var id = jQuery(this).attr('data-id');
        var iname = jQuery(this).attr('data-name');
        parent.find('input[name="ideaid"]').val(id);        
        parent.find('input[name="ideaname"]').val(iname);        
    });
        
    jQuery('.sortbyprice').click(function() {
        var direction = jQuery(this).attr('data-direction');
        var curdirection = jQuery('.fakesortvalue').attr('data-direction');
        
        if(direction==curdirection) {
            return;
        }else{            
            switch(parseInt(direction)) {
                case 1:
                    jQuery('.fakesortvalue').attr('data-direction',1).html(jQuery(this).html());
                    break;
                case 2:
                    jQuery('.fakesortvalue').attr('data-direction',2).html(jQuery(this).html());
                    break;
                case 3:
                    jQuery('.fakesortvalue').attr('data-direction',3).html(jQuery(this).html());
                    break;
                default:
                    return;
                    break;    
            }
        }
        sortby.direction = parseInt(direction);
        processFilterData();              
    });
    jQuery('.listcontainer').on('mouseenter','.product-container .product-item',function() {
        jQuery(this).find('.product-image').addClass('hover');
    });
    jQuery('.listcontainer').on('mouseleave','.product-container .product-item',function() {
        jQuery(this).find('.product-image').removeClass('hover');
    });
    //datepicker 
    var lang = jQuery('html').attr('lang');
    var langopt = new Array();
    langopt['en-gb'] = { 
        dateFormat: "yy-mm-dd", 
        minDate:0,
        onClose: function(d,o) {
            onCloseDatePicker(d,o)
        } 
    };
    langopt['zh-tw'] = {
        minDate:0,
		closeText: '關閉',
		prevText: '&#x3C;上月',
		nextText: '下月&#x3E;',
		currentText: '今天',
		monthNames: ['一月','二月','三月','四月','五月','六月',
		'七月','八月','九月','十月','十一月','十二月'],
		monthNamesShort: ['一月','二月','三月','四月','五月','六月',
		'七月','八月','九月','十月','十一月','十二月'],
		dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],
		dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],
		dayNamesMin: ['日','一','二','三','四','五','六'],
		weekHeader: '周',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: true,
		yearSuffix: '年',
        onClose: function(d,o) {
            onCloseDatePicker(d,o)
        }
    };
    langopt['zh-cn'] = {
        minDate:0,
		closeText: '關閉',
		prevText: '&#x3C;上月',
		nextText: '下月&#x3E;',
		currentText: '今天',
		monthNames: ['一月','二月','三月','四月','五月','六月',
		'七月','八月','九月','十月','十一月','十二月'],
		monthNamesShort: ['一月','二月','三月','四月','五月','六月',
		'七月','八月','九月','十月','十一月','十二月'],
		dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],
		dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],
		dayNamesMin: ['日','一','二','三','四','五','六'],
		weekHeader: '周',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: true,
		yearSuffix: '年',
        onClose: function(d,o) {
            onCloseDatePicker(d,o)
        }
    };      
    jQuery('.datepicker').datepicker(langopt[lang]);
    
    //to top button
    jQuery('.iconToTop,.iconToTop2').click(function(){
		jQuery('html, body').animate({scrollTop : 0},1000);
		return false;
	});
    jQuery('label[data-toggle=\"tab\"]').on('shown.bs.tab', function(e) {
        jQuery(this).find('input').prop( 'checked', true );
    });
    
    //drag price
    var pfrom = parseFloat(jQuery('.product-container:first-child').attr('data-price'));
    var pto = parseFloat(jQuery('.product-container:first-child').attr('data-price'));
    if(isNaN(pfrom)) {
        pfrom = 0;
    }
    if(isNaN(pto)) {
        pto = 19999;
    }
    var itemprice;
    jQuery('.product-container').each(function() {
        itemprice = parseFloat(jQuery(this).attr('data-price'));
        if(itemprice<pfrom) {
            pfrom = itemprice;
        }
        if(itemprice>pto) {
            pto = itemprice;
        }
        
    });
    if(pfrom==pto) {
        pto = pto + 500;
    }
    jQuery('#greendragprice').slider({
        from: pfrom,
        to: pto,
        format: { format: '$#', locale: 'cn' },
        onstatechange: function(value) {
            var price = value.split(';');
            jQuery('#pricefrom').html('$'+price[0]);
            jQuery('#priceto').html('$'+price[1]);
        },
        callback : function (value) {
            var price = value.split(';');
            jQuery('#pricefrom').html('$'+price[0]);
            jQuery('#priceto').html('$'+price[1]);
            conditions.pricefrom = price[0];
            conditions.priceto = price[1];
            processFilterData();
        },
        limits: false
    });
    
    // via selection
    jQuery('.viaselect input').on('click',function() {
        var stop = 0;
        jQuery('.viaselect input').each(function() {
            if(jQuery(this).attr('id')=='directflight') {
                if(jQuery(this).is(':checked')) {
                    stop += 1;    
                }                    
            }
            if(jQuery(this).attr('id')=='onestop') {
                if(jQuery(this).is(':checked')) {
                    stop += 2;    
                }                    
            }
        });
        conditions.stop = stop;
        processFilterData();
    });
    
    //Airline selection
    jQuery('.airlineselect input').on('click',function() {
        var airlineArray = [];
        jQuery('.airlineselect input').each(function() {
            if(jQuery(this).is(':checked')) {
                airlineArray.push(jQuery(this).attr('data-id'));
            }
        });
        conditions.airline = airlineArray;
        processFilterData();
    });
    
    //star filer selection
    jQuery('.starfilter input').on('click',function() {
        var cruiseArray = [];
        jQuery('.starfilter input').each(function() {
            if(jQuery(this).is(':checked')) {
                cruiseArray.push(jQuery(this).attr('data-id'));
            }
        });
        conditions.hotelstar = cruiseArray;
        processFilterData();
    });
    
    //hotel area selection
    jQuery('.areaselection ul li a').click(function() {
        jQuery('.areaselection span.fakeselect').html(jQuery(this).html());
        var cruiselineid = jQuery(this).attr('data-id');
        conditions.hotelarea = cruiselineid;
        processFilterData();       
    });
    
    //cruise line selection
    jQuery('#cruisehome .cruisedestinationselect ul li a').click(function() {
        jQuery('#cruisehome .cruisedestinationselect span.fakeselect').html(jQuery(this).html());
        var cruiselineid = parseInt(jQuery(this).attr('data-id'));
        jQuery('.product-container').fadeOut();
        jQuery('.product-container').each(function() {
            if(parseInt(jQuery(this).attr('data-destinationid')) == cruiselineid) {
                jQuery(this).fadeIn('slow');
            }        
        });        
    });
    //cruise line selection
    jQuery('#cruisehome .cruiselineselect ul li a').click(function() {
        jQuery('#cruisehome .cruiselineselect span.fakeselect').html(jQuery(this).html());
        var cruiselineid = parseInt(jQuery(this).attr('data-id'));
        jQuery('.product-container').fadeOut('slow');
        jQuery('.product-container').each(function() {
            if(parseInt(jQuery(this).attr('data-cruiselineid')) == cruiselineid) {
                jQuery(this).fadeIn();
            }        
        });        
    });
    //cruise vessel selection
    jQuery('#cruisehome .cruisevesselselect ul li a').click(function() {
        jQuery('#cruisehome .cruisevesselselect span.fakeselect').html(jQuery(this).html());
        var cruiselineid = parseInt(jQuery(this).attr('data-id'));
        jQuery('.product-container').fadeOut('slow');
        jQuery('.product-container').each(function() {
            if(parseInt(jQuery(this).attr('data-cruisevesselid')) == cruiselineid) {
                jQuery(this).fadeIn();
            }        
        });        
    });
    //cruise year selection
    jQuery('#cruisehome .cruiseyearselect ul li a').click(function() {
        jQuery('#cruisehome .cruiseyearselect span.fakeselect').html(jQuery(this).html());
        var cruiselineid = parseInt(jQuery(this).attr('data-id'));
        jQuery('.product-container').fadeOut('slow');
        jQuery('.product-container').each(function() {
            if(parseInt(jQuery(this).attr('data-year')) == cruiselineid) {
                jQuery(this).fadeIn();
            }        
        });        
    });
    //cruise month selection
    jQuery('#cruisehome .cruisemonthselect ul li a').click(function() {
        jQuery('#cruisehome .cruisemonthselect span.fakeselect').html(jQuery(this).html());
        var cruiselineid = parseInt(jQuery(this).attr('data-id'));
        jQuery('.product-container').fadeOut('slow');
        jQuery('.product-container').each(function() {
            if(parseInt(jQuery(this).attr('data-month')) == cruiselineid) {
                jQuery(this).fadeIn();
            }        
        });        
    });
    //cruise nights selection
    jQuery('#cruisehome .cruisenightsselect ul li a').click(function() {
        jQuery('#cruisehome .cruisenightsselect span.fakeselect').html(jQuery(this).html());
        var cruiselineid = parseInt(jQuery(this).attr('data-id'));
        jQuery('.product-container').fadeOut('slow');
        jQuery('.product-container').each(function() {
            var night = parseInt(jQuery(this).attr('data-nights'));
            switch(cruiselineid) {
                case 1:
                    if(night<=6) {
                        jQuery(this).fadeIn();
                    }
                    break;
                case 2:
                    if(night>6 && night<10) {
                        jQuery(this).fadeIn();
                    }
                    break;
                case 3:
                    if(night>9 && night<15) {
                        jQuery(this).fadeIn();
                    }
                    break;
                case 4:
                    if(night>14 && night<26) {
                        jQuery(this).fadeIn();
                    }
                    break;
                case 5:
                    if(night>=26) {
                        jQuery(this).fadeIn();
                    }
                    break;
                default:
                    jQuery(this).fadeIn();
                    break;
            }      
        });        
    });
    
    //Script for flight detail
    jQuery('#myLargeAirTicketModal').on('show.bs.modal',function(e) {
        var pid = jQuery(e.relatedTarget).attr('data-productid');
        jQuery('#myLargeAirTicketModal .showtermbtn').attr('data-productid',pid);
        var air = jQuery(e.relatedTarget).attr('data-airline');
        jQuery('#myLargeAirTicketModal .showtermbtn').attr('data-airline',air);
        var remark = jQuery(e.relatedTarget).attr('data-remark');
        jQuery('#myLargeAirTicketModal .remarkcontent').html(remark);    
    });
    jQuery('#myLargeAirTicketModal .showtermbtn').click(function() {
        var id = jQuery(this).attr('data-productid');
        var airline = jQuery(this).attr('data-airline');
        jQuery('.timeline-list').html('');
        jQuery.post('index.php?option=com_stravel&task=getfareterms',{'detailid':id},function(response) {
            var obj = jQuery.parseJSON(response);
            if(obj.data.TermsList.Terms.length>0) {
                jQuery.each(obj.data.TermsList.Terms,function(i,e) {
                    jQuery('.timeline-list').append('<div class="term-item"><h6>'+e.category+'</h6><p>'+e.content+'</p></div>');
                });
            }        
        });
    });
    
    //Script for hotel detail
    jQuery('#myLargeHotelResultModal').on('show.bs.modal',function(e) {
        var pid = jQuery(e.relatedTarget).attr('data-hotelcode');
        jQuery.post('index.php?option=com_stravel&task=gethotelinfo',{'hotelcode':pid},function(response) {
            response = response.replace(/\@attributes/g,'attributes');
            response = response.replace(/\"0\"/g,'"zeroattribute"');
            var obj = jQuery.parseJSON(response);
            var phtml = '<div class="row">';
            jQuery('#myLargeHotelResultModal .loading').hide();
            if(typeof obj.data.MultimediaDescriptions.MultimediaDescription.ImageItems.ImageItem != "undefined") {
                phtml +=  '<div class="col-md-4">';
                jQuery.each(obj.data.MultimediaDescriptions.MultimediaDescription.ImageItems.ImageItem,function(i,e) {
                    phtml+=  '<div><img class="img-responsive" src="'+e.ImageFormat.URL+'" alt="'+e.ImageFormat.attributes.Title+'"/></div>'    
                });
                phtml   +=  '</div>';  
            }
            phtml   += '<div class="col-md-8">';
            var starhtml = '<div class="hotelstar">';
            switch(parseInt(obj.data.AffiliationInfo.Awards.Award.attributes.Rating)) {
                case 1:
                    starhtml += '<i class="fa fa-star"></i>';
                    break;
                case 2:
                    starhtml += '<i class="fa fa-star"></i><i class="fa fa-star"></i>';
                    break;
                case 3:
                    starhtml += '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
                    break;
                case 4:
                    starhtml += '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
                    break;
                case 5:
                    starhtml += '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
                    break;
                default:
                    starhtml += '&nbsp';
                    break;
            }
            starhtml += '</div>'; 
            switch(obj.lang) {
                case 'en-US':
                    phtml   +=  '<div class="hotelname"><h3>'+obj.data.HotelInfo.HotelName+starhtml+'<h3></div>';
                    var htaddress = obj.data.ContactInfos.ContactInfo.Addresses.Address.AddressLine.join(', ');
                    phtml   +=  '<div class="hoteladdress">'+stravel.hotel_address_label_zh_tw+htaddress+'</div>';
                    if(typeof obj.data.ContactInfos.ContactInfo.Phones.zeroattribute != 'undefined') {
                    phtml   +=  '<div class="hotelphone">'+obj.data.ContactInfos.ContactInfo.Phones.zeroattribute.attributes.PhoneNumber+'</div>';
                    }
                    phtml    +=  '<div class="hotelarea">'+obj.data.TPA_Extensions.District+'</div>';
                    if(typeof obj.data.ContactInfos.ContactInfo.URLs != 'undefined') {
                    phtml   +=  '<div class="hotelwebsite"><a href="'+obj.data.ContactInfos.ContactInfo.URLs.URL+'">'+obj.data.ContactInfos.ContactInfo.URLs.URL+'</a></div>';
                    }
                    jQuery.each(obj.data.MultimediaDescriptions.MultimediaDescription.TextItems.TextItem,function(i,e) {
                        phtml   +=  '<div class="hoteldesc"><h4>'+e.attributes.Title+'</h4><p>'+e.Description+'<p></div>';
                    });
                    break;
                case 'zh-TW':
                    phtml   +=  '<div class="hotelname"><h3>'+obj.data.TPA_Extensions.HotelNameZhTw+starhtml+'<h3></div>';
                    phtml   +=  '<div class="hoteladdress">'+stravel.hotel_address_label_zh_tw+obj.data.TPA_Extensions.HotelAddressZhTw.AddressLine+'</div>';
                    if(typeof obj.data.ContactInfos.ContactInfo.Phones.zeroattribute != 'undefined') {
                    phtml   +=  '<div class="hotelphone">'+obj.data.ContactInfos.ContactInfo.Phones.zeroattribute.attributes.PhoneNumber+'</div>';
                    }
                    phtml    +=  '<div class="hotelarea">'+obj.data.TPA_Extensions.District+'</div>';
                    if(typeof obj.data.ContactInfos.ContactInfo.URLs != 'undefined') {
                    phtml   +=  '<div class="hotelwebsite"><a href="'+obj.data.ContactInfos.ContactInfo.URLs.URL+'">'+obj.data.ContactInfos.ContactInfo.URLs.URL+'</a></div>';
                    }
                    if(typeof obj.data.MultimediaDescriptions.MultimediaDescription.TextItems.TextItem != 'undefined') {
                        jQuery.each(obj.data.MultimediaDescriptions.MultimediaDescription.TextItems.TextItem,function(i,e) {
                            if(typeof e.Title != 'undefined' && typeof Description != 'undefined') {
                            phtml   +=  '<div class="hoteldesc"><h4>'+e.attributes.Title+'</h4><p>'+e.Description+'<p></div>';
                            }
                        });
                    }
                    break;
                case 'zh-CN':
                    phtml   +=  '<div class="hotelname"><h3>'+obj.data.TPA_Extensions.HotelNameZhCn+starhtml+'<h3></div>';
                    phtml   +=  '<div class="hoteladdress">'+stravel.hotel_address_label_zh_cn+obj.data.TPA_Extensions.HotelAddressZhCn.AddressLine+'</div>';
                    if(typeof obj.data.ContactInfos.ContactInfo.Phones.zeroattribute != 'undefined') {
                    phtml   +=  '<div class="hotelphone">'+obj.data.ContactInfos.ContactInfo.Phones.zeroattribute.attributes.PhoneNumber+'</div>';
                    }
                    phtml    +=  '<div class="hotelarea">'+obj.data.TPA_Extensions.District+'</div>';
                    if(typeof obj.data.ContactInfos.ContactInfo.URLs != 'undefined') {
                    phtml   +=  '<div class="hotelwebsite"><a href="'+obj.data.ContactInfos.ContactInfo.URLs.URL+'">'+obj.data.ContactInfos.ContactInfo.URLs.URL+'</a></div>';
                    }
                    jQuery.each(obj.data.MultimediaDescriptions.MultimediaDescription.TextItems.TextItem,function(i,e) {
                        phtml   +=  '<div class="hoteldesc"><h4>'+e.attributes.Title+'</h4><p>'+e.Description+'<p></div>';
                    });
                    break;
                default:
                    phtml   +=  '<div class="hotelname"><h3>'+obj.data.TPA_Extensions.HotelNameZhTw+starhtml+'<h3></div>';
                    phtml   +=  '<div class="hoteladdress">'+stravel.hotel_address_label_zh_tw+obj.data.TPA_Extensions.HotelAddressZhTw.AddressLine+'</div>';
                    if(typeof obj.data.ContactInfos.ContactInfo.Phones.zeroattribute != 'undefined') {
                    phtml   +=  '<div class="hotelphone">'+obj.data.ContactInfos.ContactInfo.Phones.zeroattribute.attributes.PhoneNumber+'</div>';
                    }
                    phtml    +=  '<div class="hotelarea">'+obj.data.TPA_Extensions.District+'</div>';
                    if(typeof obj.data.ContactInfos.ContactInfo.URLs != 'undefined') {
                    phtml   +=  '<div class="hotelwebsite"><a href="'+obj.data.ContactInfos.ContactInfo.URLs.URL+'">'+obj.data.ContactInfos.ContactInfo.URLs.URL+'</a></div>';
                    }
                    jQuery.each(obj.data.MultimediaDescriptions.MultimediaDescription.TextItems.TextItem,function(i,e) {
                        phtml   +=  '<div class="hoteldesc"><h4>'+e.attributes.Title+'</h4><p>'+e.Description+'<p></div>';
                    });
                    break;
            }       
            phtml   +=  '</div>';
            phtml   +=  '</div>';
            jQuery('#myLargeHotelResultModal .hotelinfo').html(phtml); 
        });    
    });
    jQuery('#myLargeHotelResultModal').on('hide.bs.modal',function(e) {
        jQuery('#myLargeHotelResultModal .loading').show();
        jQuery('#myLargeHotelResultModal .hotelinfo').html('');           
    });
    
    // Select hotel nights
    jQuery('.hotelnights li a').click(function() {
        var night = jQuery(this).attr('data-id');
        var parent = jQuery(this).parent().parent().parent().parent();
        parent.find('input[name="nights"]').val(night);
        parent.find('span.fakeselect').html(night);
    });
    
    // Select hotel nights
    jQuery('#enquiryNewsletterModal .dropdown li a,#staffModal .dropdown li a').click(function() {
        var dataid = jQuery(this).attr('data-id');
        var parent = jQuery(this).parent().parent();
        parent.prev().html(jQuery(this).html());
        parent.next().val(dataid);
    });
    
    //Script to post data for package enquiry
    jQuery('#enquiryModal').on('show.bs.modal',function(e){
        jQuery('.productEnqueryInfo p').remove();
        var lang = jQuery('html').attr('lang');
        switch(lang) {
            case 'en-gb':
                var packagename = stravel.package_name_label_en;
                var packagedate = stravel.package_date_label_en;
                var packagenights = stravel.package_nights_label_en;
                var packageprice = stravel.package_price_label_en;
                var packageremark = stravel.package_remark_label_en;
                
                var airname = stravel.air_name_label_en;
                var airclass = stravel.air_class_label_en;
                var airtrip = stravel.air_trip_label_en;
                
                var hotelname = stravel.hotel_name_label_en;
                var hoteldst = stravel.hotel_dest_label_en;
                var hotelcheckin = stravel.hotel_checkin_label_en;
                
                var cruisename = stravel.cruise_name_label_en;
                var cruisevessel = stravel.cruise_shipname_label_en;
                break;
            case 'zh-cn':
                var packagename = stravel.package_name_label_zh_cn;
                var packagedate = stravel.package_date_label_zh_cn;
                var packagenights = stravel.package_nights_label_zh_cn;
                var packageprice = stravel.package_price_label_zh_cn;
                var packageremark = stravel.package_remark_label_zh_cn;
                
                var airname = stravel.air_name_label_zh_cn;
                var airclass = stravel.air_class_label_zh_cn;
                var airtrip = stravel.air_trip_label_zh_cn;
                
                var hotelname = stravel.hotel_name_label_zh_cn;
                var hoteldst = stravel.hotel_dest_label_zh_cn;
                var hotelcheckin = stravel.hotel_checkin_label_zh_cn;
                
                var cruisename = stravel.cruise_name_label_zh_cn;
                var cruisevessel = stravel.cruise_shipname_label_zh_cn;
                break;
            case 'zh-tw':
                var packagename = stravel.package_name_label_zh_tw;
                var packagedate = stravel.package_date_label_zh_tw;
                var packagenights = stravel.package_nights_label_zh_tw;
                var packageprice = stravel.package_price_label_zh_tw;
                var packageremark = stravel.package_remark_label_zh_tw;
                
                var airname = stravel.air_name_label_zh_tw;
                var airclass = stravel.air_class_label_zh_tw;
                var airtrip = stravel.air_trip_label_zh_tw;
                
                var hotelname = stravel.hotel_name_label_zh_tw;
                var hoteldst = stravel.hotel_dest_label_zh_tw;
                var hotelcheckin = stravel.hotel_checkin_label_zh_tw;
                
                var cruisename = stravel.cruise_name_label_zh_tw;
                var cruisevessel = stravel.cruise_shipname_label_zh_tw;
                break;
            default:
                var packagename = stravel.package_name_label_zh_tw;
                var packagedate = stravel.package_date_label_zh_tw;
                var packagenights = stravel.package_nights_label_zh_tw;
                var packageprice = stravel.package_price_label_zh_tw;
                var packageremark = stravel.package_remark_label_zh_tw;
                
                var airname = stravel.air_name_label_zh_tw;
                var airclass = stravel.air_class_label_zh_tw;
                var airtrip = stravel.air_trip_label_zh_tw;
                
                var hotelname = stravel.hotel_name_label_zh_tw;
                var hoteldst = stravel.hotel_dest_label_zh_tw;
                var hotelcheckin = stravel.hotel_checkin_label_zh_tw;
                
                var cruisename = stravel.cruise_name_label_en;
                var cruisevessel = stravel.cruise_shipname_label_en;
                break;
        }
        var frompage = jQuery(e.relatedTarget).attr('data-frompage');
        jQuery('#inputFromPage').val(frompage);
        switch(frompage) {
            case 'package':
                var pname = jQuery(e.relatedTarget).attr('data-productname');
                jQuery('#inputProductName').val(pname);
                jQuery('.productEnqueryInfo').append('<p><span>'+packagename+'</span><span>'+pname+'</span></p>');
                var pdate = jQuery(e.relatedTarget).attr('data-productdate');
                jQuery('#inputProductDate').val(pdate);
                jQuery('.productEnqueryInfo').append('<p><span>'+packagedate+'</span><span>'+pdate+'</span></p>');
                var pnights = jQuery(e.relatedTarget).attr('data-productnights');
                jQuery('#inputProductNights').val(pnights);
                jQuery('.productEnqueryInfo').append('<p><span>'+packagenights+'</span><span>'+pnights+'</span></p>');
                var pprice = jQuery(e.relatedTarget).attr('data-productprice');
                jQuery('#inputProductPrice').val(pprice);
                jQuery('.productEnqueryInfo').append('<p><span>'+packageprice+'</span><span>'+pprice+'</span></p>');
                //jQuery('.productEnqueryInfo').append('<p><span>'+packageremark+'</span></p>');
                jQuery('#inputRemark').val(packageremark);
                break;
            case 'air':
                var pname = jQuery(e.relatedTarget).attr('data-productname');
                jQuery('#inputProductName').val(pname);
                jQuery('.productEnqueryInfo').append('<p><span>'+airname+'</span><span>'+pname+'</span></p>');
                var pdate = jQuery(e.relatedTarget).attr('data-productdate');
                jQuery('#inputProductDate').val(pdate);
                jQuery('.productEnqueryInfo').append('<p><span>'+packagedate+'</span><span>'+pdate+'</span></p>');
                var pclass = jQuery(e.relatedTarget).attr('data-productclass');
                jQuery('#inputProductClass').val(pclass);
                jQuery('.productEnqueryInfo').append('<p><span>'+airclass+'</span><span>'+pclass+'</span></p>');
                var ptrip = jQuery(e.relatedTarget).attr('data-producttrip');
                jQuery('#inputProductTrip').val(ptrip);
                jQuery('.productEnqueryInfo').append('<p><span>'+airtrip+'</span><span>'+ptrip+'</span></p>');
                var pprice = jQuery(e.relatedTarget).attr('data-productprice');
                jQuery('#inputProductPrice').val(pprice);
                jQuery('.productEnqueryInfo').append('<p><span>'+packageprice+'</span><span>'+pprice+'</span></p>');
                break;
            case 'hotel':
                var pdst = jQuery(e.relatedTarget).attr('data-productdst');
                jQuery('#inputProductDst').val(pdst);
                jQuery('.productEnqueryInfo').append('<p><span>'+hoteldst+'</span><span>'+pdst+'</span></p>');
                var pname = jQuery(e.relatedTarget).attr('data-productname');
                jQuery('#inputProductName').val(pname);
                jQuery('.productEnqueryInfo').append('<p><span>'+hotelname+'</span><span>'+pname+'</span></p>');
                var pdate = jQuery(e.relatedTarget).attr('data-productdate');
                jQuery('#inputProductDate').val(pdate);
                jQuery('.productEnqueryInfo').append('<p><span>'+hotelcheckin+'</span><span>'+pdate+'</span></p>');
                var pnights = jQuery(e.relatedTarget).attr('data-productnights');
                jQuery('#inputProductNights').val(pnights);
                jQuery('.productEnqueryInfo').append('<p><span>'+packagenights+'</span><span>'+pnights+'</span></p>');
                var pprice = jQuery(e.relatedTarget).attr('data-productprice');
                jQuery('#inputProductPrice').val(pprice);
                jQuery('.productEnqueryInfo').append('<p><span>'+packageprice+'</span><span>'+pprice+'</span></p>');
                break;
            case 'cruise':
                var pname = jQuery(e.relatedTarget).attr('data-productname');
                jQuery('#inputProductName').val(pname);
                jQuery('.productEnqueryInfo').append('<p><span>'+cruisename+'</span><span>'+pname+'</span></p>');
                var pship = jQuery(e.relatedTarget).attr('data-productship');
                jQuery('#inputProductShip').val(pship);
                jQuery('.productEnqueryInfo').append('<p><span>'+cruisevessel+'</span><span>'+pship+'</span></p>');
                var pdate = jQuery(e.relatedTarget).attr('data-productdate');
                jQuery('#inputProductDate').val(pdate);
                jQuery('.productEnqueryInfo').append('<p><span>'+packagedate+'</span><span>'+pdate+'</span></p>');
                var pnights = jQuery(e.relatedTarget).attr('data-productnights');
                jQuery('#inputProductNights').val(pnights);
                jQuery('.productEnqueryInfo').append('<p><span>'+packagenights+'</span><span>'+pnights+'</span></p>');
                var pprice = jQuery(e.relatedTarget).attr('data-productprice');
                jQuery('#inputProductPrice').val(pprice);
                jQuery('.productEnqueryInfo').append('<p><span>'+packageprice+'</span><span>'+pprice+'</span></p>');
                break;
            default:
                break;
        }
        var pid = jQuery(e.relatedTarget).attr('data-productid');
        jQuery('#inputProductId').val(pid);
        
        
        jQuery('#enquiryModal .modalform,#enquiryModal #enquiryModalLabel').show();
        jQuery('#enquiryModal .modalthankyou').hide();
        document.getElementById('enquiryModalForm').reset();
    });
    jQuery('#enquiryModal .form-button button.submitbtn').click(function() {
        jQuery('#enquiryModal .modal-body .errormsg').html('');
        var name = jQuery('#inputYourName').val();
        var email = jQuery('#inputYourEmail').val();
        var number = jQuery('#inputYourNumber').val();
        var msg = jQuery('#inputYourMessage').val();
        
        var frompage  = jQuery('#inputFromPage').val();
        var id  = jQuery('#inputProductId').val();
        var pname  = jQuery('#inputProductName').val();
        var pprice  = jQuery('#inputProductPrice').val();
        var pdate  = jQuery('#inputProductDate').val();
        var pnights  = jQuery('#inputProductNights').val();        
        var remark  = jQuery('#inputRemark').val();
        var ptrip  = jQuery('#inputProductTrip').val();
        var pclass  = jQuery('#inputProductClass').val();
        var pdstname  = jQuery('#inputProductDst').val();
        var pship  = jQuery('#inputProductShip').val();
        
        var $error = 0;
        if(name=='') {
            $error += 1;
        }
        if(email=='') {
            $error +=2;
        }
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(re.test(email)==false) {
            $error+=4;
        }
        var $msg = '';
        if($error>0) {
            switch($error) {
                case 1:
                    $msg += stravel.missing_name_zh_tw;
                    break;
                case 2:
                    $msg += stravel.missing_email_zh_tw;
                    break;
                case 3:
                    $msg += stravel.missing_name_zh_tw + "<br />" + stravel.missing_email_zh_tw;
                    break;
                case 4:
                    $msg += stravel.invalid_email_zh_tw;
                    break;
                case 5: 
                    $msg += stravel.missing_name_zh_tw + "<br />" + stravel.invalid_email_zh_tw;
                    break;
                case 6:
                    $msg += stravel.invalid_email_zh_tw;;
                    break;
                case 7:
                    $msg += stravel.missing_name_zh_tw + "<br />" + stravel.invalid_email_zh_tw;
                    break;
                default:
                    $msg += stravel.default_error_zh_tw;
                    break;
            }
            jQuery('#enquiryModal .modal-body .errormsg').html($msg);
            return false;
        }
        jQuery('#enquiryModal .modalform,#enquiryModal #enquiryModalLabel').hide();
        jQuery('#enquiryModal .modalthankyou').show();
        jQuery.post('index.php?option=com_stravel&task=sendProductEnquiry',{'frompage':frompage,'name':name,'email':email,'number':number,'message':msg,'pid':id,'pname':pname,'pprice':pprice,'pdate':pdate,'pnights':pnights,'pdstname':pdstname,'ptrip':ptrip, 'pclass':pclass, 'pship':pship,'remark':remark},function(response) {
            var obj = jQuery.parseJSON(response);
            if(obj.errorcode>0) {
                switch(obj.errorcode) {
                    case 1:
                        $msg += stravel.missing_name_zh_tw;
                        break;
                    case 2:
                        $msg += stravel.missing_email_zh_tw;
                        break;
                    case 3:
                        $msg += stravel.missing_name_zh_tw + "<br />" + stravel.missing_email_zh_tw;
                        break;
                    case 4:
                        $msg += stravel.invalid_email_zh_tw;
                        break;
                    case 5: 
                        $msg += stravel.missing_name_zh_tw + "<br />" + stravel.invalid_email_zh_tw;
                        break;
                    case 6:
                        $msg += stravel.invalid_email_zh_tw;;
                        break;
                    case 7:
                        $msg += stravel.missing_name_zh_tw + "<br />" + stravel.invalid_email_zh_tw;
                        break;
                    default:
                        $msg += stravel.default_error;
                        break;
                }
                return false;
            }
        });
    });
    //General enquery send email
    jQuery('#enquiryGeneralModal').on('show.bs.modal',function(e){
        jQuery('#enquiryGeneralModal .modalform,#enquiryGeneralModal #enquiryModalLabel').show();
        jQuery('#enquiryGeneralModal .modalthankyou').hide();
        document.getElementById('enquiryGeneralModalForm').reset();
    });
    jQuery('#enquiryGeneralModal .form-button button.submitbtn').click(function() {
        jQuery('#enquiryGeneralModal .modal-body .errormsg').html('');
        var name = jQuery('#inputYourNameGeneral').val();
        var email = jQuery('#inputYourEmailGeneral').val();
        var number = jQuery('#inputYourNumberGeneral').val();
        var msg = jQuery('#inputYourMessageGeneral').val();
        var type = jQuery('#inputTypeGeneral').val();
        var $error = 0;
        if(name=='') {
            $error += 1;
        }
        if(email=='') {
            $error +=2;
        }
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(re.test(email)==false) {
            $error+=4;
        }
        var $msg = '';
        if($error>0) {
            switch($error) {
                case 1:
                    $msg += stravel.missing_name_zh_tw;
                    break;
                case 2:
                    $msg += stravel.missing_email_zh_tw;
                    break;
                case 3:
                    $msg += stravel.missing_name_zh_tw + "<br />" + stravel.missing_email_zh_tw;
                    break;
                case 4:
                    $msg += stravel.invalid_email_zh_tw;
                    break;
                case 5: 
                    $msg += stravel.missing_name_zh_tw + "<br />" + stravel.invalid_email_zh_tw;
                    break;
                case 6:
                    $msg += stravel.invalid_email_zh_tw;;
                    break;
                case 7:
                    $msg += stravel.missing_name_zh_tw + "<br />" + stravel.invalid_email_zh_tw;
                    break;
                default:
                    $msg += stravel.default_error;
                    break;
            }
            jQuery('#enquiryGeneralModal .modal-body .errormsg').html($msg);
            return false;
        }
        jQuery('#enquiryGeneralModal .modalform,#enquiryGeneralModal #enquiryGeneralModalLabel').hide();
        jQuery('#enquiryGeneralModal .modalthankyou').show();
        jQuery.post('index.php?option=com_stravel&task=sendGeneralEnquiry',{'name':name,'email':email,'number':number,'message':msg,'type':type},function(response) {
            var obj = jQuery.parseJSON(response);
            if(obj.errorcode>0) {
                switch(obj.errorcode) {
                    case 1:
                        $msg += stravel.missing_name_zh_tw;
                        break;
                    case 2:
                        $msg += stravel.missing_email_zh_tw;
                        break;
                    case 3:
                        $msg += stravel.missing_name_zh_tw + "<br />" + stravel.missing_email_zh_tw;
                        break;
                    case 4:
                        $msg += stravel.invalid_email_zh_tw;
                        break;
                    case 5: 
                        $msg += stravel.missing_name_zh_tw + "<br />" + stravel.invalid_email_zh_tw;
                        break;
                    case 6:
                        $msg += stravel.invalid_email_zh_tw;;
                        break;
                    case 7:
                        $msg += stravel.missing_name_zh_tw + "<br />" + stravel.invalid_email_zh_tw;
                        break;
                    default:
                        $msg += stravel.default_error;
                        break;
                }
                return false;
            }
        });
    });
    
    //Newsletter email
    jQuery('#enquiryNewsletterModal .form-button button.submitbtn').click(function() {
        jQuery('#enquiryNewsletterModal .modal-body .errormsg').html('');
        var name = jQuery('#inputYourNameNewsletter').val();
        var lname = jQuery('#inputYourLNameNewsletter').val();
        var email = jQuery('#inputYourEmailNewsletter').val();
        var number = jQuery('#inputYourNumberNewsletter').val();
        var sex = jQuery('#inputYourSexNewsletter').val();
        var birthmonth = jQuery('#inputYourBirthMonthNewsletter').val();
        var birthdate = jQuery('#inputYourBirthDateNewsletter').val();
        var $error = 0;
        if(name=='' || lname=='') {
            $error += 1;
        }
        if(email=='') {
            $error += 2;
        }
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(re.test(email)==false) {
            $error+=4;
        }
        var $msg = '';
        if($error>0) {
            switch($error) {
                case 1:
                    $msg += stravel.missing_name_zh_tw;
                    break;
                case 2:
                    $msg += stravel.missing_email_zh_tw;
                    break;
                case 3:
                    $msg += stravel.missing_name_zh_tw + "<br />" + stravel.missing_email_zh_tw;
                    break;
                case 4:
                    $msg += stravel.invalid_email_zh_tw;
                    break;
                case 5: 
                    $msg += stravel.missing_name_zh_tw + "<br />" + stravel.invalid_email_zh_tw;
                    break;
                case 6:
                    $msg += stravel.invalid_email_zh_tw;;
                    break;
                case 7:
                    $msg += stravel.missing_name_zh_tw + "<br />" + stravel.invalid_email_zh_tw;
                    break;
                default:
                    $msg += stravel.default_error;
                    break;
            }
            jQuery('#enquiryNewsletterModal .modal-body .errormsg').html($msg);
            return false;
        }
        jQuery('#enquiryNewsletterModal .modalform,#enquiryNewsletterModal #enquiryNewsletterModalLabel').hide();
        jQuery('#enquiryNewsletterModal .modalthankyou').show();
        jQuery.post('index.php?option=com_stravel&task=saveNewsletter',{'name':name,'lname':lname,'email':email,'number':number,'sex':sex,'birthmonth':birthmonth,'birthdate':birthdate},function(rs) {
            //console.log(rs);
        });       
    });
    
    //staff modal email
    jQuery('#staffModal .form-button button.submitbtn').click(function() {
        jQuery('#staffModal .modal-body .errormsg').html('');
        var name = jQuery('#inputYourNameStaff').val();
        var lname = jQuery('#inputYourLNameStaff').val();
        var email = jQuery('#inputYourEmailStaff').val();
        var number = jQuery('#inputYourNumberStaff').val();
        var sex = jQuery('#inputYourSexStaff').val();
        var birthmonth = jQuery('#inputYourBirthMonthStaff').val();
        var birthdate = jQuery('#inputYourBirthDateStaff').val();
        var pname = jQuery('#inputYourProductNameStaff').val();
        var departure = jQuery('#inputYourDestinationStaff').val();
        var year = jQuery('#inputYourDepYearStaff').val();
        var month = jQuery('#inputYourDepMonthStaff').val();
        var day = jQuery('#inputYourDepDayStaff').val();
        var pno = jQuery('#inputYourPersonNoStaff').val();
        var saleprice = jQuery('#inputYourSalePriceStaff').val();
        var feedback = jQuery('#inputYourFeedBackStaff').val();
        var remark = jQuery('#inputYourRemarkStaff').val();
        var staffnumber = jQuery('#inputYourSNumberStaff').val();
        var isource = jQuery('#inputYourSourceStaff').val();
        
        var $error = 0;
        if(name=='' || lname=='') {
            $error += 1;
        }
        if(email=='') {
            $error += 2;
        }
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(re.test(email)==false) {
            $error+=4;
        }
        
        var $msg = '';
        if($error>0) {
            switch($error) {
                case 1:
                    $msg += stravel.missing_name_zh_tw;
                    break;
                case 2:
                    $msg += stravel.missing_email_zh_tw;
                    break;
                case 3:
                    $msg += stravel.missing_name_zh_tw + "<br />" + stravel.missing_email_zh_tw;
                    break;
                case 4:
                    $msg += stravel.invalid_email_zh_tw;
                    break;
                case 5: 
                    $msg += stravel.missing_name_zh_tw + "<br />" + stravel.invalid_email_zh_tw;
                    break;
                case 6:
                    $msg += stravel.invalid_email_zh_tw;;
                    break;
                case 7:
                    $msg += stravel.missing_name_zh_tw + "<br />" + stravel.invalid_email_zh_tw;
                    break;
                default:
                    $msg += stravel.default_error;
                    break;
            }
            jQuery('#staffModal .modal-body .errormsg').html($msg);
            return false;
        }
        
        if(staffnumber=='') {
            $msg += stravel.staffnumber_empty_zh_tw;
            jQuery('#staffModal .modal-body .errormsg').html($msg);
            return false;
        }
        
        jQuery('#staffModal .modalform,#staffModal #staffModalLabel').hide();
        jQuery('#staffModal .modalthankyou').show();
        jQuery.post('index.php?option=com_stravel&task=saveNewsletter',{'name':name,'lname':lname,'email':email,'number':number,'sex':sex,'birthmonth':birthmonth,'birthdate':birthdate,'pname':pname,'departure':departure,'year':year,'month':month,'day':day,'pno':pno,'saleprice':saleprice,'feedback':feedback,'remark':remark,'source':isource,'staffnumber':staffnumber},function(rs) {
            //console.log(rs);
        });       
    });
    
    //Add hottest city to air
    if(typeof hotelcities == 'undefined') {
        hotelcities = htl;    
    }
    count = 0; 
    jQuery.each(hotelcities,function(i,val){
        if(count>=10) {
            return false;
        }
        var datafname = val[1].replace('[S]',' ');
        var infoname = val[1].split('[S]');
        var name_eng = infoname[1].trim();
        infoname = infoname[0].split(/(\(.*\))/);
        var name_zhtw = infoname[0].trim();
        var name_zhcn = infoname[2].trim();
        count++;
        if(count==1) {
            var hottxt = '<span class="hottxt">'+stravel.hotcity_text_zh_tw+'</span>';
        }else{
            var hottxt = '';
        }
        jQuery('.hotelcitylistitem').append('<li data-nameeng="'+name_eng+'" data-namezhtw="'+name_zhtw+'" data-namezhcn="'+name_zhcn+'" data-name="'+datafname+'" data-code="'+val[0]+'"><a href="#" onclick="return false;">'+datafname+hottxt+'</a></li>');
    });
    //Filter for hotel, package input
    jQuery('#destinationht').on('keyup',function(e) {
        e.preventDefault();
        var thisel = jQuery(this);
        var keyword = thisel.val();
        var keystrokecode = e.which;
        if(keystrokecode==13) {
            if(keyword.length>0) {
                if(jQuery('.search .hotelcitylistitem li.selected').length>0 && jQuery('.search .hotelcitylistitem li.selected').is(':visible')) {
                    var lang = jQuery('html').attr('lang');
                    switch(lang) {
                        case 'zh-tw':
                            var airname = jQuery('.search .hotelcitylistitem li.selected').attr('data-namezhtw');
                            break;
                        case 'zh-cn':
                            var airname = jQuery('.search .hotelcitylistitem li.selected').attr('data-namezhcn');
                            break;
                        case 'en-gb':
                            var airname = jQuery('.search .hotelcitylistitem li.selected').attr('data-nameeng');
                            break;
                        default:
                            var airname = jQuery('.search .hotelcitylistitem li.selected').attr('data-namezhtw');
                            break;
                    }
                    jQuery('#destinationht').val(airname);
                    jQuery('.search .hotelcitylistitem').next().val(jQuery('.search .hotelcitylistitem li.selected').attr('data-code'));
                    jQuery('#destinationht').dropdown('toggle');
                }else{
                    var gotitem = false;
                    keyword = keyword.toString().toLowerCase();
                    jQuery.each(htl,function(i,val) {
                        if(gotitem==true) {
                            return false;
                        }
                        var datafname = val[1].replace('[S]',' ');
                        var infoname = val[1].split('[S]');
                        var name_eng = infoname[1].trim();
                        infoname = infoname[0].split(/(\(.*\))/);
                        var name_zhtw = infoname[0].trim();
                        var name_zhcn = infoname[2].trim();
                        var searchresult = datafname.toString().toLowerCase().search(keyword);
                        if(searchresult>=0) {
                            gotitem = true;
                            var aircode = val[0];
                            var lang = jQuery('html').attr('lang');
                            switch(lang) {
                                case 'zh-tw':
                                    var airname = name_zhtw;
                                    break;
                                case 'zh-cn':
                                    var airname = name_zhcn;
                                    break;
                                case 'en-gb':
                                    var airname = name_eng;
                                    break;
                                default:
                                    var airname = name_zhtw;
                                    break;
                            }
                            thisel.val(airname);
                            thisel.parentsUntil('.form-group').find('input[type="hidden"]').val(aircode);
                            thisel.dropdown('toggle');
                        }
                    });
                    if(gotitem==false) {
                        thisel.val('');
                    }    
                }    
            }
        }else if(keystrokecode==40) {
            var shouldBeSelected =  false;
            var shouldChange = true;
            jQuery('.search .hotelcitylistitem li').each(function() {
                if(jQuery('.search .hotelcitylistitem li.selected').length>0) {
                    if(jQuery(this).hasClass('selected')) {
                        jQuery('.search .hotelcitylistitem li').removeClass('selected');
                        shouldBeSelected = true;
                    }else{
                        if(shouldBeSelected && shouldChange) {
                            jQuery(this).addClass('selected');
                            shouldBeSelected = false;
                            shouldChange = false;
                        }
                    }
                }else{
                    jQuery('.search .hotelcitylistitem li').removeClass('selected');
                    jQuery(this).addClass('selected');
                    return false;    
                }
            });
        }else if(keystrokecode==38) {
            var shouldBeSelected =  false;
            var shouldChange = true;
            jQuery(jQuery('.search .hotelcitylistitem li').get().reverse()).each(function() {
                if(jQuery('.search .hotelcitylistitem li.selected').length>0) {
                    if(jQuery(this).hasClass('selected')) {
                        jQuery('.search .hotelcitylistitem li').removeClass('selected');
                        shouldBeSelected = true;
                    }else{
                        if(shouldBeSelected && shouldChange) {
                            jQuery(this).addClass('selected');
                            shouldBeSelected = false;
                            shouldChange = false;
                        }
                    }
                }else{
                    jQuery('.search .hotelcitylistitem li').removeClass('selected');
                    jQuery(this).addClass('selected');
                    return false;    
                }
            });    
        }else{
            if(keyword.length>0) {
                keyword = keyword.toString().toLowerCase();
                count = 0;
                jQuery('.search .hotelcitylistitem li').remove();
                jQuery.each(htl,function(i,val) {
                    if(count>=15) {
                        return false;
                    }
                    var datafname = val[1].replace('[S]',' ');
                    var infoname = val[1].split('[S]');
                    var name_eng = infoname[1].trim();
                    infoname = infoname[0].split(/(\(.*\))/);
                    var name_zhtw = infoname[0].trim();
                    var name_zhcn = infoname[2].trim();
                    var searchresult = datafname.toString().toLowerCase().search(keyword);
                    if(searchresult>=0) {
                        var replacetxt = datafname.substr(searchresult,keyword.length);
                        thisdata = datafname.replace(replacetxt,'<span class="changecolor">'+replacetxt+'</span>');
                        jQuery('.search .hotelcitylistitem').append('<li data-nameeng="'+name_eng+'" data-namezhtw="'+name_zhtw+'" data-namezhcn="'+name_zhcn+'" data-name="'+datafname+'" data-code="'+val[0]+'"><a href="#" onclick="return false;">'+thisdata+'</a></li>');
                        count++;
                    }
                });          
                if(count==0) {
                    jQuery('.search .hotelcitylistitem').append('<li><a href="#" onclick="return false;">'+stravel.no_city_found_en+'</a></li>');
                }  
            }else{
                //thisel.next().find('li').remove();
                jQuery('.search .hotelcitylistitem li').remove();
                //Add hottest city to air
                count = 0;
                jQuery.each(hotelcities,function(i,val){
                    if(count>=10) {
                        return false;
                    }
                    var datafname = val[1].replace('[S]',' ');
                    var infoname = val[1].split('[S]');
                    var name_eng = infoname[1].trim();
                    infoname = infoname[0].split(/(\(.*\))/);
                    var name_zhtw = infoname[0].trim();
                    var name_zhcn = infoname[2].trim();
                    count++;
                    if(count==1) {
                        var hottxt = '<span class="hottxt">'+stravel.hotcity_text_zh_tw+'</span>';
                    }else{
                        var hottxt = '';
                    }
                    jQuery('.search .hotelcitylistitem').append('<li data-nameeng="'+name_eng+'" data-namezhtw="'+name_zhtw+'" data-namezhcn="'+name_zhcn+'" data-name="'+datafname+'" data-code="'+val[0]+'"><a href="#" onclick="return false;">'+datafname+hottxt+'</a></li>');
                });
            }
        }        
    });
    
    //Filter for hotel form  in modal
    jQuery('#destination').on('keyup',function(e) {
        e.preventDefault();
        var thisel = jQuery(this);
        var keyword = thisel.val();
        var keystrokecode = e.which;
        if(keystrokecode==13) {
            if(keyword.length>0) {
                if(jQuery('.modal .hotelcitylistitem li.selected').length>0 && jQuery('.modal .hotelcitylistitem li.selected').is(':visible')) {
                    var lang = jQuery('html').attr('lang');
                    switch(lang) {
                        case 'zh-tw':
                            var airname = jQuery('.modal .hotelcitylistitem li.selected').attr('data-namezhtw');
                            break;
                        case 'zh-cn':
                            var airname = jQuery('.modal .hotelcitylistitem li.selected').attr('data-namezhcn');
                            break;
                        case 'en-gb':
                            var airname = jQuery('.modal .hotelcitylistitem li.selected').attr('data-nameeng');
                            break;
                        default:
                            var airname = jQuery('.modal .hotelcitylistitem li.selected').attr('data-namezhtw');
                            break;
                    }
                    jQuery('#destination').val(airname);
                    jQuery('.modal .hotelcitylistitem').next().val(jQuery('.modal .hotelcitylistitem li.selected').attr('data-code'));
                    jQuery('#destination').dropdown('toggle');
                }else{
                    var gotitem = false;
                    keyword = keyword.toString().toLowerCase();
                    jQuery.each(htl,function(i,val) {
                        if(gotitem==true) {
                            return false;
                        }
                        var datafname = val[1].replace('[S]',' ');
                        var infoname = val[1].split('[S]');
                        var name_eng = infoname[1].trim();
                        infoname = infoname[0].split(/(\(.*\))/);
                        var name_zhtw = infoname[0].trim();
                        var name_zhcn = infoname[2].trim();
                        var searchresult = datafname.toString().toLowerCase().search(keyword);
                        if(searchresult>=0) {
                            gotitem = true;
                            var aircode = val[0];
                            var lang = jQuery('html').attr('lang');
                            switch(lang) {
                                case 'zh-tw':
                                    var airname = name_zhtw;
                                    break;
                                case 'zh-cn':
                                    var airname = name_zhcn;
                                    break;
                                case 'en-gb':
                                    var airname = name_eng;
                                    break;
                                default:
                                    var airname = name_zhtw;
                                    break;
                            }
                            thisel.val(airname);
                            thisel.parentsUntil('.form-group').find('input[type="hidden"]').val(aircode);
                            thisel.dropdown('toggle');
                        }
                    });
                    if(gotitem==false) {
                        thisel.val('');
                    }     
                }                  
            }
        }else if(keystrokecode==40) {
            var shouldBeSelected =  false;
            var shouldChange = true;
            jQuery('.modal .hotelcitylistitem li').each(function() {
                if(jQuery('.modal .hotelcitylistitem li.selected').length>0) {
                    if(jQuery(this).hasClass('selected')) {
                        jQuery('.modal .hotelcitylistitem li').removeClass('selected');
                        shouldBeSelected = true;
                    }else{
                        if(shouldBeSelected && shouldChange) {
                            jQuery(this).addClass('selected');
                            shouldBeSelected = false;
                            shouldChange = false;
                        }
                    }
                }else{
                    jQuery('.modal .hotelcitylistitem li').removeClass('selected');
                    jQuery(this).addClass('selected');
                    return false;    
                }
            });
        }else if(keystrokecode==38) {
            var shouldBeSelected =  false;
            var shouldChange = true;
            jQuery(jQuery('.modal .hotelcitylistitem li').get().reverse()).each(function() {
                if(jQuery('.modal .hotelcitylistitem li.selected').length>0) {
                    if(jQuery(this).hasClass('selected')) {
                        jQuery('.modal .hotelcitylistitem li').removeClass('selected');
                        shouldBeSelected = true;
                    }else{
                        if(shouldBeSelected && shouldChange) {
                            jQuery(this).addClass('selected');
                            shouldBeSelected = false;
                            shouldChange = false;
                        }
                    }
                }else{
                    jQuery('.modal .hotelcitylistitem li').removeClass('selected');
                    jQuery(this).addClass('selected');
                    return false;    
                }
            });
        }else{
            if(keyword.length>0) {
                keyword = keyword.toString().toLowerCase();
                count = 0;
                jQuery('.modal .hotelcitylistitem li').remove();
                jQuery.each(htl,function(i,val) {
                    if(count>=15) {
                        return false;
                    }
                    var datafname = val[1].replace('[S]',' ');
                    var infoname = val[1].split('[S]');
                    var name_eng = infoname[1].trim();
                    infoname = infoname[0].split(/(\(.*\))/);
                    var name_zhtw = infoname[0].trim();
                    var name_zhcn = infoname[2].trim();
                    var searchresult = datafname.toString().toLowerCase().search(keyword);
                    if(searchresult>=0) {
                        var replacetxt = datafname.substr(searchresult,keyword.length);
                        thisdata = datafname.replace(replacetxt,'<span class="changecolor">'+replacetxt+'</span>');
                        jQuery('.modal .hotelcitylistitem').append('<li data-nameeng="'+name_eng+'" data-namezhtw="'+name_zhtw+'" data-namezhcn="'+name_zhcn+'" data-name="'+datafname+'" data-code="'+val[0]+'"><a href="#" onclick="return false;">'+thisdata+'</a></li>');
                        count++;
                    }
                });          
                if(count==0) {
                    jQuery('.modal .hotelcitylistitem').append('<li><a href="#" onclick="return false;">'+stravel.no_city_found_en+'</a></li>');
                }  
            }else{
                //thisel.next().find('li').remove();
                jQuery('.modal .hotelcitylistitem li').remove();
                //Add hottest city to air
                count = 0;
                jQuery.each(hotelcities,function(i,val){
                    if(count>=10) {
                        return false;
                    }
                    var datafname = val[1].replace('[S]',' ');
                    var infoname = val[1].split('[S]');
                    var name_eng = infoname[1].trim();
                    infoname = infoname[0].split(/(\(.*\))/);
                    var name_zhtw = infoname[0].trim();
                    var name_zhcn = infoname[2].trim();
                    count++;
                    if(count==1) {
                        var hottxt = '<span class="hottxt">'+stravel.hotcity_text_zh_tw+'</span>';
                    }else{
                        var hottxt = '';
                    }
                    jQuery('.modal .hotelcitylistitem').append('<li data-nameeng="'+name_eng+'" data-namezhtw="'+name_zhtw+'" data-namezhcn="'+name_zhcn+'" data-name="'+datafname+'" data-code="'+val[0]+'"><a href="#" onclick="return false;">'+datafname+hottxt+'</a></li>');
                });
            }
        }        
    });
    
    //Add hottest city to air
    if(typeof farecities == 'undefined') {
        farecities = c;
    }
    count = 0; 
    jQuery.each(farecities,function(i,val){
        if(count>=10) {
            return false;
        }
        var datafname = val[1].replace('[S]',' ');
        var infoname = val[1].split('[S]');
        var name_eng = infoname[1].trim();
        infoname = infoname[0].split(/(\(.*\))/);
        var name_zhtw = infoname[0].trim();
        var name_zhcn = infoname[2].trim();
        count++;
        if(count==1) {
            var hottxt = '<span class="hottxt">'+stravel.hotcity_text_zh_tw+'</span>';
        }else{
            var hottxt = '';
        }
        jQuery('.aircitylistitem').append('<li data-nameeng="'+name_eng+'" data-namezhtw="'+name_zhtw+'" data-namezhcn="'+name_zhcn+'" data-name="'+datafname+'" data-code="'+val[0]+'"><a href="#" onclick="return false;">'+datafname+hottxt+'</a></li>');
    });

    //Filter for hotel, package input
    jQuery('#arrivedCity2').on('keyup',function(e) {
        e.preventDefault();
        var thisel = jQuery(this);
        var keyword = thisel.val();
        var keystrokecode = e.which;
        if(keystrokecode==13) {
            if(keyword.length>0) {
                if(jQuery('.aircitylistitem li.selected').length>0 && jQuery('.aircitylistitem li.selected').is(':visible')) {
                    var lang = jQuery('html').attr('lang');
                    switch(lang) {
                        case 'zh-tw':
                            var airname = jQuery('.aircitylistitem li.selected').attr('data-namezhtw');
                            break;
                        case 'zh-cn':
                            var airname = jQuery('.aircitylistitem li.selected').attr('data-namezhcn');
                            break;
                        case 'en-gb':
                            var airname = jQuery('.aircitylistitem li.selected').attr('data-nameeng');
                            break;
                        default:
                            var airname = jQuery('.aircitylistitem li.selected').attr('data-namezhtw');
                            break;
                    }
                    jQuery('#arrivedCity2').val(airname);
                    jQuery('.aircitylistitem').next().val(jQuery('.aircitylistitem li.selected').attr('data-code'));
                    jQuery('#arrivedCity2').dropdown('toggle');
                }else{
                    var gotitem = false;
                    keyword = keyword.toString().toLowerCase();
                    jQuery.each(c,function(i,val) {
                        if(gotitem==true) {
                            return false;
                        }
                        var datafname = val[1].replace('[S]',' ');
                        var infoname = val[1].split('[S]');
                        var name_eng = infoname[1].trim();
                        infoname = infoname[0].split(/(\(.*\))/);
                        var name_zhtw = infoname[0].trim();
                        var name_zhcn = infoname[2].trim();
                        var searchresult = datafname.toString().toLowerCase().search(keyword);
                        if(searchresult>=0) {
                            gotitem = true;
                            var aircode = val[0];
                            var lang = jQuery('html').attr('lang');
                            switch(lang) {
                                case 'zh-tw':
                                    var airname = name_zhtw;
                                    break;
                                case 'zh-cn':
                                    var airname = name_zhcn;
                                    break;
                                case 'en-gb':
                                    var airname = name_eng;
                                    break;
                                default:
                                    var airname = name_zhtw;
                                    break;
                            }
                            thisel.val(airname);
                            thisel.parentsUntil('.form-group').find('input[type="hidden"]').val(aircode);
                            thisel.dropdown('toggle');
                        }
                    });
                    if(gotitem==false) {
                        thisel.val('');
                    }
                }    
            }
        }else if(keystrokecode==40) {
            var shouldBeSelected =  false;
            var shouldChange = true;
            jQuery('.aircitylistitem li').each(function() {
                if(jQuery('.aircitylistitem li.selected').length>0) {
                    if(jQuery(this).hasClass('selected')) {
                        jQuery('.aircitylistitem li').removeClass('selected');
                        shouldBeSelected = true;
                    }else{
                        if(shouldBeSelected && shouldChange) {
                            jQuery(this).addClass('selected');
                            shouldBeSelected = false;
                            shouldChange = false;
                        }
                    }
                }else{
                    jQuery('.aircitylistitem li').removeClass('selected');
                    jQuery(this).addClass('selected');
                    return false;    
                }
            });
        }else if(keystrokecode==38) {
            var shouldBeSelected =  false;
            var shouldChange = true;
            jQuery(jQuery('.aircitylistitem li').get().reverse()).each(function() {
                if(jQuery('.aircitylistitem li.selected').length>0) {
                    if(jQuery(this).hasClass('selected')) {
                        jQuery('.aircitylistitem li').removeClass('selected');
                        shouldBeSelected = true;
                    }else{
                        if(shouldBeSelected && shouldChange) {
                            jQuery(this).addClass('selected');
                            shouldBeSelected = false;
                            shouldChange = false;
                        }
                    }
                }else{
                    jQuery('.aircitylistitem li').removeClass('selected');
                    jQuery(this).addClass('selected');
                    return false;    
                }
            });
        }else{
            if(keyword.length>0) {
                keyword = keyword.toString().toLowerCase();
                count = 0;
                jQuery('.aircitylistitem li').remove();
                jQuery.each(c,function(i,val) {
                    if(count>=15) {
                        return false;
                    }
                    var datafname = val[1].replace('[S]',' ');
                    var infoname = val[1].split('[S]');
                    var name_eng = infoname[1].trim();
                    infoname = infoname[0].split(/(\(.*\))/);
                    var name_zhtw = infoname[0].trim();
                    var name_zhcn = infoname[2].trim();
                    var searchresult = datafname.toString().toLowerCase().search(keyword);
                    if(searchresult>=0) {
                        var replacetxt = datafname.substr(searchresult,keyword.length);
                        thisdata = datafname.replace(replacetxt,'<span class="changecolor">'+replacetxt+'</span>');
                        jQuery('.aircitylistitem').append('<li data-nameeng="'+name_eng+'" data-namezhtw="'+name_zhtw+'" data-namezhcn="'+name_zhcn+'" data-name="'+datafname+'" data-code="'+val[0]+'"><a href="#" onclick="return false;">'+thisdata+'</a></li>');
                        count++;
                    }
                });          
                if(count==0) {
                    jQuery('.aircitylistitem').append('<li><a href="#" onclick="return false;">'+stravel.no_city_found_en+'</a></li>');
                }  
            }else{
                //thisel.next().find('li').remove();
                jQuery('.aircitylistitem li').remove();
                //Add hottest city to air
                count = 0;
                jQuery.each(farecities,function(i,val){
                    if(count>=10) {
                        return false;
                    }
                    var datafname = val[1].replace('[S]',' ');
                    var infoname = val[1].split('[S]');
                    var name_eng = infoname[1].trim();
                    infoname = infoname[0].split(/(\(.*\))/);
                    var name_zhtw = infoname[0].trim();
                    var name_zhcn = infoname[2].trim();
                    count++;
                    if(count==1) {
                        var hottxt = '<span class="hottxt">'+stravel.hotcity_text_zh_tw+'</span>';
                    }else{
                        var hottxt = '';
                    }
                    jQuery('.aircitylistitem').append('<li data-nameeng="'+name_eng+'" data-namezhtw="'+name_zhtw+'" data-namezhcn="'+name_zhcn+'" data-name="'+datafname+'" data-code="'+val[0]+'"><a href="#" onclick="return false;">'+datafname+hottxt+'</a></li>');
                });
            }
        }        
    });
    
    //Fire when user click on a item
    jQuery('.hotelcitylistitem,.aircitylistitem').on('click','li',function(e) {
        //e.preventDefault();
        var aircode = jQuery(this).attr('data-code');
        var lang = jQuery('html').attr('lang');
        switch(lang) {
            case 'zh-tw':
                var airname = jQuery(this).attr('data-namezhtw');
                break;
            case 'zh-cn':
                var airname = jQuery(this).attr('data-namezhcn');
                break;
            case 'en-gb':
                var airname = jQuery(this).attr('data-nameeng');
                break;
            default:
                var airname = jQuery(this).attr('data-namezhtw');
                break;
        }

        jQuery(this).parentsUntil('.form-group').find('input[type="text"]').val(airname);
        jQuery(this).parentsUntil('.form-group').find('input[type="hidden"]').val(aircode);     
    });
    
    jQuery(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
    });
    //package city keyup event
    jQuery(document).on('keyup','#arrivedCity',function(e) {
        var keystrokecode = e.which;
        var thisel = jQuery(this);
        var keyword = thisel.val();
        var ullist = thisel.next();
        
        if(keystrokecode==13) {
            if(keyword.length>0) {
                if(jQuery('.packagecitylistitem li.selected').length>0 && jQuery('.packagecitylistitem li.selected').is(':visible')) {
                    jQuery('#arrivedCity').val(jQuery('.packagecitylistitem li.selected').attr('data-cityname'));
                    jQuery('#arrivedCity').next().next().val(jQuery('.packagecitylistitem li.selected').attr('data-citycode'));
                    thisel.dropdown("toggle");    
                }else{
                    keyword = keyword.toString().toLowerCase();
                    var gotitem = false;
                    jQuery('.packagecitylistitem li').each(function() {
                        if(gotitem==true) {
                            return false;
                        }
                        if(jQuery(this).hasClass('normalcity')) {
                            var dataname = jQuery(this).attr('data-name');
                            var datacityname = jQuery(this).attr('data-cityname');
                            var datacode = jQuery(this).attr('data-citycode');
                            var thisdata = dataname.toString().toLowerCase();
                            var searchresult = thisdata.search(keyword);
                            if(searchresult>=0) {
                                gotitem = true;
                                //jQuery(this).parent().prev().val(datacityname);
                                jQuery('#arrivedCity').val(datacityname);
                                jQuery(this).parent().next().val(datacode);
                                thisel.dropdown("toggle");   
                            }    
                        }        
                    });
                    if(gotitem==false) {
                        thisel.val('');
                    }
                }                        
            }
        } else if(keystrokecode==40) {
            ullist.find('li').removeClass('visible');
            ullist.find('li').each(function() {
                if(jQuery(this).is(':visible')) {
                    jQuery(this).addClass('visible');
                }                    
            });
            var shouldBeSelected =  false;
            var shouldChange = true;
            ullist.find('li.visible').each(function() {
                if(ullist.find('li.selected.visible').length>0) {
                    if(jQuery(this).hasClass('selected')) {
                        ullist.find('li').removeClass('selected');
                        shouldBeSelected = true;
                    }else{
                        if(shouldBeSelected && shouldChange) {
                            jQuery(this).addClass('selected');
                            shouldBeSelected = false;
                            shouldChange = false;
                        }
                    }
                }else{
                    ullist.find('li').removeClass('selected');
                    jQuery(this).addClass('selected');
                    return false;    
                }                    
            });
        } else if(keystrokecode==38) {
            ullist.find('li').removeClass('visible');
            ullist.find('li').each(function() {
                if(jQuery(this).is(':visible')) {
                    jQuery(this).addClass('visible');
                }                    
            });
            var shouldBeSelected =  false;
            var shouldChange = true;
            jQuery(ullist.find('li.visible').get().reverse()).each(function() {
                if(ullist.find('li.selected.visible').length>0) {
                    if(jQuery(this).hasClass('selected')) {
                        ullist.find('li').removeClass('selected');
                        shouldBeSelected = true;
                    }else{
                        if(shouldBeSelected && shouldChange) {
                            jQuery(this).addClass('selected');
                            shouldBeSelected = false;
                            shouldChange = false;
                        }
                    }
                }else{
                    ullist.find('li').removeClass('selected');
                    jQuery(this).addClass('selected');
                    return false;    
                }                    
            });
        }else{
            if(keyword.length>0) {
                keyword = keyword.toString().toLowerCase();
                thisel.next().find('li').hide();
                count = 0;            
                jQuery('.packagecitylistitem li').each(function() {
                    if(count>14) {return false;}
                    if(jQuery(this).hasClass('normalcity')) {
                        var dataname = jQuery(this).attr('data-name');
                        var datacode = jQuery(this).attr('data-citycode');
                        var thisdata = dataname.toString().toLowerCase();
                        var searchresult = thisdata.search(keyword);
                        if(searchresult>=0) {
                            var replacetxt = dataname.substr(searchresult,keyword.length); 
                            thisdata = dataname.replace(replacetxt,'<span class="changecolor">'+replacetxt+'</span>');
                            jQuery(this).html('<a href="#" onclick="return false;">'+thisdata+'</a>');
                            jQuery(this).show();
                            count += 1;
                        }else{
                            jQuery(this).hide();
                        }
                    }else{
                        jQuery(this).hide();
                    }
                });
            }else{
                thisel.next().find('li').each(function(e) {
                    /*
                    var dataname = jQuery(this).attr('data-name');
                    if(jQuery(this).attr('data-order')<11) {
                        if(jQuery(this).attr('data-order')==1) {
                            dataname = dataname + '<span class="hottxt">'+stravel.hotcity_text_zh_tw+'</span>';
                        }
                        jQuery(this).html('<a href="#" onclick="return false;">'+dataname+'</a>').show();
                    }else{
                        jQuery(this).html('<a href="#" onclick="return false;">'+dataname+'</a>').hide();
                    }
                    */
                    if(jQuery(this).hasClass('hotcity')) {
                        jQuery(this).show();
                    }else{
                        jQuery(this).hide();   
                    }
                });
            }    
        }        
    });
    //select package item
    jQuery('.packagecitylistitem').on('click','li',function() {
        //jQuery(this).parent().prev().val(jQuery(this).attr('data-cityname'));
        jQuery('#arrivedCity').val(jQuery(this).attr('data-cityname'));
        jQuery(this).parent().next().val(jQuery(this).attr('data-citycode'));
    });
    // Cruise select item
    jQuery('#cruise-search .dropdown-menu li a').click(function() {
        var value = jQuery(this).attr('data-id');
        var label = jQuery(this).html();
        jQuery(this).parent().parent().prev().html(label);    
        jQuery(this).parent().parent().parent().next().val(value);    
        jQuery(this).parent().parent().parent().next().next().val(label);    
    });
    //Air class & triptype click
    jQuery('.airclass,.triptype').on('click','li',function() {
        var thisval = jQuery(this).attr('data-id');
        var thistxt = jQuery(this).html();
        jQuery(this).parent().parent().find('span').html(thistxt);
        jQuery(this).parent().parent().next().val(thisval);
    });    
});
var equalHeight = function() {    
    var viewportwidth = jQuery(window).width();
    jQuery('.left-pane,.right-pane').removeAttr('style');
    var leftheight = jQuery('.left-pane').outerHeight();
    var rightheight = jQuery('.right-pane').outerHeight();
    if(viewportwidth>992) {
        var maxheight = 0;
        if(leftheight>rightheight) {
            maxheight = leftheight;
        }else{
            maxheight = rightheight;
        }
        //console.log(maxheight);
        jQuery('.left-pane,.right-pane').css('height',maxheight + 'px');    
    }else{
        jQuery('.left-pane,.right-pane').removeAttr('style');
    }
    setTimeout('equalHeight()',1000);
};
var centerSlideshow = function() {
    var viewportwidth = jQuery(window).width();
    if(viewportwidth>1600) {
        //jQuery('#stravel-slideshow .item img').css('margin-left','auto');
    }else{
        var sew = (1600 - viewportwidth)/2;
        jQuery('#stravel-slideshow .item img').css('margin-left','-'+sew+'px');
    }    
};
var getTotalItems = function() {
    var result = [];
    var i = 0;
    jQuery('.product-container').each(function() {
        result[i] = jQuery(this);
        i++;
    });
    return result;
};
var processFilterData = function() {
    var showeditemid = [];
    jQuery('.listcontainer .page-sep,#airtickethome .listcontainer > .row').remove();
    jQuery('.pagination .pagsseitem').remove();
    jQuery.each(totalitems,function(i,e) {
        var showed = true;
        var itemprice = jQuery(e).attr('data-price');
        var itemstop = jQuery(e).attr('data-stop');
        var itemairline = jQuery(e).attr('data-airline');
        var itemhotelarea = jQuery(e).attr('data-locationid');
        var itemhotelstar = jQuery(e).attr('data-hotelstar');
        var itemhotelcode = jQuery(e).attr('data-hotelcode');
        for(var key in conditions) {
            if(key=='hotelstar') {
                if(conditions.hotelstar.length>0 && conditions.hotelstar.indexOf(itemhotelstar)<0) {
                    showed=false;
                    break;    
                }    
            }
            if(key=='hotelcode') {
                if(itemhotelcode != conditions.hotelcode) {
                    showed = false;
                    break;    
                }
            }
            if(key=='hotelarea') {
                if(conditions.hotelarea>0) {
                    if(parseInt(itemhotelarea) != parseInt(conditions.hotelarea)) {
                        showed = false;
                        break;    
                    }                    
                }
            }
            if(key=='airline') {
                if(conditions.airline.length>0 && conditions.airline.indexOf(itemairline)<0) {
                    showed=false;
                    break;    
                }    
            }
            if(key=='stop') {
                if(conditions.stop==1) {
                    if(itemstop!='directflight') {
                        showed = false;
                        break;
                    }
                }
                if(conditions.stop==2) {
                    if(itemstop!='onestop') {
                        showed = false;
                        break;
                    }
                }
            }
            if(key == 'priceto') {
                if(parseFloat(itemprice) > parseFloat(conditions.priceto)) {
                    showed = false;
                    break;
                }
            }
            if(key == 'pricefrom') {
                if(parseFloat(itemprice) < parseFloat(conditions.pricefrom)) {
                    showed = false;
                    break;
                }
            }
        }
        if(showed==true) {
            showeditemid.push(e);
        }                    
    });
    //Sort items
    
    jQuery.each(showeditemid,function(i,e){
        jQuery.each(showeditemid,function(j,f){
            switch(sortby.direction) {
                case 1:
                    if(parseInt(jQuery(e).attr('data-price'))<parseInt(jQuery(f).attr('data-price'))) {
                       var tg = showeditemid[i];
                       showeditemid[i] = showeditemid[j];
                       showeditemid[j] = tg;    
                    }
                    break;
                case 2:
                    if(parseInt(jQuery(e).attr('data-price'))>parseInt(jQuery(f).attr('data-price'))) {
                       var tg = showeditemid[i];
                       showeditemid[i] = showeditemid[j];
                       showeditemid[j] = tg;    
                    }
                    break;
                case 3:
                    if(parseInt(jQuery(e).attr('data-effectfrom'))<parseInt(jQuery(f).attr('data-effectfrom'))) {
                       var tg = showeditemid[i];
                       showeditemid[i] = showeditemid[j];
                       showeditemid[j] = tg;    
                    }
                    break;
                default:
                    return;
                    break;    
            }
        });    
    });
    //End sort
    var n = showeditemid.length;
    if(jQuery('#hotelresult').length>0) {
        var perrow = 1;
        var perpage = 15;
        var rowperpage = perpage/perrow;     
    }else{
        var perrow = 3;
        var perpage = 12;
        var rowperpage = perpage/perrow;
    }
    if(n<perpage) {
        jQuery('.pagination').css('display','none');
    }else{
        jQuery('.pagination').css('display','inline-block');
    }
    if(n%perpage==0) {
        var page = n/perpage;
    }else{
        var page = Math.floor(n/perpage) + 1;
    }
    if(n%perrow==0) {
        var row = n/perrow;
    }else{
        var row = Math.floor(n/perrow) + 1;
    }
    var rowArray = [];
    for(var i=0;i<=row;i++) {
        var tmprow = jQuery('<div class="row"/>');
        var index = i*perrow;
        if(index<=n){
            tmprow.append(showeditemid[index]);
        }
        var index = i*perrow+1;
        if(index<=n){
            tmprow.append(showeditemid[index]);
        }
        var index = i*perrow+2;
        if(index<=n){
            tmprow.append(showeditemid[index]);
        }
        rowArray.push(tmprow);
    }
    var nn = rowArray.length;
    for(var i=0;i<page;i++) {
        pagination_index = i+1;
        var temppage = jQuery('<div class="page-sep page-sep'+pagination_index+'"/>');
        for(var j=0;j<rowperpage;j++) {
            var index = i*rowperpage+j;
            if(index<=nn){
                temppage.append(rowArray[index]);
            }
        }
        jQuery('.listcontainer').append(temppage);
        if(pagination_index==1) {
            var cls = ' disabled ';
        }else{
            var cls = '';
        }
        if(pagination_index<=5) {
            cls += ' showed ';
        }
        jQuery('.pagination .last').before('<li class="pagsseitem pagsseitem'+pagination_index+cls+'" data-currentpage="1" data-totalpage="'+page+'" data-page="'+pagination_index+'"><a href="#">'+pagination_index+'</a></li>');
        jQuery('.pagination .last,.pagination .first').attr('data-totalpage',page);
    }          
};
var onCloseDatePicker = function(d,o) {
    if(d) {
        if(jQuery(o).attr('id')=='departureDate') {
            var d = new Date(d);
            d.setDate(d.getDate()+1);
            jQuery('#arrivedDate').datepicker('option','minDate',d);
            jQuery('#arrivedDate').datepicker('show');
        }
        if(jQuery(o).attr('id')=='departureDate2') {
            var d = new Date(d);
            d.setDate(d.getDate()+1);
            jQuery('#arrivedDate2').datepicker('option','minDate',d);
            jQuery('#arrivedDate2').datepicker('show');
        }
    }
};
var equalHeightElement = function(els) {
	var viewportwidth = jQuery(window).width();
	els.removeAttr('style');
	if(viewportwidth < 992) {
	   /*
		var maxheight = 0;
		els.each(function() {
			var thiselheight = jQuery(this).height();
			if(thiselheight>maxheight) {
				maxheight = thiselheight;
			}
		});
        */
        var thiselwidth = els.width();
        var maxheight = thiselwidth * 318 / 385;
		els.css('height',maxheight+'px');
	}
};
jQuery(window).resize(function() {
    centerSlideshow();
    equalHeightElement(jQuery('.discount-item-image .maxwidth'));
});
jQuery(window).scroll(function() {
    if (jQuery(this).scrollTop()) {
        jQuery('.iconToTop2').fadeIn();
    } else {
        jQuery('.iconToTop2').fadeOut();
    }
});
