
var Custom = function(){
	
	function init(){
		listPage();
		detailPage();
		cartPage();
		orderPage();
		reportPage();
	};
	function reportPage(){
		jQuery('.datepickerz').datepicker({ dateFormat: "yy-mm-dd" });
		$('#form-report #type').change(
	    function(){
	         $(this).closest('form').trigger('submit');
	        
	    });

		$(".taken-button").click(function(){
			var orderID = $(this).attr('rel');
			$("#orderID").val(orderID);
			$("#myModalLabel").text('Taken');
			$("#reason").val('');
			$("#todo").val('taken');
			$(".form-group.edit").show();
		})
		$(".refund-button").click(function(){
			var orderID = $(this).attr('rel');
			$("#orderID").val(orderID);
			$("#myModalLabel").text('Refund');
			$("#reason").val('');
			$("#todo").val('refund');
			$(".form-group.edit").show();
		})
		$(".edit-taken").click(function(){
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear(); 
			var hour = today.getHours(); 
			var mi = today.getMinutes();
			var second = today.getSeconds();



			if(dd<10) {
			    dd='0'+dd
			} 

			if(mm<10) {
			    mm='0'+mm
			} 
			if(hour<10) {
			    hour='0'+hour
			} 
			if(mi<10) {
			    mi='0'+mi
			} 
			if(second<10) {
			    second='0'+second
			} 

			today = yyyy+'-'+mm+'-'+dd+' '+hour+':'+mi+':'+second;
			
			var orderID = $(this).attr('rel');
			$("#orderID").val(orderID);
			
			$("#reason").val($(this).text());
			$("#todo").val('edittaken');
			$(".form-group.edit").hide();
			$("#modified-date").val($("#unused-modified-"+orderID).val());
			console.log($("#unused-modified-"+orderID).val());
			$("#added-date").val($("#unused-date-"+orderID).val());
			
		})
		$(".edit-refund").click(function(){
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear(); 
			var hour = today.getHours(); 
			var mi = today.getMinutes();
			var second = today.getSeconds();



			if(dd<10) {
			    dd='0'+dd
			} 

			if(mm<10) {
			    mm='0'+mm
			} 
			if(hour<10) {
			    hour='0'+hour
			} 
			if(mi<10) {
			    mi='0'+mi
			} 
			if(second<10) {
			    second='0'+second
			} 

			today = yyyy+'-'+mm+'-'+dd+' '+hour+':'+mi+':'+second;
			

			var orderID = $(this).attr('rel');
			$("#orderID").val(orderID);
			
			$("#reason").val($(this).text());
			$("#todo").val('editrefund');
			$(".form-group.edit").hide();
			$("#modified-date").val($("#refund-modified-"+orderID).val());
			console.log($("#unused-modified-"+orderID).val());
			$("#added-date").val($("#refund-date-"+orderID).val());
		})

		//Sort
		$('.click-sort').click(function(){
			var rel =$(this).attr('rel');
			switch(rel) {
					    case '1':
						    var val ="d.deal_code";
						    var val2 ="desc";
						    if(val==$("#filter_order").val()){
						    	if(val2==$("#filter_order_Dir").val()){
						    		val2="asc";
						    	}
						    }
						    $("#filter_order").val(val);
						    $("#filter_order_Dir").val(val2);
					    break;
					     case '2':
						    var val ="d.name";
						    var val2 ="desc";
						    if(val==$("#filter_order").val()){
						    	if(val2==$("#filter_order_Dir").val()){
						    		val2="asc";
						    	}
						    }
						    $("#filter_order").val(val);
						    $("#filter_order_Dir").val(val2);
					    break;
					      case '3':
						    var val ="a.created_at";
						    var val2 ="desc";
						    if(val==$("#filter_order").val()){
						    	if(val2==$("#filter_order_Dir").val()){
						    		val2="asc";
						    	}
						    }
						    $("#filter_order").val(val);
						    $("#filter_order_Dir").val(val2);
					    break;
					      case '4':
						    var val ="a.total_price";
						    var val2 ="desc";
						    if(val==$("#filter_order").val()){
						    	if(val2==$("#filter_order_Dir").val()){
						    		val2="asc";
						    	}
						    }
						    $("#filter_order").val(val);
						    $("#filter_order_Dir").val(val2);
					    break;
					      case '7':
						    var val ="c.branch";
						    var val2 ="desc";
						    if(val==$("#filter_order").val()){
						    	if(val2==$("#filter_order_Dir").val()){
						    		val2="asc";
						    	}
						    }
						    $("#filter_order").val(val);
						    $("#filter_order_Dir").val(val2);
					    break;
					      case '8':
						    var val ="b.name";
						    var val2 ="desc";
						    if(val==$("#filter_order").val()){
						    	if(val2==$("#filter_order_Dir").val()){
						    		val2="asc";
						    	}
						    }
						    $("#filter_order").val(val);
						    $("#filter_order_Dir").val(val2);
					    break;
					      case '9':
						    var val ="b.status";
						    var val2 ="desc";
						    if(val==$("#filter_order").val()){
						    	if(val2==$("#filter_order_Dir").val()){
						    		val2="asc";
						    	}
						    }
						    $("#filter_order").val(val);
						    $("#filter_order_Dir").val(val2);
					    break;
					      case '10':
						    var val ="b.deallocated_at";
						    var val2 ="desc";
						    if(val==$("#filter_order").val()){
						    	if(val2==$("#filter_order_Dir").val()){
						    		val2="asc";
						    	}
						    }
						    $("#filter_order").val(val);
						    $("#filter_order_Dir").val(val2);
					    break;
					      case '11':
						    var val ="a.unused_date";
						    var val2 ="desc";
						    if(val==$("#filter_order").val()){
						    	if(val2==$("#filter_order_Dir").val()){
						    		val2="asc";
						    	}
						    }
						    $("#filter_order").val(val);
						    $("#filter_order_Dir").val(val2);
					    break;
					      case '12':
						    var val ="a.unused_reason";
						    var val2 ="desc";
						    if(val==$("#filter_order").val()){
						    	if(val2==$("#filter_order_Dir").val()){
						    		val2="asc";
						    	}
						    }
						    $("#filter_order").val(val);
						    $("#filter_order_Dir").val(val2);
					    break;
					      case '13':
						    var val ="a.refund_date";
						    var val2 ="desc";
						    if(val==$("#filter_order").val()){
						    	if(val2==$("#filter_order_Dir").val()){
						    		val2="asc";
						    	}
						    }
						    $("#filter_order").val(val);
						    $("#filter_order_Dir").val(val2);
					    break;
					    
					      case '14':
						    var val ="a.refund_reason";
						    var val2 ="desc";
						    if(val==$("#filter_order").val()){
						    	if(val2==$("#filter_order_Dir").val()){
						    		val2="asc";
						    	}
						    }
						    $("#filter_order").val(val);
						    $("#filter_order_Dir").val(val2);
					    break;
					    
					    default:
					    break;
			}

			$("#reportForm").submit();
		})
		
		$("#submit_modal").click(function(){
			var link = $('#link').val();
			var orderID = $("#orderID").val();
			var reason = $("#reason").val();
			var todo = $("#todo").val();

			$.ajax({
			  type: "POST",
			  dataType: "json",
			  url: link,
			  data: { id: orderID, reason: reason,todo:todo}
			})
			  .done(function( data ) {
			  	console.log(data);
			  	
			  	if(data.status==1){
			  		$('#myModal').modal('hide');
			  		switch(data.todo) {
					    case 'taken':

					        $('#orderItem-'+data.id+' .taken-button').fadeOut();
					        $('#orderItem-'+data.id+' .edit-taken').text(data.reason);
					        $('#orderItem-'+data.id+' .unused-date').text(data.date);
					        $('#orderItem-'+data.id+' .status-text span').text($("#taken-text").val());
					        $("#unused-modified-"+data.id).val(data.dates);
					        $("#unused-date-"+data.id).val(data.dates);
					        break;
					    case 'refund':
					        $('#orderItem-'+data.id+' .refund-button').fadeOut();
					        $('#orderItem-'+data.id+' .edit-refund').text(data.reason);
					         $('#orderItem-'+data.id+' .refund-date').text(data.date);
					         $('#orderItem-'+data.id+' .status-text span').text($("#refunded-text").val());
					          $("#refund-modified-"+data.id).val(data.dates);
					          $("#refund-date-"+data.id).val(data.dates);
					        break;
					    case 'edittaken':
					        $('#orderItem-'+data.id+' .edit-taken').text(data.reason);
					         // $('#orderItem-'+data.id+' .unused-date').text(data.date);
					           $("#unused-modified-"+data.id).val(data.dates);
					        break;
					    case 'editrefund':
					         $('#orderItem-'+data.id+' .edit-refund').text(data.reason);
					          // $('#orderItem-'+data.id+' .refund-date').text(data.date);
					            $("#refund-modified-"+data.id).val(data.dates);
					        break;
					    default:
					        break;
					}
			  	}else{
			  		alert('Fail');
			  	}
			    
			});
		})
	}
	function orderPage(){
		
		    $('#submit-order').bind('click', function(){
		        var txtVal1 =  $('#birthday-c1').val();
		        var txtVal2 =  $('#birthday-c2').val();
		        var submit = true;
		        if(isDate(txtVal1)==false){
		            submit = false;		            
		            $('#birthday-c1').addClass("ui-state-highlight");
		            $('#birthday-c1').focus();
		        }else{
		        	$('#birthday-c1').removeClass("ui-state-highlight");
		        }
		        if(isDate(txtVal2)==false){
		            submit = false;
		           
		            $('#birthday-c2').addClass("ui-state-highlight");
		            $('#birthday-c2').focus();
		        }else{
		        	$('#birthday-c2').removeClass("ui-state-highlight");
		        }
		        return submit;
		    });
		    if($('#birthday-c1').length){
		    	$('#birthday-c1').keyup(function(){
		    		var val1 = $(this).val();
		    		if(isDate(val1)==false){		           
			            
			            $('#birthday-c1').addClass("ui-state-highlight");
			        }else{
			        	$('#birthday-c1').removeClass("ui-state-highlight");
			        }
		    	});
		    	$('#birthday-c2').keyup(function(){
		    		var val2 = $(this).val();
		    		if(isDate(val2)==false){		           
			            
			            $('#birthday-c2').addClass("ui-state-highlight");
			        }else{
			        	$('#birthday-c2').removeClass("ui-state-highlight");
			        }
		    	});
		    }
	};
	function isDate(txtDate){
	    var currVal = txtDate;
	    if(currVal == '')
	        return false;
	    
	    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
	    var dtArray = currVal.match(rxDatePattern); // is format OK?
	    
	    if (dtArray == null) 
	        return false;
	    
	    //Checks for mm/dd/yyyy format.
	    dtDay = dtArray[1];
	    dtMonth= dtArray[3];
	    dtYear = dtArray[5];        
	    
	    if (dtMonth < 1 || dtMonth > 12) 
	        return false;
	    else if (dtDay < 1 || dtDay> 31) 
	        return false;
	    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
	        return false;
	    else if (dtMonth == 2) 
	    {
	        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
	        if (dtDay> 29 || (dtDay ==29 && !isleap)) 
	                return false;
	    }
	    return true;
	}

	
	function cartPage(){
		$('.show-cart').click(function(){
			var id = $(this).attr('rel');
			if($(".pop-"+id).hasClass("show")){				
				$(".pop-"+id).removeClass("show");
				$("#child-"+id).val(0);
				$("#adult-"+id).val(0);

			}else{
				$(".pop-"+id).removeClass("show");
				$(".pop-"+id).addClass("show");
				$("#child-"+id).val(0);
				$("#adult-"+id).val(2);
			}
		})
	}
	function listPage(){
		showMoreDeal();
		
	};
	function showMoreDeal(){
		var row =2;

		jQuery(".more-deal a").click(function(){
			
			jQuery(".product-item.row-"+(row)).addClass("show");
			jQuery(".product-item.row-"+(row+1)).addClass("show");
			jQuery(".product-item.row-"+(row+2)).addClass("show");
			row = row +3;
			if($(".product-item.hide:not(.show)").length == 2){
				$(this).parent().hide();
			}
		})
		
	};
	function detailPage(){
		$(".deal-detail .show-pop-up").click(function(){
			if($(this).parent().find(".pop-up").hasClass("show")){
				
				$(this).parent().find(".pop-up").removeClass("show");
			}else{
				$(".pop-up").removeClass("show");
				$(this).parent().find(".pop-up").addClass("show");
			}
		});

		$("*:not(.pop-up)").click(function(){
			//$(".pop-up").removeClass("show");
		});
		
		$('.order-url').click(function(){
			var url = $("#urlVal").val();
			
			var qty = parseInt($("#qtyVal").val());
			
			url = url+"&qty="+qty;
			//$('#addCart').submit();
			if($('.type-table').length){
				if($(".type-table input").is( ":checked" )){
					$('#addCart').submit();
				}else{
					$('.price-info .pop-up').addClass('show');
					alert('請選擇所需產品');
					return false;
				}
				
			 	
			}else{
				window.location = url;
			}
		})
		$('.show-types').click(function(){
			if($('.type-table').length){
				$('.price-info .pop-up').addClass('show');
			 	return false;
			}
		})
		$('.submit-type').click(function(){
			if($(".type-table input").is( ":checked" )){
				$('.types-selected').show();
				$('.types-selected ol').html("");
				var k =1;
				$(".type-table input:checked").each(function(){
					var li ='<li class="col-sm-12">';
					var li_p ='<li class="clear">';
					var id = $(this).val();
					li= li+$('.type-table .type-'+id).attr('rel');
					li_p= li_p+$('.type-table .type-'+id).attr('rels');
					li= li+$('.type-table #type-'+id).val();
					li_p= li_p+'<span class="pull-right">'+$('.type-table #type-'+id).val()+'</span>';
					li= li+'</li>';
					li_p= li_p+'</li>';
					
					if(k<=3){
						$('.types-selected > ol').append(li);
					}
					$('.types-selected-pop  > ol').append(li_p);
					$('.price-info .pop-up').removeClass('show');
					k++;
					
				})
				if($(".type-table input:checked").length>3){
					$('.more-types').show();
				}else{
					$('.more-types').hide();
				}

			}else{
				alert('請選擇所需產品');
			}
			
			// window.location = url;
		})
		$('.type-change').change(function(){
			var id =$(this).attr('rel');
			var href =$("#url"+id).val();
			href = href+"&qty="+$(this).val();
			$(".url-click-"+id).attr('href',href);
		})
		$('.page-item').click(function(){
			var page = $(this).attr('rel');
			$('.page-tr').hide();
			$('.page-'+page).show();
		})
		
	}
	return{
		init:init
	}
}();
jQuery(document).ready(function() {
	Custom.init();

    $("img.lazy").lazyload({
    	load:function(){
    		equalHeightElement(jQuery('.discount-item-image .maxwidth'));
    	}
    });

})


