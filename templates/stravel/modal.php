<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
define('DS', DIRECTORY_SEPARATOR);
$doc = JFactory::getDocument(); 
$lang = JFactory::getLanguage()->getTag();
$menu = JFactory::getApplication()->getMenu();
if(JRequest::getVar('option')!=='com_stravel') {
    //require_once JPATH_BASE.DS.'components'.DS.'com_stravel'.DS.'helper'.DS.'apihelper.php';
    JFactory::getLanguage()->load('com_stravel',JPATH_BASE.DS.'components'.DS.'com_stravel');
}else{
    $doc->addScriptDeclaration("
    var ScarabQueue = ScarabQueue || []; 
    (function(subdomain, id) { 
      if (document.getElementById(id)) return; 
      var js = document.createElement('script'); js.id = id; 
      js.src = subdomain + '.scarabresearch.com/js/10E2ECA83A6B3527/scarab-v2.js'; 
      var fs = document.getElementsByTagName('script')[0]; 
      fs.parentNode.insertBefore(js, fs); 
    })('https:' == document.location.protocol ? 'https://recommender' : 'http://cdn', 'scarab-js-api');  
    ");
    $branches = apiHelper::getContent(array('CONTENT','B2C','BRANCH'));    
}
?>
<!-- staff Pop Up Modal -->
<div class="modal fade" id="staffModal" tabindex="-1" role="dialog" aria-labelledby="staffModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="staffModalLabel"><?php echo JText::_('COM_STRAVEL_NEWSLETTER_STAFF_FORM_TITLE') ?></h4>
      </div>
      <div class="modal-body">
        <div class="modalform">
            <div class="errormsg">&nbsp;</div>
            <div class="staffForm">
                <form id="staffForm" name="staffForm" autocomplete="false" onsubmit="return false;">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input id="inputYourNameStaff" class="form-control" name="name" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_NAME') ?>" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input id="inputYourLNameStaff" class="form-control" name="lname" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_LNAME') ?>" />
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="dropdown">
                                <span data-target="#" data-toggle="dropdown" class="fakeselectgray selectsex"><?php echo JText::_('COM_STRAVEL_SEX_LABEL') ?></span>
                                <ul class="dropdown-menu">
                                    <li><a href="#" data-id="1" onclick="return false;"><?php echo JText::_('COM_STRAVEL_MALE') ?></a></li>
                                    <li><a href="#" data-id="2" onclick="return false;"><?php echo JText::_('COM_STRAVEL_FEMALE') ?></a></li>
                                    <!-- <li><a href="#" data-id="O" onclick="return false;"><?php echo JText::_('COM_STRAVEL_OTHER_SEX') ?></a></li> -->
                                </ul>
                                <input type="hidden" name="inputYourSexStaff" id="inputYourSexStaff" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="dropdown">
                                <span data-target="#" data-toggle="dropdown" class="fakeselectgray selectsex"><?php echo JText::_('COM_STRAVEL_BIRTHDAY_MONTH') ?></span>
                                <ul class="dropdown-menu">
                                    <?php 
                                        for($i=1;$i<=12;$i++) {
                                            echo '<li><a href="#" onclick="return false;" data-id="'.$i.'">'.$i.JText::_('COM_STRAVEL_MONTH').'</a></li>';
                                        }
                                    ?>
                                </ul>
                                <input type="hidden" name="inputYourBirthMonthStaff" id="inputYourBirthMonthStaff" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="dropdown">
                                <span data-target="#" data-toggle="dropdown" class="fakeselectgray selectdate"><?php echo JText::_('COM_STRAVEL_BIRTHDAY_DAY') ?></span>
                                <ul class="dropdown-menu selectdate">
                                    <?php 
                                        for($i=1;$i<=31;$i++) {
                                            echo '<li><a href="#" onclick="return false;" data-id="'.$i.'">'.$i.JText::_('COM_STRAVEL_DAY').'</a></li>';
                                        }
                                    ?>
                                </ul>
                                <input type="hidden" name="inputYourBirthDateStaff" id="inputYourBirthDateStaff" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input id="inputYourEmailStaff" class="form-control" name="email" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_EMAIL') ?>" />
                </div>
                <div class="form-group">
                    <input id="inputYourNumberStaff" class="form-control" name="number" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_CONTACT_NUMBER') ?>" />
                </div>
                <div class="staffExtend">
                    <div class="form-group">
                        <input id="inputYourProductNameStaff" class="form-control" name="pname" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_PRODUCT_NAME') ?>" />
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input id="inputYourDestinationStaff" class="form-control" name="destination" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_DESTINATION') ?>" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input id="inputYourPersonNoStaff" class="form-control" name="personno" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_NUMBER_OF_PERSON') ?>" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input id="inputYourSalePriceStaff" class="form-control" name="saleprice" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_SALE_PRICE') ?>" />
                            </div>
                        </div>                        
                    </div>
                    <div class="row">
                        <div class="col-md-2"><span class="deptitle"><?php echo JText::_('COM_STRAVEL_DEPARTURE_DATE') ?></span></div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input id="inputYourDepYearStaff" class="form-control" name="depyearstaff" placeholder="<?php echo JText::_('COM_STRAVEL_YEAR') ?>" />
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group dropdown">
                                <span data-target="#" data-toggle="dropdown" class="fakeselectgray selectsex"><?php echo JText::_('COM_STRAVEL_MONTH') ?></span>
                                <ul class="dropdown-menu">
                                    <?php 
                                        for($i=1;$i<=12;$i++) {
                                            echo '<li><a href="#" onclick="return false;" data-id="'.$i.'">'.$i.JText::_('COM_STRAVEL_MONTH').'</a></li>';
                                        }
                                    ?>
                                </ul>
                                <input type="hidden" id="inputYourDepMonthStaff" class="form-control" name="depmonthstaff" placeholder="<?php echo JText::_('COM_STRAVEL_MONTH') ?>" />
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group dropdown">
                                <span data-target="#" data-toggle="dropdown" class="fakeselectgray selectdate"><?php echo JText::_('COM_STRAVEL_DAY') ?></span>
                                <ul class="dropdown-menu selectdate">
                                    <?php 
                                        for($i=1;$i<=31;$i++) {
                                            echo '<li><a href="#" onclick="return false;" data-id="'.$i.'">'.$i.JText::_('COM_STRAVEL_DAY').'</a></li>';
                                        }
                                    ?>
                                </ul>
                                <input type="hidden" id="inputYourDepDayStaff" class="form-control" name="depdaystaff" placeholder="<?php echo JText::_('COM_STRAVEL_DAY') ?>" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group dropdown">
                                <span data-target="#" data-toggle="dropdown" class="fakeselectgray"><?php echo JText::_('COM_STRAVEL_INPUT_YOUR_FEEDBACK') ?></span>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="return false;" data-id="Success">Success</a></li>
                                    <li><a href="#" onclick="return false;" data-id="Pending">Pending</a></li>
                                    <li><a href="#" onclick="return false;" data-id="Cancel">Cancel</a></li>
                                </ul>
                                <input type="hidden" id="inputYourFeedBackStaff" class="form-control" name="feedback" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_FEEDBACK') ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea id="inputYourRemarkStaff" class="form-control" name="remark" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_REMARK') ?>"></textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group dropdown">
                                <span data-target="#" data-toggle="dropdown" class="fakeselectgray"><?php echo JText::_('COM_STRAVEL_INPUT_YOUR_SOURCE') ?></span>
                                <ul class="dropdown-menu dropup-source">
                                    <li><a href="#" onclick="return false;" data-id="walkin">Walk In</a></li>
                                    <li><a href="#" onclick="return false;" data-id="phonein">Phone In</a></li>
                                    <li><a href="#" onclick="return false;" data-id="emailin">Email In</a></li>
                                    <li><a href="#" onclick="return false;" data-id="facebook">Facebook</a></li>
                                    <li><a href="#" onclick="return false;" data-id="whatsapp">Whatsapp</a></li>
                                    <li><a href="#" onclick="return false;" data-id="referral">Referral</a></li>
                                    <li><a href="#" onclick="return false;" data-id="repeat">Repeat</a></li>
                                    <li><a href="#" onclick="return false;" data-id="groupbuying">Group Buying</a></li>
                                </ul>
                                <input type="hidden" id="inputYourSourceStaff" class="form-control" name="source" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_SOURCE') ?>" />
                            </div>    
                        </div>
                        <div class="col-md-4">
                            <div class="form-group dropdown">
                                <span data-target="#" data-toggle="dropdown" class="fakeselectgray"><?php echo JText::_('COM_STRAVEL_INPUT_YOUR_BRANCH') ?></span>
                                <ul class="dropdown-menu selectdate dropup-branch">
                                    <li><a href="#" onclick="return false;" data-id="1">觀塘</a></li>
                                    <li><a href="#" onclick="return false;" data-id="2">旺角</a></li>
                                    <li><a href="#" onclick="return false;" data-id="3">九龍灣</a></li>
                                    <li><a href="#" onclick="return false;" data-id="4">中環</a></li>
                                    <li><a href="#" onclick="return false;" data-id="5">銅鑼灣</a></li>
                                    <li><a href="#" onclick="return false;" data-id="6">杏花村</a></li>
                                    <li><a href="#" onclick="return false;" data-id="7">香港仔</a></li>
                                    <li><a href="#" onclick="return false;" data-id="8">荃灣</a></li>
                                    <li><a href="#" onclick="return false;" data-id="9">葵芳</a></li>
                                    <li><a href="#" onclick="return false;" data-id="10">將軍澳</a></li>
                                    <li><a href="#" onclick="return false;" data-id="11">馬鞍山</a></li>
                                    <li><a href="#" onclick="return false;" data-id="12">沙田</a></li>
                                    <li><a href="#" onclick="return false;" data-id="13">上水</a></li>
                                    <li><a href="#" onclick="return false;" data-id="14">元朗</a></li>
                                </ul>
                                <input type="hidden" id="inputYourBranchStaff" class="form-control" name="branch" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_BRANCH') ?>" />    
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input id="inputYourSNumberStaff" class="form-control" name="snumber" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_STAFF_NUMBER') ?>" />
                            </div>
                        </div>
                    </div>    
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-button text-right">
                            <button class="graybtn submitbtn newsleter" type="button"><?php echo JText::_('COM_STRAVEL_NEWSLETTER_SUBMIT') ?></button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="modalthankyou">
            <div class="thankyoutext text-center"><?php echo JText::_('COM_STRAVEL_THANKYOU_TEXT') ?></div>
            <div class="text-right"><a class="close2continuebtn" data-dismiss="modal"><?php echo JText::_('COM_STRAVEL_CLOSE_TO_CONTINUE') ?></a></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End staff Pop Up -->
<!-- Enquire newsletter Pop Up Modal -->
<div class="modal fade" id="enquiryNewsletterModal" tabindex="-1" role="dialog" aria-labelledby="enquiryNewsletterModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="enquiryNewsletterModalLabel"><?php echo JText::_('COM_STRAVEL_NEWSLETTER_ENQUIRY_TITLE') ?></h4>
      </div>
      <div class="modal-body">
        <div class="modalform">
            <div class="errormsg">&nbsp;</div>
            <div class="enquiryNewsletterForm">
                <form id="enquiryNewsletterForm" name="enquiryNewsletterForm" onsubmit="return false;">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input id="inputYourNameNewsletter" class="form-control" name="name" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_NAME') ?>" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input id="inputYourLNameNewsletter" class="form-control" name="lname" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_LNAME') ?>" />
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="dropdown">
                                <span data-target="#" data-toggle="dropdown" class="fakeselectgray selectsex"><?php echo JText::_('COM_STRAVEL_SEX_LABEL') ?></span>
                                <ul class="dropdown-menu">
                                    <li><a href="#" data-id="1" onclick="return false;"><?php echo JText::_('COM_STRAVEL_MALE') ?></a></li>
                                    <li><a href="#" data-id="2" onclick="return false;"><?php echo JText::_('COM_STRAVEL_FEMALE') ?></a></li>
                                    <!-- <li><a href="#" data-id="O" onclick="return false;"><?php echo JText::_('COM_STRAVEL_OTHER_SEX') ?></a></li> -->
                                </ul>
                                <input type="hidden" name="inputYourSexNewsletter" id="inputYourSexNewsletter" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="dropdown">
                                <span data-target="#" data-toggle="dropdown" class="fakeselectgray selectsex"><?php echo JText::_('COM_STRAVEL_BIRTHDAY_MONTH') ?></span>
                                <ul class="dropdown-menu">
                                    <?php 
                                        for($i=1;$i<=12;$i++) {
                                            echo '<li><a href="#" onclick="return false;" data-id="'.$i.'">'.$i.JText::_('COM_STRAVEL_MONTH').'</a></li>';
                                        }
                                    ?>
                                </ul>
                                <input type="hidden" name="inputYourBirthMonthNewsletter" id="inputYourBirthMonthNewsletter" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="dropdown">
                                <span data-target="#" data-toggle="dropdown" class="fakeselectgray selectdate"><?php echo JText::_('COM_STRAVEL_BIRTHDAY_DAY') ?></span>
                                <ul class="dropdown-menu selectdate">
                                    <?php 
                                        for($i=1;$i<=31;$i++) {
                                            echo '<li><a href="#" onclick="return false;" data-id="'.$i.'">'.$i.JText::_('COM_STRAVEL_DAY').'</a></li>';
                                        }
                                    ?>
                                </ul>
                                <input type="hidden" name="inputYourBirthDateNewsletter" id="inputYourBirthDateNewsletter" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input id="inputYourEmailNewsletter" class="form-control" name="email" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_EMAIL') ?>" />
                </div>
                <div class="form-group">
                    <input id="inputYourNumberNewsletter" class="form-control" name="number" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_CONTACT_NUMBER') ?>" />
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="checkbox">
                            <label>
                                <input id="inputConfirmNewsletter" type="checkbox" checked="checked"> <?php echo JText::_('COM_STRAVEL_RECEIVE_NEWSLETER_LABEL') ?>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-button text-right">
                            <button class="graybtn submitbtn newsleter" type="button"><?php echo JText::_('COM_STRAVEL_NEWSLETTER_SUBMIT') ?></button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="modalthankyou">
            <div class="thankyoutext text-center"><?php echo JText::_('COM_STRAVEL_THANKYOU_TEXT') ?></div>
            <div class="text-right"><a class="close2continuebtn" data-dismiss="modal"><?php echo JText::_('COM_STRAVEL_CLOSE_TO_CONTINUE') ?></a></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Enquire Pop Up -->
<!-- Enquire General Pop Up Modal -->
<div class="modal fade" id="enquiryGeneralModal" tabindex="-1" role="dialog" aria-labelledby="enquiryGeneralModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="enquiryGeneralModalLabel"><?php echo JText::_('COM_STRAVEL_PRODUCT_ENQUIRY_TITLE') ?></h4>
      </div>
      <div class="modal-body">
        <div class="modalform">
            <div class="errormsg">&nbsp;</div>
            <div class="enqueryGeneralForm">
                <form id="enquiryGeneralModalForm" name="enqueryGeneralForm" onsubmit="return false;">
                <div class="form-group">
                    <input id="inputYourNameGeneral" class="form-control" name="name" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_NAME') ?>" />
                </div>
                <div class="form-group">
                    <input id="inputYourEmailGeneral" class="form-control" name="email" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_EMAIL') ?>" />
                </div>
                <div class="form-group">
                    <input id="inputYourNumberGeneral" class="form-control" name="number" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_CONTACT_NUMBER') ?>" />
                </div>
                <div class="form-group">
                    <select id="inputTypeGeneral" class="form-control" name="enquerytype">
                        <option value="1"><?php echo JText::_('COM_STRAVEL_GENERAL_ENQUERY') ?></option>
                        <option value="2"><?php echo JText::_('COM_STRAVEL_OPINION_ENQUERY') ?></option>
                    </select>
                </div>
                <div class="form-group">
                    <textarea id="inputYourMessageGeneral" class="form-control" name="message" cols="10" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_MESSAGE') ?>"></textarea>
                </div>
                <div class="form-button text-right">
                    <button class="graybtn" type="reset"><?php echo JText::_('COM_STRAVEL_RESET') ?></button>
                    <button class="graybtn submitbtn" type="button"><?php echo JText::_('COM_STRAVEL_SUBMIT') ?></button>
                </div>
                </form>
            </div>
        </div>
        <div class="modalthankyou">
            <div class="thankyoutext text-center"><?php echo JText::_('COM_STRAVEL_THANKYOU_TEXT') ?></div>
            <div class="text-right"><a class="close2continuebtn" data-dismiss="modal"><?php echo JText::_('COM_STRAVEL_CLOSE_TO_CONTINUE') ?></a></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Enquire Pop Up -->
<?php if(!empty($branches)) : ?>
<!-- Enquire Pop Up Modal -->
<div class="modal fade" id="enquiryModal" tabindex="-1" role="dialog" aria-labelledby="enquiryModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="enquiryModalLabel"><?php echo JText::_('COM_STRAVEL_PRODUCT_ENQUIRY_TITLE') ?></h4>
      </div>
      <div class="modal-body">
        <div class="modalform">
            <div class="errormsg">&nbsp;</div>
            <div class="productEnqueryInfo"></div>
            <div class="productEnqueryForm">
                <form id="enquiryModalForm" name="productEnqueryForm" onsubmit="return false;">
                <div class="form-group hide">
                    <select id="selectBranch" class="form-control" name="branch">
                        <option value="0"><?php echo JText::_('COM_STRAVEL_SELECT_BRANCH') ?></option>
                        <?php 
                            foreach($branches as $branch) : 
                            $infos = explode("\n",$branch->Content);
                            $label = array_shift($infos);
                            $value = implode(',',$infos);
                        ?>
                        <option value="<?php echo $value; ?>"><?php echo $label; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <input id="inputYourName" class="form-control" name="name" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_NAME') ?>" />
                </div>
                <div class="form-group">
                    <input id="inputYourEmail" class="form-control" name="email" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_EMAIL') ?>" />
                </div>
                <div class="form-group">
                    <input id="inputYourNumber" class="form-control" name="number" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_CONTACT_NUMBER') ?>" />
                </div>
                <div class="form-group">
                    <textarea id="inputYourMessage" class="form-control" name="message" cols="10" placeholder="<?php echo JText::_('COM_STRAVEL_INPUT_YOUR_MESSAGE') ?>"></textarea>
                </div>
                <div class="form-button text-right">
                    <button class="graybtn" type="reset"><?php echo JText::_('COM_STRAVEL_RESET') ?></button>
                    <button class="graybtn submitbtn" type="button"><?php echo JText::_('COM_STRAVEL_SUBMIT') ?></button>
                    <?php if(($menu->getActive() != $menu->getDefault())) : ?>
                    <div class="googleremartket">
                        <!-- Google Code for Google 10-20 Conversion Page -->
                        <script type="text/javascript">
                        /* <![CDATA[ */
                        var google_conversion_id = 965813877;
                        var google_conversion_language = "en";
                        var google_conversion_format = "2";
                        var google_conversion_color = "ffffff";
                        var google_conversion_label = "Ckb4CLOC-1YQ9czEzAM";
                        var google_remarketing_only = false;
                        /* ]]> */
                        </script>
                        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                        </script>
                        <noscript>
                        <div style="display:inline;">
                        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/965813877/?label=Ckb4CLOC-1YQ9czEzAM&amp;guid=ON&amp;script=0"/>
                        </div>
                        </noscript>
                    </div>
                    <?php endif; ?>
                </div>
                <input type="hidden" name="frompage" id="inputFromPage" />
                <input type="hidden" name="productid" id="inputProductId" />
                <input type="hidden" name="productname" id="inputProductName" />
                <input type="hidden" name="productprice" id="inputProductPrice" />
                <input type="hidden" name="productnights" id="inputProductNights" />
                <input type="hidden" name="productdate" id="inputProductDate" />
                <input type="hidden" name="producttrip" id="inputProductTrip" />
                <input type="hidden" name="productclass" id="inputProductClass" />
                <input type="hidden" name="remark" id="inputRemark" />
                <input type="hidden" name="productdst" id="inputProductDst" />
                <input type="hidden" name="productship" id="inputProductShip" />
                </form>
            </div>
        </div>
        <div class="modalthankyou">
            <div class="thankyoutext text-center"><?php echo JText::_('COM_STRAVEL_THANKYOU_TEXT') ?></div>
            <div class="text-right"><a class="close2continuebtn" data-dismiss="modal"><?php echo JText::_('COM_STRAVEL_CLOSE_TO_CONTINUE') ?></a></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Enquire Pop Up -->
<?php endif; ?>

<!-- Mmodal for air ticket -->
<div id="myLargeAirTicketModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <div data-dismiss="modal" class="closebutton">x</div>
        <div class="labelremark">
            <?php echo JText::_('COM_STRAVEL_LABEL_REMARK'); ?>    
        </div>
        <div class="remarkcontent"></div>
        <div class="hide"><button class="btn btn-danger" id="testtime">Timeline</button><button class="btn btn-success showtermbtn">Terms</button></div>
        <div class="timeline-list"></div>
      </div>
    </div>
  </div>
</div>
<!-- End modal for air ticket -->

<!-- Modal for hotel -->
<div id="myHotelSearchModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myHotelSearchModalLabel"><?php echo JText::_('COM_STRAVEL_SEARCH_HOTEL') ?></h4>
        </div>
        <div class="modal-body">
            <div class="hotelsearchform">
                <form action="index.php" method="get" role="form" autocomplete="off">
                    <input type="hidden" name="option" value="com_stravel" />
                    <input type="hidden" name="view" value="hotelresult" />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="destination"><?php echo JText::_('COM_STRAVEL_HOTEL_NAME') ?></label>
                                <div class="hotelcitylist dropdown">
                                    <input id="destination" data-target="#" data-toggle="dropdown" type="text" class="form-control" name="destination">
                                    <ul class="hotelcitylistitem dropdown-menu">
                                    </ul>
                                    <input type="hidden" name="city" />
                                </div>                                    
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="checkinDateModal"><?php echo JText::_('COM_STRAVEL_CHECKIN_DATE') ?></label>
                                <input name="checkinDate" type="text" class="form-control datepicker" id="checkinDateModal">
                            </div> 
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label><?php echo JText::_('COM_STRAVEL_NIGHTS') ?></label>
                                <div class="dropdown hotelnights">
                                    <span data-target="#" data-toggle="dropdown" class="fakeselect form-control">1</span>
                                    <ul class="dropdown-menu">
                                        <?php 
                                            for($i=1;$i<=14;$i++) {
                                                echo '<li><a href="#" data-id="'.$i.'" onclick="return false;">'.$i.'</a></li>';
                                            } 
                                        ?>
                                    </ul>
                                </div>                                
                                <input type="hidden" name="nights" value="1">
                            </div> 
                        </div>                    
                    </div>
                    <div class="row submit">
                        <div class="col-md-12">
                            <button class="pull-right" type="submit"><?php echo JText::_('SEARCH'); ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
  </div>
</div>
<!-- End modal for hotel -->

<!-- Mmodal for air ticket -->
<div id="myLargeHotelResultModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <div data-dismiss="modal" class="closebutton">x</div>
        <div class="loading text-center"><i class="fa fa-spin fa-3x fa-spinner"></i></div>
        <div class="hotelinfo">            
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End modal for air ticket -->

<!-- Modal Loading -->
<div id="loadingModal" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="loadingimg">&nbsp;</div>
      </div>
    </div>
  </div>
</div>
<!-- End modal loading -->