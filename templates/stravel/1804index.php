<?php defined( '_JEXEC' ) or die;
header('Content-Type: text/html; charset=utf-8');

$useragent = preg_match('/(Firefox|Chrome)/',$_SERVER['HTTP_USER_AGENT'],$browser);
if(!empty($browser)) {
    $browser = strtolower($browser[1]);
}else{
    $browser = 'others';
}
// variables
$app = JFactory::getApplication();
$doc = JFactory::getDocument(); 
$menu = $app->getMenu();
$active = $app->getMenu()->getActive();
$params = $app->getParams();
$pageclass = $params->get('pageclass_sfx');
$tpath = $this->baseurl.'/templates/'.$this->template;

// generator tag
$this->setGenerator(null);

// template css
$doc->addStyleSheet($tpath.'/css/template.css.php');
//template js
if(JRequest::getVar('option')!=='com_enmasse') {
$doc->addScript($tpath.'/js/jquery-2.1.1.min.js');
}else{
   $doc->addStyleSheet($tpath.'/css/custom.css');
}
$doc->addScript($tpath.'/js/jquery-ui.min.js');
$doc->addScript($tpath.'/js/bootstrap.min.js');
// Add jsslider
$doc->addScript($tpath.'/js/jshashtable-2.1_src.js');
//$doc->addScript($tpath.'/js/jquery.themepunch.revolution.min.js');
$doc->addScript($tpath.'/js/jquery.numberformatter-1.2.3.js');
$doc->addScript($tpath.'/js/tmpl.js');
$doc->addScript($tpath.'/js/jquery.dependClass-0.1.js');
$doc->addScript($tpath.'/js/draggable-0.1.js');
$doc->addScript($tpath.'/js/jquery.slider.js');
$doc->addScript($tpath.'/js/stravel.js');
$doc->addScript($tpath.'/js/custom.js');
if(file_exists(JPATH_BASE.'/js/cmsjs.js')) {
    $doc->addScript(JURI::root().'js/cmsjs.js');    
}
$nonresponse = 0;
if($nonresponse) {
    $doc->addStyleDeclaration('.container {width:1170px !important;}');
    $doc->addScriptDeclaration('
        jQuery(document).ready(function() {
            for(i=1;i<=12;i++) {
                jQuery(".col-md-"+i).removeClass("col-md-"+i).addClass("col-xs-"+i);
                jQuery(".col-lg-"+i).removeClass("col-lg-"+i).addClass("col-xs-"+i);
                jQuery(".col-sm-"+i).removeClass("col-sm-"+i).addClass("col-xs-"+i);
            }
        });
    ');
}
?>
<!doctype html>

<html lang="<?php echo $this->language; ?>">

<head>
  <meta charset="UTF-8">
  <jdoc:include type="head" />
  <?php if($nonresponse) : ?>
  <!-- Disable responsiveness -->
  <?php else : ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
  <?php endif; ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="<?php echo $tpath.'/js/html5shiv.min.js' ?>"></script>
      <script src="<?php echo $tpath.'/js/respond.min.js' ?>"></script>
    <![endif]-->
  <!-- fix phone number in safari iphone -->
  <meta name="format-detection" content="telephone=no"/>
  
  <link rel="apple-touch-icon-precomposed" href="<?php echo $tpath; ?>/images/76pt_stravel_icon.jpg">
  <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $tpath; ?>/images/120pt_stravel_icon.jpg">
  <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $tpath; ?>/images/152pt_stravel_icon.jpg">
</head>
  
<body class="<?php echo $browser.' ';echo (($menu->getActive() == $menu->getDefault()) ? ('front') : ('site')).' '.$active->alias.' '.$pageclass; ?>"> 
<?php if($this->countModules('top')) : ?>
    <section id="top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <jdoc:include type="modules" name="top" style="html5"/>   
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>     

  <header>  
    <div class="container">
        <div class="row">
            <div class="col-md-2 logo">
                <?php if($this->countModules('logo')) : ?>
                    <jdoc:include type="modules" name="logo" style="html5"/>
                <?php else : ?>
                    <a href="<?php echo JURI::root(); ?>"><img src="<?php echo $tpath.'/images/logo.png' ?>" alt="<?php echo JFactory::getConfig()->get('sitename'); ?>" /></a>
                <?php endif; ?>
            </div>
            <div class="col-md-10">
                <?php if($this->countModules('topinfo')) : ?>
                    <div class="topinfo">
                        <jdoc:include type="modules" name="topinfo" style="html5"/>
                    </div>
                <?php endif; ?>  
                <?php if($this->countModules('mainnav-1')) : ?>
                    <div class="mainnav-1">
                        <jdoc:include type="modules" name="mainnav-1" style="html5"/>
                    </div>
                <?php endif; ?>    
            </div>
        </div>
        <?php if($this->countModules('mainnav-2')) : ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="mainnav">
                <jdoc:include type="modules" name="mainnav-2" style="html5"/>
                </div>    
            </div>
        </div>
        <?php endif; ?>
    </div>
  </header>
  <?php if($this->countModules('breadcrumbs')) : ?>
  <div id="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <jdoc:include type="modules" name="breadcrumbs" />        
            </div>
        </div>
    </div>
  </div>
  <?php endif; ?>
  
    <?php if($this->countModules('slide')) : ?>
    <section id="slide">
        <jdoc:include type="modules" name="slide" style="slide" />
    </section>
    <?php endif; ?>    
  
  
  <div id="maincontent">
    <!-- Section for system message -->
    <jdoc:include type="message" />
    <!-- End section for system message -->
    <!-- Show main component content -->
    <jdoc:include type="component" />
    <!-- End main component content -->
  </div>  
  
  
  <!-- Footer -->
  <section id="abovefooter">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <?php if($this->countModules('foot1')) : ?>
                    <jdoc:include type="modules" name="foot1" style="html5" />
                <?php endif; ?>
            </div>
            <div class="col-md-2">
                <?php if($this->countModules('foot2')) : ?>
                    <jdoc:include type="modules" name="foot2" style="html5" />
                <?php endif; ?>
            </div>
            <div class="col-md-2">
                <?php if($this->countModules('foot3')) : ?>
                    <jdoc:include type="modules" name="foot3" style="html5"/>
                <?php endif; ?>
            </div>
            <div class="col-md-2">
                <?php if($this->countModules('foot4')) : ?>
                    <jdoc:include type="modules" name="foot4" style="html5"/>
                <?php endif; ?>
            </div>
            <div class="col-md-2">
                <?php if($this->countModules('foot5')) : ?>
                    <jdoc:include type="modules" name="foot5" style="html5"/>
                <?php endif; ?>
            </div>
            <div class="col-md-2">
                <div class="extendright">
                    <?php if($this->countModules('foot6')) : ?>
                        <jdoc:include type="modules" name="foot6" style="html5"/>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
  </section>
  <footer>
    <div class="footermenu">
        <?php if($this->countModules('footermenu')) : ?>
            <jdoc:include type="modules" name="footermenu" />
        <?php endif; ?>    
    </div>
    <div class="totop text-center">
        <span class="iconToTop"><i class="fa fa-angle-up fa-2x"></i></span>
    </div>   
  </footer>
  <span class="iconToTop2"><i class="fa fa-arrow-up"></i></span>
  <!-- End footer -->
  <jdoc:include type="modules" name="debug" />
  <?php include_once('modal.php'); ?>
  <?php include_once("analyticstracking.php") ?>
  <?php if(!defined('GOOGLE_CONVERSATION_EXISTED')) : ?>
  <!-- Google remartketing -->
  <script type="text/javascript">
	
    /* <![CDATA[ */
    var google_conversion_id = 965813877;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/965813877/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
  <!-- End Google remarketing -->
  <?php endif; ?>
  <script type="text/javascript">if(typeof wabtn4fg==="undefined"){wabtn4fg=1;h=document.head||document.getElementsByTagName("head")[0],s=document.createElement("script");s.type="text/javascript";s.src="<?php echo JURI::root().'templates/stravel'?>/js/whatsapp-button.js";h.appendChild(s);}</script>
</body>

</html>