<?php defined( '_JEXEC' ) or die;

// variables
$app = JFactory::getApplication();
$doc = JFactory::getDocument(); 
$tpath = $this->baseurl.'/templates/'.$this->template;
$attribs['style'] = 'html5';
?><!doctype html>

<html lang="<?php echo $this->language; ?>">

<head>
  <meta charset="UTF-8">  
  <title><?php echo $this->error->getCode(); ?> - <?php echo $this->title; ?></title>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" /> <!-- mobile viewport optimized -->
  <link rel="stylesheet" href="<?php echo $tpath; ?>/css/template.css.php">
</head>

<body>
    <header>  
        <div class="container">
            <div class="row">
                <div class="col-md-2 logo">
                        <a href="<?php echo JURI::root(); ?>"><img src="<?php echo $tpath.'/images/logo.png' ?>" alt="<?php echo htmlspecialchars(JFactory::getConfig()->get('sitename')); ?>" /></a>
                </div>
                <div class="col-md-10">
                    <div class="topinfo">
                    <?php // render module mod_search
                        $module = JModuleHelper::getModule('menu','topinfo');
                        echo JModuleHelper::renderModule($module);
                      ?>
                    </div>      
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="mainnav">
                        <?php // render module mod_search
                            $module = JModuleHelper::getModule('menu','Main Navigation');
                            echo JModuleHelper::renderModule($module);
                          ?>
                    </div>    
                </div>
            </div>
        </div>
    </header>
  <div align="center">
    <div id="error">
      <h1>
        <?php echo htmlspecialchars($app->getCfg('sitename')); ?>
      </h1>
      <p>
        <?php 
          echo '<h2 style="font-size:50px;">'.$this->error->getCode().' '.JText::_('JERROR_ERROR').'</h2>'; 
          if (($this->error->getCode()) == '404') {
            //echo '<br />';
            echo JText::_('JERROR_LAYOUT_REQUESTED_RESOURCE_WAS_NOT_FOUND');
          }
        ?>
      </p>
      <p>
        <?php echo JText::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE'); ?>: 
        <a href="<?php echo $this->baseurl; ?>/"><?php echo JText::_('JERROR_LAYOUT_HOME_PAGE'); ?></a>.
      </p>
    </div>
  </div>
  
  <!-- Footer -->
  <section id="abovefooter">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <?php // render module mod_search
                    $module = JModuleHelper::getModules('foot1');
                    foreach($module as $mod) {
                        echo str_replace('images/',JURI::root().'images/',JModuleHelper::renderModule($mod,$attribs));    
                    }                    
                  ?>
            </div>
            <div class="col-md-2">
                <?php // render module mod_search
                    $module = JModuleHelper::getModules('foot2');
                    foreach($module as $mod) {
                        echo JModuleHelper::renderModule($mod,$attribs);    
                    }
                  ?>
            </div>
            <div class="col-md-2">
                <?php // render module mod_search
                    $module = JModuleHelper::getModules('foot3');
                    foreach($module as $mod) {
                        echo JModuleHelper::renderModule($mod,$attribs);    
                    }
                  ?>
            </div>
            <div class="col-md-2">
                <?php // render module mod_search
                    $module = JModuleHelper::getModules('foot4');
                    foreach($module as $mod) {
                        echo JModuleHelper::renderModule($mod,$attribs);    
                    }
                  ?>
            </div>
            <div class="col-md-2">
                <?php // render module mod_search
                    $module = JModuleHelper::getModules('foot5');
                    foreach($module as $mod) {
                        echo JModuleHelper::renderModule($mod,$attribs);    
                    }
                  ?>
            </div>
            <div class="col-md-2">
                <?php // render module mod_search
                    $module = JModuleHelper::getModules('foot6');
                    foreach($module as $mod) {
                        echo str_replace('images/',JURI::root().'images/',JModuleHelper::renderModule($mod,$attribs)); 
                    }
                  ?>
            </div>
        </div>
    </div>
  </section>
  <footer>
    <div class="footermenu">
        <?php // render module mod_search
            $module = JModuleHelper::getModule('menu','Footer bottom navigation');
            echo JModuleHelper::renderModule($module);
          ?>    
    </div>
    <div class="totop text-center">
        <span class="iconToTop"><i class="fa fa-angle-up fa-2x"></i></span>
    </div>   
  </footer>
  <!-- End footer -->
  <?php require 'modal.php'; ?>
  <script src="<?php echo $tpath.'/js/jquery-2.1.1.min.js' ?>"></script>
  <script src="<?php echo $tpath.'/js/bootstrap.min.js' ?>"></script>
</body>

</html>
