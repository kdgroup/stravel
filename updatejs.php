<?php 
set_time_limit(0); 
define( '_JEXEC', 1 );
define('JPATH_BASE', dirname(__FILE__));
define( 'DS', DIRECTORY_SEPARATOR );
require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );
$mainframe =& JFactory::getApplication('site');
$mainframe->initialise(); 
jimport('joomla.filesystem.file');
$apiheper = JPATH_BASE.DS.'components'.DS.'com_stravel'.DS.'helper'.DS.'apihelper.php';
if(JFile::exists($apiheper)) {
    require $apiheper;
}else{
    echo $apiheper;
    echo json_encode(array('success'=>0,'message'=>'Could not find API script!'));
    exit;
}
$java = apiHelper::getJavascriptData();
$jsfile = JPATH_BASE.DS.'js'.DS.'cmsjs.js';

$result = JFile::write($jsfile,$java);  
if($result) {
    echo json_encode(array('success'=>1,'message'=>'Save successfully.'));
    exit;    
}else{
    echo json_encode(array('success'=>0,'message'=>'Could not save script!'));
    exit;
}
?>