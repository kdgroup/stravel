<?php
defined('_JEXEC') or die;

class sTravelRouter extends JComponentRouterBase
{
	/**
	 * Build the route for the com_content component
	 *
	 * @param   array  &$query  An array of URL arguments
	 *
	 * @return  array  The URL arguments to use to assemble the subsequent URL.
	 *
	 * @since   3.3
	 */
	public function build(&$query)
	{
		$segments = array();
		// Get a menu item based on Itemid or currently active
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
        $activeMenu = $menu->getActive();
        if(isset($query['view'])) {
            $view = $query['view'];
            switch($view) {
                case 'packagehome':
                    $link = $menu->getItems('link','index.php?option=com_stravel&view=packagehome',true);
                    if(isset($query['city']) && isset($query['cityname'])) {
                        $segments[] = $query['cityname'] . '-c' . $query['city'];                        
                    }
                    unset($query['city'],$query['cityname']);
                    
                    if(isset($query['departureDate'])) {
                        $segments[] = $query['departureDate'].'-dd';
                    }
                    if(isset($query['arrivedDate'])) {
                        $segments[] = $query['arrivedDate'].'-rd';
                    }
                    unset($query['departureDate'],$query['arrivedDate']);
                    if(isset($query['ideaid']) && isset($query['ideaname'])) {
                        $segments[] = $query['ideaname'] . '-i' . $query['ideaid'];
                    }
                    unset($query['ideaname'],$query['ideaid']);
                    break;
                case 'tourhome':
                    $link = $menu->getItems('link','index.php?option=com_stravel&view=tourhome',true);
                    if(isset($query['city']) && isset($query['cityname'])) {
                        $segments[] = $query['cityname'] . '-c' . $query['city'];                        
                    }
                    unset($query['city'],$query['cityname']);
                    
                    if(isset($query['departureDate'])) {
                        $segments[] = $query['departureDate'].'-dd';
                    }
                    if(isset($query['arrivedDate'])) {
                        $segments[] = $query['arrivedDate'].'-rd';
                    }
                    unset($query['departureDate'],$query['arrivedDate']);
                    if(isset($query['ideaid']) && isset($query['ideaname'])) {
                        $segments[] = $query['ideaname'] . '-i' . $query['ideaid'];
                    }
                    unset($query['ideaname'],$query['ideaid']);
                    break;
                case 'packageflyer':
                    $link = $menu->getItems('link','index.php?option=com_stravel&view=packageflyer',true);
                    //$segments[] = $query['view'];
                    if(isset($query['id']) && isset($query['name'])) {
                        $segments[] = $query['name'] . '-' . $query['id'];
                        unset($query['id'],$query['name']);
                    }
                    break;
                case 'tourflyer':
                    $link = $menu->getItems('link','index.php?option=com_stravel&view=tourflyer',true);
                    //$segments[] = $query['view'];
                    if(isset($query['id']) && isset($query['name'])) {
                        $segments[] = $query['name'] . '-' . $query['id'];
                        unset($query['id'],$query['name']);
                    }
                    break;
                case 'airtickethome':
                    $link = $menu->getItems('link','index.php?option=com_stravel&view=airtickethome',true);
                    break;
                case 'airsearchresult':
                    $link = $menu->getItems('link','index.php?option=com_stravel&view=airsearchresult',true);
                    //$segments[] = $query['view'];
                    if(isset($query['citycode']) && isset($query['cityname'])) {
                        $segments[] = $query['cityname'] . '-c' . $query['citycode'];
                        unset($query['citycode'],$query['cityname']);
                    }
                    if(isset($query['departureDate'])) {
                        $segments[] = $query['departureDate'].'-dd';
                    }
                    if(isset($query['arrivedDate'])) {
                        $segments[] = $query['arrivedDate'].'-rd';
                    }
                    unset($query['departureDate'],$query['arrivedDate']);
                    if(isset($query['type'])) {
                        $typename = '';
                        switch($query['type']) {
                            case 'Y':
                                $typename = '經濟艙';
                                break;
                            case 'C':
                                $typename = '商務艙';
                                break;
                            case 'F':
                                $typename = '頭等艙';
                                break;
                            case 'P':
                                $typename = '豪華經濟艙';
                                break;
                            default:
                                $typename = $query['type'];
                                break;
                        }
                        $segments[] = $typename . '-t' . $query['type'];
                    }
                    unset($query['type']);
                    if(isset($query['triptype'])) {
                        $triptypename = '';
                        switch($query['triptype']) {
                            case 1:
                                $triptypename = '單程';
                                break;
                            case 2:
                                $triptypename = '往返';
                                break;
                            default:
                                $triptypename = 'Any';
                                break;
                        }
                        $segments[] = $triptypename . '-e' . $query['triptype'];
                    }
                    unset($query['triptype']);
                    break;
                case 'hotelhome':
                    $link = $menu->getItems('link','index.php?option=com_stravel&view=hotelhome',true);
                    break;
                case 'hotelresult':
                    $link = $menu->getItems('link','index.php?option=com_stravel&view=hotelresult',true);
                    //$segments[] = $query['view'];
                    if(isset($query['city']) && isset($query['destination'])) {
                        $segments[] = $query['destination'] . '-c' . $query['city'];
                        unset($query['city'],$query['destination']);
                    }
                    if(isset($query['checkinDate'])) {
                        $segments[] = $query['checkinDate'].'-i';
                    }
                    if(isset($query['nights'])) {
                        $segments[] = $query['nights'].'-n';
                    }
                    unset($query['checkinDate'],$query['nights']);
                    break;
                case 'cruisehome':
                    $link = $menu->getItems('link','index.php?option=com_stravel&view=cruisehome',true);
                    if(isset($query['cruisecpn']) && isset($query['cruisecpnname'])) {
                        $segments[] = $query['cruisecpnname'] . '-c' . $query['cruisecpn'];
                        unset($query['cruisecpn'],$query['cruisecpnname']);
                    }
                    if(isset($query['cruisedst']) && isset($query['cruisedstname'])) {
                        $segments[] = $query['cruisedstname'] . '-d' . $query['cruisedst'];
                        unset($query['cruisedst'],$query['cruisedstname']);
                    }
                    if(isset($query['cruisevessel']) && isset($query['cruisevesselname'])) {
                        $segments[] = $query['cruisevesselname'] . '-v' . $query['cruisevessel'];
                        unset($query['cruisevessel'],$query['cruisevesselname']);
                    }
                    if(isset($query['year'])) {
                        $segments[] = $query['year'].'-y';
                        unset($query['year']);
                    }
                    if(isset($query['month'])) {
                        $segments[] = $query['month'].'-m';
                        unset($query['month']);
                    }
                    if(isset($query['numday'])) {
                        $segments[] = $query['numday'].'-n';
                        unset($query['numday']);
                    }
                    break;
                case 'cruiseflyer':
                    $link = $menu->getItems('link','index.php?option=com_stravel&view=cruiseflyer',true);
                    //$segments[] = $query['view'];
                    if(isset($query['id']) && isset($query['name'])) {
                        $segments[] = $query['name'] . '-' . $query['id'];
                        unset($query['id'],$query['name']);
                    }
                    break;
                case 'urlmanagement':
                    $link = $menu->getItems('link','index.php?option=com_stravel&view=urlmanagement',true);
                    if(isset($query['layout'])) {
                        $segments[] = $query['layout'];
                        unset($query['layout']);
                    }
                    if(isset($query['id'])) {
                        $segments[] = $query['id'];
                        unset($query['id']);
                    }
                    break;
            }
            if(!empty($link->id)) {
                $query['Itemid'] = $link->id;
            }else{
                if(empty($query['Itemid'])) {
                    $query['Itemid'] = $activeMenu->id;    
                }
            }
            unset($query['view']);    
        }
        if(isset($query['task'])) {
            $segments[] = 'task';
            $segments[] = $query['task'];
            unset($query['task']);
        }
		return $segments;
	}

	/**
	 * Parse the segments of a URL.
	 *
	 * @param   array  &$segments  The segments of the URL to parse.
	 *
	 * @return  array  The URL attributes to be used by the application.
	 *
	 * @since   3.3
	 */
	public function parse(&$segments)
	{
		$total = count($segments);
		$vars = array();

		// Get the active menu item.
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$item = $menu->getActive();
		$db = JFactory::getDbo();
        if(!empty($item)) {
            $query = $item->query;
            $vars['view'] = $query['view'];
        }
		if(isset($query['view'])) {
		  if($query['view'] !== 'task') {
		      $firstparam = $query['view'];
              switch($firstparam) {
                case 'tourflyer':
                case 'packageflyer':
                case 'cruiseflyer':
                    if(isset($segments[0])) {
                        $idd = strrchr($segments[0],'-');
                        $name = str_replace($idd,'',$segments[0]);
                        $id = substr($idd,1);
                        $vars['id']=$id;
                        $vars['name'] = $name;
                    }
                    break;
                case 'airsearchresult':
                    for($i=0;$i<$total;$i++) {
                        if(isset($segments[$i])) {
                            $idd = strrchr($segments[$i],'-');
                            $name = str_replace($idd,'',$segments[$i]);
                            $id = substr($idd,2);
                            if(preg_match('/\-c/',$idd)) {
                                $vars['citycode']=$id;
                                $vars['cityname'] = $name;
                            }
                            if(preg_match('/\-dd/',$idd)) {
                                $vars['departureDate']=$name;
                            }
                            if(preg_match('/\-rd/',$idd)) {
                                $vars['arrivedDate']=$name;
                            }
                            if(preg_match('/\-t/',$idd)) {
                                $vars['type']=$id;
                                $vars['typename'] = $name;
                            }
                            if(preg_match('/\-e/',$idd)) {
                                $vars['triptype']=$id;
                                $vars['triptypename'] = $name;
                            }
                        }    
                    }
                    break;
                case  'hotelresult':
                    for($i=0;$i<$total;$i++) {
                        if(isset($segments[$i])) {
                            $idd = strrchr($segments[$i],'-');
                            $name = str_replace($idd,'',$segments[$i]);
                            $id = substr($idd,2);
                            if(preg_match('/\-c/',$idd)) {
                                $vars['city']=$id;
                                $vars['destination'] = $name;
                            }
                            if(preg_match('/\-i/',$idd)) {
                                $vars['checkinDate']=$name;
                            }
                            if(preg_match('/\-n/',$idd)) {
                                $vars['nights']=$name;
                            }
                        }    
                    }
                    break;
                case 'packagehome':
                    for($i=0;$i<$total;$i++) {
                        if(isset($segments[$i])) {
                            $idd = strrchr($segments[$i],'-');
                            $name = str_replace($idd,'',$segments[$i]);
                            $id = substr($idd,2);
                            if(preg_match('/\-i/',$idd)) {
                                $vars['ideaid']=$id;
                                $vars['ideaname'] = $name;    
                            }
                            if(preg_match('/\-c/',$idd)) {
                                $vars['city']=$id;
                                $vars['cityname'] = $name;
                            }
                            if(preg_match('/\-dd/',$idd)) {
                                $vars['departureDate']=$name;
                            }
                            if(preg_match('/\-rd/',$idd)) {
                                $vars['arrivedDate']=$name;
                            }
                        }    
                    }
                    break;
                case 'tourhome':
                    for($i=0;$i<$total;$i++) {
                        if(isset($segments[$i])) {
                            $idd = strrchr($segments[$i],'-');
                            $name = str_replace($idd,'',$segments[$i]);
                            $id = substr($idd,2);
                            if(preg_match('/\-i/',$idd)) {
                                $vars['ideaid']=$id;
                                $vars['ideaname'] = $name;    
                            }
                            if(preg_match('/\-c/',$idd)) {
                                $vars['city']=$id;
                                $vars['cityname'] = $name;
                            }
                            if(preg_match('/\-dd/',$idd)) {
                                $vars['departureDate']=$name;
                            }
                            if(preg_match('/\-rd/',$idd)) {
                                $vars['arrivedDate']=$name;
                            }
                        }    
                    }
                    break;
                case 'cruisehome':
                    for($i=0;$i<$total;$i++) {
                        if(isset($segments[$i])) {
                            $idd = strrchr($segments[$i],'-');
                            $name = str_replace($idd,'',$segments[$i]);
                            $id = substr($idd,2);
                            if(preg_match('/\-d/',$idd)) {
                                $vars['cruisedst']=$id;
                                $vars['cruisedstname'] = $name;    
                            }
                            if(preg_match('/\-c/',$idd)) {
                                $vars['cruisecpn']=$id;
                                $vars['cruisecpnname'] = $name;
                            }
                            if(preg_match('/\-v/',$idd)) {
                                $vars['cruisevessel']=$id;
                                $vars['cruisevesselname'] = $name;
                            }
                            if(preg_match('/\-y/',$idd)) {
                                $vars['year'] = $name;
                            }
                            if(preg_match('/\-m/',$idd)) {
                                $vars['month'] = $name;
                            }
                            if(preg_match('/\-n/',$idd)) {
                                $vars['numday'] = $name;
                            }
                        }    
                    }
                    break;
                case 'urlmanagement':
                    if(!empty($segments[0])) {
                        $vars['layout'] = $segments[0];
                    }
                    if(!empty($segments[1])) {
                        $vars['id'] = $segments[1];
                    }
                    break;
              }  
		  }		  
		}

		return $vars;
	}
}

/**
 * stravel router functions
 *
 * These functions are proxys for the new router interface
 * for old SEF extensions.
 *
 * @deprecated  4.0  Use Class based routers instead
 */
function sTravelBuildRoute(&$query)
{
	$router = new sTravelRouter;

	return $router->build($query);
}

function sTravelParseRoute($segments)
{
	$router = new sTravelRouter;

	return $router->parse($segments);
}
