<?php
/**
 * @module		com_ola
 * @author-name Christophe Demko
 * @adapted by  Ribamar FS
 * @copyright	Copyright (C) 2012 Christophe Demko
 * @license		GNU/GPL, see http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla controller library
jimport('joomla.application.component.controller');
 
/**
 * sTravel Component Controller
 */
class sTravelController extends JControllerLegacy
{
    public function saveNewsletter() {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        $email = JRequest::getVar('email');
        $name = JRequest::getVar('name',$email);
        $lname = JRequest::getVar('lname');
        $number = JRequest::getVar('number');
        $sex = JRequest::getVar('sex');
        $month = JRequest::getVar('birthmonth');
        $date = JRequest::getVar('birthdate');
        
        $pname = JRequest::getVar('pname','');
        $departure = JRequest::getVar('departure','');
        $year = JRequest::getVar('year','');
        $month = JRequest::getVar('month','');
        $day = JRequest::getVar('day','');
        $pno = JRequest::getVar('pno','');
        $saleprice = JRequest::getVar('saleprice','');
        $feedback = JRequest::getVar('feedback','');
        $remark = JRequest::getVar('remark','');
        $staffnumber = JRequest::getVar('staffnumber','');
        $source = JRequest::getVar('source','');
        
        if(!empty($email)) {
            //$result = @ apiHelper::saveSubscription($email,$ip_address,$name,$sex,$number,'0001-'.$month.'-'.$date.'T00:00:00');
        }
        $data = array(
            'inp_2'=>$name,
            'inp_1'=>$lname,
            'inp_3'=>$email,
            'inp_37'=>$number,
            'inp_46'=>$sex,
            'inp_4'=>array($date,$month),
            'submit1'=>'完成登記',
            'optin'=>'y',
            'CID'=>396239983,
            'SID'=>'',
            'UID'=>'',
            'f'=>536,
            'p'=>2,
            'a'=>'r',
            'el'=>'',
            'endlink'=>'',
            'llid'=>'',
            'c'=>'',
            'counted'=>'',
            'RID'=>'',
            'mailnow'=>'',
            'inp_729'=>$pname,
            'inp_728'=>$departure,
            'inp_731'=>array($year,$month,$day),
            'inp_730'=>$pno,
            'inp_732'=>$saleprice,
            'inp_733'=>$source,
            'inp_734'=>$feedback,
            'inp_735'=>$remark,
            'inp_727'=>$staffnumber   
        );
        $ret = $this->sendtoServer($data);
        echo json_encode($ret);exit;
    }
    public function sendtoServer($data,$url="http://link.stravel.com.hk/u/register.php") {
        if(empty($data)) return false;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;    
    }
    public function saveSubcribe() {
        parent::display();
    }
    public function getfareterms() {
        $detail = JRequest::getVar('detailid');
        if(empty($detail)) {
            echo json_encode(array('code'=>'ERROR','message'=>'NO FARE HAS BEEN SENT','data'=>null));
            exit;
        }
        $flight_terms = apiHelper::getTermByDetailId($detail);
        echo json_encode(array('code'=>'SUCCESS','message'=>'Success','data'=>$flight_terms));
        exit;    
    }
    public function getfareschedule() {
        $detail = JRequest::getVar('detailid');
        if(empty($detail)) {
            echo json_encode(array('code'=>'ERROR','message'=>'NO FARE HAS BEEN SENT','data'=>null));
            exit;
        }
        $flight_schedule = apiHelper::getFlightSchedule($detail);
        echo json_encode(array('code'=>'SUCCESS','message'=>'Success','data'=>$flight_schedule));
        exit;
    }
    public function gethotelinfo() {
        $detail = JRequest::getVar('hotelcode');
        $lang = JFactory::getLanguage()->getTag();
        if(empty($detail)) {
            echo json_encode(array('code'=>'ERROR','message'=>'NO HOTEL CODE HAS BEEN SENT','data'=>null));
            exit;
        }
        $hotelinfo = apiHelper::getHotelInfo($detail);
        echo json_encode(array('code'=>'SUCCESS','lang'=>$lang,'message'=>'Success','data'=>$hotelinfo));
        exit;    
    }
    public function sendProductEnquiry() {
        $name = JRequest::getVar('name');
        $email = JRequest::getVar('email');
        $number = JRequest::getVar('number');
        $message = JRequest::getVar('message');
        
        $frompage = JRequest::getVar('frompage','package');
        $pid = JRequest::getVar('pid','');
        $pname = JRequest::getVar('pname','');
        $pprice = JRequest::getVar('pprice','');
        $pdate = JRequest::getVar('pdate','');
        $pnights = JRequest::getVar('pnights','');
        $premark = JRequest::getVar('remark','');
        $ptrip = JRequest::getVar('ptrip','');
        $pclass = JRequest::getVar('pclass','');
        $pdstname = JRequest::getVar('pdstname','');
        $pship = JRequest::getVar('pship','');
        
        $error = 0;
        if(empty($name)) {
            $error += 1;
        }
        if(empty($email)) {
            $error += 2;
        }
        if(!filter_var($email,FILTER_VALIDATE_EMAIL)) {
            $error += 4;
        }
        if($error>0) {
            echo json_encode(array('error'=>true,'errorcode'=>$error));
        }else{
            $infos = apiHelper::getContent(array('CONTENT','B2C','BRANCH','ADMINEMAIL'));
            $receiptlist = explode("\n",$infos->Content);
            if(!empty($receiptlist)) {
                $mailer = JFactory::getMailer();
                $mailer->setSender(array($email,$name));
                $mailer->addRecipient($receiptlist);
                $mailer->addCC($email);
                $mailer->setSubject('心程旅遊 - 查詢內容');
                $mailer->isHTML(true);
                $mailer->Encoding = 'base64';
                switch($frompage) {
                    case 'package':
                        $body = $this->buildPackageEmailTemplate($name,$number,$email,$message,$pname,$pdate,$pnights,$pprice,$premark);
                        break;
                    case 'hotel':
                        $body = $this->buildHotelEmailTemplate($name,$number,$email,$message,$pname,$pdate,$pnights,$pprice,$premark,$pdstname);
                        break;
                    case 'cruise':
                        $body = $this->buildCruiseEmailTemplate($name,$number,$email,$message,$pname,$pdate,$pnights,$pprice,$premark,$pship);
                        break;
                    case 'air':
                        $body = $this->buildAirEmailTemplate($name,$number,$email,$message,$pname,$pdate,$ptrip,$pclass,$pprice,$premark);
                        break;
                }
                
                $mailer->setBody($body);
                $send = $mailer->Send();
                if($send) {
                    echo json_encode(array('error'=>false,'errorcode'=>$error));    
                }else{
                    echo json_encode(array('error'=>true,'errorcode'=>8));    
                }
            }           
            
        }
        exit;
    }
    
    //Build email template for package
    public function buildPackageEmailTemplate($uname = '', $utel = '', $uemail = '', $umsg = '',$pname = '',$pdate = '',$pnights = '',$pprice = '',$premark = '') {
        $rooturl = JUri::root();
        if(substr($rooturl,strlen($rooturl)-1,1)=='/') {
            $rooturl = substr($rooturl,0,strlen($rooturl)-1);
        }
        $packageurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=packagehome');
        $hotelurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=hotelhome');
        $cruiseurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=cruisehome');
        $airurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=airtickethome');
        $contacturl = $rooturl. JRoute::_('index.php?option=com_stravel&view=general&generalpage=contactus');
        $termurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=general&generalpage=terms');
        $disclaimerurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=general&generalpage=disclaimer');
        $body = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center">
                            <table border="0" cellpadding="0" cellspacing="0" width="800" bgcolor="#d7d7d7">
                              <tbody>
                                <tr>
                                  <td width="5%"></td>  <!--left sidebar-->
                                   <td> <!--Center main content-->
                                     <table border="0" cellpadding="5" width="100%">
                                      <tbody>            
                                       <!--Logo + menu link -->
                                        <tr>
                                          <td>
                                          <table style="padding-top:20px;padding-bottom:10px;color:#808080; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                              <tbody>
                                                <tr>
                                                  <td><img id="_x0000_i1026" src="'.$rooturl.'/templates/stravel/images/logo.png" border="0">
                                                  <br>License No. 353618</br></td>
                                                  <td><p align="right"><a style="text-decoration:none;color:#808080;" href="'.$packageurl.'" target="_blank">套票</a> <a style="text-decoration:none;color:#808080;" href="'.$airurl.'" target="_blank">機票</a> <a style="text-decoration:none;color:#808080;" href="'.$hotelurl.'" target="_blank">酒店</a> <a style="text-decoration:none;color:#808080;" href="'.$cruiseurl.'" target="_blank">郵輪</a> <a href="http://www.facebook.com/stravelhk" target="_blank"><img id="_x0000_i1027" src="http://www.adholidays.com/adh/cms/Upload/Content/image/common/fb_new.jpg" border="0" height="27" width="29"></a></p></td>
                                                </tr>
                                              </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                        
                                        
                                        <!--Main content (welcome info , Enquiry content ,Guest info) -->
                                        <tr>
                                          <td bgcolor="#FFFFFF">
                                            <div>
                                              <p style="color:#808080; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;">多謝你的查詢，我們會即時跟進! 以下附上你的查詢內容已供參考。<br>
                                                <br>
                                                我們隨時為你服務<br>
                                                心程旅遊 </p>
                                            </div>
                                            
                                            <div style="color:#808080; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;" name="EnquiryContent">
                                              <div name="Lable"><p style="color:#ff0000; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;">你的查詢內容 </p></div>
                                              <div name="Package_title">旅游套票： '.$pname.'</div>
                                              <div name="Dep_date">出發日期： '.$pdate.'</div>
                                              <div name="Days">晚數： '.$pnights.'</div>
                                              <div name="Price">價格: '.$pprice.'</div>
                                              <div name="Message">查詢內容: '.$umsg.'</div>
                                              <div name="Remark">備註： '.$premark.'</div>
                                            </div>
                             
                                            <div style="font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;color:#000000;" name="GuestInfo">
                                              <div name="Lable"><p>你的聯絡資料</p></div>
                                              <div name="Name">姓名： '.$uname.'</div>
                                              <div name="Tel">電話： '.$utel.'</div>
                                              <div name="Mail">電郵： '.$uemail.'</div>
                                            </div>
                            			  </td>
                                        </tr>
                                        
                                        
                                        <!--footer info -->
                                        <tr>
                                          <td style="color:#808080;font-size:12px;padding-top:10px;padding-bottom:10px;">
                                            <div>版權所有 @ 心程旅遊</div>
                                            <div><a style="color:#808080;text-decoration:none;" href="'.$termurl.'" target="_blank">使用條款</a>/<a style="color:#808080;text-decoration:none;" href="'.$disclaimerurl.'">免責聲明及私隱政策</a></div>
                                            <div><br />收到此電郵，證明你已成功收取心程旅遊查詢。<br>
                                                 查詢熱線：81006223 ｜<a style="color:#808080;text-decoration:none;" href="'.$contacturl.'" target="_blank">分店地址</a> ｜ 電郵：<a style="color:#808080;text-decoration:none;" target="_blank" href="mailto:info@stravel.com.hk">info@stravel.com.hk</a>  |   公司網址：<a style="color:#808080;text-decoration:none;" target="_blank" href="http://www.stravel.com.hk">www.stravel.com.hk</a></div>               </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                   </td>
                                   <td width="5%"></td> <!--right sidebar-->
                                </tr>
                              </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
                ';
        return $body;
    }
    
    //Build email template for air
    public function buildAirEmailTemplate($uname = '', $utel = '', $uemail = '',$umsg = '',$pname = '',$pdate = '',$ptrip = '', $pclass = '',$pprice = '',$premark = '') {
        $rooturl = JUri::root();
        if(substr($rooturl,strlen($rooturl)-1,1)=='/') {
            $rooturl = substr($rooturl,0,strlen($rooturl)-1);
        }
        $packageurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=packagehome');
        $hotelurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=hotelhome');
        $cruiseurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=cruisehome');
        $airurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=airtickethome');
        $contacturl = $rooturl. JRoute::_('index.php?option=com_stravel&view=general&generalpage=contactus');
        $termurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=general&generalpage=terms');
        $disclaimerurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=general&generalpage=disclaimer');
        $body = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center">
                            <table border="0" cellpadding="0" cellspacing="0" width="800" bgcolor="#d7d7d7">
                              <tbody>
                                <tr>
                                  <td width="5%"></td>  <!--left sidebar-->
                                   <td> <!--Center main content-->
                                     <table border="0" cellpadding="5" width="100%">
                                      <tbody>            
                                       <!--Logo + menu link -->
                                        <tr>
                                          <td>
                                          <table style="padding-top:20px;padding-bottom:10px;color:#808080; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                              <tbody>
                                                <tr>
                                                  <td><img id="_x0000_i1026" src="'.$rooturl.'/templates/stravel/images/logo.png" border="0">
                                                  <br>License No. 353618</br></td>
                                                  <td><p align="right"><a style="text-decoration:none;color:#808080;" href="'.$packageurl.'" target="_blank">套票</a> <a style="text-decoration:none;color:#808080;" href="'.$airurl.'" target="_blank">機票</a> <a style="text-decoration:none;color:#808080;" href="'.$hotelurl.'" target="_blank">酒店</a> <a style="text-decoration:none;color:#808080;" href="'.$cruiseurl.'" target="_blank">郵輪</a> <a href="http://www.facebook.com/stravelhk" target="_blank"><img id="_x0000_i1027" src="http://www.adholidays.com/adh/cms/Upload/Content/image/common/fb_new.jpg" border="0" height="27" width="29"></a></p></td>
                                                </tr>
                                              </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                        
                                        
                                        <!--Main content (welcome info , Enquiry content ,Guest info) -->
                                        <tr>
                                          <td bgcolor="#FFFFFF">
                                            <div>
                                              <p style="color:#808080; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;">多謝你的查詢，我們會即時跟進! 以下附上你的查詢內容已供參考。<br>
                                                <br>
                                                我們隨時為你服務<br>
                                                心程旅遊 </p>
                                            </div>
                                            
                                            <div style="color:#808080; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;" name="EnquiryContent">
                                              <div name="Lable"><p style="color:#ff0000; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;">你的查詢內容 </p></div>
                                              <div name="Ticket_title">機票資料： '.$pname.'</div>
                                              <div name="Dep_date">出發日期： '.$pdate.'</div>
                                              <div name="Class">客艙等級： '.$pclass.'</div>
                                              <div name="Trip">航程： '.$ptrip.'</div>
                                              <div name="Price">價格: '.$pprice.'</div>
                                              <div name="Message">查詢內容: '.$umsg.'</div>
                                              <div name="Remark">備註：<br /> 
                                                - 所有票價均以港幣為單位及以每位成人計算。<br />
                                                - 所有票價、稅款 / 航空公司附加費僅供參考，以最終出票確認為準。<br />
                                                - 因部份航空公司未能顯示詳盡資訊(如稅項或轉機等），如欲查詢上述航班資料，請致電查詢。</div>
                                            </div>
                             
                                            <div style="font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;color:#000000;" name="GuestInfo">
                                              <div name="Lable"><p>你的聯絡資料</p></div>
                                              <div name="Name">姓名： '.$uname.'</div>
                                              <div name="Tel">電話： '.$utel.'</div>
                                              <div name="Mail">電郵： '.$uemail.'</div>
                                            </div>
                            			  </td>
                                        </tr>
                                        
                                        
                                        <!--footer info -->
                                        <tr>
                                          <td style="color:#808080;font-size:12px;padding-top:10px;padding-bottom:10px;">
                                            <div>版權所有 @ 心程旅遊</div>
                                            <div><a style="color:#808080;text-decoration:none;" href="'.$termurl.'" target="_blank">使用條款</a>/<a style="color:#808080;text-decoration:none;" href="'.$disclaimerurl.'">免責聲明及私隱政策</a></div>
                                            <div><br />收到此電郵，證明你已成功收取心程旅遊查詢。<br>
                                                 查詢熱線：81006223 ｜<a style="color:#808080;text-decoration:none;" href="'.$contacturl.'" target="_blank">分店地址</a> ｜ 電郵：<a style="color:#808080;text-decoration:none;" target="_blank" href="mailto:info@stravel.com.hk">info@stravel.com.hk</a>  |   公司網址：<a style="color:#808080;text-decoration:none;" target="_blank" href="http://www.stravel.com.hk">www.stravel.com.hk</a></div>               </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                   </td>
                                   <td width="5%"></td> <!--right sidebar-->
                                </tr>
                              </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
                ';
        return $body;
    }
    
    //Build email template for hotel
    public function buildHotelEmailTemplate($uname = '', $utel = '', $uemail = '',$umsg='',$pname = '',$pdate = '',$pnights = '',$pprice = '',$premark = '',$pdstname='') {
        $rooturl = JUri::root();
        if(substr($rooturl,strlen($rooturl)-1,1)=='/') {
            $rooturl = substr($rooturl,0,strlen($rooturl)-1);
        }
        $packageurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=packagehome');
        $hotelurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=hotelhome');
        $cruiseurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=cruisehome');
        $airurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=airtickethome');
        $contacturl = $rooturl. JRoute::_('index.php?option=com_stravel&view=general&generalpage=contactus');
        $termurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=general&generalpage=terms');
        $disclaimerurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=general&generalpage=disclaimer');
        $body = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center">
                            <table border="0" cellpadding="0" cellspacing="0" width="800" bgcolor="#d7d7d7">
                              <tbody>
                                <tr>
                                  <td width="5%"></td>  <!--left sidebar-->
                                   <td> <!--Center main content-->
                                     <table border="0" cellpadding="5" width="100%">
                                      <tbody>            
                                       <!--Logo + menu link -->
                                        <tr>
                                          <td>
                                          <table style="padding-top:20px;padding-bottom:10px;color:#808080; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                              <tbody>
                                                <tr>
                                                  <td><img id="_x0000_i1026" src="'.$rooturl.'/templates/stravel/images/logo.png" border="0">
                                                  <br>License No. 353618</br></td>
                                                  <td><p align="right"><a style="text-decoration:none;color:#808080;" href="'.$packageurl.'" target="_blank">套票</a> <a style="text-decoration:none;color:#808080;" href="'.$airurl.'" target="_blank">機票</a> <a style="text-decoration:none;color:#808080;" href="'.$hotelurl.'" target="_blank">酒店</a> <a style="text-decoration:none;color:#808080;" href="'.$cruiseurl.'" target="_blank">郵輪</a> <a href="http://www.facebook.com/stravelhk" target="_blank"><img id="_x0000_i1027" src="http://www.adholidays.com/adh/cms/Upload/Content/image/common/fb_new.jpg" border="0" height="27" width="29"></a></p></td>
                                                </tr>
                                              </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                        
                                        
                                        <!--Main content (welcome info , Enquiry content ,Guest info) -->
                                        <tr>
                                          <td bgcolor="#FFFFFF">
                                            <div>
                                              <p style="color:#808080; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;">多謝你的查詢，我們會即時跟進! 以下附上你的查詢內容已供參考。<br>
                                                <br>
                                                我們隨時為你服務<br>
                                                心程旅遊 </p>
                                            </div>
                                            
                                            <div style="color:#808080; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;" name="EnquiryContent">
                                              <div name="Lable"><p style="color:#ff0000; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;">你的查詢內容 </p></div>
                                              <div name="Dest_name">目的地： '.$pdstname.'</div>
                                              <div name="Hotel_name">酒店名稱： '.$pname.'</div>
                                              <div name="Check-in">入住日期： '.$pdate.'</div>
                                              <div name="Days">晚數： '.$pnights.'</div>
                                              <div name="Price">價格: '.$pprice.'</div>
                                              <div name="Message">查詢內容: '.$umsg.'</div>
                                              <div name="Remark">備註：<br />
                                                                - 参考星级<br />
                                                                - 以上酒店星級只供參考，詳情有關資訊請瀏覽酒店官方網址。
                                              </div>
                                            </div>
                             
                                            <div style="font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;color:#000000;" name="GuestInfo">
                                              <div name="Lable"><p>你的聯絡資料</p></div>
                                              <div name="Name">姓名： '.$uname.'</div>
                                              <div name="Tel">電話： '.$utel.'</div>
                                              <div name="Mail">電郵： '.$uemail.'</div>
                                            </div>
                            			  </td>
                                        </tr>
                                        
                                        
                                        <!--footer info -->
                                        <tr>
                                          <td style="color:#808080;font-size:12px;padding-top:10px;padding-bottom:10px;">
                                            <div>版權所有 @ 心程旅遊</div>
                                            <div><a style="color:#808080;text-decoration:none;" href="'.$termurl.'" target="_blank">使用條款</a>/<a style="color:#808080;text-decoration:none;" href="'.$disclaimerurl.'">免責聲明及私隱政策</a></div>
                                            <div><br />收到此電郵，證明你已成功收取心程旅遊查詢。<br>
                                                 查詢熱線：81006223 ｜<a style="color:#808080;text-decoration:none;" href="'.$contacturl.'" target="_blank">分店地址</a> ｜ 電郵：<a style="color:#808080;text-decoration:none;" target="_blank" href="mailto:info@stravel.com.hk">info@stravel.com.hk</a>  |   公司網址：<a style="color:#808080;text-decoration:none;" target="_blank" href="http://www.stravel.com.hk">www.stravel.com.hk</a></div>               </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                   </td>
                                   <td width="5%"></td> <!--right sidebar-->
                                </tr>
                              </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
                ';
        return $body;
    }
    
    //Build email template for cruise
    public function buildCruiseEmailTemplate($uname = '', $utel = '', $uemail = '',$umsg='',$pname = '',$pdate = '',$pnights = '',$pprice = '',$premark = '',$pship = '') {
        $rooturl = JUri::root();
        if(substr($rooturl,strlen($rooturl)-1,1)=='/') {
            $rooturl = substr($rooturl,0,strlen($rooturl)-1);
        }
        $packageurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=packagehome');
        $hotelurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=hotelhome');
        $cruiseurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=cruisehome');
        $airurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=airtickethome');
        $contacturl = $rooturl. JRoute::_('index.php?option=com_stravel&view=general&generalpage=contactus');
        $termurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=general&generalpage=terms');
        $disclaimerurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=general&generalpage=disclaimer');
        $body = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center">
                            <table border="0" cellpadding="0" cellspacing="0" width="800" bgcolor="#d7d7d7">
                              <tbody>
                                <tr>
                                  <td width="5%"></td>  <!--left sidebar-->
                                   <td> <!--Center main content-->
                                     <table border="0" cellpadding="5" width="100%">
                                      <tbody>            
                                       <!--Logo + menu link -->
                                        <tr>
                                          <td>
                                          <table style="padding-top:20px;padding-bottom:10px;color:#808080; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                              <tbody>
                                                <tr>
                                                  <td><img id="_x0000_i1026" src="'.$rooturl.'/templates/stravel/images/logo.png" border="0">
                                                  <br>License No. 353618</br></td>
                                                  <td><p align="right"><a style="text-decoration:none;color:#808080;" href="'.$packageurl.'" target="_blank">套票</a> <a style="text-decoration:none;color:#808080;" href="'.$airurl.'" target="_blank">機票</a> <a style="text-decoration:none;color:#808080;" href="'.$hotelurl.'" target="_blank">酒店</a> <a style="text-decoration:none;color:#808080;" href="'.$cruiseurl.'" target="_blank">郵輪</a> <a href="http://www.facebook.com/stravelhk" target="_blank"><img id="_x0000_i1027" src="http://www.adholidays.com/adh/cms/Upload/Content/image/common/fb_new.jpg" border="0" height="27" width="29"></a></p></td>
                                                </tr>
                                              </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                        
                                        
                                        <!--Main content (welcome info , Enquiry content ,Guest info) -->
                                        <tr>
                                          <td bgcolor="#FFFFFF">
                                            <div>
                                              <p style="color:#808080; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;">多謝你的查詢，我們會即時跟進! 以下附上你的查詢內容已供參考。<br>
                                                <br>
                                                我們隨時為你服務<br>
                                                心程旅遊 </p>
                                            </div>
                                            
                                            <div style="color:#808080; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;" name="EnquiryContent">
                                              <div name="Lable"><p style="color:#ff0000; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;">你的查詢內容 </p></div>
                                              <div name="Cruise_name">郵輪套票： '.$pname.'</div>
                                              <div name="Ship_name">郵輪名稱： '.$pship.'</div>
                                              <div name="Dep_date">出發日期： '.$pdate.'</div>
                                              <div name="Days">晚數： '.$pnights.'</div>
                                              <div name="Price">價格: '.$pprice.'</div>
                                              <div name="Message">查詢內容: '.$umsg.'</div>
                                              <div name="Remark">備註：<br /> 
                                                    - 所有價格均以港幣或美元為單位，以二人一房每位計算；價格僅供參考，以最終出票確認為準。   
                                              </div>
                                            </div>
                             
                                            <div style="font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;color:#000000;" name="GuestInfo">
                                              <div name="Lable"><p>你的聯絡資料</p></div>
                                              <div name="Name">姓名： '.$uname.'</div>
                                              <div name="Tel">電話： '.$utel.'</div>
                                              <div name="Mail">電郵： '.$uemail.'</div>
                                            </div>
                            			  </td>
                                        </tr>
                                        
                                        
                                        <!--footer info -->
                                        <tr>
                                          <td style="color:#808080;font-size:12px;padding-top:10px;padding-bottom:10px;">
                                            <div>版權所有 @ 心程旅遊</div>
                                            <div><a style="color:#808080;text-decoration:none;" href="'.$termurl.'" target="_blank">使用條款</a>/<a style="color:#808080;text-decoration:none;" href="'.$disclaimerurl.'">免責聲明及私隱政策</a></div>
                                            <div><br />收到此電郵，證明你已成功收取心程旅遊查詢。<br>
                                                 查詢熱線：81006223 ｜<a style="color:#808080;text-decoration:none;" href="'.$contacturl.'" target="_blank">分店地址</a> ｜ 電郵：<a style="color:#808080;text-decoration:none;" target="_blank" href="mailto:info@stravel.com.hk">info@stravel.com.hk</a>  |   公司網址：<a style="color:#808080;text-decoration:none;" target="_blank" href="http://www.stravel.com.hk">www.stravel.com.hk</a></div>               </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                   </td>
                                   <td width="5%"></td> <!--right sidebar-->
                                </tr>
                              </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
                ';
        return $body;
    }
    
    //Build email template for general enquery
    public function buildGeneralEmailTemplate($uname = '', $utel = '', $uemail = '',$msg = '',$type='') {
        $rooturl = JUri::root();
        if(substr($rooturl,strlen($rooturl)-1,1)=='/') {
            $rooturl = substr($rooturl,0,strlen($rooturl)-1);
        }
        $packageurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=packagehome');
        $hotelurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=hotelhome');
        $cruiseurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=cruisehome');
        $airurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=airtickethome');
        $contacturl = $rooturl. JRoute::_('index.php?option=com_stravel&view=general&generalpage=contactus');
        $termurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=general&generalpage=terms');
        $disclaimerurl = $rooturl. JRoute::_('index.php?option=com_stravel&view=general&generalpage=disclaimer');
        $body = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center">
                            <table border="0" cellpadding="0" cellspacing="0" width="800" bgcolor="#d7d7d7">
                              <tbody>
                                <tr>
                                  <td width="5%"></td>  <!--left sidebar-->
                                   <td> <!--Center main content-->
                                     <table border="0" cellpadding="5" width="100%">
                                      <tbody>            
                                       <!--Logo + menu link -->
                                        <tr>
                                          <td>
                                          <table style="padding-top:20px;padding-bottom:10px;color:#808080; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                              <tbody>
                                                <tr>
                                                  <td><img id="_x0000_i1026" src="'.$rooturl.'/templates/stravel/images/logo.png" border="0">
                                                  <br>License No. 353618</br></td>
                                                  <td><p align="right"><a style="text-decoration:none;color:#808080;" href="'.$packageurl.'" target="_blank">套票</a> <a style="text-decoration:none;color:#808080;" href="'.$airurl.'" target="_blank">機票</a> <a style="text-decoration:none;color:#808080;" href="'.$hotelurl.'" target="_blank">酒店</a> <a style="text-decoration:none;color:#808080;" href="'.$cruiseurl.'" target="_blank">郵輪</a> <a href="http://www.facebook.com/stravelhk" target="_blank"><img id="_x0000_i1027" src="http://www.adholidays.com/adh/cms/Upload/Content/image/common/fb_new.jpg" border="0" height="27" width="29"></a></p></td>
                                                </tr>
                                              </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                        
                                        
                                        <!--Main content (welcome info , Enquiry content ,Guest info) -->
                                        <tr>
                                          <td bgcolor="#FFFFFF">
                                            <div>
                                              <p style="color:#808080; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;">多謝你的查詢，我們會即時跟進! 以下附上你的查詢內容已供參考。<br>
                                                <br>
                                                我們隨時為你服務<br>
                                                心程旅遊 </p>
                                            </div>
                                            
                                            <div style="color:#808080; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;" name="EnquiryContent">
                                              <div name="Lable"><p style="color:#ff0000; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;">你的查詢內容 </p></div>
                                              <div name="Remark">查詢內容： '.$msg.'</div>
                                              <div name="Enquiry_type"><br />查詢類別： '.$type.'</div>
                                            </div>
                             
                                            <div style="font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;color:#000000;" name="GuestInfo">
                                              <div name="Lable"><p>你的聯絡資料</p></div>
                                              <div name="Name">姓名： '.$uname.'</div>
                                              <div name="Tel">電話： '.$utel.'</div>
                                              <div name="Mail">電郵： '.$uemail.'</div>
                                            </div>
                            			  </td>
                                        </tr>
                                        
                                        
                                        <!--footer info -->
                                        <tr>
                                          <td style="color:#808080;font-size:12px;padding-top:10px;padding-bottom:10px;color:#808080; font-size: 12px; font-family: \'Microsoft Jhenghei\',Tahoma,Arial,sans-serif;">
                                            <div>版權所有 @ 心程旅遊</div>
                                            <div><a style="color:#808080;text-decoration:none;" href="'.$termurl.'" target="_blank">使用條款</a>/<a style="color:#808080;text-decoration:none;" href="'.$disclaimerurl.'">免責聲明及私隱政策</a></div>
                                            <div><br />收到此電郵，證明你已成功收取心程旅遊查詢。<br>
                                                 查詢熱線：81006223 ｜<a style="color:#808080;text-decoration:none;" href="'.$contacturl.'" target="_blank">分店地址</a> ｜ 電郵：<a style="color:#808080;text-decoration:none;" target="_blank" href="mailto:info@stravel.com.hk">info@stravel.com.hk</a>  |   公司網址：<a style="color:#808080;text-decoration:none;" target="_blank" href="http://www.stravel.com.hk">www.stravel.com.hk</a></div>               </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                   </td>
                                   <td width="5%"></td> <!--right sidebar-->
                                </tr>
                              </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
                ';
        return $body;
    }
    
    public function sendGeneralEnquiry() {
        $name = JRequest::getVar('name');
        $email = JRequest::getVar('email');
        $number = JRequest::getVar('number');
        $message = JRequest::getVar('message');
        $type = JRequest::getVar('type','--');
        if($type==1) {
            $type = JText::_('COM_STRAVEL_GENERAL_ENQUERY');    
        }
        if($type==2) {
            $type = JText::_('COM_STRAVEL_OPINION_ENQUERY');    
        }
        $error = 0;
        if(empty($name)) {
            $error += 1;
        }
        if(empty($email)) {
            $error += 2;
        }
        if(!filter_var($email,FILTER_VALIDATE_EMAIL)) {
            $error += 4;
        }
        if($error>0) {
            echo json_encode(array('error'=>true,'errorcode'=>$error));
        }else{
            switch(JRequest::getVar('type')) {
                case 1:
                    $infos = apiHelper::getContent(array('CONTENT','B2C','BRANCH','ADMINEMAIL'));
                    break;
                case 2:
                    $infos = apiHelper::getContent(array('CONTENT','B2C','BRANCH','GENERALENQUIRY'));
                    break;
                default:
                    $infos = apiHelper::getContent(array('CONTENT','B2C','BRANCH','ADMINEMAIL'));
                    break;
            }
            
            $receiptionlist = explode("\n",(string)$infos->Content);
            $receiptionlist = array_merge($receiptionlist,array($email));
            if(!empty($receiptionlist)) {
                $mailer = JFactory::getMailer();
                $mailer->setSender(array($email,$name));
                $mailer->addRecipient($receiptionlist);
                $mailer->setSubject('心程旅遊 - 查詢內容');
                $mailer->isHTML(true);
                $mailer->Encoding = 'base64';
                $body = $this->buildGeneralEmailTemplate($name,$number,$email,$message,$type);
                $mailer->setBody($body);
                $send = $mailer->Send();
                if($send) {
                    echo json_encode(array('error'=>false,'errorcode'=>$error,'info'=>$receiptionlist));    
                }else{
                    echo json_encode(array('error'=>true,'errorcode'=>8));    
                }
            }           
            
        }
        exit;
    }
    
    /**
     * Function to cache 26 hotel items
     * */
    public function gethotelhomeitems() {
        set_time_limit(1500);
        $start = time();
        $citylist = apiHelper::getContent(array('CONTENT','B2C','HOTEL','HOTELHOME'));
        if(empty($citylist)) {
           return false; 
        }
        
        /*
        $cache = JFactory::getCache('stravel_hotelhome');
        $cache->setCaching(true);        
        $cache->setLifeTime(86400);        
        */
        
        $db = JFactory::getDbo();
        $query = 'DELETE FROM #__stravel_hotelhome WHERE 1';
        $db->setQuery($query)->execute();
        
        $checkin = date('Y-m-d\TH:m:s',time() + 10*24*60*60);
        $checkout = date('Y-m-d\TH:m:s',time() + 20*24*60*60);
        //$data = $cache->get(md5('hotellist'));
        
        if(empty($data)) {            
            foreach($citylist as $item) {
                $result_item = null;
                $hotel_item = apiHelper::searchHotelRate($checkin,$checkout,$item->Keyword);
                $is_added = false;
                $tmphtl = null;
                foreach($hotel_item as $hotel) {
                    if($is_added) break;
                    if((int)$hotel->RqtPrice != 0 && empty($tmphtl)) {
                        $tmphtl = $hotel;
                    }
                    if((int)$hotel->Star == (int)$item->Content && (int)$hotel->RqtPrice != 0) {
                        $result_item = $hotel;
                        $is_added = true;    
                    }    
                }
                if($is_added==false) {
                    $result_item = $tmphtl;
                }
                $citycode = $db->quote($item->Keyword,true);
                $htlname = $db->quote($result_item->HotelName,true);
                $htlname1 = $db->quote($result_item->HotelName1,true);
                $htlname2 = $db->quote($result_item->HotelName2,true);
                $htlcode = $db->quote($result_item->HotelCode,true);
                $htlimg = $db->quote($result_item->PhotoURL,true);
                $htlprice = $db->quote($result_item->RqtPrice,true);
                $rank = $db->quote($item->Rank,true);
                $query = "INSERT INTO #__stravel_hotelhome values ($citycode,$htlname,$htlname1,$htlname2,$htlcode,$htlimg,$htlprice,$rank)";
                $db->setQuery($query)->execute();
            }            
            //$cache->store($itemlist,md5('hotellist'));    
        }
        $end = time() - $start;
        echo $end;
        exit;
    }
}
