<?php
/**
 * @module		com_stravel
 * @author-name Kenric Nguyen
 * @copyright	Copyright (C) 2012 Kenric Nguyen
 * @license		GNU/GPL, see http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
define('DS', DIRECTORY_SEPARATOR); 
// import joomla controller library
jimport('joomla.application.component.controller');

//Add API helper
$apipath = JPATH_COMPONENT_SITE.DS.'helper'.DS.'apihelper.php';
if(file_exists($apipath)) {
    require $apipath;
}
//Add API helper
$presetpath = JPATH_COMPONENT_SITE.DS.'helper'.DS.'presethelper.php';
if(file_exists($presetpath)) {
    require $presetpath;
} 
// Get an instance of the controller prefixed by sTravel
$controller = JControllerLegacy::getInstance('sTravel');
 
// Perform the Request task
$controller->execute(JRequest::getCmd('task'));
// Redirect if set by the controller
$controller->redirect();
