<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class apiHelper {
    
    public function convertUrl($name=null) {
        if(empty($name)) {
            return '';
        }
        $name = trim($name);
        //Remove default special character
        $result = preg_replace('/(\/*)(\?*)(\.*)(\>*)(\<*)(\,*)(\;*)(\:*)(\"*)(\[*)(\]*)(\{*)(\}*)(\|*)(\`*)(\~*)(\!*)(\@*)(\#*)(\$*)(\^*)(\&*)(\**)(\(*)(\)*)(\_*)(\=*)(\+*)/','',$name);
        //Remove or change to - for special character CN
        $result = preg_replace('/(\／*)(\、*)(\【*)(\】*)(\！*)/','-',$result);
        $result = preg_replace('/(\s+)/','-',$result);
        $result = preg_replace('/(\-+)/','-',$result);
        $result = urlencode(strtolower(trim($result)));
        return $result; 
    }
    
    public function getMetadata() {
        $db = JFactory::getDbo();
        $uri = JFactory::getURI()->toString();
        $query = "SELECT * FROM #__stravel_urls WHERE sef_url=".$db->quote($uri);
        return $db->setQuery($query)->loadObject();
    }
    
    public function __wordProcess($word=null,$length = 13) {
        if(empty($word)) {
            return '';
        }
        if(empty($length)) {
            $length = 13;
        }
        $tmp = $word;
        $is_decoded = false;
        while (mb_detect_encoding($tmp)=="UTF-8")
        {
            $tmp = utf8_decode($tmp);
            $is_decoded = true;
        }
        $wordlen = strlen($tmp);
        if($is_decoded) {
            $length = 35;
        }
        if($wordlen>13) {
            $tmp = mb_strcut($word,0,$length,'UTF-8');
            $tmp .= '...';
        }else{
            $tmp = $word;
        }
        return $tmp;
    }
    /**
    *   Package Service: http://ws.adholidays.com/STrvl_Services/B2C/PackageService.asmx 
    *   Airfare Service: http://ws.adholidays.com/STrvl_Services/B2C/FareService.asmx 
    *   Content Service: http://ws.adholidays.com/STrvl_Services/B2C/ContentService.asmx 
    *   Cruise Service: http://ws.adholidays.com/STrvl_Services/B2C/cruiseService.asmx 
    *   Hotel Service: http://ws.adholidays.com/STrvl_Services/B2C/hotelService.asmx
    **/
    
    public static $_host    = 'http://ws.adholidays.com';
    public static $_Package = '/STrvl_Services/B2C/PackageService.asmx';
    public static $_Content = '/STrvl_Services/B2C/ContentService.asmx';
    public static $_Airfare = '/STrvl_Services/B2C/FareService.asmx';
    public static $_Cruise  = '/STrvl_Services/B2C/cruiseService.asmx';
    public static $_Hotel   = '/STrvl_Services/B2C/hotelService.asmx';
    protected static $_token = 'mUWQUNdVQErowh9US2B9MXjvPEeaF/+qqVOssQFJ8ts=';
    
    /**
     *  Function to get javascript data 
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getJavascriptData($preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        $url = self::$_host . self::$_Content; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetJavascriptData xmlns="http://www.westminstertravel.com/">
                          <lang>'.$lang.'</lang>
                        </GetJavascriptData>
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Content . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data),
            'SOAPAction: "http://www.westminstertravel.com/GetJavascriptData"'
        );
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetJavascriptDataResponse ->GetJavascriptDataResult)) {
            return $data->soapBody->GetJavascriptDataResponse ->GetJavascriptDataResult;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function to get content
     *  $module_code: Array of module code. Default: return slideshow content for home => array('CONTENT','B2C','HOME','BANNER');  
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getContent($module_code = array('CONTENT','B2C','HOME','SLIDESHOW'),$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        $url = self::$_host . self::$_Content;
        if(!empty($module_code)) {
            $moduleCode = '';
            foreach($module_code as $module)  {
                $moduleCode .= '<string>'.$module.'</string>';
            }
        }else{
            return false;
        } 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetContent xmlns="http://www.westminstertravel.com/">
                          <moduleCodes>
                            '.$moduleCode.'
                          </moduleCodes>
                        </GetContent>
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Content . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetContentResponse->GetContentResult->ContentInfoList->ContentInfo)) {
            return $data->soapBody->GetContentResponse->GetContentResult->ContentInfoList->ContentInfo;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function to get content by id
     *  $contentid  : Integer. Default: null  
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getContentById($contentid = null,$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        $url = self::$_host . self::$_Content;
        if(empty($contentid)) {
            return false;
        } 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetContentById xmlns="http://www.westminstertravel.com/">
                          <id>'.$contentid.'</id>
                        </GetContentById>
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Content . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetContentByIdResponse->GetContentByIdResult->ContentInfoList->ContentInfo)) {
            return $data->soapBody->GetContentByIdResponse->GetContentByIdResult->ContentInfoList->ContentInfo;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function to save subcribtion
     *  $email      : Email address of subcriber. Default: null.
     *  $ip         : IP of subcriber. Default: null.
     *  $name       : Name of subcriber. Default: null.
     *  $gender     : Gender of subscriber (int). Default: null.
     *  $phone      : Phone number of subcriber. Default: null.
     *  $birthdate  : Birtdata of subcriber. Default: null.    
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function saveSubscription($email=null,$ip=null,$name=null,$gender=null,$phone=null,$birthdate=null,$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        $url = self::$_host . self::$_Content;
        if(empty($email) || empty($name) || empty($gender) || empty($phone) || empty($birthdate) || empty($ip)) {
            return false;
        }
          
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <SaveSubscription xmlns="http://www.westminstertravel.com/">
                          <subscriptionParams>
                            <Name>'.$name.'</Name>
                            <Gender>'.$gender.'</Gender>
                            <Phone>'.$phone.'</Phone>
                            <Birthdate>'.$birthdate.'</Birthdate>
                            <Email>'.$email.'</Email>
                            <UserHostAddress>'.$ip.'</UserHostAddress>
                          </subscriptionParams>
                        </SaveSubscription>
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Content . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->SaveSubscriptionResponse->SaveSubscriptionResult)) {
            return $data->soapBody->SaveSubscriptionResponse->SaveSubscriptionResult;
        }else{
            return false;
        }      
    }
    
    /**
     *  Search Package 
     *  $depdate    : Departure date in format yyyy-mm-ddThh:ii:ss. Default: null
     *  $retDate    : Return date in format yyyy-mm-ddThh:ii:ss. Default: null
     *  $ideaId     : ideaId. Default: 0
     *  $city       : Array of city list. Default: Empty array.
     *  $carrier    : Array of carrier list. Default: Empty array.
     *  $hotel      : Array of hotel list. Default: Empty array.
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null  
     **/
    public function searchPackage($depdate = null,$retDate=null,$ideaId=0,$city = array(),$carrier=array(),$hotel=array(),$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        if(empty($depdate)) {
            $depdate = '<depDate>0001-01-01T00:00:00</depDate>';
        }else{
            $depdate = '<depDate>'.$depdate.'</depDate>';
        }
        if(empty($retDate)) {
            $retDate = '<retDate>0001-01-01T00:00:00</retDate>';
        }else{
            $retDate = '<retDate>'.$retDate.'</retDate>';
        }
        if(empty($ideaId)) {
            $ideaId = '0';
        }
        if(empty($city)) {
            $cityString = '<citiesCodeOrName />';
        }else{
            $cityString = '<citiesCodeOrName>';
            foreach($city as $val)  {
                $cityString .= '<string>'.$val.'</string>';
            }
            $cityString .= '</citiesCodeOrName>';
        }
        if(empty($carrier)) {
            $carrierString = '<carriersCodeOrName />';
        }else{
            $carrierString = '<carriersCodeOrName>';
            foreach($carrier as $val)  {
                $carrierString .= '<string>'.$val.'</string>';
            }
            $carrierString .= '<carriersCodeOrName />';
        }
        if(empty($hotel)) {
            $hotelString = '<hotelCodeOrName />';
        }else{
            $hotelString = '<hotelCodeOrName>';
            foreach($hotel as $val)  {
                $hotelString .= '<string>'.$val.'</string>';
            }
            $hotelString .= '</hotelCodeOrName>';
        }
        $url = self::$_host . self::$_Package;
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                        <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                          <soap:Header>
                            <ProfileInformation xmlns="http://www.westminstertravel.com/">
                              <Preview>'.$preview.'</Preview>
                              <Token>'.$token.'</Token>
                              <Language>'.$lang.'</Language>
                              <DataFilter>'.$filter.'</DataFilter>
                              <Device>'.$device.'</Device>
                            </ProfileInformation>
                          </soap:Header>
                          <soap:Body>
                            <SearchPackage xmlns="http://www.westminstertravel.com/">
                              '.$cityString.'
                              '.$depdate.'
                              '.$retDate.'
                              <ideaId>'.$ideaId.'</ideaId>
                              '.$carrierString.'
                              '.$hotelString.'
                            </SearchPackage>
                          </soap:Body>
                        </soap:Envelope>';
        $header = array(
            "POST ". self::$_Package . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );   
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->SearchPackageResponse->SearchPackageResult->PackageFlyerList->PackageFlyer)) {
            return $data->soapBody->SearchPackageResponse->SearchPackageResult->PackageFlyerList->PackageFlyer;
        }else{
            return false;
        }
    }
      
    /**
     *  Function to get featured package
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null 
     **/
     public function getFeaturedPackage($preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        $url = self::$_host . self::$_Package;
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetPackageAdvIdeas xmlns="http://www.westminstertravel.com/" />
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Package . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetPackageAdvIdeasResponse->GetPackageAdvIdeasResult->AdvIdea)) {
            return $data->soapBody->GetPackageAdvIdeasResponse->GetPackageAdvIdeasResult->AdvIdea;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function to get hot package cities
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null 
     **/
     public function getHotPackageCities($preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        $url = self::$_host . self::$_Package;
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetPackageCities xmlns="http://www.westminstertravel.com/" />
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Package . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetPackageCitiesResponse->GetPackageCitiesResult->City)) {
            return $data->soapBody->GetPackageCitiesResponse->GetPackageCitiesResult->City;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function to get package by provided id
     *  $id         : id of package. Default: null
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null 
     **/
     public function getPackageById($id=null,$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($id)) {
            return false;
        }
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        $url = self::$_host . self::$_Package;
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetPackageById xmlns="http://www.westminstertravel.com/">
                          <id>'.$id.'</id>
                        </GetPackageById>
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Package . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetPackageByIdResponse->GetPackageByIdResult->PackageFlyerList->PackageFlyer)) {
            return $data->soapBody->GetPackageByIdResponse->GetPackageByIdResult->PackageFlyerList->PackageFlyer;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function to get hot deal package in provided number
     *  $number     : number of package. Default: 10
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null 
     **/
     public function getTopPackage($number=10,$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($number)) {
            $number = 10;
        }
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        $url = self::$_host . self::$_Package;
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                         <GetPackageHotDeal2IndexByTop xmlns="http://www.westminstertravel.com/">
                          <topSize>'.$number.'</topSize>
                        </GetPackageHotDeal2IndexByTop>
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Package . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetPackageHotDeal2IndexByTopResponse->GetPackageHotDeal2IndexByTopResult->PackageFlyerList->PackageFlyer)) {
            return $data->soapBody->GetPackageHotDeal2IndexByTopResponse->GetPackageHotDeal2IndexByTopResult->PackageFlyerList->PackageFlyer;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function to send email by flyer
     *  $flyerId    : id of the package. Default: null
     *  $imgLink    : img link. Default: null
     *  $from       : email of customer. Default: null
     *  $tos        : Array of receiption email. Default: array()
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null 
     **/
     public function sendEmailByFlyer($flyerId=null,$imgLink=null,$from=null,$tos=array(),$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($flyerId)||empty($imgLink)||empty($from)||empty($tos)) {
            return false;
        }
        if(!empty($tos)) {
            $tosString = '';
            foreach($tos as $to) {
                $tosString .= '<string>'.$to.'</string>';
            }
        }      
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        $url = self::$_host . self::$_Package;
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                         <SendEmailByFlyer xmlns="http://www.westminstertravel.com/">
                          <flyerId>'.$flyerId.'</flyerId>
                          <imgLink>'.$imgLink.'</imgLink>
                          <from>'.$from.'</from>
                          <tos>'.$tosString.'</tos>
                        </SendEmailByFlyer>
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Package . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->SendEmailByFlyerResponse->SendEmailByFlyerResult)) {
            return $data->soapBody->SendEmailByFlyerResponse->SendEmailByFlyerResult;
        }else{
            return false;
        }      
    }
    
    
    /**
     *  Function GetFareSearchResult
     *  $triptype   : Any or Oneway or Roundtrip. Default: Any
     *  $org        : String. Default: null.
     *  $dst        : String. Defaul: null.
     *  $depdate    : Datetime. Default: 0001-01-01T00:00:00
     *  $retdate    : Datetime. Default: 0001-01-01T00:00:00  
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getFareSearchResult($dst = null,$airline=null,$fareclass=null,$triptype='Any',$depdate='0001-01-01T00:00:00',$retdate = '0001-01-01T00:00:00',$haveflight = false,$farematrix = false,$adult = 0,$child = 0,$infan = 0,$org=null,$farecat=null,$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        if(empty($triptype)) {
            $triptype = 'Any';
        }
        if(empty($org)) {
            $org = 'HKG';
        }
        if(empty($dst)) {
            $dst = '<Dst />';
        }else{
            $dst = '<Dst>'.$dst.'</Dst>';
        }
        if(empty($farecat)) {
            $farecat = '<FareCat />';
        }else{
            $farecat = '<FareCat>'.$farecat.'</FareCat>';
        }
        if(empty($fareclass)) {
            $fareclass = '<FareClass>Y</FareClass>';
        }else{
            $fareclass = '<FareClass>'.$fareclass.'</FareClass>';
        }
        if(empty($depdate)) {
            $depdate = '0001-01-01T00:00:00';
        }
        if(empty($retdate)) {
            $retdate = '0001-01-01T00:00:00';
        }
        if(empty($airline)) {
            $airline = '<Airline />';
        }else{
            $airline = '<Airline>'.$airline.'</Airline>';
        }
        if(empty($haveflight)) {
            $haveflight = 'false';
        }else{
            $haveflight = 'true';
        }
        if(empty($farematrix)) {
            $farematrix = 'false';
        }else{
            $farematrix = 'true';
        }
        if(empty($adult)) {
            $adult = 0;
        }
        if(empty($child)) {
            $child = 0;
        }
        if(empty($infan)) {
            $infan = 0;
        }
        $url = self::$_host . self::$_Airfare; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                        <soap:Body>
                            <GetFareSearchResult xmlns="http://www.westminstertravel.com/">
                              <spar>
                                <Trip>'.$triptype.'</Trip>
                                <Org>'.$org.'</Org>
                                '.$dst.'
                                '.$farecat.'
                                '.$fareclass.'
                                <DepDate>'.$depdate.'</DepDate>
                                <RetDate>'.$retdate.'</RetDate>
                                '.$airline.'
                                <HaveFlight>'.$haveflight.'</HaveFlight>
                                <IsFareMatrix>'.$farematrix.'</IsFareMatrix>
                                <AdultQty>'.$adult.'</AdultQty>
                                <ChildQty>'.$child.'</ChildQty>
                                <InfantQty>'.$infan.'</InfantQty>
                              </spar>
                            </GetFareSearchResult>
                          </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Airfare . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetFareSearchResultResponse->GetFareSearchResultResult->WestFareList->Fare)) {
            return $data->soapBody->GetFareSearchResultResponse->GetFareSearchResultResult->WestFareList->Fare;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function getLowestFare2Index
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getLowestFare2Index($preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        $url = self::$_host . self::$_Airfare; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetLowestFare2Index xmlns="http://www.westminstertravel.com/" />
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Airfare . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data),
            'SOAPAction: "http://www.westminstertravel.com/GetLowestFare2Index"'
        );
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetLowestFare2IndexResponse->GetLowestFare2IndexResult->LowestFareList->LowestFare)) {
            return $data->soapBody->GetLowestFare2IndexResponse->GetLowestFare2IndexResult->LowestFareList->LowestFare;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function GetLowestFlightSchedule
     *  $detailid   : detail id of flight. Default: null
     *  $depdate    : Departure date. Default: 0001-01-01T00:00:00
     *  $retdate    : Return date. Default: 0001-01-01T00:00:00 
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getLowestFlightSchedule($detailid = null, $depdate = null, $retdate = null,$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        if(empty($detailid)) {
            return false;
        }
        if(empty($depdate)) {
            $depdate = '0001-01-01T00:00:00';
        }
        if(empty($depdate)) {
            $retdate = '0001-01-01T00:00:00';
        }
        $url = self::$_host . self::$_Airfare; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetLowestFlightSchedule xmlns="http://www.westminstertravel.com/">
                          <spar>
                            <DetailID>'.$detailid.'</DetailID>
                            <DepDate>'.$depdate.'</DepDate>
                            <RetDate>'.$retdate.'</RetDate>
                          </spar>
                        </GetLowestFlightSchedule>
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Airfare . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetLowestFlightScheduleResponse->GetLowestFlightScheduleResult->DepartSchedules->UO_FlightSchedule)) {
            return $data->soapBody->GetLowestFlightScheduleResponse->GetLowestFlightScheduleResult->DepartSchedules->UO_FlightSchedule;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function GetFlightSchedule
     *  $detailid   : detail id of flight. Default: null
     *  $via        : city code of via city. Default: null
     *  $airline    : Airline code. Default: null
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getFlightSchedule($detailid = null, $via = null, $airline = null,$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        if(empty($detailid)) {
            return false;
        }
        if(empty($via)) {
            $via = '<via />';
        }else{
            $via = '<via>'.$via.'</via>';
        }
        if(empty($airline)) {
            $airline = '<airline />';
        }else{
            $airline = '<airline>'.$airline.'</airline>';
        }
        $url = self::$_host . self::$_Airfare; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                        <soap:Body>
                            <GetFlightSchedule xmlns="http://www.westminstertravel.com/">
                              <detailId>'.$detailid.'</detailId>
                            </GetFlightSchedule>
                          </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Airfare . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetFlightScheduleResponse->GetFlightScheduleResult->DepartSchedules->UO_FlightSchedule)) {
            return $data->soapBody->GetFlightScheduleResponse->GetFlightScheduleResult->DepartSchedules->UO_FlightSchedule;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function GetLowestFareByCountry
     *  $areaId     : Integer - Area ID. Default: 0
     *  $countryCode: Coutry code. Default: null
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getLowestFareByCountry($areaId=0,$countryCode=null,$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        if(empty($areaId)) {
            $areaId = 0;    
        }
        if(!empty($countryCode)) {
            $countryCodeString = '<countryCode>'.$countryCode.'</countryCode>';
        }else{
            $countryCodeString = '<countryCode />';
        }
        $url = self::$_host . self::$_Airfare; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetLowestFareByCountry xmlns="http://www.westminstertravel.com/">
                          <areaId>'.$areaId.'</areaId>
                          '.$countryCodeString.'
                        </GetLowestFareByCountry>
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Airfare . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data),
            "SOAPAction: \"http://www.westminstertravel.com/GetLowestFareByCountry\""
        );
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetLowestFareByCountryResponse->GetLowestFareByCountryResult->LowestFareList->LowestFare)) {
            return $data->soapBody->GetLowestFareByCountryResponse->GetLowestFareByCountryResult->LowestFareList->LowestFare;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function GetTermByDetailId
     *  $detailId   : Integer - detail ID. Default: null
     *  $tripType   : Any or Oneway or Roundtrip. Default: Any
     *  $via        : via city code. Default: null
     *  $dst        : destination city code. Default: null
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getTermByDetailId($detailId=null,$tripType='Any',$via = null,$dst = null,$preview = 'Published',$lang = 'en_us',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        if(empty($via)) {
            $viaString = '<via />';    
        }else{
            $viaString = '<via>'.$via.'</via>';
        }
        if(empty($dst)) {
            $dstString = '<dst />';    
        }else{
            $dstString = '<dst>'.$dst.'</dst>';
        }
        if(empty($tripType)) {
            $tripType = 'Any';
        }
        if(empty($detailId)) return false;
        $url = self::$_host . self::$_Airfare; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetTermByDetailId xmlns="http://www.westminstertravel.com/">
                          <detailId>'.$detailId.'</detailId>
                          <tripType>'.$tripType.'</tripType>
                          '.$viaString.'
                          '.$dstString.'
                        </GetTermByDetailId>
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Airfare . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetTermByDetailIdResponse->GetTermByDetailIdResult)) {
            return $data->soapBody->GetTermByDetailIdResponse->GetTermByDetailIdResult;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function GetCitiesByCodes
     *  $citycode   : city code. Default: array()
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getCitiesByCodes($citycode = array(),$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        if(empty($citycode)) {
            return false;
        }else{
            $cityString = '<codes>';
            foreach($citycode as $city) {
                $cityString.='<string>'.$city.'</string>';
            }
            $cityString.= '</codes>';
        }
        $url = self::$_host . self::$_Hotel; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetCitiesByCodes xmlns="http://www.westminstertravel.com/">
                          '.$cityString.'
                        </GetCitiesByCodes>
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Hotel . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetCitiesByCodesResponse->GetCitiesByCodesResult->UO_HotelCity)) {
            return $data->soapBody->GetCitiesByCodesResponse->GetCitiesByCodesResult->UO_HotelCity;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function GetCities
     *  $citycode   : city code. Default: array()
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getCities($preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        $url = self::$_host . self::$_Hotel; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetCities xmlns="http://www.westminstertravel.com/" />
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Hotel . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetCitiesResponse->GetCitiesResult->City)) {
            return $data->soapBody->GetCitiesResponse->GetCitiesResult->City;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function GetBestDealsHtl2Index
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getBestDealsHtl2Index($preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        $url = self::$_host . self::$_Hotel; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetBestDealsHtl2Index xmlns="http://www.westminstertravel.com/" />
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Hotel . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetBestDealsHtl2IndexResponse->GetBestDealsHtl2IndexResult->BestDealsHotelList->BestDealsHotel)) {
            return $data->soapBody->GetBestDealsHtl2IndexResponse->GetBestDealsHtl2IndexResult->BestDealsHotelList->BestDealsHotel;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function GetBestDealsHtlByCountry
     *  $coutrycode : 2 word of coutry code. Default: null
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getBestDealsHtlByCountry($coutry_code = null,$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        if(empty($coutry_code)) {
            $coutry_code = '<countryCode />';
        }else{
            $coutry_code = '<countryCode>'.$coutry_code.'</countryCode>';
        }
        $url = self::$_host . self::$_Hotel; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetBestDealsHtlByCountry xmlns="http://www.westminstertravel.com/">
                          '.$coutry_code.'
                        </GetBestDealsHtlByCountry>
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Hotel . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetBestDealsHtlByCountryResponse->GetBestDealsHtlByCountryResult->BestDealsHotelList->BestDealsHotel)) {
            return $data->soapBody->GetBestDealsHtlByCountryResponse->GetBestDealsHtlByCountryResult->BestDealsHotelList->BestDealsHotel;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function GetHotelInfo
     *  $coutrycode : 2 word of coutry code. Default: null
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getHotelInfo($hotel_code = null,$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        if(empty($hotel_code)) {
            return false;
        }
        $url = self::$_host . self::$_Hotel; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetHotelInfo xmlns="http://www.westminstertravel.com/">
                          <hotelCode>'.$hotel_code.'</hotelCode>
                        </GetHotelInfo>
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Hotel . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetHotelInfoResponse->GetHotelInfoResult->HotelDescriptiveContents->HotelDescriptiveContent)) {
            return $data->soapBody->GetHotelInfoResponse->GetHotelInfoResult->HotelDescriptiveContents->HotelDescriptiveContent;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function GetBestDealsHtlByCityCode
     *  $c_code     : citycode. Default: null
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getBestDealsHtlByCityCode($c_code = null,$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        if(empty($c_code)) {
            return false;
        }
        $url = self::$_host . self::$_Hotel; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetBestDealsHtlByCityCode xmlns="http://www.westminstertravel.com/">
                          <cityCode>'.$c_code.'</cityCode>
                        </GetBestDealsHtlByCityCode>
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Hotel . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetBestDealsHtlByCityCodeResponse->GetBestDealsHtlByCityCodeResult->BestDealsHotelList->BestDealsHotel)) {
            return $data->soapBody->GetBestDealsHtlByCityCodeResponse->GetBestDealsHtlByCityCodeResult->BestDealsHotelList->BestDealsHotel;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function SearchHotelRate
     *  $coutrycode : 2 word of coutry code. Default: null
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function searchHotelRate($checkindate = '0001-01-01T00:00:00',$checkoutdate='0001-01-01T00:00:00',$citycode = null,$coutry_code = null,$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        if(empty($citycode)) {
            $citycode = '';    
        }else{
            $citycode = '<CityCode>'.$citycode.'</CityCode>';
        }
        if(empty($coutry_code)) {
            $coutry_code = '';
        }else{
            $coutry_code = '<Country>'.$coutry_code.'</Country>';
        }
        if(empty($citydesc)) {
            $citydesc = '';
        }else{
            $citydesc = '<CityDesc>'.$citydesc.'</CityDesc>';
        }
        if(empty($landmark)) {
            $landmark = '';
        }else{
            $landmark = '<LandMarkCode>'.$landmark.'</LandMarkCode>';
        }
        if(empty($checkindate)) {
            $checkindate = '<CheckInDate>0001-01-01T00:00:00</CheckInDate>';
        }else{
            $checkindate = '<CheckInDate>'.$checkindate.'</CheckInDate>';
        }
        if(empty($checkoutdate)) {
            $checkoutdate = '<CheckOutDate>0001-01-01T00:00:00</CheckOutDate>';
        }else{
            $checkoutdate = '<CheckOutDate>'.$checkoutdate.'</CheckOutDate>';
        }
        if(empty($hotelcode)) {
            $hotelcode = '';
        }else{
            $hotelcode = '<HotelCode>'.$hotelcode.'</HotelCode>';
        }
        if(empty($hotelname)) {
            $hotelname = '';
        }else{
            $hotelname = '<HotelName>'.$hotelname.'</HotelName>';
        }
        if(empty($showclose)) {
            $showclose = '<ShowCloseOut>false</ShowCloseOut>';
        }else{
            $showclose = '<ShowCloseOut>'.$showclose.'</ShowCloseOut>';
        }
        if(empty($ismorerate)) {
            $ismorerate = '<IsMoreRate>false</IsMoreRate>';
        }else{
            $ismorerate = '<IsMoreRate>'.$ismorerate.'</IsMoreRate>';
        }
        if(empty($issortbyprice)) {
            $issortbyprice = '<IsSortByPrice>false</IsSortByPrice>';
        }else{
            $issortbyprice = '<IsSortByPrice>'.$issortbyprice.'</IsSortByPrice>';
        }
        if(empty($seachxml)) {
            $seachxml = '<SearchXml>false</SearchXml>';
        }else{
            $seachxml = '<SearchXml>'.$seachxml.'</SearchXml>';
        }
        if(empty($showcomrate)) {
            $showcomrate = '<ShowCommRates>false</ShowCommRates>';
        }else{
            $showcomrate = '<ShowCommRates>'.$showcomrate.'</ShowCommRates>';
        }
        if(empty($isinstantonly)) {
            $isinstantonly = '<IsInstantOnly>false</IsInstantOnly>';
        }else{
            $isinstantonly = '<IsInstantOnly>'.$isinstantonly.'</IsInstantOnly>';
        }
        if(empty($searchdesc)) {
            $searchdesc = '';
        }else{
            $searchdesc = '<SearchDesc>'.$searchdesc.'</SearchDesc>';
        }
        if(empty($searchvalue)) {
            $searchvalue = '';
        }else{
            $searchvalue = '<SearchValue>'.$searchvalue.'</SearchValue>';
        }
        if(empty($fromprice)) {
            $fromprice = '<FromPrice>0</FromPrice>';
        }else{
            $fromprice = '<FromPrice>'.$fromprice.'</FromPrice>';
        }
        if(empty($toprice)) {
            $toprice = '<ToPrice>0</ToPrice>';
        }else{
            $toprice = '<ToPrice>'.$toprice.'</ToPrice>';
        }
        if(empty($fromstar)) {
            $fromstar = '<FromStar>0</FromStar>';
        }else{
            $fromstar = '<FromStar>'.$fromstar.'</FromStar>';
        }
        if(empty($tostar)) {
            $tostar = '<ToStar>0</ToStar>';
        }else{
            $tostar = '<ToStar>'.$tostar.'</ToStar>';
        }
        
        if(empty($ispackonly)) {
            $ispackonly = '<IsPackOnly>false</IsPackOnly>';
        }else{
            $ispackonly = '<IsPackOnly>'.$ispackonly.'</IsPackOnly>';
        }
        if(empty($star)) {
            $star = '';
        }else{
            $star = '<Star>'.$star.'</Star>';
        }
        if(empty($paxnation)) {
            $paxnation = '';
        }else{
            $paxnation = '<PaxNation>'.$paxnation.'</PaxNation>';
        }
        if(empty($getsession)) {
            $getsession = 'false';
        }
        $url = self::$_host . self::$_Hotel; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <SearchHotelRate xmlns="http://www.westminstertravel.com/">
                          <spar>
                            <GetSession>'.$getsession.'</GetSession>
                            '.$coutry_code.'
                            '.$citycode.'
                            '.$citydesc.'
                            '.$landmark.'
                            '.$checkindate.'
                            '.$checkoutdate.'
                            '.$hotelcode.'
                            '.$hotelname.'
                            '.$showclose.'
                            '.$ismorerate.'
                            '.$issortbyprice.'
                            '.$seachxml.'
                            '.$showcomrate.'
                            '.$isinstantonly.'
                            '.$searchdesc.'
                            '.$searchvalue.'
                            '.$fromprice.'
                            '.$toprice.'
                            '.$fromstar.'
                            '.$tostar.'
                            '.$star.'
                            '.$ispackonly.'
                            '.$paxnation.'
                          </spar>
                        </SearchHotelRate>
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Hotel . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->SearchHotelRateResponse->SearchHotelRateResult->ExtHotel)) {
            return $data->soapBody->SearchHotelRateResponse->SearchHotelRateResult->ExtHotel;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function GetB2CCruiseVessel
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getB2CCruiseVessel($preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        $url = self::$_host . self::$_Cruise; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetB2CCruiseVessel xmlns="http://www.westminstertravel.com/" />
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Cruise . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetB2CCruiseVesselResponse->GetB2CCruiseVesselResult->CruiseVessel)) {
            return $data->soapBody->GetB2CCruiseVesselResponse->GetB2CCruiseVesselResult->CruiseVessel;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function GetCruiseById
     *  $id         : id of cruise. Default: null
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getCruiseById($id = null,$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        if(empty($id)) {
            return false;
        }
        $url = self::$_host . self::$_Cruise; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetCruiseById xmlns="http://www.westminstertravel.com/">
                          <id>'.$id.'</id>
                        </GetCruiseById>
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Cruise . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetCruiseByIdResponse->GetCruiseByIdResult)) {
            return $data->soapBody->GetCruiseByIdResponse->GetCruiseByIdResult;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function GetB2CCruiseLine
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getB2CCruiseLine($preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        $url = self::$_host . self::$_Cruise; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetB2CCruiseLine xmlns="http://www.westminstertravel.com/" />
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Cruise . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetB2CCruiseLineResponse->GetB2CCruiseLineResult->CruiseLine)) {
            return $data->soapBody->GetB2CCruiseLineResponse->GetB2CCruiseLineResult->CruiseLine;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function GetB2CCruiseDestination
     *  $hascruise  : true/false. Default: true
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getB2CCruiseDestination($hascruise = true,$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        if(empty($hascruise)) {
            $hascruise = 'false';
        }else{
            $hascruise = 'true';
        }
        $url = self::$_host . self::$_Cruise; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetB2CCruiseDestination xmlns="http://www.westminstertravel.com/">
                          <hasCruise>'.$hascruise.'</hasCruise>
                        </GetB2CCruiseDestination>
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Cruise . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetB2CCruiseDestinationResponse->GetB2CCruiseDestinationResult->CruiseDestination)) {
            return $data->soapBody->GetB2CCruiseDestinationResponse->GetB2CCruiseDestinationResult->CruiseDestination;
        }else{
            return false;
        }      
    }
    
    /**
     *  Function GetCruiseByB2CSearch
     *  $line       : line id. Default: 0
     *  $vessel     : vessel id. Defaul: 0
     *  $dst        : destination id. Default: null
     *  $preview    : Published or Preview or PublishedAndPreview. Default: Published
     *  $lang       : en_us or zh_cn or zh_tw. Default: zh_tw
     *  $filter     : ValidData or AllData. Default: ValidData
     *  $device     : Desktop or Mobile. Default: Desktop
     *  $token      : not needed. Default: null   
     **/
    public function getCruiseByB2CSearch($line = 0, $vessel = 0,$dst = null,$cat = null,$numdays = null,$year=null,$month=null,$preview = 'Published',$lang = 'zh_tw',$filter = 'ValidData',$device = 'Desktop',$token = null) {
        if(empty($token)) {
            $token = self::$_token;
        }
        if(empty($preview)) {
            $preview = 'Published';
        }
        if(empty($lang)) {
            $lang = 'zh_tw';
        }
        if(empty($filter)) {
            $filter = 'ValidData';
        }
        if(empty($device)) {
            $device = 'Desktop';
        }
        if(empty($line)) {
            $line = 0;
        }
        if(empty($vessel)) {
            $vessel = 0;
        }
        if(empty($dst)) {
            $dst = '';
        }else{
            $dst = '<DestinationIds>
                      <int>'.$dst.'</int>
                    </DestinationIds>';
        }
        if(empty($cat)) {
            $cat = '';
        }else{
            $cat = '<CategoryIds>
                      <int>'.$cat.'</int>
                    </CategoryIds>';
        }
        if(empty($numdays)) {
            $numdays = '';
        }else{
            $numdays = '<NumDays>
                          <NumDay>
                            <From>'.$numdays.'</From>
                            <To>'.$numdays.'</To>
                          </NumDay>
                        </NumDays>';
        }
        if(empty($year) && empty($month)) {
            $searchdate = '';
        }else{
            $searchdate = '<CruiseSearchDate>';
            if(!empty($year)) $searchdate.='<Year>'.$year.'</Year>';
            if(!empty($month)) $searchdate .= '<Month>
                                <int>'.$month.'</int>
                              </Month>';                 
            $searchdate .= '</CruiseSearchDate>';
        }
        $url = self::$_host . self::$_Cruise; 
        $xml_data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header>
                        <ProfileInformation xmlns="http://www.westminstertravel.com/">
                          <Preview>'.$preview.'</Preview>
                          <Token>'.$token.'</Token>
                          <Language>'.$lang.'</Language>
                          <DataFilter>'.$filter.'</DataFilter>
                          <Device>'.$device.'</Device>
                        </ProfileInformation>
                      </soap:Header>
                      <soap:Body>
                        <GetCruiseByB2CSearch xmlns="http://www.westminstertravel.com/">
                          <para>
                            '.$dst.'
                            <CruiseLineId>'.$line.'</CruiseLineId>
                            '.$cat.'
                            <VesselId>'.$vessel.'</VesselId>
                            '.$numdays.'
                            '.$searchdate.'
                          </para>
                        </GetCruiseByB2CSearch>
                      </soap:Body>
                    </soap:Envelope>';
        $header = array(
            "POST ". self::$_Cruise . " HTTP/1.1",
            "Host: ws.adholidays.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: " . strlen($xml_data)
        );
        
        $data = self::call($url,array('headers'=>$header,'xml_data'=>$xml_data));
        if(!empty($data->soapBody->GetCruiseByB2CSearchResponse->GetCruiseByB2CSearchResult->CruiseSearchDetail)) {
            return $data->soapBody->GetCruiseByB2CSearchResponse->GetCruiseByB2CSearchResult->CruiseSearchDetail;
        }else{
            return false;
        }      
    }
    
    /**
     * Public function for calling api 
     * $url : link of API
     * $params: parameters to be sent to API
     *          should be array(
     *              headers => array(...),
     *              xml_data => XML_STRING_TO_API
     *          )
     **/
    public function call($url=null,$params=array(),$return_cookie = false) {
        if(empty($url) || empty($params)) {
            return false;
        }
        $sessionId = JFactory::getSession()->getId();
        $sessionId24 = substr($sessionId,0,24);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($ch, CURLOPT_TIMEOUT_MS, 200000);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $params['headers']); 
        curl_setopt($ch, CURLOPT_POST, 1); 
        curl_setopt($ch, CURLOPT_COOKIE, 'ASP.NET_SessionId='.$sessionId24.';SessionId='.$sessionId.';'); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params['xml_data']);
        //curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36');
        if($return_cookie) {
            curl_setopt($ch, CURLOPT_HEADER  ,1);
        }
        $data = curl_exec($ch);
        if($return_cookie) {
            var_dump($data);
        }
        $err_no = curl_errno($ch);
        $err_msg = curl_error($ch);        
        curl_close($ch);
        $data = str_replace('soap:Body','soapBody',$data);
        $data = str_replace('soap:Envelope','soapEnvelope',$data);
        $data = simplexml_load_string((string) $data);
        
        if(!empty($data)) {
            return $data;
        }else{
            return false;
        }
    }
}
