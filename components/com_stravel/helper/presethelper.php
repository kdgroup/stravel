<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
define('DS', DIRECTORY_SEPARATOR); 

class presetHelper {
    //Define Preset List
    private $_db        = null;
    public $_lang       = null;
    public $_cityList   = null;
    public $_coutryList = null;
    public $_airportList= null;
    public $_airlineList= null;
    
    public function __construct($preload=false) {
        $this->_db = JFactory::getDbo();
        $this->_lang = JFactory::getLanguage()->getTag(); 
        if($preload) {
            $this->_cityList = $this->getCityList();
            $this->_coutryList = $this->getCountryList();
            $this->_airportList = $this->getAirportList();
            $this->_airlineList = $this->getAirlineList();     
        }           
    }
    public function getCountryList() {
        return $this->_db->setQuery('SELECT * FROM `#__stravel_countries`')->loadObjectList();
    }
    public function getCityList() {
        return $this->_db->setQuery('SELECT * FROM `#__stravel_cities`')->loadObjectList();
    }
    public function getAirlineList() {
        return $this->_db->setQuery('SELECT * FROM `#__stravel_airlines`')->loadObjectList();
    }
    public function getAirportList() {
        return $this->_db->setQuery('SELECT * FROM `#__stravel_airports`')->loadObjectList();
    }
    public function getCoutryByCoutryCode($country_code=null) {
        if(empty($country_code)) return false;
        return $this->_db->setQuery("SELECT * FROM `#__stravel_countries` WHERE `country_code` like '".$this->_db->escape($country_code)."'")->loadObject();
    }
    public function getAirlineByCode($airline_code=null) {
        if(empty($airline_code)) return false;
        return $this->_db->setQuery("SELECT * FROM `#__stravel_airlines` WHERE `airline_code` like '".$this->_db->escape($airline_code)."'")->loadObject();
    }
    public function getCityByCode($code=null) {
        if(empty($code)) return false;
        return $this->_db->setQuery("SELECT * FROM `#__stravel_cities` WHERE `city_code` like '".$this->_db->escape($code)."'")->loadObject();
    }
    public function getMoreInfo($list = null) {
        if(empty($list)) {
            return false;
        }
        $arrayList = array();
        foreach($list as $item) {
            $airline = $this->getAirlineByCode($item->Airline);
            $destination = $this->getCityByCode($item->Dst);
            $item = get_object_vars($item);          
            if($this->_lang=='en-US') {
                $item['AirlineName'] = $airline->airline_english;
                if(!empty($destination->city_en)) {
                    $item['DstName'] = $destination->city_en;
                }else{
                    $item['DstName'] = $destination->city_cn;
                } 
            }else{
                $item['AirlineName'] = $airline->airline_chinese;
                if(!empty($destination->city_cn)) {
                    $item['DstName'] = $destination->city_cn;
                }else{
                    $item['DstName'] = $destination->city_en;
                }    
            }
            if(!empty($item['DstName'])) {
                $arrayList[] = (object) $item;
            }            
        }
        return $arrayList;    
    }
    public function getSearchPanel($page = 1) {
        if(empty($page)) {
            $page = 1;
        }
        switch($page) {
            case 1:
                $pk_checked = 'checked="checked"';$pk_cls = 'in active';
                $jet_checked = $air_checked = $cruise_checked = $hotel_checked = '';
                $jet_cls = $air_cls = $cruise_cls = $hotel_cls = '';
                break;
            case 2:
                $air_checked = 'checked="checked"';$air_cls = 'in active';
                $jet_checked = $pk_checked = $cruise_checked = $hotel_checked = '';
                $jet_cls = $pk_cls = $cruise_cls = $hotel_cls = '';
                break;
            case 4:
                $cruise_checked = 'checked="checked"';$cruise_cls = 'in active';
                $jet_checked = $air_checked = $pk_checked = $hotel_checked = '';
                $jet_cls = $air_cls = $pk_cls = $hotel_cls = '';
                break;
            case 3:
                $hotel_checked = 'checked="checked"';$hotel_cls = 'in active';
                $jet_checked = $air_checked = $cruise_checked = $pk_checked = '';
                $jet_cls = $air_cls = $cruise_cls = $pk_cls = '';
                break;
            case 5:
                $jet_checked = 'checked="checked"';$jet_cls = 'in active';
                $pk_checked = $air_checked = $cruise_checked = $hotel_checked = '';
                $pk_cls = $air_cls = $cruise_cls = $hotel_cls = '';
                break;
        }
        
        $doc = JFactory::getDocument();
        $farejs = array();
        $mergeaircities = apiHelper::getContent(array('CONTENT','B2C','FARE','CITY'));
        foreach($mergeaircities as $item) {
            $infos = explode("\n",$item->Content);            
            $farejs[] = '["'.$item->Keyword.'","'.$infos[1].'"]';
        }
        $farejs = '['.implode(',',$farejs).']';
        $hoteljs = array();
        $mergehtlcities = apiHelper::getContent(array('CONTENT','B2C','HOTEL','HOTELCITY'));
        foreach($mergehtlcities as $item) {
            $infos = explode("\n",$item->Content);            
            $hoteljs[] = '["'.$item->Keyword.'","'.$infos[1].'"]';
        }
        $hoteljs = '['.implode(',',$hoteljs).']';
        $doc->addScriptDeclaration("
            var farecities = $farejs;
            var hotelcities = $hoteljs;
        ");
        
        switch(JRequest::getVar('type')) {
            case 'Y':
                $pclass = JText::_('COM_STRAVEL_ECONOMY');
                break;
            case 'C':
                $pclass = JText::_('COM_STRAVEL_BUSSINESS');
                break;
            case 'F':
                $pclass = JText::_('COM_STRAVEL_FIRST_CLASS');
                break;
			case 'P':
                $pclass = JText::_('COM_STRAVEL_PREMIUM_ECONOMY_CLASS');
                break;
            default:
                $pclass = JText::_('COM_STRAVEL_ECONOMY');
                break;
        }
        switch(JRequest::getVar('triptype',2)) {
            case 1:
                $ptrip = JText::_('COM_STRAVEL_SINGLETRIP');
                break;
            case 2:
                $ptrip = JText::_('COM_STRAVEL_ROUNDTRIP');
                break;
            default:
                $ptrip = JText::_('COM_STRAVEL_ROUNDTRIP');
                break;
        }
        //Package city
        $hotCities = apiHelper::getHotPackageCities();
        $i=1;
        $lang = strtolower(str_replace('-','_',$this->_lang));
        $citynamelang = 'CityName_'.$lang;
        $pkcity = '';
        $cls = 'displaynone normalcity';
        foreach($hotCities as $city) {
            //if($i>10) $cls = 'displaynone'; else $cls = '';
            //if($i==1) {$extratxt = '<span class="hottxt">'.JText::_('COM_STRAVEL_HOTCITY_LABEL').'</span>';}else{ $extratxt = ''; }
            $pkcity .= '<li class="'.$cls.'" data-cityname="'.$city->$citynamelang.'" data-citycode="'.$city->CityCode.'" data-name="'.$city->CityName_zh_tw.' ('.$city->CityCode.') '.$city->CityName_zh_cn.' '.$city->CityName_en_us.'" data-order="'.$i.'"><a href="#" onclick="return false;">'.$city->CityName_zh_tw.' ('.$city->CityCode.') '.$city->CityName_zh_cn.' '.$city->CityName_en_us.'</a></li>';
            $i++;            
        }
        
        //Package merge city	    
        $mergecities = apiHelper::getContent(array('CONTENT','B2C','PACKAGE','P02'));
        $i = 1;
        $mergecityhtml = ''; 
        foreach($mergecities as $city) {            
            if($i==1) {$extratxt = '<span class="hottxt">'.JText::_('COM_STRAVEL_HOTCITY_LABEL').'</span>';}else{ $extratxt = ''; }
            if($i>10) {break;} $i++;
            $infos = explode("\n",$city->Content);
            $mergecityhtml .= '<li class="hotcity" data-cityname="'.$infos[0].'" data-citycode="'.$city->Keyword.'" data-name=""><a href="#" onclick="return false;">'.$infos[1].$extratxt.'</a></li>';
        }
        
        $featuredPackage = apiHelper::getFeaturedPackage();
        //PAckage featured
        $packageFeatured = '<li data-name="'.JText::_('COM_STRAVEL_ALL_PACKAGE_FEATURE').'" data-id="0"><a href="#" onclick="return false;">'.JText::_('COM_STRAVEL_ALL_PACKAGE_FEATURE').'</a></li>';
        foreach($featuredPackage as $item) {
            if($item->Lang==$lang && $item->Type!=12 && $item->Type!=13) {
                $packageFeatured .= '<li data-name="'.$item->Name.'" data-id="'.$item->Id.'"><a href="#" onclick="return false;">'.$item->Name.'</a></li>';
            }
        }
        if(JRequest::getVar('ideaname')) {
            $featuredPackagetxt = JRequest::getVar('ideaname');
        }else{
            $featuredPackagetxt = JText::_('COM_STRAVEL_ALL_PACKAGE_FEATURE');
        }
        
        //Nights
        $nights = '';
        for($i=1;$i<=14;$i++) {
            $nights .= '<li><a href="#" data-id="'.$i.'" onclick="return false;">'.$i.'</a></li>';
        }
        
        //Cruise Destination
        $cdestination      = apiHelper::getB2CCruiseDestination(true);
        $cruise_dst = '';
        foreach($cdestination as $item) {
            $cruise_dst .= '<li><a href="#" data-id="'.$item->Id.'" onclick="return false;">'.$item->DstName.'</a></li>';
        }
        
        //Month
        $month = '';
        for($i=1;$i<=12;$i++) {
            $month.= '<li><a data-id="'.$i.'" href="#" onclick="return false;">'.$i.JText::_('COM_STRAVEL_MONTH').'</a></li>';
        }
        
        //Cruise cpn
        $ccruisecpn        = apiHelper::getB2CCruiseLine();
        $cruisecpn = '';
        $cruisecpntxt = JRequest::getVar('cruisecpnname');
        if(empty($cruisecpntxt)) {
            $cruisecpntxt = JText::_('COM_STRAVEL_SELECT_CRUISE');
        }
        foreach($ccruisecpn as $item){
            $cruisecpn.= '<li><a data-id="'.$item->Id.'" href="#" onclick="return false">'.$item->CruiseLineName.'</a></li>';
        }
        
        //Cruise vessel
        $ccruisevessel     = apiHelper::getB2CCruiseVessel();
        $cruise_vessel = '';
        $cruise_vesseltxt = JRequest::getVar('cruisevesselname');
        if(empty($cruise_vesseltxt)) {
            $cruise_vesseltxt = JText::_('COM_STRAVEL_SELECT_CRUISE2');
        }
        foreach($ccruisevessel as $item) {
            $cruise_vessel.= '<li><a data-id="'.$item->Id.'" href="#" onclick="return false">'.$item->VesselName.'</a></li>';
        }
        
        $yeartxt = JRequest::getVar('year');
        if(empty($yeartxt)) {
            $yeartxt = date('Y');
        }
        
        $html = '
            <!-- Start search panel -->
            <div class="searchform search">
                <div class="tabbtn" role="tablist">
                    <div class="row">
                        <div class="col-xs-4">
                            <label data-toggle="tab" data-target="#package-search" class="radio-inline">
                              <input '.$pk_checked.' type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1"> <img class="packageIcon" src="images/Package.png" alt="package" /> '.JText::_('COM_STRAVEL_SEARCH_PACKAGE').'
                            </label>
                        </div>
                        <div class="col-xs-4">
                            <label data-toggle="tab" data-target="#airticket-search" class="radio-inline">
                              <input '.$air_checked.' type="radio" name="inlineRadioOptions" id="inlineRadio2" value="2"> <img class="airticketIcon" src="images/air.png" alt="airticket" /> '.JText::_('COM_STRAVEL_SEARCH_AIRTICKET').'
                            </label>
                        </div>
                        <div class="col-xs-4">
                            <label data-toggle="tab" data-target="#hotel-search" class="radio-inline">
                              <input '.$hotel_checked.' type="radio" name="inlineRadioOptions" id="inlineRadio3" value="3"> <img class="hotelIcon" src="images/Hotel.png" alt="hotel" /> '. JText::_('COM_STRAVEL_SEARCH_HOTEL').'
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <label data-toggle="tab" data-target="#cruise-search" class="radio-inline">
                              <input '.$cruise_checked.' type="radio" name="inlineRadioOptions" id="inlineRadio4" value="4"> <img class="cruiseIcon" src="images/Cruise.png" alt="cruise" /> '. JText::_('COM_STRAVEL_SEARCH_CRUISE').'
                            </label>
                        </div>
                        <div class="col-xs-8 hide">
                            <label data-toggle="tab" data-target="#jettour-search" class="radio-inline">
                              <input '.$jet_checked.' type="radio" name="inlineRadioOptions" id="inlineRadio5" value="5"> <img class="jettourIcon" src="images/Tour.png" alt="jettour" />'. JText::_('COM_STRAVEL_SEARCH_TOUR').'
                            </label>
                        </div>
                    </div>
                    
                </div>
                <!-- Search form -->
                <div class="tab-content">
                      <!-- package search -->
                      <div class="tab-pane fade '.$pk_cls.'" id="package-search">
                        <form action="index.php" name="packageSearch" method="get" role="form" autocomplete="off">
                            <input type="hidden" name="option" value="com_stravel" />
                            <input type="hidden" name="view" value="packagehome" />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <label>'.JText::_('COM_STRAVEL_DEPARTURE_CITY_HONGKONG').'</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="arrivedCity">'.JText::_('COM_STRAVEL_ARRIVED_CITY').'</label>
                                        <div class="hotelcitylist dropdown">
                                            <input value="'.JRequest::getVar('cityname','').'" data-target="#" data-toggle="dropdown" id="arrivedCity" type="text" class="form-control" name="cityname" placeholder="'.JText::_('COM_STRAVEL_CITY_SELECT_PLACEHOLDER').'">
                                            <ul class="packagecitylistitem dropdown-menu">
                                                '.$mergecityhtml.$pkcity.'
                                            </ul>
                                            <input value="'.JRequest::getVar('city','').'" type="hidden" name="city" />
                                        </div>                                    
                                    </div>    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="departureDate">'.JText::_('COM_STRAVEL_DEPARTURE_DATE').'</label>
                                        <input value="'.JRequest::getVar('departureDate','').'" name="departureDate" type="text" class="form-control datepicker" id="departureDate">
                                    </div>    
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="arrivedDate">'.JText::_('COM_STRAVEL_ARRIVED_DATE').'</label>
                                        <input value="'.JRequest::getVar('arrivedDate','').'" name="arrivedDate" type="text" class="form-control datepicker" id="arrivedDate">
                                    </div> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>'.JText::_('COM_STRAVEL_THEME_PACKAGE').'</label>
                                        <div class="dropdown themepackage">
                                            <span data-toggle="dropdown" data-target="#" class="fakeselect form-control">'.$featuredPackagetxt.'</span>
                                            <ul class="dropdown-menu">
                                                '.$packageFeatured.'
                                            </ul>
                                            <input value="'.JRequest::getVar('ideaid','').'" type="hidden" name="ideaid">
                                            <input value="'.JRequest::getVar('ideaname','').'" type="hidden" name="ideaname">
                                        </div>
                                    </div>    
                                </div>    
                            </div>
                            <div class="row search_button_row">
                                <div class="col-md-12 search_button">
                                    <button class="searchbtn">'.JText::_('SEARCH').'</button>
                                </div>
                            </div>
                        </form>  
                      </div>
                      <!-- End package search -->
                      <!-- Search air ticket -->
                      <div class="tab-pane fade '.$air_cls.'" id="airticket-search">
                        <form action="index.php" name="airticketSearch" method="get" role="form" autocomplete="off">
                            <input type="hidden" name="option" value="com_stravel" />
                            <input type="hidden" name="view" value="airsearchresult" />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <label>'.JText::_('COM_STRAVEL_DEPARTURE_CITY_HONGKONG').'</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="arrivedCity2">'.JText::_('COM_STRAVEL_ARRIVED_CITY').'</label>
                                        <div class="aircitylist dropdown">
                                            <input value="'.JRequest::getVar('cityname','').'" data-target="#" data-toggle="dropdown" placeholder="'.JText::_('COM_STRAVEL_CITY_SELECT_PLACEHOLDER').'" id="arrivedCity2" type="text" class="form-control showname" name="cityname">
                                            <ul class="aircitylistitem dropdown-menu"></ul>
                                            <input value="'.JRequest::getVar('citycode','').'" type="hidden" name="citycode" />
                                        </div>                                    
                                    </div>    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="departureDate2">'.JText::_('COM_STRAVEL_DEPARTURE_DATE').'</label>
                                        <input value="'.JRequest::getVar('departureDate','').'" name="departureDate" type="text" class="form-control datepicker" id="departureDate2">
                                    </div>    
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="arrivedDate2">'.JText::_('COM_STRAVEL_ARRIVED_DATE').'</label>
                                        <input value="'.JRequest::getVar('arrivedDate','').'" name="arrivedDate" type="text" class="form-control datepicker" id="arrivedDate2">
                                    </div> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group airclass">
                                        <label>'.JText::_('COM_STRAVEL_CLASS').'</label>
                                        <div class="dropdown">
                                            <span data-toggle="dropdown" data-target="#" class="fakeselect form-control">'.$pclass.'</span>
                                            <ul class="dropdown-menu" role="menu">
                                                <li data-id="Y"><a href="#" onclick="return false;">'.JText::_('COM_STRAVEL_ECONOMY').'</a></li>
                                                <li data-id="C"><a href="#" onclick="return false;">'.JText::_('COM_STRAVEL_BUSSINESS').'</a></li>
                                                <li data-id="F"><a href="#" onclick="return false;">'.JText::_('COM_STRAVEL_FIRST_CLASS').'</a></li>
                                                <li data-id="P"><a href="#" onclick="return false;">'.JText::_('COM_STRAVEL_PREMIUM_ECONOMY_CLASS').'</a></li>
                                            </ul>
                                        </div> 
                                        <input value="'.JRequest::getVar('type','').'" type="hidden" name="type">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group triptype">
                                        <label>&nbsp;</label>
                                        <div class="dropdown">
                                            <span data-toggle="dropdown" data-target="#" class="fakeselect form-control">'.$ptrip.'</span>
                                            <ul class="dropdown-menu" role="menu">
                                                <li data-id="1"><a href="#" onclick="return false;">'.JText::_('COM_STRAVEL_SINGLETRIP').'</a></li>
                                                <li data-id="2"><a href="#" onclick="return false;">'.JText::_('COM_STRAVEL_ROUNDTRIP').'</a></li>
                                            </ul>
                                        </div>
                                        <input value="'.JRequest::getVar('triptype','').'" type="hidden" name="triptype">
                                    </div>    
                                </div>    
                            </div>
                            <div class="row search_button_row">
                                <div class="col-md-12 search_button">
                                    <button class="searchbtn">'.JText::_('SEARCH').'</button>
                                </div>
                            </div>
                        </form>
                      </div>
                      <!-- End search air ticket -->
                      
                      <!-- Hotel search -->
                      <div class="tab-pane fade '.$hotel_cls.'" id="hotel-search">
                        <form action="index.php" name="hotelSearch" method="get" role="form" autocomplete="off">
                            <input type="hidden" name="option" value="com_stravel" />
                            <input type="hidden" name="view" value="hotelresult" />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="destinationht">'.JText::_('COM_STRAVEL_DESTINATION').'</label>
                                        <div class="hotelcitylist dropdown">
                                            <input value="'.JRequest::getVar('destination','').'" id="destinationht" data-target="#" data-toggle="dropdown" type="text" class="form-control" name="destination" placeholder="'.JText::_('COM_STRAVEL_CITY_SELECT_PLACEHOLDER').'">
                                            <ul class="hotelcitylistitem dropdown-menu">
                                            </ul>
                                            <input value="'.JRequest::getVar('city','').'" type="hidden" name="city" />
                                        </div>                                    
                                    </div>    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="checkinDate">'.JText::_('COM_STRAVEL_CHECKIN_DATE').'</label>
                                        <input value="'.JRequest::getVar('checkinDate','').'" name="checkinDate" type="text" class="form-control datepicker" id="checkinDate">
                                    </div> 
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>'.JText::_('COM_STRAVEL_NIGHTS').'</label>
                                        <div class="dropdown hotelnights">
                                            <span data-target="#" data-toggle="dropdown" class="fakeselect form-control">'.JRequest::getVar('nights','1').'</span>
                                            <ul class="dropdown-menu">
                                                '.$nights.'
                                            </ul>
                                        </div>
                                        <input value="'.JRequest::getVar('nights','1').'" type="hidden" name="nights">
                                    </div> 
                                </div>
                            </div>
                            <div class="row search_button_row">
                                <div class="col-md-12 search_button">
                                    <button class="searchbtn">'.JText::_('SEARCH').'</button>
                                </div>
                            </div>
                        </form>
                      </div>
                      <!-- End hotel search -->
                      <!-- Cruise search -->
                      <div class="tab-pane fade '.$cruise_cls.'" id="cruise-search">
                        <form action="index.php" name="cruiseSearch" method="get" role="form" autocomplete="off">
                            <input type="hidden" name="option" value="com_stravel" />
                            <input type="hidden" name="view" value="cruisehome" />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>'.JText::_('COM_STRAVEL_SELECT_ROUTE').'</label>
                                        <div class="dropdown selectroutecruise">
                                            <span data-target="#" data-toggle="dropdown" class="fakeselect form-control">'.JRequest::getVar('cruisedstname',JText::_('COM_STRAVEL_SELECT_ROUTE')).'</span>
                                            <ul class="dropdown-menu">
                                            '.$cruise_dst.'
                                            </ul>
                                        </div>                                    
                                        <input value="'.JRequest::getVar('cruisedst','').'" type="hidden" name="cruisedst">
                                        <input value="'.JRequest::getVar('cruisedstname','').'" type="hidden" name="cruisedstname">
                                    </div>    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>'.JText::_('COM_STRAVEL_SELECT_YEAR').'</label>
                                        <div class="dropdown cruiseyear">
                                            <span data-target="#" data-toggle="dropdown" class="fakeselect form-control">'.$yeartxt.'</span>
                                            <ul class="dropdown-menu">
                                                <li><a data-id="2014" href="#" onclick="return false;">2014</a></li>
                                                <li><a data-id="2015" href="#" onclick="return false;">2015</a></li>
                                            </ul>
                                        </div>
                                        <input value="'.JRequest::getVar('year','').'" type="hidden" name="year">
                                    </div> 
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>'.JText::_('COM_STRAVEL_SELECT_MONTH').'</label>
                                        <div class="dropdown">
                                            <span data-target="#" data-toggle="dropdown" class="fakeselect form-control">'.JRequest::getVar('month').'月</span>
                                            <ul class="dropdown-menu">
                                            '.$month.'
                                            </ul>
                                        </div>                                    
                                        <input value="'.JRequest::getVar('month','').'" type="hidden" name="month">
                                    </div>
                                </div>
                                <div class="col-md-4 hide">
                                    <div class="form-group">
                                        <label>'.JText::_('COM_STRAVEL_NIGHTS').'</label>
                                        <div class="dropdown">
                                            <span data-toggle="dropdown" data-target="#" class="fakeselect form-control">&lt;6</span>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a data-id="1" href="#" onclick="return false;">&lt;6</a></li>
                                                <li><a data-id="2" href="#" onclick="return false;">7-9</a></li>
                                                <li><a data-id="3" href="#" onclick="return false;">10-14</a></li>
                                                <li><a data-id="4" href="#" onclick="return false;">15-25</a></li>
                                                <li><a data-id="5" href="#" onclick="return false;">&gt;26</a></li>
                                            </ul>
                                        </div>                                            
                                        <input value="'.JRequest::getVar('numday','').'" type="hidden" name="numday">
                                    </div>
                                </div>
                                    
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>'.JText::_('COM_STRAVEL_SELECT_CRUISE').'</label>
                                        <div class="dropdown selectcruisecpn">
                                            <span data-target="#" data-toggle="dropdown" class="fakeselect form-control">'.$cruisecpntxt.'</span>
                                            <ul class="dropdown-menu">
                                            '.$cruisecpn.'
                                            </ul>
                                        </div>
                                        <input value="'.JRequest::getVar('cruisecpn','').'" type="hidden" name="cruisecpn">
                                        <input value="'.JRequest::getVar('cruisecpnname','').'" type="hidden" name="cruisecpnname">
                                    </div>    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>'.JText::_('COM_STRAVEL_SELECT_CRUISE2').'</label>
                                        <div class="dropdown selectcruisevessel">
                                            <span data-target="#" data-toggle="dropdown" class="fakeselect form-control">'.$cruise_vesseltxt.'</span>
                                            <ul class="dropdown-menu">
                                            '.$cruise_vessel.'
                                            </ul>
                                        </div>                                    
                                        <input value="'.JRequest::getVar('cruisevessel','').'" type="hidden" name="cruisevessel">
                                        <input value="'.JRequest::getVar('cruisevesselname','').'" type="hidden" name="cruisevesselname">
                                    </div>    
                                </div>
                            </div>
                            <div class="row search_button_row">
                                <div class="col-md-12 search_button">
                                    <button class="searchbtn">'.JText::_('SEARCH').'</button>
                                </div>
                            </div>
                        </form>
                      </div>
                      <!-- End cruise search -->
                      
                      <!-- Jettour search -->
                      <div class="tab-pane fade '.$jet_cls.'" id="jettour-search">
                            <form action="index.php" name="jettourSearch" method="get" role="form" autocomplete="off">
                                <input type="hidden" name="option" value="com_stravel" />
                                <input type="hidden" name="task" value="" />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>'.JText::_('COM_STRAVEL_CHOOSE_COUNTRY').'</label>
                                            <span class="fakeselect form-control">台中</span>
                                            <input type="hidden" name="jettourChooseCountry">
                                        </div> 
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>'.JText::_('COM_STRAVEL_CHOOSE_CITY').'</label>
                                            <span class="fakeselect form-control">台中</span>
                                            <input name="jettourChooseCity" type="hidden" />
                                        </div> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="jettourDepartureDate">'.JText::_('COM_STRAVEL_DEPARTURE_DATE').'</label>
                                            <input name="jettourDepartureDate" type="text" class="form-control datepicker" id="jettourDepartureDate">
                                        </div> 
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="jettourArrivedDate">'.JText::_('COM_STRAVEL_ARRIVED_DATE').'</label>
                                            <input name="jettourArrivedDate" type="text" class="form-control datepicker" id="jettourArrivedDate">
                                        </div> 
                                    </div>
                                </div>
                                <div class="row search_button_row">
                                    <div class="col-md-12 search_button">
                                        <button class="searchbtn">'.JText::_('SEARCH').'</button>
                                    </div>
                                </div>
                            </form>
                      </div>  
                      <!-- End jettour search -->
                    </div>
                <!-- End search form -->
            </div>
        ';
        return $html;    
    }
}
?>