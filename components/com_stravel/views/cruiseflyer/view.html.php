<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');
 
class sTravelViewCruiseFlyer extends JViewLegacy
{
	// Overwriting JView display method
	function display($tpl = null) 
	{
	    $this->cruisevessel = apiHelper::getB2CCruiseVessel();   
        $this->destination = apiHelper::getB2CCruiseDestination(true);
        $this->cruisecpn = apiHelper::getB2CCruiseLine();

	    $cruisecpn = apiHelper::getContent(array('CONTENT','B2C','CRUISE','CRUISECOMPANY'));
        $cruisedst = apiHelper::getContent(array('CONTENT','B2C','CRUISE','CRUISEDEST'));
        $hotcruisecpn = array();
        foreach($cruisecpn as $cpn) {
            foreach($this->cruisecpn as $ccpn) {
                if(strcmp($cpn->Content,$ccpn->CruiseLineName)===0) {
                    $hotcruisecpn[] = (object) array('Id'=>$ccpn->Id,'Content'=>$ccpn->CruiseLineName);
                }
            }        
        }
        
        $hotcruisedst = array();
        foreach($cruisedst as $cpn) {
            foreach($this->destination as $ccpn) {
                if(strcmp($cpn->Content,$ccpn->DstName)===0) {
                    $hotcruisedst[] = (object) array('Id'=>$ccpn->Id,'Content'=>$ccpn->DstName);
                }
            }        
        }
        $this->hotcruisecpn = $hotcruisecpn;
        $this->hotcruisedst = $hotcruisedst;   
        $id = JRequest::getVar('id');
        if(empty($id)) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_STRAVEL_CRUISE_NOT_FOUND'),'error');
            JFactory::getApplication()->redirect('index.php?option=com_stravel&view=cruisehome'); 
        }else{
            $this->cruise = apiHelper::getCruiseById($id);
            //var_dump($this->cruise);
            $this->allcruises = apiHelper::getCruiseByB2CSearch();
        }
        
        //Metadata
        $doc = JFactory::getDocument();
        $metadata = apiHelper::getMetadata();
        if(!empty($metadata)) {
            $doc->setTitle($metadata->page_title);
            $doc->setDescription($metadata->page_description);       
            $doc->setMetaData('keywords',$metadata->page_keyword);
        }else{
            $page_params = JFactory::getApplication()->getParams();        
            $pagedesc = $page_params->get('menu-meta_description');
            $pagekeywords = $page_params->get('menu-meta_keywords');
            $title = $doc->getTitle();
            if(!empty($this->cruise->Title)){
                $name = $this->cruise->Title;
                $title = $title .' - '. $name;
                $pagedesc = $name. ' - ' .$pagedesc;
                $pagekeywords = $name. ', ' . $pagekeywords;
            }
            $cruisekeyword = (string) $this->cruise->Keyword;
            if(!empty($cruisekeyword)) {
                $pagekeywords = $cruisekeyword .','.$pagekeywords;
            }
            $doc->setTitle($title);
            $doc->setDescription($pagedesc);       
            $doc->setMetaData('keywords',$pagekeywords);
        }
        
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Display the view
		parent::display($tpl);
	}
}
