<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');
 
/**
 * Ola class for the Ola Component
 */
class sTravelViewCruiseHome extends JViewLegacy
{
	// Overwriting JView display method
	function display($tpl = null) 
	{
	    $this->destination = apiHelper::getB2CCruiseDestination(true);
        $this->cruisevessel = apiHelper::getB2CCruiseVessel();
        $this->cruisecpn = apiHelper::getB2CCruiseLine();   
	    $cruisecpn = apiHelper::getContent(array('CONTENT','B2C','CRUISE','CRUISECOMPANY'));
        $cruisedst = apiHelper::getContent(array('CONTENT','B2C','CRUISE','CRUISEDEST'));
        $this->disclaimer = apiHelper::getContent(array('CONTENT','B2C','CRUISE','CRUISEDISCLAIMER'));

        $hotcruisecpn = array();
        foreach($cruisecpn as $cpn) {
            foreach($this->cruisecpn as $ccpn) {
                if(strcmp($cpn->Content,$ccpn->CruiseLineName)===0) {
                    $hotcruisecpn[] = (object) array('Id'=>$ccpn->Id,'Content'=>$ccpn->CruiseLineName);
                }
            }        
        }
        
        $hotcruisedst = array();
        foreach($cruisedst as $cpn) {
            foreach($this->destination as $ccpn) {
                if(strcmp($cpn->Content,$ccpn->DstName)===0) {
                    $hotcruisedst[] = (object) array('Id'=>$ccpn->Id,'Content'=>$ccpn->DstName);
                }
            }        
        }
        $this->hotcruisecpn = $hotcruisecpn;
        $this->hotcruisedst = $hotcruisedst;
        
        $cruiseline = JRequest::getInt('cruisecpn',0);
        $cruisevessel = JRequest::getInt('cruisevessel',0);
        $cruisedstid = JRequest::getInt('cruisedst',null);
        $cruisecat = JRequest::getInt('cruisecat',null); 
        $numdays = JRequest::getInt('numdays',null); 
        $year = JRequest::getInt('year',null); 
        $month = JRequest::getInt('month',null); 
        
        $this->cruises = apiHelper::getCruiseByB2CSearch($cruiseline,$cruisevessel,$cruisedstid,$cruisecat,$numdays,$year,$month);
        //var_dump(($this->cruises));
        $year = array();
        foreach($this->cruises as $cruise) {
            $year[] = date('Y',strtotime($cruise->TravelPeriodFr));
        }
        $this->yearlist = array_unique($year);
        
        //Metadata
        $doc = JFactory::getDocument();
        $metadata = apiHelper::getMetadata();
        if(!empty($metadata)) {
            $doc->setTitle($metadata->page_title);
            $doc->setDescription($metadata->page_description);       
            $doc->setMetaData('keywords',$metadata->page_keyword);
        }else{
            $page_params = JFactory::getApplication()->getParams();        
            $pagedesc = $page_params->get('menu-meta_description');
            $pagekeywords = $page_params->get('menu-meta_keywords');
            $title = $doc->getTitle();
            $cityname = JRequest::getVar('cruisecpnname');
            if(!empty($cityname)){
                $title = $title .' - '. $cityname;
                $pagedesc = $cityname.JText::_('COM_STRAVEL_SEARCH_CRUISE'). ' - ' .$pagedesc;
                $pagekeywords = $cityname.JText::_('COM_STRAVEL_SEARCH_CRUISE') . ', ' . $pagekeywords;
            }
            $ideaname = JRequest::getVar('cruisedstname');
            if(!empty($ideaname)) {
                $title = $title . ' - ' . $ideaname;
                $pagedesc = $ideaname. ' - ' .$pagedesc;
                $pagekeywords = $ideaname.', ' . $pagekeywords;
            }
            $doc->setTitle($title);
            $doc->setDescription($pagedesc);       
            $doc->setMetaData('keywords',$pagekeywords);
        }
        //var_dump(($this->cruisecpn));   
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Display the view
		parent::display($tpl);
	}
}
