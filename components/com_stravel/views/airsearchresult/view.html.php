<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');
 
class sTravelViewAirSearchResult extends JViewLegacy
{
	// Overwriting JView display method
	function display($tpl = null) 
	{
	    $this->presethelper = new presetHelper;
        //Get parameter
        $this->hottestcity = apiHelper::getContent(array('CONTENT','B2C','FARE','CITY'));
        $this->disclaimer = apiHelper::getContent(array('CONTENT','B2C','FARE','FLIGHTDISCLAIMER'));
        $departure = '';
        //Get parameters
        $depdate = JRequest::getVar('departureDate');
        if(empty($depdate)) {
            $depdate = date('Y-m-d\TH:i:s',time()+60*60*24*10);
        }else{
            $departuredate .= $depdate;
            $depdate = $depdate.'T00:00:00';
        }
        
        $retdate = JRequest::getVar('arrivedDate');
        if(empty($retdate)) {
            $retdate = '0001-01-01T00:00:00';
            $retdate = date('Y-m-d\TH:i:s',time()+60*60*24*365);
        }else{
            if(strlen($departuredate)>0) {
                $departuredate .= ' '.JText::_('COM_STRAVEL_TO').' '.$retdate;
            }else{
                $departuredate .= $retdate;
            }
            $retdate = $retdate.'T00:00:00';
        }
        $this->departuredate = $departuredate;
        $triptype = JRequest::getVar('triptype');
        if(empty($triptype)) {
            $triptype = 'Any';    
        }else{
            switch($triptype) {
                case 1:
                    $triptype = 'Oneway';
                    break;
                case 2:
                    $triptype = 'Roundtrip';
                    break;
                default:
                    $triptype = 'Any';
                    break;
            }
        }
        $fareclass = JRequest::getVar('type','Y');
        $citycode = JRequest::getVar('citycode');
        $aircode = JRequest::getVar('airlinecode');

        $cityname = JRequest::getVar('cityname');
        if(!empty($cityname)) {
            $this->cityName = $cityname;
        }
        $this->airresultlist = apiHelper::getFareSearchResult($citycode,$aircode,$fareclass,$triptype,$depdate,$retdate);
	    //$airlist = apiHelper::getLowestFareByCountry();
        
        //$airlist = $this->presethelper->getMoreInfo($airlist);
        //$this->top20air = $airlist;
        //Temporary
        $airCo = array();
        $this->lowestPrice = (int)$this->airresultlist->PriceList->FarePrice->adult;
        foreach($this->airresultlist as $item) {
            if($item->PriceList->FarePrice->adult < $this->lowestPrice) {
                $this->lowestPrice = $item->PriceList->FarePrice->adult;
            }
            $airCo[] = (string) $item->airline_code;         
        }
        $airCotmp = array_unique($airCo);
        $airCo = array();
        foreach($airCotmp as $air) {
            $airCo[] = $this->presethelper->getAirlineByCode($air);
        }
        
        //$airCo = array();
        //foreach($airlist as $air) {
        //    $airCo[$air->Airline] = $air->AirlineName;
        //}
        $this->airCo = $airCo;
        
        //Metadata
        $doc = JFactory::getDocument();
        $metadata = apiHelper::getMetadata();
        if(!empty($metadata)) {
            $doc->setTitle($metadata->page_title);
            $doc->setDescription($metadata->page_description);       
            $doc->setMetaData('keywords',$metadata->page_keyword);
        }else{
            $page_params = JFactory::getApplication()->getParams();        
            $pagedesc = $page_params->get('menu-meta_description');
            $pagekeywords = $page_params->get('menu-meta_keywords');
            $title = $doc->getTitle();
            if(!empty($cityname)){
                $title = $title .' - '. $cityname;
                $pagedesc = $cityname.JText::_('COM_STRAVEL_SEARCH_AIRTICKET'). ' - ' .$pagedesc;
                $pagekeywords = $cityname.JText::_('COM_STRAVEL_SEARCH_AIRTICKET') . ', ' . $pagekeywords;
            }
            $doc->setTitle($title);
            $doc->setDescription($pagedesc);       
            $doc->setMetaData('keywords',$pagekeywords);
        }
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Display the view
		parent::display($tpl);
	}
}
