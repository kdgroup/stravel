<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');
 
/**
 * Ola class for the Ola Component
 */
class sTravelViewHotelHome extends JViewLegacy
{
	// Overwriting JView display method
	function display($tpl = null) 
	{
       $this->hotellist = apiHelper::getBestDealsHtlByCountry();
       $this->hothtlcities = apiHelper::getContent(array('CONTENT','B2C','HOTEL','HOTELCITY'));
       $this->disclaimer = apiHelper::getContent(array('CONTENT','B2C','HOTEL','HOTELDISCLAIMER'));
       $this->hotellist26 = $this->get('cities');
       
        $doc = JFactory::getDocument();
        $metadata = apiHelper::getMetadata();
        if(!empty($metadata)) {
            $doc->setTitle($metadata->page_title);
            $doc->setDescription($metadata->page_description);       
            $doc->setMetaData('keywords',$metadata->page_keyword);
        }else{
            $page_params = JFactory::getApplication()->getParams();        
            $pagedesc = $page_params->get('menu-meta_description');
            $pagekeywords = $page_params->get('menu-meta_keywords');
            $doc->setDescription($pagedesc);       
            $doc->setMetaData('keywords',$pagekeywords);
        }
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Display the view
		parent::display($tpl);
	}
}
