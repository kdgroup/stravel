<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');
 
class sTravelViewHotelResult extends JViewLegacy
{
	// Overwriting JView display method
	function display($tpl = null) 
	{

	   $checkindate = JRequest::getVar('checkinDate');
       if(empty($checkindate)) {
            $checkindate = date('Y-m-d\T00:00:00',time() + 10*24*60*60); 
       }else{
            $checkindate = $checkindate.'T00:00:00';
       }
       $nights = JRequest::getInt('nights');
       if(empty($nights)) {
        $nights = 1;
       }

       $checkoutdate = date('Y-m-d\T00:00:00',strtotime('+'.$nights.' day',strtotime($checkindate)));
       $city = JRequest::getVar('city');

	   $this->hotellist = apiHelper::searchHotelRate($checkindate,$checkoutdate,$city);
       $area = array(JText::_('COM_STRAVEL_ALL_AREA'));
       $lang = JFactory::getLanguage()->getTag();
       $pricelist = array();
       $hotelnamelist = array();
       foreach($this->hotellist as $item) {
            if(!empty($item->Location)) {
                $area[] = (string) $item->Location;
            }
            if((int)$item->RqtPrice!=0) {
                $pricelist[] = (int) $item->RqtPrice;
                switch($lang) {
                    case 'en-GB':
                        if(!empty($item->HotelName)) {
                            $hotelname = $item->HotelName;    
                        }else{
                            if(!empty($item->HotelName2)) {
                                $hotelname = $item->HotelName2;
                            }else{
                                $hotelname = $item->HotelName1;
                            }    
                        }                                                
                        break;
                    case 'zh-CN':
                        if(!empty($item->HotelName2)) {
                            $hotelname = $item->HotelName2;    
                        }else{
                            if(!empty($item->HotelName1)) {
                                $hotelname = $item->HotelName1;
                            }else{
                                $hotelname = $item->HotelName;
                            }    
                        } 
                        break;
                    case 'zh-TW':
                        if(!empty($item->HotelName1)) {
                            $hotelname = $item->HotelName1;    
                        }else{
                            if(!empty($item->HotelName2)) {
                                $hotelname = $item->HotelName2;
                            }else{
                                $hotelname = $item->HotelName;
                            }    
                        }
                        break;
                    default:
                        if(!empty($item->HotelName2)) {
                            $hotelname = $item->HotelName2;    
                        }else{
                            if(!empty($item->HotelName1)) {
                                $hotelname = $item->HotelName1;
                            }else{
                                $hotelname = $item->HotelName;
                            }    
                        }
                        break;    
                }
                $hotelnamelist[] = array($item->HotelCode, (string) $hotelname);
            } 
       }
       sort($pricelist);
       $this->lowestprice = $pricelist[0];
       $this->arealist = array_unique($area);
       $this->hotelnamelist = $hotelnamelist;
       $this->hothtlcities = apiHelper::getContent(array('CONTENT','B2C','HOTEL','HOTELCITY'));
       $this->disclaimer = apiHelper::getContent(array('CONTENT','B2C','HOTEL','HOTELDISCLAIMER'));
       //$this->top20hotel = apiHelper::getBestDealsHtlByCountry();
       
       //Metadata
        $doc = JFactory::getDocument();
        $metadata = apiHelper::getMetadata();
        if(!empty($metadata)) {
            $doc->setTitle($metadata->page_title);
            $doc->setDescription($metadata->page_description);       
            $doc->setMetaData('keywords',$metadata->page_keyword);
        }else{
            $page_params = JFactory::getApplication()->getParams();        
            $pagedesc = $page_params->get('menu-meta_description');
            $pagekeywords = $page_params->get('menu-meta_keywords');
            $title = $doc->getTitle();
            $cityname = JRequest::getVar('destination');
            if(!empty($cityname)){
                $title = $title .' - '. $cityname;
                $pagedesc = $cityname.JText::_('COM_STRAVEL_SEARCH_HOTEL'). ' - ' .$pagedesc;
                $pagekeywords = $cityname.JText::_('COM_STRAVEL_SEARCH_HOTEL') . ', ' . $pagekeywords;
            }
            $doc->setTitle($title);
            $doc->setDescription($pagedesc);       
            $doc->setMetaData('keywords',$pagekeywords);
        }
       if(JRequest::getVar('code')) {
            $fux = apiHelper::getHotelInfo('31401');
            var_dump($fux); 
       }
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Display the view
		parent::display($tpl);
	}
}
