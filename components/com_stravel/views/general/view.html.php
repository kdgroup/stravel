<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');
 
class sTravelViewGeneral extends JViewLegacy
{
  // Overwriting JView display method
  function display($tpl = null) 
  {
      $page = JRequest::getWord('generalpage','aboutus');
        switch($page) {
            case 'aboutus':
                $this->navigation = apiHelper::getContent(array('CONTENT','B2C','FOOTER','ABOUTUS','NAVIGATION'));
                $this->content = apiHelper::getContent(array('CONTENT','B2C','FOOTER','ABOUTUS','DETAIL'));
                break;
            case 'terms':
                $this->content = apiHelper::getContent(array('CONTENT','B2C','FOOTER','TERMS','DETAIL'));
                $this->navigation = apiHelper::getContent(array('CONTENT','B2C','FOOTER','TERMS','NAVIGATION'));
                break;
            case 'disclaimer':
                $lang = JRequest::getVar('language','zh_tw');
                $this->content = apiHelper::getContent(array('CONTENT','B2C','FOOTER','DISCLAIMER','DETAIL'),null,$lang);
                $this->navigation = apiHelper::getContent(array('CONTENT','B2C','FOOTER','DISCLAIMER','NAVIGATION'),null,$lang);
                break;
            case 'contactus':
                $this->content = apiHelper::getContent(array('CONTENT','B2C','FOOTER','CONTACT','DETAIL'));
                $this->navigation = apiHelper::getContent(array('CONTENT','B2C','FOOTER','CONTACT','NAVIGATION'));
                $this->branches = apiHelper::getContent(array('CONTENT','B2C','FOOTER','CONTACT','BRANCHINFO'));
                break;
            case 'joinus':
                $this->content = apiHelper::getContent(array('CONTENT','B2C','FOOTER','JOINUS','DETAIL'));
                $this->navigation = apiHelper::getContent(array('CONTENT','B2C','FOOTER','JOINUS','NAVIGATION'));
                break;
            case 'sitemap':
                $this->content = apiHelper::getContent(array('CONTENT','B2C','FOOTER','SITEMAP','DETAIL'));
                $this->navigation = apiHelper::getContent(array('CONTENT','B2C','FOOTER','SITEMAP','NAVIGATION'));
                break;
            case 'Travelinsurance':
                $this->content = apiHelper::getContent(array('CONTENT','B2C','FOOTER','TRAVELINSURANCE','DETAIL'));
                $this->navigation = apiHelper::getContent(array('CONTENT','B2C','FOOTER','TRAVELINSURANCE','NAVIGATION'));
                break;
            case 'eurorailway':
                $this->content = apiHelper::getContent(array('CONTENT','B2C','FOOTER','EURORAILWAY','DETAIL'));
                $this->navigation = apiHelper::getContent(array('CONTENT','B2C','FOOTER','EURORAILWAY','NAVIGATION'));
                break;
            case 'japanrailway':
                $this->content = apiHelper::getContent(array('CONTENT','B2C','FOOTER','JAPANRAILWAY','DETAIL'));
                $this->navigation = apiHelper::getContent(array('CONTENT','B2C','FOOTER','JAPANRAILWAY','NAVIGATION'));
                break;
            case 'chinashorttrip':
                $this->content = apiHelper::getContent(array('CONTENT','B2C','CHINASHORTTRIP'));
                $this->navigation = apiHelper::getContent(array('CONTENT','B2C','CHINASHORTTRIP','NAVIGATION'));
                break;
            default:
                $this->content = apiHelper::getContent(array('CONTENT','B2C','FOOTER','ABOUTUS','DETAIL'));
                $this->navigation = apiHelper::getContent(array('CONTENT','B2C','FOOTER','ABOUTUS','NAVIGATION'));
                break;
        }
        
        $doc = JFactory::getDocument();
        $metadata = apiHelper::getMetadata();
        if(!empty($metadata)) {
            $doc->setTitle($metadata->page_title);
            $doc->setDescription($metadata->page_description);       
            $doc->setMetaData('keywords',$metadata->page_keyword);
        }else{
            $page_params = JFactory::getApplication()->getParams();        
            $pagedesc = $page_params->get('menu-meta_description');
            $pagekeywords = $page_params->get('menu-meta_keywords');
            $doc->setDescription($pagedesc);       
            $doc->setMetaData('keywords',$pagekeywords);
        }
        
    // Check for errors.
    if (count($errors = $this->get('Errors'))) 
    {
      JError::raiseError(500, implode('<br />', $errors));
      return false;
    }
    // Display the view
    parent::display($tpl);
  }
}
