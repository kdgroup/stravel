<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');
 
class sTravelViewTourFlyer extends JViewLegacy
{
	// Overwriting JView display method
	function display($tpl = null) 
	{
        $id = JRequest::getVar('id');
        if(empty($id)) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_STRAVEL_PACKAGE_NOT_FOUND'),'error');
            JFactory::getApplication()->redirect('index.php?option=com_stravel&view=tourhome'); 
        }else{
            $package = apiHelper::getPackageById($id);
            if(empty($package)) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_STRAVEL_PACKAGE_NOT_FOUND'),'error');
                JFactory::getApplication()->redirect('index.php?option=com_stravel&view=tourhome');    
            }else{
                $this->package = $package;
                $city = array();
                if(!empty($package->CityList->City)) {
                    foreach($package->CityList->City as $val) {
                        $city[] = $val->CityCode; 
                    }    
                }
                //$this->relatedPackage = apiHelper::searchPackage(null,null,null,$city);
                //$this->otherPackage = apiHelper::getTopPackage(4);
            }
        } 
	    //$this->featuredPackage     = apiHelper::getFeaturedPackage();
        //$this->mergecities = apiHelper::getContent(array('CONTENT','B2C','PACKAGE','P02'));
        $this->packageList         = apiHelper::searchPackage();
        $tmpfp = $tmphct = array();
        foreach($this->packageList as $item) {
            $belongTour = false;
            foreach($item->AdvIdeaList->AdvIdea as $idea) {
                if($idea->Type==12) {
                    $total++;
                    $belongTour = true;    
                }                   
            }
            if($belongTour) {
                foreach($item->AdvIdeaList->AdvIdea as $idea) {
                    if($idea->Type!=12 && $idea->Type==13) {
                        $tmpfp[(int)$idea->Id] = $idea;
                    }
                }
                foreach($item->CityList->City as $city) {
                    $tmphct[(string)$city->CityCode] = $city;
                }    
            }
            
        }

        $this->featuredPackage = $tmpfp;
        $this->hotCities = $tmphct;
        
        //Metadata
        $doc = JFactory::getDocument();
        $metadata = apiHelper::getMetadata();
        if(!empty($metadata)) {
            $doc->setTitle($metadata->page_title);
            $doc->setDescription($metadata->page_description);       
            $doc->setMetaData('keywords',$metadata->page_keyword);
        }else{
            $page_params = JFactory::getApplication()->getParams();        
            $pagedesc = $page_params->get('menu-meta_description');
            $pagekeywords = $page_params->get('menu-meta_keywords');
            $title = $doc->getTitle();
            if(!empty($package->Abstract)){
                $name = $package->Abstract;
                $title = $title .' - '. $name;
                $pagedesc = $name. ' - ' .$pagedesc;
                $pagekeywords = $name. ', ' . $pagekeywords;
            }
            $cruisekeyword = (string) $package->Keyword;
            if(!empty($cruisekeyword)) {
                $pagekeywords = $cruisekeyword .','.$pagekeywords;
            }
            $doc->setTitle($title);
            $doc->setDescription($pagedesc);       
            $doc->setMetaData('keywords',$pagekeywords);
        }
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Display the view
		parent::display($tpl);
	}
}
