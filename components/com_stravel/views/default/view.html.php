<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');
 
/**
 * Default View class for the sTravel Component
 */
class sTravelViewdefault extends JViewLegacy
{
	// Overwriting JView display method
	function display($tpl = null) 
	{
	    $presethelper = new presetHelper;
        $lang = str_replace('-','_',strtolower(JFactory::getLanguage()->getTag()));
        
	    $this->slideshowContent = apiHelper::getContent();
        $this->featuredPackage  = apiHelper::getFeaturedPackage();
        $this->top10package     = apiHelper::searchPackage();
        $airlist = apiHelper::getLowestFareByCountry();
        $this->top10air         = $presethelper->getMoreInfo($airlist);
        $this->top10airmanual   = apiHelper::getContent(array('CONTENT','B2C','FARE','TOP10AIR'));
        
        if(!empty($this->top10airmanual)) {
            if(count($this->top10airmanual)<10) {
                $this->top10airmanual = false;
            }
        }
        
        $this->top10hotel       = apiHelper::getBestDealsHtlByCountry();
        $this->packagebanner    = apiHelper::getContent(array('CONTENT','B2C','HOME','TEXTBANNER'));
        $this->destination      = apiHelper::getB2CCruiseDestination(true);
        $this->cruisevessel     = apiHelper::getB2CCruiseVessel();
        $this->cruisecpn        = apiHelper::getB2CCruiseLine();
        $this->packagehotcities = apiHelper::getHotPackageCities();
        $this->hot1stitem       = apiHelper::getContent(array('CONTENT','B2C','HOME','2ITEMS','1STITEM'));
        $this->hot2nditem       = apiHelper::getContent(array('CONTENT','B2C','HOME','2ITEMS','2NDITEM'));

        $mergecities = apiHelper::getContent(array('CONTENT','B2C','PACKAGE','P02'));
        $i = 1;
        $mergecityhtml = ''; 
        foreach($mergecities as $city) {            
            if($i==1) {$extratxt = '<span class="hottxt">'.JText::_('COM_STRAVEL_HOTCITY_LABEL').'</span>';}else{ $extratxt = ''; }
            if($i>10) {break;} $i++;
            $infos = explode("\n",$city->Content); 
            $mergecityhtml .= '<li class="hotcity" data-cityname="'.$infos[0].'" data-citycode="'.$city->Keyword.'" data-name=""><a href="#" onclick="return false;">'.$infos[1].$extratxt.'</a></li>';
        }
        $this->mergecity = $mergecityhtml;
        
        $doc = JFactory::getDocument();
        $farejs = array();
        $mergeaircities = apiHelper::getContent(array('CONTENT','B2C','FARE','CITY'));
        foreach($mergeaircities as $item) {
            $infos = explode("\n",$item->Content);            
            $farejs[] = '["'.$item->Keyword.'","'.$infos[1].'"]';
        }
        $farejs = '['.implode(',',$farejs).']';
        $hoteljs = array();
        $mergehtlcities = apiHelper::getContent(array('CONTENT','B2C','HOTEL','HOTELCITY'));
        foreach($mergehtlcities as $item) {
            $infos = explode("\n",$item->Content);            
            $hoteljs[] = '["'.$item->Keyword.'","'.$infos[1].'"]';
        }
        $hoteljs = '['.implode(',',$hoteljs).']';
        $doc->addScriptDeclaration("
            var farecities = $farejs;
            var hotelcities = $hoteljs;
        ");
        
        $doc = JFactory::getDocument();
        $metadata = apiHelper::getMetadata();
        if(!empty($metadata)) {
            $doc->setTitle($metadata->page_title);
            $doc->setDescription($metadata->page_description);       
            $doc->setMetaData('keywords',$metadata->page_keyword);
        }else{
            $page_params = JFactory::getApplication()->getParams();        
            $pagedesc = $page_params->get('menu-meta_description');
            $pagekeywords = $page_params->get('menu-meta_keywords');
            $doc->setDescription($pagedesc);       
            $doc->setMetaData('keywords',$pagekeywords);
        }
		
		$this->front_deal = $this->get('FrontDeal');
        $this->latestBlogPosts = $this->get('WpRecentPosts');
		
        //var_dump(($this->hotCities));
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Display the view
		parent::display($tpl);
	}
}
