<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');
 
class sTravelViewUrlManagement extends JViewLegacy
{
	// Overwriting JView display method
	function display($tpl = null) 
	{
	   if (!JFactory::getUser()->authorise('core.manageurl', 'com_stravel')) {
            return JError::raiseWarning(401, JText::_('JERROR_ALERTNOAUTHOR'));
       }
	   if(JRequest::getVar('action')=='save') {
            $this->get('saveUrl');   
       }
	   $layout = JRequest::getCmd('layout','default');
       $this->setLayout($layout);
       $url_id = JRequest::getInt('id',0);
       if($url_id) {
            $url = $this->get('Url');               
       }
       $this->totalUrl = $this->get('TotalUrl');
       $this->dupurl = $this->get('DuplicateUrlCount');       
       $this->duptitle = $this->get('DuplicatePageTitleCount');       
       $this->dupkeyword = $this->get('DuplicatePageKeywordCount');       
       $this->dupdesc = $this->get('DuplicatePageDescCount');       
       $items = $this->get('Items');
       $pagination = $this->get('Pagination');
       
	   // Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
        if(!empty($url)) {
            $this->url = $url;    
        }        
        $this->items = $items;
        $this->pagination = $pagination;
		// Display the view
		parent::display($tpl);
	}
}
