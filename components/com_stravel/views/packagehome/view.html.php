<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');
 
/**
 * Ola class for the Ola Component
 */
class sTravelViewPackageHome extends JViewLegacy
{
	// Overwriting JView display method
	function display($tpl = null) 
	{
	    $departuredate = '';
	    $depdate = JRequest::getVar('departureDate');   
        if(empty($depdate)) {
            $depdate = null;
        }else{
            $departuredate .= $depdate;
            $depdate = $depdate.'T00:00:00';
        }
        $retdate = JRequest::getVar('arrivedDate');   
        if(empty($retdate)) {
            $retdate = null;
        }else{
            if(strlen($departuredate)>0) {
                $departuredate .= ' '.JText::_('COM_STRAVEL_TO').' '.$retdate;
            }else{
                $departuredate .= $retdate;
            }
            $retdate = $retdate.'T00:00:00';
        }
        $this->departuredate = $departuredate;
        $ideaid = JRequest::getVar('ideaid');   
        if(empty($ideaid)) {
            $ideaid = null;
        }
        $city = JRequest::getVar('city');   
        if(empty($city)) {
            $city = null;
        }else{
            $city = explode('-',$city);
        }
        $carrier = JRequest::getVar('carrier');   
        if(empty($carrier)) {
            $carrier = null;
        }else{
            $carrier = explode('-',$carrier);
        }
        $hotel = JRequest::getVar('hotel');   
        if(empty($hotel)) {
            $hotel = null;
        }else{
            $hotel = explode('-',$hotel);
        }
        if(empty($hotel) && empty($depdate) && empty($retdate) && empty($ideaid) && empty($city) && empty($carrier)) {
            $this->packagehome = true;
        }else{
            $this->packagehome = false;
        }
        //var_dump($depdate,$retdate);
	    $this->packageList         = apiHelper::searchPackage($depdate,$retdate,$ideaid,$city,$carrier,$hotel);
	    $this->top10               = apiHelper::searchPackage();
        
        $total = 0;
        foreach($this->packageList as $item) {
            foreach($item->AdvIdeaList->AdvIdea as $idea) {
                if($idea->Id==183) {
                    $total++;
                }    
            }
        }
        if($this->packagehome) {
            $this->totalhomelist = $total;
        }else {
            $this->totalhomelist = count($this->packageList);
        }
        
        $this->disclaimer = apiHelper::getContent(array('CONTENT','B2C','PACKAGE','PACKAGEDISCLAIMER')); 
        $this->mergecities = apiHelper::getContent(array('CONTENT','B2C','PACKAGE','P02')); 
        $this->featuredPackage = apiHelper::getFeaturedPackage();
        $this->hotCities = apiHelper::getHotPackageCities();
        
        $db = JFactory::getDbo();
        $uri = JFactory::getURI()->toString();
        //Metadata
        $doc = JFactory::getDocument();
        $metadata = apiHelper::getMetadata();
        if(!empty($metadata)) {
            $doc->setTitle($metadata->page_title);
            $doc->setDescription($metadata->page_description);       
            $doc->setMetaData('keywords',$metadata->page_keyword);
        }else{
            $page_params = JFactory::getApplication()->getParams();        
            $pagedesc = $page_params->get('menu-meta_description');
            $pagekeywords = $page_params->get('menu-meta_keywords');
            $title = $doc->getTitle();
            $cityname = JRequest::getVar('cityname');
            if(!empty($cityname)){
                $title = $title .' - '. $cityname;
                $pagedesc = $cityname.JText::_('COM_STRAVEL_SEARCH_PACKAGE'). ' - ' .$pagedesc;
                $pagekeywords = $cityname.JText::_('COM_STRAVEL_SEARCH_PACKAGE') . ', ' . $pagekeywords;
            }
            $ideaname = JRequest::getVar('ideaname');
            if(!empty($ideaname)) {
                $title = $title . ' - ' . $ideaname;
                $pagedesc = $ideaname. ' - ' .$pagedesc;
                $pagekeywords = $ideaname.', ' . $pagekeywords;
            }
            $doc->setTitle($title);
            $doc->setDescription($pagedesc);       
            $doc->setMetaData('keywords',$pagekeywords);
        }                 
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Display the view
		parent::display($tpl);
	}
}
