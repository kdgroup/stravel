<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');
 
/**
 * Ola class for the Ola Component
 */
class sTravelViewAirticketHome extends JViewLegacy
{
	// Overwriting JView display method
	function display($tpl = null) 
	{
	   $presethelper = new presetHelper;
        //Get parameter
        $this->hottestcity = apiHelper::getContent(array('CONTENT','B2C','FARE','CITY'));
        $this->disclaimer = apiHelper::getContent(array('CONTENT','B2C','FARE','FLIGHTDISCLAIMER'));
        
        //$topten = apiHelper::getLowestFare2Index();
        //$topten = $presethelper->getMoreInfo($topten);
        //$this->topten = $topten;
        //var_dump($topten);
        
        $this->top10airmanual   = apiHelper::getContent(array('CONTENT','B2C','FARE','TOP10AIR'));
        if(!empty($this->top10airmanual)) {
            if(count($this->top10airmanual)<10) {
                $this->top10airmanual = false;
            }
        }
        
	    $airlist = apiHelper::getLowestFareByCountry();
        $airlist = $presethelper->getMoreInfo($airlist);        
        $this->airlist = $airlist;
        
        $doc = JFactory::getDocument();
        $metadata = apiHelper::getMetadata();
        if(!empty($metadata)) {
            $doc->setTitle($metadata->page_title);
            $doc->setDescription($metadata->page_description);       
            $doc->setMetaData('keywords',$metadata->page_keyword);
        }else{            
            $page_params = JFactory::getApplication()->getParams();        
            $pagedesc = $page_params->get('menu-meta_description');
            $pagekeywords = $page_params->get('menu-meta_keywords');
            $doc->setDescription($pagedesc);       
            $doc->setMetaData('keywords',$pagekeywords);    
        }
           
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Display the view
		parent::display($tpl);
	}
}
