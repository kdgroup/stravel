<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');

class sTravelViewTourHome extends JViewLegacy
{
	// Overwriting JView display method
	function display($tpl = null) 
	{
	    $this->slideshowContent = apiHelper::getContent(array('CONTENT','B2C','TOUR','SLIDESHOW'));
        //$featuredPackage = apiHelper::getFeaturedPackage();
        //$hotCities = apiHelper::getHotPackageCities();
        
        $ideaid = JRequest::getVar('ideaid');   
        $city = JRequest::getVar('city');   
        if(empty($city)) {
            $city = null;
        }else{
            $city = explode('-',$city);
        }

        //var_dump($depdate,$retdate);
	    $this->packageList         = apiHelper::searchPackage();
        
        $tmpfp = $tmphct = array();
        $promotionList = $normalList = array();
        foreach($this->packageList as $item) {
            $belongTour = false;
            $isPromotion = false;
            foreach($item->AdvIdeaList->AdvIdea as $idea) {
                if($idea->Type==12) {
                    $belongTour = true;    
                }
                if($idea->Id==208) {
                    $isPromotion = true;    
                }                   
            }
            if($isPromotion) {
                $promotionList[] = $item; 
            }else{
                $normalList[] = $item;
            }
            if($belongTour) {
                foreach($item->AdvIdeaList->AdvIdea as $idea) {
                    if($idea->Type!=12 && $idea->Type==13) {
                        $tmpfp[(int)$idea->Id] = $idea;
                    }
                }
                foreach($item->CityList->City as $city) {
                    $tmphct[(string)$city->CityCode] = $city;
                }    
            }
            
        }
        
        /*
        foreach($promotionList as $i=>$v) {
            foreach($promotionList as $j=>$k) {
                $v_created = strtotime($v->CreateOn);
                $k_created = strtotime($k->CreateOn);
                if($v_created<$k_created) {
                    $tg = $promotionList[$j];
                    $promotionList[$j]=$promotionList[$i];
                    $promotionList[$i]=$tg;
                }
            }    
        }
        */
        foreach($normalList as $i=>$v) {
            foreach($normalList as $j=>$k) {
                $v_created = strtotime($v->CreateOn);
                $k_created = strtotime($k->CreateOn);
                if($v_created>$k_created) {
                    $tg = $normalList[$j];
                    $normalList[$j]=$normalList[$i];
                    $normalList[$i]=$tg;
                }
            }    
        }
        
        $packageList = array_merge($promotionList,$normalList);
        
        $this->packageList = $packageList;

        $this->featuredPackage = $tmpfp;
        $this->hotCities = $tmphct;
        
        $db = JFactory::getDbo();
        $uri = JFactory::getURI()->toString();
        //Metadata
        $doc = JFactory::getDocument();
        $metadata = apiHelper::getMetadata();
        if(!empty($metadata)) {
            $doc->setTitle($metadata->page_title);
            $doc->setDescription($metadata->page_description);       
            $doc->setMetaData('keywords',$metadata->page_keyword);
        }else{
            $page_params = JFactory::getApplication()->getParams();        
            $pagedesc = $page_params->get('menu-meta_description');
            $pagekeywords = $page_params->get('menu-meta_keywords');
            $title = $doc->getTitle();
            $cityname = JRequest::getVar('cityname');
            if(!empty($cityname)){
                $title = $title .' - '. $cityname;
                $pagedesc = $cityname.JText::_('COM_STRAVEL_SEARCH_PACKAGE'). ' - ' .$pagedesc;
                $pagekeywords = $cityname.JText::_('COM_STRAVEL_SEARCH_PACKAGE') . ', ' . $pagekeywords;
            }
            $ideaname = JRequest::getVar('ideaname');
            if(!empty($ideaname)) {
                $title = $title . ' - ' . $ideaname;
                $pagedesc = $ideaname. ' - ' .$pagedesc;
                $pagekeywords = $ideaname.', ' . $pagekeywords;
            }
            $doc->setTitle($title);
            $doc->setDescription($pagedesc);       
            $doc->setMetaData('keywords',$pagekeywords);
        }                 
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Display the view
		parent::display($tpl);
	}
}
