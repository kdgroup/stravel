<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla modelitem library
jimport('joomla.application.component.modellist');
 
/**
 * sTravel Model
 */
class sTravelModelUrlManagement extends JModelList
{ 
	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'urlmanagement', $prefix = 'sTravelTable', $config = array()) 
	{
		return JTable::getInstance($type, $prefix, $config);
	}
    
    protected function getListQuery()
    {
            // Create a new query object.           
            $db = JFactory::getDBO();
            $query = $db->getQuery(true);
            // Select some fields
            $query->select('*');
            // From the tablename
            $query->from('#__stravel_urls');
            //where
            $where = $this->buildWhere();
            if(!empty($where)) {
                $query->where($where);
            }
            $orderby = $this->buildOrderBy();
            if(!empty($orderby)) {
                $query->order($orderby);
            }
            return $query;
    }
    
    public function buildWhere() {
        $return = false;
        if(JRequest::getVar('filterurl')==1) {
            $return = "url_params IN (SELECT url_params FROM #__stravel_urls GROUP BY url_params HAVING count(*)>1)";
        }
        if(JRequest::getVar('filtertitle')==1) {
            $return = "page_title IN (SELECT page_title FROM #__stravel_urls GROUP BY page_title HAVING count(*)>1)";
        }
        if(JRequest::getVar('filterkeyword')==1) {
            $return = "page_keyword IN (SELECT page_keyword FROM #__stravel_urls GROUP BY page_keyword HAVING count(*)>1)";
        }
        if(JRequest::getVar('filterdesc')==1) {
            $return = "page_description IN (SELECT page_description FROM #__stravel_urls GROUP BY page_description HAVING count(*)>1)";
        }
        return $return;
    }
    
    public function buildOrderBy() {
        $return = false;
        if(JRequest::getVar('filterurl')==1) {
            $return = "url_params";
        }
        if(JRequest::getVar('filtertitle')==1) {
            $return = "page_title";
        }
        if(JRequest::getVar('filterkeyword')==1) {
            $return = "page_keyword";
        }
        if(JRequest::getVar('filterdesc')==1) {
            $return = "page_description";
        }
        return $return;
    }
    
    public function getTotalUrl() {
        $db = JFactory::getDBO();
        $query = "SELECT * FROM #__stravel_urls";
        $items = $db->setQuery($query)->loadObjectList();
        return count($items);
    }
    
    public function getDuplicateUrlCount() {
        $db = JFactory::getDBO();
        $query = "SELECT *,count(*) as c FROM #__stravel_urls GROUP BY url_params HAVING c>1";
        $items = $db->setQuery($query)->loadObjectList();
        $count = 0;
        foreach($items as $item) {
            $count += $item->c;    
        }
        return $count;    
    }
    
    public function getDuplicatePageTitleCount() {
        $db = JFactory::getDBO();
        $query = "SELECT *,count(*) as c FROM #__stravel_urls GROUP BY page_title HAVING c>1";
        $items = $db->setQuery($query)->loadObjectList();
        $count = 0;
        foreach($items as $item) {
            $count += $item->c;    
        }
        return $count;    
    }
    
    public function getDuplicatePageKeywordCount() {
        $db = JFactory::getDBO();
        $query = "SELECT *,count(*) as c FROM #__stravel_urls GROUP BY page_keyword HAVING c>1";
        $items = $db->setQuery($query)->loadObjectList();
        $count = 0;
        foreach($items as $item) {
            $count += $item->c;    
        }
        return $count;    
    }
    
    public function getDuplicatePageDescCount() {
        $db = JFactory::getDBO();
        $query = "SELECT *,count(*) as c FROM #__stravel_urls GROUP BY page_description HAVING c>1";
        $items = $db->setQuery($query)->loadObjectList();
        $count = 0;
        foreach($items as $item) {
            $count += $item->c;    
        }
        return $count;    
    }
    
    public function getsaveUrl() {
        $id = JRequest::getVar('id');
        
        if(empty($id)) return false;
        
        $page_title = JRequest::getVar('pagetitle');
        $page_keyword = JRequest::getVar('pagekeyword');
        $page_desc = JRequest::getVar('pagedesc');
        
        $obj = array(
            'id' => $id,
            'page_title' => $page_title,
            'page_keyword' => $page_keyword,
            'page_description' => $page_desc,
        );        
        
        $tbl = $this->getTable();
        $result = $tbl->save($obj);
        if($result) {
            JFactory::getApplication()->enqueueMessage('Successfully saved!');
        }else{
            JFactory::getApplication()->enqueueMessage('Save failed! Please try again!','error');
        }
    }
    public function getUrl() {
        $id = JRequest::getInt('id');
        if(empty($id)) return false;
        $db = JFactory::getDBO();
        $query = "SELECT * FROM #__stravel_urls WHERE id=".$id;
        return $db->setQuery($query)->loadObject();
    } 
}
