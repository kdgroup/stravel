<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla modelitem library
jimport('joomla.application.component.modelitem');
 
/**
 * sTravel Model
 */
class sTravelModeldefault extends JModelItem
{
	/**
	 * @var object item
	 */
	protected $item;
 
	/**
	 * Method to auto-populate the model state.
	 *
	 * This method should only be called once per instantiation and is designed
	 * to be called on the first call to the getState() method unless the model
	 * configuration flag to ignore the request is set.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState() 
	{
		$app = JFactory::getApplication();
		// Get the message id
		$id = JRequest::getInt('id');
		$this->setState('message.id', $id);
 
		// Load the parameters.
		$params = $app->getParams();
		$this->setState('params', $params);
		parent::populateState();
	}
 
	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'default', $prefix = 'sTravelTable', $config = array()) 
	{
		return JTable::getInstance($type, $prefix, $config);
	}

    /**
     * Load an JSON string into the registry into the given namespace [or default if a namespace is not given]
     *
     * @param    string    JSON formatted string to load into the registry
     * @return    boolean True on success
     * @since    1.5
     * @deprecated 1.6 - Oct 25, 2010
     */
    public function loadJSON($data)
    {
        return $this->loadString($data, 'JSON');
    }
 
	/**
	 * Get the message
	 * @return object The message to be displayed to the user
	 */
	public function getItem() 
	{

		if (!isset($this->item)) 
		{
			$this->item = 'null';
		}
		return $this->item;
	}
	
	/**
	* Get 3 frontpage deal
	**/
	public function getFrontDeal() {
		require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."DatetimeWrapper.class.php");
		require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
		$db = JFactory::getDbo();
		$query = "SELECT * FROM `#__enmasse_deal` WHERE 
														`frontpage` IN (1,2,3) AND
														`published`=1 AND
														status != '".EnmasseHelper::$DEAL_STATUS_LIST['Pending']."' AND 
														status != '".EnmasseHelper::$DEAL_STATUS_LIST['Voided']."' AND  
														end_at >= '". DatetimeWrapper::getDatetimeOfNow() . "' AND 
														start_at <='".DatetimeWrapper::getDatetimeOfNow()."'
												ORDER BY `frontpage` ASC 
												LIMIT 3";
		$deals = $db->setQuery($query)->loadObjectList();
		return $deals;
	}
    
    /** 
     * Get 3 latest posts from WP blog
     **/
    public function getWpRecentPosts() {
        $cache = JFactory::getCache()->getInstance();
        $cache->setCaching(1);
        $recent_posts = $cache->get('wprecentposts','travelnews');
        if(empty($recent_posts)) {
            // Include the wp-load'er
            include(JPATH_ROOT.'/travelnews/wp-load.php');
            
            // Returns posts as arrays instead of get_posts' objects
            $recent_posts = wp_get_recent_posts(array(
            	'numberposts' => 3,
    			'post_status' => 'publish'
            ));
            
            foreach($recent_posts as $k=>$v) {
                if(has_post_thumbnail( $v['ID'] )) {
                    $recent_posts[$k]['Image'] = wp_get_attachment_image_src( get_post_thumbnail_id( $v['ID'] ), 'single-post-thumbnail' );    
                }else{
                    $recent_posts[$k]['Image'] = array('http://placehold.it/250x170?text=No+image');
                }
                $recent_posts[$k]['permalink'] = get_permalink($v['ID']); 
            }
            
            $cache->store($recent_posts,'wprecentposts','travelnews');    
        }        
        
        return $recent_posts;
    }
}
