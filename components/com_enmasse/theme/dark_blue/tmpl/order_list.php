<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse".DS."helpers". DS ."EnmasseHelper.class.php");
$theme =  EnmasseHelper::getThemeFromSetting();
$enableRTL =  EnmasseHelper::getRTLFromSetting();
//if($enableRTL){
//	JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen_rtl.css');
//}
//JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen.css');
$oMenu = JFactory::getApplication()->getMenu();
$oItem = $oMenu->getItems('link','index.php?option=com_enmasse&view=dealtoday',true);
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <h3><?php echo JText::_("LIST_YOUR_ORDER")?></h3>
	<div class="em_innerBlock em_orderPage">
            <div class="em_orderList">
                <table width="100%">
                    <tr class="oderTitle">
                        <th><?php echo JText::_('ORDER_ID');?></th>
                        <th><?php echo JText::_('ORDER_DEAL'); ?>
                        &nbsp | &nbsp <?php echo JText::_('ORDER_QTY'); ?></th>
                        <th><?php echo JText::_('ORDER_TOTAL');?></th>
                        <th><?php echo JText::_('ORDER_DATE');?></th>
                        <th><?php echo JText::_('ORDER_DELIVERY');?></th>
                        <th><?php echo JText::_('ORDER_METHOD');?></th>
                        <th><?php echo JText::_('ORDER_STATUS');?></th>
                        <th style="text-align: center;"><?php echo JText::_('ORDER_ACTION');?></th>
                    </tr>
                    
                    <?php $count = 0;?>
                    <?php foreach($this->orderList as $orderRow):?>
                    <tr  <?php if($count % 2 == 0) echo "class=\"highlight\""?> style="min-height: 72px;">
                                <td><?php echo $orderRow->display_id?></td>
                                <td>
                                    <table width="100%">
                            <?php foreach ($orderRow->orderItem as $i => $orderItem) { ?>
                                    <tr style="background: none;">
                                        <td><?php echo $orderItem->description ?></td>
                                        <td width="30"><?php echo $orderItem->qty ?></td>
                                    </tr>

                            <?php } ?>
                                    </table>
                                </td>
                                <td><?php echo EnmasseHelper::displayCurrency($orderRow->total_buyer_paid);?></td>
                                <td><?php echo DatetimeWrapper::getDisplayDatetime($orderRow->created_at);?></td>
                                <td><?php echo DatetimeWrapper::getDisplayDatetime($orderRow->updated_at); ?></td>
                                <td><?php echo $orderRow->payment_method; ?></td>
                                <td><p style="min-height: 28px;padding-top: 14px;"><?php echo JTEXT::_('ORDER_'.strtoupper($orderRow->status));?></p></td>
                                <td style="text-align: center">
                                <?php if($orderRow->status == 'Delivered'){ ?>
                                    <div style="display: inline-block;" class="actionQuotation">
                                        <a href="#" onclick='reSendCoupon(<?php echo $orderRow->id; ?>)'><div class="actionOrder downReceipt"><p>Download</p><p>Receipt</p></div></a>
                                        <!--<div class="actionOrder downReceipt"><a href="#"><p>Download</p><p>Quotation</p></a></div>-->
                                    </div>
                                <?php } ?>
                                </td>

                            </tr>
                            <?php $count++?>
                        <?php endforeach;?>
                    </table>
            </div><!--em_orderList-->
    </div><!--em_innerBlock-->
	
	<script type="text/javascript">
        function reSendCoupon(orderId){
            jQuery.ajax({
                url:"index.php?option=com_enmasse&controller=mail&task=reSendCoupon",
                type: 'POST',
                dataType: 'html',
                data: {
                    'orderId': orderId
                },
                success:function(rs){
                    if(rs == '1'){
                        alert('Resend email successfull.');
                    }
                    else if(rs == '0') {
                        alert('You just resend email a moment ago.');
                    }
                    else {
                        alert("You don't have permission.Please contact with admin to ask for help.");
                    }
                }
            }); 
        }
        
    </script>
