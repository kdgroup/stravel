<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once(JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
  	
$theme =  EnmasseHelper::getThemeFromSetting();
//$enableRTL =  EnmasseHelper::getRTLFromSetting();
//if($enableRTL){
//	JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen_rtl.css');
//}
//JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen.css');

$rows = $this->dealList;

$option = 'com_enmasse';
$filter = $this->filter;
$emptyJOpt = JHTML::_('select.option', '', JText::_('') );

// create list status for combobox
$statusJOptList = array();
array_push($statusJOptList, $emptyJOpt);
foreach ($this->statusList as $key=>$name)
{
	$var = JHTML::_('select.option', $key, JText::_('DEAL_'.str_replace(' ','_',$name)) );
	array_push($statusJOptList, $var);
}

$publishedJOptList = array();
array_push($publishedJOptList, $emptyJOpt);
array_push($publishedJOptList, JHTML::_('select.option', 1, JText::_('PUBLISHED') ));
array_push($publishedJOptList, JHTML::_('select.option', 0, JText::_('NOT_PUBLISHED') ));

// create list merchant for combobox
$merchantJOptList = array();
array_push($merchantJOptList, $emptyJOpt);
foreach ($this->merchantList as $item)
{
	$var = JHTML::_('select.option', $item->id, JText::_($item->name) );
	array_push($merchantJOptList, $var);
}

$itemId = JRequest::getVar('Itemid');

?>
<div class="em_wrapTit">	
    <h3><?php echo JText::_("SALE_REPORT");?></h3>	
</div>
<script type="text/javascript">
        jQuery(document).ready(function($){
                jQuery("#btnOk").click(function(){
                        var filterName = jQuery("#filterName").val();
                        var filterFromDate = jQuery("#filterFromDate").val();
                        var filterToDate = jQuery("#filterToDate").val();
                        //var isSubmit = false;

                        if(filterFromDate == "" && filterToDate == ""){
                                //isSubmit = true;
                        } else if(filterFromDate > filterToDate){
                                jQuery("#messageErrors").html("<font color='#55CCEE'><b style='padding-left:10px;'>The To Date should be greater than or equal to From Date</b></font>");
                                jQuery("#filterFromDate").val("");
                                jQuery("#filterToDate").val("");
                                jQuery(".adminlist").hide();
                                return false;
                        } else if((filterFromDate !="" & filterToDate == "") || (filterFromDate =="" & filterToDate != "")){
                                jQuery("#messageErrors").html("<font color='#55CCEE'><b style='padding-left:10px;'>The To Date or From Date should not be empty</b></font>");
                                jQuery("#filterFromDate").val("");
                                jQuery("#filterToDate").val("");
                                jQuery(".adminlist").hide();
                                return false;
                        } else{
                                //isSubmit = true;
                        }
                });
        });	
</script>

<div id="messageErrors"></div>
<style type="text/css">
@media print {
        .header, .footer_full, .noBorder {display:none !important;}
        .maincol_full {border:0; border-radius:0;}
}
</style>	
<script type="text/javascript">
        function printContent(id){
                document.getElementById('button').style.display = 'none';
                window.print();
                document.getElementById('button').style.display = 'inline';
        }
</script>
<?php if(JRequest::getVar('editor')==true): ?>
<script type="text/javascript">
        function initCouponEditor(){
                var cont = $('couponInfo');
                if(!cont) return;

                //initialize
                var mdTop = 0;
                var mdLeft = 0;
                var mdWidth = 0;
                var mdHeight = 0;
                var curEl = null;
                //create resize icon
                var divResize = new Element('div', {
                        'class': 'divResize',
                        'styles': {
                                'position': 'absolute',
                                'bottom': 0,
                                'right': 0,
                                'width': 10,
                                'height': 10,
                                'cursor': 'se-resize'
                        }
                });
                //inject resize icon
                var els = cont.getChildren();		
                els.each(function(el, index){
                        var clone = divResize.clone();
                        clone.inject(el);
                        clone.addEvents({
                                'mouseenter': function(){
                                },
                                'mouseleave': function(){
                                },
                                'mousedown': function(e){
                                        mdTop = e.client.y;
                                        mdLeft = e.client.x;
                                        mdWidth = this.getParent().getSize().x;
                                        mdHeight = this.getParent().getSize().y;
                                        this.getParent().resizing = true;
                                        curEl = this.getParent();
                                        curEl.setStyle('z-index', 1000);
                                        curEl.removeEvents();
                                }				
                        });
                        el.setStyles({
                                'cursor': 'move',
                                'z-index': 999
                        });
                        el.makeDraggable();
                });
                cont.addEvents({
                        'mousemove': function(e){
                                if(curEl && curEl.resizing){
                                        var size = curEl.getSize();
                                        curEl.setStyles({
                                                'width': mdWidth + e.client.x - mdLeft,
                                                'height': mdHeight + e.client.y - mdTop
                                        });
                                }
                        },
                        'mouseup': function(e){
                                if(curEl){
                                        var size = curEl.getSize();
                                        curEl.setStyles({
                                                'width': mdWidth + e.client.x - mdLeft,
                                                'height': mdHeight + e.client.y - mdTop
                                        });
                                        curEl.resizing = false;
                                        curEl.removeEvents();
                                        curEl.makeDraggable();
                                        curEl.setStyle('z-index', 999);
                                        curEl = null;
                                }
                        }
                });
        }
        window.addEvent('domready', function(e){
                initCouponEditor();
        });
</script>
<?php endif; ?>

<div class="em_innerBlock em_saleReportPage">
		<form id="searchForm" method="post" action="index.php" class="em_wrapSaleInfo">
                    <div class="em_saleInfo row-fluid">
                        <ul>
                            <li class="span4">
                                <label for="txtCode"><?php echo JText::_('DEAL_CODE');?>:</label>
                                                        <input type="text" name="filter[code]" value="<?php echo $filter['code']; ?>" />
                            </li>
                            <li class="span4">
                                <label for="filterName"><?php echo JText::_( 'DEAL_SEARCH_NAME' ); ?>:</label>
                                <input type="text" name="filter[name]" id="filterName" value="<?php echo $filter['name']; ?>" />
                            </li>
                            <li class="span4">
                                <label for="filtermerchant_id"><?php echo JText::_('DEAL_MERCHANT');?>:</label>
                                <?php echo JHTML::_('select.genericList', $merchantJOptList, 'filter[merchant_id]', null , 'value', 'text', $filter['merchant_id']);?>
                            </li>
                        </ul>
                        <ul>                    
                            <li class="span4">
                                <label for="filterFromDate"><?php echo JText::_( 'FROM_DATE' ); ?>:</label>
                                <div class="input-append">
                                    <td><?php echo JHTML::_('calendar', '', "filter[fromdate]", "filterFromDate" , '%Y-%m-%d', 'readonly');?>
                                </div>
                            </li>
                            <li class="span4">
                                <label for="filterToDate"><?php echo JText::_( 'TO_DATE' ); ?>:</label>
                                <div class="input-append">
                                    <?php echo JHTML::_('calendar', '', "filter[todate]" , "filterToDate", '%Y-%m-%d', 'readonly');?>
                                </div>
                            </li>
                            <li class="span4">
                                <p class="em_listBtn">
                                        <input class="em_btn" type="submit" name="ok" id="btnOk" value="<?php echo JTEXT::_('SALE_SEARCH_BUTTON');?>" /> 
                                        <input class="em_btn" type="button" value="<?php echo JTEXT::_('SALE_RESET_BUTTON');?>" onClick="location.href='<?php echo JRoute::_('index.php?option=com_enmasse&controller=salereports&task=dealReport')?>'" />
                                </p><!--em_listBtn-->
                            </li>
                        </ul>
                    </div><!--em_saleInfo-->
                    <p class="em_listDoc">
				<?php if(count($rows) >0) 
                                    {					
				 ?>
					<input type="image" src="components/com_enmasse/theme/dark_blue/images/print.png" id="button" name="button" style="border:0" value="<?php echo JTEXT::_('Print Sale');?>" onClick="printContent('content')">
					<?php $filter = JRequest::getVar('filter'); ?>
					<b style=""><a href="index.php?option=com_enmasse&controller=salereports&task=createPdf&name=<?php echo $filter['name']; ?>&merchant_id=<?php echo $filter['merchant_id']; ?>&fromdate=<?php echo $filter['fromdate']; ?>&todate=<?php echo $filter['todate']; ?>&code=<?php echo $filter['code']; ?>" target="_blank">
					<img src="components/com_enmasse/theme/dark_blue/images/pdf.png"></a></b></td>
				<?php 
                                    } 
				?>
                    </p><!--em_listDoc-->
                    <input type="hidden" name="option" value="com_enmasse" />
                    <input type="hidden" name="controller" value="salereports" />
                    <input type="hidden" name="task" value="dealReport" />
                    <input type="hidden" name="Itemid" value="<?php echo $itemId;?>" />
            </form><!--em_wrapSaleInfo-->
	
            <form action="index.php" method="post" name="adminForm" class="adminForm"><br />
		<table class="em_tbGeneral em_tbSale" border="0" cellpadding="0" cellspacing="0">
                    <tr> 	 	 	 	 	 	
                        <th>#</th>
                        <th><?php echo JText::_( 'DEAL_CODE' ); ?></th>
                        <th><?php echo JText::_( 'DEAL_SEARCH_NAME' ); ?></th>
                        <th><?php echo JText::_( 'DEAL_MERCHANT' ); ?></th>
                        <th><?php echo JText::_( 'DEAL_SOLD' ); ?></th>
                        <th><?php echo JText::_( 'Unit Price' ); ?></th>
                        <th><?php echo JText::_( 'TOTAL_SALES' ); ?></th>
                    </tr>
                    <?php 
                        $total_amount = 0;
			for ($i=0; $i < $n=count( $rows ); $i++)
			{
                            $k = $i % 2;

                            $row = &$rows[$i];
                            $merchant_name 	= JModelLegacy::getInstance('merchant','enmasseModel')->retrieveName($row->merchant_id);
                            $total_sales = $row->price * $row->cur_sold_qty;
                            if($row->use_dynamic == 1)
                                    {
                                            $max_min = EnmasseHelper::getMinMaxDynamicAtt($row->deal_id);
                                            $total_sales = EnmasseHelper::getTotalSaleDynamicAtt($row->deal_id);
                                    }
                    ?>
	
                            <tr class="<?php echo "row$k"; ?>">
                                    <td align="center" name="number"><?php echo $i+1; ?></td>
                                    <td align="center" name="id"><?php echo $row->deal_code; ?></td>
                                    <td align="left" name="name"><?php echo $row->name; ?></td>
                                    <td align="center"><?php echo $merchant_name; ?></td>
                                    <td align="center" name="max_qty"><?php echo $row->cur_sold_qty; ?></td>
                                    <?php if($row->use_dynamic == 1) {?>
                                        <td align="center"><?php echo $this->currency_prefix.$max_min->min_price." - ".$max_min->max_price; ?></td>
                                        <td align="center" ><?php echo $this->currency_prefix.$total_sales; ?></td>
                                    <?php } else { ?>
                                        <td align="center"><?php echo $this->currency_prefix.$row->price; ?></td>
                                        <td align="center" ><?php echo $this->currency_prefix.$total_sales; ?></td>
                                    <?php } ?>
                            </tr>
                    <?php 
                            $total_amount += $total_sales;				
                        }
                    ?>
                    <tr>
                            <td align="right" colspan="6" style="padding-right: 40px; border-bottom: 1px solid #FFFFFF !important; border-left: 1px solid #FFFFFF !important;text-align:right"><b><?php echo JText::_( 'COMMISSION_TOTAL' ); ?></b></td>
                            <td align="center" ><?php echo $this->currency_prefix.$total_amount; ?></td>
                    </tr>
		</table>
		<input type="hidden" name="option" value="com_enmasse" /> 
		<input type="hidden" name="controller" value="salereports" /> 
		<input type="hidden" name="task" value="dealReport" /> 
		<input type="hidden" name="Itemid" value="<?php echo $itemId;?>" />
	</form>
</div>