<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//check which category contain this deal

$main_image = JModelLegacy::getInstance('setting', 'enmasseModel')->getMainImage();
$tempImage = $main_image;
if(isset($is_listing) && $is_listing == 1){
   // $tempImage = $main_image;
    if(isset($catId)){
        // setting backgound for category
        $main_image = JModelLegacy::getInstance('category', 'enmasseModel')->getBackGroundImage($catId);
    }else if(isset($dealID)){
         //setting background for deal detail
            $listCat = JModelLegacy::getInstance('dealcategory', 'enmasseModel')->getCategoryByDealId($dealID);
            $main_image = JModelLegacy::getInstance('category', 'enmasseModel')->getBackGroundImage($listCat[0]);
    }
}

if(empty($main_image)){
    $main_image = $tempImage;
}

if(!empty($main_image)){
    $main_image = JUri::base().$main_image;
?>
<style>
    body {
         background: url("<?php echo $main_image ?>") repeat fixed 50% 0 #444444;
    }
</style>
<?php } ?>
