<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 
require_once(JPATH_SITE . DS."components". DS ."com_enmasse".DS."theme".DS."dark_blue".DS."tmpl".DS."background_image.php");
require_once(JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
 	
$theme =  EnmasseHelper::getThemeFromSetting();
$enableRTL =  EnmasseHelper::getRTLFromSetting();
//if($enableRTL){
//	JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen_rtl.css');
//}
//JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen.css');

$rows = $this->dealList;

$option = 'com_enmasse';
$filter = $this->filter;
$emptyJOpt = JHTML::_('select.option', '', JText::_('') );

// create list status for combobox
$statusJOptList = array();
array_push($statusJOptList, $emptyJOpt);
foreach ($this->statusList as $key=>$name)
{
	$var = JHTML::_('select.option', $key, JText::_('DEAL_'.str_replace(' ','_',$name)) );
	array_push($statusJOptList, $var);
}

$publishedJOptList = array();
array_push($publishedJOptList, $emptyJOpt);
array_push($publishedJOptList, JHTML::_('select.option', 1, JText::_('PUBLISHED') ));
array_push($publishedJOptList, JHTML::_('select.option', 0, JText::_('NOT_PUBLISHED') ));
	
?>
<!--        <div class="em_wrapTit">
            <h3><?php echo JText::_("MY_SALE");?></h3>
        </div>-->
	<?php
	//load submenu for sale person
	$oldTpl = $this->setLayout('sales_person_sub_menu');
	echo $this->loadTemplate();
	$this->setLayout($oldTpl); 
	?>
	
<div class="em_innerBlock em_mySalePage">
        <form id="searchForm" method="post" class="em_wrapSaleInfo" action="<?php JRoute::_('index.php?option=com_enmasse$controller=salesPerson&task=dealShow')?>">
            <div class="row-fluid">
                <div class="span4">
                    <span class="saleLabel"><?php echo JText::_('DEAL_CODE');?>:</span>
                    <input type="text" name="filter[code]" value="<?php echo $filter['code']; ?>" />
                </div>
                <div class="span4">
                    <span class="saleLabel"><?php echo JText::_( 'DEAL_SEARCH_NAME' ); ?>:</span>
                    <input type="text" name="filter[name]" value="<?php echo $filter['name']; ?>" />
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    <span class="saleLabel"><?php echo JText::_( 'PUBLISHED' ); ?>:</span>
                    <?php echo JHTML::_('select.genericList', $publishedJOptList, 'filter[published]', null , 'value', 'text', $filter['published']);?>
                </div>
                <div class="span4">
                    <span class="saleLabel"><?php echo JText::_( 'STATUS' ); ?>:</span>
                    <?php echo JHTML::_('select.genericList', $statusJOptList, 'filter[status]', null , 'value', 'text', $filter['status']);?>
                </div>
                <div class="span4 ctrSubmit">
                    <input type="submit" class="em_btn" value="<?php echo JTEXT::_('SALE_SEARCH_BUTTON');?>" />
                    <input type="button" class="em_btn" value="<?php echo JTEXT::_('SALE_RESET_BUTTON');?>" onClick="location.href='<?php echo JRoute::_('index.php?option=com_enmasse&controller=salesPerson&task=dealShow')?>'" />
                </div>
            </div>
	</form>
	<form action="index.php" method="post" name="adminForm" class="adminForm"><br />
		<table class="em_tbGeneral em_tbMySales" border="0" cellpadding="0" cellspacing="0">
        	<tr> 	 	 	 	 	 	 	 	 	
            	<th><?php echo '#' ; ?></th>
                <th><?php echo JText::_( 'DEAL_CODE' ); ?></th>
                <th><?php echo JText::_( 'DEAL_SEARCH_NAME' ); ?></th>
                <th><?php echo JText::_( 'MIN_QTY' ); ?></th>
                <th><?php echo JText::_( 'DEAL_BOUGHT' ); ?></th>
                <th><?php echo JText::_( 'PUBLISHED' ); ?></th>
                <th><?php echo JText::_( 'STATUS' ); ?></th>
                <th><?php echo JText::_( 'DEAL_LIST_OPTION_END_DATE' ); ?></th>
                <th><?php echo JText::_( 'CREATED_AT' ); ?></th>
                <th><?php echo JText::_( 'UPDATED_AT' ); ?></th>
            </tr>
			<?php
			for ($i=0; $i < $n=count( $rows ); $i++)
			{
				$k = $i % 2;
				
				$row = &$rows[$i];
				//$checked = JHTML::_('grid.id', $i, $row->id );
				$published = JHTML::_('grid.published', $row, $i );
				$link =  JRoute::_('index.php?option=' .$option .'&controller=salesPerson'.'&task=dealEdit&cid[]='. $row->id) ;
			?>
		
			<tr class="<?php echo "row$k"; ?>">
				<td name="number"><?php echo $i+1; ?></td>
				<td name="id"><a href="<?php echo $link; ?>"><?php echo $row->deal_code; ?></a></td>
				<td name="name"><a href="<?php echo $link; ?>"><?php echo $row->name; ?></a></td>
				<td name="min_needed_qty"><?php echo $row->min_needed_qty; ?></td>
				<td name="max_qty"><?php echo $row->cur_sold_qty; ?></td>
				<td align="center" name="published"><?php if($row->published){ echo "Published"; }else{ echo "Not Published";}; ?></td>
				<td name="status" align="center" ><?php echo JText::_('DEAL_'.str_replace(' ','_',$row->status)) ; ?></td>
				<td name="end_at"><?php echo DatetimeWrapper::getDisplayDatetime($row->end_at); ?></td>
				<td name="date"><?php echo DatetimeWrapper::getDisplayDatetime($row->created_at); ?></td>
				<td name="date"><?php echo DatetimeWrapper::getDisplayDatetime($row->updated_at); ?></td>
			</tr>
			<?php
			}
			?>
		</table>
		<input type="hidden" name="option" value="com_enmasse" /> 
		<input type="hidden" name="controller" value="salesPerson" /> 
		<input type="hidden" name="task" value="dealShow" />
	</form>
</div>	
