<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 


require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."DatetimeWrapper.class.php");
$catId = $this->categoryId;
$is_listing = '1';
require_once(JPATH_SITE . DS."components". DS ."com_enmasse".DS."theme".DS."dark_blue".DS."tmpl".DS."background_image.php");
$theme =  EnmasseHelper::getThemeFromSetting();
//--------- add stylesheet and javascript
//$enableRTL =  EnmasseHelper::getRTLFromSetting();
//if($enableRTL){
//	JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen_rtl.css');
//}
//JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen.css');

//set default timezone
DatetimeWrapper::setTimezone(DatetimeWrapper::getTimezone());

$oDefault = new JObject();
$oDefault->name = '所有';
$oDefault->id   = '';
//add an empty select option for location and category list
array_unshift($this->locationList, $oDefault);
array_unshift($this->categoryList, $oDefault);
array_unshift($this->cityList, $oDefault);

$dealList = $this->dealList;


$nItemId = JFactory::getApplication()->getMenu()->getItems('link','index.php?option=com_enmasse&view=deallisting',true)->id;
$templateUrl = JURI::base()."templates/stravel/";
$catId = JRequest::getVar('categoryId', '');
$cityId = JRequest::getVar('cityId', '');
$itemId = JRequest::getVar('itemId', '');
$currentUrl = JURI::current();


if($cityId){
    $catUrl=$currentUrl."?cityId=".$cityId;
}else{
    $catUrl =$currentUrl."?";
}
if($catId){
    $cityUrl=$currentUrl."?categoryId=".$catId;
}else{
    $cityUrl =$currentUrl."?";
}


?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<style>
.em_dealMain .bannergroup img{
    width: 100%;
}
.site #maincontent{background: none;background-color: #f3f3f4} 
</style>
<div class="em_innerBlock em_allDealPage container">
    <div class="filter-section">
        <h3 class="home-title-icon"><img src="<?php echo $templateUrl.'images/deal/deal-home.png';?>"/>團購優惠</h3>
        <ul  class="categort-list ul-list pull-left"><li class="first" >熱門套票</li></ul>
        <ul class="categort-list ul-list">
            
            <?php foreach ($this->categoryList as $key => $value) {
                if($value->name){
                    $active="";
                    if($value->id==$catId){
                       $active="active";
                    }
                     echo '<li class="'.$active.'"><a title="'.$value->name.'" href="'.$catUrl.'&categoryId='.$value->id.'">'.$value->name.'</a></li>';     
                   if($key==10){
                    break;
                   }
                }
                
            }?>

        </ul>
        <div class="clear"></div>
        <ul class="city-list ul-list pull-left">
            <li class="first" >熱門地區</li>
        </ul>    
        <ul class="city-list ul-list">
            
            <?php foreach ($this->cityList as $key => $value) {
                if($value->name){
                    $active="";
                    if($value->id==$cityId){
                       $active="active";
                    }
                    echo '<li class="'.$active.'"><a title="'.$value->name.'" href="'.$cityUrl.'&cityId='.$value->id.'">'.$value->name.'</a></li>';
                }
                 if($key==10){
                    break;
                   }
            }?>

        </ul>
        <div class="clear"></div>
    </div>

    <div class="featured-deal">
        <div class="row">
            <?php
                if(count($dealList)>1){
                     $featuredDeals[]=  array_shift($dealList);
                     $featuredDeals[]=  array_shift($dealList);
                }else{
                    if(count($dealList)>0){
                        $featuredDeals[]=  array_shift($dealList);
                    }else{
                        echo "<h2>".JText::_('DEAL_LIST_NO_DEAL_MESSAGE')."</h2>";
                    }
                }

                include('top_feature_deal.php');
               
            ?>
        </div>
    </div>

    <!-- Show General Item box Size -->
    <div class="general-items">
        <div class="row">
        
        <?php 
        //  for ($i=0; $i < 5; $i++) { 
        //    $dealList = array_merge($dealList, $dealList);
           
        // }
        include('feature_deal.php');
        include('general_deal.php');

          ?>
        </div>
    </div>
    <?php 
    
    if(count($dealList)>8){
        ?>
        <div class="text-center">
            <p class="more-deal">
                <a href="javascript:void(0)">
                    <img src="<?php echo  $templateUrl;?>images/deal/more-icon.png"/> 
                    更多團購優惠
                </a>
            </p>
        </div>
        <?php
    }

    ?>
    
</div>


<?php 
  //  print_r('<pre>');
    //print_r($dealList);
?>