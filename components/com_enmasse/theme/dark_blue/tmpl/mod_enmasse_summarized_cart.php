<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse".DS."helpers". DS ."EnmasseHelper.class.php");

$theme =  EnmasseHelper::getThemeFromSetting();
$enableRTL =  EnmasseHelper::getRTLFromSetting();

// load language pack
$language = JFactory::getLanguage();
$base_dir = JPATH_SITE.DS.'components'.DS.'com_enmasse';
$version = new JVersion;
$joomla = $version->getShortVersion();
if(substr($joomla,0,3) >= '1.6'){
    $extension = 'com_enmasse16';
}else{
    $extension = 'com_enmasse';
}
if($language->load($extension, $base_dir, $language->getTag(), true) == false)
{
	 $language->load($extension, $base_dir, 'en-GB', true);
}

?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<div class="em_corBlock em_corBlock2">
    <div class="em_contCart">
        <div class="mod_SummarizedCart" id="SummarizedCartModule">
          <div class="left_cl">
            <div class="mod_SummarizedCart_title txt-center"><?php echo JTEXT::_('CART_TOTAL_ITEM');?></div>
	    <div class="mod_SummarizedCart_information txt-center"><?php echo $cart->getTotalItem();?></div>

          </div>
          <div class="right_cl">
            <div class="mod_SummarizedCart_title txt-center"><?php echo JTEXT::_('CART_TOTAL_PRICE');?></div>
	    <div class="mod_SummarizedCart_information txt-center"><?php echo EnmasseHelper::displayCurrency($cart->getAmountToPay());?></div>
          </div>
          <div class="center">
            <input type="button" class="em_btn" value="<?php echo JText::_('CHECK_OUT_BUTTON');?>" onclick="window.location.href='<?php echo JRoute::_('index.php?option=com_enmasse&controller=shopping&task=checkout')?>'"></input>
          </div>
        </div>
    </div><!--contCart-->
</div><!--em_corBlock2-->