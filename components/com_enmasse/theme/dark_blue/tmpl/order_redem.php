<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");

$templateUrl = JURI::base()."templates/stravel/";
$templateUrlDeal = $templateUrl."images/deal/";
define('GOOGLE_CONVERSATION_EXISTED',1);
?>
 <style>
.em_dealMain .bannergroup img{
    width: 100%;
}
.site #maincontent{background: none;background-color: #f3f3f4} 
</style>
<div class="em_innerBlock em_allDealPage container cart order-confirm">

	<h3 class="home-title-icon"><img src="<?php echo $templateUrlDeal.'deal-home.png';?>"/>團購優惠</h3>
    <div class="row flow-bar">
        <div class="col-sm-3  col-xs-6  text-center ">
            選擇產品
            <br/>
            <span>1</span>
        </div>
        <div class="col-sm-3  col-xs-6  text-center">
            核實訂單 
            <br/>
            <span>2</span>
        </div>
        <div class="col-sm-3  col-xs-6  text-center"> 
            輸入資料 
            <br/>
            <span>3</span>
        </div>
        <div class="col-sm-3  col-xs-6 active text-center"> 
            付款/成功訂購 
            <br/>
            <span>4</span>
        </div>
        <div class="line"></div>
        <div class="clear"></div>
    </div>
    <div class="row row-item result-page">
  			<div class="col-sm-10 first" style="">
	    		<h3 class=" text-center">
	    			<?php 
	    				$link = JRoute::_("index.php?option=com_enmasse&controller=deal&Itemid=165", false);
	    				if($_GET['success']){
	    					echo "多謝你已成功訂購產品，我們會將訂購記錄傳送至你的電郵地址。<br/>請你於5個工作天內，前往分行提取文件及完成餘額付款。";
	    				}else{
	    					echo "很抱歉，訂購不成功。請重新訂購，<br/> 如需協助，請請致電我們：23139800。";
	    				}

	    				?> 			
	    		</h3>

    		</div>
    		<div class="col-sm-10">
        			<p class="text-right " style="clear:both">
                       
                         <a href="<?php echo $link;?>">
                              <span class="next">繼續瀏覽</span>
                        </a>
                        
                        
                    </p>
                </div>
    </div>
    
   
    
    
    
		
							   
					
			
		
</div>
<!-- Google Code for S Travel_Online Shopping Transaction Confirmation Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 975128849;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "MSatCNnLjVoQkZL90AM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/975128849/?label=MSatCNnLjVoQkZL90AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php //print_r("<pre>");?>
<?php 
        

?>
<?php //print_r($this->payGtyList);?>