<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");

$oItems = $this->cart->getAll();
$oCartItem = array_pop($oItems);//we just support the cart with one item, so we only need to get the first item in the itemslist of the cart
$item = $oCartItem->getItem();
$deal = JModelLegacy::getInstance('deal','enmasseModel')->getById($item->id);
//tier pricing
if (($item->tier_pricing)==1)
{	
	$item->price = EnmasseHelper::getCurrentPriceStep(unserialize($item->price_step),$deal->price,$item->cur_sold_qty);;
	$item->types =NULL;
	$calculatorPrice = EnmasseHelper::getCalculatorPrice(unserialize($item->price_step),$item->price,$item->cur_sold_qty,$oCartItem->getCount(),$deal->price);
	$printOut = "";
	foreach($calculatorPrice as $k => $v)
	{
		if($k != 'newPrice')
		{
			$printOut.= "+(".$v."x".$k.")";
		}
	}
}
$item_price = $item->price;
?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<div class="em_borBlock em_shopCart">
	<div class="em_wrapDealCart row-fluid">
            <table cellpadding="0" cellspacing="0" border="0" class="em_titDealList">
                <tr>
                <th>Deal Name</th>
                <th>Price</th>
                <th>Qty</th>
                <th>Total</th>
                <th>&nbsp;</th>
            </tr>
            <tr>
                <td>
                        <p class="em_imgDeal"><img alt="" src="../img/img_deal_02.jpg" /></p>
                        <p class="em_txtDeal">Gardens By The Bay and Singapore Flyer Package</p>
                </td>
                <td><span>$<strong>100.00</strong></span></td>
                <td>
                        <input type="hidden" value="1" name="itemId">
                    <input type="hidden" value="com_enmasse" name="option">
                    <input type="hidden" value="shopping" name="controller">
                    <input type="hidden" value="changeItem" name="task">
                    <input type="hidden" value="0" name="buy4friend">
                    <input type="input" class="required validate-numeric" value="1" name="value" id="value" size="1px">
                </td>
                <td><span>$<strong>100.00</strong></span></td>
                <td><a href="#" title="Delete" class="em_btnDetele"><img alt="" src="../img/dummy.gif" /></a></td>
            </tr>
            <tr>
                <td>
                        <p class="em_imgDeal"><img alt="" src="../img/img_deal_02.jpg" /></p>
                        <p class="em_txtDeal">Gardens By The Bay and Singapore Flyer Package</p>
                </td>
                <td><span>$<strong>100.00</strong></span></td>
                <td>
                        <input type="hidden" value="1" name="itemId">
                    <input type="hidden" value="com_enmasse" name="option">
                    <input type="hidden" value="shopping" name="controller">
                    <input type="hidden" value="changeItem" name="task">
                    <input type="hidden" value="0" name="buy4friend">
                    <input type="input" class="required validate-numeric" value="1" name="value" id="value" size="1px">
                </td>
                <td><span>$<strong>100.00</strong></span></td>
                <td><a href="#" title="Delete" class="em_btnDetele"><img alt="" src="../img/dummy.gif" /></a></td>
            </tr>
        </table>
            
		<dl class="em_dealname span4">
	        <dt><?php echo JText::_('SHOP_CARD_DEAL_NAME');?></dt>
	        <dd>
	            <form action="index.php" id="changeItem" method="post" name="changeItem" class="form-validate" onSubmit="return myValidate(this);">	
	                <?php
					echo($item->name . '<br />');
					if(!($item->type))
					{
					?>
						<input type="hidden" name="dealAttribute" value="<?php echo($item->id).'|origin';?>" />
					<?php
					}
					else
					{
					?>
					<select name="dealAttribute">
						<?php
						  	foreach($item->types as $type)
						  	{
								if($type->status) 
								{
						  	?>
						  			<option value="<?php echo($type->id);?>" <?php if($item->type){if($item->type->id == $type->id){echo 'selected="selected"';}} ?>><?php echo($type->name);?></option>
						  	<?php
								}
						} //end foreach 					
					}
					  ?>
					</select> 
	        </dd>
	    </dl>
		<dl class="em_priceCart span2">
	        <dt><?php echo JText::_('SHOP_CARD_PRICE');?></dt>
	        <dd><?php echo  EnmasseHelper::displayCurrency($item->price)?></dd>
	    </dl>
		<dl class="em_qty span2">
	        <dt><?php echo JText::_('SHOP_CARD_QTY');?></dt>
	        <dd>
		            <input type="hidden" name="itemId" value="<?php echo $item->id; ?>" />
					<input type="hidden" name="option" value="com_enmasse" />
					<input type="hidden" name="controller" value="shopping" />
					<input type="hidden" name="task" value="changeItem" />
					<input type="hidden" name="buy4friend" value="<?php echo JRequest::getVar('buy4friend', 0, 'method', 'int')?>" />
					<input type="input" size="1px" id="value" name="value" value="<?php echo $oCartItem->getCount();?>" class="required validate-numeric" />
				</form>
			</dd>
	    </dl>
		<dl class="em_total span2">
	        <dt><?php echo JText::_('SHOP_CARD_TOTAL');?></dt>
	        <dd><?php echo  EnmasseHelper::displayCurrency($this->cart->getAmountToPay());?>	</dd>
	    </dl>
	    <dl class=" span2">
	        <dt><?php echo JText::_('SHOP_CARD_UPDATE_QTY');?></dt>
	        <dd>
	           	<input type="button" class="button" value="<?php echo JText::_('UPDATE_BUTTON');?>" onclick="javascript:document.changeItem.submit();"></input>
	        </dd>
	    </dl>
		
		<div class="em_totalCart">
	        <dl>
	            <dt><?php echo JText::_('SHOP_CARD_TOTAL_ITEM');?>: <?php echo $this->cart->getTotalItem();?></dt>
	            <dd><?php echo JText::_('SHOP_CARD_TOTAL_PRICE');?>: <?php echo EnmasseHelper::displayCurrency($this->cart->getAmountToPay());?></dd>
	            <?php if(($item->tier_pricing)==1) { echo '<dd>Note: '.EnmasseHelper::displayCurrency($this->cart->getAmountToPay()).' = '.$printOut.'</dd>';}?>
	        </dl>
	    </div>
	</div>	
</div>
	