<?php

foreach ($featuredDeals as $key => $oDeal) {
	$link = 'index.php?option=com_enmasse&controller=deal&task=view&id=' . $oDeal->id ."&slug_name=" .$oDeal->slug_name ."&Itemid=$nItemId";

	if (!EnmasseHelper::is_urlEncoded($oDeal->pic_dir)) {
		$imageUrl = $oDeal->pic_dir;
	} else {
		$imageUrlArr = unserialize(urldecode($oDeal->pic_dir));
		$imageUrl = str_replace("\\", "/", $imageUrlArr[0]);
	}
	$tick ="";
	$daysRemain = ((strtotime($oDeal->end_at)-time())/(60*60*24));

	

	$daysRemainz ="";
	if($daysRemain <=1){
		$tick="daysRemain";
	}else{
		$tick="dayRemainNumber";
		$daysRemainz ="<b>".(int)$daysRemain."</b>";
		if((int)$daysRemain>14){
			$tick="dayRemainNumber hide";
		}
	}
	$lastSeats = $oDeal->max_coupon_qty - $oDeal->cur_sold_qty;
		
		// $lastSeats =  2;
	$tick2="";
		if($lastSeats < 10 && $lastSeats){
			$tick2="lastSeats";
		}
	$soldOut = false;
	if($lastSeats<1 || ($lastSeats<2&&$oDeal->quantity_odd==0)){
		$soldOut=true;
	}
	if($lastSeats	< $oDeal->min_item_per_order){
		$soldOut =true;
	}
	$productOrderText ="立即<br>訂購";
	if($soldOut&&$oDeal->max_coupon_qty!='-1'){
		$link ='javascript:void(0);';
		$productOrderText ='經已<br/>售罄';
	}
	?>
	<div class="col-md-6 col-sm-6 item-<?php echo $key;?> listcontainer featured-item">
		<div class="row">
			<div class="col-md-8 col-sm-8 pull-left">
				<a class="product-img"href="<?php echo JRoute::_($link);?>" title="<?php echo $oDeal->name; ?>">
					<img src="<?php echo $imageUrl?>" alt="<?php echo $oDeal->name?>" />
					<?php 
					if($soldOut){
						$tick="sold-out";
						$daysRemainz="";
						$tick2="";
					}
					if($tick||$tick2){
						
						if($tick){ 
							$top="";
							if($tick2){
								$top='style="top:105px"';
							}
							echo '<span '.$top.' class="'.$tick.'">'.$daysRemainz.'</span>';
						}
						if($tick2){ 
							echo '<span class="'.$tick2.'"><b>'.$lastSeats.'</b></span>';
						}
						
					}

					
					?>
				</a>
				<a class="product-img"href="<?php echo JRoute::_($link);?>" title="<?php echo $oDeal->name; ?>">
				<div class="highlight over-lay">
 					<p>
 						<?php
 						if($oDeal->highlight){
 							echo $oDeal->highlight;
 						}else{
 							echo JText::_('NO_HIGH_LIGHT');
 						}
 						?>
 					</p>
 				</div>
 				</a>
			</div>
			<div class="col-md-4 col-sm-4 pull-left">
				<div class="base-info">
					<span class="product-title"><a href="<?php echo JRoute::_($link);?>" title="<?php echo $oDeal->name; ?>"><?php echo $oDeal->name; ?></a></span>
					<span class="product-price"><?php echo '$'.(int)($oDeal->price) ?> 
					<?php //if($oDeal->price!=$oDeal->origin_price){?>
					<span class="percent-saved"><span class="circle-saved">慳</span><?php echo (100 - intval($oDeal->price / $oDeal->origin_price * 100)) ?>%</span>
					<?php //}?>
					</span>
				</div>
				<div class="order-info">
					<div class="col-left">
						<span class="product-orginal-price">原價</span>
						<span class="product-orginal-price-value"><?php echo '$'.(int)($oDeal->origin_price) ?></span>

						<span class="product-bought">已購買人數</span>
						<span class="product-bought-value"><?php echo $oDeal->cur_sold_qty?></span>
					</div>
					<div class="col-right">
						<span class="product-order"><a href="<?php echo JRoute::_($link)?>" title=""><?php echo $productOrderText;?></a></span>
					</div>




				</div>


			</div>
		</div>
	</div>
	<?php
}
?>