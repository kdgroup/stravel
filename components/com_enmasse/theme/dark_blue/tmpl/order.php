<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");

$oItems = $this->cart->getAll();


$nItemId = JFactory::getApplication()->getMenu()->getItems('link','index.php?option=com_enmasse&view=deallisting',true)->id;
$templateUrl = JURI::base()."templates/stravel/";
$templateUrlDeal = $templateUrl."images/deal/";
$catId = JRequest::getVar('categoryId', '');
$cityId = JRequest::getVar('cityId', '');
$itemId = JRequest::getVar('itemId', '');
$currentUrl = JURI::current();
$deal = $oItems[1]->item;
foreach ($oItems as $key => $value) {
    $deal = $value->item;
    $deal->count = $value->count;
}


$dealD->title= $deal->name;
$dealD->subTitle= $deal->subTitle;
$dealD->shortDes= $deal->short_desc;
$dealD->subTitle= $deal->subTitle;

$dealD->insurances = $deal->insuranceList;

$dealD->saved= (int)($deal->price*100/$deal->origin_price);
$dealD->saved= $dealD->saved."%";
$dealD->orginal= "$".(int)$deal->origin_price;
$dealD->price= "$".(int)$deal->price;

$dealD->highlight= $deal->highlight;
$dealD->terms= $deal->terms;

$dealD->Des1= $deal->description;
$dealD->Des2= $deal->description1;
$dealD->Des3= $deal->description2;
$dealD->Des4= $deal->description3;
$dealD->purchase= $deal->description3;
$imageUrl = JURI::root().str_replace("\\","/",$imageUrlArr[0]);
$dealD->image= '<img data-thumb="'.$imageUrl.'" src="'.$imageUrl.'" alt="'. $deal->name.'" />';
$dealD->url = 'index.php?option=com_enmasse&controller=shopping&task=addToCart&dealId='.$deal->id.'&slug_name='.$deal->slug_name.'&referralid='.JRequest::getVar('referralid').'&typeId=none'. '&Itemid=' . $oItem->id;

$dealD->cur_sold_qty =$deal->cur_sold_qty;
$TargetDate  =date('Y/m/d H:i:s', strtotime($deal->end_at));
$StartDate = date('Y/m/d H:i:s', strtotime($deal->start_at));
$dealD->daysRemain = (int)(strtotime($deal->end_at)-strtotime($deal->start_at))/(60*60*24);
$passengerInfo ="";
if($this->cart->passengerInfo){
  $passengerInfo = $this->cart->passengerInfo ;
}

?>
 <style>
.em_dealMain .bannergroup img{
    width: 100%;
}
.site #maincontent{background: none;background-color: #f3f3f4} 
</style>
<div class="em_innerBlock em_allDealPage container cart">
	<h3  class="home-title-icon"><img src="<?php echo $templateUrlDeal.'deal-home.png';?>"/>團購優惠</h3>
    <div class="row flow-bar">
        <div class="col-sm-3  col-xs-6  text-center ">
            選擇產品
            <br/>
            <span>1</span>
        </div>
        <div class="col-sm-3   col-xs-6 text-center">
            核實訂單 
            <br/>
            <span>2</span>
        </div>
        <div class="col-sm-3  col-xs-6 active text-center"> 
            輸入資料 
            <br/>
            <span>3</span>
        </div>
        <div class="col-sm-3  col-xs-6 text-center"> 
            付款/成功訂購 
            <br/>
            <span>4</span>
        </div>
        <div class="line"></div>
        <div class="clear"></div>
    </div>
    <form action='index.php' id="adminForm" name="adminForm"  class="form-validate" method="post" onSubmit="">
    <div class="row">
    	<div class="col-sm-5">
    		<div class="deal-price-info">
    			<h3>套票價格</h3>
    			     
                    <p class="deal-price">
                        <span class="col-sm-6">套票金額: </span>
                    <span class="col-sm-6"><?php echo '$'.$deal->price;?></span>
                     <span class="clear"></span>
                    </p>
                    <?php if($deal->deposit){?>
                        <p class="deal-price">
                            <span class="col-sm-6">訂金: </span>
                        <span class="col-sm-6"><?php echo '$'.((int)$deal->count * (int)$deal->deposit );?></span>
                         <span class="clear"></span>
                        </p> 
                    <?php }?> 
                    <p class="deal-price">
                        <span class="col-sm-6">購買數量: </span>
                    <span class="col-sm-6"><?php echo $deal->count;?></span>
                     <span class="clear"></span>
                    </p>
                    <?php if($this->cart->insuranceFee){?>
                    <p class="insurance-fee">
                        <span class="col-sm-6">保險費用: </span>
                    <span class="col-sm-6"><?php  
                    if(!$this->cart->insuranceFee){
                        $this->cart->insuranceFee = 0;
                    }
                    echo '$'.($this->cart->insuranceFee);?></span>
                    <span class="clear"></span>
					<?php if(!empty($deal->showinsurance) && !empty($this->cart->insuranceFee)) : ?>
                    <p class="insurance-count">成人 : <?php 
                     if(!$this->cart->insuranceAdult){
                        $this->cart->insuranceAdult = 2;
                    }
                     echo $this->cart->insuranceAdult;?> 小童 : <?php 
                     if(!$this->cart->insuranceChild){
                        $this->cart->insuranceChild = 0;
                    }
                     echo $this->cart->insuranceChild;?></p>
					<?php endif; ?> 
                    </p>
                    <?php }?>

                    <div class="line"></div>
                    <?php if($deal->deposit){?>
                    <p class="total">
                    <span class="col-sm-6">餘額: </span>
                    <span class="col-sm-6"><?php echo '$'.((int)$this->cart->getTotalPrice() - (int)$deal->count * (int)$deal->deposit );?></span>
                         <span class="clear"></span>
                   </p>
                    <?php }?>
                    <p class="total">
                    <span class="col-sm-6">總金額: </span>
                    <span class="col-sm-6"><?php echo '$'.((int)$this->cart->getTotalPrice());?></span>
                         <span class="clear"></span>
                   </p>
                    <p class="remark">(*以上價格未包各項稅款及附加費用)</p>
    		</div>
            <div class="deal-price-info second">
                 <h3>如有問題請致電我們</h3>

            </div>
            <div class="deal-price-info three">
                <h3><img src="<?php echo $templateUrlDeal.'phone.jpg';?>" />5542 6866 </h3>    
            </div>
            
    	</div>
    	<div class="col-sm-7 deal-order orginal-price">
            <div class="row passenger-section first deal-detail">
                <h3 class="deal-name"><?php echo $dealD->title;?></h3>
                 <a href="javascript:void(0);" class="show-pop-up"><input type="checkbox" checked="checked"/>我已閱讀 條款及細則</a>

                <div class="pop-up terms hide" style="background-color: rgb(140, 197, 64);left: 180px;width: 200px;top: -14px;">
                        <img class="arrow" src="<?php echo $templateUrlDeal;?>pop-arrow.png"/>
                        <?php 
                            if($dealD->terms){
                                 echo $dealD->terms;
                            }
                             echo '<p><a target="_blank" href="'.$deal->master_term_url.'" title="'.$deal->master_term.'">'.$deal->master_term.'</a></p>';
                        ?>
                    </div>
            </div>
    		<!--<div class="row passenger-section">
    			
    			<h3><?php echo $this->cart->setting->passenger_title;?></h3>
    			<p><?php echo $this->cart->setting->passenger_description;?> </p>
    		</div>
    		<div class="row passenger-info first">
    			<h3>第一位旅客</h3>
                <div class="col-sm-6 input-item">
                    <span>英文姓</span> <input type="text" value="<?php if($passengerInfo->surname){echo $passengerInfo->surname;}?>" class="required" name="surname" /> 
                </div>
                <div class="col-sm-6 input-item">
                    <span>英文名</span> <input type="text" class="required" name="name" value="<?php if($passengerInfo->surname){echo $passengerInfo->surname;}?>" />
                </div>
                <div class="clear"></div>
                <div class="col-sm-5 input-item">
                    <span>性別</span> <input  class=" raido required" type="radio" name="sex" value="男"<?php if($passengerInfo->sex=="男"){echo 'checked="checked"';}?> />男 
                    <input class=" raido required" type="radio" name="sex" value="女" <?php if($passengerInfo->sex=="女"){echo 'checked="checked"';}?>/>女
                </div>
                <div class="col-sm-7 input-item">
                    <span>出生日期</span><input type="text"  id="birthday-c1" pattern="\d{1,2}/\d{1,2}/\d{4}" class=" birthday  required" value="<?php if($passengerInfo->surname){echo $passengerInfo->birthday;}?>"placeholder="dd/mm/yyyy" name="birthday" />
                </div>
    			
    		</div>
    		<div class="row passenger-info second">
    			<h3>第二位旅客  </h3>
                <div class="col-sm-6 input-item">
                    <span>英文姓</span> <input type="text" value="<?php if($passengerInfo->surname2){echo $passengerInfo->surname2;}?>" class="required" name="surname2" /> 
                </div>
                <div class="col-sm-6 input-item">
                    <span>英文名</span> <input type="text" class="required" name="name2" value="<?php if($passengerInfo->name2){echo $passengerInfo->name2;}?>" />
                </div>
                <div class="clear"></div>
                <div class="col-sm-5 input-item">
                    <span>性別</span> <input  class=" raido required" type="radio" name="sex2" value="男"<?php if($passengerInfo->sex=="男"){echo 'checked="checked"';}?> />男 
                    <input class=" raido required" type="radio" name="sex2" value="女" <?php if($passengerInfo->sex=="女"){echo 'checked="checked"';}?>/>女
                </div>
                <div class="col-sm-7 input-item">
                    <span>出生日期</span><input type="text" id="birthday-c2"  pattern="\d{1,2}/\d{1,2}/\d{4}" class=" birthday  required" value="<?php if($passengerInfo->birthday2){echo $passengerInfo->birthday2;}?>" placeholder="dd/mm/yyyy"  name="birthday2" />
                </div>
    			<!-- <p>英文姓 <input type="text" class="required" name="surname2" value="<?php if($passengerInfo->surname2){echo $passengerInfo->surname2;}?>" />
                 英文名 <input type="text" class="required" name="name2" value="<?php if($passengerInfo->name2){echo $passengerInfo->name2;}?>" /></p>
    			<p>性別 <input <?php if($passengerInfo->sex2=="男"){echo 'checked="checked"';}?> type="radio" name="sex2" value="男"/>
                男 <input type="radio" name="sex2" value="女" <?php if($passengerInfo->sex2=="女"){echo 'checked="checked"';}?>/>女
    			英文姓 <input type="text" class="required" name="birthday2" value="<?php if($passengerInfo->birthday2){echo $passengerInfo->birthday2;}?>" />
    			</p> -2->
    		</div>-->
    		<div class="row passenger-info contact-person">
    			<h3>聯絡人資料</h3>
                <div class="col-sm-6 input-item">
                    <span>姓</span> <input title="必需填寫或選擇資料" type="text" placeholder="輸入姓" value="<?php if($passengerInfo->surname3){echo $passengerInfo->surname3;}?>" class="required" name="surname3" /> 
                </div>
                <div class="col-sm-6 input-item">
                    <span>名</span> <input title="必需填寫或選擇資料" type="text" placeholder="輸入名" class="required" name="name3" value="<?php if($passengerInfo->name3){echo $passengerInfo->name3;}?>" />
                </div>
                <div class="col-sm-6 input-item">
                    <span>聯絡電話</span> <input title="必需填寫或選擇資料" type="text" placeholder="輸入電話" value="<?php if($passengerInfo->phone3){echo $passengerInfo->phone3;}?>" class="required" name="phone3" /> 
                </div>
                <div class="col-sm-6 input-item">
                    <span>電郵</span> <input title="必需填寫或選擇資料" type="text" placeholder="輸入電郵" class="required email" id="email3" name="email3" value="<?php if($passengerInfo->email3){echo $passengerInfo->email3;}?>" />
                </div>
                <div class="col-sm-6 col-sm-offset-6 input-item">
                    <span>確認電郵</span> <input title="必需填寫或選擇資料" type="text" placeholder="輸入電郵" class="required email" name="reemail3" value="<?php if($passengerInfo->email3){echo $passengerInfo->reemail3;}?>" />
                </div>
    			<!-- <p>姓 <input type="text" class="required" name="surname3" value="<?php if($passengerInfo->surname3){echo $passengerInfo->surname3;}?>" />
                 名  <input type="text" class="required" name="name3" value="<?php if($passengerInfo->name3){echo $passengerInfo->name3;}?>" /></p>
    			<p>聯絡電話  <input type="text" class="required" name="phone3" value="<?php if($passengerInfo->phone3){echo $passengerInfo->phone3;}?>" />
                 電郵   <input type="text" class="required" name="email3" value="<?php if($passengerInfo->email3){echo $passengerInfo->email3;}?>"  /></p> -->
    		</div>
    		<div class="row passenger-info discount-code">
    			<p>優惠代碼: <input name="discount" type="text"  placeholder="輸入代碼" value="<?php if($passengerInfo->discount){echo $passengerInfo->discount;}?>" ></p>
    			<p><input type="checkbox" checked="checked" name="term" class="term"/>本人確定接收本公司的優惠資訊</p>
    			<p class="remark">如你不同意，將有機會錯過收到由心程旅遊及合作商戶提供之折扣優惠及最新資訊。 </p>
    		</div>
    		<div class="row passenger-info payment">
    			<p>領取文件及完成餘額付款
    			
    			<?php //if($this->cart->getAmountToPay() > 0){?>
    		<select style="display:none"name="payGtyId" required class="ui-widget-content" id="payGtyId" class="required">
					<option value=""><?php echo JText::_('請選擇分行');?></option>
					<?php 
							$cart = $this->cart;
							$item_price = $cart->getTotalPrice();					
							
							$numberOfPayment = count($this->merchant); 						
							foreach($this->payGtyList as $row):
								if($row->class_name != "point")
								{
										if($numberOfPayment==1)
										{
											echo "<option value=\"".$row->id."\" SELECTED>".$row->name."</option>";
										}
										else
										{
                                            $select ="";
                                            if($passengerInfo->payGtyId==$row->id){
                                                $select ="SELECTED";
                                            }
                                            if(payGtyId)
											echo "<option ".$select." value=\"".$row->id."\">".$row->name."</option>";
										}
								}
							endforeach;
							//}
							?>
						</select>
    		<?php //}?>
            <?php //if($this->cart->getAmountToPay() > 0){
                // print_r("<pre>");                    
                //                 print_r($this->locations);
                ?>
                <select title="必需填寫或選擇資料" name="branch" required class="ui-widget-content" id="branch" class="required">
                        <option value=""><?php echo JText::_('請選擇分行');?></option>
                        <?php 
                                $cart = $this->cart;
                                $item_price = $cart->getTotalPrice();                   
                                $branches = json_decode($this->merchant->branches);
                                $numberOfPayment = count($this->merchant);  

                                foreach($branches as $row):
                                  
                                            if(!in_array($row->name, $this->locations)){
                                                continue;
                                            }
                                                $select ="";
                                                if($cart->branch==$row->name){
                                                    $select ="SELECTED";
                                                }
                                                
                                                echo "<option ".$select." value=\"".$row->name."\">".$row->name."</option>";
                                           
                                endforeach;
                                //}
                                ?>
                            </select>
                <?php //}?>
    			</p>
    		</div>
    		 <input type="hidden" name="check" value="post" /> 
							   <input type="hidden" name="option" value="com_enmasse" /> 
							   <input type="hidden" name="controller" value="shopping" /> 
							   <input type="hidden" name="task" value="submitPersonalData" />
							   <div class="em_totalCart"  style="margin-top: -10px;">
    		
    		
    	</div>
    </div>
    </div>
    <div class="row">
            <p class="text-right" >
                <a style="margin-right: 20px;" href="<?php echo JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$buy4friend&Itemid=$nItemId&step=1", false);?>">
                    <span class="back"><img height="21"   src="<?php echo $templateUrlDeal;?>Previous-Reset.png"/></span>
                </a>
                 <a href="javascript:void(0);">
                    <span class="next" style="padding: 8px 0px 10px 10px;"><input id="submit-order" style="padding: 8px 70px 10px 10px;" type="submit" class="submit" value="下一步"/></span>
                </a>
                <!-- <a href="javascript:void(0);" >
                    <span class="next">
                        <input style="width:127px;" class="order-submit" type="submit"  value="立即訂購"></input>
                        
                    </span>
                </a> -->
                
                
            </p>
        </div>

		</form>
</div>
<?php //print_r("<pre>");?>
<?php //print_r($this->cart);?>