<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once(JPATH_SITE . DS."components". DS ."com_enmasse".DS."theme".DS."dark_blue".DS."tmpl".DS."background_image.php");
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."DatetimeWrapper.class.php");

$theme =  EnmasseHelper::getThemeFromSetting();
$oDeal = $this->objDeal;
$aComments = $this->aComments;
//tier pricing
if (($oDeal->tier_pricing)==1)
{
	$oDeal->price = EnmasseHelper::getCurrentPriceStep(unserialize($oDeal->price_step),$oDeal->price,$oDeal->cur_sold_qty);
	$oDeal->types =NULL;
}
?>
<script type="text/javascript" src="components/com_enmasse/theme/js/rating.js"></script>

<div class="em_innerBlock em_postReviewPage">
    <div class="em_titReview row-fluid">
            <div class="em_price span5">
            <span><?php echo EnmasseHelper::displayCurrency($oDeal->price)?></span>
            <?php if(strtotime($oDeal->end_at) + 86400 < time()):?>
                    <a href="#" class="btnBuy"><?php echo JText::_('BUY_DEAL_EXPIRED')?></a>
            <?php elseif(strtotime($oDeal->start_at) > time()):?>
                    <a href="#" class="btnBuy"><?php echo JText::_('BUY_DEAL_UPCOMING')?></a>
            <?php elseif ($oDeal->status == 'Voided'):?>
                    <a href="#" class="btnBuy"><?php echo JText::_('BUY_DEAL_VOIDED')?></a>
            <?php elseif($oDeal->max_coupon_qty != "-1" && $oDeal->cur_sold_qty >= $oDeal->max_coupon_qty):?>
                    <a href="#" class="btnBuy"><?php echo JText::_('BUY_DEAL_SOLD_OUT')?></a>
            <?php else :?>
                <a href="index.php?option=com_enmasse&controller=shopping&task=addToCart&dealId=<?php echo $oDeal->id .'&slug_name=' .$oDeal->slug_name;?>" class="btnBuy"><?php echo JText::_('Buy')?></a>
            <?php endif;?>
        </div><!--em_price-->
        <h4 class="span7">
            <span><?php echo $oDeal->name; ?></span>
            <a href="<?php echo JRoute::_('index.php?option=com_enmasse&controller=deal&task=view&id=' . $oDeal->id ."&slug_name=" .$oDeal->slug_name); ?>" ><?php echo JText::_('RETURN_TO_DEAL'); ?></a>
        </h4>
    </div><!--em_titReview-->

    <?php foreach($aComments as $aComment): 
            $user = JFactory::getUser($aComment['user_id']);
    ?>
            <div class="comment_area">
                <p class="comment_content"><?php echo nl2br($aComment['comment']); ?></p>
                <p class="comment_details">
                    <span class="rating disabled">
                        <?php for($i=1; $i<=$aComment['rating']; $i++): ?>
                            <span class="ratingStar filled">&nbsp;</span>
                        <?php endfor;?>
                        <?php for($i=$aComment['rating']; $i<5; $i++): ?>
                            <span class="ratingStar">&nbsp;</span>
                        <?php endfor;?>                            
                    </span>                    
                    <br />
                    <span class="author"><?php echo $user->name; ?> - <?php echo $aComment['created_at']; ?></span>
                    <br />
                </p>
            </div>
    <?php endforeach; ?>
    
    <div class="em_postReview">
        <?php if(JFactory::getUser()->get("guest")):
            $sRedirectUrl = base64_encode('index.php?option=com_enmasse&controller=deal&task=comment&id=' . $oDeal->id);
            $sLoginLink = JRoute::_("index.php?option=com_users&view=login&return=" . $sRedirectUrl, false);
        ?>
            <a class="sign_in_to_review" href="<?php echo $sLoginLink; ?>"><?php echo JText::_('SIGN_IN_TO_REVIEW'); ?></a> 
        <?php else: ?>
            <h4><?php echo JText::_('POST_REVIEW_TITLE'); ?></h4>
            <p class="average_rating"><?php echo JText::_('RATE_THIS_DEAL'); ?>
                <span class="rating">                
                    <span class="ratingStar">&nbsp;</span>
                    <span class="ratingStar">&nbsp;</span>
                    <span class="ratingStar">&nbsp;</span>
                    <span class="ratingStar">&nbsp;</span>
                    <span class="ratingStar">&nbsp;</span>
                </span> 
            </p>
            <form id="review" name="review" method="post" action="index.php">
                    <input type="hidden" name="option" value="com_enmasse" />
                    <input type="hidden" name="controller" value="comment" />
                    <input type="hidden" name="task" value="submit_review" />
                    <input type="hidden" name="nDealId" value="<?php echo $oDeal->id; ?>" />
                    <input type="hidden" id="nRating" name="nRating" value="" />
                    <div class="em_review_form">
                        <textarea cols="70" name="sReviewBody" id="sReviewBody" rows="10"></textarea>
                    </div>
                    <div id="review_errors" class="review_errors"></div>
                    <input type="button" class="em_btn" onclick="submit_form();" value="<?php echo JText::_('POST_REVIEW_BUTTON');?>"></input>
            </form>
        <?php endif; ?>
    </div><!--em_postReview-->
</div><!--em_innerBlock-->
     
<script type="text/javascript">
function submit_form()
{
    var form = document.review;
    if(form.nRating.value == '' || (form.nRating.value <= 0 && form.nRating.value > 5))
    {
        document.getElementById("review_errors").innerHTML = "<?php echo JText::_('PLEASE_RATE'); ?>";
        return false;
    }
    if(form.sReviewBody.value == '')
    {
        document.getElementById("review_errors").innerHTML = "<?php echo JText::_('PLEASE_ENTER_REVIEW'); ?>";
        return false;
    }
    form.submit();
}
</script>