<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once(JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
 JHtml::_('behavior.formvalidation');
$theme =  EnmasseHelper::getThemeFromSetting();
// getting data from view
$data = $this->data;

// getting class for subscription
$integrationClass = EnmasseHelper::getSubscriptionClassFromSetting();
$sub_page = JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."subscription". DS .$integrationClass. DS."sub_page.php";
$oMenu = JFactory::getApplication()->getMenu();
$oItem = $oMenu->getItems('link','index.php?option=com_enmasse&view=dealtoday',true);
$sRedirectLink = JRoute::_('index.php?option=com_enmasse&controller=deal&task=today&Itemid=' . $oItem->id, false);
if(!$sub_page){
	JFactory::getApplication()->redirect($sRedirectLink,$msg);
	exit;
}
?>

<div class="modal hide fade" id="modal-subscription-box" >
  <div class="modal-header">
    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    	<h3><?php echo JText::_('SUBSCRIPTION_PAGE_TITLE');?></h3>
  </div>
<?php if($integrationClass != 'acyenterprise') { ?>
    <form action="" method="post" name="adminForm" id="adminForm"  class="form-validate" >
<?php } ?>
        
<div class="modal-body">
	<?php 
                include $sub_page;
	?>
</div>
        
  <div class="modal-footer">
   <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('CLOSE');?></button>
   <?php if($integrationClass != 'acyenterprise') { ?>
        <button class="btn btn-primary"><?php echo JText::_('JSUBMIT');?></button>
      </div>
     </form>
   <?php } else { ?>
        <input class="button btn btn-primary" type="submit" value="Subscribe" name="Submit" onclick="submit_multiform();">
    </div>
   <?php } ?>
</div>

<script type="text/javascript">

    var today_deal_link = '<?php echo $sRedirectLink; ?>';
            jQuery('#modal-subscription-box').modal({
                keyboard: false
            }).on('hide', function () {
                      window.location = today_deal_link;
                      });
  
</script>
