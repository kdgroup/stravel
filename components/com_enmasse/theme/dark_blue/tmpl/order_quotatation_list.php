<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
JHtml::_('behavior.framework');
//JHTML::_('behavior.calendar');
JHTML::_('behavior.tooltip');
JFactory::getDocument()->addScript(JURI::root() . "administrator/components" . "/com_enmasse" . "/script" . "/datepicker" . "/Locale.en-US.DatePicker.js");
JFactory::getDocument()->addScript(JURI::root() . "administrator/components" . "/com_enmasse" . "/script" . "/datepicker" . "/Picker.js");
JFactory::getDocument()->addScript(JURI::root() . "administrator/components" . "/com_enmasse" . "/script" . "/datepicker" . "/Picker.Attach.js");
JFactory::getDocument()->addScript(JURI::root() . "administrator/components" . "/com_enmasse" . "/script" . "/datepicker" . "/Picker.Date.js");
JFactory::getDocument()->addStyleSheet(JURI::root() . "administrator/components" . "/com_enmasse" . "/script" . "/datepicker" . "/datepicker_dashboard" . "/datepicker_dashboard.css");
?>
<script type="text/javascript">
    addEvent('domready', function() {
        new Picker.Date($$('.calendar'), {
            timePicker: true,
            draggable: false,
            positionOffset: {x: 5, y: 0},
            pickerClass: 'datepicker_dashboard',
            format: 'db',
            useFadeInOut: !Browser.ie
        });
    });
</script>
<?php
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.modal');
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "helpers" . DS . "EnmasseHelper.class.php");
$theme = EnmasseHelper::getThemeFromSetting();
$enableRTL = EnmasseHelper::getRTLFromSetting();
//if($enableRTL){
//	JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen_rtl.css');
//}
//JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen.css');
$oMenu = JFactory::getApplication()->getMenu();
$oItem = $oMenu->getItems('link', 'index.php?option=com_enmasse&view=dealtoday', true);
?>
<style>
    #sbox-window {
        padding: 0px !important;
    }
    #sbox-content{
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
    }
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<h3><?php echo JText::_("My Quotation") ?></h3>
<div class="em_innerBlock em_orderPage">
    <div class="em_orderList">
        <table width="100%">
            <tr class="oderTitle">
                <th><?php echo JText::_('ORDER_ID'); ?></th>
                <th><?php echo JText::_('ORDER_DEAL'); ?>
                &nbsp | &nbsp <?php echo JText::_('ORDER_QTY'); ?></th>
                <th><?php echo JText::_('ORDER_TOTAL'); ?></th>
                <th><?php echo JText::_('ORDER_DATE'); ?></th>
                <th><?php echo JText::_('ORDER_DELIVERY'); ?></th>
                <th><?php echo JText::_('ORDER_METHOD'); ?></th>
                <th><?php echo JText::_('ORDER_STATUS'); ?></th>
                <th style="text-align: center;"><?php echo JText::_('ORDER_ACTION'); ?></th>
            </tr>

            <?php $count = 0; ?>
            <?php foreach ($this->orderList as $orderRow):
                   
                ?>
                    <tr  <?php if ($count % 2 == 0) echo "class=\"highlight\"" ?>>
                        <td><?php echo $orderRow->display_id ?></td>
                        <td>
                            <table width="100%">
                    <?php foreach ($orderRow->orderItem as $i => $orderItem) { ?>
                            <tr style="background: none;">
                                <td><?php echo $orderItem->description ?></td>
                                <td width="30"><?php echo $orderItem->qty ?></td>
                            </tr>
                                
                    <?php } ?>
                            </table>
                        </td>
                        <td><?php echo EnmasseHelper::displayCurrency($orderRow->total_buyer_paid); ?></td>
                        <td><?php echo DatetimeWrapper::getDisplayDatetime($orderRow->created_at); ?></td>
                        <td><?php echo DatetimeWrapper::getDisplayDatetime($orderRow->updated_at); ?></td>
                        <td><?php echo $orderRow->payment_method; ?></td>
                        <td><p style="min-height: 28px;padding-top: 14px;line-height: 14px;"><?php if($orderRow->payment_status == '1'){echo "Validating" ;}else{echo $orderRow->status; }?><br/>Payment</p></td>
                        <td>
                            <div style="display: inline-block;width: 104px;" class="actionQuotation actionQuotationBtn">
                                <?php if($orderRow->pay_gty_id == '1'){ ?>
<!--                                <a class="modal" href="index.php?option=com_enmasse&controller=order&task=popupPaymentSubmit&orID=//<?php echo $orderRow->id; ?>&payGtyId=<?php echo $orderRow->pay_gty_id; ?>&tmpl=component" rel="{handler: 'iframe', size: {x: 351, y: 270}, onClose: function() {}}">
                                    <div class="actionOrder submitPayment">
                                        <p>submit</p><p>payment</p>
                                    </div>
                                </a>-->
                                <?php } ?>
                                <a href="#"  onclick='sendQuotation(<?php echo $orderRow->id; ?>)' style=""><div class="actionOrder downQuotation"><p>Download</p><p>Quotation</p></div></a>
                            </div>
                        </td>
                    </tr>
                <?php $count++ ?>
            <?php endforeach; ?>
        </table>
    </div><!--em_orderList-->
</div><!--em_innerBlock-->
<script type="text/javascript">
    function sendQuotation(orderId) {
        jQuery.ajax({
            url: "index.php?option=com_enmasse&controller=mail&task=reSendQuotation",
            type: 'POST',
            dataType: 'html',
            data: {
                'orderId': orderId
            },
            success: function(rs) {
                if (rs == '1') {
                    alert('Your quotation has being sent to your email.');
                }
                else if(rs == '0') {
                    alert('Please check your email, we have just sent your quotation a moment ago.');
                }
                else {
                    alert("You don't have permission.Please contact with admin to ask for help.");
                }
            }
        });

    }

</script>
<style>
    .modalContainQuotation{
        padding:25px 30px;
    }
    .titleModelQuotation{
        font-size: 13px;
        display: inline-block;
        margin-bottom: 5px;
    }
    .datepicker_dashboard{
        z-index: 9999;
    }
    #dateBankIn{
        background: url("<?php echo JURI::root(); ?>components/com_enmasse/theme/dark_blue/images/text-arrow.jpg") no-repeat scroll 310px 0 rgba(0, 0, 0, 0);
        width: 95%;
    }
    .btModelQuotation{
        margin-bottom: 11px;
        margin-top: -24px;
        text-align: center;
    }
    .btModelQuotation input{
        font-size: 25px;
        height: 30px;
        width: 100px;
        background: url("<?php echo JURI::root(); ?>components/com_enmasse/theme/dark_blue/images/btn-login-2.png") repeat scroll 0 0 rgba(0, 0, 0, 0);
    }
</style>