<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');
require_once(JPATH_SITE . DS . "components" . DS . "com_enmasse" . DS . "theme" . DS . "dark_blue" . DS . "tmpl" . DS . "background_image.php");
$nItemId = JFactory::getApplication()->getMenu()->getItems('link','index.php?option=com_enmasse&view=deallisting',true)->id;
JFactory::getDocument()->addScript("components/"."com_enmasse/"."theme/"."dark_blue/"."js/"."UIslider.js");
?>

<div class="em_catelogy_dt">
    <script>
    $(document).ready(function(){
        jQuery('.em_catelogy_dt').parent().addClass('em_catelogy');
    });
    </script>
    <div class="tabbable">
        
<!--        <a class="btnArrow btnBack" href="javascript:void(0);" style="color: white;text-decoration: none;position: absolute;top: 0;left: 0;bottom: 0;z-index: 6;background: #4f81bd;margin: 7px 5px;line-height: 5;font-size: 24px;padding: 0 10px;border: 4px solid #385d8a;display:none;">&#171;</a>
        <a class="btnArrow btnNext" href="javascript:void(0);" style="color: white;text-decoration: none;position: absolute;top: 0;right: 0;bottom: 0;z-index: 6;background: #4f81bd;margin: 7px 5px;line-height: 5;font-size: 24px;padding: 0 10px;border: 4px solid #385d8a;display:none;">&#187;</a>-->
        <div id="slider1_container" style="position: relative; top: 1px; left: -5px; width: 880px;height: 42px;">
<!--            <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;background-color: #000; top: 0px; left: 0px;width: 100%;height:100%;">
                </div>
            </div>-->
            <ul u="slides" class="nav nav-tabs avenger" style="cursor: move; position: absolute; left: 5px; top: 0px; width: 880px; height: 42px;
                    overflow: hidden;">
                <?php foreach ($this->categoryList as $i => $category){ ?>
                <li class="<?php if($i==0){echo 'active';} ?>"><a href="#tab<?php echo $i; ?>" class="tab<?php echo $i; ?>" data-toggle="tab"><span class="icon"></span><span><?php echo $category->name; ?></span></a></li>   
                <?php } ?>
            </ul>
            <style>
                .jssord03l, .jssord03r, .jssord03ldn, .jssord03rdn
                {
                    position: absolute;
                    cursor: pointer;
                    display: block;
                    background: #4f81bd;
                    overflow: hidden;
                    height: 42px !important;
                    top: 0px !important;
                    width: 20px !important;
                }
                .jssord03r,.jssord03l,.jssord03rdn,.jssord03ldn { font-size: 38px;line-height: 3.5;text-align: center; color:#fff; opacity:1; text-indent:-1000em;border-radius:5px 0 0 5px; }
                .jssord03l,.jssord03ldn {background: url('components/com_enmasse/theme/dark_blue/images/btnBack.png')center center no-repeat;}
                .jssord03l:hover,.jssord03ldn {background:  url('components/com_enmasse/theme/dark_blue/images/btnBack2.png')center center no-repeat;}
                .jssord03r,.jssord03rdn {background: url('components/com_enmasse/theme/dark_blue/images/btnNext.png')center center no-repeat;border-radius:0px 5px 5px 0px;}
                .jssord03r:hover,.jssord03rdn {background:  url('components/com_enmasse/theme/dark_blue/images/btnNext2.png')center center no-repeat;border-radius:0px 5px 5px 0px;}

                .avenger > div {left:-32px !important;}
                .avenger > div > a {display: block;height: 42px;position: relative;}
                .avenger > div > a > img {max-width:100%;display: block;position: absolute;top: 0;left: 0;right: 0;bottom: 0;margin: auto; }
                #slider1_container li{
                    /*width: none  !important;*/
                }
            </style>
            <!-- Arrow Left -->
            <span u="arrowleft" class="jssord03l" style="width: 105px; height: 55px; top: 123px; left: 5px;">
            </span>
            <!-- Arrow Right -->
            <span u="arrowright" class="jssord03r" style="width: 55px; height: 55px; top: 123px; right: 5px">
            </span>
        </div>
            
        <script>
                jssor_slider1_starter('slider1_container');
        </script>
        
        
        <div class="tab-content">
            <?php foreach ($this->categoryList as $i => $category){ ?>
            <div class="tab-pane <?php if($i==0){echo 'active';} ?>" id="tab<?php echo $i; ?>">
                <div class="listProduct">
                    <div class="tableListProduct">
                        <table id="tblList">
                            <?php foreach ($category->listDeal as $index => $oDeal){
                                $link = 'index.php?option=com_enmasse&controller=deal&task=view&id='.$oDeal->id .'&slug_name='.$oDeal->slug_name.'&sideDealFlag=1&Itemid='.$nItemId;
//                                $link = 'index.php?option=com_enmasse&controller=shopping&task=addToCart&dealId='.$oDeal->id.'&slug_name='.$oDeal->slug_name.'&referralid='.JRequest::getVar('referralid').'&typeId=none'."&Itemid=$nItemId";
                                if (!EnmasseHelper::is_urlEncoded($oDeal->pic_dir)) {
                                        $imageUrl = $oDeal->pic_dir;
                                } else {
                                        $imageUrlArr = unserialize(urldecode($oDeal->pic_dir));
                                        $imageUrl = str_replace("\\", "/", $imageUrlArr[0]);
                                }
                                $sDealName = $oDeal->name;
                            ?>
                            <tr>
                                <?php if($category->name != "Gift Voucher"){?>
                                    <td style="width: 215px;"><img width="215" src="<?php echo $imageUrl?>" alt="" /></td>
                                <?php }else{ ?>
                                    <td></td>
                                <?php }?>
                                <td>
                                    <h3 class="title-product"><?php echo $sDealName?></h3>
                                    <p><?php echo $oDeal->short_desc; ?></p>
                                </td>
                                <td class="price">
                                    <div class="em_price1" id="price_tag_cont">
                                        <span><?php echo EnmasseHelper::displayCurrency($oDeal->price) ?> </span>
                                    </div>
                                    <a href="<?php echo JRoute::_($link);?>"><span class="em_btn_02">View This Deal</span></a>
                                </td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
