<?php
$order = $this->order;
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.modal');
JHtml::_('behavior.framework');
//JHTML::_('behavior.calendar');
JHTML::_('behavior.tooltip'); 
JFactory::getDocument()->addScript(JURI::root() . "administrator/components" . "/com_enmasse" . "/script" . "/datepicker" . "/Locale.en-US.DatePicker.js");
JFactory::getDocument()->addScript(JURI::root() . "administrator/components" . "/com_enmasse" . "/script" . "/datepicker" . "/Picker.js");
JFactory::getDocument()->addScript(JURI::root() . "administrator/components" . "/com_enmasse" . "/script" . "/datepicker" . "/Picker.Attach.js");
JFactory::getDocument()->addScript(JURI::root() . "administrator/components" . "/com_enmasse" . "/script" . "/datepicker" . "/Picker.Date.js");
JFactory::getDocument()->addStyleSheet(JURI::root() . "administrator/components" . "/com_enmasse" . "/script" . "/datepicker" . "/datepicker_dashboard" . "/datepicker_dashboard.css");

$pay_detail_fields = $order->pay_detail;

if(empty($pay_detail_fields)):
	$ordModel = JModelLegacy::getInstance('order','enmasseModel');
	$pay_detail_fields = $ordModel->getPayDetailById(JRequest::getInt('payGtyId', 0));
endif;
?>
<script type="text/javascript">
    addEvent('domready', function() {
        new Picker.Date($$('.calendar'), {
            timePicker: true,
            draggable: false,
            positionOffset: {x: 5, y: 0},
            pickerClass: 'datepicker_dashboard',
            format: 'db',
            useFadeInOut: !Browser.ie
        });
    });
</script>

<div class="modalQuotation">
    <form method="post" action="index.php">
        <div class="headerQuotaion">
            <h3>Payment Information:</h3>
        </div>
        <div class="modalContainQuotation">
		<?php
		if($pay_detail_fields):
			$arr_decode = json_decode($pay_detail_fields, true);

			foreach($arr_decode as $key=>$val):
				//print_r( $val).'<br>';
				$tag_attributes = NULL;
				switch($val['type']){
					case 'text':
						$tag_attributes = 'class="inputbox" required="true"'; break;
					case 'datepicker':
						$tag_attributes = 'class="calendar" readonly placeholder="0000-00-00 00:00:00" required="true"'; break;
					default:
						break;
				}
			?>
				<input type="hidden" name="pay_attr[<?php echo $key?>][label]" value="<?php echo $val['label']?>" />
				<input type="hidden" name="pay_attr[<?php echo $key?>][type]" value="<?php echo $val['type']?>" />
				<span class="titleModelQuotation"><?php echo $val['label']?></span>
				<?php if($val['type'] == 'file'): ?>
				<input type="file" style="width:96%" name="pay_attr[<?php echo $key?>][file]" />
				<?php else: ?>
				<input type="text" style="width:96%" name="pay_attr[<?php echo $key?>][value]" value="<?php if($val[value]) echo $val[value]?>" <?php echo $tag_attributes?>/>
				<?php endif;?>
			<?php
			endforeach;
		endif;
		?>	
        </div>
        <div class="btModelQuotation"><input class="em_btn" type="submit" value="Submit" /></div>
        <input type="hidden" name="oid" value="<?php echo $order->id; ?>" />
        <input type="hidden" name="status" value="<?php echo $order->status; ?>" /> 
        <input type="hidden" name="option" value="com_enmasse" /> 
        <input type="hidden" name="controller" value="order" /> 
        <input type="hidden" name="views" value="<?php echo JRequest::getVar('views');?>" /> 
        <input type="hidden" name="task" value="submitPayment" />
    </form>
</div>
<script type="text/javascript">
    jQuery('.btModelQuotation').click(function() {
        var chequeNumber = jQuery('#chequeNumber').val();
        if (chequeNumber == "") {
            return false;
        }
        if (jQuery('#dateBankIn').val() == '') {
            alert("please pick date !");
            return false;
        }
    });
</script>
<style>
    @font-face {
        font-family: "Libel";
        src: url("<?php echo JUri::root(); ?>/templates/enmasse_18/fonts/utm_libel_kt-webfont.eot?#iefix") format("embedded-opentype"), url("<?php echo JUri::root(); ?>/templates/enmasse_18/fonts/utm_libel_kt-webfont.woff") format("woff"), url("../fonts/utm_libel_kt-webfont.ttf") format("truetype"), url("../fonts/utm_libel_kt-webfont.svg#utm_libel_ktregular") format("svg");
    }

    .modalContainQuotation{
        margin-top: 10px;
        padding: 0 26px;
    }
    .titleModelQuotation{
        font-size: 16px;
        display: inline-block;
        margin-bottom: 8px;
        margin-top:10px;
    }
    .datepicker_dashboard{
        top: 29px !important;
        left: 120px !important;
        z-index: 9999;
    }
    #dateBankIn, .calendar{
        background: url("<?php echo JURI::root(); ?>components/com_enmasse/theme/dark_blue/images/text-arrow.jpg") no-repeat scroll 256px 0 rgba(0, 0, 0, 0);
        width: 95%;
    }
	
    .btModelQuotation{
        margin-top: 12px;
        text-align: center;
    }
    .btModelQuotation input{
        padding: 0 !important;
        font-size: 13px;
        height: 24px;
        width: 100px;
        background: url("<?php echo JURI::root(); ?>components/com_enmasse/theme/dark_blue/images/btn-login-2.png") repeat-x scroll 0 0 rgba(0, 0, 0, 0);
        border: 0 none;
    }
    .btModelQuotation input:hover{

    }
    .modalQuotation{
        margin-left: -8px;
        margin-top: -9px;
        width: 351px;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
    }
    .headerQuotaion{
        text-align: center;
        background-color: #F7F7F7;
        border-bottom: 1px solid #CDCDCD;
        padding-top: 1px;
        -webkit-border-top-left-radius: 7px;
        -webkit-border-top-right-radius: 3px;
        -moz-border-radius-topleft: 7px;
        -moz-border-radius-topright: 3px;
        border-top-left-radius: 7px;
        border-top-right-radius: 3px;
    }
    .headerQuotaion h3{
        border: medium none;
        color: #a11d21;
        font-family: Arial, Tahoma;
        font-size: 18px;
        font-weight: normal;
        line-height: 14px;
        margin-left: 0px;
        padding-bottom: 0;
        text-transform: uppercase;
		font-weight: bold;
    }
    .modalContainQuotation input{
        background-color: #FFFFFF;
        border: 1px solid #CCCCCC;
        -webkit-border-radius: 3px !important;
        -moz-border-radius: 3px !important;
        border-radius: 3px !important;
        height: 25px;
		margin-top: 7px;
    }
    .em_btn{
        -webkit-border-radius: 25px !important;
        -moz-border-radius: 25px !important;
        border-radius: 25px !important;
    }
</style>