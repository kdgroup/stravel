<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");

$oItems = $this->cart->getAll();


$nItemId = JFactory::getApplication()->getMenu()->getItems('link','index.php?option=com_enmasse&view=deallisting',true)->id;
$templateUrl = JURI::base()."templates/stravel/";
$templateUrlDeal = $templateUrl."images/deal/";
$catId = JRequest::getVar('categoryId', '');
$cityId = JRequest::getVar('cityId', '');
$itemId = JRequest::getVar('itemId', '');
$currentUrl = JURI::current();

$deal = $oItems[1]->item;
foreach ($oItems as $key => $value) {
    $deal = $value->item;
     $deal->count = $value->count;

}

$dealD->title= $deal->name;
$dealD->subTitle= $deal->subTitle;
$dealD->shortDes= $deal->short_desc;
$dealD->subTitle= $deal->subTitle;

$dealD->insurances = $deal->insuranceList;

$dealD->saved= (int)($deal->price*100/$deal->origin_price);
$dealD->saved= $dealD->saved."%";
$dealD->orginal= "$".(int)$deal->origin_price;
$dealD->price= "$".(int)$deal->price;

$dealD->highlight= $deal->highlight;
$dealD->terms= $deal->terms;

$dealD->Des1= $deal->description;
$dealD->Des2= $deal->description1;
$dealD->Des3= $deal->description2;
$dealD->Des4= $deal->description3;
$dealD->purchase= $deal->description3;
$imageUrl = JURI::root().str_replace("\\","/",$imageUrlArr[0]);
$dealD->image= '<img data-thumb="'.$imageUrl.'" src="'.$imageUrl.'" alt="'. $deal->name.'" />';
$dealD->url = 'index.php?option=com_enmasse&controller=shopping&task=addToCart&dealId='.$deal->id.'&slug_name='.$deal->slug_name.'&referralid='.JRequest::getVar('referralid').'&typeId=none'. '&Itemid=' . $oItem->id;

$dealD->cur_sold_qty =$deal->cur_sold_qty;
$TargetDate  =date('Y/m/d H:i:s', strtotime($deal->end_at));
$StartDate = date('Y/m/d H:i:s', strtotime($deal->start_at));
$dealD->daysRemain = (int)(strtotime($deal->end_at)-strtotime($deal->start_at))/(60*60*24);
$passengerInfo ="";
if($this->cart->passengerInfo){
  $passengerInfo = $this->cart->passengerInfo ;
}

?>
 <style>
.em_dealMain .bannergroup img{
    width: 100%;
}
.site #maincontent{background: none;background-color: #f3f3f4} 
</style>
<div class="em_innerBlock em_allDealPage container cart order-confirm">

	<h3 class="home-title-icon"><img src="<?php echo $templateUrlDeal.'deal-home.png';?>"/>團購優惠</h3>
    <div class="row flow-bar">
        <div class="col-sm-3  col-xs-6  text-center ">
            選擇產品
            <br/>
            <span>1</span>
        </div>
        <div class="col-sm-3  col-xs-6  text-center">
            核實訂單 
            <br/>
            <span>2</span>
        </div>
        <div class="col-sm-3  col-xs-6  text-center"> 
            輸入資料 
            <br/>
            <span>3</span>
        </div>
        <div class="col-sm-3  col-xs-6 active text-center"> 
            付款/成功訂購 
            <br/>
            <span>4</span>
        </div>
        <div class="line"></div>
        <div class="clear"></div>
    </div>
    <div class="row row-item">
    <div class="col-sm-4 pull-right status-right">
            <div class="deal-price-info right-content">
               
                <h3>套票價格</h3>
                    <p class="deal-price">
                    <span class="col-sm-6">套票金額: </span>
                    <span class="col-sm-6"><?php echo 'HK$'.$deal->price;?></span>
                     <span class="clear"></span>
                  </p>
                   <?php if($deal->deposit){?>
                        <p class="deal-price">
                            <span class="col-sm-6">訂金: </span>
                       <span class="col-sm-6"><?php echo '$'.((int)$deal->count * (int)$deal->deposit );?></span>
                         <span class="clear"></span>
                        </p> 
                    <?php }?> 
                 <p class="deal-price">
                        <span class="col-sm-6">購買數量: </span>
                    <span class="col-sm-6"><?php echo $deal->count;?></span>
                     <span class="clear"></span>
                    </p>
                    <div class="line"></div>
                     <?php if($this->cart->insuranceFee){?>
                    <p class="insurance-fee">
                        <span class="col-sm-6">保險費用: </span>
                    <span class="col-sm-6"><?php echo 'HK$'.$this->cart->insuranceFee;?></span>
                     <span class="clear"></span>
                    </p>
                    <?php if($deal->showinsurance) : ?>
                      <p class="insurance-count">成人 : <?php echo ($this->cart->insuranceAdult)?$this->cart->insuranceAdult:0;?> 小童 : <?php echo $this->cart->insuranceChild;?></p>
                    <?php endif; ?>
                   
                    <?php } ?>
                     <div class="line"></div>
                    <p class="discount-fee"><span class="col-sm-6">優惠代碼: </span><span class="col-sm-6"><?php 
                    if($this->cart->passengerInfo->discountObject){
                        $discountObject =$this->cart->passengerInfo->discountObject;
                        if($discountObject->type=='Percentage'){
                            echo '-HK$'.((int)$discountObject->discount_amount*$this->cart->getTotalPrice()/100);
                        }else{
                            if(($deal->price*$deal->count)<$discountObject->discount_amount){
                                echo '-HK$'.((int)($deal->price*$deal->count));
                            }else{
                                echo '-HK$'.((int)$discountObject->discount_amount);
                            }
                            
                        }
                        
                    }else{
                        echo '-HK$0';
                    }
                    ?></span>
                     <span class="clear"></span>
                    </p>

                    <div class="line"></div>
                    <?php if($deal->deposit){?>
                    <p class="total">
                    <span class="col-sm-6">餘額: </span>
                    <span class="col-sm-6"><?php echo '$'.((int)$this->cart->getTotalPrice() - (int)$deal->count * (int)$deal->deposit );?></span>
                         <span class="clear"></span>
                   </p>
                    <?php }?>
                    <p class="total">
                         <span class="col-sm-6">總金額: </span>
                    <span class="col-sm-6"><?php echo 'HK$'.((int)$this->cart->getTotalPrice());?></span>
                     <span class="clear"></span>
                    </p>
                    <p class="remark">(*以上價格未包各項稅款及附加費用)</p>
            
            </div>
        </div>
    	<div class="col-sm-8">
            <div class="left-content">
        		<h3 class="deal-name"><?php echo $dealD->title;?></h3>
        		<h3 class="deal-name"><?php echo $dealD->subTitle;?></h3>
                <?php 
                    if($deal->types && $deal->typeIDs){
                        foreach ($deal->orderTypes as $type) {
                            ?>
                                <div class="col-sm-8"><p><?php echo $type->name;?></p></div>
                                <div class="col-sm-4"><p><?php echo '數量 : '.$type->orderQty;?></p></div>
                                <?php
                          
                        }
                    }
                ?>
                <div class="line"></div>
        		<h3 class="deal-des"><img width ="25" src="<?php echo $templateUrlDeal;?>ProductInclude.png"/> 產品包括</h3>
        		<p><?php echo $dealD->Des1;?></p>
            </div>
    	</div>
    	
    </div>
    <?php if($dealD->Des2){?>
    <div class="row row-item">
        <div class="col-sm-4 pull-right plane-right">
            <div class="right-content">
                     <img  src="<?php echo $templateUrlDeal.'AirBlock.png';?>"/>
                </div>
        </div>
    	<div class="col-sm-8  ">
            <div class="left-content">
    		<p>航班詳情(只供參考)：</p>
    		
    		<p><?php echo $dealD->Des2;?></p>
            </div>
    	</div>
    	
    </div>
    <?php }?>
    <?php if($dealD->Des3){?>
    <div class="row row-item">
         <div class="col-sm-4  pull-right hotel-right">
            <div class="right-content">
                     <img  src="<?php echo $templateUrlDeal.'HotelBlock.png';?>"/>
                </div>
        </div>
    	<div class="col-sm-8">
    		<div class="left-content">
                <p>酒店詳情</p>
    		
    		  <p><?php echo $dealD->Des3;?></p>
            </div>
    	</div>
    	
    </div>
    <?php }?>
    <?php if($deal->showinsurance) : ?>
    <div class="row row-item">
        <div class="col-sm-4 pull-right insurance-right">
            <div class="right-content">
                     <img  src="<?php echo $templateUrlDeal.'ValueAddBlock.png';?>"/>
                </div>
        </div>
    	<div class="col-sm-8  insurance-info">
    		<div class="left-content">
            <p class="insurance"><span>保險</span> (<?php echo $deal->insurance_description;?>)</p>
            <p>受保日期:  <?php echo $deal->policy_date;?> </p>
            <p class="type-title">類別</p>
            <?php 
                    $insurancesList = json_decode($this->cart->insuranceJson);
                    if(count($insurancesList)){
                        foreach ($insurancesList as $key => $value) {
                            echo '<p>';
                            echo $value->insurance_name;
                            echo '&nbsp&nbsp&nbsp成人 : '; 
                            echo ($this->cart->insuranceAdult)?$this->cart->insuranceAdult:2;;
                            echo '&nbsp&nbsp小童 : '.$this->cart->insuranceChild;
                            echo '</p>';
                         }
                    }else{
                       
                           echo '<p>';
                            echo '沒有選購旅遊保險';
                            // echo '&nbsp&nbsp&nbsp成人 : ';
                            // echo ($this->cart->insuranceAdult)?$this->cart->insuranceAdult:2;;
                            // echo '&nbsp&nbsp小童 : '.$this->cart->insuranceChild;
                            echo '</p>';
                    }
                    
                
                   
                    ?>
            </div>
    	</div>
    	
    </div>
    <?php endif; ?>
   <!--  <div class="row row-item">
    	<div class="col-sm-12">
            <div class="  left-content">
    		<h3>第一位旅客</h3>
    		<p>
                <span>
                    <b>英文姓</b>: <?php echo $this->cart->passengerInfo->surname;?> 
                </span>
                <span>
                <b>英文名</b>: <?php echo $this->cart->passengerInfo->name;?> 
                </span>
                <span>
                <b>性別</b>: <?php echo $this->cart->passengerInfo->sex;?> 
                </span>
            </p>
            </div>
    	</div>
    </div>
    <div class="row row-item ">

    	<div class="col-sm-12 ">
         <div class="  left-content">
    		<h3>第二位旅客</h3>
            <p>
                <span>
                    <b>英文姓</b>: <?php echo $this->cart->passengerInfo->surname2;?> 
                </span>
                <span>
                <b>英文名</b>: <?php echo $this->cart->passengerInfo->name2;?> 
                </span>
                <span>
                <b>性別</b>: <?php echo $this->cart->passengerInfo->sex2;?> 
                </span>
            </p>
    		</div>
    	</div>
    </div> -->
     <div class="row row-item">
    	<div class="col-sm-12 ">
        <div class="  left-content">
    		<h3>聯絡人資料</h3>
            <p>
                <span>
                    <b>英文姓</b>: <?php echo $this->cart->passengerInfo->surname3;?> 
                </span>
                <span>
                <b>英文名</b>: <?php echo $this->cart->passengerInfo->name3;?> 
                </span>
                <span>
                
            </p>
            <p>
                <span>
                    <b>聯絡電話</b>: <?php echo $this->cart->passengerInfo->phone3;?> 
                </span>
                <span>
                <b>電郵</b>: <?php echo $this->cart->passengerInfo->email3;?> 
                </span>
                <span>
                
            </p>
    		
    		<p><b>優惠代碼</b>:<?php if($passengerInfo->discount){echo $passengerInfo->discount;}?></p>
    		<?php 
    			$paymentGate = "";
    			foreach ($this->payGtyList as $row) {
    				if($passengerInfo->payGtyId==$row->id){
    					$paymentGate = $row->name;
    				}
    			}
    		?>
    		<p><b>領取文件及完成餘額付款</b>: <?php echo $this->cart->branch;?></p>
    	</div>
        </div>
    </div>
    
		<form action='index.php' id="adminForm" name="adminForm"  class="form-validate" method="post" onSubmit="">
			
							
							<input type="hidden" class="ui-widget-content required" name="name" id="name" value="<?php echo $this->cart->passengerInfo->name3;?>" />
						
							
							<input type="hidden" class="ui-widget-content required" name="email" id="email" value="<?php echo $this->cart->passengerInfo->email3;?>" />
							<input type="hidden" class="ui-widget-content required" name="payGtyId"  value="<?php echo $passengerInfo->payGtyId;?>" />
						
				
							
							<input type="hidden" name="terms" id="terms" class="required" value="checked">  					
							   <input type="hidden" name="check" value="post" /> 
							   <input type="hidden" name="option" value="com_enmasse" /> 
							   <input type="hidden" name="controller" value="shopping" /> 
							   <input type="hidden" name="task" value="submitCheckOut" />
							   
					
			<div class="row">
                <div class="col-sm-12">
        			<p class="text-right " style="clear:both">
                        <a style="float:left" href="<?php echo JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$buy4friend&Itemid=$nItemId&step=2", false);?>">
                            <span class="back" style="marin-right:10px" ><img width="70" src="<?php echo $templateUrlDeal;?>PreviousPage.png"></span>
                        </a>
                         <a href="javascript:void(0);">
                              <span class="next" style="padding: 8px 0px 10px 10px;"><input style="padding: 8px 70px 10px 10px;" type="submit" class="submit" value="立即付款"/></span>
                        </a>
                        
                        
                    </p>
                </div>
            </div>
		</form>
</div>

<?php 
// print_r("<pre>");
// print_r($deal);

?>
<?php 
        

?>
<?php //print_r($this->payGtyList);?>