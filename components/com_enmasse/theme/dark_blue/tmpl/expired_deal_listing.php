<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse".DS."helpers". DS ."EnmasseHelper.class.php");

$theme =  EnmasseHelper::getThemeFromSetting();
//$enableRTL =  EnmasseHelper::getRTLFromSetting();
//if($enableRTL){
//	JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen_rtl.css');
//}
//JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen.css');

$oDefault = new JObject();
$oDefault->name = '';
$oDefault->id   = '';
//add an empty select option for location and category list
array_unshift($this->locationList, $oDefault);
array_unshift($this->categoryList, $oDefault);

JFactory::getDocument()->addScript('components/com_enmasse/theme/js/jquery/jquery-1.7.2.min.js');

$dealList = $this->dealList;
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

 <div class="em_wrapTit">
    <h3><?php echo JText::_('DEAL_LIST_EXPIRED_DEAL')?></h3>
     <div class="em_rss">
       <?php if( !($locationId = JRequest::getVar('locationId')) ):?>
               <a href="<?php echo JRoute::_('index.php?option=com_enmasse&controller=rss&task=listexpireddeal'); ?>"><img src="components/com_enmasse/theme/<?php echo $theme?>/images/rss.gif" alt="Expired Deals RSS Feed" title="RSS Feed"/></a>
       <?php else :?>
               <a href="<?php echo JRoute::_('index.php?option=com_enmasse&controller=rss&task=location&locationId=' . $locationId); ?>"><img src="components/com_enmasse/theme/<?php echo $theme?>/images/rss.gif" alt="Expired Deals RSS Feed" title="RSS Feed"/></a>
       <?php endif;?>
     </div>
</div>

<div class="em_innerBlock em_allDealPage">
    <form action="<?php echo JRoute::_('index.php')?>" id="em_frmListSearch" class="em_frmListSearch" name="em_frmListSearch">
            <input type="hidden" name="option" id="option" value="com_enmasse"/>
            <input type="hidden" name="controller" id="controller" value="deal"/>
            <input type="hidden" name="task" id="task" value="expiredlisting"/>
            <ul>
                <li>
                    <span><?php echo JText::_('DEAL_SEARCH_NAME');?>:</span>
                    <input type="text" name="keyword" size="20" style="width:100px" value="<?php echo $this->keyword; ?>"/>
                </li>
                <li>
                    <span><?php echo JText::_('DEAL_SEARCH_LOCATION');?>:</span>
                    <?php echo JHTML::_('select.genericList', $this->locationList, 'locationId', ' style=width:120px' , 'id', 'name', $this->locationId);?>
                </li>
                <li>
                    <span><?php echo JText::_('DEAL_SEARCH_CATEGORY');?>:</span>
                    <?php echo JHTML::_('select.genericList', $this->categoryList, 'categoryId', ' style=width:120px' , 'id', 'name', $this->categoryId);?>
                </li>
                <li>
                    <span>&nbsp;</span>
                    <select name="sortBy"  style='width:120px'>
                        <option value=""><?php echo JText::_('DEAL_LIST_OPTION_SORT_BY');?></option>
                        <option value="name" <?php if($this->sortBy=="name"){ echo " selected"; }?>><?php echo JText::_('DEAL_LIST_OPTION_NAME');?></option>
                        <option value="end_at" <?php if($this->sortBy=="end_at"){ echo " selected"; }?>><?php echo JText::_('DEAL_LIST_OPTION_END_DATE');?></option>
                        <option value="price" <?php if($this->sortBy=="price"){ echo " selected"; }?>><?php echo JText::_('DEAL_LIST_OPTION_PRICE');?></option>
                    </select>
                </li>
                <li><span>&nbsp;</span><input type="submit" class="em_btn" value="<?php echo JText::_('DEAL_LIST_SEARCH'); ?>"></li>
            </ul>
    </form>
    
    <div class="em_dealType row-fluid">
	<?php if(!count($dealList)):?>
		<h3><?php echo JText::_('DEAL_LIST_NO_DEAL_MESSAGE') ?></h3></div>
	<?php else:?>
            <?php
                    $oDeal = array_shift($dealList);
                    $nItemId = JFactory::getApplication()->getMenu()->getItems('link','index.php?option=com_enmasse&view=dealtoday',true)->id;

                    $link = 'index.php?option=com_enmasse&controller=deal&task=view&id=' . $oDeal->id ."&slug_name=" .$oDeal->slug_name ."&Itemid=$nItemId";
                    if (!EnmasseHelper::is_urlEncoded($oDeal->pic_dir)) {
                        $imageUrl = $oDeal->pic_dir;
                    } else {
                        $imageUrlArr = unserialize(urldecode($oDeal->pic_dir));
                        $imageUrl = str_replace("\\", "/", $imageUrlArr[0]);
                    }
            ?>
                
            <p class="em_imgBig span7"><a title="<?php echo $oDeal->name; ?>" href="<?php echo JRoute::_($link);?>"><img src="<?php echo $imageUrl?>" alt="<?php echo $oDeal->name?>" /></a></p><!--em_imgBig-->
            <dl class="span5">
                <dt>
                    <p><a href="<?php echo JRoute::_($link);?>" title="<?php echo $oDeal->name; ?>"><?php echo $oDeal->name; ?></a></p>
                    <strong><?php echo implode(", ", EnmasseHelper::getDealLocationNames($oDeal->id)); ?></strong>
                </dt>
                <dd><?php echo $oDeal->short_desc?></dd>
                <dd class="em_time">
                    <span><?php echo JText::_('START_AT');?> <?php echo DatetimeWrapper::getDisplayDatetime($oDeal->start_at)?></span>
                </dd>
                <dd><input name="" type="button" class="em_btn" value="<?php echo JText::_('DEAL_LIST_VIEW_THIS_DEAL')?>" onclick="window.location.href='<?php echo JRoute::_($link)?>'" /></dd>
            </dl>
    </div><!--em_dealType-->
             
    <div class="em_wrapItemDeal">
        <ul class="row-fluid">
        <?php foreach ($dealList as $i => $oDeal): ?>
                <?php
                $link = 'index.php?option=com_enmasse&controller=deal&task=view&id=' . $oDeal->id ."&slug_name=" .$oDeal->slug_name ."&Itemid=$nItemId";
                if (!EnmasseHelper::is_urlEncoded($oDeal->pic_dir)) {
                        $imageUrl = $oDeal->pic_dir;
                } else {
                        $imageUrlArr = unserialize(urldecode($oDeal->pic_dir));
                        $imageUrl = str_replace("\\", "/", $imageUrlArr[0]);
                }
                $sDealName = $oDeal->name;
                if(strlen($sDealName) > 30)
                {
                        $sDealName = substr($sDealName, 0, 30) ."...";
                }
        ?>
        <?php if($i > 0 && $i%2 == 0){echo '</ul><ul class="row-fluid">';} ?>
            <li class="span6">
                    <div class="em_itemDeal">
                    <a href="<?php echo JRoute::_($link);?>" title="" class="em_imgItem"><img alt="" src="<?php echo $imageUrl?>" ></a>
                    <a href="<?php echo JRoute::_($link);?>" title="<?php echo $oDeal->name?>" class="em_itemDeal_tit"><?php echo $sDealName?></a>
                    <p class="em_itemDealPrice">
                        <span><?php echo JText::_('DEAL_VALUE'); ?>: <strong><?php echo EnmasseHelper::displayCurrency($oDeal->origin_price) ?> </strong></span>
                        <span><?php echo JText::_('DEAL_PRICE'); ?>: <strong><?php echo EnmasseHelper::displayCurrency($oDeal->price) ?> </strong></span>
                    </p>
                    <span class="em_discount"><?php echo (100 - intval($oDeal->price / $oDeal->origin_price * 100)) ?>%</span>
                    <p class="em_line"><input name="" type="button" class="em_btn" value="<?php echo JText::_('DEAL_LIST_VIEW_THIS_DEAL')?>" onclick="window.location.href='<?php echo JRoute::_($link)?>'" /></p>
                </div><!--em_itemDeal-->
            </li>
        <?php endforeach; ?>
        </ul>
    </div><!--em_wrapItemDeal-->    
    <?php endif; ?>
</div>
