<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."DatetimeWrapper.class.php");
require_once(JPATH_SITE . DS . "components". DS ."com_enmasse". DS ."helpers". DS ."DealType.class.php"); 
$is_listing = '1';
$dealID = $this->deal->id;
require_once(JPATH_SITE . DS."components". DS ."com_enmasse".DS."theme".DS."dark_blue".DS."tmpl".DS."background_image.php");

JModelLegacy::getInstance('deal','enmasseModel')->addNumView($dealID);
//$oSession = JFactory::getSession();
//$numno = $oSession->get('aaaa');
////$oSession->set('aaaa', '0');
//echo $numno;die;
//if(!$numno){
//    $numno = 0;
//}
//$numno += 1;
//$oSession->set('aaaa', $numno);
//if($numno == '3'){
//    echo $numno;die;
//}

if($_GET['t']==1){
    include 'deal_detail2.php';
    return;
}
$juriRoot = JUri::root();
$theme =  EnmasseHelper::getThemeFromSetting();

//--------- add stylesheet and javascript

//$enableRTL =  EnmasseHelper::getRTLFromSetting();
//if($enableRTL){
//	JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen_rtl.css');
//}
//JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen.css');

//set default timezone
DatetimeWrapper::setTimezone(DatetimeWrapper::getTimezone());

$deal = $this->deal;
// print_r("<pre>");
// print_r($deal);die;
$merchant = $deal->merchant;
//------- to set the meta data and page title for SEO
$document = JFactory::getDocument();
$document->setMetadata('Keywords',  $deal->name);

$version = new JVersion;
$joomla = $version->getShortVersion();
if(substr($joomla,0,3) >= 1.6){
    $document   = JFactory::getDocument();
    $document->setTitle( $deal->name );
}else{
    $mainframe->setPageTitle($deal->name);
      
}

// load the deal image size
$dealImageSize = EnmasseHelper::getDealImageSize();
if(!empty($dealImageSize))
{
	$image_height = $dealImageSize->image_height;
	$image_width = $dealImageSize->image_width;
}
else
{
	$image_height = 252 ;
	$image_width = 400;
}

//contruct deal image url 
$imageUrlArr = array();
if(EnmasseHelper::is_urlEncoded($deal->pic_dir)){
	$imageUrlArr = unserialize(urldecode($deal->pic_dir));
}else{
	$imageUrlArr[0] = $deal->pic_dir;
}


//contruct data for social network sharing
$oMenu = JFactory::getApplication()->getMenu();
$oItem = $oMenu->getItems('link','index.php?option=com_enmasse&view=dealtoday',true);
$user = JFactory::getUser();
$userID = $user->get('id');
$shareName = $deal->name;
$shareUrl = JURI::base() . 'index.php?option=com_enmasse&controller=deal&task=view&id='. $deal->id . '&slug_name=' . $deal->slug_name . '&Itemid=' . $oItem->id;
if($userID!='0')
{
	$shareUrl .= '&referralid='.$userID;
}
$shareShortDesc = str_replace("\"", "'", $deal->short_desc);
$shareImages = JURI::base(). str_replace("\\","/",$imageUrlArr[0]);
$logoMerchant = array();
if(EnmasseHelper::is_urlEncoded($merchant->logo_url)){
	$logoMerchant = unserialize(urldecode($merchant->logo_url));
}else{
	$logoMerchant[0] = $merchant->logo_url;
}
$logoImages = JURI::base(). str_replace("\\","/",$logoMerchant[0]);
if(!$logoMerchant[0]){
    $logoImages = '';
}
if (($deal->tier_pricing)==1)
{
	$deal->price = EnmasseHelper::getCurrentPriceStep(unserialize($deal->price_step),$deal->price,$deal->cur_sold_qty);
	$deal->types =NULL;
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" >DD_roundies.addRule ('.descrip', '2px', true );</script>
<?php if(count($merchant->branches)>0):?>
	<?php JFactory::getDocument()->addScript("http://maps.google.com/maps/api/js?sensor=true");?>
	<script language="javascript">
		jQuery(document).ready( function(){
			initialize();
		})
	</script>
<?php endif;?>


  

<?php

$nItemId = JFactory::getApplication()->getMenu()->getItems('link','index.php?option=com_enmasse&view=deallisting',true)->id;
$templateUrl = JURI::base()."templates/stravel/";
$templateUrlDeal = $templateUrl."images/deal/";
$catId = JRequest::getVar('categoryId', '');
$cityId = JRequest::getVar('cityId', '');
$itemId = JRequest::getVar('itemId', '');
$currentUrl = JURI::current();


$dealD->title= $deal->name;
$dealD->subTitle= $deal->subTitle;
$dealD->shortDes= $deal->short_desc;
$dealD->subTitle= $deal->subTitle;

$dealD->saved=  100 - intval($deal->price / $deal->origin_price * 100);//(int)($deal->price*100/$deal->origin_price);
$dealD->saved= $dealD->saved."%";
$dealD->orginal= "$".(int)$deal->origin_price;
$dealD->price= "$".(int)$deal->price;

$dealD->highlight= $deal->highlight;
$dealD->terms= $deal->terms;

$dealD->Des1= $deal->description;
$dealD->Des2= $deal->description1;
$dealD->Des3= $deal->description2;
$dealD->Des4= $deal->description3;
$dealD->purchase= $deal->description3;
$imageUrl = JURI::root().str_replace("\\","/",$imageUrlArr[0]);
$dealD->image= '<img data-thumb="'.$imageUrl.'" src="'.$imageUrl.'" alt="'. $deal->name.'" />';
$dealD->url = 'index.php?option=com_enmasse&controller=shopping&task=addToCart&dealId='.$deal->id.'&slug_name='.$deal->slug_name.'&referralid='.JRequest::getVar('referralid').'&typeId=none'. '&Itemid=' . $oItem->id;
$form_url = JURI::base() . 'index.php?option=com_enmasse&controller=shopping&task=addToCart&dealId='.$deal->id.'&slug_name='.$deal->slug_name.'&referralid='.JRequest::getVar('referralid').''. '&Itemid=' . $oItem->id;
$dealD->cur_sold_qty =$deal->cur_sold_qty;
$TargetDate  =date('Y/m/d H:i:s', strtotime($deal->end_at));
$StartDate = date('Y/m/d H:i:s', strtotime($deal->start_at));
$dealD->daysRemain = (int)((strtotime($deal->end_at)-time())/(60*60*24));


?>
 <style>
.em_dealMain .bannergroup img{
    width: 100%;
}
.site #maincontent{background: none;background-color: #f3f3f4} 
</style>
<div class="em_innerBlock em_allDealPage container">
    
    <h3 class="home-title-icon"><img src="<?php echo $templateUrlDeal.'deal-home.png';?>"/>團購優惠</h3>
    <div class="row flow-bar">
        <div class="col-sm-3 col-xs-6 active text-center ">
            選擇產品
            <br/>
            <span>1</span>
        </div>
        <div class="col-sm-3 col-xs-6 text-center">
            核實訂單 
            <br/>
            <span>2</span>
        </div>
        <div class="col-sm-3 col-xs-6 text-center"> 
            輸入資料 
            <br/>
            <span>3</span>
        </div>
        <div class="col-sm-3 col-xs-6 text-center"> 
            付款/成功訂購 
            <br/>
            <span>4</span>
        </div>
        <div class="line"></div>
        <div class="clear"></div>
    </div>
    <div class=" deal-detail">
        <div class="deal-top">
           
           <div class=" col-sm-7">
           <div class="detail-info">
                <div class="col-sm-8">
                    <h2 class="title"> <?php echo $dealD->title;?></h2>
                    <?php 
                        if($dealD->subTitle){
                            ?>
                             <h3 class="sub-title"> <?php echo $dealD->subTitle;?></h3>
                            <?php
                        }
                    ?>
                   
                </div>
                
                <div class="col-sm-4">
                    <span class="price">
                        <?php echo $dealD->price;?>
                    </span>
                </div>
                <div class="clear"></div>
            </div>
            <div class="orginal-price row">
                <?php //if($dealD->saved!='0%'){?>
                <div class="saved col-sm-3">
                    <span class="circle-saved">慳</span>
                    <?php echo $dealD->saved;?>
                </div>
                <?php //}?>
                <?php if($deal->deposit){?>
                  <div class="orginal col-sm-2">
                    原價 
                    <span><?php echo $dealD->orginal;?></span>
                </div>
                <div class="no-purchased  col-sm-2">
                    訂金 
                    <span><?php echo $deal->deposit;?></span>
                </div>
                <div class="no-purchased col-sm-2" style="padding:0px;">
                    已購買人數 
                    <span><?php echo $dealD->cur_sold_qty;?></span>
                </div>  
                 <?php   }else{
                    ?>
                    <div class="orginal col-sm-3">
                    原價 
                    <span><?php echo $dealD->orginal;?></span>
                </div>
                <div class="no-purchased col-sm-3">
                    已購買人數 
                    <span><?php echo $dealD->cur_sold_qty;?></span>
                </div>  
                    <?php }?>
                
                <?php
                        $default = 1;
                        $min_item_per_order = $deal->min_item_per_order;
                        if(!$min_item_per_order||$min_item_per_order==0) {
                            $min_item_per_order = 1;
                        }
                        if($deal->quantity_odd==0 && $min_item_per_order==1){
                            $min_item_per_order=2;
                        }
                        $total = $deal->max_coupon_qty - $deal->cur_sold_qty;
                        $max =  $deal->max_buy_qty;
                        $max_item_per_order =  $deal->max_item_per_order;
                        if(!$max_item_per_order||$max_item_per_order==0){
                            $max_item_per_order=99999;
                        }
                        if($max_item_per_order<$max){
                            $max=$max_item_per_order;
                        }

                        if($total>=$max){
                            $default=$max;
                        }else{
                            $default =$total;
                        }
                        $soldOut = 0;
                        if($default < $min_item_per_order){
                            $soldOut=1;
                        }
                        if($default==1 && $deal->quantity_odd==0){
                             $soldOut=1;
                        }
                        if($default==0){
                            $soldOut=1;
                        }

                        $typeDefault = 0;
                        $typeQtyDefault =0;
                    ?>
                <div class="price-info col-sm-3">
                    <a href="javascript:void(0);" id="show-type-pop" class="show-pop-up">
                    <img onclick="$('#popupContBuynormal').show();" width ="15" src="<?php echo $templateUrlDeal;?>PriceInfo.png"/> 選擇優惠項目  
                     </a>
                     <?php
                if($deal->types) 
                {
            ?>
                <div class="pop-up hide" >
                    <div class="innerPopContBuy">
                        <img class="arrow" src="<?php echo $templateUrlDeal;?>pop-arrow2.png"/>
                            
                            <h3 style="float:left;"><?php echo JText::_('CHOOSE_YOUR_DEAL') ?></h3>
                            <h3 style="float:right;"><?php echo JText::_('CHOOSE_YOUR_QTY') ?></h3>
                            <form action="<?php echo $form_url;?>" name="addCart" id="addCart" method="post">
                                
                            
                                <table class="type-table" style="clear:both"cellspacing="0" cellpadding="0" border="0">
                                    <tbody>

                                    <?php
                                        $k==0;
                                        $l==0;
                                            foreach($deal->types as $type)
                                            {
                                                    if($class_tr == '')
                                                            $class_tr = 'bgBlueL';
                                                    else
                                                            $class_tr = '';

                                                        $k++;
                                                        $style="border-bottom:1px solid;";
                                                         if($k % 4 == 1){
                                                            $style="border-bottom:1px solid;border-top:1px solid;";
                                                              $l++;
                                                        }
                                                        
                                    ?>
                                            <tr class="<?php echo($class_tr).' page-tr page-'.(int)($l-1); ?> type-<?php echo $type->id;?>" rel="<?php echo($type->name); ?> <?php echo '$'.(int)($type->price);?> <?php echo JText::_('CHOOSE_YOUR_QTY') ?>:" rels="<?php echo($type->name); ?> <?php echo '$'.(int)($type->price);?>" style="<?php echo $style;?>">
                                                    <td><input  <?php if(($type->qty-$type->sold_qty)<=0){ echo 'disabled="disabled"';}?> name="type[]" type="checkbox" value="<?php echo $type->id;?>"/></td>
                                                    <td width="335">
                                                        <?php 
                                                        $url = 'index.php?option=com_enmasse&controller=shopping&task=addToCart&dealId='.$deal->id .'&slug_name=' .$deal->slug_name.'&referralid='.JRequest::getVar('referralid').'&typeId='.$type->id.'&Itemid='.$oItem->id;
                                                        $url = 'javascript:void(0);';
                                                        ?>
                                                            <p><?php echo JText::_('TYPE_NO')." ".$k." : ";?><?php echo($type->name); ?></p>
                                                            <p class="wrapDetail">
                                                                
                                                                    <span class="txtValue light-yellow">
                                                                    
                                                                    <?php echo JText::_('DEAL_TYPE_VALUE') ?>  
                                                                    <?php echo '$'.(int)($type->origin_price); ?>
                                                                    &nbsp;
                                                                    
                                                                    <?php echo (int)(DealType::percent($type->origin_price,$type->price)); ?>%<?php echo JText::_('DEAL_TYPE_OFF') ?>
                                                                    
                                                                    &nbsp;
                                                                   
                                                                    <?php echo JText::_('DEAL_TYPE_SAVE') ?><?php echo '$'.(int)(DealType::minus($type->origin_price,$type->price)); ?>
                                                                    
                                                                    </span>
                                                               
                                                            </p><!--wrapDetail-->
                                                    </td>
                                                    <td class="text-right">
                                                        <span class="light-yellow">
                                                            <?php echo JText::_('DEAL_TYPE_PRICE') ?>
                                                            <br/>
                                                            <?php echo '$'.(int)($type->price);?>
                                                        </span>
                                                    </td>
                                                    <td class="last text-right" <?php if(($type->qty-$type->sold_qty)<=0){echo 'style="width:65px"';}?>>
                                                    <?php if(($type->qty-$type->sold_qty)>0){

                                                        if($typeQtyDefault==0){
                                                            $typeQtyDefault = $type->qty-$type->sold_qty;
                                                            $typeDefault = $type->id;
                                                        }

                                                        ?>
                                                    <p>
                                                        <input type="hidden" name="url<?php echo $type->id;?>" id="url<?php echo $type->id;?>" value="<?php echo $url; ?>">
                                                         <select style="color:#000;" name="typeQty[<?php echo $type->id;?>]" class="type-change" rel="<?php echo $type->id;?>" id="type-<?php echo $type->id;?>">
                                                            <?php for ($i=$min_item_per_order; $i <= ($type->qty-$type->sold_qty); $i++) { 
                                                                 if($deal->quantity_odd==0){
                                                                    if($i%2==1 ){
                                                                        continue;
                                                                    }
                                                                }
                                                                ?>
                                                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                <?php
                                                            }?>
                                                        </select>
                                                    </p>
                                                    <?php }else{
                                                        echo '<span>售&nbsp;&nbsp;罄</span>';
                                                        // echo '<span>售&nbsp;罄'.JText::_('SOLD_OUT').'</span>';
                                                    }?>
                                                    </td>
                                            </tr>
                                    <?php
                                            }
                                    ?>
                                    <tr class='<?php echo($class_tr); ?>'>
                                        <td colspan="4" class="deal-intro">
                                          <?php 
                                                if($k>4){
                                                    // $pages = (int)($k/4);
                                                    for ($i=0; $i < $l; $i++) { 
                                                        echo '<a href="javascript:void(0)" class="page-item " rel ="'.$i.'">'.($i+1).'</a>';
                                                    }
                                                }
                                            ?>
                                            <a class="submit-type" style="width:110px;float:right;" href="javascript:void(0);"> <span  style="margin-top:10px;" class="order-button">確認資料</span></a> 
                                        </td>
                                    </tr>
                                    </tbody></table>
                            </form>   
                        </div><!--innerPopContBuy-->
                    </div>
                
            <?php } ?>
                </div>
                <div class="clear"></div>
            </div>
            <div class="include">
                <h3> <img width ="15" src="<?php echo $templateUrlDeal;?>ProductInclude.png"/> 產品包括</h3>
                <?php 
                    if($dealD->shortDes){
                         echo $dealD->shortDes;
                    }
                ?>
            </div>
             <div class="include types-selected" style="display:none;">
                <h3> <img width ="15" src="<?php echo $templateUrlDeal;?>arrowicon.jpg"/> <?php echo JText::_('DEAL_TYPE_SELECTED') ?> </h3>
                <ol>
                    
                </ol>
                 <div class="clear more-types deal-intro text-" style="position:relative;display:none;">
                    <a href="javascript:void(0);" class="show-pop-up" style="color:#39B54A;margin-left:25px;">
                    
                    更多...
                    </a>
                        <div class=" pop-up types-selected-pop hide text-left" style="left: 65px; background-color: #7a7a7a;  margin-left: 15px;">
                            <img class="arrow" src="<?php echo $templateUrlDeal;?>pop-arrow3.png"/>
                            <div class="type-header" style="border-bottom:1px solid">
                                <?php echo JText::_('DEAL_TYPE_SELECTED') ?>
                                <span class="pull-right"> <?php echo JText::_('CHOOSE_YOUR_QTY') ?></span>
                                <div class="clear"></div>
                            </div>
                            <ol>
                                <li class="clear">Type1 $0909303 <span class="pull-right">1</span></li>
                                <li class="clear">Type1 $0909303 <span class="pull-right">1</span></li>
                                <li class="clear">Type1 $0909303 <span class="pull-right">1</span></li>
                                <li class="clear">Type1 $0909303 <span class="pull-right">1</span></li>
                                <li class="clear">Type1 $0909303 <span class="pull-right">1</span></li>
                            </ol>
                        </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="deal-intro row">
                <div class="col-sm-3">
                <a href="javascript:void(0);" class="show-pop-up">
                <img  width ="15" src="<?php echo $templateUrlDeal;?>Terms.png"/>    
                細則及條款
                </a>
                    <div class="pop-up terms hide">
                        <img class="arrow" src="<?php echo $templateUrlDeal;?>pop-arrow.png"/>
                        <?php 
                            if($dealD->terms){
                                 echo $dealD->terms;
                                 
                            }
                            echo '<p><a target="_blank" href="'.$deal->master_term_url.'" title="'.$deal->master_term.'">'.$deal->master_term.'</a></p>';
                        ?>
                    </div>
                </div>
                <div class="col-sm-3">
                <a href="javascript:void(0);" class="show-pop-up">
                    <img  width ="15" src="<?php echo $templateUrlDeal;?>PriceInfo.png"/>
                     價格說明 
                </a>
                
                    <div class="pop-up highlight hide">
                        <img class="arrow" src="<?php echo $templateUrlDeal;?>pop-arrow.png"/>
                        <?php 
                            if($deal->price_description){
                                 echo $deal->price_description;
                            }
                        ?>
                    </div>
                </div>
                <div class="col-sm-3" style="position:relative;color:#808285;padding:0px">
                    <?php if($typeDefault){
                        echo '<p><a href="javascript:void(0)" class="show-types" title="購買數量">購買數量</a></p>';
                        }else{?>
                    <p>購買數量: 
                        <select name="qty" id="qtyVal">
                            <?php 
                            $selected ="";
                            for ($i=$min_item_per_order; $i <= $default; $i++) { 

                                if($deal->quantity_odd==0){
                                    if($i%2==1 ){
                                        continue;
                                    }
                                }
                                ?>
                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                <?php
                            }?>
                        </select>
                   
                    </p>
                    <?php }?>
                    <input type="hidden" value="<?php echo $dealD->url;?>" name="url" id="urlVal">
                    <input type="hidden" value="<?php echo $max;?>" name="maxOrder" id="maxOrder">
                    <input type="hidden" value="<?php echo $total;?>" name="max" id="max">
                    <input type="hidden" value="<?php echo $min_item_per_order;?>" name="min_item_per_order" id="min_item_per_order">
                    <input type="hidden" value="<?php echo $default;?>" name="default" id="default">
                    <input type="hidden" value="<?php echo $soldout;?>" name="soldout" id="soldout">

                    <input type="hidden" value="<?php echo JText::_('本團購產品已售罄');?>" name="sold-out-message" id="sold-out-message">

                </div>
                <div class="col-sm-3"> <a class="order-url" href="javascript:void(0);"> <span class="order-button">立即訂購</span></a>   </div>
                <div class="clear"></div>
            </div> 
            </div>
            <div class="deal-image col-sm-5">
               <?php echo $dealD->image;?>
                <?php if($dealD->daysRemain<=14){?>
               <span class="days-remain text-center <?php if($dealD->daysRemain<=1) echo 'last-day';?>"><?php if($dealD->daysRemain>1) echo $dealD->daysRemain;?></span>
                <?php }?>
           </div>
            <div class="clear"></div>
        </div>
        
        <div class="deal-description">
            <div class="description"> <h3 class="title">產品說明</h3></div>
               
                 <?php 
                    if($dealD->Des1){
                        ?><div class="description"><p><?php
                         echo $dealD->Des1;
                         ?></p></div><?php
                    }
                ?>
                 <?php 
                    if($dealD->Des2){
                        ?><div class="description"><p><?php
                         echo $dealD->Des2;
                         ?></p></div><?php
                    }
                ?>
                 <?php 
                    if($dealD->Des3){
                        ?><div class="description"><p><?php
                         echo $dealD->Des3;
                         ?></p></div><?php
                    }
                ?>
        </div>

        <div class="deal-purchase">
            <h3>預訂方法</h3>
            <div class="purchase">
                <?php 
                    if($dealD->purchase){
                       
                         echo $dealD->purchase;
                         
                    }
                ?>
            </div>
        </div>

        <div class="text-right">
            <p>
                <a href="javascript:void(0);" onclick="window.history.back();">
                    <span class="back"><img width="70" style="marin-right:10px" src="<?php echo $templateUrlDeal;?>PreviousPage.png"></span>
                </a>
                <a  class="order-url"  href="javascript:void(0);">
                    <span class="next">立即訂購</span>
                </a>
                
                
            </p>
        </div>
        
    </div>
</div>
<?php //print_r("<pre>");?>
<?php //print_r($this->deal);?>
