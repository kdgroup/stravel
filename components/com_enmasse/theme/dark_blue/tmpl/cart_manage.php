<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");

$oItems = $this->cart->getAll();


$nItemId = JFactory::getApplication()->getMenu()->getItems('link','index.php?option=com_enmasse&view=deallisting',true)->id;
$templateUrl = JURI::base()."templates/stravel/";
$templateUrlDeal = $templateUrl."images/deal/";
$catId = JRequest::getVar('categoryId', '');
$cityId = JRequest::getVar('cityId', '');
$itemId = JRequest::getVar('itemId', '');
$currentUrl = JURI::current();

$deal = $oItems[1]->item;
foreach ($oItems as $key => $value) {
    $deal = $value->item;
    $deal->count = $value->count;
}
// print_r('<pre>');
// print_r($oItems);die;

$dealD->title= $deal->name;
$dealD->subTitle= $deal->subTitle;
$dealD->shortDes= $deal->short_desc;
$dealD->subTitle= $deal->subTitle;

$dealD->insurances = $deal->insuranceList;

$dealD->saved= (int)($deal->price*100/$deal->origin_price);
$dealD->saved= $dealD->saved."%";
$dealD->orginal= "$".(int)$deal->origin_price;
$dealD->price= "$".(int)$deal->price;

$dealD->highlight= $deal->highlight;
$dealD->terms= $deal->terms;

$dealD->Des1= $deal->description;
$dealD->Des2= $deal->description1;
$dealD->Des3= $deal->description2;
$dealD->Des4= $deal->description3;
$dealD->purchase= $deal->description3;
$imageUrl = JURI::root().str_replace("\\","/",$imageUrlArr[0]);
$dealD->image= '<img data-thumb="'.$imageUrl.'" src="'.$imageUrl.'" alt="'. $deal->name.'" />';
$dealD->url = 'index.php?option=com_enmasse&controller=shopping&task=addToCart&dealId='.$deal->id.'&slug_name='.$deal->slug_name.'&referralid='.JRequest::getVar('referralid').'&typeId=none'. '&Itemid=' . $oItem->id;

$dealD->cur_sold_qty =$deal->cur_sold_qty;
$TargetDate  =date('Y/m/d H:i:s', strtotime($deal->end_at));
$StartDate = date('Y/m/d H:i:s', strtotime($deal->start_at));
$dealD->daysRemain = (int)(strtotime($deal->end_at)-strtotime($deal->start_at))/(60*60*24);


?>
 <style>
.em_dealMain .bannergroup img{
    width: 100%;
}
.site #maincontent{background: none;background-color: #f3f3f4} 
</style>
<div class="em_innerBlock em_allDealPage container cart">
<form action="index.php" id="changeItem" method="post" name="changeItem" class="form-validate" style="margin: 0px;">
    <h3 class="home-title-icon"><img src="<?php echo $templateUrlDeal.'deal-home.png';?>"/>團購優惠</h3>
    <div class="row flow-bar">
        <div class="col-sm-3 col-xs-6 text-center ">
            選擇產品
            <br/>
            <span>1</span>
        </div>
        <div class="col-sm-3  col-xs-6 active text-center">
            核實訂單 
            <br/>
            <span>2</span>
        </div>
        <div class="col-sm-3  col-xs-6 text-center"> 
            輸入資料 
            <br/>
            <span>3</span>
        </div>
        <div class="col-sm-3  col-xs-6 text-center"> 
            付款/成功訂購 
            <br/>
            <span>4</span>
        </div>
        <div class="line"></div>
        <div class="clear"></div>
    </div>
     <div class=" insurance section">
        
        <div class="right-content">
           <div class="row"> 
           <p class="col-sm-6">團購產品名稱 : <?php echo $deal->name;?></p>
            <p class="col-sm-6 text-right">購買數量 : <?php echo $deal->count;?></p>
             <?php 
                    if($deal->types && $deal->typeIDs){
                        foreach ($deal->orderTypes as $type) {
                            ?>
                                <div class="col-sm-8"><p><?php echo $type->name;?></p></div>
                                <div class="col-sm-4 text-right"><p><?php echo '數量 : '.$type->orderQty;?></p></div>
                                <?php
                          
                        }
                    }
                ?>
            </div>
        </div>
        
    </div>
    <?php  if($deal->description1){?>
    <div class=" air-plane section">
        <div class="pull-left left-content">
            <img width ="147" height="65" src="<?php echo $templateUrlDeal.'AirBlock1.png';?>"/>
        </div>
        <div class="right-content">
            <p><?php echo $deal->description1;?></p>
        </div>
        <div class="clear"></div>
    </div>
    <?php }?>
    <?php  if($deal->description2){?>
    <div class=" hotel section">
        <div class="pull-left left-content">
            <img width ="147" height="65" src="<?php echo $templateUrlDeal.'HotelBlock1.png';?>"/>
        </div>
        <div class="right-content">
            <p><?php echo $deal->description2;?></p>
        </div>
        <div class="clear"></div>
    </div>
    <?php }?>
    <?php if($dealD->insurances && !empty($deal->showinsurance)){?>
    <div class=" insurance section rdr">
        <div class="pull-left left-content">
            <img width ="147" height="65" src="<?php echo $templateUrlDeal.'ValueAddBlock1.png';?>"/>
        </div>
        <div class="right-content">
            <p class="insurance"><span>保險</span> (<?php echo $deal->insurance_description;?>)</p>
            <p>受保日期:  <?php echo $deal->policy_date;?> </p>
            <!-- <p><?php echo $dealD->Des4;?></p> -->
            <?php if($dealD->insurances){?>
        
                 <table cellpadding="0" cellspacing="0" border="0" width="100%" class="insuranceList">
                     <tr class=" insurance-title">
                        <th>類別</th>
                        <th>價格</th>
                        <th>優惠價</th>
                        <th></th>
                        <th>選擇</th>
                        <th>&nbsp;</th>
                    </tr>
                     <?php foreach ($dealD->insurances as $insurance) {
                    ?>
                        <tr class="insurance-item">
                            <td><?php echo $insurance->insurance_name;?></td>
                            <td>成人<?php echo "$".(int)$insurance->adult_price;?>小童<?php echo "$".(int)$insurance->child_price;?></td>
                            <td>成人<?php echo "$".(int)$insurance->adult_offer_price;?>小童<?php echo "$".(int)$insurance->child_offer_price;?></td>
                            <td align="right" class="" style="text-align:right;"> 
                                <div class="hide pop-<?php echo $insurance->id?> pop-up item-<?php echo $insurance->id?>">
                                    <label>成人</label>
                                    <input type="text" name="adult-<?php echo $insurance->id?>" id="adult-<?php echo $insurance->id?>" value="0"/>
                                    
                                    <label>小童</label>
                                    <input type="text" name="child-<?php echo $insurance->id?>" id="child-<?php echo $insurance->id?>" value="0"/>
                                 </div>
                            </td>
                            <td>
                                <a href="javascript:void(0)" class="show-cart" rel="<?php echo $insurance->id?>">
                                    <img width="22" src="<?php echo $templateUrlDeal;?>Cart.png">
                                </a>
                               
                            </td>
                            
                        </tr>
                        
                        <?php
                     }?>
                </table>
            <?php } ?>
            

           
        </div>
        <div class="clear"></div>
    </div>
    <?php }?>
     
     <div class="text-right ">

            <p class="text-right">
                <?php
                    $backLink = 'index.php?option=com_enmasse&controller=deal&task=view&id=' . $deal->id ."&slug_name=" .$deal->slug_name ."&Itemid=$nItemId";

                ?>
                <a href="<?php echo JRoute::_($backLink);?>">
                    <span class="back" style="margin-right:20px" ><img width="70" src="<?php echo $templateUrlDeal;?>PreviousPage.png"></span>
                </a>
                <a class="seousfhsoe" href="javascript:void(0);" onclick="javascript:document.changeItem.submit();">
                    <span class="next">確認訂購</span>
                </a>
                
                
            </p>
        </div>
            <input type="hidden" value="0" id="deleteItem" name="deleteItem">
                <input type="hidden" value="1" name="itemId">
                <input type="hidden" value="com_enmasse" name="option">
                <input type="hidden" value="shopping" name="controller">
                <input type="hidden" value="changeItem" name="task">
                <input type="hidden" value="0" name="buy4friend">
                <input style="min-width: 100px; margin: 0px;display:none;" type="button" class="em_btn" value="<?php echo JText::_('UPDATE_BUTTON');?>" onclick="javascript:document.changeItem.submit();"></input>
                 <?php
            foreach($oItems as $oCartItem){
                $item = $oCartItem->getItem();
                
                $deal = JModelLegacy::getInstance('deal','enmasseModel')->getById($item->id);

                $imageUrlArr = array();
                if(EnmasseHelper::is_urlEncoded($deal->pic_dir)){
                        $imageUrlArr = unserialize(urldecode($deal->pic_dir));
                }else{
                        $imageUrlArr[0] = $deal->pic_dir;
                }
                //tier pricing
                if (($item->tier_pricing)==1)
                {   
                        $item->price = EnmasseHelper::getCurrentPriceStep(unserialize($item->price_step),$deal->price,$item->cur_sold_qty);;
                        $item->types =NULL;
                        $calculatorPrice = EnmasseHelper::getCalculatorPrice(unserialize($item->price_step),$item->price,$item->cur_sold_qty,$oCartItem->getCount(),$deal->price);
                        $printOut = "";
                        $num_step_price = 0;
                        foreach($calculatorPrice as $k => $v)
                        {
                                if($k != 'newPrice' && $k != 'totalPrice')
                                {
                                    if($num_step_price > 0){
                                        $printOut.= "+(".$v."x".$k.")";
                                    } else {
                                        $printOut.= "(".$v."x".$k.")";
                                    }
                                    $num_step_price++;
                                }
                        }
                        $totalPrice = $calculatorPrice['totalPrice'];
                }
                //$item_price = $item->price;
            ?>
             <div class="clear"></div>  
            <div style="display:none;">
                        <input type="text" class="required validate-numeric" value="<?php echo $oCartItem->getCount();?>" name="value<?php echo $item->id; ?>" id="value" size="1px">
                  
                        <span><strong><?php if (($item->tier_pricing)==1){echo EnmasseHelper::displayCurrency($totalPrice);}else{echo EnmasseHelper::displayCurrency($oCartItem->getCount()*$item->price);} ?></strong></span>
                   
                    <span title="Delete" class="em_btnDetele" onclick="javascript:document.getElementById('deleteItem').value=<?php echo  $item->id; ?>;document.changeItem.submit();"><img alt="" src="components/com_enmasse/theme/dark_blue/images/dummy.gif" /></span></td>
               </div>
            <?php } ?>
         
                   <dd style="font-size:1.1em;display:none;"><span><?php echo JText::_('SHOP_CARD_GRAND_TOTAL');?>:</span> <span><?php echo EnmasseHelper::displayCurrency($this->cart->getAmountToPay());?></span></dd> 
                  

           
    </form>
</div>
