<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
------------------------------------------------------------------------- */
// No direct access 
defined('_JEXEC') or die('Restricted access');
include_once JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "views" . DS . "headerMenu" . DS
. "menuReport.php";
$orderItemList = $this->orderItemList;
$emptyJOpt = JHTML::_('select.option', '', JText::_('DEAL_COUPON_SELECTION_MSG'));
$allItems = JHTML::_('select.option', '100009', JText::_('All Items'));
$dealJOptList = array();
array_push($dealJOptList, $emptyJOpt);
array_push($dealJOptList, $allItems);
foreach ($this->dealList as $item) {
	$var = JHTML::_('select.option', $item->id, $item->deal_code." - ".JText::_($item->name));
	array_push($dealJOptList, $var);
}
JHtml::_('behavior.framework');
JHTML::_('behavior.tooltip');
// JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Locale.en-US.DatePicker.js");
// JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.js");
// JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.Attach.js");
// JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.Date.js");
// JFactory::getDocument()->addStyleSheet("components/com_enmasse/script/datepicker/datepicker_dashboard/datepicker_dashboard.css");
$user = JFactory::getUser();

$link = JRoute::_("index.php?option=com_enmasse&controller=salereports&task=updateOrder", false);
$linkGenerate = "index.php?option=com_enmasse&controller=salereports&task=generateReport";

if(JRequest::getVar('deal_id',"")){
	$linkGenerate.="&deal_id=".JRequest::getVar('deal_id',"");
}
if(JRequest::getVar('from_at',"")){
	$linkGenerate.="&from_at=".JRequest::getVar('from_at',"");
}
if(JRequest::getVar('from_to',"")){
	$linkGenerate.="&from_to=".JRequest::getVar('from_to',"");
}
if(JRequest::getVar('type',"")){
	$linkGenerate.="&type=".JRequest::getVar('type',"");
}
if(JRequest::getVar('branch',"")){
	$linkGenerate.="&branch=".JRequest::getVar('branch',"");
}
$linkGenerate = JRoute::_($linkGenerate, false);
// $this->orderItemList = array_values($sort);

?>
<?php
				$typeJOptList = array();
				array_push($typeJOptList, JHTML::_('select.option', null, JText::_('--Selected--')));
				array_push($typeJOptList, JHTML::_('select.option', 'Used', JText::_('Used')));
				array_push($typeJOptList, JHTML::_('select.option', 'Taken', JText::_('Taken')));
				array_push($typeJOptList, JHTML::_('select.option', 'Refunded', JText::_('REFUNDED')));
				array_push($typeJOptList, JHTML::_('select.option', 'Unused', JText::_('TAKEN_WITH_REASON')));
                // array_push($typeJOptList, JHTML::_('select.option', 'Refunded', JText::_('Refunded')));
				
				?>
<input type="hidden" name="link" id="link" value="<?php echo $link;?>"/>
<input type="hidden" name="refunded" id="refunded-text" value="<?php echo JTEXT::_('COUPON_REFUNDED');?>"/>
<input type="hidden" name="taken" id="taken-text" value="<?php echo JTEXT::_('COUPON_TAKEN');?>"/>
 <form action="<?php echo JRoute::_('index.php', true, 0); ?>" method="post">
			<div class="em_wrapTit container"><h3><?php echo $user->name;?> <input type="submit" name="Submit" class="btn btn-primary" value="<?php echo JText::_('JLOGOUT'); ?>" /></h3>
			
				
				<input type="hidden" name="option" value="com_users" />
				<input type="hidden" name="task" value="user.logout" />
				<input type="hidden" name="return" value="<?php echo base64_encode('index.php'); ?>" />
				<?php echo JHtml::_('form.token'); ?>
			
			</div>
		</form>
<div class="em_innerBlock em_merchantPage container">



	<div class="row zh-TW">
		<div class="col-md-12">
			<div class="detailcontent">
				<div class="aboutuspage" style="width:100%;background:#FFF;font-size:12px;padding:20px;">

				<form id="form-report" action="" method="GET" role="form">
			<input type="hidden" name="filter_order_Dir" value="<?php echo JRequest::getVar('filter_order_Dir',"desc");?>"/>
	<input type="hidden" name="filter_order" value="<?php echo JRequest::getVar('filter_order',"a.created_at");?>"/>
					<input class="btn btn-primary pull-right" type="button" value="<?php echo JText::_('GENERATE_EXCEL');?>" onClick="location.href='<?php echo $linkGenerate;?>'" />
					<legend><?php echo JText::_('REPORT_TITLE'); ?>
						
					</legend>
					<div class="clear"></div>
					<div class="form-group">
						<div class="row">
						<div class=" col-sm-4 col-md-4 col-lg-4">
							<label for=""><b><?php echo JText::_('DEAL_NAME'); ?>:</b></label>
							<?php echo JHTML::_('select.genericList', $dealJOptList, 'deal_id',  ' class=" form-control" ', 'value', 'text', $this->deal_id); ?>
						</div>
						<div class="col-sm-4 col-md-4 col-lg-4">
							<label><b class="marTop5 flRight"><?php echo JText::_('From'); ?>:</b></label>
							<input value="<?php echo JRequest::getVar('from_at',"");?>" name="from_at" type="text" class="form-control datepickerz" id="from_at"/>

							
						</div>
						<div class="col-sm-4 col-md-4 col-lg-4">
							<label><b class="marTop5 flRight"><?php echo JText::_('To'); ?>:</b></label>
							<input value="<?php echo JRequest::getVar('from_to',"");?>" name="from_to" type="text" class="form-control datepickerz" id="from_to"/>
							
						</div>
						</div>
						<br/>
						<div class="row">
						<div class=" col-sm-4 col-md-4 col-lg-4">
							<label for=""><b><?php echo JText::_('Status'); ?>:</b></label>
							<?php echo JHTML::_('select.genericList', $typeJOptList, 'type', ' class=" form-control" ', 'value', 'text',JRequest::getVar('type'));?>
							
						</div>
						
						<div class="col-sm-4 col-md-4 col-lg-4">
							<label for=""><b><?php echo JText::_('Branch'); ?>:</b></label>
							<select name="branch" class=" form-control ui-widget-content" id="branch2" class="required">
								<option value=""><?php echo JText::_('所有分行');?></option>
								<?php 
								
								$branches = json_decode($this->merchant->branches);
								$numberOfPayment = count($this->merchant);                      
								foreach($branches as $row):
									
									
									$select ="";
								if(JRequest::getVar('branch')==$row->name){
									$select ="SELECTED";
								}
								
								echo "<option ".$select." value=\"".$row->name."\">".$row->name."</option>";
								
								endforeach;
			                                //}
								?>
							</select>

						</div>
						<div class="col-sm-4 col-md-4 col-lg-4">
								<label for="coupon"><?php echo JText::_('MERCHANT_LOGIN_COUPON_SERIAL') ?>:</label>
	               				 <input type="text" width="30" name="coupon" class="form-control" value="<?php echo $this->coupon; ?>" id="coupon" />
							
						</div>

						</div>
					</div>
				
					<input type="submit" class="btn btn-primary" id="search_Voucher" value="<?php echo JTEXT::_('Search') ?>" >
							<input class="btn btn-primary" type="button" value="<?php echo JText::_('Reset');?>" onClick="location.href=''" />
				
					
				</form>
				<br/>
		

<form action="" id="reportForm">
	<input type="hidden" name="deal_id" value="<?php echo JRequest::getVar('deal_id',"");?>"/>
	<input type="hidden" name="from_at" value="<?php echo JRequest::getVar('from_at',"");?>"/>
	<input type="hidden" name="from_to" value="<?php echo JRequest::getVar('from_to',"");?>"/>
	<input type="hidden" name="type" value="<?php echo JRequest::getVar('type',"");?>"/>
	<input type="hidden" name="branch" value="<?php echo JRequest::getVar('branch',"");?>"/>
	<input type="hidden" id="filter_order_Dir" name="filter_order_Dir" value="<?php echo JRequest::getVar('filter_order_Dir',"desc");?>"/>
	<input type="hidden" id="filter_order" name="filter_order" value="<?php echo JRequest::getVar('filter_order',"a.created_at");?>"/>
	</form>
	<div class="table-responsive order-report-table">
	<table class="  table">
		<thead>
			<tr>
				<th width="5%"><?php echo JTEXT::_('REPORT_SERIAL'); ?></th>
				
				
				<th width="5"><a href="javascript:void(0)" class="click-sort" rel="1" title="Click to sort"><?php echo JText::_('DEAL_CODE');?></a><?php  //echo JHTML::_( 'grid.sort', JText::_('Deal Code'), 'd.deal_code', $this->order['order_dir'], $this->order['order']); ?></th>
				<th width="5"><a href="javascript:void(0)" class="click-sort" rel="2" title="Click to sort"><?php echo JText::_('DEAL_NAME');?></a><?php  //echo JHTML::_( 'grid.sort', JText::_('Deal Name'), 'd.name', $this->order['order_dir'], $this->order['order']); ?></th>
				<th width="5"><a href="javascript:void(0)" class="click-sort" rel="3" title="Click to sort"><?php echo JText::_('REPORT_PURCHASE_DATE');?></a><?php  //echo JHTML::_( 'grid.sort', JText::_('REPORT_PURCHASE_DATE'), 'a.created_at', $this->order['order_dir'], $this->order['order']); ?></th>
				<th width="5"><a href="javascript:void(0)" class="click-sort" rel="4" title="Click to sort"><?php echo JText::_('AMOUNT');?></a><?php  //echo JHTML::_( 'grid.sort', JText::_('Amount'), 'a.total_price', $this->order['order_dir'], $this->order['order']); ?></th>
				<th width="15%"><?php echo JTEXT::_('DEPOSIT'); ?></th>
				<th width="15%"><?php echo JTEXT::_('REPORT_BUYER_NAME'); ?></th>
				<th width="15%"><?php echo JTEXT::_('REPORT_BUYER_MAIL'); ?></th>
				<th width="5"><a href="javascript:void(0)" class="click-sort" rel="7" title="Click to sort"><?php echo JText::_('Branch');?><?php  //echo JHTML::_( 'grid.sort', JText::_('Branch'), 'c.branch', $this->order['order_dir'], $this->order['order']); ?></th>
				<th width="5"><a href="javascript:void(0)"class="click-sort" rel="8" vtitle="Click to sort"><?php echo JText::_('REPORT_COUPON_SERIAL');?></a><?php  //echo JHTML::_( 'grid.sort', JText::_('REPORT_COUPON_SERIAL'), 'b.name', $this->order['order_dir'], $this->order['order']); ?></th>
				<th width="5"><a href="javascript:void(0)" class="click-sort" rel="9" title="Click to sort"><?php echo JText::_('REPORT_COUPON_STATUS');?></a><?php  //echo JHTML::_( 'grid.sort', JText::_('REPORT_COUPON_STATUS'), 'b.status', $this->order['order_dir'], $this->order['order']); ?></th>
				 <th width="15%"><a href="javascript:void(0)" class="click-sort" rel="10" title="Click to sort"><?php echo JTEXT::_('REDEMPTION_DATE'); ?></a></th> 
				 <th width="15%"><a href="javascript:void(0)" class="click-sort" rel="11" title="Click to sort"><?php echo JTEXT::_('UNUSED_DATE'); ?></a></th> 
				 <th width="15%"><a href="javascript:void(0)" class="click-sort" rel="12" title="Click to sort"><?php echo JTEXT::_('UNUSED_REASON'); ?></a></th> 
				 <th width="15%"><a href="javascript:void(0)" class="click-sort" rel="13" title="Click to sort"><?php echo JTEXT::_('REFUND_DATE'); ?></a></th> 
				 <th width="15%"><a href="javascript:void(0)" class="click-sort" rel="14" title="Click to sort"><?php echo JTEXT::_('REFUND_REASON'); ?></a></th> 
				

             

            </tr>
        </thead>
        <?php
        $count = 1;
        for ($i = 0; $i < count($orderItemList); $i++) {

        	$orderItem = $orderItemList[$i];
        	$buyerDetail = json_decode($orderItem->buyer_detail);
        	?>
        	<input type="hidden" name="unused-date" id ="unused-date-<?php echo $orderItem->id;?>"value="<?php echo date("Y-m-d H:i:s",strtotime($orderItem->unused_date));?>"/>
        	<input type="hidden" name="unused-date" id="refund-date-<?php echo $orderItem->id;?>" value="<?php echo date("Y-m-d H:i:s",strtotime($orderItem->refund_date));?>"/>
        	<input type="hidden" name="unused-date" id="unused-modified-<?php echo $orderItem->id;?>" value="<?php 
        	if(!strtotime($orderItem->unused_modified)){
        		$orderItem->unused_modified = $orderItem->unused_date;
        	}
        	echo date("Y-m-d H:i:s",strtotime($orderItem->unused_modified));?>"/>
        	<input type="hidden" name="unused-date" id="refund-modified-<?php echo $orderItem->id;?>" value="<?php 
        	if(!strtotime($orderItem->refund_modified)){
        		$orderItem->refund_modified = $orderItem->refund_date;
        	}
        	echo date("Y-m-d H:i:s",strtotime($orderItem->refund_modified));?>"/>
        	<tr id="orderItem-<?php echo $orderItem->id;?>">

        		<td><?php echo $count++; ?>
        			<td><?php echo $orderItem->deal_code; ?></td>
        			<td><?php echo $orderItem->name; ?></td>
        			<td><?php echo DatetimeWrapper::getDisplayDatetime($orderItem->created_at); ?></td>
        			
        			<td><?php echo $orderItem->total_buyer_paid; ?></td>
        			<td><?php echo (int)($orderItem->deposit * $orderItem->qty); ?></td>
        			<td><?php echo $buyerDetail->name; ?></td>
        			<td><?php echo $buyerDetail->email; ?></td>
        			<td><?php echo $orderItem->branch; ?></td>
                    <!-- <td><?php echo $deliveryDetail->name; ?></td>
                    <td><?php echo $deliveryDetail->email; ?></td>
                    
                    <td><?php echo $orderItem->order->description; ?></td> -->
                    
                    <td align="center"><?php echo $orderItem->nameIn; ?></td>
                    <td align="center" class="status-text"><?php 
                    	if($orderItem->status=='Refunded'){
                    		echo "<span>".JTEXT::_('COUPON_REFUNDED')."</span>";
                    	}else{
                    		echo "<span>".JTEXT::_('COUPON_' . strtoupper($orderItem->statusIn))."</span>";
                    		if($orderItem->statusIn =='Used'){
                    			echo '<a href="#" class="btn btn-primary taken-button take-'.$orderItem->id.'" data-toggle="modal" data-target="#myModal" rel="'.$orderItem->id.'" title="Taken">'.JTEXT::_('Taken').'</a>';
                    		}
                    		if($orderItem->statusIn =='Used'||$orderItem->statusIn =='Taken'){

                    			echo '<a href="#" class="btn btn-primary refund-button refund-'.$orderItem->id.'" data-toggle="modal" data-target="#myModal" rel="'.$orderItem->id.'" title="Taken">'.JTEXT::_('Refund').'</a>';
                    		}
                    	}

                    	?></td>
                    <td align="center "><?php echo DatetimeWrapper::getDisplayDatetime($orderItem->deallocated_at); 
                    		if(strtotime($orderItem->deallocated_at)){
                    		echo " ".date("H:i:s",strtotime($orderItem->deallocated_at));
                    		
                    		
                    	}
                    ?></td>
                    <td align="center " class="unused-date"><?php echo DatetimeWrapper::getDisplayDatetime($orderItem->unused_date);
                    	if(strtotime($orderItem->unused_date)){
                    			echo " ".date("H:i:s",strtotime($orderItem->unused_date)); 

                   			 }
                    ?></td>
                      <td align="center">
						<a href="#" class="edit-taken" data-toggle="modal" data-target="#myModal" rel="<?php echo $orderItem->id;?>" title="Click to edit">
                     		 <?php echo $orderItem->unused_reason; ?>
						</a>
                      </td>
                      
                    <td align="center " class="refund-date">
				
                    <?php echo DatetimeWrapper::getDisplayDatetime($orderItem->refund_date); 
                    	if(strtotime($orderItem->refund_date)){
                    		echo " ".date("H:i:s",strtotime($orderItem->refund_date));
                    		
                    		
                    	}
                    ?></td>
                    <td align="center">
						<a href="#" class="edit-refund" data-toggle="modal" data-target="#myModal" rel="<?php echo $orderItem->id;?>" title="Click to edit">
                    		<?php echo $orderItem->refund_reason; ?>
						</a>
                    </td>
                    
                    </tr>
                    <?php
            //print_r('<pre>');
           // print_r($orderItem );die;
            // $buyerDetail = json_decode($orderItem->order->buyer_detail);
            // $deliveryDetail = json_decode($orderItem->order->delivery_detail);

            // for ($j = 0; $j < count($orderItem->invtyList); $j++) {
            //     $invty = $orderItem->invtyList[$j];
            //     ?>
            
            <?php
            // }
        }
        ?>
        <tfoot>
        	<tr>
        		<td colspan="16"><?php //echo $this->pagination->getListFooter(); ?></td>
        	</tr>
        </tfoot>
    </table>
    </div>

</div>
				</div> 
			</div>
		</div>    
	</div>


	
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Taken</h4>
      </div>
      <div class="modal-body">
       	<form action="" method="POST" role="form">
       		
       		<input type="hidden" name="orderID" id="orderID" value=""/>
       		<input type="hidden" name="todo" id="todo" value=""/>
       		<div class="form-group">
       			<label for=""><?php echo JTEXT::_('REASON'); ?></label>
       			<textarea class="form-control" name="reason" id="reason" rows="3"></textarea>

       			
       		</div>
       		<div class="form-group">
       			<label for=""><?php echo JTEXT::_('ADDED_DATE'); ?></label>
       			<input type="text" disabled class="form-control" value="<?php echo date('Y-m-d H:i:s');?>" name="added" id="added-date" placeholder="Input field">
       		</div>
       		<div class="form-group">
       			<label for=""><?php echo JTEXT::_('MODIFIED_DATE'); ?></label>
       			<input type="text" disabled class="form-control" value="<?php echo date('Y-m-d H:i:s');?>" name="modified" id="modified-date" placeholder="Input field">
       		</div>
       		
       	
       		
       	
       		
       	</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo JTEXT::_('CLOSE'); ?></button>
        <button type="button" class="btn btn-primary" id="submit_modal"><?php echo JTEXT::_('SAVE_CHANGES'); ?></button>
      </div>
    </div>
  </div>
</div>

