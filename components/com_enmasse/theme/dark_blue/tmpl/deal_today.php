<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 


require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."DatetimeWrapper.class.php");
require_once(JPATH_SITE . DS . "components". DS ."com_enmasse". DS ."helpers". DS ."DealType.class.php"); 
$is_listing = '1';
$dealID = $this->deal->id;
require_once(JPATH_SITE . DS."components". DS ."com_enmasse".DS."theme".DS."dark_blue".DS."tmpl".DS."background_image.php");

JModelLegacy::getInstance('deal','enmasseModel')->addNumView($dealID);
//$oSession = JFactory::getSession();
//$numno = $oSession->get('aaaa');
////$oSession->set('aaaa', '0');
//echo $numno;die;
//if(!$numno){
//    $numno = 0;
//}
//$numno += 1;
//$oSession->set('aaaa', $numno);
//if($numno == '3'){
//    echo $numno;die;
//}
$juriRoot = JUri::root();
$theme =  EnmasseHelper::getThemeFromSetting();

//--------- add stylesheet and javascript

//$enableRTL =  EnmasseHelper::getRTLFromSetting();
//if($enableRTL){
//	JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen_rtl.css');
//}
//JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen.css');

//set default timezone
DatetimeWrapper::setTimezone(DatetimeWrapper::getTimezone());

$deal = $this->deal;
$merchant = $deal->merchant;
//------- to set the meta data and page title for SEO
$document = JFactory::getDocument();
$document->setMetadata('Keywords',  $deal->name);

$version = new JVersion;
$joomla = $version->getShortVersion();
if(substr($joomla,0,3) >= 1.6){
    $document   = JFactory::getDocument();
    $document->setTitle( $deal->name );
}else{
    $mainframe->setPageTitle($deal->name);
      
}

// load the deal image size
$dealImageSize = EnmasseHelper::getDealImageSize();
if(!empty($dealImageSize))
{
	$image_height = $dealImageSize->image_height;
	$image_width = $dealImageSize->image_width;
}
else
{
	$image_height = 252 ;
	$image_width = 400;
}

//contruct deal image url 
$imageUrlArr = array();
if(EnmasseHelper::is_urlEncoded($deal->pic_dir)){
	$imageUrlArr = unserialize(urldecode($deal->pic_dir));
}else{
	$imageUrlArr[0] = $deal->pic_dir;
}

//contruct data for social network sharing
$oMenu = JFactory::getApplication()->getMenu();
$oItem = $oMenu->getItems('link','index.php?option=com_enmasse&view=dealtoday',true);
$user = JFactory::getUser();
$userID = $user->get('id');
$shareName = $deal->name;
$shareUrl = JURI::base() . 'index.php?option=com_enmasse&controller=deal&task=view&id='. $deal->id . '&slug_name=' . $deal->slug_name . '&Itemid=' . $oItem->id;
if($userID!='0')
{
	$shareUrl .= '&referralid='.$userID;
}
$shareShortDesc = str_replace("\"", "'", $deal->short_desc);
$shareImages = JURI::base(). str_replace("\\","/",$imageUrlArr[0]);
$logoMerchant = array();
if(EnmasseHelper::is_urlEncoded($merchant->logo_url)){
	$logoMerchant = unserialize(urldecode($merchant->logo_url));
}else{
	$logoMerchant[0] = $merchant->logo_url;
}
$logoImages = JURI::base(). str_replace("\\","/",$logoMerchant[0]);
if(!$logoMerchant[0]){
    $logoImages = '';
}
if (($deal->tier_pricing)==1)
{
	$deal->price = EnmasseHelper::getCurrentPriceStep(unserialize($deal->price_step),$deal->price,$deal->cur_sold_qty);
	$deal->types =NULL;
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" >DD_roundies.addRule ('.descrip', '2px', true );</script>
<?php if(count($merchant->branches)>0):?>
	<?php JFactory::getDocument()->addScript("http://maps.google.com/maps/api/js?sensor=true");?>
	<script language="javascript">
		jQuery(document).ready( function(){
			initialize();
		})
	</script>
<?php endif;?>


    <div class="em_wrapTit">
    	<h3><?php echo $deal->name;?></h3>
        <div class="em_rss">
            <a href="<?php echo JRoute::_('index.php?option=com_enmasse&controller=rss&task=deal&id='.$deal->id); ?>"><img src="components/com_enmasse/theme/<?php echo $theme?>/images/rss.gif" alt="Deal RSS Feed" title="RSS Feed"/></a>
        </div>
    </div>		

            <div class="em_innerBlock">
                <div class="em_DealInfo">
                    <div class="row-fluid">
                        <div class="span4 em_DealInfoLeft">
                            <div class="em_innerDeal">
                                <div class="em_price" id="price_tag_cont">
                                    <?php 
                                        $iscomming = FALSE;
                                        if($deal->types) { ?>
                                                <p class='fromprc'><?php echo JText::_('FROM') ?></p>
                                    <?php } ?>
                                    <span><?php echo EnmasseHelper::displayCurrency($deal->price)?></span>

                                    <?php if(strtotime($deal->end_at)   < strtotime(DatetimeWrapper::getDatetimeOfNow())): ?>
                                        <a title="<?php echo JText::_('BUY_DEAL_EXPIRED')?>" class="btnBuy"><?php echo JText::_('BUY_DEAL_EXPIRED')?></a>
                                    <?php elseif(strtotime($deal->start_at) > strtotime(DatetimeWrapper::getDatetimeOfNow())):
                                            $iscomming  = TRUE;
                                     ?>
                                         <a title="<?php echo JText::_('BUY_DEAL_UPCOMING')?>" class="btnBuy"><?php echo JText::_('BUY_DEAL_UPCOMING')?></a>
                                    <?php elseif ($deal->status == 'Voided'): ?>
                                         <a title="<?php echo JText::_('BUY_DEAL_VOIDED')?>" class="btnBuy"><?php echo JText::_('BUY_DEAL_VOIDED')?></a>
                                    <?php elseif($deal->max_coupon_qty != "-1" && $deal->cur_sold_qty >= $deal->max_coupon_qty):?>
                                         <a title="<?php echo JText::_('BUY_DEAL_SOLD_OUT')?>" class="btnBuy"><?php echo JText::_('BUY_DEAL_SOLD_OUT')?></a>
                                    <?php else :?>
                                    <a href="<?php if($deal->types) { 
                                                        echo'#';
                                                    }else{ 
                                                        echo 'index.php?option=com_enmasse&controller=shopping&task=addToCart&dealId='.$deal->id.'&slug_name='.$deal->slug_name.'&referralid='.JRequest::getVar('referralid').'&typeId=none'. '&Itemid=' . $oItem->id;
                                                    } ?>"
                                            class="<?php if($deal->types){echo 'showBuy'; }else{echo 'btnBuy'; } ?>" title="<?php echo JText::_('Buy')?>"><?php echo JText::_('Buy')?></a>
                                    <?php endif; ?>
                                </div><!--em_price-->
                                <!--Tier Pricing-->
                                <?php
                                if(($deal->tier_pricing)==1)
                                {
                                        $price_step = unserialize($deal->price_step);
                                        foreach ($price_step as $k=>$v)
                                        {
                                                if(empty($k) or empty($v))
                                                {
                                                }
                                                else
                                                {	
                                                    echo '<div class="em_dynamicPrice">';	
                                                            echo '<p class="step_price">';
                                                                    echo "<b>".EnmasseHelper::displayCurrency($k) . "</b> ".JText::_('WHEN_REACH_SALE')." <b>$v</b> <br />";
                                                            echo '</p>';							
                                                    echo '</div>';
                                                }
                                        }
                                }
                                ?>
                                <!--Tier Pricing-->
                                <div class="em_dealDiscount">
                                        <div class="wrapPrice">
                                            <dl>
                                                    <dt><?php echo JText::_('DEAL_VALUE');?></dt>
                                                    <dd><?php echo EnmasseHelper::displayCurrency($deal->origin_price)?></dd>
                                            </dl>
                                            <dl class="discount">
                                                    <dt><?php echo JText::_('DEAL_DISCOUNT');?></dt>
                                                    <dd><?php echo empty($deal->origin_price)? 100 : (100 - intval($deal->price/$deal->origin_price*100))?>%</dd>
                                            </dl>
                                            <dl class="save">
                                                    <dt><?php echo JText::_('DEAL_SAVE');?></dt>
                                                    <dd><?php echo EnmasseHelper::displayCurrency($deal->origin_price - $deal->price)?></dd>
                                            </dl>
                                        </div>
<!--                                        <div id="for_a_friend">
                                            <?php if(strtotime($deal->end_at) < strtotime(DatetimeWrapper::getDatetimeOfNow())):?>
                                                    <a class="icon_gift" href="#"><?php echo JText::_('BUY_IT_FOR_FRIEND')?></a>
                                            <?php elseif(strtotime($deal->start_at) > strtotime(DatetimeWrapper::getDatetimeOfNow())):?>
                                                    <a class="icon_gift" href="#"><?php echo JText::_('BUY_IT_FOR_FRIEND')?></a>
                                            <?php elseif ($deal->status == 'Voided'):?>
                                                    <a class="icon_gift" href="#"><?php echo JText::_('BUY_IT_FOR_FRIEND')?></a>
                                            <?php elseif($deal->max_coupon_qty != "-1" && $deal->cur_sold_qty >= $deal->max_coupon_qty):?>
                                                    <a class="icon_gift" href="#"><?php echo JText::_('BUY_IT_FOR_FRIEND')?></a>
                                            <?php else :?>	
                                                    <a class="icon_gift <?php if($deal->types) echo 'showBuy'; ?> " href="<?php if(!$deal->types){ ?>index.php?option=com_enmasse&controller=shopping&task=addToCart&dealId=<?php echo $deal->id ."&buy4friend=1&slug_name=" .$deal->slug_name; }else echo 'javascript:void();' ?>"><?php echo JText::_('BUY_IT_FOR_FRIEND')?></a>
                                            <?php endif;?> 	
                                        </div>-->
                                </div><!--em_dealDiscount-->
                                <div class="em_remainingTime">
                                    <div class="countdown">
                                        <ul>
                                            <li><?php if(strtotime($deal->start_at)   > strtotime(DatetimeWrapper::getDatetimeOfNow())){
                                                            echo JText::_("DEAL_TIME_DEAL_AVAILABLE");
                                                    } else { 
                                                            echo JText::_("DEAL_TIME_LEFT");
                                                    } ?>    
                                            </li>
                                            <li class="counter">
                                                <span id="cday">00</span>
                                                <span id="chour">00</span>
                                                <span id="cmin">00</span>
                                                <span id="csec">00</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div><!--em_remainingTime-->
                                <div class="em_numberSold">
                                    <?php if (isset($this->upcoming)):?>
                                            <strong class="em_countPoint"><?php echo $deal->cur_sold_qty ." " .JText::_('DEAL_BOUGHT')?></strong>
                                            <div class="em_infoPoint"><?php echo JText::_('DEAL_IS_UPCOMMING')?></div>
                                    <?php elseif ($deal->status == 'Voided'):?>
                                            <strong class="em_countPoint"><?php echo $deal->cur_sold_qty ." " .JText::_('DEAL_BOUGHT')?></strong>
                                            <div class="em_infoPoint"><?php echo JText::_('DEAL_IS_CANCEL')?></div>
                                    <?php else:?>
                                        <?php if($bDealOn = (($deal->min_needed_qty - $deal->cur_sold_qty) <= 0) ):?>
                                            <strong class="em_countPoint"><?php echo JText::_('DEAL_IS_ON'); ?></strong>
                                        <?php endif;?>
                                        <?php if( !$bDealOn || ($deal->max_coupon_qty!="-1" && $deal->cur_sold_qty!=$deal->max_coupon_qty) ):?>
                                            <strong class="em_countPoint"><?php echo $deal->cur_sold_qty?> <?php echo JText::_('DEAL_BOUGHT');?></strong>
                                            <div class="em_point">
                                                <div class="em_leftpoint"></div>
                                                <div class="em_centerpoint">
                                                    <div class="em_leftvote"></div>
                                                    <div class="em_centervote" style="width: <?php echo $bDealOn? 92 * ($deal->cur_sold_qty/$deal->max_coupon_qty) : 92 * ($deal->cur_sold_qty/$deal->min_needed_qty); ?>%;" class="centervote"></div>
                                                    <div class="em_rightvote"></div>
                                                </div>
                                                <div class="em_rightpoint"></div>
                                            </div>
                                            <div class="em_point">
                                                <div class="em_leftnumber">0</div>
                                                <div class="em_rightnumber"><?php echo ($bDealOn? $deal->max_coupon_qty : $deal->min_needed_qty);?></div>
                                            </div>
                                        <?php endif;?>
                                        <?php if(!$bDealOn):?>
                                                <div class="em_infoPoint"><?php echo $deal->min_needed_qty - $deal->cur_sold_qty?> <?php echo JText::_('DEAL_NEED_MORE');?></div>
                                        <?php elseif($deal->max_coupon_qty!="-1" && $deal->cur_sold_qty!=$deal->max_coupon_qty):?>
                                                <div class="em_infoPoint"><?php echo $deal->cur_sold_qty . " " . JText::_('OF') . " " . $deal->max_coupon_qty . " " . JText::_('BOUGHT') . " - " . ($deal->max_coupon_qty - $deal->cur_sold_qty) . " " . JText::_('REMAINING')?></div>
                                        <?php elseif($deal->cur_sold_qty == $deal->max_coupon_qty):?>
                                                <div class="em_infoPoint"><?php echo JText::_('NO_COUPON_LEFT')?></div>
                                        <?php endif?>
                                    <?php endif;?>
                                </div>
                                

                                <div class="em_sharing">
                                    <strong class="em_countPoint"><?php echo JText::_('SHARE_BOX_TITLE');?></strong>
                                    <a href="http://www.facebook.com/share.php?u=<?php echo urlencode($shareUrl); ?>" target="blank"><img src="components/com_enmasse/images/social_media/facebook.png" alt="Share to Facebook" /></a>
                                    <a href="http://twitter.com/share?url=<?php echo urlencode($shareUrl); ?>" class="" data-url="" data-text="<?php echo $shareShortDesc; ?>" data-count="none" data-via="<?php echo $shareName; ?>" target="_blank"><img src="components/com_enmasse/images/social_media/twitter.png" alt="Share to Twitter" /></a>
                                    <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>					
                                    <script language="JavaScript" type="text/javascript">
                                            function mailToFriend() {
                                                    window.open ("index.php?option=com_enmasse&controller=mail&task=mailForm&tmpl=component&dealid=<?php echo $deal->id; ?>&userid=<?php echo $userID; ?>&itemid=<?php echo $oItem->id; ?>", "mywindow","location=0,status=0,scrollbars=0, width=500,height=400");
                                            }
                                    </script>                    
                                    <a href="javascript:void mailToFriend()"><img src="components/com_enmasse/images/social_media/email.png" alt="Mail to friend" /></a>
                                </div>
                            </div><!--em_innerDeal-->
                        </div><!--em_DealInfoLeft-->
            <!-- Deal left end -->

                        <div class="span8 em_DealInfoRight">
                            <div class="em_innerDeal">
                              <div class="em_slideBanner">
                                  <div class="slider-wrapper theme-default">
                                      <div id="slider" class="nivoSlider">
                                          <?php for ($i=0; $i<count($imageUrlArr);$i++):?>
                                                <img data-thumb="<?php echo JURI::root().str_replace("\\","/",$imageUrlArr[$i]);?>" src="<?php echo JURI::root().str_replace("\\","/",$imageUrlArr[$i]);?>" alt="<?php echo $deal->name; ?>" />
                                            <?php endfor;?>
                                      </div>
                                  </div>
                              </div><!--slideBanner-->

                              <div class="em_digest">
                                  <span class="em_highlight"><?php echo strtok($deal->short_desc, ' ')?></span>
                                    <?php echo nl2br(strtok(''))?>
                              </div>
                            </div><!--em_innerDeal-->
                      </div><!--em_DealInfoRight-->
            <?php
                if($deal->types) 
                {
            ?>
                <div class="popupContBuy" id='popupContBuynormal' style="left:-15000px; top:-15000px;">
                    <div class="innerPopContBuy">
                        <div class="contPopBuy">
                            <a href="javascript:void(0)" id="enmasse_attr_close"><img src="components/com_enmasse/images/closebutton.png" alt="Close" /></a>
                            <h3><?php echo JText::_('CHOOSE_YOUR_DEAL') ?>:</h3>
                            <div class="listDealPop">
                                <table cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                    <?php
                                            foreach($deal->types as $type)
                                            {
                                                    if($class_tr == '')
                                                            $class_tr = 'bgBlueL';
                                                    else
                                                            $class_tr = '';

                                    ?>
                                            <tr class='<?php echo($class_tr); ?>'>
                                                    <td width="335">
                                                            <h4><?php echo($type->name); ?></h4>
                                                            <p class="wrapDetail">
                                                                    <span class="txtValue"><strong><?php echo  EnmasseHelper::displayCurrency($type->origin_price);?></strong> <?php echo JText::_('DEAL_TYPE_VALUE') ?></span>
                                                                    <span class="txtPer">- <strong><?php echo(DealType::percent($type->origin_price,$type->price)); ?>%</strong> <?php echo JText::_('DEAL_TYPE_OFF') ?></span>
                                                                    <span class="txtSave">- <strong><?php echo JText::_('DEAL_TYPE_SAVE') ?></strong> <?php echo(EnmasseHelper::displayCurrency(DealType::minus($type->origin_price,$type->price))); ?></span>
                                                            </p><!--wrapDetail-->
                                                    </td>
                                                    <td class="last">
                                                    <p><a href="index.php?option=com_enmasse&controller=shopping&task=addToCart&dealId=<?php echo $deal->id .'&slug_name=' .$deal->slug_name;?>&referralid=<?php echo JRequest::getVar('referralid'); ?>&typeId=<?php echo($type->id); ?>&Itemid='<?php echo $oItem->id; ?>" class="button"><?php echo(EnmasseHelper::displayCurrency($type->price));?></a></p>
                                                    </td>
                                            </tr>
                                    <?php
                                            }
                                    ?>
                                    </tbody></table>
                                </div><!--listDealPop-->
                            </div><!--contPopBuy-->
                        </div><!--innerPopContBuy-->
                    </div>
                <div class="popupContBuy" id='popupContBuyFriend' style="left:-15000px; top:-15000px;">
                    <div class="innerPopContBuy">
                        <div class="contPopBuy">
                        <a href="javascript:void(0);" id="enmasse_attr_close"><img src="components/com_enmasse/images/closebutton.png" alt="Close" /></a>
                        <h3><?php echo JText::_('CHOOSE_YOUR_DEAL') ?>:</h3>
                        <div class="listDealPop">
                            <table cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <?php
                                        foreach($deal->types as $type)
                                        {
                                                if($class_tr == '')
                                                        $class_tr = 'bgBlueL';
                                                else
                                                        $class_tr = '';

                                ?>
                                        <tr class='<?php echo($class_tr); ?>'>
                                                <td width="335">
                                                        <h4><?php echo($type->name); ?></h4>
                                                        <p class="wrapDetail">
                                                                <span class="txtValue"><strong><?php echo  EnmasseHelper::displayCurrency($type->origin_price);?></strong> <?php echo JText::_('DEAL_TYPE_VALUE') ?></span>
                                                                <span class="txtPer">- <strong><?php echo(DealType::percent($type->origin_price,$type->price)); ?>%</strong> <?php echo JText::_('DEAL_TYPE_OFF') ?></span>
                                                                <span class="txtSave">- <strong><?php echo JText::_('DEAL_TYPE_SAVE') ?></strong> <?php echo EnmasseHelper::displayCurrency(DealType::minus($type->origin_price,$type->price)); ?></span>
                                                        </p><!--wrapDetail-->
                                                </td>
                                                <td class="last">
                                                <p><a href="index.php?option=com_enmasse&controller=shopping&task=addToCart&dealId=<?php echo $deal->id .'&slug_name=' .$deal->slug_name;?>&referralid=<?php echo JRequest::getVar('referralid'); ?>&typeId=<?php echo($type->id); ?>&buy4friend=1&Itemid='<?php echo $oItem->id; ?>" class="button"><?php echo(EnmasseHelper::displayCurrency($type->price));?></a></p>
                                                </td>
                                        </tr>
                                <?php
                                        }
                                ?>
                            </tbody>
                            </table>
                        </div><!--listDealPop-->
                        </div><!--contPopBuy-->
                    </div><!--innerPopContBuy-->
                </div>
            <?php } ?>
        </div><!--row-fluid-->
    </div><!--em_DealInfo-->

    <div class="em_dealDetails">
            <div class="row-fluid">
            <div class="span8 em_dealDetailLeft">
                    <div class="em_innerDeal">
                        <h4><?php echo JText::_('DEAL_DESCRIPTION');?></h4>
			<?php echo $deal->description; ?>
			<h4><?php echo JText::_('DEAL_HIGHLIGHT');?></h4>
			<?php echo $deal->highlight?>
			<h4><?php echo JText::_('DEAL_TERM_CONDITION');?></h4>
			<?php echo $deal->terms?>
                    
                        <?php // ==== Start Rating and Review ==== //
                            $nItemId = JFactory::getApplication()->getMenu()->getItems('link','index.php?option=com_enmasse&view=dealtoday',true)->id;
                            $sCommentLink = JRoute::_('index.php?option=com_enmasse&controller=deal&task=comment&id=' . $deal->id . '&Itemid=' . $nItemId, false);
                            $nRating = EnmasseHelper::countDealRatingByDealId($deal->id);
                            $nRating = round($nRating);
                        ?>
                        <p class="average_rating"><?php echo JText::_('AVERAGE_RATING');?>           
                            <span class="rating disabled">                
                                <?php for($i = 1; $i <= $nRating; $i++): ?>
                                    <span class="ratingStar filled">&nbsp;</span>
                                <?php endfor;?>
                                <?php for($i = $nRating; $i < 5; $i++): ?>
                                    <span class="ratingStar">&nbsp;</span>
                                <?php endfor;?>
                            </span> 
                        </p>
                    <div class="floatR"><input style="width:200px;" type="button" onclick="window.location='<?php echo $sCommentLink; ?>'" value="<?php echo JText::_('COMMENT_BUTTON');?>" class="em_btn" /></div>
                </div><!--em_innerDeal-->
            </div><!--em_dealDetailLeft-->
            <div class="span4 em_dealDetailRight">
                <div class="em_innerDeal">
                    <?php if($merchant): ?>
                            <h5><?php echo $merchant->name?></h5>
                            <?php if($logoImages){ ?>
                                <img src="<?php echo  $logoImages;?>" />
                            <?php } ?>
                            <p>
                            <?php if(!empty($merchant->description)):?>
                                    <?php echo nl2br($merchant->description); ?><br>
                            <?php endif;?>
                            <?php if(!empty($merchant->web_url)):?>
                                    <a href="http://<?php echo str_replace('http://', '',$merchant->web_url)?>"><?php echo $merchant->web_url?></a><br>
                            <?php endif;?>
                            <?php if(!empty($merchant->branches) && count($merchant->branches) > 0){ ?>
                                    <?php echo JText::_('BRANCHES');?><br>
                                    <select id="branchSelection" style="width: 150px; display:<?php echo count($merchant->branches)>1? "block" : "none"?>">
                                            <?php $count = 1; foreach ($merchant->branches as $branch):?>
                                                    <option value="branch<?php echo $count?>" <?php echo ($count==1? " selected" : " ")?>><?php echo $branch['name']?></option>
                                                    <?php $count++?>
                                            <?php endforeach;?>
                                    </select><br>
                            </p>

                                    <!-- this script using for initialize google maps -->

                                    <script type="text/javascript">
                                    <?php
                                            //escape quote and carier return, otherwise it will cause javascript error at browser
                                            $pt = array('\\r\\n', '\\n', '\\', '\'');
                                            $rp = array( '', '','\\\\', '\\\'');
                                            $sJSON = str_replace($pt, $rp, json_encode($merchant->branches));
                                    ?>
                                            var arMapDta = jQuery.parseJSON('<?php echo $sJSON?>');
                                            var arMap = new Array();
                                            var arMarker = new Array();
                                            var arPoint = new Array();
                                            function initialize()
                                            {

                                                    var i = 0;
                                                    for(o in arMapDta)
                                                    {

                                                            var oBranch = arMapDta[o];
                                                            var iZoom = parseInt(oBranch.google_map_zoom);

                                                            arPoint[i] = new google.maps.LatLng(parseFloat(oBranch.google_map_lat), parseFloat(oBranch.google_map_long));

                                                            var oOption = { center: arPoint[i], zoom: iZoom, mapTypeId: google.maps.MapTypeId.ROADMAP };
                                                            var sView = "map_canvas_" + (i+1);

                                                            arMap[i] = new google.maps.Map( document.getElementById(sView), oOption);
                                                            arMarker[i] = new google.maps.Marker({position: arPoint[i], map: arMap[i], title: oBranch.name});

                                                            google.maps.event.addListener(arMap[i], 'zoom_changed', function() {
                                                                    var _this = this;
                                                                    setTimeout(function(){
                                                                         moveToDarwin(_this);
                                                                    }, 3000);
                                                            });	
                                                            google.maps.event.addListener(arMarker[i], 'click', function() {
                                                                      this.map.setZoom(this.map.zoom + 1);
                                                                    });
                                                            i++;
                                                    }
                                            }

                                            function moveToDarwin(map)
                                            {
                                                    map.setCenter(arPoint[arMap.indexOf(map)]);

                                            }

                                            jQuery('document').ready(function() {
                                                    var branchId = "";
                                                    jQuery("select#branchSelection option:selected").each(function() {
                                                            branchId = jQuery(this).val();
                                                    });
                                                    if(branchId != ""){	 
                                                            jQuery("div#"+branchId).css("display","block");
                                                    }
                                            });
                                            jQuery('#branchSelection').change(function() {
                                                    var branchId = "";
                                                    jQuery("select#branchSelection option:selected").each(function() {
                                                            branchId = jQuery(this).val();
                                                    });
                                                    jQuery("div[id^='branch']").each(function(){
                                                            jQuery(this).css("display","none");
                                                    });
                                                    if(branchId != ""){	 
                                                            jQuery("div#"+branchId).css('display','block').css('visibility','inherit');
                                                    }
                                            }).change();
                                    </script>

                                    <?php $count = 1;foreach ($merchant->branches as $branch):?>
                                            <div id="branch<?php echo $count?>" style="<?php echo $count === 1 ? 'display:block' : 'visibility:hidden' ?>">
                                                    <div class="branches">
                                                            <?php if(isset($branch['name'])):?>
                                                                    <?php echo nl2br($branch['name'])?><br />
                                                            <?php endif;?>
                                                            <?php if(isset($branch['description'])):?>
                                                                    <?php echo nl2br($branch['description'])?><br />
                                                            <?php endif;?>
                                                            <?php if(isset($branch['address'])):?>
                                                                    <?php echo nl2br($branch['address'])?><br />
                                                            <?php endif;?>
                                                            <?php if(isset($branch['telephone'])):?>
                                                                    <?php echo JTEXT::_('DEAL_MERCHANT_TEL') ?> : <?php echo $branch['telephone']?><br />
                                                            <?php endif;?>
                                                            <?php if(isset($branch['fax'])):?>
                                                                    <?php echo JTEXT::_('DEAL_MERCHANT_FAX') ?> : <?php echo $branch['fax']?><br />
                                                            <?php endif;?>

                                                    </div>
                                                    <?php if($merchant->google_map_width!='' && $merchant->google_map_height!=''):?>
                                                    <div class="map" id="map_canvas_<?php echo $count?>" align="center" style="width:100%; height:<?php echo $merchant->google_map_height?>px;"></div>
                                                    <?php endif;?>
                                            </div>
                                            <?php $count++?>
                                    <?php endforeach;?>
                            <?php } else { echo '</p>'; } ?>
                        <?php endif; ?>
                </div><!--em_innerDeal-->
            </div><!--em_dealDetailRight-->
        </div>
    </div><!--em_dealDetails-->

</div><!--innerBlock-->
                    
<!--Time Count Down Script--> 
<script language="JavaScript">
	TargetDate = "<?php echo date('Y/m/d H:i:s', strtotime($deal->end_at));?>";
	StartDate = "<?php echo date('Y/m/d H:i:s', strtotime($deal->start_at));?>";
    CurrentDate = "<?php echo date('Y/m/d H:i:s', strtotime(DatetimeWrapper::getDatetimeOfNow())); ?>";
	CountActive = true;
	CountStepper = -1;
	LeadingZero = true;
	
	function calcage(secs, num1, num2) {
	  s = ((Math.floor(secs/num1))%num2).toString();
	  if (LeadingZero && s.length < 2)
	    s = "0" + s;
	  return  s ;
	}
	
	function CountBack(secs) {
	  if (secs < 0) {
	    return;
	  }
	  document.getElementById("cday").innerHTML = calcage(secs,86400,100000)+" <?php echo JTEXT::_('DAY');?>";
	  document.getElementById("chour").innerHTML = calcage(secs,3600,24)+" <?php echo JTEXT::_(':');?>";
	  document.getElementById("cmin").innerHTML = calcage(secs,60,60)+" <?php echo JTEXT::_(':');?>";
	  document.getElementById("csec").innerHTML = calcage(secs,1,60);
	
	  if (CountActive)
	    setTimeout("CountBack(" + (secs+CountStepper) + ")", SetTimeOutPeriod);
	}
	
	if (typeof(TargetDate)=="undefined")
	  TargetDate = "12/31/2020 5:00 AM";
	if (typeof(CountActive)=="undefined")
	  CountActive = true;
	if (typeof(FinishMessage)=="undefined")
	  FinishMessage = "";
	if (typeof(CountStepper)!="number")
	  CountStepper = -1;
	if (typeof(LeadingZero)=="undefined")
	  LeadingZero = true;
	
	
	CountStepper = Math.ceil(CountStepper);
	if (CountStepper == 0)
	  CountActive = false;
	var SetTimeOutPeriod = (Math.abs(CountStepper)-1)*1000 + 990;
	
	var dthen = new Date(TargetDate);
	var dstart = new Date(StartDate);
	var dnow = new Date(CurrentDate);
	
	if(CountStepper>0)
	  ddiff = new Date(dnow-dthen);
	else
	  ddiff = new Date(dthen-dnow);
	if(dstart>dnow)
	  ddiff = new Date(dstart-dnow);
	gsecs = Math.floor(ddiff.valueOf()/1000);
	
	CountBack(gsecs);
</script>
<script>
    jQuery(document).ready(function(){
	showBuyPopUP();
	function showBuyPopUP(){
			var maskLayer = jQuery('#maskLayer');
			if(maskLayer.length == 0) {
				var maskLayer = jQuery('<div id="maskLayer"></div>').appendTo(document.body);
			}
			maskLayer.css({
				'position': 'fixed',
				'zIndex': 9,
				'height': '100%',
				'width': jQuery(window).width(),
				'top':0,
				'left':0,
				'background-image':'url("components/com_enmasse/images/modal_bg.png")',
				'display':'none'
			});
                        
                        var buyObj = jQuery('#price_tag_cont  .showBuy');
			buyObj.click(function(){
				maskLayer.css('display', 'block');
                                var coords = buyObj.position();
                                var htmlWidth = jQuery(document).width();
                                var leftPosition = coords.left + buyObj.innerWidth();
                                //alert(htmlWidth + ' - ' + buyObj.innerWidth() + ' - ' +  coords.left );
                                if((htmlWidth - leftPosition) < 380){
                                    jQuery('#popupContBuynormal').css({
                                            'top': coords.top + buyObj.innerHeight() + 7,
                                            'right': htmlWidth - leftPosition - 45,
                                            'left': 'auto'
                                    });
                                } else {
                                    jQuery('#popupContBuynormal').css({
                                            'top': coords.top,
                                            'left': leftPosition,
                                            'right': 'auto'
                                    });
                                }
				return false;
			});
			
			jQuery('#for_a_friend .showBuy').click(function(){
				maskLayer.css('display', 'block');
                                var coords = buyObj.position();
                                var htmlWidth = jQuery(document).width();
                                var leftPosition = coords.left + buyObj.innerWidth();
                                if((htmlWidth - leftPosition) < 380){
                                    jQuery('#popupContBuyFriend').css({
                                            'top': coords.top + buyObj.innerHeight() + 7,
                                            'right': htmlWidth - leftPosition - 45,
                                            'left': 'auto'
                                    });
                                } else {
                                    jQuery('#popupContBuyFriend').css({
                                            'top': coords.top,
                                            'left': leftPosition,
                                            'right': 'auto'
                                    });
                                }
				return false;
			});
                        
			jQuery('.popupContBuy #enmasse_attr_close').click(function(){
                                jQuery('.popupContBuy').css({
					'top': -15000,
                                        'left': -15000,
                                        'right': 'auto'
				});
				jQuery('#maskLayer').css('display', 'none');
				return false;
			});
                        
			jQuery('#maskLayer').click(function(){
                                jQuery('.popupContBuy').css({
					'top': -15000,
                                        'left': -15000,
                                        'right': 'auto'
				});
				jQuery('#maskLayer').css('display', 'none');
				return false;
			});
		//}
	}
	jQuery(document.body).bind('click', function(e){
		
	});
    });
</script>