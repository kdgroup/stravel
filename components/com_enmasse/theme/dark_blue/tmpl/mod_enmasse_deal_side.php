<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 


require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse".DS."helpers". DS ."EnmasseHelper.class.php");

$theme =  EnmasseHelper::getThemeFromSetting();
//$enableRTL =  EnmasseHelper::getRTLFromSetting();
//if($enableRTL){
//	JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen_rtl.css');
//}
//JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen.css');
// load language pack
$language = JFactory::getLanguage();
$base_dir = JPATH_SITE.DS.'components'.DS.'com_enmasse';
$version = new JVersion;
$joomla = $version->getShortVersion();
if(substr($joomla,0,3) >= '1.6'){
    $extension = 'com_enmasse16';
}else{
    $extension = 'com_enmasse';
}
if($language->load($extension, $base_dir, $language->getTag(), true) == false)
{
	 $language->load($extension, $base_dir, 'en-GB', true);
}
foreach($items as $item){
    if(!EnmasseHelper::is_urlEncoded($item->pic_dir))
     {
            $item->imageUrl = $item->pic_dir;
     }
     else
     {
            $imageUrlArr= unserialize(urldecode($item->pic_dir));
            $item->imageUrl = str_replace("\\","/",$imageUrlArr[0]);
     }
}

$nItemId = JFactory::getApplication()->getMenu()->getItems('link','index.php?option=com_enmasse&view=deallisting',true)->id;
?>

<div class="em_corBlock em_corBlock2">
    <?php 
        foreach($items as $i => $item){
            $link = 'index.php?option=com_enmasse&controller=deal&task=view&id='.$item->id .'&slug_name='.$item->slug_name.'&sideDealFlag=1&Itemid='.$nItemId;
//            $link = 'index.php?option=com_enmasse&controller=shopping&task=addToCart&dealId='.$item->id.'&slug_name='.$item->slug_name.'&referralid='.JRequest::getVar('referralid').'&typeId=none&Itemid='.$nItemId;
    ?>
        <div class="em_moreDeal">
            <dl class="em_itemDealMore">
                <dd class="em_thumbDeal"><a href="<?php echo $link; ?>" title=""><img alt="<?php echo $item->name; ?>" src="<?php echo $item->imageUrl; ?>" /></a></dd>
                <dd class="em_titDeal"><a href="<?php echo $link; ?>" title="<?php echo JTEXT::_($item->name); ?>"> <?php echo JTEXT::_($item->name); ?></a></dd>
            </dl>
            <a href="<?php echo $link; ?>"><span class="em_btn em_btn_02">View This Deal</span></a>
        </div><!--em_moreDeal-->
    <?php } ?>
</div><!--em_corBlock2-->

