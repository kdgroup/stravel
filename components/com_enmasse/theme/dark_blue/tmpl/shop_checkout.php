<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once(JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
require_once(JPATH_SITE . DS."components". DS ."com_enmasse".DS."theme".DS."dark_blue".DS."tmpl".DS."background_image.php"); 	
$theme =  EnmasseHelper::getThemeFromSetting();
//$enableRTL =  EnmasseHelper::getRTLFromSetting();
//if($enableRTL){
//	JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen_rtl.css');
//}
JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen.css');

$oItem = array_pop($this->cart->getAll());
JHTML::_('behavior.formvalidation');
$step = JRequest::getVar('step');
?>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<style>
    .main_meal_banner {
        display: none;
    }
    textarea:focus, input[type="text"]:focus, input[type="password"]:focus, input[type="datetime"]:focus, input[type="datetime-local"]:focus, input[type="date"]:focus, input[type="month"]:focus, input[type="time"]:focus, input[type="week"]:focus, input[type="number"]:focus, input[type="email"]:focus, input[type="url"]:focus, input[type="search"]:focus, input[type="tel"]:focus, input[type="color"]:focus, .uneditable-input:focus {
        border-color: rgba(82, 168, 236, 0.8);
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(82, 168, 236, 0.6);
        outline: 0 none;
    }
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<div class="em_innerBlock em_shopCartPage">
		<?php 
		switch ($step) {
			case '2':
				include "order.php";
				break;
			case '3':
				include "order_confirm.php";
				break;

			default:
				include "cart_manage.php";
				break;
		}
		
		

		?>
		
</div>
