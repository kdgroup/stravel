<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse".DS."helpers". DS ."EnmasseHelper.class.php");

$theme =  EnmasseHelper::getThemeFromSetting();
$enableRTL =  EnmasseHelper::getRTLFromSetting();
//if($enableRTL){
//	JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen_rtl.css');
//}
//JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen.css');

$orderItemList 	= $this->orderItemList;
$orderItemListCoupon 	= $this->orderItemListCoupon;

$emptyJOpt = JHTML::_('select.option', '', JText::_('MERCHANT_LOGIN_DEAL_OP') );

$dealJOptList = array();
array_push($dealJOptList, $emptyJOpt);
foreach ($this->dealList as $item)
{
	$var = JHTML::_('select.option', $item->id, JText::_($item->name) );
	array_push($dealJOptList, $var);
}
JHtml::_('behavior.framework');

$user = JFactory::getUser();
$isSupper = false;
if(in_array(13, $user->groups)){
	$isSupper =true;
}

?>
	  <form action="<?php echo JRoute::_('index.php', true, 0); ?>" method="post">
			<div class="em_wrapTit container"><h3><?php echo $user->name;?> <input type="submit" name="Submit" class="btn btn-primary" value="<?php echo JText::_('JLOGOUT'); ?>" /></h3>
			
				
				<input type="hidden" name="option" value="com_users" />
				<input type="hidden" name="task" value="user.logout" />
				<input type="hidden" name="return" value="" />
				<?php echo JHtml::_('form.token'); ?>
			
			</div>
		</form>
	<div class="em_innerBlock em_merchantPage container">



<div class="row zh-TW">
            <div class="col-md-12">
                <div class="detailcontent">
                	<div class="aboutuspage" style="width:100%;background:#FFF;font-size:12px;padding:20px;">
	<h4><?php echo JText::_('MERCHANT_LOGIN_MSG3') ?></h4>
		<form class="em_frmStatus" action="index.php" id="em_frmStatus" name="em_frmStatus">
	    	<ul>
	        	<li>
	            	<label for="coupon"><?php echo JText::_('MERCHANT_LOGIN_COUPON_SERIAL') ?>:</label>
	                <input type="text" width="30" name="coupon" value="<?php echo $this->coupon; ?>"required id="coupon" />
	            </li>
	            
				<?php if ($this->enmaseSetting->enable_security_code) 
				{
				?>
				<li>
	            	<label for="coupon"><?php echo JText::_('MERCHANT_LOGIN_COUPON_SECURITY_CODE') ?>:</label>
	                <input type="text" width="30" name="coupon_security_code" id="coupon_security_code" />
	            </li>
				<?php 
				}
				?>
				
	            <li>
	            	<input type="submit" class="em_btn"  value="<?php echo JTEXT::_('MERCHANT_LOGIN_CHECK_SERIAL_BUTTON');?>" />

	            </li>
	        </ul>
	         <table cellpadding="0" cellspacing="0" border="0" class="em_tbGeneral em_tbMerchant" style="margin-top:5px;">
	           <tr> 	 	 	 	 	
	                <th><input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" /></th>
	                <th><?php echo JTEXT::_('MERCHANT_LOGIN_BUYER');?></th>
	               <!--  <th><?php echo JTEXT::_('MERCHANT_LOGIN_COMMENT');?></th> -->
	                <th><?php echo JTEXT::_('MERCHANT_LOGIN_PURCHASE_DATE');?></th>
	                <th><?php echo JTEXT::_('MERCHANT_LOGIN_COUPON_SERIAL');?></th>
	                <th><?php echo JTEXT::_('STATUS');?></th>
	                <th><?php echo JTEXT::_('MERCHANT_REDEMPTION_DATE');?></th>
	              <!--   <th><?php echo JTEXT::_('MERCHANT_COUPON_SETTLEMENT_STATUS');?></th> -->
	                <th><?php echo JTEXT::_('Branch');?></th>
	                <th ><?php echo JTEXT::_('REFUND_DATE'); ?></th> 
				 	<th ><?php echo JTEXT::_('REFUND_REASON'); ?></th> 
	            </tr>
				<?php
				$count = 0;
				$tm = JFactory::getSession()->get('tm');	
				JFactory::getSession()->set('tm',"");	
				
				// if($tm=="coupon"){
				// 	$orderItemListCoupon =$orderItemList;
				// 	$orderItemList = array();
				// 	// print_r("<pre>");
				// 	// print_r($orderItemListCoupon);die;
				// }
				for($i=0; $i < count($orderItemListCoupon); $i++)
				{
					$orderItem = $orderItemListCoupon[$i];
					$buyerDetail = json_decode($orderItem->order->buyer_detail);
			
					for($j=0; $j < count($orderItem->invtyList); $j++)
					{
						$invty = $orderItem->invtyList[$j];
						$count++;
						if($this->coupon!=$invty->name){
							continue;
						}
				?>
	            <tr>
	                <td><?php echo JHTML::_('grid.id', $count - 1, $invty->id );?></td>
	                <td><?php echo $buyerDetail->name; ?></td>
	              <!--   <td><?php echo $orderItem->order->description; ?></td> -->
	                <td><?php echo DatetimeWrapper::getDisplayDatetime($orderItem->created_at); ?></td>
	                <td><?php echo $invty->name; ?></td>
	                <td>
						<?php
							
							if($orderItem->status=='Refunded'){
								echo "<b>";echo JTEXT::_('COUPON_REFUNDED'); echo "</b>";
							}else{
								if($invty->status=="Used")
								{ 
									echo "<b>";echo JTEXT::_('COUPON_'.$invty->status); echo "</b>";
								}
								else
									echo JTEXT::_('COUPON_'.$invty->status);
							}
							
						?> 
					</td>
					<td><?php echo DatetimeWrapper::getDisplayDatetime($invty->deallocated_at); ?></td>
	               <!--  <td>
						<?php 
							if($invty->status != EnmasseHelper::$INVTY_STATUS_LIST['Used'] && $invty->settlement_status == EnmasseHelper::$MERCHANT_SETTLEMENT_STATUS_LIST['Not_Paid_Out'])
							{
								echo JText::_("MERCHANT_COUPON_CAN_NOT_PAID_OUT");
							}elseif ($invty->status == EnmasseHelper::$INVTY_STATUS_LIST['Used'] && $invty->settlement_status == EnmasseHelper::$MERCHANT_SETTLEMENT_STATUS_LIST['Not_Paid_Out'])
							{
								echo JText::_("MERCHANT_COUPON_CAN_BE_PAID_OUT");
							}
							elseif ($invty->settlement_status == EnmasseHelper::$MERCHANT_SETTLEMENT_STATUS_LIST['Should_Be_Paid_Out'])
							{
								echo JText::_("MERCHANT_COUPON_WATTING_FOR_PAID_OUT");
							}elseif ($invty->settlement_status == EnmasseHelper::$MERCHANT_SETTLEMENT_STATUS_LIST['Paid_Out'])
							{
								echo JText::_("MERCHANT_COUPON_PAID_OUT");
							}
						?>
					</td> -->
					<td><?php echo $orderItem->order->branch; ?></td>
					<td><?php echo DatetimeWrapper::getDisplayDatetime($orderItem->refund_date); 
                    	if(strtotime($orderItem->refund_date)){
                    		echo " ".date("H:i:s",strtotime($orderItem->refund_date));
                    		
                    		
                    	}
                    ?></td>
					<td><?php echo $orderItem->refund_reason; ?></td>
	            </tr>
				<?php
					}
				}
				?>
	        </table>
			<input type="hidden" name="newStatus" value="Used" />

			<input type="hidden" name="option" id="option" value="com_enmasse"/>
			<input type="hidden" name="controller" id="controller" value="merchant"/>
			<input type="hidden" name="task" id="task" value="check"/>
	</form>
	<br/>
 
                		<h4><?php echo JText::_('MERCHANT_LOGIN_MSG1') ?></h4>
		<form class="em_frmStatus" action="index.php" id="em_frmStatus" name="em_frmStatus">
	    	<ul>
	        	<li>
	            	<label for="coupon"><?php echo JText::_('MERCHANT_LOGIN_COUPON_SERIAL') ?>:</label>
	                <input type="text" width="30" name="coupon" required id="coupon" />
	            </li>
	            <li>
	            	<label style="margin-left:30px;" for="filterdeal_id"><?php echo JText::_('Branch') ?>:</label>
					  <select name="branch" required class="ui-widget-content" id="branch2" class="required">
                        <option value=""><?php echo JText::_('請選擇分行');?></option>
                        <?php 
                                               
                                $branches = json_decode($this->merchant->branches);
                                $numberOfPayment = count($this->merchant);                      
                                foreach($branches as $row):
                                  
                                            
                                                $select ="";
                                                if($this->branchActived==$row->name){
                                                    $select ="SELECTED";
                                                }
                                                
                                                echo "<option ".$select." value=\"".$row->name."\">".$row->name."</option>";
                                           
                                endforeach;
                                //}
                                ?>
                                </select>

				</li>
				<?php if ($this->enmaseSetting->enable_security_code) 
				{
				?>
				<li>
	            	<label for="coupon"><?php echo JText::_('MERCHANT_LOGIN_COUPON_SECURITY_CODE') ?>:</label>
	                <input type="text" width="30" name="coupon_security_code" id="coupon_security_code" />
	            </li>
				<?php 
				}
				?>
				
	            <li>
	            	<input class="em_btn" type="submit" onclick="this.form.newStatus.value='Used'" value="<?php echo JText::_('MAKE_AS_USED') ?>" name="submit">
	              <?php if(1==0){

	              	?>
	                <input class="em_btn" type="submit" onclick="this.form.newStatus.value='Taken'" value="<?php echo JText::_('MAKE_AS_TAKEN') ?>" name="submit">
	                <?php }?>
	            </li>
	        </ul>
			<input type="hidden" name="newStatus" value="Used" />

			<input type="hidden" name="option" id="option" value="com_enmasse"/>
			<input type="hidden" name="controller" id="controller" value="merchant"/>
			<input type="hidden" name="task" id="task" value="update"/>
	</form>
	<br/>
	<?php 
 if($isSupper){?>
		<h4><?php echo JText::_('MERCHANT_LOGIN_MSG2') ?></h4>
		<form id="adminForm" name="adminForm" method="post" action="" >
	    	<input type="hidden" name="option" value="com_enmasse" />
			<input type="hidden" name="controller" id="controller" value="merchant"/>
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="boxchecked" value="0" />	
			<input type="hidden" name="product" value="search" />	
			<!-- <input type="hidden" name="coupon" value="merchantpz" />	 -->
			<ul>
	        	<li>
	            	<label for="filterdeal_id"><?php echo JText::_('MERCHANT_LOGIN_DEAL') ?>:</label>
	                <?php echo JHTML::_('select.genericList', $dealJOptList, 'filter[deal_id]', 'style="width:320px"' , 'value', 'text', $this->filter['deal_id']);?>
	            </li>
	            <li>
	            	<label style="margin-left:30px;" for="filterdeal_id"><?php echo JText::_('Branch') ?>:</label>
					  <select name="branch" required class="ui-widget-content" id="branch2" class="required">
                        <option value=""><?php echo JText::_('請選擇分行');?></option>
                        <?php 
                                               
                                $branches = json_decode($this->merchant->branches);
                                $numberOfPayment = count($this->merchant);                      
                                foreach($branches as $row):
                                  
                                            
                                                $select ="";
                                                if($this->branchActived==$row->name){
                                                    $select ="SELECTED";
                                                }
                                                
                                                echo "<option ".$select." value=\"".$row->name."\">".$row->name."</option>";
                                           
                                endforeach;
                                //}
                                ?>
                                </select>

				</li>
	            <li>
	            	<input type="submit" class="em_btn"  value="<?php echo JTEXT::_('MERCHANT_LOGIN_GO_BUTTON');?>" />
					
	             </li>
	        </ul>
	    
	        <table cellpadding="0" cellspacing="0" border="0" class="em_tbGeneral em_tbMerchant" style="margin-top:5px;">
	           <tr> 	 	 	 	 	
	                <th><input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" /></th>
	                <th><?php echo JTEXT::_('MERCHANT_LOGIN_BUYER');?></th>
	               <!--  <th><?php echo JTEXT::_('MERCHANT_LOGIN_COMMENT');?></th> -->
	                <th><?php echo JTEXT::_('MERCHANT_LOGIN_PURCHASE_DATE');?></th>
	                <th><?php echo JTEXT::_('MERCHANT_LOGIN_COUPON_SERIAL');?></th>
	                <th><?php echo JTEXT::_('STATUS');?></th>
	              <!--   <th><?php echo JTEXT::_('MERCHANT_COUPON_SETTLEMENT_STATUS');?></th> -->
	                <th><?php echo JTEXT::_('Branch');?></th>
	                <th ><?php echo JTEXT::_('REFUND_DATE'); ?></th> 
				 	<th ><?php echo JTEXT::_('REFUND_REASON'); ?></th> 
	            </tr>
				<?php
				$count = 0;
				for($i=0; $i < count($orderItemList); $i++)
				{
					$orderItem = $orderItemList[$i];
					$buyerDetail = json_decode($orderItem->order->buyer_detail);
			
					for($j=0; $j < count($orderItem->invtyList); $j++)
					{
						$invty = $orderItem->invtyList[$j];
						$count++;
				?>
	            <tr>
	                <td><?php echo JHTML::_('grid.id', $count - 1, $invty->id );?></td>
	                <td><?php echo $buyerDetail->name; ?></td>
	              <!--   <td><?php echo $orderItem->order->description; ?></td> -->
	                <td><?php echo DatetimeWrapper::getDisplayDatetime($orderItem->created_at); ?></td>
	                <td><?php echo $invty->name; ?></td>
	                <td>
						<?php
							if($orderItem->status=='Refunded'){
								echo "<b>";echo JTEXT::_('COUPON_REFUNDED'); echo "</b>";
							}else{
								if($invty->status=="Used")
								{ 
									echo "<b>";echo JTEXT::_('COUPON_'.$invty->status); echo "</b>";
								}
								else
									echo JTEXT::_('COUPON_'.$invty->status);
							}
						?>
					</td>
	               <!--  <td>
						<?php 
							if($invty->status != EnmasseHelper::$INVTY_STATUS_LIST['Used'] && $invty->settlement_status == EnmasseHelper::$MERCHANT_SETTLEMENT_STATUS_LIST['Not_Paid_Out'])
							{
								echo JText::_("MERCHANT_COUPON_CAN_NOT_PAID_OUT");
							}elseif ($invty->status == EnmasseHelper::$INVTY_STATUS_LIST['Used'] && $invty->settlement_status == EnmasseHelper::$MERCHANT_SETTLEMENT_STATUS_LIST['Not_Paid_Out'])
							{
								echo JText::_("MERCHANT_COUPON_CAN_BE_PAID_OUT");
							}
							elseif ($invty->settlement_status == EnmasseHelper::$MERCHANT_SETTLEMENT_STATUS_LIST['Should_Be_Paid_Out'])
							{
								echo JText::_("MERCHANT_COUPON_WATTING_FOR_PAID_OUT");
							}elseif ($invty->settlement_status == EnmasseHelper::$MERCHANT_SETTLEMENT_STATUS_LIST['Paid_Out'])
							{
								echo JText::_("MERCHANT_COUPON_PAID_OUT");
							}
						?>
					</td> -->
					<td><?php echo $orderItem->order->branch; ?></td>
					<td><?php echo DatetimeWrapper::getDisplayDatetime($orderItem->refund_date); 
                    	if(strtotime($orderItem->refund_date)){
                    		echo " ".date("H:i:s",strtotime($orderItem->refund_date));
                    		
                    		
                    	}
                    ?></td>
					<td><?php echo $orderItem->refund_reason; ?></td>
	            </tr>
				<?php
					}
				}
				?>
	        </table>
			<!-- <input type="button" class="em_btn" value="<?php echo JText::_('MERCHANT_PAY_OUT_SELECTED_COUPONS_BUTTON')?>" onclick="javascript:{if(document.adminForm.boxchecked.value == 0) {alert('<?php echo JText::_('MERCHANT_NO_COUPON_SELECTED_MSG')?>'); return}else if(confirm('<?php echo JText::_('MERCHANT_PAY_OUT_COUPONS_CONFIRM_MSG')?>')){document.adminForm.task.value = 'payOutCoupons';document.adminForm.method='post';document.adminForm.submit()} }" /> -->
	   </form>	

<?php } ?>
<br/>


					</div> 
				</div>
            </div>    
</div>


	
	</div><!--em_innerBlock-->

