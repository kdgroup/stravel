/**
 * @description:  Call functions of website
 * @version: 1.0 
 **/
 
/****************************
 ***** Global variables *****
 ****************************/
var emProject = {};	

/*************************************
 ***** Main functions start here *****
 *************************************/
jQuery(document).ready(function(){	
	emProject.initForm.initFormSearch();
	emProject.initForm.initfrmContact();	
	emProject.initElements.nivoSlider();
	emProject.initElements.dropdownMenu();
	emProject.initElements.initVote();
	emProject.ValidateInitForm.initfrmCheckout();
	jCustomCheckbox(jQuery('.checkButtonRemember'));
	jQuery("#delivery_date").datepicker({
            dateFormat: 'dd-mm-yy'
        });
	jQuery('#delivery_time').datetimepicker({
		datepicker:false,
		format:'H:i',
		startHour: 6  
	});
});

/******************************
 ***** Show error if have *****
 ******************************/
window.onerror = errorHandler;
function errorHandler(msg, url, l){
	if (window.console){
		console.log(msg + " : " + url + " : " + l);
	}
	return true;
}

/******************************
 ***** Functions built in *****
 ******************************/ 
 emProject.ValidateInitForm = {
	initfrmCheckout: function(){
		$("#adminForm").tooltip({
			show: false,
			hide: false,
			position: { my: "left top+2", at: "left bottom", collision: "flipfit" }
		});
		$("#adminForm").validate({
			showErrors: function(map, list) {
				var focussed = document.activeElement;
				if (focussed && $(focussed).is("input, textarea")) {
					$(this.currentForm).tooltip("close", { currentTarget: focussed }, true)
				}
				this.currentElements.removeAttr("title").removeClass("ui-state-highlight");
				$.each(list, function(index, error) {
					$(error.element).attr("title", error.message).addClass("ui-state-highlight");
				});
				if (focussed && $(focussed).is("input, textarea")) {
					$(this.currentForm).tooltip("open", { target: focussed });
				}
			},
		    rules: {
				"name": {
					required: true
				},
				"email": {
					required: true,
					email: true
				},
                "email3": {
					required: true,
					email: true
				},
                "reemail3": {
					required: true,
					email: true,
                    equalTo: "#email3"
				},
				"receiver_phone": {
					required: true,
					number: true,
					maxlength: 8,
					minlength: 8
				},
				"receiver_address":{
					required: true
				}
				,"post_code":{
					required: true,
					number: true,
					maxlength: 6,
					minlength: 6
				}
				,"delivery_date":{
					required: true,
					dateformat: true
				}
				,"delivery_time":{
					required: true,
					datetime: true
				}
                ,"terms":{
					required: true,
					maxlength: 2
				}  
			},
    		messages: {
				"receiver_phone": "Please enter a phone number (8 number)",
				"receiver_address": "Please enter address",
				"post_code": "Please enter post code (6 number)",
				"terms": "Please check agree terms & conditions"
			},
            errorElement: "div"
			
			
		});
		$.validator.addMethod(
			"dateformat",
			function(value, element) {
				return this.optional(element) || /^\d\d?\-\d\d?\-\d\d\d\d$/.test(value)
				//return value.match(/^\d\d?\-\d\d?\-\d\d\d\d$/).test(value);
			},
			"Date in the format dd-mm-yyyy."
		);
		$.validator.addMethod("datetime", function(value, element) {  
				return this.optional(element) || /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/i.test(value);  
			}, "Please enter a valid time."
		);
	}
 }

emProject.initForm = { 
	initfrmContact: function(){
		var strId = 'frmContact';
		var elements = [{
			field: 'txtLastName',
			valid: 'required',
			messages: 'Please input your last name...'
		},{
			field: 'txtEmail',			
			valid: 'required|email',
			messages: 'Please input your email...' + '|' + 'Email is invalid'
		},{
			field: 'txtPhone',			
			valid: 'required|content',
			messages: 'Please input your phone...' + '|'
		}];
		jValidateForm(strId, elements);
	},
	initFormSearch: function(){
		var strId = 'frmSearch';
		var elements = [{
			field: 'txtSearch',
			init: 'Search...',
			valid: 'required',
			messages: 'Please insert search text!'
		}];
		jValidateForm(strId, elements);
	}
};

emProject.initElements = {
	nivoSlider: function(){
		jQuery(jQuery('.nivoSlider img')[0]).css('display','block');
		jQuery(jQuery('.nivoSlider img')[0]).load(function() {
        	loadNivo();
    	});
		setTimeout(function(){
			loadNivo();
		}, 2000);
		function loadNivo(){
			jQuery('.nivoSlider img').css('display','block');
			$('#slider').nivoSlider({directionNav: false, pauseOnHover: false, pauseTime: 7500, effect: 'fade'});
			isLoadNivo=true;
		}
	},
	dropdownMenu: function(){
		$(".nav li").hover(function(){
			$(this).find('.em_subNav').css({display: "none"}).stop().fadeIn(500);
			if ($(this).find('ul').hasClass('em_subNav'))
				$(this).addClass("activeTab");
        },function(){ 
			$(this).find('.em_subNav').stop().fadeOut(300);
			$(this).removeClass("activeTab");
        }); 
	},
	initVote: function(){
		var els = jQuery('.rating');
		
		els.each(function(pos, el){
			if(!jQuery(el).hasClass('disabled')){
				var stars = jQuery(el).children();
				//get current vote
				el.curVote = stars.filter(function(i, st){
					return jQuery(st).hasClass('filled');
				});
				el.curVote = el.curVote.length-1;
				//set event
				stars.each(function(pos1, st){
					jQuery(st).bind({
						'mouseover': function(e){                        
							fillToPosition(pos1, stars);						
						},
						'click': function(e){
							e.preventDefault();
							if(jQuery('#nRating').length > 0){
								fillToPosition(pos1, stars, 'clicked', el);
								jQuery('#nRating').attr('value', pos1+1);
							}
						}
					});
				});
				jQuery(el).bind('mouseleave', function(e){
					emptyAll(stars, el);			
				});		
			}        
		});
		function fillToPosition(pos, lst, action, parent){
			lst.removeClass('filled');
			lst.each(function(index, el){
				if(index <= pos){              
					jQuery(el).addClass('filled');
				}
			});
			if(action == 'clicked'){
				parent.clicked = pos;
			}
		}
		function emptyAll(lst, parent){
			lst.each(function(index, el){
				if(parent.clicked!=null){
					if(index > parent.clicked){
						jQuery(el).removeClass('filled');				
					}else{
						jQuery(el).addClass('filled');
					}
				}else{
					if(index > parent.curVote){
						jQuery(el).removeClass('filled');
					}
				}
			});
		}
	}
}

