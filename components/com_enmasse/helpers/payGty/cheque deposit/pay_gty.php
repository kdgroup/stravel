<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
$cart = $this->cart;
?>
<style>
    .em_dealMain .bannergroup{
        display: none;
    }
</style>
<h3>Payment</h3>
<div class="em_innerBlock em_paymentPage">
	<?php
    	$application = JFactory::getApplication();
    	// Add a message to the message queue
    	$application->enqueueMessage(JText::_('CASH_ORDER_SUCCESS'), 'message');
?>	
	<div class="em_innerBlock em_paymentPage">
		<div class="em_borBlock em_payID">
                    <?php echo '<b>'.JTEXT::_('ORDER_ID').':</b> '.$this->orderId;?><br>
                    <?php 
                        $voucher = $cart->getVoucher();
                        if($voucher){ 
                            if($voucher->type == 'Percentage'){
                                $discount = $cart->getTotalPrice() * 100 / (100 - $voucher->discount_amount) - $cart->getTotalPrice();
                            }
                            else {
                                $discount = $voucher->discount_amount;
                            }
                            $total_price = $cart->getTotalPrice() + $discount;
                    ?>
                        <b><?php echo JText::_('SHOP_CARD_TOTAL_PRICE');?>:</b> <?php echo EnmasseHelper::displayCurrency($total_price);?><br>
                        <b><?php echo JText::_('SHOP_CARD_DISCOUNT');?>:</b> <?php echo EnmasseHelper::displayCurrency($discount);?><br>
                    <?php } ?>
                    <?php echo '<b>'.JTEXT::_('SHOP_CARD_GRAND_TOTAL').':</b> '.EnmasseHelper::displayCurrency($this->cart->getTotalPrice());?>
                    
                    <table cellpadding="10" style="min-width:50%; margin-left: 20px;">
                        <tr>
                            <td><?php echo '<b>'.JTEXT::_('DEAL_NAME').'</b> '; ?></td>
                            <td style="text-align:center;"><?php echo '<b>'.JTEXT::_('SHOP_CARD_ITEM').'</b>'; ?></td>
                            <td style="text-align:center;"><?php echo '<b>'.JTEXT::_('SHOP_CARD_PRICE').'</b>'; ?></td>
                        </tr>
                        <?php 
                            $oDealModel = JModelLegacy::getInstance('deal','enmasseModel');
                            foreach($cart->getAll() as $i => $cartItem){ 
                                $item = $cartItem->getItem();
                                $deal = $oDealModel->getById($item->id);
                                //tier pricing
                                if (($item->tier_pricing)==1)
                                {	
                                        $item->price = EnmasseHelper::getCurrentPriceStep(unserialize($item->price_step),$deal->price,$item->cur_sold_qty);;
                                        $item->types =NULL;
                                        $calculatorPrice = EnmasseHelper::getCalculatorPrice(unserialize($item->price_step),$item->price,$item->cur_sold_qty,$cartItem->getCount(),$deal->price);
                                        $printOut = "";
                                        $num_step_price = 0;
                                        foreach($calculatorPrice as $k => $v)
                                        {
                                                if($k != 'newPrice' && $k != 'totalPrice')
                                                {
                                                    if($num_step_price > 0){
                                                        $printOut.= "+(".$v."x".$k.")";
                                                    } else {
                                                        $printOut.= "(".$v."x".$k.")";
                                                    }
                                                    $num_step_price++;
                                                }
                                        }
                                        $totalPrice = $calculatorPrice['totalPrice'];
                                }
                            ?>
                            <tr>
                                <td><?php 
                                        echo $item->name;
                                        if (is_object( $item->type ))
                                        {
                                                echo ' ('.$item->type->name .')';
                                        } 
                                    ?>
                                </td>
                                <td style="text-align:center;"><?php echo $cartItem->getCount(); ?></td>
                                <td style="text-align:center;"><?php if (($item->tier_pricing)==1){echo EnmasseHelper::displayCurrency($totalPrice);}else{echo EnmasseHelper::displayCurrency($cartItem->getCount()*$item->price);} ?></td>
                            </tr>
                        <?php } ?>
                    </table>
        </div><!--em_borBlock-->
		<div class="em_borBlock em_payDetails">
        	<h4><?php echo JTEXT::_('CASH_PAY_INFO'); ?></h4>
            <div class="payContent">
            	<?php echo $this->attributeConfig->instruction;?>
            </div>
        </div><!--em_borBlock-->
	</div>
	<div class="deal_bottom"></div>
</div>
<?php 
 $this->cart->deleteAll();
 JFactory::getSession()->set('cart', null);
?>