<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$point = $this->cart->getPoint();
$cartItem = array_pop($this->cart->getAll());
// $price = number_format($cartItem->item->price * $cartItem->item->prepay_percent / 100, 2) * $this->cart->getTotalItem();
$price = $this->cart->getTotalPrice();
$price = $price + $point;
if($cartItem->item->deposit){
  $price = $cartItem->item->deposit*$this->cart->getTotalItem();
}
$templateUrl = JURI::base()."templates/stravel/";
$templateUrlDeal = $templateUrl."images/deal/";

?>
 <style>
.em_dealMain .bannergroup img{
    width: 100%;
}
.site #maincontent{background: none;background-color: #f3f3f4} 
</style>
<div class="em_innerBlock em_allDealPage container cart order-confirm">

  <h3 class="home-title-icon"><img src="<?php echo $templateUrlDeal.'deal-home.png';?>"/>團購優惠</h3>
    <div class="row flow-bar">
        <div class="col-sm-3  col-xs-6  text-center ">
            選擇產品
            <br/>
            <span>1</span>
        </div>
        <div class="col-sm-3  col-xs-6  text-center">
            核實訂單 
            <br/>
            <span>2</span>
        </div>
        <div class="col-sm-3  col-xs-6  text-center"> 
            輸入資料 
            <br/>
            <span>3</span>
        </div>
        <div class="col-sm-3  col-xs-6 active text-center"> 
            付款/成功訂購 
            <br/>
            <span>4</span>
        </div>
        <div class="line"></div>
        <div class="clear"></div>
    </div>
    <div class="row row-item result-page">
        <div class="col-sm-10 first" style="">
          <h3 class=" text-center">
            <?php echo JText::_('PAYPAL_REDIRECT'); ?>  
          </h3>

        </div>
       
    </div>
    
    
</div>

<div style="margin-top:0px">
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" name="paymentForm">
<input type="hidden" name="cmd" value="_xclick">
<input type="hidden" name="business" value="<?php echo $this->attributeConfig->merchant_email; ?>">
<input type="hidden" name="lc" value="<?php echo $this->attributeConfig->country_code; ?>">
<input type="hidden" name="button_subtype" value="services">
<INPUT TYPE="hidden" name="charset" value="utf-8">
<input type="hidden" name="no_note" value="1">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="rm" value="2">
<input type="hidden" name="discount_amount" value="<?php echo $point; ?>">
<INPUT TYPE="hidden" NAME="display" value="0">
<INPUT TYPE="hidden" NAME="amount" value="<?php echo $price; ?>">
<INPUT TYPE="hidden" NAME="quantity" value="<?php echo 1;//$this->cart->getTotalItem();?>">
<INPUT TYPE="hidden" NAME="currency_code" value="<?php echo $this->attributeConfig->currency_code; ?>">
<!-- <input type="hidden" name="tax" value="<?php echo $this->cart->getInsuranceFee();?>"> -->
<INPUT TYPE="hidden" NAME="cbt" value="Return back to <?php echo $this->systemName ;?>">
<input type="hidden" name="item_name" value="Make Purchase from <?php echo $this->systemName ;?>">
<input type="hidden" name="item_number" value="<?php echo $this->orderDisplayId; ?>">
<INPUT TYPE="hidden" NAME="return" value="<?php echo $this->returnUrl;?>">
<INPUT TYPE="hidden" NAME="cancel_return" value="<?php echo $this->cancelUrl; ?>">
<INPUT TYPE="hidden" NAME="notify_url" value="<?php echo $this->notifyUrl; ?>">

<input type="hidden" name="bn" value="Matamko_Channel_EC_SG">
<img alt="Paypal Payment Gateway" border="0" src="https://www.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1">
</form>

</div>
<script>
    setTimeout(function(){document.paymentForm.submit()}, 3000);    
</script>
<?php 
	$this->cart->deleteAll();
	JFactory::getSession()->set('cart', null);
?>