<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
 
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 
?>	
	<p >
	<span ><?php echo JText::_('COM_ENMASSE_EMAIL');?>&nbsp;</span><input type="text" name="jform[email]" class="validate-email required" id="jform_email" value="" onclick="this.value=''" size="30" required="required">
	</p>
	<p>
		<span ><?php echo JText::_('COM_ENMASSE_NAME');?>&nbsp;</span><input type="text" name="jform[name]" class="required" id="jform_name" value="" onclick="this.value=''" size="30"  required="required">
	</p>