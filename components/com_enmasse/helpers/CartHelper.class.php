<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 


class CartHelper
{
	public static function checkGty($payGty)
	{
		$payClass 	= $payGty->class_name;
		$className 	= 'PayGty'.ucfirst($payClass);
		require_once JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."payGty". DS .$payClass. DS .$className. ".class.php";

		if (call_user_func_array(array($className, "checkConfig"), array($payGty)) )
			return true;	
		else
		{
			return false;
		}
	}

	public static function checkCart($cart)
	{
		if ( empty($cart) || $cart->getTotalItem() == 0 )
		{
			$msg = JText::_( "CART_IS_EMPTY");
			$link = JRoute::_("index.php?option=com_enmasse&controller=deal", false);
			JFactory::getApplication()->redirect($link, $msg);
		}
	}

	public static function saveOrder($cart, $user, $payGty, $payGtyDetail, $deliveryGty, $deliveryDetail, $status, $guest_buying='0',$insurance_fee="",$branch="")
	{
		$db = JFactory::getDBO();
		$cartItem = array_pop($cart->getAll());
		
		$__data =new stdClass();
		$__data->total_buyer_paid 	= $cart->getTotalPrice();
		$__data->point_used_to_pay 	= $cart->getPoint();
		$__data->status 			= $status;
		$__data->session_id 		= JFactory::getSession()->getId();
		
		$__data->buyer_id 			= $user['id'];
		$__data->buyer_detail 		= json_encode($user);				
		
		$__data->referral_id 		= $cart->getReferralId();
		$__data->pay_gty_id 		= $payGty->id;
		$__data->delivery_gty_id 	= $deliveryGty->id;
		$__data->delivery_detail 	= json_encode($deliveryDetail);
		
		$__data->created_at = DatetimeWrapper::getDatetimeOfNow();
		$__data->updated_at = DatetimeWrapper::getDatetimeOfNow();
		
                $__data->paid_amount = $cart->getTotalPrice();
                $__data->insurance_fee = $insurance_fee;
                $__data->branch = $branch;
		//$__data->paid_amount = round($cartItem->getCount() * $cartItem->item->price * $cartItem->item->prepay_percent / 100,2);
		
		$db->insertObject( '#__enmasse_order', $__data, 'id');
				
		if ($db->getErrorNum())
		{
			echo $db->stderr();
			return false;
		}
		else
			return $__data;
	}

	public static function saveOrderItem($cart, $order, $status)
	{
		$orderItemList = array();
		$db = JFactory::getDBO();
		foreach ($cart->getAll() as $id=>$cartItem)
		{
			$deal = JModelLegacy::getInstance('deal', 'enmasseModel')->getById($cartItem->getItem()->id);
			
			$__data 				= new stdClass();
			$__data->signature 		= $id;
			$__data->description 	= $deal->name ;
			$__data->unit_price 	= $cartItem->getItem()->price;
			$__data->qty 			= $cartItem->getCount();
			$__data->total_price 	= $cartItem->getTotalPrice();
			$__data->pdt_id 		= $cartItem->getItem()->id;
			if($cartItem->getItem()->type)
			{
				$type = $cartItem->getItem()->type;
				$attr_info = array($type->id,$type->code);
				$__data->attr_info		= implode("|",$attr_info);
				$__data->description = $deal->name . '('. $type->name .')';
			}
			if(!empty($cartItem->getItem()->orderTypes)){
				$order_types = json_encode($cartItem->getItem()->orderTypes);
				$__data->order_types = $order_types;

				foreach ($cartItem->getItem()->orderTypes as $key => $orderType) {
					$r = JModelLegacy::getInstance('dealType', 'enmasseModel')->addQtySold($orderType->id,$orderType->orderQty);
				}
				
			}
			$__data->order_id 		= $order->id;
			$__data->status 		= $status;
			$__data->created_at = $order->created_at;
			$__data->updated_at = $order->updated_at;
			$db->insertObject( '#__enmasse_order_item', $__data, 'id');
			if ($db->getErrorNum())
			{
				echo $db->stderr();
				return false;
			}
			array_push($orderItemList, $__data);
		}
		return $orderItemList;
	}
	// to update the coupon in inventorry to located to an order
	public static function allocatedInvty($orderItemList,$deallocated_at,$status)
	{
		$db = JFactory::getDBO();
		$freeCouponList = JModelLegacy::getInstance('invty','enmasseModel')->getCouponFreeByPdtID($orderItemList[0]->pdt_id);
		
		// for($i=0 ; $i<$orderItemList[0]->qty ; $i++)
		for($i=0 ; $i<1 ; $i++)
		{
		 $sequent = $i+1;

		  // get random number change by Duc
            $length = 6;
            $characters = '0123456789012345678909';
            $characters2 = 'abcdefghlijmnopqrstuvwxyzabcdefghlijmnopqrstuvwxyzaz';
            $string = '';    
           for ($p = 0; $p < 3; $p++) {
                 $string .= $characters2[rand(1, 30)];
            }
            for ($p1 =  0; $p1 < 3; $p1++) {
                 $string .= $characters[rand(1, 10)];
            }

            $name=$string;   
 

		 $query = 'UPDATE 
		 				#__enmasse_invty
		           SET
		                order_item_id = '.$orderItemList[0]->id.',name ="'.$name.'",deallocated_at="'.$deallocated_at.'" , status="'.$status.'"
		           WHERE
		                id = '.$freeCouponList[$i]->id;
		
		$db->setQuery( $query );
		$db->query();
		}
		return true;
		
	}
        
        public function saveOrderVoucher($voucher_id, $order_id, $discount)
        {
            $oDb = JFactory::getDbo();
            $oQuery = $oDb->getQuery(true);

            $oQuery->insert('#__enmasse_order_voucher');
            $oQuery->set('order_id = '.$oDb->quote($order_id));
            $oQuery->set('voucher_id = '.$oDb->quote($voucher_id));
            $oQuery->set('discount_amount = '.$oDb->quote($discount));
            $oQuery->set('created_at = '.$oDb->quote(date('Y-m-d')));

            $oDb->setQuery($oQuery);
            return $oDb->query();
        }

}