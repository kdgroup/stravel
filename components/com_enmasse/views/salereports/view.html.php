<?php
/*------------------------------------------------------------------------
# En Masse - Social Buying Extension 2010
# ------------------------------------------------------------------------
# By Matamko.com
# Copyright (C) 2010 Matamko.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.matamko.com
# Technical Support:  Visit our forum at www.matamko.com
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view');

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");

class EnmasseViewSaleReports extends JViewLegacy
{
	function display($tpl = null)
	{ 
		$task = JRequest::getWord('task');
		$app		= JFactory::getApplication();
		$menus		= $app->getMenu();
		

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();
		$orderReport = $menu->params->get('order-report');
		if($orderReport){
			$task ='orderReport';
		}
		switch ($task) 
		{ 
			case 'dealReport':
			$salesPersonId = JFactory::getSession()->get('salesPersonId');
			$filter = JRequest::getVar('filter', array('name' => "", 'code' => "", 'merchant_id' => "", 'fromdate' => "", 'todate' => ""));
			$this->assignRef('filter',$filter);
			$dealList 			= JModelLegacy::getInstance('deal','enmasseModel')->searchBySaleReports($salesPersonId, $filter['name'], $filter['merchant_id'], $filter['fromdate'], $filter['todate'], $filter['code']);
			$currency_prefix 	= JModelLegacy::getInstance('setting','enmasseModel')->getCurrencyPrefix(); 
			$this->dealList = $dealList;
			$this->assignRef('currency_prefix',$currency_prefix);
			$this->statusList = EnmasseHelper::$DEAL_STATUS_LIST;
			$this->merchantList = JModelLegacy::getInstance('merchant','enmasseModel')->listAllPublished();
			$this->_setPath('template',JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."theme". DS .EnmasseHelper::getThemeFromSetting(). DS ."tmpl". DS);
			$this->_layout="sales_person_deal_report";
			parent::display($tpl);
			break;

			
			case 'orderReport':
			
			$orderTitle="checking";
			$order['order_dir'] = JRequest::getVar( 'filter_order_Dir','desc' );
			$order['order']     = JRequest::getVar('filter_order','a.created_at' );
			$branch     = JRequest::getVar('branch','' );
			$orderby = "created_at DESC";
        //if($order['order']=='dealcode'||$order['order']=='name'){
			$orderby = " ORDER BY ".$order['order']." ".$order['order_dir'];
       // }
			$dealList = JModelLegacy::getInstance('deal', 'enmasseModel')->listConfirmed();
			$this->assignRef('dealList', $dealList);

			$dealId = JRequest::getVar('deal_id');
			$this->assignRef('deal_id', $dealId);

			// $dealId = $filter['deal_id'];
			$from_at = "";
			$from_to = "";
			$type = "";
			$this->merchant = JModelLegacy::getInstance('merchant','enmasseModel')->getByID(($deal->merchant_id )? $deal->merchant_id : 1);
      //  $state = $this->get( 'state' );
            // get order values

                // var_dump($order);
                // var_dump($filter);
			if (!empty($dealId)) {
				if($dealId=='100009'){
					$orderItemList =array();

               // foreach ($dealList as $key => $value) {

					$from_at = JRequest::getVar('from_at');
					$from_to = JRequest::getVar('from_to');
					$type = JRequest::getVar('type');
					$coupon = JRequest::getVar('coupon');
					$orderItemList = JModelLegacy::getInstance('orderItem', 'enmasseModel')->listByPdtIdAndStatusAndDateCustom('all', "Delivered' OR a.status ='Refunded",$from_at,$from_to,$orderby,$branch,$type,$coupon);

                    // for ($count = 0; $count < count($orderItemListall); $count++) {
                    //     $orderItemListall[$count]->invtyList = JModelLegacy::getInstance('invty', 'enmasseModel')->listByOrderItemIdAndStatus($orderItemListall[$count]->id,$type);
                    //     $orderItemListall[$count]->order = JModelLegacy::getInstance('order', 'enmasseModel')->getById($orderItemListall[$count]->order_id);
                    //     $orderI->invtyList = JModelLegacy::getInstance('invty', 'enmasseModel')->listByOrderItemIdAndStatus($orderItemListall[$count]->id,$type);
                    //     $orderI->order = JModelLegacy::getInstance('order', 'enmasseModel')->getById($orderItemListall[$count]->order_id);
                    //     $orderItemListall[$count]->dealcode = $deal->deal_code;
                    //     $orderItemListall[$count]->dealname = $deal->name;
                    //     $orderItemList[] = $orderItemListall[$count];
                    // }
              //  }
				}else{
					$deal = JModelLegacy::getInstance('deal', 'enmasseModel')->getById($dealId);
					$this->assignRef('deal', $deal);

					$from_at = JRequest::getVar('from_at');
					$from_to = JRequest::getVar('from_to');
					$type = JRequest::getVar('type');
					$coupon = JRequest::getVar('coupon');
					$orderItemList = JModelLegacy::getInstance('orderItem', 'enmasseModel')->listByPdtIdAndStatusAndDateCustom($dealId, "Delivered' OR a.status ='Refunded",$from_at,$from_to,$orderby,$branch,$type,$coupon);

                    // for ($count = 0; $count < count($orderItemList); $count++) {
                    //     $orderItemList[$count]->invtyList = JModelLegacy::getInstance('invty', 'enmasseModel')->listByOrderItemIdAndStatus($orderItemList[$count]->id,$type);
                    //     $orderItemList[$count]->order = JModelLegacy::getInstance('order', 'enmasseModel')->getById($orderItemList[$count]->order_id);
                    //     $orderItemList[$count]->dealcode = $deal->deal_code;
                    //     $orderItemList[$count]->dealname = $deal->name;
                    // }
				}

				$this->assignRef('orderItemList', $orderItemList['orderItemList']);
			} else {
				$orderItemList = array();
				$this->assignRef('orderItemList', $orderItemList['orderItemList']);
			}


        /// load pagination
			// $pagination = JModelLegacy::getInstance('orderItem', 'enmasseModel')->getPagination($orderItemList['_total']);
			$this->assignRef('pagination', $pagination);
			$this->assignRef('order', $order);
			$this->assignRef('order_report',$orderTitle);

				// $this->merchantList = JModelLegacy::getInstance('merchant','enmasseModel')->listAllPublished();
			$this->_setPath('template',JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."theme". DS .EnmasseHelper::getThemeFromSetting(). DS ."tmpl". DS);
			$this->_layout="order_report";
			parent::display($tpl);
			break;
			default:
			$link = JRoute::_("index.php?option=com_enmasse&controller=salereports&task=dealReport", false);
			JFactory::getApplication()->redirect($link, $null);
		}		

	}
}
?>