<?php
/*------------------------------------------------------------------------
# En Masse - Social Buying Extension 2010
# ------------------------------------------------------------------------
# By Matamko.com
# Copyright (C) 2010 Matamko.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.matamko.com
# Technical Support:  Visit our forum at www.matamko.com
-------------------------------------------------------------------------*/
defined( '_JEXEC' ) or die( 'Restricted access' );
$varList 		= $this->varList;
$elementList 	= $this->elementList;
if(!EnmasseHelper::is_urlEncoded($this->bgImageUrl))
 {
 	$bgImageUrl = $this->bgImageUrl;
 }
 else
 {
	$imageUrlArr= unserialize(urldecode($this->bgImageUrl));
	$bgImageUrl = str_replace("\\","/",$imageUrlArr[0]);
 }
 
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
<style type="text/css">

div#couponImage 
{
	z-index:-100px;
	width: 100%;
	height: 100%;   
    position: relative;
    overflow: hidden;
}
div#couponImage div#couponInfo 
{
	position:absolute;
	left:0px;
	top:0px;
	width: 100%;
	height: 100%;
	z-index:100px;
}
div#button{margin-top: 30px}
</style>
<script type="text/javascript" src="<?php echo JURI::base();?>/media/system/js/mootools-core.js"></script>
<script type="text/javascript" src="<?php echo JURI::base();?>/media/system/js/mootools-more.js"></script>

    <?php
    $body = "";
    $qrcodeImage = "";
    $qrcodeNum = "";

    for($i=0 ; $i < count($elementList); $i++)
    {
        if(!isset($varList[$elementList[$i]->name]))
        {
            if($elementList[$i]->name== 'serial')
            {
                if($varList[$elementList[$i]->name] == '' || $varList[$elementList[$i]->name] == null)
                {
                    $num = 'SERIAL';
                }
                else
                {
                    $num = 	$varList[$elementList[$i]->name];
                }
                $body.='<div id="'.$elementList[$i]->id.'" name="'.$elementList[$i]->name.'" style="position: absolute; left:' .$elementList[$i]->x.'px; top:'.$elementList[$i]->y.'px; font-size:'.$elementList[$i]->font_size.'px; width:'.$elementList[$i]->width.'px; height:'.$elementList[$i]->height.'px; overflow:hidden;">';
                    $body .= '<img src="'.JURI::base() .'index.php?option=com_enmasse&controller=barcode&task=generateBarcode&num='.$num.'"/>';
                    $qrcodeNum = '<img style="vertical-align:middle" src="'.JURI::base() .'index.php?option=com_enmasse&controller=barcode&task=generateBarcode&num='.$num.'"/>';
                $body.='</div>';	
            }
            else{

            $body.='<div id="'.$elementList[$i]->id.'" name="'.$elementList[$i]->name.'" style="border: red 2px dashed; position: absolute; left:' .$elementList[$i]->x.'px; top:'.$elementList[$i]->y.'px; font-size:'.$elementList[$i]->font_size.'px; width:'.$elementList[$i]->width.'px; height:'.$elementList[$i]->height.'px">';
                $body .= "[".strtoupper($elementList[$i]->name)."]";
            $body.='</div>';
            }
        }
        elseif($elementList[$i]->name== 'serial')
        {
            if($varList[$elementList[$i]->name] == '' || $varList[$elementList[$i]->name] == null)
            {
                $num = 'SERIAL';
            }
            else
            {
            $num = 	$varList[$elementList[$i]->name];
            }
            $body.='<div id="'.$elementList[$i]->id.'" name="'.$elementList[$i]->name.'" style="position: absolute; left:' .$elementList[$i]->x.'px; top:'.$elementList[$i]->y.'px; font-size:'.$elementList[$i]->font_size.'px; width:'.$elementList[$i]->width.'px; height:'.$elementList[$i]->height.'px;overflow:hidden;">';
                $body .= '<img src="'.JURI::base() .'index.php?option=com_enmasse&controller=barcode&task=generateBarcode&num='.$num.'"/>';
                $qrcodeNum= '<img style="vertical-align:middle" src="'.JURI::base() .'index.php?option=com_enmasse&controller=barcode&task=generateBarcode&num='.$num.'"/>';
            $body.='</div>';	
        }
        else
        {
        $body.='<div id="'.$elementList[$i]->id.'" name="'.$elementList[$i]->name.'" style="position: absolute; left:' .$elementList[$i]->x.'px; top:'.$elementList[$i]->y.'px; font-size:'.$elementList[$i]->font_size.'px; width:'.$elementList[$i]->width.'px; height:'.$elementList[$i]->height.'px;overflow:hidden;">';
            $body .= $varList[$elementList[$i]->name];
        $body.='</div>';
        }
        
        /** phuocndt
         * Begin QR Code
         */
        if($elementList[$i]->name == 'qr_code')
        {
            if($varList[$elementList[$i]->name] == '' || $varList[$elementList[$i]->name] == null)
            {
                $val = 'COUPON_SERIAL';
            }
            else
            {
                $val = 	$varList[$elementList[$i]->name];
            }
            $body.='<div id="'.$elementList[$i]->id.'" name="'.$elementList[$i]->name.'" style="position: absolute; left:' .$elementList[$i]->x.'px; top:'.$elementList[$i]->y.'px; font-size:'.$elementList[$i]->font_size.'px; width:'.$elementList[$i]->width.'px; height:'.$elementList[$i]->height.'px;overflow:hidden;">';
                $body .= '<img src="'.JURI::base() .'index.php?option=com_enmasse&controller=qrcode&task=generateQrcode&val='.$val.'"/>';
            $body.='</div>';	
            $qrcodeImage = '<img style="vertical-align:middle" src="'.JURI::base() .'index.php?option=com_enmasse&controller=qrcode&task=generateQrcode&val='.$val.'"/>';
        }
    }
  //  echo $body;
    ?>
    
<script type="text/javascript">
	function printContent(id){
		document.getElementById('button').style.display = 'none';
		window.print();
		document.getElementById('button').style.display = 'inline';
	}
</script>
<?php if(JRequest::getVar('editor')==true): ?>
<script type="text/javascript">
	function initCouponEditor(){
		var cont = $('couponInfo');
		if(!cont) return;
		
		//initialize
		var mdTop = 0;
		var mdLeft = 0;
		var mdWidth = 0;
		var mdHeight = 0;
		var curEl = null;
		//create resize icon
		var divResize = new Element('div', {
			'class': 'divResize',
			'styles': {
				'position': 'absolute',
				'bottom': 0,
				'right': 0,
				'width': 10,
				'height': 10,
				'cursor': 'se-resize'
			}
		});
		//inject resize icon
		var els = cont.getChildren();		
		els.each(function(el, index){
			var clone = divResize.clone();
			clone.inject(el);
			clone.addEvents({
				'mouseenter': function(){
				},
				'mouseleave': function(){
				},
				'mousedown': function(e){
					mdTop = e.client.y;
					mdLeft = e.client.x;
					mdWidth = this.getParent().getSize().x;
					mdHeight = this.getParent().getSize().y;
					this.getParent().resizing = true;
					curEl = this.getParent();
					curEl.setStyle('z-index', 1000);
					curEl.removeEvents();
				}				
			});
			el.setStyles({
				'cursor': 'move',
				'z-index': 999
			});
			el.makeDraggable();
		});
		cont.addEvents({
			'mousemove': function(e){
				if(curEl && curEl.resizing){
					var size = curEl.getSize();
					curEl.setStyles({
						'width': mdWidth + e.client.x - mdLeft,
						'height': mdHeight + e.client.y - mdTop
					});
				}
			},
			'mouseup': function(e){
				if(curEl){
					var size = curEl.getSize();
					curEl.setStyles({
						'width': mdWidth + e.client.x - mdLeft,
						'height': mdHeight + e.client.y - mdTop
					});
					curEl.resizing = false;
					curEl.removeEvents();
					curEl.makeDraggable();
					curEl.setStyle('z-index', 999);
					curEl = null;
				}
			}
		});
	}
	window.addEvent('domready', function(e){
		initCouponEditor();
	});
</script>
<?php endif; ?>
	<!-- <div id="button">
	<form>
		<input type="button" value="<?php echo JText::_('COUPON_PRINT_BUTTON');?>" onClick="printContent('content')">
	</form>
	</div> -->




	<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 


require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."DatetimeWrapper.class.php");
require_once(JPATH_SITE . DS . "components". DS ."com_enmasse". DS ."helpers". DS ."DealType.class.php"); 
$is_listing = '1';
$dealID = $this->deal->id;
require_once(JPATH_SITE . DS."components". DS ."com_enmasse".DS."theme".DS."dark_blue".DS."tmpl".DS."background_image.php");

//contruct deal image url 
$imageUrlArr = array();
if(EnmasseHelper::is_urlEncoded($this->dealDetail->pic_dir)){
	$imageUrlArr = unserialize(urldecode($this->dealDetail->pic_dir));
}else{
	$imageUrlArr[0] = $this->dealDetail->pic_dir;
}
$imageUrl = JURI::root().str_replace("\\","/",$imageUrlArr[0]);
$this->dealDetail->image= '<img width="240" height="197" data-thumb="'.$imageUrl.'" src="'.$imageUrl.'" alt="'. $this->dealDetail->name.'" />';

?>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<div style="margin:0 auto">
	<table cellspacing="1" cellpadding="1" width="780" border="0">
	    <tbody>
	        <tr>
	            <td style="padding: 10px 25px"><img src="http://www.stravel.com.hk/templates/stravel/images/logo.png" alt="" /></td>
	            <td align="right" style="padding: 10px 25px"><font size="4">客戶服務 Customer Service</font><br />
	            55426866｜customerservice@stravel.com.hk 			<br />
	            </td>
	        </tr>
	    </tbody>
	</table>
	<p>&nbsp;</p>
	<table cellspacing="1" cellpadding="1" width="780" border="0" bgcolor="B279B4">
	    <tbody>
	        <tr>
	            <td style="padding: 10px 25px"><font color="ffffff" size="4"><?php echo $this->dealDetail->name; ?> 套票</font> | <font color="fffff" size="3">總金額: HK$<?php echo (int)$this->dealDetail->price; ?></font><hr style="height:1px; border:none; color:#000; background-color:#000;" />
	            </td>
	            <td align="right"><?php echo $qrcodeNum;?>
	            <?php echo $qrcodeImage;?>
	            </td>
	        </tr>
	        <tr>
	            <td style="padding: 10px 25px">產品包括：<br />
	            <?php echo $this->dealDetail->description; ?></td>
	            <td align="center"><?php echo $this->dealDetail->image; ?></td>
	        </tr>
	    </tbody>
	</table>
	<p>&nbsp;</p>
	<table cellspacing="1" cellpadding="1" width="780" border="0">
	    <tbody>
	        <tr>
	            <td width="50%" align="top" style="padding-right: 5px;vertical-align: top;">
	            <table cellspacing="1" cellpadding="1" width="100%" border="0" bgcolor="37B34A">
	                <tbody>
	                    <tr>
	                        <td style="padding: 10px 25px"><font color="ffffff" size="4">機票資料</font><font color="ffffff" size="2">(只供參考)</font><hr style="height:1px; border:none; color:#000; background-color:#000;" />
	                        </td>
	                    </tr>
	                    <tr>
	                        <td style="padding: 10px 25px"><?php echo $this->dealDetail->description1; ?></td>
	                    </tr>
	                </tbody>
	            </table>
	            </td>
	            <td width="50%" align="top" style="padding-left: 5px;vertical-align: top;">
	            <table cellspacing="1" cellpadding="1" width="100%" border="0" bgcolor="2AAAE1">
	                <tbody>
	                    <tr>
	                        <td style="padding: 10px 25px"><font color="ffffff" size="4">酒店詳情</font><hr style="height:1px; border:none; color:#000; background-color:#000;" />
	                        </td>
	                    </tr>
	                    <tr>
	                        <td style="padding: 10px 25px"><?php echo $this->dealDetail->description2; ?></td>
	                    </tr>
	                </tbody>
	            </table>
	            </td>
	        </tr>
	    </tbody>
	</table>
	<p>&nbsp;</p>
	<table cellspacing="1" cellpadding="1" width="780" border="0" bgcolor="F9AE41">
	    <tbody>
	        <tr>
	            <td style="padding: 10px 25px"><font color="ffffff" size="4">增值服務</font><hr style="height:1px; border:none; color:#000; background-color:#000;" />
	            </td>
	        </tr>
	        <tr>
	            <td style="padding: 10px 25px"><p>保險 (<?php echo $this->dealDetail->insurance_description; ?>);<br />
	            受保日期：<?php echo $this->dealDetail->policy_date; ?><br/>
	           	類別: 
	           	 <?php 
                    $insuranceArray = json_decode($this->order->insurance_fee);
                  
                    if(count($insuranceArray)){
                    	$insuranceName = "";
                    	$child = 0;
                    	$adult = 0;
                        foreach ($insuranceArray as $key => $value) {
							
							if($value->childFee){								
								$insurance_fee+=$value->childFee;								
							}
							if($value->adultFee){
								
								$adult +=$value->adultVal;
								
							}
							$insuranceName= $value->insurance_name;
						}
						echo $insuranceName;
                    }else{
                         echo '沒有選購旅遊保險';
                    }
                    
                	if(isset($adult)){
                		echo '&nbsp;&nbsp;&nbsp;成人: '.$adult.' &nbsp;&nbsp; 小童:'.$child;
                	}
                   
                    ?>
	           	

	            </p></td>
	        </tr>
	    </tbody>
	</table>
	<p>&nbsp;</p>
	<table cellspacing="1" cellpadding="1" width="780" border="0" bgcolor="F3F3F4">
	    <tbody>
	        <tr>
	            <td style="padding: 10px 25px"><font size="4">客戶資料</font><hr style="height:1px; border:none; color:#000; background-color:#000;" />
	            </td>
	        </tr>
	        <tr>
	            <td style="padding: 10px 25px">
	            <table  width="100%" >
	            	
	            	<tr>
	            		<td> <b>聯絡人資料</b></td>
	            	</tr>
	            	<tr>
	            		<td>  姓：　<?php echo $this->passengerInfo->surname3;?>　</td>
	            		<td> 名：　<?php echo $this->passengerInfo->name3;?></td>
	            		<td> </td>
	            		<td> </td>
	            	</tr>
	            	
	            	<tr>
	            		<td>   優惠代碼：　<?php echo $this->voucher_code;?></td>
	            		<td> 領取文件及完成餘額付款： <?php echo $this->branch->name;?></td>
	            		<td> </td>
	            		<td> </td>
	            	</tr>
	            	 <tr>
	            		<td colspan="4"><hr style="border-top: dotted 1px;" /></td>
	            	</tr>
	            	<tr>
	            		<td colspan="4">
	            			<?php 
	            			$body =  "   分店名稱： ".$this->branch->name." <br/>";
				             $body .=  "       分店地址： ".$this->branch->address."<br/>";
				             $body .=  "       辦公時間： ".$this->branch->description." <br/>";
				            $body .=  "        聯絡電話： ".$this->branch->telephone."";
				            echo $body;
				            ?>
	            		</td>

	            	</tr>
	            	
	            </table>
	           
	          
	        </tr>
	    </tbody>
	</table>
	<p>&nbsp;</p>
	<table cellspacing="1" cellpadding="1" width="780" border="0" bgcolor="F3F3F4">
	    <tbody>
	        <tr>
	            <td style="padding: 10px 25px"><font size="2">條款</font><hr style="height:1px; border:none; color:#000; background-color:#000;" />
	            </td>
	        </tr>
	        <tr>
	            <td style="padding: 10px 25px"><?php echo $this->dealDetail->terms; ?></td>
	        </tr>
	    </tbody>
	</table>
</div>

