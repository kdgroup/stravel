<?php
/*------------------------------------------------------------------------
# En Masse - Social Buying Extension 2010
# ------------------------------------------------------------------------
# By Matamko.com
# Copyright (C) 2010 Matamko.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.matamko.com
# Technical Support:  Visit our forum at www.matamko.com
-------------------------------------------------------------------------*/
defined( '_JEXEC' ) or die( 'Restricted access' );
$varList    = $this->varList;
$elementList  = $this->elementList;
if(!EnmasseHelper::is_urlEncoded($this->bgImageUrl))
{
  $bgImageUrl = $this->bgImageUrl;
}
else
{
  $imageUrlArr= unserialize(urldecode($this->bgImageUrl));
  $bgImageUrl = str_replace("\\","/",$imageUrlArr[0]);
}

?>


<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."DatetimeWrapper.class.php");
require_once(JPATH_SITE . DS . "components". DS ."com_enmasse". DS ."helpers". DS ."DealType.class.php"); 
$is_listing = '1';
$dealID = $this->deal->id;
require_once(JPATH_SITE . DS."components". DS ."com_enmasse".DS."theme".DS."dark_blue".DS."tmpl".DS."background_image.php");

//contruct deal image url 
$imageUrlArr = array();
if(EnmasseHelper::is_urlEncoded($this->dealDetail->pic_dir)){
  $imageUrlArr = unserialize(urldecode($this->dealDetail->pic_dir));
}else{
  $imageUrlArr[0] = $this->dealDetail->pic_dir;
}
$imageUrl = JURI::root().str_replace("\\","/",$imageUrlArr[0]);
$this->dealDetail->image= '<img width="240" height="197" data-thumb="'.$imageUrl.'" src="'.$imageUrl.'" alt="'. $this->dealDetail->name.'" />';

?>
<?php
$body = "";
$qrcodeImage = "";
$qrcodeNum = "";

for($i=0 ; $i < count($elementList); $i++)
{
  if(!isset($varList[$elementList[$i]->name]))
  {
    if($elementList[$i]->name== 'serial')
    {
      if($varList[$elementList[$i]->name] == '' || $varList[$elementList[$i]->name] == null)
      {
        $num = 'SERIAL';
      }
      else
      {
        $num =  $varList[$elementList[$i]->name];
      }
      $body.='<div id="'.$elementList[$i]->id.'" name="'.$elementList[$i]->name.'" style="position: absolute; left:' .$elementList[$i]->x.'px; top:'.$elementList[$i]->y.'px; font-size:'.$elementList[$i]->font_size.'px; width:'.$elementList[$i]->width.'px; height:'.$elementList[$i]->height.'px; overflow:hidden;">';
      $body .= '<img src="'.JURI::base() .'index.php?option=com_enmasse&controller=barcode&task=generateBarcode&num='.$num.'"/>';
      $qrcodeNum = '<img style="vertical-align:middle" src="'.JURI::base() .'index.php?option=com_enmasse&controller=barcode&task=generateBarcode&num='.$num.'"/>';
      $body.='</div>';  
    }
    else{

      $body.='<div id="'.$elementList[$i]->id.'" name="'.$elementList[$i]->name.'" style="border: red 2px dashed; position: absolute; left:' .$elementList[$i]->x.'px; top:'.$elementList[$i]->y.'px; font-size:'.$elementList[$i]->font_size.'px; width:'.$elementList[$i]->width.'px; height:'.$elementList[$i]->height.'px">';
      $body .= "[".strtoupper($elementList[$i]->name)."]";
      $body.='</div>';
    }
  }
  elseif($elementList[$i]->name== 'serial')
  {
    if($varList[$elementList[$i]->name] == '' || $varList[$elementList[$i]->name] == null)
    {
      $num = 'SERIAL';
    }
    else
    {
      $num =  $varList[$elementList[$i]->name];
    }
    $body.='<div id="'.$elementList[$i]->id.'" name="'.$elementList[$i]->name.'" style="position: absolute; left:' .$elementList[$i]->x.'px; top:'.$elementList[$i]->y.'px; font-size:'.$elementList[$i]->font_size.'px; width:'.$elementList[$i]->width.'px; height:'.$elementList[$i]->height.'px;overflow:hidden;">';
    $body .= '<img src="'.JURI::base() .'index.php?option=com_enmasse&controller=barcode&task=generateBarcode&num='.$num.'"/>';
    $qrcodeNum= '<img style="vertical-align:middle" src="'.JURI::base() .'index.php?option=com_enmasse&controller=barcode&task=generateBarcode&num='.$num.'"/>';
    $body.='</div>';  
  }
  else
  {
    $body.='<div id="'.$elementList[$i]->id.'" name="'.$elementList[$i]->name.'" style="position: absolute; left:' .$elementList[$i]->x.'px; top:'.$elementList[$i]->y.'px; font-size:'.$elementList[$i]->font_size.'px; width:'.$elementList[$i]->width.'px; height:'.$elementList[$i]->height.'px;overflow:hidden;">';
    $body .= $varList[$elementList[$i]->name];
    $body.='</div>';
  }

        /** phuocndt
         * Begin QR Code
         */
        if($elementList[$i]->name == 'qr_code')
        {
          if($varList[$elementList[$i]->name] == '' || $varList[$elementList[$i]->name] == null)
          {
            $val = 'COUPON_SERIAL';
          }
          else
          {
            $val =  $varList[$elementList[$i]->name];
          }
          $body.='<div id="'.$elementList[$i]->id.'" name="'.$elementList[$i]->name.'" style="position: absolute; left:' .$elementList[$i]->x.'px; top:'.$elementList[$i]->y.'px; font-size:'.$elementList[$i]->font_size.'px; width:'.$elementList[$i]->width.'px; height:'.$elementList[$i]->height.'px;overflow:hidden;">';
          $body .= '<img src="'.JURI::base() .'index.php?option=com_enmasse&controller=qrcode&task=generateQrcode&val='.$val.'"/>';
          $body.='</div>';  
          $qrcodeImage = '<img style="vertical-align:middle" src="'.JURI::base() .'index.php?option=com_enmasse&controller=qrcode&task=generateQrcode&val='.$val.'"/>';
        }
    }

  

  //  echo $body;
    function convertContent($description2){
            $allowTags ='<p><a><br><table><th><tr><td><div><strong><h2><h3><h4>';  
            $description2  = preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i",'<$1$2>', $description2);
            $description2 = strip_tags($description2,$allowTags);
            $description2 = str_replace('</p>','<br/>',$description2);
            $description2 = str_replace('<p>','',$description2);
            $description2 = str_replace('</div>','<br/>',$description2);
            $description2 = str_replace('<div>','',$description2);
            $description2 = str_replace('<table>','<table border="1" cellpadding="1" cellspacing="1">',$description2);
            $description2 = str_replace('、','、 ',$description2);
            $description2 = str_replace('，','， ',$description2);
            $description2 = str_replace('：','： ',$description2);
            $description2 = str_replace('（',' （',$description2);
            $description2 = str_replace('】','】 ',$description2);  
            return $description2;
    }
    ?>
    




<table cellspacing="0" cellpadding="0" width="780" border="0">
   <tbody>
      <tr>
         <td> 
            <table cellspacing="0" cellpadding="0" width="780" border="0">
               <tbody>
                  <tr>
                     <td style="padding: 10px 25px;width:370px;"><img src="http://www.stravel.com.hk/templates/stravel/images/logo.png" alt="" /></td>
                     <td align="right" style="padding: 10px 25px;">
                        <table style="border-width:1px; border-style:solid; border-radius: 8px;">
                           <tr>
                              <td align="center">
                                 <font size="4">客戶服務 Customer Service</font><br>
                                 <hr style="height:1px; border:none; color:#000; background-color:#000;text-align:right;margin-right:0" />
                                 <font style="text-align:justify;" size=2>熱線：55426866 ｜ 流動電話：*3232<br>
                                    電郵：info@stravel.com.hk
                                 </font>
                                 <br />
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
      <tr>
         <td> 
            <table cellspacing="0" cellpadding="0" width="780" style="border-width:1px; border-style:solid;">
               <tbody>
                  <tr>
                     <td style="padding: 10px 25px;width:408px;">
                        <table>
                           <tbody>
                            <?php 
                  $insuranceArray = json_decode($this->order->insurance_fee);
                  $insuranceName = "";
                  $insurance_fee = 0;
                  $child = 0;
                  $adult = 0;
                  if(count($insuranceArray)){



                    foreach ($insuranceArray as $key => $value) {

                      if($value->childFee){               
                        $insurance_fee+=$value->childFee; 
                        $child +=$value->childtVal;             
                      }
                      if($value->adultFee){

                        $adult +=$value->adultVal;
                        $insurance_fee+=$value->adultFee; 

                      }
                      $insuranceName= $value->insurance_name;
                    }

                  }else{
                    $insuranceName= '沒有選購旅遊保險';
                  }
                  $insuranceNumber ="";
                  if(isset($adult)){
                    $insuranceNumber = '&nbsp;&nbsp;&nbsp;成人: '.$adult.' &nbsp;&nbsp; 小童:'.$child;
                  }

                  ?>    <tr>
                    <td colspan="3"><font size="4"><?php echo $this->dealDetail->name; ?> 套票</font> <hr style="height:1px; border:none; color:#000; background-color:#000;" /></td>
                  </tr>
                  <?php 
                                if($this->orderItem->order_types){
                                    $orderTypes = json_decode($this->orderItem->order_types);
                                    foreach ($orderTypes as $key => $orderType) {
                                       echo '<tr>';
                                       echo '<td>'.$orderType->name.'</td>';
                                        echo '<td> 套票金額: HK$'.$orderType->price.'</td>';
                                       echo '<td> 數量：'.$orderType->orderQty.'</td>';
                                       echo '</tr>';
                                    }
                                }
                                ?>
                  <tr>
                    <td>套票金額: HK$<?php echo $this->order->unit_price; ?></td>
                    <td>保險費用: HK$<?php echo $insurance_fee;?></td>
                    <td>優惠代碼: -HK$<?php echo $this->order->point_used_to_pay;?></td>
                  </tr>
                  <tr>
                    <td>購買數量: <?php echo $this->order->productQty;?></td>
                    <td><?php echo $insuranceNumber;?></td>
                    <td>總金額: HK$<?php echo $this->order->paid_amount;?></td>
                  </tr>
                  <?php 
                    if($this->dealDetail->deposit){
                  ?>
                  <tr>
                                            
                    <td colspan="2">訂金: <?php echo "$".((int)$this->dealDetail->deposit * (int)$this->order->productQty);?></td>
                    <td>餘額: <?php echo "$".((int)$this->order->paid_amount - ((int)$this->dealDetail->deposit * (int)$this->order->productQty));?></td>
                                           
                  </tr>
                  <?php }?>
                  <tr>
                    <td colspan="3">(*以上價格未包各項稅款及附加費用) 
                      <hr style="height:1px; border:none; color:#000; background-color:#000;" />
                    </td>
                  </tr>
                             
                              
                           </tbody>
                        </table>
                     </td>
                     
                     <td style="width: 200px;" align="right">
                        <table>
                            <tbody>
                                <tr>
                                    <td style="padding-top: 5px;"><?php echo $qrcodeNum;?></td>
                                    <td style="padding-top: 5px;"><?php echo $qrcodeImage;?></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align:center"><?php echo "000000000".$this->order->id;?></td>
                                   
                                </tr>
                            </tbody>
                        </table>
                     </td>
                  </tr>
                  <tr>
                    
                     <td colspan="2" style="padding: 10px 25px;width:650px;">產品包括：<br />
              <?php 
            echo convertContent($this->dealDetail->description);
            
              
            // echo strip_tags($this->dealDetail->description,'<p>'); ?>
              
            </td>

            
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
      <?php if($this->dealDetail->description1){?>
      <tr>
         <td>
            <table cellspacing="0" cellpadding="0" width="780" style="border-width:1px; border-style:solid;">
               <tbody>
                  <tr>
                     <td style="padding: 10px 25px;width:650px;">
                        <font size="4">機票資料</font><font size="2">(只供參考)</font>
                        <hr style="height:1px; border:none; color:#000; background-color:#000;" />
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 10px 25px;width:650px;">
                        <?php 
                       echo  convertContent($this->dealDetail->description1);
             
              
              // echo strip_tags($this->dealDetail->description1,'<p>'); ?>
                     </td>
                  </tr>
                  
               </tbody>
            </table>
         </td>
      </tr>
      <?php }?>
       <?php if($this->dealDetail->description2){?>
      <tr>
         <td>
            <table cellspacing="0" cellpadding="0" width="780" style="border-width:1px; border-style:solid;">
               <tbody>
                  <tr>
                     <td style="padding: 10px 25px;width: 650px;">
                        <font size="4">酒店詳情</font>
                        <hr style="height:1px; border:none; color:#000; background-color:#000;" />
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 10px 25px;width:650px;">
                        <?php 
             
            echo convertContent($this->dealDetail->description2);
              
              // echo strip_tags($this->dealDetail->description2,'<p>'); ?>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
      <?php }?>
      <?php if($this->dealDetail->showinsurance) : ?>
      <tr>
         <td>
            <table cellspacing="0" cellpadding="0" width="780" style="border-width:1px; border-style:solid;">
               <tbody>
                  <tr>
                     <td style="padding: 10px 25px">
                        <font size="4">增值服務</font>
                        <hr style="height:1px; border:none; color:#000; background-color:#000;" />
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 10px 25px;width: 650px;">
                       保險 (<?php echo strip_tags($this->dealDetail->insurance_description); ?>);<br />
              受保日期：<?php echo $this->dealDetail->policy_date; ?><br/>
              類別: 
              <?php 

              echo $insuranceName;
              echo $insuranceNumber;


              ?>              

            
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
      <?php endif; ?>
      
      <tr>
         <td>
            <table cellspacing="0" cellpadding="0" width="780" style="border-width:1px; border-style:solid;">
               <tbody>
                  <tr>
                     <td style="padding: 10px 25px;width:650px;">
                        <font size="4">聯絡人資料</font>
                        <hr style="height:1px; border:none; color:#000; background-color:#000;" />
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 10px 25px;width: 650px;">
                        <table  width="100%" >
              <tr>
                <td>  姓：　<?php echo $this->passengerInfo->surname3;?>　</td>
                <td> 名：　<?php echo $this->passengerInfo->name3;?></td>
                <td> </td>
                <td> </td>
              </tr>

              <tr>
                <td>   優惠代碼：　<?php echo $this->voucher_code;?></td>
                <td> 領取文件及完成餘額付款： <?php echo $this->branch->name;?></td>
                <td> </td>
                <td> </td>
              </tr>
              <tr>
                <td colspan="4"><hr style="border-top: dotted 1px;" /></td>
              </tr>
              <tr>
                <td colspan="4">
                  <?php 
                  $body =  "   分店名稱： ".$this->branch->name." <br/>";
                  $body .=  "       分店地址： ".$this->branch->address."<br/>";
                  $body .=  "       辦公時間： ".$this->branch->description." <br/>";
                  $body .=  "        聯絡電話： ".$this->branch->telephone."";
                  echo $body;
                  ?>
                </td>

              </tr>

                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
      <tr>
         <td>
            <table cellspacing="0" cellpadding="0" width="780" style="border-width:1px; border-style:solid;">
               <tbody>
                  <tr>
                     <td style="padding: 10px 25px;width:650px;">
                        <font size="2">條款</font>
                        <hr style="height:1px; border:none; color:#000; background-color:#000;" />
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 10px 25px;width:650px;">
                        <?php 
             echo convertContent($this->dealDetail->terms);
             
            
                           echo '<br/><a target="_blank" href="'.$this->dealDetail->master_term_url.'" title="'.$this->dealDetail->master_term.'">'.$this->dealDetail->master_term.'</a>';
                         ?>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
