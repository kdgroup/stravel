<?php
/*------------------------------------------------------------------------
# En Masse - Social Buying Extension 2010
# ------------------------------------------------------------------------
# By Matamko.com
# Copyright (C) 2010 Matamko.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.matamko.com
# Technical Support:  Visit our forum at www.matamko.com
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");

class EnmasseViewCoupon extends JViewLegacy
{
	function display($tpl = null)
	{
		$token = JRequest::getVar('token', null);
		$invtyName = JRequest::getVar('invtyName', null);
		$elementList 	= JModelLegacy::getInstance('couponElement','enmasseModel')->listAll();
		$bgImageUrl		= JModelLegacy::getInstance('setting','enmasseModel')->getCouponBg();
			
		if($invtyName!="")
		{
			$invty 			= JModelLegacy::getInstance('invty','enmasseModel')->getByName($invtyName);
			$orderItem 		= JModelLegacy::getInstance('orderItem','enmasseModel')->getById($invty->order_item_id ? $invty->order_item_id : 0);
			$order 			= JModelLegacy::getInstance('order','enmasseModel')->getById($orderItem->order_id ? $orderItem->order_id : 0);
			$deal 			= JModelLegacy::getInstance('deal','enmasseModel')->getById($invty->pdt_id ? $invty->pdt_id : 0);
			$merchant		= JModelLegacy::getInstance('merchant','enmasseModel')->getById($deal->merchant_id ? $deal->merchant_id : 0);
                        $merchant->branches = json_decode($merchant->branches);
           	
			$deliveryDetail = json_decode($order->delivery_detail);

			$varList = array();
			$varList['dealName'] 		= $orderItem->description;//$deal->name;
			$varList['serial'] 			= $invty->name;
                        $varList['security_code'] 	= $invty->security_code;
            /** phuocndt
             * Get status value of Coupon
             */
                        $varList['qr_code'] 		= $invty->name;
			$varList['detail'] 			= $deal->short_desc;
                        $varList['merchantName']    = '';
			if($merchant!= null)
			{
                            $varList['merchantName'] = $merchant->name;
                            if(isset($merchant->branches->branch1))
                            {                    
                                $varList['merchantName'] .= $merchant->branches->branch1->name;
                                $varList['merchantName'] .= '<br />' . $merchant->branches->branch1->address;
                                $varList['merchantName'] .= '<br />' . $merchant->branches->branch1->telephone;
                            }
			}
			$varList['highlight'] 		= $deal->highlight;
			$varList['personName'] 		= $deliveryDetail->name;
			$varList['term'] 			= $deal->terms;

			 $ri = JRequest::getVar('ri','');
	          $order = JModelLegacy::getInstance('order', 'enmasseModel')->getById($orderItem->order_id);
	          $order->productQty = $orderItem->qty;
	          $order->unit_price = $orderItem->unit_price;
	          $db = JFactory::getDBO();
	            $query = "SELECT * FROM #__enmasse_merchant_branch WHERE ";
	            $query .= "id=1";
	            $db->setQuery($query);
	            $item = $db->loadObject();            

	            $branch ="";
	            
	            $branches  = json_decode($item->branches);
	            foreach ($branches as $key => $value) {
	                if($value->name == $order->branch){
	                    $branch = $value;
	                    break;
	                }
	            }
	            
	            $query2 ="SELECT * FROM #__enmasse_order_item WHERE order_id = ".$order->id;
	            $db->setQuery($query2);
	            $deal = $db->loadObject();

	            $query3 ="SELECT * FROM #__enmasse_order_passenger WHERE order_id = ".$order->id;
	            $db->setQuery($query3);
	            $passengerInfo = $db->loadObject(); 

	            $voucher_code ="";
	            $query4 ="SELECT * FROM #__enmasse_deal WHERE id = ".$deal->pdt_id;
	            $db->setQuery($query4);
	            $dealDetail = $db->loadObject();
	            
	            $query5 = "select b.voucher_code from a9alo_enmasse_order_voucher a ";
				$query5.="left join a9alo_enmasse_voucher b ";
				$query5.="on a.voucher_id =b.id ";
				$query5.="where order_id = ".$order->id;
	            
	            $db->setQuery($query5);
	            $voucher = $db->loadObject();
	            if($voucher->voucher_code){
	            	$voucher_code = $voucher->voucher_code;
	            }
	          // $this->_setPath('template',JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."theme". DS .EnmasseHelper::getThemeFromSetting(). DS ."tmpl". DS);
	          // $this->_layout="order_redem";
	            
	          $this->assignRef( 'order', $order );  
	          $this->assignRef( 'orderItem', $deal );  
	          $this->assignRef( 'dealDetail', $dealDetail );  
	          $this->assignRef( 'branch', $branch );  
	          $this->assignRef( 'passengerInfo', $passengerInfo );  
	          $this->assignRef( 'voucher_code', $voucher_code );  
		} 
		else
        {
			$varList['serial'] 		= "";
            $varList['qr_code'] 	= "";
		}
        
        /** phuocndt
         * Debug QR code
         */
        //echo '<pre>'; print_r($varList); echo '</pre>'; die;
        	
		if($token != EnmasseHelper::generateCouponToken($varList['serial']))
		{
			$msg 	= JText::_(INVALID_COUPON_TOKEN);
			$link 	= JRoute::_("/", false);

			JFactory::getApplication()->redirect($link, $msg);
		}
		else
		{
			$this->assignRef('varList', $varList );
			$this->assignRef('elementList', $elementList );
			$this->assignRef('bgImageUrl', $bgImageUrl );
			
			parent::display($tpl);
			exit(0);
		}
	}

}
?>