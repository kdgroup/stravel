<?php
/*------------------------------------------------------------------------
# En Masse - Social Buying Extension 2010
# ------------------------------------------------------------------------
# By Matamko.com
# Copyright (C) 2010 Matamko.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.matamko.com
# Technical Support:  Visit our forum at www.matamko.com
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.view');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php"); 
 $language = JFactory::getLanguage();
$base_dir = JPATH_SITE.DS.'components'.DS.'com_enmasse';
$version = new JVersion;
$joomla = $version->getShortVersion();
if(substr($joomla,0,3) >= '1.6'){
    $extension = 'com_enmasse16';
}else{
    $extension = 'com_enmasse';
}
if($language->load($extension, $base_dir, $language->getTag(), true) == false)
{
	$language->load($extension, $base_dir, 'en-GB', true);
} 
class EnmasseViewDealToday extends JViewLegacy
{
    const CS_SESSION_LOCATIONID = 'CS_SESSION_LOCATIONID';

    function display($tpl = null)
    {
      	$nLocId = JRequest::getInt(self::CS_SESSION_LOCATIONID, null, 'COOKIE');

      	if($nLocId)
      	{
      		$deal = JModelLegacy::getInstance('deal', 'enmasseModel')->getDealMaxSoldQtyFromLocation($nLocId);
      		if(empty($deal->id))
      		{
      			$msg = JText::_("NO_DEAL_ON_YOUR_LOCATION");
      			JFactory::getApplication()->enqueueMessage($msg);
      		}
      	}
      	if(!$nLocId || empty($deal->id))
      	{      		
      		$deal = JModelLegacy::getInstance('deal','enmasseModel')->todayDeal();
      	}
        //Referral ID
        $referralId = JRequest::getVar('referralid');
        $this->assignRef( 'referralId', $referralId );		
		 
    	if(empty($deal))
		{
			//redirect to upcomming deal page because there havent deal on today
			$link = JRoute::_("index.php?option=com_enmasse&controller=deal&task=upcoming", false);
			$msg = JText::_('NO_DEAL_TODAY');

			JFactory::getApplication()->redirect($link, $msg);
		}
		
        $deal->merchant = JModelLegacy::getInstance('merchant','enmasseModel')->getById($deal->merchant_id);
        $deal->merchant->branches = json_decode($deal->merchant->branches, true);
		$layout = JRequest::getVar('layout','deal_today');

		//tin.doan added
		$deal->types = JModelLegacy::getInstance('dealType','enmasseModel')->getDealTypeListByIdDealEnable($deal->id);
        $this->assignRef( 'deal', $deal );
        if($layout=="order"){

          $ri = JRequest::getVar('ri','');
          $order = JModelLegacy::getInstance('order', 'enmasseModel')->getById($ri);$db = JFactory::getDBO();
            $query = "SELECT * FROM #__enmasse_merchant_branch WHERE ";
            $query .= "id=1";
            $db->setQuery($query);
            $item = $db->loadObject();            

            $branch ="";
            
            $branches  = json_decode($item->branches);
            foreach ($branches as $key => $value) {
                if($value->name == $order->branch){
                    $branch = $value;
                    break;
                }
            }
            
            $query2 ="SELECT * FROM #__enmasse_order_item WHERE order_id = ".$order->id;
            $db->setQuery($query2);
            $deal = $db->loadObject();

            $query3 ="SELECT * FROM #__enmasse_order_passenger WHERE order_id = ".$order->id;
            $db->setQuery($query3);
            $passengerInfo = $db->loadObject(); 

            $query4 ="SELECT * FROM #__enmasse_deal WHERE id = ".$deal->pdt_id;
            $db->setQuery($query4);
            $dealDetail = $db->loadObject();
            

          $this->_setPath('template',JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."theme". DS .EnmasseHelper::getThemeFromSetting(). DS ."tmpl". DS);
          $this->_layout="order_redem";

          $this->assignRef( 'order', $order );  
          $this->assignRef( 'dealDetail', $dealDetail );  
          $this->assignRef( 'branch', $branch );  
          $this->assignRef( 'passengerInfo', $passengerInfo );  
        }else{
          $this->_setPath('template',JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."theme". DS .EnmasseHelper::getThemeFromSetting(). DS ."tmpl". DS);
          $this->_layout="deal_today";
        }
        
        parent::display($tpl);
    }

}
?>