<?php
/*------------------------------------------------------------------------
# En Masse - Social Buying Extension 2010
# ------------------------------------------------------------------------
# By Matamko.com
# Copyright (C) 2010 Matamko.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.matamko.com
# Technical Support:  Visit our forum at www.matamko.com
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
require_once( JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."CartHelper.class.php");

class EnmasseViewShopCheckout extends JViewLegacy
{
	function display($tpl = null)
	{
		$cart = unserialize(JFactory::getSession()->get('cart'));
		CartHelper::checkCart($cart);

		$setting = JModelLegacy::getInstance('setting','enmasseModel')->getSetting();
		$arData = array();
		
		$this->payGtyList = JModelLegacy::getInstance('payGty','enmasseModel')->listAll();
		
		$oItems = $cart->getAll();

		$deal = $oItems[1]->item;
		foreach ($oItems as $key => $value) {
		    $deal = $value->item;
		    $deal->count = $value->count;
		}
		$this->merchant = JModelLegacy::getInstance('merchant','enmasseModel')->getByID($deal->merchant_id ? $deal->merchant_id : 0);
		$this->locations = $this->getLocatonsByDealID($deal->id);
		
		$this->assignRef( 'termArticleId', $setting->article_id);
		$this->assignRef( 'theme', $setting->theme);
		$this->user = JModelLegacy::getInstance('user','enmasseModel')->getUser();
		$this->assignRef( 'cart', $cart );
		//get user data was save in the session
		$arData = JFactory::getApplication()->getUserState('com_enmasse.checkout.data');
		if(empty($arData))
		{
			//contruct default value for the inputs
			$arData['name'] = $this->user->name;
			$arData['email'] = $this->user->email;
			$arData['receiver_name'] = "";
			$arData['receiver_email'] = "";
			$arData['receiver_msg'] = "";
			$arData['receiver_address'] = "";
			$arData['receiver_phone'] = "";
		}
//                $menu = & JSite::getMenu();
//                $oItem = $menu->getActive();
//                $nItemId = $oItem->id;
                $oMenu = JFactory::getApplication()->getMenu();
                $oItem = $oMenu->getItems('link','index.php?option=com_enmasse&view=deallisting',true);
                $this->allDealLink = 'index.php?option=com_enmasse&view=deallisting&Itemid='.$oItem->id;
                
		$this->arData = $arData;
                
		$this->_setPath('template', JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."theme". DS .EnmasseHelper::getThemeFromSetting(). DS ."tmpl". DS);
		$this->_layout="shop_checkout";
		parent::display($tpl);
	}
	function getLocationNameByLocationId($locationId)
	{
		$mainframe = JFactory::getApplication('site');
		$db = JFactory::getDBO();
		$query = "SELECT name FROM #__enmasse_location WHERE id = " . $locationId;				
		$db->setQuery( $query );
		$row = $db->loadResult();	
		return $row;		
	}
	function getLocatonsByDealID($dealID)
	{
		$mainframe = JFactory::getApplication('site');
		$db = JFactory::getDBO();
		$query = "SELECT name FROM #__enmasse_location WHERE id in (SELECT location_id from #__enmasse_deal_location where deal_id = ".$dealID.") " ;//. $locationId;				
		$db->setQuery( $query );
		
		$rows = $db->loadAssocList();
		$return = array();
		foreach ($rows as $item) {
				$return[] = $item['name'];
			}	
		return $return;
	}

}
?>