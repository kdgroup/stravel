<?php
/*------------------------------------------------------------------------
# En Masse - Social Buying Extension 2010
# ------------------------------------------------------------------------
# By Matamko.com
# Copyright (C) 2010 Matamko.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.matamko.com
# Technical Support:  Visit our forum at www.matamko.com
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view');

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");

class EnmasseViewMerchant extends JViewLegacy
{
	function display($tpl = null)
	{
		$task = JRequest::getWord('task');
		$tm = JRequest::getWord('tm');
		$product = JRequest::getWord('product');
		
		
		switch ($task) 
		{
    		case 'dealCouponMgmt':
				$merchantId = JFactory::getSession()->get('merchantId');		
				// To list deal by merchant
				$dealList = JModelLegacy::getInstance('deal','enmasseModel')->listConfirmedByMerchantId($merchantId);
				$enmaseSetting = EnmasseHelper::getSetting();
				$this->assignRef('enmaseSetting',$enmaseSetting);
				$this->assignRef('dealList',$dealList);
				$dealId ='';
				$orderItemList = null;
				$filter = JRequest::getVar('filter');
				$coupon = JRequest::getVar('coupon');
				$branch = JRequest::getVar('branch');
				$this->assignRef('filter',$filter);
				$dealId = $filter['deal_id'];
				$this->branchActived = $branch;

				// var_dump($this->filter);
				// var_dump($branch);//die;
				$merchant = JModelLegacy::getInstance('merchant','enmasseModel')->getByUserName(JFactory::getUser()->get('username'));
				if($merchant==null){
					$merchant = JModelLegacy::getInstance('merchant','enmasseModel')->getBySuppervisorName(JFactory::getUser()->get('username'));
				}

				$this->merchant = JModelLegacy::getInstance('merchant','enmasseModel')->getByID($merchant->id);
				
				if(!empty($dealId))
				{
					$deal =  JModelLegacy::getInstance('deal','enmasseModel')->getById($dealId);
					$this->assignRef('deal',$deal);	
					
					// $orderItemList = JModelLegacy::getInstance('orderItem','enmasseModel')->listByPdtIdAndStatusBranch($dealId, "Delivered",$branch);
					$orderItemList = JModelLegacy::getInstance('orderItem','enmasseModel')->listByPdtIdAndStatusBranch($dealId, "Delivered' OR a.status='Refunded ",$branch);
					
					for($count =0; $count < count($orderItemList); $count++)
					{
						$orderItemList[$count]->invtyList 	= JModelLegacy::getInstance('invty','enmasseModel')->listByOrderItemId($orderItemList[$count]->id);
						$orderItemList[$count]->order 		= JModelLegacy::getInstance('order','enmasseModel')->getById($orderItemList[$count]->order_id);
						// if($branch && $orderItemList[$count]->order!=$branch){
						// 	unset($orderItemList[$count]);
						// }
					}
					
				}
				// if($tm=='coupon'){
				// 	$this->assignRef('orderItemListCoupon',$orderItemList);
				// 	//$this->assignRef('orderItemList',$orderItemList);
				// }else{pz
					if($product!="search"){
						$this->assignRef('orderItemListCoupon',$orderItemList);
					}else{
						$this->assignRef('orderItemList',$orderItemList);
					}
					
					$this->assignRef('coupon',$coupon);
				//}
				
				$this->_setPath('template',JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."theme". DS .EnmasseHelper::getThemeFromSetting(). DS ."tmpl". DS);
				$this->_layout="merchant_deal_coupon_mgmt";
				parent::display($tpl);
				break;
			
			default:
				$link = JRoute::_("index.php?option=com_enmasse&controller=merchant&task=dealCouponMgmt", false);
				JFactory::getApplication()->redirect($link, $null);
		}
	}

}
?>