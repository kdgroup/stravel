<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.controller');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."DatetimeWrapper.class.php");
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
require_once( JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."Cart.class.php");
require_once( JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."CartHelper.class.php");

class EnmasseControllerPayment extends JControllerLegacy
{
	function gateway()
	{
		JRequest::setVar('view', 'paygty');
		parent::display();
	}

	function returnUrl_()
	{
		sleep(2);
		$msg = JText::_("PAYMENT_BEING_PROCESS_MSG");
		$link = JRoute::_("index.php?option=com_enmasse&controller=order&view=orderList", false);
		JFactory::getApplication()->redirect($link, $msg);
	}

	function returnUrl()
	{	
		$session = & JFactory::getSession();
		$user = JFactory::getUser();
	
		$orderId 	= JRequest::getVar('orderId');
		$payClass 	= JRequest::getVar('payClass', '');
		
		$className = 'PayGty'.ucfirst($payClass);
		if (!(EnmasseHelper::checkValidPayclass($payClass)))
		{
				$msg = "Invalid pay class";
				$link = JRoute::_("index.php?option=com_enmasse&controller=deal", false);
				JFactory::getApplication()->redirect($link, $msg);
				die;
		}
		require_once JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."payGty". DS .$payClass. DS .$className. ".class.php";

		if ( ! call_user_func_array(array($className, "validateTxn"), array($payClass)) )
		{
			echo JTEXT::_("PAYMENT_VALIDATION_FAILED");
			exit(0);
		}
		else
		{
			$payDta = call_user_func_array(array($className, "generatePaymentDetail"), array());	
			$payDetail = json_encode($payDta);		
			if($payClass=="pagseguro")
			{
				header('Content-Type: text/html; charset=ISO-8859-1');
				$Referencia  = JRequest::getVar('Referencia',NULL,'post');
				if(isset($Referencia))
				{
					$orderId = stripslashes( $_POST['Referencia'] );
					$order = JModelLegacy::getInstance('order','enmasseModel')->getById($orderId);
					if($order == null)
					{
						echo JTEXT::_("PAYMENT_ERROR_MSG") . $orderId;
						exit(0);
					}
					else if($order->status=="Unpaid") // Pass checking
					{
						switch(stripslashes($_POST['StatusTransacao']))
						{
							case "Completo": //Full payment
								EnmasseHelper::doNotify($orderId);			
								break;
							case "Aguardando Pagto": //Awaiting Customer Payment
								EnmasseHelper::setPendingStatusByOrderId($orderId);
								break;
							case "Aprovado": //Payment approved, awaiting compensation
								EnmasseHelper::doNotify($orderId);
								break;
							//case "Em An...": //Payment approved, under reviewing by Pagseguro
								//break;
							case "Cancelado": //Cancelled
								EnmasseHelper::setRefundedStatusByOrderId($orderId);
								break;																								
							default:
								$msg = "Muito obrigado por seu pedido!";
								$link = JRoute::_("index.php?option=com_enmasse&controller=deal", false);
								JFactory::getApplication()->redirect($link, $msg);
								break;								
						}
						JModelLegacy::getInstance('order','enmasseModel')->updatePayDetail($orderId, $payDetail);								
					}
				}	
				else					
				{
					$msg = "Muito obrigado por seu pedido!";
					$link = JRoute::_("index.php?option=com_enmasse&controller=deal", false);
					JFactory::getApplication()->redirect($link, $msg);	
				}
			}
			elseif($payClass=="payfast")
			{
				$paymentstatus = JRequest::getVar('payment_status','','POST') ;
				if(isset($paymentstatus))
				{
					$flag	= JRequest::getVar('flag');							
					
					if($paymentstatus == "COMPLETE")
					{
						$orderId = JRequest::getVar('m_payment_id','','POST') ;
						EnmasseHelper::doNotify($orderId);
						JModelLegacy::getInstance('order','enmasseModel')->updatePayDetail($orderId, $payDetail);
					}
				}
				$msg = '';
				if(stripslashes( $_GET['flag'] )=="returnUrl")
				{
					$msg = JText::_( "Thank you for purchasing!");
				}
				else if( stripslashes( $_GET['flag'] )=="cancelUrl")
				{
					$msg = JText::_( "Your payment was cancelled.");
				}					
				$link = JRoute::_("index.php?option=com_enmasse&controller=deal", false);	
				JFactory::getApplication()->redirect($link, $msg);
			}
			elseif($payClass=="twocheckout")
			{
				$payGty = JModelLegacy::getInstance('payGty','enmasseModel')->getByClass($payClass);
				$attribute_config = json_decode($payGty->attribute_config);
				$accountNumber = $attribute_config->sid;
				$demo = $attribute_config->demo;
				if($demo == "Y" &&  $_REQUEST["credit_card_processed"] == "Y")
				{
					$orderId = $_REQUEST["cart_order_id"];
					EnmasseHelper::doNotify($orderId);
					JModelLegacy::getInstance('order','enmasseModel')->updatePayDetail($orderId, $payDetail);		
					$msg = JText::_( "Thank you for purchasing!");					
				} // md5 checking is only available in live payment
				elseif(demo != "Y" &&  $_REQUEST["credit_card_processed"] == "Y")
				{				
					$secretWord = $attribute_config->secret_word;
					$string_to_hash = $secretWord . $accountNumber . $_REQUEST["order_number"] . $_REQUEST["total"];
					$check_key = strtoupper(md5($string_to_hash));
					if($check_key == $_REQUEST["key"])
					{						
						$orderId = $_REQUEST["cart_order_id"];
						EnmasseHelper::doNotify($orderId);
						JModelLegacy::getInstance('order','enmasseModel')->updatePayDetail($orderId, $payDetail);
						$msg = JText::_( "Thank you for purchasing!");
					}
					else 
					{
						$msg = JText::_( "Can't verify your payment! Please contact Administrator!");
					}
				}
				$link = JRoute::_("index.php?option=com_enmasse&controller=deal", false);
				JFactory::getApplication()->redirect($link, $msg);
			}
			elseif($payClass=="authorizenet")
			{
				$approved = JRequest::getVar('approved','','POST');
				$invoice_number = JRequest::getVar('invoice_number','','POST');
				if($approved=='true');
				{
					$orderId = $invoice_number;
					EnmasseHelper::doNotify($orderId);
					JModelLegacy::getInstance('order','enmasseModel')->updatePayDetail($orderId, $payDetail);									$transactionid = stripslashes( $_POST['transaction_id']);
					$msg = JText::_( "Thank you for purchasing! Your transaction ID is " . $transactionid . ".");
				}
				$link = JRoute::_("index.php?option=com_enmasse&controller=deal", false);
				JFactory::getApplication()->redirect($link, $msg);
			}
			elseif($payClass=="ewayhosted")
			{			
				if($_REQUEST['trxnstatus']=='true');
				{
					$orderId = $_REQUEST['orderId'];
					EnmasseHelper::doNotify($orderId);
					JModelLegacy::getInstance('order','enmasseModel')->updatePayDetail($orderId, $payDetail);					
					$msg = JText::_( "Thank you for purchasing!");
				}
				$link = JRoute::_("index.php?option=com_enmasse&view=orderlist", false);
				JFactory::getApplication()->redirect($link, $msg);
			}			
			elseif($payClass=="paypal")
			{
				$payDta = call_user_func_array(array($className, "generatePaymentDetail"), array());	
				$payDetail = json_encode($payDta);
				JModelLegacy::getInstance('order','enmasseModel')->updatePayDetail($orderId, $payDetail);
				
				$link = JRoute::_("index.php?option=com_enmasse&controller=payment&task=doNotify&$postData&orderId=$orderId", false);
				JFactory::getApplication()->redirect($link);
			}	
			elseif($payClass=="moneybookers")
			{				
				$transaction_id = JRequest::getVar('transaction_id','','post');
				$status = JRequest::getVar('status','','post');
				if(isset($transaction_id) && $status == 2)
				{
                    $orderId = $transaction_id;
                    if(EnmasseHelper::getOrderStatusByOrderId($orderId) == "Unpaid")
                    {
                        EnmasseHelper::doNotify($orderId);
                        JModelLegacy::getInstance('order','enmasseModel')->updatePayDetail($orderId, $payDetail); 
                    }
				}               
            }
			elseif($payClass=="ogone")
			{				
				$aData = array();
				foreach($_REQUEST as $key=>$value)
				{
					$aData[strtoupper($key)] = $value;
				}			
				$orderId = $aData['ORDERID'];
				if($aData['STATUS'] == '5' || $aData['STATUS'] == '9')
				{
					EnmasseHelper::doNotify($orderId);
					JModelLegacy::getInstance('order','enmasseModel')->updatePayDetail($orderId, $payDetail);
					$sMessage = JText::_('PAYMENT_BEING_PROCESS_MSG');					
				}
				else
				{
					$sMessage = JTEXT::_("PAYMENT_VALIDATION_FAILED");	
				}
				$link = JRoute::_("/", false);
				JFactory::getApplication()->redirect($link, $sMessage);
            }
            elseif($payClass=="googlecheckout")
            {
            	$merchantId = '624370439819433';
            	$merchantKey = 'r86DDFDkmjzrqrr7kW_NMQ';
            	$authorization = base64_encode($merchantId.':'.$merchantKey);
            	header('Authorization: Basic '.$authorization);
            	header('Content-Type: application/xml;charset=UTF-8');
            	header('Accept: application/xml;charset=UTF-8');
            	
            	$serial_number = $_REQUEST['serial-number'];
            	
				$orderId = $session->get( 'newOrderId', 0 );
				if (isset($orderId) && $orderId) {
	            	EnmasseHelper::doNotify($orderId);
	            	JModelLegacy::getInstance('order','enmasseModel')->updatePayDetail($orderId, $payDetail);
				}
            	
            	echo '<?xml version="1.0" encoding="UTF-8"?>
            			<notification-acknowledgment xmlns="http://checkout.google.com/schema/2" serial-number="'.$serial_number.'"/>';
            	exit;
            }
			elseif($payClass=="realex")
			{			
				if($_REQUEST['RESULT']=='00');
				{
					$orderId = $_REQUEST['ORDER_ID'];
					EnmasseHelper::doNotify($orderId);
					JModelLegacy::getInstance('order','enmasseModel')->updatePayDetail($orderId, $payDetail);					
					$msg = JText::_( "Thank you for purchasing!");
				}
				$link = JRoute::_("index.php?option=com_enmasse&view=orderlist", false);
				JFactory::getApplication()->redirect($link, $msg);
			}         
		}
		$session->clear('newOrderId');
	}

	function doNotify()
	{		
		$orderId = JRequest::getVar("orderId", null);
		
		$order = JModelLegacy::getInstance('order','enmasseModel')->getById($orderId);
		if($order == null)
		{
			echo JTEXT::_("PAYMENT_ERROR_MSG") . $orderId;
			exit(0);
		}
		else if($order->status=="Unpaid") // Pass checking
		{
			EnmasseHelper::doNotify($orderId);
		}
                //free cart
                JFactory::getSession()->set('cart', null);
		$msg = "";//JTEXT::_("PAYMENT_SUCCESS");

		$link = JRoute::_("index.php?option=com_enmasse&controller=shopping&task=result&success=true", false);
		JFactory::getApplication()->redirect($link, $msg);
	}
	function sendPDFEmail()
	{		
		$orderId = JRequest::getVar("orderId", null);
		
		$order = JModelLegacy::getInstance('order','enmasseModel')->getById($orderId);
		if($order == null)
		{
			echo JTEXT::_("PAYMENT_ERROR_MSG") . $orderId;
			exit(0);
		}
		else // Pass checking
		{
			EnmasseHelper::sendOrderEmailTo($order);
		}
         echo "<h1>Success</h1>";
        die;      
               
	}
	function testGeneratePdf(){
		header('Content-Type: text/html; charset=utf-8');
		// $file = JPATH_SITE . DS ."media". DS ."com_enmasse". DS .'coupon-204.html';
		$file = 'http://www.stravel.com.hk/index.php?option=com_enmasse&controller=coupon&task=generate&invtyName=nmo417&token=913b52a0315392d44051b186285bcc6e&tmpl=pdf';
		       $current = file_get_contents($file);
		       echo($current);
		         // file_put_contents($desk, $current);
		  // die;       
		// Instantiate the application.
		 require_once(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_enmasse' . DS . 'helpers' . DS . 'html2pdf' . DS . 'html2pdf.class.php');
		                $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8');
		                $html2pdf->setDefaultFont('cid0jp');
		                $html2pdf->writeHTML($current);
		                // $html2pdf->writeHTML('<p>This is your first PDF File</p>');
		                $outFileName = JPATH_SITE . DS ."media". DS ."com_enmasse". DS .'coupon-4-2-4.pdf';//'report-' . DatetimeWrapper::getDateOfNow() . '.pdf';
		                $html2pdf->Output($outFileName, 'F');

		    //             $html2pdf = new HTML2PDF('P', 'A4', 'en');
						// $html2pdf->writeHTML('<p>This is your first PDF File</p>');
						// $html2pdf->Output($outFileName,'F');


		echo '<h1>PDF generated!</h1>';die;
	}
	function testSendEmail(){
		ini_set("error_reporting", E_ALL);

			$orderId = JRequest::getVar("orderId", null);
		
			$order = JModelLegacy::getInstance('order','enmasseModel')->getById($orderId);
			 $app = JFactory::getApplication();
            $mailer = JFactory::getMailer();
            $config = JFactory::getConfig();
            $subject = '心程旅遊團購換領劵';

            $sender = array(
                $config->get('config.mailfrom'),
                '心程旅遊 sTravel'
            );
            echo 5;

            $db = JFactory::getDBO();
            $query = "SELECT * FROM #__enmasse_merchant_branch WHERE ";
            $query .= "id=1";
            $db->setQuery($query);
            $item = $db->loadObject();            

            $branch ="";
            
            $branches  = json_decode($item->branches);
            foreach ($branches as $key => $value) {
                if($value->name == $order->branch){
                    $branch = $value;
                    break;
                }
            }
            echo 42;
            // die;
            var_dump($order);
            $query2 ="SELECT * FROM #__enmasse_order_item WHERE order_id = ".$order->id;
            $db->setQuery($query2);
            $deal = $db->loadObject();

            $query3 ="SELECT * FROM #__enmasse_order_passenger WHERE order_id = ".$order->id;
            $db->setQuery($query3);
            $passengerInfo = $db->loadObject();
            // var_dump($query2);
            // var_dump($deal);
            // var_dump($branch);
            // var_dump($query3);
            // var_dump($passengerInfo);
            $query = "	SELECT
						* 
					FROM 
						#__enmasse_invty 
					WHERE
	              		order_item_id = ".$order->orderItemId;
		$db->setQuery( $query );
		$invtyList = $db->loadObjectList();

            // $invtyList = JModelLegacy::getInstance('invty','enmasseModel')->listByOrderItemId($order->orderItemId);
            $link = "";
            // var_dump($item);
            // die;
            foreach ($invtyList as $invty) {
                // $link = JURI::root() ."index.php?option=com_enmasse&controller=coupon&task=generate&invtyName=".$invty->name
                //       ."&token=".md5($invty->name);
            }
            echo 34;
            die;
           $attachFileName = JPATH_SITE . DS ."media". DS ."com_enmasse". DS .'coupon-0187.pdf';//self::generateHTML($order->orderItemId,$link) ;

            $body ="<h4>".$passengerInfo->name3." 您好，</h4>";
             $body .=   "<p>謝謝訂購我們的團購產品：".$deal->description."</p>";
             $body .=  "   <p>請打印附件的換領劵，並於辦公時間到下列的分店完成相關的旅遊套票手續。</p>";
             $body .=  "   <p>";
             $body .=  "   分店名稱： ".$branch->name." <br/>";
             $body .=  "       分店地址： ".$branch->address."<br/>";
             $body .=  "       辦公時間： ".$branch->description." <br/>";
            $body .=  "        聯絡電話： ".$branch->telephone."";

              $body .=  "  </p>";
              $body.='<p>如不能打開附件，請點擊 <a href="'.$link.'">這裡</a></p>';
              // $body.='<p>'.JRoute::_('index.php?itemId=194&tmpl=component&ri=46').'</p>';
               $body .=  " <p>            ";
               $body .=  "     祝   旅途愉快！<br/>";
              $body .=  "      心程旅遊 sTravel";
              $body .=  "  <p>";

            $body .=  "";
            echo 2;
            // $recipient = array($passengerInfo->email3); //$user->email;
            // if($branch->email){
            //     $mailer->addCC($branch->email);
            // }
            // if($item->email){
            //     $mailer->addCC($item->email);
            // }
            //Send to Admin
            // $mailer->addCC('sales-redeem@stravel.com.hk');

            $mailer->addBCC('abc@lifeabc.com');
            $mailer->addBCC('huynhkhanhduc312@gmail.com');

            $mailer->setSubject($subject);
            $mailer->isHTML(true);
            $mailer->setBody($body);
            if (!empty($attachFileName)) {
                // $mailer->addAttachment($attachFileName);
            }
            $mailer->addRecipient("huynhkhanhduc312@gmail.com");
            $mailer->setSender($sender);

            echo 1;
            var_dump($mailer);
            die;
            $send = &$mailer->Send();

            if ($send !== true) {
                //$app->enqueueMessage('Error Sending email:<br/>'.$send->message,'error');
                echo '<h1>Send Mail Fail</h1>';
            } else {
               echo '<h1>Send Mail Success</h1>';
            }

		echo '<h1>Send Mail</h1>';die;
	}
	function cancelUrl()
	{
		$msg = "";//Text::_( "CANCEL_TRANSACTION");
		$link = JRoute::_("index.php?option=com_enmasse&controller=shopping&task=result", false);
		JFactory::getApplication()->redirect($link, $msg);
	}
	 function notifyUrl() {
        define("LOG_FILE", "./ipn.log");
        $payClass = JRequest::getVar('payClass');
        if($payClass==='paypal') {
            // CONFIG: Enable debug mode. This means we'll log requests into 'ipn.log' in the same directory.
            // Especially useful if you encounter network errors or other intermittent problems with IPN (validation).
            // Set this to 0 once you go live or don't require logging.
            define("DEBUG", 0);            
            // Set to 0 once you're ready to go live
            define("USE_SANDBOX", 0);                       
            // Read POST data
            // reading posted data directly from $_POST causes serialization
            // issues with array data in POST. Reading raw POST data from input stream instead.
            $raw_post_data = file_get_contents('php://input');
            $raw_post_array = explode('&', $raw_post_data);
            $myPost = array();
            foreach ($raw_post_array as $keyval) {
            	$keyval = explode ('=', $keyval);
            	if (count($keyval) == 2)
            		$myPost[$keyval[0]] = urldecode($keyval[1]);
            }
            // read the post from PayPal system and add 'cmd'
            $req = 'cmd=_notify-validate';
            if(function_exists('get_magic_quotes_gpc')) {
            	$get_magic_quotes_exists = true;
            }
            foreach ($myPost as $key => $value) {
            	if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
            		$value = urlencode(stripslashes($value));
            	} else {
            		$value = urlencode($value);
            	}
            	$req .= "&$key=$value";
            }
            
            // Post IPN data back to PayPal to validate the IPN data is genuine
            // Without this step anyone can fake IPN data
            
            if(USE_SANDBOX == true) {
            	$paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
            } else {
            	$paypal_url = "https://www.paypal.com/cgi-bin/webscr";
            }
            
            $ch = curl_init($paypal_url);
            if ($ch == FALSE) {
            	return FALSE;
            }
            
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
            
            if(DEBUG == true) {
            	curl_setopt($ch, CURLOPT_HEADER, 1);
            	curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
            }
            
            // CONFIG: Optional proxy configuration
            //curl_setopt($ch, CURLOPT_PROXY, $proxy);
            //curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
            
            // Set TCP timeout to 30 seconds
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
            
            // CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
            // of the certificate as shown below. Ensure the file is readable by the webserver.
            // This is mandatory for some environments.
            
            //$cert = __DIR__ . "./cacert.pem";
            //curl_setopt($ch, CURLOPT_CAINFO, $cert);
            
            $res = curl_exec($ch);
            if (curl_errno($ch) != 0) // cURL error
            	{
            	if(DEBUG == true) {	
            		error_log(date('[Y-m-d H:i e] '). "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, LOG_FILE);
            	}
            	curl_close($ch);
            	exit;
            
            } else {
            		// Log the entire HTTP response if debug is switched on.
            		if(DEBUG == true) {
            			//error_log(date('[Y-m-d H:i e] '). "HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $req" . PHP_EOL, 3, LOG_FILE);
            			//error_log(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res" . PHP_EOL, 3, LOG_FILE);
                        error_log(date('[Y-m-d H:i e] '). "HTTP response of validation request successfully!" . PHP_EOL, 3, LOG_FILE);
            			// Split response headers and payload
            			list($headers, $res) = explode("\r\n\r\n", $res, 2);
            		}
            		curl_close($ch);
            }
            
            // Inspect IPN validation result and act accordingly
            
            if (strcmp ($res, "VERIFIED") == 0) {
            	// check whether the payment_status is Completed
            	// check that txn_id has not been previously processed
            	// check that receiver_email is your PayPal email
            	// check that payment_amount/payment_currency are correct
            	// process payment and mark item as paid.
            
            	// assign posted variables to local variables
            	$item_name = $_POST['item_name'];
            	$item_number = $_POST['item_number'];
            	$payment_status = $_POST['payment_status'];
            	$payment_amount = $_POST['mc_gross'];
            	$payment_currency = $_POST['mc_currency'];
            	$txn_id = $_POST['txn_id'];
            	$receiver_email = $_POST['receiver_email'];
            	$payer_email = $_POST['payer_email'];
                //error_log(date('[Y-m-d H:i e] '). "Payment verified". PHP_EOL, 3, LOG_FILE);
                if(strcmp($payment_status,'Completed')===0) {
                    $payDta = array('txn_id'=>$txn_id,'payment_status'=>$payment_status,'mc_currency'=>$payment_currency,'mc_gross'=>$payment_amount);	
        			$payDetail = json_encode($payDta);
                    $db = JFactory::getDbo();
                    $query = "SELECT * FROM `#__enmasse_order` WHERE `pay_detail` like '$payDetail'";
                    $validate = $db->setQuery($query)->loadObject();
                    $query = "SELECT * FROM `#__enmasse_pay_gty` WHERE `class_name` like 'paypal'";
                    $paypal = $db->setQuery($query)->loadObject();
                    $paypal_config = json_decode($paypal->attribute_config);
                    //error_log(date('[Y-m-d H:i e] '). "Payment status = completed". PHP_EOL, 3, LOG_FILE);
                    if(!$validate && strcmp($receiver_email,$paypal_config->merchant_email)==0) {
                        //error_log(date('[Y-m-d H:i e] '). "First time,process...". PHP_EOL, 3, LOG_FILE);
                        $orderId = JRequest::getVar("orderId", null);
                        JModel::getInstance('order','enmasseModel')->updatePayDetail($orderId, $payDetail);                    		
                        $order = JModel::getInstance('order','enmasseModel')->getById($orderId);
                        if(!empty($order)) {
                            if($order->status=="Unpaid") // Pass checking
                    		{
                    			EnmasseHelper::doNotify($orderId);
                                if(DEBUG == true) {
                                    error_log(date('[Y-m-d H:i e] '). "Confirm Paypal order!" . $orderId . PHP_EOL, 3, LOG_FILE);
                                }
                    		}
                        }
                    }
                }
            
            	if(DEBUG == true) {
            		error_log(date('[Y-m-d H:i e] '). "Verified IPN: $req ". PHP_EOL, 3, LOG_FILE);
            	}
            } else if (strcmp ($res, "INVALID") == 0) {
            	// log for manual investigation
            	// Add business logic here which deals with invalid IPN messages
                error_log(date('[Y-m-d H:i e] '). "Payment Invalid". PHP_EOL, 3, LOG_FILE);
            	if(DEBUG == true) {
            		error_log(date('[Y-m-d H:i e] '). "Invalid IPN: $req" . PHP_EOL, 3, LOG_FILE);
            	}
            }
        }
        elseif($payClass==='alipay') 
        {
            
        
        }
        else {
	            if(!empty($_REQUEST)) {
	                $data = json_encode($_REQUEST);
	            }
	            //验证失败
	            error_log(date('[Y-m-d H:i e] '). "Invalid Alipay IPN. Data was received: $data " . PHP_EOL, 3, LOG_FILE);
	            echo "fail";exit;
	            //调试用，写文本函数记录程序运行情况是否正常
	            //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
	    }
      
    }

}
?>