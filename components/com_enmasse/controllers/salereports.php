<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.controller');
JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_enmasse'.DS.'tables');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."DatetimeWrapper.class.php");

class EnmasseControllerSaleReports extends JControllerLegacy
{		
	function dealReport()
	{
		$this->checkAccess();
		
		JRequest::setVar('view', 'salereports');
		JRequest::setVar('task', 'dealReport');
		parent::display();
	}
	function updateOrder(){
		$id = JRequest::getVar('id');
		$todo = JRequest::getVar('todo');
		$reason = JRequest::getVar('reason');
		$data =array();
		$data['reason'] =$reason;
		$data['date'] =DatetimeWrapper::getDisplayDatetime(date('Y-m-d H:i:s'))." ".date(' H:i:s');
		$data['dates'] =date('Y-m-d H:i:s');
		switch ($todo) {
			case 'taken':
				JModelLegacy::getInstance('invty','enmasseModel')->updateStatusByOrderItemId($id,"Taken");
				$return = JModelLegacy::getInstance('orderItem','enmasseModel')->addNewReason($id,$reason,'unused');
				$data['id'] = $id;
				$data['todo'] = $todo;
				if($return){
					$data['status'] = 1;
				}else{
					$data['status'] = 0;
				}
				
				break;
			case 'edittaken':
				$return = JModelLegacy::getInstance('orderItem','enmasseModel')->updateReason($id,$reason,'unused');
				$data['id'] = $id;
				$data['todo'] = $todo;
				if($return){
					$data['status'] = 1;
				}else{
					$data['status'] = 0;
				}
				break;
			case 'editrefund':
				$return = JModelLegacy::getInstance('orderItem','enmasseModel')->updateReason($id,$reason,'refund');
				$data['id'] = $id;
				$data['todo'] = $todo;
				if($return){
					$data['status'] = 1;
				}else{
					$data['status'] = 0;
				}
				break;
			default:
			case 'refund':
				$return = JModelLegacy::getInstance('orderItem','enmasseModel')->updateStatus($id,'Refunded');
				$return = JModelLegacy::getInstance('orderItem','enmasseModel')->addNewReason($id,$reason,'refund');
				$data['id'] = $id;
				$data['todo'] = $todo;
				if($return){
					$data['status'] = 1;
				}else{
					$data['status'] = 0;
				}
				break;
			default:
				
				break;
		}
		// Get Coupon

		echo json_encode($data);
		die;
	}
	private function checkAccess()
	{
	    if (JFactory::getUser()->get('guest'))
		{			
			$msg = JText::_( "SALE_PLEASE_LOGIN_BEFORE");
			$redirectUrl = base64_encode("index.php?option=com_enmasse&controller=saleReports&task=dealReport");  
		    $version = new JVersion;
            $joomla = $version->getShortVersion();
            if(substr($joomla,0,3) >= '1.6'){
                $link = JRoute::_("index.php?option=com_users&view=login&return=".$redirectUrl, false);  
            }else{
                $link = JRoute::_("index.php?option=com_user&view=login&return=".$redirectUrl, false);    
            }
			JFactory::getApplication()->redirect($link, $msg);
		}
		
		$salesPersonId = JFactory::getSession()->get('salesPersonId');

		if($salesPersonId == null)
		{
			$salesPerson = JModelLegacy::getInstance('salesPerson','enmasseModel')->getByUserName(JFactory::getUser()->get('username'));
			if ($salesPerson != null)
				JFactory::getSession()->set('salesPersonId', $salesPerson->id);
			else
			{
	         	$msg = JText::_('SALE_NO_ACCESS');
				$link = JRoute::_("index.php?option=com_enmasse&controller=deal&task=listing", false);
				JFactory::getApplication()->redirect($link, $msg);
			}
		}
		return $salesPersonId;
	}
	
	public function createPdf(){
		$total_amount = 0;
		$salesPersonId = JFactory::getSession()->get('salesPersonId');
		//$filter = JRequest::getVar('filter', array('name' => "", 'code' => "", 'merchant_id' => "", 'fromdate' => "", 'todate' => ""));
		//$filter = JRequest::getVar('filter',array(),'post','array');
        $currency_prefix 	= JModelLegacy::getInstance('setting','enmasseModel')->getCurrencyPrefix(); 
		$dealList 			= JModelLegacy::getInstance('deal','enmasseModel')->searchBySaleReports($salesPersonId, JRequest::getVar('name'), JRequest::getVar('merchant_id'), JRequest::getVar('fromdate'), JRequest::getVar('todate'), JRequest::getVar('code'));
		if(empty($dealList)) return null;
		$result = '<table style="border:1px dotted #D5D5D5; border-collapse: collapse;"><tr valign="middle"><th style="border:1px dotted #D5D5D5;" align="center" width="50">'
						.JText::_("No").'</th><th style="border:1px dotted #D5D5D5;" width="10">'
						.JText::_("Deal Code").'</th><th style="border:1px dotted #D5D5D5;" width="50">'
						.JText::_("Deal Name").'</th><th style="border:1px dotted #D5D5D5;" width="50">'
						.JText::_("Merchant").'</th><th style="border:1px dotted #D5D5D5;" align="center" width="50">'
						.JText::_("Qty Sold").'</th><th style="border:1px dotted #D5D5D5;" align="center" width="50">'
						.JText::_("Unit Price").'</th><th style="border:1px dotted #D5D5D5;" align="center" width="50">'
						.JText::_("Total Sales").'</th></tr>';
		$i = 0;
		foreach ($dealList as $row)
		{
			$i++;
			$merchant_name 	= JModelLegacy::getInstance('merchant','enmasseModel')->retrieveName($row->merchant_id);
			$total_sales = $row->price * $row->cur_sold_qty;
			$result .= '<tr>
				<td style="border:1px dotted #D5D5D5;" align="center">'.$i.'</td>
				<td style="border:1px dotted #D5D5D5;">'.$row->deal_code.'</td>
				<td style="border:1px dotted #D5D5D5;width=250;">'.$row->name.'</td>
				<td style="border:1px dotted #D5D5D5;">'.$merchant_name.'</td>
				<td style="border:1px dotted #D5D5D5;" align="center">'.$row->cur_sold_qty.'</td>';
				if($row->use_dynamic == 0)
				{
					$result .= '<td style="border:1px dotted #D5D5D5;" align="center">'.$currency_prefix.$row->price.'</td>
				<td style="border:1px dotted #D5D5D5;" align="center">'.$currency_prefix.$total_sales.'</td></tr>';
				}
				else
				{
					$max_min = EnmasseHelper::getMinMaxDynamicAtt($row->deal_id);
					$total_sales = EnmasseHelper::getTotalSaleDynamicAtt($row->deal_id);
					$result .= '<td style="border:1px dotted #D5D5D5;" align="center">'.$currency_prefix.$max_min->min_price .' - '.$max_min->max_price.'</td>
				<td style="border:1px dotted #D5D5D5;" align="center">'.$currency_prefix.$total_sales.'</td></tr>';
				}
			$total_amount += $total_sales;
		}
		$result .= '<tr><td style="border:1px dotted #D5D5D5;" colspan="6" style="text-align:right" >Total Amount: </td>
					<td style="border:1px dotted #D5D5D5;" align="center">' .$currency_prefix.$total_amount. '</td></tr></table>';
		//todo
		
		require_once(JPATH_ADMINISTRATOR. DS .'components' . DS .'com_enmasse' .DS .'helpers' .DS .'html2pdf' . DS .'html2pdf.class.php');
		
		$html2pdf = new HTML2PDF('P', 'A4', 'en');
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($result);
		$outFileName = 'report-' .DatetimeWrapper::getDateOfNow() .'.pdf';
		$html2pdf->Output($outFileName,'I');
		
		die;
	}

	
    function generateReport() {
        $dealId = JRequest::getVar('deal_id');
        $from_at = "";
        $from_to = "";
        $type = "";
        $tmp = "";
        if (!empty($dealId)) {
            $orderItemList =array();
            if($dealId=='100009'){
                $dealList = JModelLegacy::getInstance('deal', 'enmasseModel')->listConfirmed();

                foreach ($dealList as $key => $value) {
                    $dealId = $value->id;
                    $deal = JModelLegacy::getInstance('deal', 'enmasseModel')->getById($dealId);
                    
                    
                    $from_at = JRequest::getVar('from_at');
                    $from_to = JRequest::getVar('from_to');
                    $type = JRequest::getVar('type');
                    $coupon = JRequest::getVar('coupon');
                    $orderItemList =  JModelLegacy::getInstance('orderItem', 'enmasseModel')->listByPdtIdAndStatusAndDateCustom('all', "Delivered' OR a.status ='Refunded",$from_at,$from_to,$orderby,$branch,$type,$coupon);

                    // for ($count = 0; $count < count($orderItemListall); $count++) {
                    //     $orderItemListall[$count]->invtyList = JModelLegacy::getInstance('invty', 'enmasseModel')->listByOrderItemIdAndStatus($orderItemListall[$count]->id,$type);
                    //     $orderItemListall[$count]->order = JModelLegacy::getInstance('order', 'enmasseModel')->getById($orderItemListall[$count]->order_id);
                    //     $orderI->invtyList = JModelLegacy::getInstance('invty', 'enmasseModel')->listByOrderItemIdAndStatus($orderItemListall[$count]->id,$type);
                    //     $orderI->order = JModelLegacy::getInstance('order', 'enmasseModel')->getById($orderItemListall[$count]->order_id);
                    //     $orderItemListall[$count]->dealcode = $deal->deal_code;
                    //     $orderItemListall[$count]->dealname = $deal->name;
                    //     $orderItemList[] = $orderItemListall[$count];
                    // }
                    $orderItemList = $orderItemList['orderItemList'];
                }
            }else{
                $deal = JModelLegacy::getInstance('deal', 'enmasseModel')->getById($dealId);
                    
                    
                    $from_at = JRequest::getVar('from_at');
                    $from_to = JRequest::getVar('from_to');
                    $type = JRequest::getVar('type');
                    $coupon = JRequest::getVar('coupon');
                    $orderItemList =  JModelLegacy::getInstance('orderItem', 'enmasseModel')->listByPdtIdAndStatusAndDateCustom($dealId, "Delivered' OR a.status ='Refunded",$from_at,$from_to,$orderby,$branch,$type,$coupon);

                    // for ($count = 0; $count < count($orderItemList); $count++) {
                    //     $orderItemList[$count]->invtyList = JModelLegacy::getInstance('invty', 'enmasseModel')->listByOrderItemIdAndStatus($orderItemList[$count]->id,$type);
                    //     $orderItemList[$count]->order = JModelLegacy::getInstance('order', 'enmasseModel')->getById($orderItemList[$count]->order_id);
                    //     $orderItemList[$count]->dealcode = $deal->deal_code;
                    //     $orderItemList[$count]->dealname = $deal->name;
                    // }
                     $orderItemList = $orderItemList['orderItemList'];
            }
           // print_r('<pre>');
            // print_r($orderItemList);die;
            $count = 1;
            $id = 0; //id of the item in itemList array
            for ($i = 0; $i < count($orderItemList); $i++) {
                $orderItem = $orderItemList[$i];
                $buyerDetail = json_decode($orderItem->buyer_detail);
                $deliveryDetail = json_decode($orderItem->delivery_detail);

                
                   // $invty = $orderItem->invtyList[$j];

                    $itemList[$id]['Serial No.'] = $count++ . "\t";
                    $itemList[$id]['ORDER_ID'] =  $orderItem->order_id. "\t";
                    $itemList[$id]['Deal Code'] = $orderItem->deal_code. "\t";
                    $itemList[$id]['Deal Name'] = $orderItem->name. "\t";
                    $itemList[$id]['Purchase Date'] = DatetimeWrapper::getDisplayDatetime($orderItem->created_at) . "\t";
                    $itemList[$id]['Amount'] = $orderItem->total_buyer_paid . "\t";
                    $itemList[$id]['Deposit'] = (int)($orderItem->deposit*$orderItem->qty) . "\t";
                    $itemList[$id]['Buyer Name'] = $buyerDetail->name . "\t";
                    $itemList[$id]['Buyer Email'] = $buyerDetail->email . "\t";
                    $itemList[$id]['Branch'] = $orderItem->branch . "\t";
                    $itemList[$id]['Delivery Name'] = $deliveryDetail->name . "\t";
                    $itemList[$id]['Delivery Email'] = $deliveryDetail->email . "\t";
                    // $itemList[$id]['Order Comment'] = $orderIte->description . "\t";
                   
                    $itemList[$id]['Coupon Serial'] = $orderItem->nameIn . "\t";

                    $itemList[$id]['Coupon Status'] = JTEXT::_('COUPON_' . strtoupper($orderItem->statusIn)) . "</br>";
                    if(strtotime($orderItem->deallocated_at)){
                    		
 					$itemList[$id]['deallocated_at'] = DatetimeWrapper::getDisplayDatetime($orderItem->deallocated_at) ." ".date("H:i:s",strtotime($orderItem->deallocated_at)). "\t";
                    		
                    	}else{
                     $itemList[$id]['deallocated_at'] = DatetimeWrapper::getDisplayDatetime($orderItem->deallocated_at) . "\t";		
                    	}
                   if(strtotime($orderItem->unused_date)){
                    		
 					$itemList[$id]['unused_date'] = DatetimeWrapper::getDisplayDatetime($orderItem->unused_date) ." ".date("H:i:s",strtotime($orderItem->unused_date)). "\t";
                    		
                    	}else{
                     $itemList[$id]['unused_date'] = DatetimeWrapper::getDisplayDatetime($orderItem->unused_date) . "\t";		
                    	}
                    	if(strtotime($orderItem->refund_date)){
                    		
 					$itemList[$id]['refund_date'] = DatetimeWrapper::getDisplayDatetime($orderItem->refund_date) ." ".date("H:i:s",strtotime($orderItem->refund_date)). "\t";
                    		
                    	}else{
                     $itemList[$id]['refund_date'] = DatetimeWrapper::getDisplayDatetime($orderItem->refund_date) . "\t";		
                    	}
                    // $itemList[$id]['unused_date'] =  DatetimeWrapper::getDisplayDatetime($orderItem->unused_date). "\t";
                    $itemList[$id]['unused_reason'] = $orderItem->unused_reason . "\t";
                    // $itemList[$id]['refund_date'] = DatetimeWrapper::getDisplayDatetime($orderItem->refund_date) . "\t";
                    $itemList[$id]['refund_reason'] = $orderItem->refund_reason . "\t";

                    $id++;
              
            }

            if ($tmp == "pdf") {
               
               
            } else {
                $filename = "Report" . date('Ymd') . ".xls";
               
                $rs =  '<table border="1">';
		        $rs .= "<thead>
		                         <tr><td colspan='9' style='font-size:16px; color:#0000FF; text-align:center;'>" . JTEXT::_('REPORT_TITLE') . "</td> </tr>
		                        <tr>
		                            <th width=\"5%\">" . JTEXT::_('REPORT_SERIAL') . "</th>
		                            <th width=\"5%\">" . JTEXT::_('ORDER_ID') . "</th>
		                            <th width=\"5%\">" . JTEXT::_('DEAL_CODE') . "</th>
		                            <th width=\"5%\">" . JTEXT::_('DEAL_NAME') . "</th>
		                            <th width=\"5%\">" . JTEXT::_('REPORT_PURCHASE_DATE') . "</th>
		                            <th width=\"5%\">" . JTEXT::_('AMOUNT') . "</th>
		                            <th width=\"5%\">" . JTEXT::_('DEPOSIT') . "</th>
		                            <th width=\"15%\">" . JTEXT::_('REPORT_BUYER_NAME') . "</th>
		                            <th width=\"15%\">" . JTEXT::_('REPORT_BUYER_MAIL') . "</th>
		                            <th width=\"15%\">" . JTEXT::_('Branch') . "</th>
		                          
		                            <th width=\"5%\">" . JTEXT::_('REPORT_COUPON_SERIAL') . "</th>
		                            <th width=\"5%\">" . JTEXT::_('REPORT_COUPON_STATUS') . "</th>
		                            <th width=\"5%\">" . JTEXT::_('REDEMPTION_DATE') . "</th>
		                            <th width=\"5%\">" . JTEXT::_('UNUSED_DATE') . "</th>
		                            <th width=\"5%\">" . JTEXT::_('UNUSED_REASON') . "</th>
		                            <th width=\"5%\">" . JTEXT::_('REFUND_DATE') . "</th>
		                            <th width=\"5%\">" . JTEXT::_('REFUND_REASON') . "</th>
		                        </tr>
		                    </thead>";

		        for ($i = 0; $i < count($itemList); $i++) {
		            $itemOrder = $itemList[$i];
		            $rs .= '<tr>';
		            $rs .= '<td>' . $itemOrder['Serial No.'] . '</td>';
		            $rs .= '<td>' . $itemOrder['ORDER_ID'] . '</td>';
		            $rs .= '<td>' . $itemOrder['Deal Code'] . '</td>';
		            $rs .= '<td>' . $itemOrder['Deal Name'] . '</td>';
		            $rs .= '<td>' . $itemOrder['Purchase Date'] . '</td>';
		            $rs .= '<td>' . $itemOrder['Amount'] . '</td>';
		            $rs .= '<td>' . $itemOrder['Deposit'] . '</td>';
		            $rs .= '<td>' . $itemOrder['Buyer Name'] . '</td>';
		            $rs .= '<td>' . $itemOrder['Buyer Email'] . '</td>';
		            $rs .= '<td>' . $itemOrder['Branch'] . '</td>';
		           
		            // $rs .= '<td>' . $itemOrder['Purchase Date'] . '</td>';
		            $rs .= '<td style="text-align:center;">' . $itemOrder['Coupon Serial'] . '</td>';
		            $rs .= '<td style="text-align:center;">' . $itemOrder['Coupon Status'] . '</td>';
		            $rs .= '<td style="text-align:center;">' . $itemOrder['deallocated_at'] . '</td>';
		            $rs .= '<td style="text-align:center;">' . $itemOrder['unused_date'] . '</td>';
		            $rs .= '<td style="text-align:center;">' . $itemOrder['unused_reason'] . '</td>';
		            $rs .= '<td style="text-align:center;">' . $itemOrder['refund_date'] . '</td>';
		            $rs .= '<td style="text-align:center;">' . $itemOrder['refund_reason'] . '</td>';

		            $rs .= '</tr>';
		        }
		        $rs .= '</table>';
		        $rs = chr(239) . chr(187) . chr(191) . $rs;
		        echo $rs;
                header('Content-Encoding: UTF-8');
                header("Content-Disposition: attachment; filename=\"$filename\"");
                header("Content-Type: application/vnd.ms-excel;charset=UTF-8");
                header("Pragma: no-cache");
                header("Expires: 0");
                header("Lacation: excel.htm?id=yes");
                exit(0);
            }
        } else {
        	$link = JRoute::_("index.php?Itemid=256", false);
            $this->setRedirect($link, JTEXT::_('REPORT_EMPTY_MSG'));
        }
    }
    
}
?>
