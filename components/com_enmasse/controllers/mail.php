<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse".DS."helpers". DS ."EnmasseHelper.class.php");

class EnmasseControllerMail extends JControllerLegacy
{
	function __construct()
	{
		parent::__construct();
	}

	function display($cachable = false, $urlparams = false) {
		
	}
	function sendMail()
	{
		$post = JRequest::get('post');
		$share_url = JURI::base().'index.php?option=com_enmasse&controller=deal&task=view&id='.$post['dealid'];
        if($post['userid']!='0')
        {
			$share_url .= '&referralid='.$post['userid'];	                	
		}
        $share_url .= '&Itemid='.$post['itemid'];
		$content = $post['content'] . "<br/>" . JText::_('DEAL_LINK') . ": " . $share_url;		
		if(EnmasseHelper::sendMail($post['recipient'], $post['subject'], $content))
		{
			$this->setRedirect('index.php?option=com_enmasse&controller=mail&task=mailForm&tmpl=component&success=1');
		}
		else
		{
			$this->setRedirect('index.php?option=com_enmasse&controller=mail&task=mailForm&tmpl=component');
		}
	}

	function mailForm()
	{
		JRequest::setVar('view', 'mail');
		parent::display();
	}
        
        function reSendCoupon(){
             $orderId = JRequest::getVar( 'orderId');
             $orderItemList = JModelLegacy::getInstance('orderItem', 'enmasseModel')->listByOrderId($orderId);
             
             $oSession = JFactory::getSession();
             if($oSession->get('sendCoupon'.$orderId))
             {
                 echo '0';
                 exit();
             }
             
             $user = JFactory::getUser();
             //check order belong this user or not
             $oModelOrder = JModelLegacy::getInstance('Order', 'EnmasseModel');
             $valid = $oModelOrder->checkOrderUser($orderId, $user->id);
             
             if($valid){
             $order = JModelLegacy::getInstance('order', 'enmasseModel')->getById($orderId);
             if($order->status == "Delivered"){
                    foreach($orderItemList as $orderItem){
                       $buyerDetail = json_decode($order->buyer_detail);
                       $deliveryDetail = json_decode($order->delivery_detail);

                       $params = array();

                        // Email the Receiver
                       $token = EnmasseHelper::generateOrderItemToken($orderItem->id, $orderItem->created_at);
                       $link = JURI::root() . '/index.php?option=com_enmasse&controller=coupon&task=listing&orderItemId=' . $orderItem->id . '&token=' . $token . '&buffer=';
                       $params = array();
                       $params['$orderId'] = EnmasseHelper::displayOrderDisplayId($order->id);
                       $params['$dealName'] = $orderItem->description;
                       $params['$buyerName'] = $buyerDetail->name;
                       $params['$deliveryName'] = $deliveryDetail->name;
                       $params['$deliveryEmail'] = $deliveryDetail->email;
                       $params['$deliveryMsg'] = $deliveryDetail->msg;
                       $params['$linkToCoupon'] = $link;

                       EnmasseHelper::sendMailByTemplate($deliveryDetail->email, 'confirm_deal_receiver', $params);
                    }
                    $oSession->set('sendCoupon'.$orderId, '1');
                    echo "1";
                    exit();
                }else{
                    echo "Your order doesn't delivered yet";
                    exit();
                }
            }
            else {
                echo "You don't have permission to make this process";
                exit();
            }
          
        }
        
        function reSendQuotation(){
             $orderId = JRequest::getVar( 'orderId');
             
             $oSession = JFactory::getSession();
             if($oSession->get('sendQuotation'.$orderId))
             {
                 echo '0';
                 exit();
             }
             $user = JFactory::getUser();
             //check order belong this user or not
             $oModelOrder = JModelLegacy::getInstance('Order', 'EnmasseModel');
             $valid = $oModelOrder->checkOrderUser($orderId, $user->id);
             
             if($valid){
                EnmasseHelper::sendOrderEmail($orderId);
             }
             else {
                 echo "You don't have permission to make this process";
                 exit();
             }
             
            $oSession->set('sendQuotation'.$orderId, '1');
            echo "1";
            exit();
        }
}
?>