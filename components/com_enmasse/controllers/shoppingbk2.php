<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.controller');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."DatetimeWrapper.class.php");
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");


require_once( JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."Cart.class.php");
require_once( JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."CartHelper.class.php");

class EnmasseControllerShopping extends JControllerLegacy
{
	function __construct()
	{
		parent::__construct();
	}

	function display($cachable = false, $urlparams = false) {
	}

	// Shopping Cart
	function viewCart()
	{
		JRequest::setVar('view', 'shopcart');
		parent::display();
	}

	// Shopping Cart
	function reCheckout()
	{
		JRequest::setVar('view', 'shopcheckout');
		parent::display();
	}

	function addToCart()
	{
                //JFactory::getSession()->set('cart','');die;
		$dealId = JRequest::getVar('dealId');
		$referralId = JRequest::getVar('referralid');
		$bBuy4friend = JRequest::getVar('buy4friend',0);
        $typeId = JRequest::getVar('typeId');
		$qtyVal = JRequest::getVar('qty',1);
		$deal = JModelLegacy::getInstance('deal','enmasseModel')->getById($dealId);
		//from enmasse 3.1 we have to have atributes for a deals, here we go, load all types of deal
		$deal->types = JModelLegacy::getInstance('dealType','enmasseModel')->getDealTypeListByIdDeal($dealId);
		
		//load type of deal
		if(($deal->tier_pricing)==0)
		{
			if($typeId != 'none' and  !empty($typeId) )
			{
				$deal->type = JModelLegacy::getInstance('dealType','enmasseModel')->getById($typeId);
				
				$deal->price = $deal->type->price;
			}
			else
			{
				$deal->type = NULL;
		
				if (!empty($deal->types))
				{				
					$deal->type = JModelLegacy::getInstance('dealType','enmasseModel')->getById($deal->types[0]->id);
					$deal->price = $deal->type->price;
				}
			}
		}
		else
		{		
			$deal->type = NULL;	
			$deal->price = EnmasseHelper::getCurrentPriceStep(unserialize($deal->price_step),$deal->price,$deal->cur_sold_qty);
		}
		//*************************************************************************************
		// check Deal
		$now = DatetimeWrapper::getDatetimeOfNow();
		if ($now < $deal->start_at)
		{
			$msg = JText::_( "DEAL_NOT_READY");
			$link = JRoute::_("index.php?option=com_enmasse&controller=deal&task=view&id=".$deal->id, false);
			JFactory::getApplication()->redirect($link, $msg);
		}
		elseif ( time() > (strtotime($deal->end_at) + 24*60*60))
		{
			$msg = JText::_( "DEAL_END_MSG");
			$link = JRoute::_("index.php?option=com_enmasse&controller=deal&task=view&id=".$deal->id, false);
			JFactory::getApplication()->redirect($link, $msg);
		}
		elseif($deal->published == false)
		{
			$msg = JText::_( "DEAL_NOLONGER_PUBLISH");
			$link = JRoute::_("index.php?option=com_enmasse&controller=deal&task=view&id=".$deal->id, false);
			JFactory::getApplication()->redirect($link, $msg);
		}
		elseif($deal->status == "Voided")
		{
			$msg = JText::_( "DEAL_HAVE_BEEN_VOID");
			$link = JRoute::_("index.php?option=com_enmasse&controller=deal&task=view&id=".$deal->id, false);
			JFactory::getApplication()->redirect($link, $msg);
		}
		else
		{
			// add to cart (return multi item from version 5.0)
			
			$cart = unserialize(JFactory::getSession()->get('cart'));
                        $new_cart = '0';

            //Duc Edit: Set order 1 item
             $cart = new Cart();

			if (empty($cart)){
                            $cart = new Cart();
                            $new_cart = '1';
                        }
                        //check current buy of user on this deal
                        $arTotal = 0;
                        if(JFactory::getUser()->get('id')){
                            $arTotal = EnmasseHelper::getTotalBoughtQtyOfUser(JFactory::getUser()->id, array($deal->id));
                        }
                        foreach($cart->getAll() as $cartItem){
                            $item = $cartItem->getItem();
                            if($item->id == $deal->id){
                                $bought_qty = $cartItem->getCount();
                            }
                        }
                        $buying = $bought_qty;
                        if($arTotal){
                            $deal_qty = array_pop($arTotal);
                            $bought_qty = $deal_qty->total + $bought_qty;
                        }
                        
                        //check min max item allow per order
                        $min_item_in_order = '1';
                        if($deal->min_item_per_order){
                            if($buying < $deal->min_item_per_order){
                                $min_item_in_order  = $deal->min_item_per_order;
                                $bought_qty =  $bought_qty + $min_item_in_order - 1;// need -1 to validation purpose
                            }
                        }
                        if($deal->max_item_per_order){
                            if(($buying+$min_item_in_order) > $deal->max_item_per_order)
                            {
                                $msg = JText::_("QUANTITY_MAX_FOR_ORDER").' ('.$deal->max_item_per_order.').'.$item->name;
                                JFactory::getApplication()->redirect("index.php?option=com_enmasse&controller=shopping&task=checkout",$msg);
                            }
                        }
                        //
                        //------ End check min max --------
                        
			$max_buy_qty = $deal->max_buy_qty;
                        if($max_buy_qty == '-1' || ($max_buy_qty >=0 && $bought_qty < $max_buy_qty)){
                            if($qtyVal>0){
                                $min_item_in_order = $qtyVal;
                            }
                            $cart->addItem($deal,$min_item_in_order);                            
                        }else if($buying >0){
                           $msg = JText::_('ITEM_UPDATE_GREATER_THAN_MAX').$item->name;
                            JFactory::getApplication()->redirect("index.php?option=com_enmasse&controller=shopping&task=checkout",$msg);
                        }  else {
                            $msg = JText::_("QUANTITY_GREATER_THAN_MAX").$item->name;
                            JFactory::getApplication()->redirect("index.php?option=com_enmasse&&view=deallisting",$msg);
                        }
                        
			//Set sesstion for referral ID
			if($referralId!='')
			{
				$cart->setReferralId($referralId);
			}
                        
			JFactory::getSession()->set('cart', serialize($cart));
			$dealName = $deal->name;
			//$cartItemCount = $cart->getItem($dealId)->getCount();

			$msg = "";//$dealName . " ". JText::_( "DEAL_ADD_TO_CART");
			$link = JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$bBuy4friend", false);
			JFactory::getApplication()->redirect($link, $msg);
		}
	}

	function emptyCart()
	{
		$cart = unserialize(JFactory::getSession()->get('cart'));

		if ( !empty($cart) || $cart->getTotalItem() != 0 )
		{
			$cart->deleteAll();
			JFactory::getSession()->set('cart', serialize($cart));
		}

		$msg = JText::_( "EMPTY_CART");
		$link = JRoute::_("index.php?option=com_enmasse&controller=deal&task=listing", false);
		JFactory::getApplication()->redirect($link, $msg);
	}

	function removeItem()
	{
		$itemId = JRequest::getVar('itemId');

		$cart = unserialize(JFactory::getSession()->get('cart'));
		CartHelper::checkCart($cart);
		$cart->deleteItem($itemId);
		JFactory::getSession()->set('cart', serialize($cart));

		$msg = JText::_( "ITEM_REMOVE_MSG");
		$link = JRoute::_("index.php?option=com_enmasse&controller=shopping&task=viewCart", false);
		JFactory::getApplication()->redirect($link, $msg);
	}

	function changeItem()
	{
                $delete_item = JRequest::getVar('deleteItem');

		$cart = unserialize(JFactory::getSession()->get('cart'));

		CartHelper::checkCart($cart);
                $oItems = $cart->getAll();
                $user_id = JFactory::getUser()->id;
                $nItemId = JFactory::getApplication()->getMenu()->getItems('link','index.php?option=com_enmasse&view=deallisting',true)->id;
                
                foreach($oItems as $oCartItem){
                    $item = $oCartItem->getItem();
                    $new_qty = JRequest::getVar('value'.$item->id);
                    
                    //remove item when customer click on delete
                    if($delete_item == $item->id){
                        $new_qty = 0;
                    }
                    
                    $maxQtyOfDeal = EnmasseHelper::getMaxQtyOfDeal($item->id);
                    $maxBuyQtyOfDeal = EnmasseHelper::getMaxBuyQtyOfDeal($item->id);
                    $totalBoughtQtyOfUser = EnmasseHelper::getTotalBoughtQtyOfUserByDeal($user_id,array($item->id))->total;
                    
                    if($new_qty > 0){
                        if($item->min_item_per_order){
                            if($new_qty < $item->min_item_per_order){
                                $msg = JText::_("QUANTITY_MIN_FOR_ORDER").' ('.$item->min_item_per_order.').'.$item->name;
                                JFactory::getApplication()->redirect("index.php?option=com_enmasse&controller=shopping&task=checkout&Itemid=".$nItemId,$msg);
                            }
                        }
                        if($item->max_item_per_order){
                            if($new_qty > $item->max_item_per_order){
                                $msg = JText::_("QUANTITY_MAX_FOR_ORDER").' ('.$item->max_item_per_order.').'.$item->name;
                                JFactory::getApplication()->redirect("index.php?option=com_enmasse&controller=shopping&task=checkout&Itemid=".$nItemId,$msg);
                            }
                        }
                    }
                    //check max quantity of coupon
                    if($maxQtyOfDeal >= 0 && ($new_qty + $item->cur_sold_qty) > $maxQtyOfDeal)
                    {				
                            $msg = JText::_("ITEM_UPDATE_GREATER_THAN_MAX").$item->name.'Erro1';

                            JFactory::getApplication()->redirect("index.php?option=com_enmasse&controller=shopping&task=checkout&Itemid=".$nItemId,$msg);
                    }
                    
                    //check max quantity by user
                    if($maxBuyQtyOfDeal >= 0 && ($new_qty + $totalBoughtQtyOfUser) > $maxBuyQtyOfDeal)
                    {	
                            $msg = JText::_("ITEM_UPDATE_GREATER_THAN_MAX").$item->name.'Erro2'.$new_qty.'-'.$totalBoughtQtyOfUser.'-'.$maxBuyQtyOfDeal;
                            JFactory::getApplication()->redirect("index.php?option=com_enmasse&controller=shopping&task=checkout&Itemid=".$nItemId,$msg);
                    }
                    
                    $voucher_code = JRequest::getVar('voucher_code');
                    $voucher = '';
                    if($voucher_code){
                        $voucher = $this->getVoucher($voucher_code);
                        $valid = $this->checkValidVoucher($voucher);
                        if($valid == '9'){
                            $cart->setVoucher($voucher);
                            $cart->setVoucherCode($voucher_code);
                        }else{
                            $cart->setVoucher('');
                            $cart->setVoucherCode('');
                        }
                        JFactory::getSession()->set('coupon_valid',$valid);
                    }
                    
                    if($item->insurances){
                        $insuranceFee = 0;
                        $insuranceChild = 0;
                        $insuranceAdult = 0;
                        $insuranceJson ="";
                        $insuranceArray =array();
                        foreach ($item->insuranceList as $insurance) {
                           $childVal = JRequest::getVar('child-'.$insurance->id);
                           $adultVal = JRequest::getVar('adult-'.$insurance->id);
                           if($childVal){
                                $insuranceChild+=$childVal;
                                $insuranceFee +=$childVal*$insurance->child_offer_price;

                                $insuranceArray[$insurance->id]->id =$insurance->id;
                                $insuranceArray[$insurance->id]->childVal =$childVal;
                                $insuranceArray[$insurance->id]->childFee =$childVal*$insurance->child_offer_price;
                                $insuranceArray[$insurance->id]->insurance_name =$insurance->insurance_name;
                           }
                           if($adultVal){
                                $insuranceAdult+=$adultVal;
                                $insuranceFee +=$adultVal*$insurance->adult_offer_price;

                                $insuranceArray[$insurance->id]->id =$insurance->id;
                                $insuranceArray[$insurance->id]->adultVal =$adultVal;
                                $insuranceArray[$insurance->id]->adultFee =$adultVal*$insurance->adult_offer_price;
                                $insuranceArray[$insurance->id]->insurance_name =$insurance->insurance_name;
                           }
                          
                        }
                        
                        $cart->insuranceFee =$insuranceFee;
                        $cart->insuranceChild =$insuranceChild;
                        $cart->insuranceAdult =$insuranceAdult;
                        $cart->insuranceJson = json_encode($insuranceArray);
                        $cart->setInsuranceFee($insuranceFee);
                    }


                    $cart->changeItem($item->id, $new_qty, $item->price,$item->types,$item->type);
                }
        $setting = JModelLegacy::getInstance('setting','enmasseModel')->getSetting();
        $cart->setting = $setting;        
		JFactory::getSession()->set('cart', serialize($cart));
                
                $msg = "";//JText::_( "ITEM_UPDATE_MSG");
		$link = JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$buy4friend&Itemid=$nItemId&step=2", false);
		JFactory::getApplication()->redirect($link, $msg);
	}
        
        public function getVoucher($voucher_code)
        {
            $oDb = JFactory::getDbo();
            $oQuery = $oDb->getQuery(true);

            $oQuery->select('*');
            $oQuery->from('#__enmasse_voucher');
            $oQuery->where('voucher_code = '.$oDb->quote($voucher_code));

            $oDb->setQuery($oQuery);
            $rs = $oDb->loadObject();
		
            return $rs;
        }
        
        function checkValidVoucher($voucher = '')
        {
            if(!$voucher)
                return 0;//voucher don't exist
            
            $user = JFactory::getUser();
            //check valid user for this coupon
            if($voucher->user_id && $voucher->user_id != '0'){
                if(!$user->id || $user->id != $voucher->id){
                    return 1;//invalid user, this deal for special person
                }
            }
            
            //check valid use per voucher
            if($voucher->use_per_coupon != '-1'){
                $num_of_used = $this->countUsedVoucher($voucher->id);
                if($num_of_used >= $voucher->use_per_coupon)
                {
                    return 2;//over max used allow for each voucher
                }
            }
            
            //check valid voucher used by each user
            if($voucher->use_per_user != '-1'){
                // if(!$user->id)
                // {
                //     return 4;//must login to used this voucher
                // }
                
                $num_used_user = $this->countUsedUser($voucher->id,$user->id);
                if($num_used_user >= $voucher->use_per_user)
                {
                    return 3;//over max used allow for user on this voucher
                }
            }
            
            return 9;// voucher is valid
        }
        
        public function countUsedVoucher($voucher_id)
        {
            $oDb = JFactory::getDbo();
            $oQuery = $oDb->getQuery(true);

            $oQuery->select('count(*) as num_used');
            $oQuery->from('#__enmasse_order_voucher');
            $oQuery->where('voucher_id = '.$oDb->quote($voucher_id));

            $oDb->setQuery($oQuery);
            $rs = $oDb->loadObject();
	
            return $rs->num_used;
        }
        
        public function countUsedUser($voucher_id, $user_id)
        {
            $oDb = JFactory::getDbo();
            $oQuery = $oDb->getQuery(true);

            $oQuery->select('count(*) as num_used');
            $oQuery->from('#__enmasse_order_voucher AS ov');
            $oQuery->join('left', '#__enmasse_order AS o ON o.id = ov.order_id');
            $oQuery->where('ov.voucher_id = '.$oDb->quote($voucher_id));
            $oQuery->where('o.buyer_id = '.$oDb->quote($user_id));

            $oDb->setQuery($oQuery);
            $rs = $oDb->loadObject();

            return $rs->num_used;
        }
        
	function changePoint()
	{
		$value = JRequest::getVar('value');
		$buy4friend = JRequest::getVar('buy4friend', 0, 'method', 'int');

		$cart = unserialize(JFactory::getSession()->get('cart'));
		CartHelper::checkCart($cart);
		$currentPoint = $cart->getPoint();
		//$point = $value + $currentPoint;
		$totalPrice = $cart->getTotalPrice();

		//------------------------
		//gemerate integration class
		$isPointSystemEnabled = EnmasseHelper::isPointSystemEnabled();
		if($isPointSystemEnabled==true)
		{
			$integrationClass = EnmasseHelper::getPointSystemClassFromSetting();
			$integrateFileName = $integrationClass.'.class.php';
			require_once (JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."pointsystem". DS .$integrationClass. DS.$integrateFileName);
			 
			$integrationObject = new $integrationClass();
			$user = JFactory::getUser();
			$user_id = $user->get('id');

				
			if($value < 0 || !is_numeric($value))
			{
				$msg = JText::_("INVALID_POINT");
				JFactory::getApplication()->redirect(JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$buy4friend"),$msg);
			}

			if(!$integrationObject->checkEnoughPoint($user_id, $value))
			{
				$msg = JText::_("NOT_ENOUGH_POINT");
				JFactory::getApplication()->redirect(JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$buy4friend"),$msg);

			}

			if($value < 0 || $value > $totalPrice)
			{
				$msg = JText::_("POINT_GREATER_THAN_TOTAL_PRICE");
				JFactory::getApplication()->redirect(JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$buy4friend"),$msg);
			}
				
			$cart->changePoint($value);

			JFactory::getSession()->set('cart', serialize($cart));
			$msg = JText::_( "ITEM_UPDATE_MSG");
			$link = JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$buy4friend", false);
			JFactory::getApplication()->redirect($link, $msg);
		}
		else
		{
			$msg = JText::_( "NO_POINT_SYSTEM");
			$link = JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$buy4friend", false);
			JFactory::getApplication()->redirect($link, $msg);
		}
	}

	//--------------------------------------------------------------------

	function checkout()
	{
		JRequest::setVar('view', 'shopcheckout');
		$activeGuestBuying = EnmasseHelper::isGuestBuyingEnable();
		if (JFactory::getUser()->get('guest') && $activeGuestBuying==false)
		{
                        $nItemId = JFactory::getApplication()->getMenu()->getItems('link','index.php?option=com_enmasse&view=deallisting',true)->id;
			$msg = JText::_('ORDER_PLEASE_LOGIN_BEFORE');
			$redirectUrl = base64_encode(JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&Itemid=".$nItemId,false));
			$link = JRoute::_("index.php?option=com_users&view=registration&return=".$redirectUrl, false);

			JFactory::getApplication()->redirect($link, $msg);
		}
		else
		{
			$cart = unserialize(JFactory::getSession()->get('cart'));
			if(empty($cart) || $cart->getTotalItem() == 0)
			{
				$msg = JText::_( "CART_IS_EMPTY");
				$link = JRoute::_("index.php?option=com_enmasse&controller=deal&task=listing", false);

				JFactory::getApplication()->redirect($link, $msg);
			}
				
			$arTotal = array();

			if(JFactory::getUser()->get('id'))
			{
				$arDealId =array();

				foreach ($cart->getAll() as $cartItem)
				{
					$arDealId[] = $cartItem->getItem()->id;
				}

				//get array total quantity of deals
				$arTotal = EnmasseHelper::getTotalBoughtQtyOfUser(JFactory::getUser()->get('id'), $arDealId);
			}
			
			// check cart item
			$this->checkMaxCartItem($cart);
			
			parent::display();

		}

	}
    function result(){
        JRequest::setVar('view', 'result');
        parent::display();
    }
    function submitPersonalData(){
        ini_set('display_errors',1);
        error_reporting(E_ALL);
        $arData = JRequest::get('post');
        $data = "";
        $submit = true;
        $msg = JText::_( "");

        $cart = unserialize(JFactory::getSession()->get('cart'));
        if($arData){
           //  $data->surname=$arData['surname'];
           //  $data->name=$arData['name'];
           //  $data->sex=$arData['sex'];
           // $data->birthday=$arData['birthday'];
           //  $data->surname2=$arData['surname2'];
           //  $data->name2=$arData['name2'];
           //  $data->sex2=$arData['sex2'];
           //   $data->birthday2=$arData['birthday2'];
            $data->surname3=$arData['surname3'];
            $data->name3=$arData['name3'];
            $data->phone3=$arData['phone3'];
            $data->email3=$arData['email3'];
            $data->payGtyId=$arData['payGtyId'];
            foreach ($data as $key => $value) {
                if($value==""){
                    $submit= false;
                    $msg = JText::_( "Please input the ".$key);
                    $cart->passengerInfo = $data;
                    break;
                }
            }
            $voucher_code = $arData['discount'];
            if($voucher_code&&$submit){

                    $voucher = '';
                    if($voucher_code){
                        $voucher = $this->getVoucher($voucher_code);
                        $valid = $this->checkValidVoucher($voucher);

                        if($valid == '9'){
                            $cart->setVoucher($voucher);
                            $cart->setVoucherCode($voucher_code);
                             $msg = "";//JText::_( "The Voucher Code is valid");
                            $submit= true;
                            $data->discountObject = $voucher;
                            $data->discount = $arData['discount'];

                        }else{
                            $cart->setVoucher('');
                            $cart->setVoucherCode('');
                            $msg = JText::_( "The Voucher Code is not correct");
                            $submit= false;
                            $data->discountObject ="";
                            $data->discount ="";
                        }
                        JFactory::getSession()->set('coupon_valid',$valid);
                    }
                    
            }
            // Calculate Total
            $cart->recalculateCart();
                 //$id = JModelLegacy::getInstance('order','enmasseModel')->storeOrderPassengers($data);
            $cart->passengerInfo = $data;
           
            $branch = $arData['branch'];
            if($branch==""){
                 $msg = JText::_( "請選擇分行");
                $submit =false;
            }else{
                 $cart->branch =$branch;
            }
            $sEmailPt = "/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/";
            if (!preg_match($sEmailPt, $data->email3))
            {
                $msg = JText::_("SHOP_CARD_CHECKOUT_RECEIVER_EMAIL_INVALID_MSG");
                $submit=false;
               
            }
         
        }
        $link = JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$buy4friend&Itemid=$nItemId&step=3", false);
        if($submit==false){
            
            $link = JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$buy4friend&Itemid=$nItemId&step=2", false);
        }

        JFactory::getSession()->set('cart', serialize($cart));
        JFactory::getApplication()->redirect($link, $msg);

    }
	function submitCheckOut()
	{
		$activeGuestBuying = EnmasseHelper::isGuestBuyingEnable();
		$bBuy4friend = JRequest::getVar('buy4friend', 0);
		$sEmailPt = "/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/";

		//save user input data into the session
		if(JRequest::getMethod() == "POST")
		{
			$arData = JRequest::get('post');
			JFactory::getApplication()->setUserState("com_enmasse.checkout.data", $arData);
		}

		//check the permission for checkout action
		if (JFactory::getUser()->get('guest') && !$activeGuestBuying)
		{
			$msg = JText::_( "MERCHANT_PLEASE_LOGIN_BEFORE");
			$redirectUrl = base64_encode("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$bBuy4friend&step=2");
			$link = JRoute::_("index.php?option=com_users&view=login&return=".$redirectUrl, false);
			JFactory::getApplication()->redirect($link, $msg, 'error');
		}

		//validate the cart
		$cart = unserialize(JFactory::getSession()->get('cart'));
		CartHelper::checkCart($cart);
		foreach($cart->getAll() as $cartItem)
		{
			$item = $cartItem->getItem();
		}
		//get enmasse setting
		$setting = JModelLegacy::getInstance('setting','enmasseModel')->getSetting();
		
		// check max cart Item
		$this->checkMaxCartItem($cart);

		//validate Buyer information
		$buyerName 	= JRequest::getVar('name');
		$buyerEmail 	= JRequest::getVar('email');
                $receiverMsg 	= JRequest::getVar('receiver_msg');
                $receiverAddress = JRequest::getVar('receiver_address');
                $receiverPhone = JRequest::getVar('receiver_phone');
                $receiverPCode	= JRequest::getVar('post_code');
//                $receiverDate	= JRequest::getVar('delivery_date');
//                $receiverTime 	= JRequest::getVar('delivery_time');
//                $receiverDate = $receiverDate.' '.$receiverTime.':00';
                        
		if(empty($buyerName) || empty($buyerEmail))
		{
			$msg = JText::_("SHOP_CARD_CHECKOUT_BUYER_INFORMATION_REQUIRED_MSG");
			$link = JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$bBuy4friend&step=2", false);
			JFactory::getApplication()->redirect($link, $msg, 'error');
		}
		elseif (!preg_match($sEmailPt, $buyerEmail))
		{
			$msg = JText::_("SHOP_CARD_CHECKOUT_BUYER_EMAIL_INVALID_MSG");
			$link = JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$bBuy4friend&step=2", false);
			JFactory::getApplication()->redirect($link, $msg, 'error');
		}

		//----- If the deal permit partial payment, it mean the coupon was delivery by directly, so we need to validate address and phone number of receiver
		/*if($item->prepay_percent <100)
		{
			$receiverAddress = JRequest::getVar('receiver_address');
			$receiverPhone = JRequest::getVar('receiver_phone');
				
			if(empty($receiverPhone) || empty($receiverAddress))
			{
				$msg = JText::_( "SHOP_CARD_CHECKOUT_RECEIVER_INFORMATION_REQUIRED_MSG");
				$link = JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$bBuy4friend", false);
				JFactory::getApplication()->redirect($link, $msg, 'error');
			}else if(!preg_match('/^[0-9 \.,\-\(\)\+]*$/', $receiverPhone))
			{
				$msg = JText::_( "SHOP_CARD_CHECKOUT_RECEIVER_PHONE_INVALID");
				$link = JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$bBuy4friend", false);
				JFactory::getApplication()->redirect($link, $msg, 'error');
			}
		}*/

		$item->prepay_percent = 100;
		if($bBuy4friend)
		{
			$receiverName = JRequest::getVar('receiver_name');
			$receiverEmail = JRequest::getVar('receiver_email');
			$receiverMsg 	= JRequest::getVar('receiver_msg');
			if(empty($receiverName) || empty($receiverEmail))
			{
				$msg = JText::_( "SHOP_CARD_CHECKOUT_RECEIVER_INFORMATION_REQUIRED_MSG");
				$link = JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$bBuy4friend&step=2", false);
				JFactory::getApplication()->redirect($link, $msg, 'error');
			}
			elseif (!preg_match($sEmailPt, $receiverEmail))
			{
				$msg = JText::_("SHOP_CARD_CHECKOUT_RECEIVER_EMAIL_INVALID_MSG");
				$link = JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$bBuy4friend&step=2", false);
				JFactory::getApplication()->redirect($link, $msg, 'error');
			}

		}

		//------------------------------------------------------
		// to check it this deal is free for customer
		if($cart->getTotalPrice() > 0)
		{
			//deal is not free
			$payGtyId 	= JRequest::getVar('payGtyId');
				
			if($payGtyId == null )
			{
				$msg = JText::_( "SELECT_PAYMENT_MSG");
				$link = JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$bBuy4friend&step=2", false);
				JFactory::getApplication()->redirect($link, $msg, 'error');
			}

			if($setting->article_id != 0 && JRequest::getVar('terms')==false)
			{
				$msg = JText::_( "AGREE_TERM_CONDITION_MSG");
				$link = JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$bBuy4friend&step=2", false);
				JFactory::getApplication()->redirect($link, $msg, 'error');
			}
				
			$payGty = JModelLegacy::getInstance('payGty','enmasseModel')->getById($payGtyId);
			// checking gateway configuration
			if(CartHelper::checkGty($payGty)==false)
			{
				$msg = JText::_( "PAYMENT_INCOMPLETE_MSG");
				$link = JRoute::_("index.php?option=com_enmasse&controller=shopping&task=checkout&buy4friend=$bBuy4friend&step=2", false);
				JFactory::getApplication()->redirect($link, $msg);
			}
				
			// save gty info into session
			JFactory::getSession()->set('payGty', serialize($payGty));
			JFactory::getSession()->set('attribute_config', json_encode($payGty->attribute_config));
				
			//--------If admin set the prepay_percent of the deal to 0.00, set the order status to 'Paid' (with paid_amount is 0.00)
			if($item->prepay_percent == 0.00)
			{
				$status = EnmasseHelper::$ORDER_STATUS_LIST['Paid'];
				$couponStatus = EnmasseHelper::$INVTY_STATUS_LIST['Hold'];
			}else
			{
				//------------------------------------
				// generate name of payment gateway file and class
				$payGtyFile = 'PayGty'.ucfirst($payGty->class_name).'.class.php';
				$className = 'PayGty'.ucfirst($payGty->class_name);
				//---------------------------------------------------
				// get payment gateway object
				require_once (JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."payGty". DS .$payGty->class_name. DS.$payGtyFile);
				$paymentClassObj = new $className();
				$paymentReturnStatusObj = $paymentClassObj->returnStatus();

				$status = $paymentReturnStatusObj->order;
				$couponStatus = $paymentReturnStatusObj->coupon;
			}
				
		}
		else
		{
			//deal is free
			$payGty = "Free";
			$status = 'Unpaid';
			$couponStatus = 'Pending';
				
			//save the payGty as free
			JFactory::getSession()->set('payGty', 'Free');
		}
		//----------------------------------------
		//determine information of coupon receiver
		if($bBuy4friend)
		{
			$deliveryDetail = array ('name' => $receiverName, 'email' => $receiverEmail, 'msg' => $receiverMsg, 'address' => $receiverAddress, 'phone' => $receiverPhone);
		}
		else
		{
			$deliveryDetail = array ('name' => $buyerName, 'email' => $buyerEmail, 'msg' => $receiverMsg, 'address' => $receiverAddress, 'phone' => $receiverPhone);
		}

		//--------------------------
		//generate order
		$dvrGty = ($item->prepay_percent < 100)? 2: 1;
		$deliveryGty 	= JModelLegacy::getInstance('deliveryGty','enmasseModel')->getById($dvrGty);

		$user = array();
		$user['id'] = JFactory::getUser()->get('id',0);
		$user['name'] = $buyerName;
		$user['email'] = $buyerEmail;
        $insurance_fee ="";
        $insurance_fee = $cart->insuranceJson;
        $branch = $cart->branch;
		$order 			= CartHelper::saveOrder($cart, $user, $payGty, null, $deliveryGty, $deliveryDetail,$status,0,$insurance_fee,$branch);

              
                //archive order
                $mOrder = JModelLegacy::getInstance('order','enmasseModel');
                $totalOrder = $mOrder->getCountOrder();
                if($totalOrder%100 == 0){
                    $mOrder->archiveOrderHaveDealExprie();
                    $mOrder->archiveOrderPaidOlder30();
                }
                //-------- end archive --------
                
		$session =& JFactory::getSession();
		$session->set( 'newOrderId', $order->id );

		$orderItemList 	= CartHelper::saveOrderItem($cart, $order,$status);
                //save order coupon(discount voucher)
                $voucher = $cart->getVoucher();
                if($voucher){
                    if($voucher->type == 'Percentage'){
                        $discount = $cart->getAmountToPay() * 100 / (100 - $voucher->discount_amount) - $cart->getAmountToPay();
                    }
                    else {
                        $discount = $voucher->discount_amount;
                    }
                    CartHelper::saveOrderVoucher($voucher->id, $order->id, $discount);
                }
        //Save Passenger Info
        $passengerInfo = $cart->passengerInfo;
        unset($passengerInfo->payGtyId);        
        unset($passengerInfo->discount);        
        unset($passengerInfo->discountObject);        
        $passengerInfo->order_id =$order->id;        
        $passengerID = JModelLegacy::getInstance('order','enmasseModel')->storeOrderPassengers($passengerInfo);         
		//-----------------------------
		// if this deal is set limited the coupon to sold out, go to invty and allocate coupons for this order
		// if not create coupons for that order
                
                $i = 0;
                foreach($cart->getAll() as $cartItem)
		{
			$item = $cartItem->getItem();
                        if($item->max_coupon_qty > 0)
                        {
                                $now = DatetimeWrapper::getDatetimeOfNow();
                                $nunOfSecondtoAdd = (EnmasseHelper::getMinuteReleaseInvtyFromSetting($payGty))*60;
                                $intvy             = CartHelper::allocatedInvty(array($orderItemList[$i]),DatetimeWrapper::mkFutureDatetimeSecFromNow($now,$nunOfSecondtoAdd),$couponStatus);
                        }
                        else
                        {
                               JModelLegacy::getInstance('invty','enmasseModel')->generateForOrderItem($orderItemList[$i]->pdt_id, $orderItemList[$i]->id, $orderItemList[$i]->qty, $couponStatus);
                        }
                        $i++;
		}
       
		//------------------------
		//generate integration class
		$isPointSystemEnabled = EnmasseHelper::isPointSystemEnabled();
		if($isPointSystemEnabled)
		{
			$integrationClass = EnmasseHelper::getPointSystemClassFromSetting();
			$integrateFileName = $integrationClass.'.class.php';
			require_once (JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."pointsystem". DS .$integrationClass. DS.$integrateFileName);
			$user = JFactory::getUser();
			$user_id = $user->get('id');
			$point = $cart->getPoint();
			if($point>0) //If user buys with point, point will be greater than zero
			{
				$integrationObject = new $integrationClass();
				$integrationObject->integration($user_id,'paybypoint',$point);
			}
		}

		//validating is ok, flush user data
		JFactory::getApplication()->setUserState("com_enmasse.checkout.data", null);
			
		// --------------------------------
		// if deal is free then directly do the notify
		if($cart->getTotalPrice() > 0)
		{
			$link = JRoute::_("index.php?option=com_enmasse&controller=payment&task=gateway&orderId=" . $order->id, false);
			//deal is not free, check if buyer must prepay a specific amount
			/*if($item->prepay_percent > 0)
			{
				
			}else
			{
				//do notify for the order that not to prepay
				EnmasseHelper::doNotify($order->id);

				$link = JRoute::_("index.php?option=com_enmasse&controller=deal&task=listing");
				$msg = JText::_("PARTIAL_PAYMENT_NO_PREPAY_CHECKOUT_MSG");
				JFactory::getApplication()->redirect($link, $msg);
			}
				*/
				
		}
		else
		{
			//deal is free
			$link = JRoute::_("index.php?option=com_enmasse&controller=payment&task=doNotify&orderId=$order->id", false);
                        
		}
          //send notify email for customer after customer make order
               //EnmasseHelper::sendOrderEmail($order->id);
                $orderSend = JModelLegacy::getInstance('order', 'enmasseModel')->getById($order->id);
               //  $this->sendOrderEmailTo($orderSend);
               //  var_dump($order);
               // die;
		JFactory::getApplication()->redirect($link);
			
	}
     function sendOrderEmailTo($order){

            $app = JFactory::getApplication();
            $mailer = JFactory::getMailer();
            $config = JFactory::getConfig();
            $subject = '心程旅遊團購換領劵';

            $sender = array(
                $config->get('config.mailfrom'),
                '心程旅遊 sTravel'
            );

            $db = JFactory::getDBO();
            $query = "SELECT * FROM #__enmasse_merchant_branch WHERE ";
            $query .= "id=1";
            $db->setQuery($query);
            $item = $db->loadObject();            

            $branch ="";
            
            $branches  = json_decode($item->branches);
            foreach ($branches as $key => $value) {
                if($value->name == $order->branch){
                    $branch = $value;
                    break;
                }
            }
            
            $query2 ="SELECT * FROM #__enmasse_order_item WHERE order_id = ".$order->id;
            $db->setQuery($query2);
            $deal = $db->loadObject();

            $query3 ="SELECT * FROM #__enmasse_order_passenger WHERE order_id = ".$order->id;
            $db->setQuery($query3);
            $passengerInfo = $db->loadObject();
            // var_dump($query2);
            // var_dump($deal);
            // var_dump($branch);
            // var_dump($query3);
            // var_dump($passengerInfo);

            $body ="<h4>".$passengerInfo->name3." 您好，</h4>";
             $body .=   "<p>謝謝訂購我們的團購產品：".$deal->description."</p>";
             $body .=  "   <p>請打印附件的換領劵，並於辦公時間到下列的分店完成相關的旅遊套票手續。</p>";
             $body .=  "   <p>";
             $body .=  "   分店名稱： ".$branch->name." <br/>";
             $body .=  "       分店地址： ".$branch->address."<br/>";
             $body .=  "       辦公時間： ".$branch->description." <br/>";
            $body .=  "        聯絡電話： ".$branch->telephone."";

              $body .=  "  </p>";
              $body.='<p><a href="http://stravel.beabc.com/redemption?tmpl=component&ri='.$order->id.'">Redemption</a></p>';
              // $body.='<p>'.JRoute::_('index.php?itemId=194&tmpl=component&ri=46').'</p>';
               $body .=  " <p>            ";
               $body .=  "     祝   旅途愉快！<br/>";
              $body .=  "      心程旅遊 sTravel";
              $body .=  "  <p>";

            $body .=  "";
           
            $recipient = array($passengerInfo->email3); //$user->email;

            $mailer->setSubject($subject);
            $mailer->isHTML(true);
            $mailer->setBody($body);
            if (!empty($attachFileName)) {
                $mailer->addAttachment($attachFileName);
            }
            $mailer->addRecipient($recipient);
            $mailer->setSender($sender);
            $send = &$mailer->Send();

            if ($send !== true) {
                //$app->enqueueMessage('Error Sending email:<br/>'.$send->message,'error');
                return false;
            } else {
                return true;
            }
       
    }

	private function checkMaxCartItem($cart) {
		if(JFactory::getUser()->get('id'))
			{
				$arDealId =array();

				foreach ($cart->getAll() as $cartItem)
				{
					$arDealId[] = $cartItem->getItem()->id;
				}

				//get array total quantity of deals
				$arTotal = EnmasseHelper::getTotalBoughtQtyOfUser(JFactory::getUser()->get('id'), $arDealId);

			}
		foreach($cart->getAll() as $cartItem)
		{
			$item = $cartItem->getItem();
			$max_buy_qty = $item->max_buy_qty;
			$currentBuyQty = $cartItem->getCount();
			$boughtQty = empty($arTotal)? 0 : $arTotal[$item->id]->total;
			// to check total bought if it is over allowed qty.
			if($max_buy_qty >=0 && ($boughtQty+$currentBuyQty) > $max_buy_qty)
			{
				$msg = JText::_("QUANTITY_GREATER_THAN_MAX");
				JFactory::getApplication()->redirect(JRoute::_("index.php?option=com_enmasse&controller=deal&task=listing"), $msg, 'error');
			}
			//----------------------------
			// to check if this deal is set with limited coupon to solve out
			if($item->max_coupon_qty > 0)
			{
				// get the coupon which is free from inventory
				$freeCouponArr = JModelLegacy::getInstance('invty','enmasseModel')->getCouponFreeByPdtID($item->id);
				//check if the free coupon enough for this order
				if($currentBuyQty > count($freeCouponArr))
				{
					$msg = JText::_("LIMIT_COUPON_QTY").' '.count($freeCouponArr);
					JFactory::getApplication()->redirect(JRoute::_("index.php?option=com_enmasse&controller=shopping&task=reCheckout"), $msg, 'error');
				}

			}
		}
	}
}



?>