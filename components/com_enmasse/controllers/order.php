<?php

/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

class EnmasseControllerOrder extends JControllerLegacy {

    function popupPaymentSubmit() {
        JRequest::setVar('view', 'orderlist');
        parent::display();
    }
    
    function submitPayment(){
        $views = JRequest::getVar('views');
        $oid = JRequest::getVar('oid');
        $status = JRequest::getVar('status');
        $oOrder = JModelLegacy::getInstance('order','enmasseModel');
		$pay_detail = json_encode(JRequest::getVar('pay_attr'));
		$oOrder->updateSubmitPayment($oid,$pay_detail);
        if(isset($views)&& $views !=""&&$status=="Pending"){
            EnmasseHelper::doNotify($oid); 	
            $msg = JTEXT::_('SAVE_SUCCESS_MSG_AND_SEND_RECEIPT');
        }        
        echo '<script type="text/javascript">
            window.parent.location.reload();                                
            window.parent.SqueezeBox.close();
           </script>';
        exit();
    }

}