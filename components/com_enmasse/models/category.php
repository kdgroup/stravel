<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.model' );

class EnmasseModelCategory extends JModelLegacy
{
	function listAll()
	{
		$db = JFactory::getDBO();
		$query = "SELECT * FROM #__enmasse_category";
		$db->setQuery( $query );
		$rows = $db->loadObjectList();

		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return $rows;
	}
	
	
	function listAllPublished()
	{
		$db 	= JFactory::getDBO();
		$query = " SELECT 
		id, name, parent_id
		FROM 
		              #__enmasse_category 
		WHERE 
		published = 1 AND id in (select category_id from #__enmasse_deal_category 
			where deal_id in (
				Select id from #__enmasse_deal 
					where status NOT LIKE 'Pending' AND
		                          published = '1' AND
		                        start_at <='".DatetimeWrapper::getDatetimeOfNow()."' 
		                          AND end_at >= '".DatetimeWrapper::getDatetimeOfNow()."' 
					
				)
			group by category_id

			)
		ORDER BY
		name ASC";
		$db->setQuery($query);
		$rs = $db->loadObjectList();
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		
		 //sort category 
		$category = array();
		for($i = 0; $i < count($rs); $i++){
			if($rs[$i]->parent_id == ''){
				$category[] = $rs[$i];
				for($j = 0; $j < count($rs); $j++){
					if($rs[$j]->parent_id == $rs[$i]->id){
						$category[] = $rs[$j];
					}
				}
			}
		}

		return $category;
	}

	function listAllParentPublished()
	{
		$db 	= JFactory::getDBO();
		$query = ' SELECT 
		              * 
		FROM 
		              #__enmasse_category 
		WHERE 
		published = 1 AND parent_id = '.$db->quote('').'
		ORDER BY
		name ASC';
		$db->setQuery($query);
		$category = $db->loadObjectList();
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return $category;
	}

	function listAllChildrenPublished($parent_id)
	{
		$db 	= JFactory::getDBO();
		$query = ' SELECT 
		              *
		FROM 
		              #__enmasse_category 
		WHERE 
		published = 1 AND parent_id = '.$db->quote($parent_id).'
		ORDER BY
		name ASC';
		$db->setQuery($query);
		$category = $db->loadObjectList();
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return $category;
	}

	function getById($id)
	{
		$row = JTable::getInstance('category', 'Table');
		$row->load($id);
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return $row;
	}

	function getByDealID($dealid)
	{
//                $dealid = intval($dealid);
		$dealid = 2;
		$db 	= JFactory::getDBO();
		$query = " SELECT 
		category_id
		FROM 
		              #__enmasse_deal_category 
		WHERE 
		deal_id=' {$dealid}'
		";
		$db->setQuery($query);
		$category = $db->loadAssocList(null,'category_id');

		$strC = implode(';', $category);
		$strC.=';';


		if ($db->getErrorNum()) {
			JError::raiseError( 500, $db->stderr() );
			return false;
		}
		return $strC;
	}
	
	function retrieveName($id)
	{
		$db 	= JFactory::getDBO();
		$query = ' SELECT name as text FROM #__enmasse_category WHERE id = '.$id;
		$db->setQuery($query);
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		
		return $db->loadResult();
	}
	function getCategoryListInArrId($idArr)
	{
		$db 	= JFactory::getDBO();
		$query = " SELECT 
		id, name 
		FROM 
		              #__enmasse_category 
		WHERE 
		id IN (".implode(",", $idArr).")
		ORDER BY
		name ASC";
		$db->setQuery($query);
		$category = $db->loadObjectList();
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return $category;
	}

	function getBackGroundImage($cat_id){
		$cat =  $this->getById($cat_id);
		return $cat->background_image;
	}
	function listCitiesAllPublished()
	{
		$db 	= JFactory::getDBO();
		$query = " SELECT 
		id, name
		FROM 
		              #__enmasse_city 
		WHERE 
		published = 1 and hot = 1 and id in (Select city_id from #__enmasse_deal 
			where status NOT LIKE 'Pending' AND
                          published = '1' AND
                        start_at <='".DatetimeWrapper::getDatetimeOfNow()."' 
                          AND end_at >= '".DatetimeWrapper::getDatetimeOfNow()."' 
			)
		ORDER BY
		name ASC";
		$db->setQuery($query);
		$rs = $db->loadObjectList();
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		
		 //sort category 
		$cities = array();
		for($i = 0; $i < count($rs); $i++){
			
				$cities[] = $rs[$i];
				
			
		}

		return $cities;
	}
}
?>