<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.model' );
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");

class EnmasseModelOrderItem extends JModelLegacy
{
	function getById($orderItemId)
	{
		$db = JFactory::getDBO();
		$query = "	SELECT
						* 
					FROM 
						#__enmasse_order_item 
					WHERE
	              		id = $orderItemId
	              ";
		$db->setQuery( $query );
		$orderItem = $db->loadObject();
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		
		return $orderItem;
	}
	
	function listByPdtIdAndStatus($pdtId, $status=null)
	{
		$db = JFactory::getDBO();
		$query = "	SELECT
						* 
					FROM 
						#__enmasse_order_item 
					WHERE";
		if($status!=null)						
			$query .= "	status = '".$status."' AND";
		$query .= "		pdt_id = $pdtId";
		$db->setQuery( $query );
		$orderItemList = $db->loadObjectList();
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return $orderItemList;
	}
	function listByPdtIdAndStatusBranch($pdtId, $status=null,$branch=null)
	{
		$db = JFactory::getDBO();
		$query = "	SELECT
						a.*
					FROM 
						#__enmasse_order_item a left join #__enmasse_order b on a.order_id = b.id
					WHERE";
		if($status!=null)						
			$query .= "	(a.status = '".$status."') AND";

		if($branch!=null){
			$query .= "	b.branch = '".$branch."' AND";
		}
		$query .= "		a.pdt_id = $pdtId";
		// print_r($query);die;
		$db->setQuery( $query );
		$orderItemList = $db->loadObjectList();
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return $orderItemList;
	}
	function listByOrderId($orderId)
	{
		$db = JFactory::getDBO();
		$query = "	SELECT
						* 
					FROM 
						#__enmasse_order_item 
					WHERE
	              		order_id = $orderId
	              ";
		$db->setQuery( $query );
		$orderItemList = $db->loadObjectList();
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		
		return $orderItemList;
	}
	
	function updateStatus($id,$value)
	{
		$db = JFactory::getDBO();
		$query = 'UPDATE #__enmasse_order_item SET status ="'.$value.'", updated_at = "'. DatetimeWrapper::getDatetimeOfNow() .'" where id ='.$id;
		$db->setQuery( $query );
		$db-> query();
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return true;
	}
	function updateIsDelivered($id,$value)
	{
		$db = JFactory::getDBO();
		$query = '	UPDATE 
						#__enmasse_order_item 
					SET 
						is_delivered ='.$value.'  
					WHERE 
						id ='.$id;
		$db->setQuery( $query );
		$db-> query();
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return true;
	}
	        function listByPdtIdAndStatusAndDate($pdtId, $status=null,$from_at,$from_to)
	{
		$db = JFactory::getDBO();
		$query = "	SELECT
						* 
					FROM 
						#__enmasse_order_item 
					WHERE";
		if($status!=null)						
			$query .= "	status = '".$status."' AND";
		$query .= "		pdt_id = $pdtId";
                if($from_at != ""){
                    $query .= " AND created_at >= '".$from_at."'"	;
                }
                if($from_to != ""){
                    $query .= " AND created_at <= '".$from_to."'"	;
                }
		$db->setQuery( $query );

		$orderItemList = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return $orderItemList;
	}
	
	function listByPdtIdAndStatusAndDateCustom($pdtId, $status=null,$from_at,$from_to,$orderby,$branch="",$type="",$coupon)
	{
		$db = JFactory::getDBO();
		$query = "	SELECT a.*,a.status, a.total_price,b.status as statusIn,b.deallocated_at,b.name as nameIn,c.branch,d.deal_code,d.deposit,d.name,c.buyer_detail,c.total_buyer_paid
					FROM #__enmasse_order_item a 
					inner join #__enmasse_invty b on a.id = b.order_item_id 
					left join #__enmasse_order c  on a.order_id = c.id
					left join #__enmasse_deal d on a.pdt_id = d.id
					WHERE ";
		
		if($type!='Refunded'&&$type!='Unused'){
			if($status!=null)						
			$query .= "	(a.status = '".$status."')";

			if($type){
	         	$query .= "	AND b.status = '".$type."' ";
	         }
		}else{
			if($type=='Refunded'){
				$query .= "	(a.status = '".$type."')";
			}else{
				$query .= "	(a.unused_date <> '0000-00-00 00:00:00' and a.status <> 'Refunded')";
			}
		}
		//$query .= "		pdt_id = $pdtId";
                if($from_at != ""){
                    $query .= " AND a.created_at >= '".$from_at."' "	;
                }
                if($from_to != ""){
                    $query .= " AND a.created_at <= '".$from_to."' "	;
                }
          if($pdtId!='all'){
         	$query .= "	AND a.pdt_id = ".$pdtId." ";
         }
          if($branch){
         	$query .= "	AND c.branch = '".$branch."' ";
         }
          if($coupon){
         	$query .= "	AND b.name = '".$coupon."' ";
         }
         
         if($orderby){
         	$query .= " ".$orderby." "	;
         }
         
		$db->setQuery( $query );
		 $this->_total = $this->_getListCount($query); 
		$orderItemList = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		// $this->_total = $this->_getListCount($query); var_dump($this->_total);die;
		$data['orderItemList'] = $orderItemList;
		$data['_total'] = $this->_total;
		return $data;
	}
	function updateReason($id,$reason,$todo)
	{
		$db = JFactory::getDBO();
		$query = 'UPDATE #__enmasse_order_item SET 
					updated_at = "'. DatetimeWrapper::getDatetimeOfNow() .'", 
					'.$todo.'_modified = "'. DatetimeWrapper::getDatetimeOfNow() .'" ,
					'.$todo.'_reason = "'. $reason .'" 

					where id ='.$id;
		$db->setQuery( $query );
		$db-> query();
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return true;
	}
	function addNewReason($id,$reason,$todo)
	{
		$db = JFactory::getDBO();
		$query = 'UPDATE #__enmasse_order_item SET 
					
					'.$todo.'_date = "'. DatetimeWrapper::getDatetimeOfNow() .'" ,
					'.$todo.'_modified = "'. DatetimeWrapper::getDatetimeOfNow() .'" ,
					'.$todo.'_reason = "'. $reason .'" 

					where id ='.$id;
		$db->setQuery( $query );
		$db-> query();
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return true;
	}
	
}
?>