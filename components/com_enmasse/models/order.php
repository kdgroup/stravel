<?php
/*------------------------------------------------------------------------
# En Masse - Social Buying Extension 2010
# ------------------------------------------------------------------------
# By Matamko.com
# Copyright (C) 2010 Matamko.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.matamko.com
# Technical Support:  Visit our forum at www.matamko.com
-------------------------------------------------------------------------*/

// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.model' );
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");

class EnmasseModelOrder extends JModelLegacy
{
	
	//Order id
	function getById($orderId)
	{
		$db = JFactory::getDBO();
	    $query = "	SELECT 
	    				* 
	    			FROM 
	    				#__enmasse_order 
	    			WHERE
	              		id = $orderId";
	    $db->setQuery( $query );
	    $order = $db->loadObject();

		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
	    
		return $order;
	}
	//user id
	
	function listForBuyer($buyerId)
	{
		$db = JFactory::getDBO();
	    $query = "	SELECT 
	    				* 
	    			FROM 
	    				#__enmasse_order 
	    			WHERE
	    				status !='Unpaid' AND
	              		buyer_id = $buyerId
	              	ORDER BY
	              		created_at DESC";
	    $db->setQuery( $query );
	    $orderList = $db->loadObjectList();
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		
		return $orderList;
	}
        
        function listForBuyerByPaid($buyerId)
	{
		$db = JFactory::getDBO();
	    $query = "	SELECT 
	    				* 
	    			FROM 
	    				#__enmasse_order
	    			WHERE
	    				status IN('Paid','Delivered') and buyer_id = $buyerId
	              	ORDER BY
	              		created_at DESC";
	    $db->setQuery( $query );
	    $orderList = $db->loadObjectList();
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		
		return $orderList;
	}
         function listForBuyerByNotPaid($buyerId)
	{
		$db = JFactory::getDBO();
	    $query = "	SELECT 
	    				* 
	    			FROM 
	    				#__enmasse_order
	    			WHERE
	    				status NOT IN('Paid','Delivered') and buyer_id = $buyerId
	              	ORDER BY
	              		created_at DESC";
	    $db->setQuery( $query );
	    $orderList = $db->loadObjectList();
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		
		return $orderList;
	}
	
	function updateStatus($id, $value)
	{
		$db = JFactory::getDBO();
		$query = 'UPDATE #__enmasse_order SET status ="'.$value.'", updated_at = "'. DatetimeWrapper::getDatetimeOfNow() .'" where id ='.$id;
		$db->setQuery( $query );
		$db-> query();
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		
//Increase qty sold if order status changed to "Paid" 
		if($value == "Paid" or $value=='Full Paid')
		{
			$orderItemList = JModelLegacy::getInstance('orderitem', 'enmasseModel')->listByOrderId($id);
			foreach($orderItemList as $orderItem)
			{
				JModelLegacy::getInstance('deal', 'enmasseModel')->addQtySold($orderItem->pdt_id, $orderItem->qty);
			}
		}		return true;
	}
	
	function updatePayDetail($id, $payDetail)
	{
		$db = JFactory::getDBO();
		$query = "UPDATE #__enmasse_order SET pay_detail ='".$payDetail."', updated_at = '". DatetimeWrapper::getDatetimeOfNow() ."' where id =".$id;
		$db->setQuery( $query );
		$db->query();
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return true;
	}
    function listByBuyerId($buyerId)
	{
		$db = JFactory::getDBO();
	    $query = "	SELECT 
	    				* 
	    			FROM 
	    				#__enmasse_order 
	    			WHERE
	              		buyer_id =$buyerId AND status IN ('Paid', 'Delivered')";
	    $db->setQuery( $query );
	    $orderList = $db->loadObjectList();
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		
		return $orderList;
	}
	
	public function getOrdersByIds($arIds)
	{
		$db = JFactory::getDBO();
	    $query = "	SELECT 
	    				* 
	    			FROM 
	    				#__enmasse_order 
	    			WHERE
	              		id IN (" .implode(', ', $arIds) . ")"
	    				." AND status NOT IN ('".
	    				EnmasseHelper::$ORDER_STATUS_LIST["Delivered"]."','".
	    				EnmasseHelper::$ORDER_STATUS_LIST["Waiting_For_Refund"]."','".
	    				EnmasseHelper::$ORDER_STATUS_LIST["Refunded"]."'".
	    				")";
	    $db->setQuery( $query );
	    $orderList = $db->loadObjectList();
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		
		return $orderList;
	}
	
	public function getOrderByOrderItemId($oiId)
	{
		$db = JFactory::getDBO();
		$query = "SELECT o.*, oi.description as deal_name
					FROM #__enmasse_order o
					LEFT JOIN #__enmasse_order_item oi ON o.id = oi.order_id
					WHERE oi.id = $oiId";
		
		$db->setQuery( $query );
		return $db->loadObject();
	}
	
	// Web service function
	
	public function ws_getOrderHistory($buyerId)
	{
		$db = JFactory::getDBO();
		$query = "SELECT o.*, oi.description as deal_name
					FROM #__enmasse_order o
					LEFT JOIN #__enmasse_order_item oi ON o.id = oi.order_id
					WHERE o.buyer_id = $buyerId";
		
		$db->setQuery($query);
	    $orderList = $db->loadObjectList();
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		
		return $orderList;
	}
	
	public function getCountOrder() {
            $db = JFactory::getDbo();
            $query = "SELECT count(*) FROM #__enmasse_order AS o";
            $db->setQuery($query);
            $count = $db->loadResult();
            return $count;
        }
        
        function archiveOrderHaveDealExprie(){
            $db = JFactory::getDBO();
            $query = $db->getQuery(true);
            $query->update('#__enmasse_order o,#__enmasse_order_item AS oi,#__enmasse_deal d');
            $query->set("o.archived = 1");
            $query->where(' o.id = oi.order_id and d.id = oi.pdt_id' );
            $query->where('d.end_at < "'.DatetimeWrapper::getDatetimeOfNow().'"');
            $query->where("o.status IN ('Pending','Unpaid')");
            $db->setQuery($query);
            $result =$db->query();
            if ($this->_db->getErrorNum()) {
                JError::raiseError(500, $this->_db->stderr());
                return false;
            }
            return $result;
        }
        
        function archiveOrderPaidOlder30(){
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->update("#__enmasse_order o");
            $query->set("o.archived = 1");
            $query->where("o.status IN ('Paid','Delivered')");
            $query->where( "o.updated_at <= DATE(DATE_SUB(NOW(),INTERVAL 30 DAY))");
            $db->setQuery($query);
            if ($this->_db->getErrorNum()) {
                JError::raiseError(500, $this->_db->stderr());
                return false;
            }
            return $db->query();
        }
        
        function updateSubmitPayment($id,$pay_detail){
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->update("#__enmasse_order o");
            $query->set("o.payment_status = 1");
            $query->set("o.pay_detail = '".$db->escape($pay_detail)."'");
            $query->where("o.id = ".$id);
            $db->setQuery($query);
            if ($this->_db->getErrorNum()) {
                JError::raiseError(500, $this->_db->stderr());
                return false;
            }
            return $db->query();
        }
		
		function getPayDetailById($payGtyId){
			$db = JFactory::getDBO();
            $query = $db->getQuery(true);
			$query->select("pay_detail_fields");
			$query->from("#__enmasse_pay_gty");
			$query->where("id=".(int) $payGtyId);
			$db->setQuery($query);
			return $db->loadResult();
		}
        
            function getPaymentMethod($id)
            {
                $db = JFactory::getDBO();
                $query = $db->getQuery(true);
                
                $query->select("*");
                $query->from("#__enmasse_pay_gty");
                $query->where("id=".(int) $id);
                
                $db->setQuery($query);
                if ($this->_db->getErrorNum()) {
                    JError::raiseError(500, $this->_db->stderr());
                    return false;
                }
                return $db->loadObject();
            }
            
            function gerOrderbyProcessingCenter($userid){
                $db = JFactory::getDbo();
                $query = $db->getQuery(TRUE);
                $query->select("o.*");
                $query->from("#__enmasse_order o,#__enmasse_processing p,#__enmasse_processing_center pc");
                $query->where("o.id = p.order_id and pc.id = p.processing_center_id and pc.user_in_charge ='".$userid."'");
                $db->setQuery( $query );
                $orderList = $db->loadObjectList();
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return $orderList;
            }
            
            public function checkOrderUser($order_id, $user_id = ''){
            if(!$user_id || !$order_id){
                return false;
            }
            
            $oDb = JFactory::getDbo();
            $oQuery = $oDb->getQuery(true);

            $oQuery->select('*');
            $oQuery->from('#__enmasse_order');
            $oQuery->where("id = ".$oDb->quote($order_id));
            $oQuery->where("buyer_id = ".$oDb->quote($user_id));
            
            $oDb->setQuery($oQuery);
            $result = $oDb->loadObject();
            
            if($result)
                return true;
            else 
                return false;
        }
        public function storeOrderPassengers($data){
        	$birthday = str_replace('/', '-', $data->birthday);
        	$birthday2 = str_replace('/', '-', $data->birthday2);
        	 var_dump($birthday);
        	 var_dump($birthday2);
        	$data->birthday = date('Y:m:d H:i:s',strtotime($birthday));
        	$data->birthday2 = date('Y:m:d H:i:s',strtotime($birthday2));
        	$data->created = DatetimeWrapper::getDatetimeOfNow();
           
			 $result = JFactory::getDbo()->insertObject('#__enmasse_order_passenger', $data);
			 $id = JFactory::getDbo()->insertid();
			 
			 return $id;

			// Set the query using our newly populated query object and execute it.
			

        }
        function getSetting(){
			$db = JFactory::getDBO();
            $query = $db->getQuery(true);
			$query->select("*");
			$query->from("#__enmasse_setting");
			
			$db->setQuery($query);
			return $db->loadResult();
		}
}
?>