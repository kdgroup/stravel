<?php
/*------------------------------------------------------------------------
# En Masse - Social Buying Extension 2010
# ------------------------------------------------------------------------
# By Matamko.com
# Copyright (C) 2010 Matamko.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.matamko.com
# Technical Support:  Visit our forum at www.matamko.com
-------------------------------------------------------------------------*/

 
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
  if(!defined('DS')){
define('DS',DIRECTORY_SEPARATOR);
}
// Require the base controller
 JHtml::_('behavior.framework');
JHtml::_('bootstrap.framework');
require_once( JPATH_COMPONENT.DS.'controller.php' );

//require enmasse datetime helper
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."DatetimeWrapper.class.php");

 
// Require specific controller if requested
if($controller = JRequest::getWord('controller')) {
    $path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
    if (file_exists($path)) {
        require_once $path;
    } else {
        $controller = '';
    }
}

// Create the controller
$classname    = 'EnmasseController'.ucFirst($controller);
$controller   = new $classname( );

//load language pack
$language = JFactory::getLanguage();
$base_dir = JPATH_SITE.DS.'components'.DS.'com_enmasse';
$version = new JVersion;
$joomla = $version->getShortVersion();
if(substr($joomla,0,3) >= 1.6){
    $extension = 'com_enmasse16';
}else{
    $extension = 'com_enmasse';
}
if($language->load($extension, $base_dir, $language->getTag(), true) == false)
{
	 $language->load($extension, $base_dir, 'en-GB', true);
}
// Set default time zone for fix PHP notification
DatetimeWrapper::setTimezone(DatetimeWrapper::getTimezone());

// Perform the Request task
$controller->execute( JRequest::getWord( 'task' ) );

// Redirect if set by the controller
$controller->redirect();

// add theme
$theme =  EnmasseHelper::getThemeFromSetting();
JFactory::getDocument()->addStyleSheet('components/com_enmasse/theme/' . $theme . '/css/screen.css');
// add js of theme

if(JRequest::getVar('view') != 'subscriptpage' && JRequest::getVar('view') != 'uploader'){
	JFactory::getDocument()->addScript('components/com_enmasse/theme/' . $theme . '/js/jquery-1.8.3.min.js');
}
JFactory::getDocument()->addScript('components/com_enmasse/theme/' . $theme . '/js/html5.js');
JFactory::getDocument()->addScript('components/com_enmasse/theme/' . $theme . '/js/jquery.nivo.slider.js');
JFactory::getDocument()->addScript('components/com_enmasse/theme/' . $theme . '/js/functions.js');
JFactory::getDocument()->addScript('components/com_enmasse/theme/' . $theme . '/js/start.js');

?>