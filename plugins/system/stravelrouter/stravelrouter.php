<?php
// no direct access
defined('_JEXEC') or die;

define('ROUTER_MODE_SKIP_SEF', 2);
define('ROUTER_MODE_SKIP_RAW', -1);
 
class plgSystemsTravelRouter extends JPlugin
{
        /**
         * @return  void
         */
        public function onAfterInitialise()
        {
                $app = JFactory::getApplication();
                if($app->isAdmin()) {
                    return;
                }
                //Redirect if it is old aspx request
                $juri = $_SERVER['REQUEST_URI'];
                if(preg_match('/(\.(aspx|ASPX))|(\/(packageflyer|cruiseflyer)\/(.*)\.pdf)/',$juri)) {
                    $app->redirect(JFactory::getURI()->root(),'','warning',true);
                }
                // Execute the plugin only if SEF is activated
                if(JFactory::getConfig()->get('sef') == 1)
                {
                    // Only manipulate HTML document requests to avoid errors in other document types such as JSON or XML
                    if(JFactory::getDocument() instanceof JDocumentHTML)
                    {
                        $query_parameters = JFactory::getURI()->getQuery(true);
                        if(!empty($query_parameters['option']))
                        {
                            //Only work with stravel component.
                            if($query_parameters['option']==='com_stravel') 
                            {
                                if(!empty($query_parameters['view'])) {
                                    $view = $query_parameters['view'];
                                    $url_ext = '';
                                    switch($view) {
                                        case 'packagehome':
                                            if(!empty($query_parameters['city'])) {
                                                $url_ext .= '&city='.$query_parameters['city'];
                                                if(!empty($query_parameters['cityname'])) {
                                                    $url_ext .= '&cityname='.$query_parameters['cityname'];    
                                                }
                                            }
                                            if(!empty($query_parameters['ideaid'])) {
                                                $url_ext .= '&ideaid='.$query_parameters['ideaid'];
                                                if(!empty($query_parameters['ideaname'])) {
                                                    $url_ext .= '&ideaname='.$query_parameters['ideaname'];    
                                                }
                                            }
                                            if(!empty($query_parameters['departureDate'])) {
                                                $url_ext .= '&departureDate='.$query_parameters['departureDate'];
                                            }
                                            if(!empty($query_parameters['arrivedDate'])) {
                                                $url_ext .= '&arrivedDate='.$query_parameters['arrivedDate'];
                                            }
                                            $url = JRoute::_('index.php?option=com_stravel&view=packagehome'.$url_ext);
                                            break;
                                        case 'tourhome':
                                            if(!empty($query_parameters['city'])) {
                                                $url_ext .= '&city='.$query_parameters['city'];
                                                if(!empty($query_parameters['cityname'])) {
                                                    $url_ext .= '&cityname='.$query_parameters['cityname'];    
                                                }
                                            }
                                            if(!empty($query_parameters['ideaid'])) {
                                                $url_ext .= '&ideaid='.$query_parameters['ideaid'];
                                                if(!empty($query_parameters['ideaname'])) {
                                                    $url_ext .= '&ideaname='.$query_parameters['ideaname'];    
                                                }
                                            }
                                            if(!empty($query_parameters['departureDate'])) {
                                                $url_ext .= '&departureDate='.$query_parameters['departureDate'];
                                            }
                                            if(!empty($query_parameters['arrivedDate'])) {
                                                $url_ext .= '&arrivedDate='.$query_parameters['arrivedDate'];
                                            }
                                            $url = JRoute::_('index.php?option=com_stravel&view=tourhome'.$url_ext);
                                            break;
                                        case 'packageflyer':
                                            if(!empty($query_parameters['id'])) {
                                                $url_ext .= '&id='.$query_parameters['id'];
                                                if(!empty($query_parameters['name'])) {
                                                    $url_ext .= '&name='.$query_parameters['name'];    
                                                }
                                            }
                                            $url = JRoute::_('index.php?option=com_stravel&view=packageflyer'.$url_ext);
                                            break;
                                        case 'tourflyer':
                                            if(!empty($query_parameters['id'])) {
                                                $url_ext .= '&id='.$query_parameters['id'];
                                                if(!empty($query_parameters['name'])) {
                                                    $url_ext .= '&name='.$query_parameters['name'];    
                                                }
                                            }
                                            $url = JRoute::_('index.php?option=com_stravel&view=tourflyer'.$url_ext);
                                            break;
                                        case 'airtickethome':
                                            $url = JRoute::_('index.php?option=com_stravel&view=airtickethome');
                                            break;
                                        case 'airsearchresult':
                                            if(!empty($query_parameters['citycode'])) {
                                                $url_ext .= '&citycode='.$query_parameters['citycode'];
                                                if(!empty($query_parameters['cityname'])) {
                                                    $url_ext .= '&cityname='.$query_parameters['cityname'];    
                                                }
                                            }
                                            if(!empty($query_parameters['departureDate'])) {
                                                $url_ext .= '&departureDate='.$query_parameters['departureDate'];
                                            }
                                            if(!empty($query_parameters['arrivedDate'])) {
                                                $url_ext .= '&arrivedDate='.$query_parameters['arrivedDate'];
                                            }
                                            if(!empty($query_parameters['type'])) {
                                                $url_ext .= '&type='.$query_parameters['type'];
                                            }
                                            if(!empty($query_parameters['triptype'])) {
                                                $url_ext .= '&triptype='.$query_parameters['triptype'];
                                            }
                                            $url = JRoute::_('index.php?option=com_stravel&view=airsearchresult'.$url_ext);
                                            break;
                                        case 'hotelhome':
                                            $url = JRoute::_('index.php?option=com_stravel&view=hotelhome');
                                            break;
                                        case 'hotelresult':
                                            if(!empty($query_parameters['city'])) {
                                                $url_ext .= '&city='.$query_parameters['city'];
                                                if(!empty($query_parameters['destination'])) {
                                                    $url_ext .= '&destination='.$query_parameters['destination'];    
                                                }
                                            }
                                            if(!empty($query_parameters['checkinDate'])) {
                                                $url_ext .= '&checkinDate='.$query_parameters['checkinDate'];
                                            }
                                            if(!empty($query_parameters['nights'])) {
                                                $url_ext .= '&nights='.$query_parameters['nights'];
                                            }
                                            $url = JRoute::_('index.php?option=com_stravel&view=hotelresult'.$url_ext);
                                            break;
                                        case 'cruisehome':
                                            if(!empty($query_parameters['cruisecpn'])) {
                                                $url_ext .= '&cruisecpn='.$query_parameters['cruisecpn'];
                                                if(!empty($query_parameters['cruisecpnname'])) {
                                                    $url_ext .= '&cruisecpnname='.$query_parameters['cruisecpnname'];    
                                                }
                                            }
                                            if(!empty($query_parameters['cruisedst'])) {
                                                $url_ext .= '&cruisedst='.$query_parameters['cruisedst'];
                                                if(!empty($query_parameters['cruisedstname'])) {
                                                    $url_ext .= '&cruisedstname='.$query_parameters['cruisedstname'];    
                                                }
                                            }
                                            if(!empty($query_parameters['cruisevessel'])) {
                                                $url_ext .= '&cruisevessel='.$query_parameters['cruisevessel'];
                                                if(!empty($query_parameters['cruisevesselname'])) {
                                                    $url_ext .= '&cruisevesselname='.$query_parameters['cruisevesselname'];    
                                                }
                                            }
                                            if(!empty($query_parameters['year'])) {
                                                $url_ext .= '&year='.$query_parameters['year'];
                                            }
                                            if(!empty($query_parameters['month'])) {
                                                $url_ext .= '&month='.$query_parameters['month'];
                                            }
                                            if(!empty($query_parameters['numday'])) {
                                                $url_ext .= '&numday='.$query_parameters['numday'];
                                            }
                                            $url = JRoute::_('index.php?option=com_stravel&view=cruisehome'.$url_ext);
                                            break;
                                        case 'cruiseflyer':
                                            if(!empty($query_parameters['id'])) {
                                                $url_ext .= '&id='.$query_parameters['id'];
                                                if(!empty($query_parameters['name'])) {
                                                    $url_ext .= '&name='.$query_parameters['name'];    
                                                }
                                            }
                                            $url = JRoute::_('index.php?option=com_stravel&view=cruiseflyer'.$url_ext);
                                            break;
                                        case 'default':
                                            $url = null;
                                            break;
                                        case 'general':
                                            $url = null;
                                            break;
                                        default:
                                            //$url = JFactory::getURI()->root();
                                            break;
                                    }
                                    if(!empty($url)) {
                                        $app->redirect($url);    
                                    }
                                }       
                            }
                        }
                    }    
                }
        }
        
        /**
         *  onAfterRender event
         *  Try to get metadata from page to save to database
         *  
         */         
         public function onAfterRender(){
            $app = JFactory::getApplication();
            $db = JFactory::getDbo();
            $url = JFactory::getURI()->toString();
            $option = JRequest::getVar('option');
            $view = JRequest::getVar('view');
            if(preg_match('/(jpg|png|gif|jpeg|aspx|JPG|PNG|GIF|JPEG|ASPX)$/',$url) || !preg_match('/(www\.stravel\.com\.hk)/',$url) || $option!='com_stravel' || $view=='urlmanagement') {
                return;
            }            
            $url = $db->quote($db->escape($url),false);
            $existed = $db->setQuery('SELECT * FROM #__stravel_urls WHERE `sef_url` like '.$url)->loadObject();
            if($app->isAdmin() || count($existed)>0) {
                return;
            }
            
            
            $date = $db->quote(JFactory::getDate()->toSql(),false);
            $title = $db->quote($db->escape(JFactory::getDocument()->getTitle()),false);
            $page_params = JFactory::getApplication()->getParams();        
            $pagedesc = $db->quote($db->escape($page_params->get('menu-meta_description')),false);
            $pagekeywords = $db->quote($db->escape($page_params->get('menu-meta_keywords')),false);
            $parame = $db->quote($db->escape(json_encode( JRequest::get())),false);
            $query = "INSERT INTO #__stravel_urls values (null,{$url},{$parame},{$title},{$pagekeywords},{$pagedesc},1,{$date},{$date},'')";
            $db->setQuery($query)->execute();
         }       
}