﻿<?php
define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);
define('JPATH_BASE', dirname(dirname(__FILE__)));

require_once JPATH_BASE . '/includes/defines.php';
require_once JPATH_BASE . '/includes/framework.php';

try {
  $app = JFactory::getApplication('site');
  if (!is_object($app)) {
    throw new Exception('Application object was not created');
  }
  $db = JFactory::getDBO();
  if (!is_object($db)) {
    throw new Exception('Database object was not created');
  }
  $root = JPATH_BASE . DS . 'cache' . DS . 'page';
  $query = $db->getQuery(true);
  $query->delete($db->quoteName('#__jotcache'));
  $cursor = $db->setQuery($query)->execute();

  if ($handle = opendir($root)) {
    while (false !== ($file = readdir($handle))) {
		if ($file != "." && $file != "..") {
			$file2 = $root . DS . $file;
			@chmod($file2, 0777);
			@unlink($file2);
		}
    }
    closedir($handle);
  }
  exit("<h2>Clear Cache finished.</h2>");
} catch (Exception $e) {
  exit('[Cache clean] error : ' . $e->getMessage());
}
?>
