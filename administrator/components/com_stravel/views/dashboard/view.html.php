<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');

class sTravelViewdashboard extends JViewLegacy
{
	/**
	 * dashboard view display method
	 * @return void
	 */
	function display($tpl = null) 
	{

		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
 
		// Set the toolbar
		$this->addToolBar();
 
		// Display the template
		parent::display($tpl);

	}
 
	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() 
	{
		$canDo = sTravelHelper::getActions();
		JToolBarHelper::title(JText::_('sTravel API Integrate System'), 'user');
		if ($canDo->get('core.admin')) 
		{
			JToolBarHelper::divider();
			JToolBarHelper::preferences('com_stravel');
		}
	}
}
