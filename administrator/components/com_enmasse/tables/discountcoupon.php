<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class TableDiscountcoupon extends JTable
{
	var $id = null;
	var $name = null;
        var $voucer_code = null;
	var $type = null;
        var $discount_amount = null;
        var $use_per_coupon = null;
        var $use_per_user = null;
        var $user_id = null;
        var $status = null;
	var $start_at = null;
	var $end_at= null;
	var $updated_at = null;
        var $created_at = null;
	function __construct(&$db)
	{
		parent::__construct( '#__enmasse_voucher', 'id', $db );
	}

}
?>