<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class TableOrdervoucher extends JTable
{
	var $id = null;
	var $order_id = null;
        var $voucer_id = null;
        var $discount_amount = null;
        var $use_per_coupon = null;
        var $create_at = null;
	function __construct(&$db)
	{
		parent::__construct( '#__enmasse_order_voucher', 'id', $db );
	}

}
?>