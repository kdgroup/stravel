<?php

/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');
JTable::addIncludePath('components' . DS . 'com_enmasse' . DS . 'tables');
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "helpers" . DS . "EnmasseHelper.class.php");

class EnmasseControllerReport extends JControllerLegacy {

    function display($cachable = false, $urlparams = false) {
        JRequest::setVar('view', 'report');
        JRequest::setVar('layout', 'deal_coupon');
        parent::display();
    }

    function dealCouponList() {
        JRequest::setVar('view', 'report');
        JRequest::setVar('layout', 'deal_coupon');
        parent::display();
    }

    function control() {
        $this->setRedirect('index.php?option=com_enmasse');
    }

    function generateReport() {
        $dealId = JRequest::getVar('dealId');
        $from_at = "";
        $from_to = "";
        $type = "";
        $tmp = "";
        if (!empty($dealId)) {
            $orderItemList =array();
            if($dealId=='100009'){
                $dealList = JModelLegacy::getInstance('deal', 'enmasseModel')->listConfirmed();

                foreach ($dealList as $key => $value) {
                    $dealId = $value->id;
                    $deal = JModelLegacy::getInstance('deal', 'enmasseModel')->getById($dealId);
                    
                    
                    $from_at = JRequest::getVar('from_at');
                    $from_to = JRequest::getVar('from_to');
                    $type = JRequest::getVar('type');
                    $orderItemListall = JModelLegacy::getInstance('orderItem', 'enmasseModel')->listByPdtIdAndStatusAndDate($dealId, "Delivered",$from_at,$from_to);

                    for ($count = 0; $count < count($orderItemListall); $count++) {
                        $orderItemListall[$count]->invtyList = JModelLegacy::getInstance('invty', 'enmasseModel')->listByOrderItemIdAndStatus($orderItemListall[$count]->id,$type);
                        $orderItemListall[$count]->order = JModelLegacy::getInstance('order', 'enmasseModel')->getById($orderItemListall[$count]->order_id);
                        $orderI->invtyList = JModelLegacy::getInstance('invty', 'enmasseModel')->listByOrderItemIdAndStatus($orderItemListall[$count]->id,$type);
                        $orderI->order = JModelLegacy::getInstance('order', 'enmasseModel')->getById($orderItemListall[$count]->order_id);
                        $orderItemListall[$count]->dealcode = $deal->deal_code;
                        $orderItemListall[$count]->dealname = $deal->name;
                        $orderItemList[] = $orderItemListall[$count];
                    }
                }
            }else{
                $deal = JModelLegacy::getInstance('deal', 'enmasseModel')->getById($dealId);
                    
                    
                    $from_at = JRequest::getVar('from_at');
                    $from_to = JRequest::getVar('from_to');
                    $type = JRequest::getVar('type');
                    $orderItemList = JModelLegacy::getInstance('orderItem', 'enmasseModel')->listByPdtIdAndStatusAndDate($dealId, "Delivered",$from_at,$from_to);

                    for ($count = 0; $count < count($orderItemList); $count++) {
                        $orderItemList[$count]->invtyList = JModelLegacy::getInstance('invty', 'enmasseModel')->listByOrderItemIdAndStatus($orderItemList[$count]->id,$type);
                        $orderItemList[$count]->order = JModelLegacy::getInstance('order', 'enmasseModel')->getById($orderItemList[$count]->order_id);
                        $orderItemList[$count]->dealcode = $deal->deal_code;
                        $orderItemList[$count]->dealname = $deal->name;
                    }
            }
           // print_r('<pre>');
            //print_r($orderItemList);die;
            $count = 1;
            $id = 0; //id of the item in itemList array
            for ($i = 0; $i < count($orderItemList); $i++) {
                $orderItem = $orderItemList[$i];
                $buyerDetail = json_decode($orderItem->order->buyer_detail);
                $deliveryDetail = json_decode($orderItem->order->delivery_detail);

                for ($j = 0; $j < count($orderItem->invtyList); $j++) {
                    $invty = $orderItem->invtyList[$j];

                    $itemList[$id]['Serial No.'] = $count++ . "\t";
                    $itemList[$id]['Deal Code'] = $orderItem->dealcode. "\t";
                    $itemList[$id]['Deal Name'] = $orderItem->dealname . "\t";
                    $itemList[$id]['Purchase Date'] = DatetimeWrapper::getDisplayDatetime($orderItem->created_at) . "\t";
                    $itemList[$id]['Amount'] = $orderItem->total_price . "\t";
                    $itemList[$id]['Buyer Name'] = $buyerDetail->name . "\t";
                    $itemList[$id]['Buyer Email'] = $buyerDetail->email . "\t";
                    $itemList[$id]['Branch'] = $orderItem->order->branch . "\t";
                    $itemList[$id]['Delivery Name'] = $deliveryDetail->name . "\t";
                    $itemList[$id]['Delivery Email'] = $deliveryDetail->email . "\t";
                    $itemList[$id]['Order Comment'] = $orderItem->order->description . "\t";
                   
                    $itemList[$id]['Coupon Serial'] = $invty->name . "\t";
                    $itemList[$id]['Coupon Status'] = JTEXT::_('COUPON_' . strtoupper($invty->status)) . "</br>";
                    $id++;
                }
            }

            if ($tmp == "pdf") {
                $rs = '<table style="border:1px solid black;border-collapse: collapse; font-size:9px;">';
                $rs .= "
                         <tr><td colspan='9' style='border:1px dotted #D5D5D5;font-size:16px; color:#0000FF; text-align:center;'>" . JTEXT::_('REPORT_TITLE') . "</td> </tr>
                        <tr>
                           <th style='border:1px dotted #D5D5D5;' align='center' width='25'>" . JTEXT::_('REPORT_SERIAL') . "</th>
                            <th style='border:1px dotted #D5D5D5;' align='center' width='100'>" . JTEXT::_('Deal Code') . "</th>
                            <th style='border:1px dotted #D5D5D5;' align='center' width='100'>" . JTEXT::_('Deal Name') . "</th>
                            <th style='border:1px dotted #D5D5D5;' align='center' width='100'>" . JTEXT::_('REPORT_PURCHASE_DATE') . "</th>
                            <th style='border:1px dotted #D5D5D5;' align='center' width='100'>" . JTEXT::_('Amount') . "</th>
                             <th style='border:1px dotted #D5D5D5;' align='center' width='100'>" . JTEXT::_('REPORT_BUYER_NAME') . "</th>
                            <th style='border:1px dotted #D5D5D5;' align='center' width='100'>" . JTEXT::_('REPORT_BUYER_MAIL') . "</th>
                            <th style='border:1px dotted #D5D5D5;' align='center' width='100'>" . JTEXT::_('Branch') . "</th>
                            <th style='border:1px dotted #D5D5D5;' align='center' width='100'>" . JTEXT::_('REPORT_DELIVERY_NAME') . "</th>
                            <th style='border:1px dotted #D5D5D5;' align='center' width='100'>" . JTEXT::_('REPORT_DELIVERY_MAIL') . "</th>
                            <th style='border:1px dotted #D5D5D5;' align='center' width='70'>" . JTEXT::_('REPORT_ORDER_COMMENT') . "</th>
                            
                            <th style='border:1px dotted #D5D5D5;' align='center' width='50'>" . JTEXT::_('REPORT_COUPON_SERIAL') . "</th>
                            <th style='border:1px dotted #D5D5D5;' align='center' width='35'>" . JTEXT::_('REPORT_COUPON_STATUS') . "</th>
                        </tr>
                    ";
                for ($i = 0; $i < count($itemList); $i++) {
                    $itemOrder = $itemList[$i];

                    $rs .= "<tr>
               <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['Serial No.']) . "</td>" . "
             <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['Deal Code']) . " </td>" . "
             <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['Deal Name']) . " </td>" . "
             <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['Purchase Date']) . " </td>" . "
             <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['Amount']) . " </td>" . "
             <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['Buyer Name']) . " </td>" . "
             <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['Buyer Email']) . " </td>" . "
             <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['Branch']) . " </td>" . "
             <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['Delivery Name']) . "</td>" . "
             <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['Delivery Email']) . "</td>" . "
             <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['Order Comment']) . "</td>" . "
           
             <td style='border:1px dotted #D5D5D5;' align='center'>#" . replaceBr($itemOrder['Coupon Serial']) . "</td>" . "
             <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['Coupon Status']) . "</td>" . "
             </tr> ";
                }
                $rs .= '</table>';
                require_once(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_enmasse' . DS . 'helpers' . DS . 'html2pdf' . DS . 'html2pdf.class.php');
                $html2pdf = new HTML2PDF('P', 'A4', 'en');
                $html2pdf->setDefaultFont('Arial');
                $html2pdf->writeHTML($rs);
                $outFileName = 'report-' . DatetimeWrapper::getDateOfNow() . '.pdf';
                $html2pdf->Output($outFileName, 'I');
                die;
            } else {
                $filename = "Report" . date('Ymd') . ".xls";
                enmasseHelper::reportGenerator($itemList);
                header('Content-Encoding: UTF-8');
                header("Content-Disposition: attachment; filename=\"$filename\"");
                header("Content-Type: application/vnd.ms-excel;charset=UTF-8");
                header("Pragma: no-cache");
                header("Expires: 0");
                header("Lacation: excel.htm?id=yes");
                exit(0);
            }
        } else {
            $this->setRedirect(JURI::root() . 'administrator/index.php?option=com_enmasse&controller=report', JTEXT::_('REPORT_EMPTY_MSG'));
        }
    }

}

function replaceBr($strText) {
    $strText = str_replace('</br>', '', $strText);
    return $strText;
}

?>