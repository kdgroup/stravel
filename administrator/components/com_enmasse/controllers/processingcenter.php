<?php
/*------------------------------------------------------------------------
# En Masse - Social Buying Extension 2010
# ------------------------------------------------------------------------
# By Matamko.com
# Copyright (C) 2010 Matamko.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.matamko.com
# Technical Support:  Visit our forum at www.matamko.com
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.controller');
JTable::addIncludePath('components'.DS.'com_enmasse'.DS.'tables');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse".DS."helpers". DS ."EnmasseHelper.class.php");

class EnmasseControllerProcessingcenter extends JControllerLegacy
{

	function display($cachable = false, $urlparams = false)
	{
		JRequest::setVar('view', 'processingcenter');
		JRequest::setVar('layout', 'show');
		parent::display();
	}
	function edit()
	{
		JRequest::setVar('view', 'processingcenter');
		JRequest::setVar('layout', 'edit');
		parent::display();
	}
	function control()
	{
		$this->setRedirect('index.php?option=com_enmasse');
	}
	function add()
	{
		JRequest::setVar('view', 'processingcenter');
		JRequest::setVar('layout', 'edit');
		parent::display();
	}
	function save()
	{
		$data = JRequest::get( 'post' );

		$model = JModelLegacy::getInstance('processingcenter','enmasseModel');
		$err = $this->validateProcessingcenter($data);
		//------------------------
		//gemerate integration class
		 $integrateFileName = EnmasseHelper::getSubscriptionClassFromSetting().'.class.php';
		 $integrationClass = EnmasseHelper::getSubscriptionClassFromSetting();
		 require_once (JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."subscription". DS .$integrationClass. DS.$integrateFileName);
		 $integrationObject = new $integrationClass();
		if(! empty($err))
		{
			$msg = JText::_('SAVE_ERROR_MSG') .": " . $err['msg'];
			if($data['id'] == null)
				$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller').'&task=add', $msg, 'error');
			else
				$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller').'&task=edit&cid[0]='. $data['id'], $msg, 'error');
		}	
		else if ($row = $model->store($data))
		{
			$integrationObject->integration($row,'processingcenter');
			$msg = JText::_('SAVE_SUCCESS_MSG');
			$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller'), $msg);
		}
		else
		{
			$msg = JText::_('SAVE_ERROR_MSG') .": " . $model->getError();
			if($data['id'] == null)
				$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller').'&task=add', $msg, 'error');
			else
				$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller').'&task=edit&cid[0]='. $data['id'], $msg, 'error');
		}
	}

	function remove()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );

		$model = JModelLegacy::getInstance('processingcenter','enmasseModel');
		$msg = "";
                
		if(!$model->deleteList($cids))
		{ 
			$msg .= JText::_('DELETE_ERROR_MSG') .": " . $model->getError();
			$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller'), $msg, 'error');
		}
		JFactory::getApplication()->redirect('index.php?option=com_enmasse&controller=processingcenter', $msg , 'error');
	}



	function publish()
	{
		EnmasseHelper::changePublishState(1,'enmasse_processing_center','processingcenter','processingcenter');
	}
	function unpublish()
	{
		EnmasseHelper::changePublishState(0,'enmasse_processing_center','processingcenter','processingcenter');
	}
	function checkDuplicatedCenter($center_id)
	{
            $processingcenterName = addslashes(JRequest::getVar("name"));
            $processingcenterObj = JModelLegacy::getInstance('processingcenter','enmasseModel')->getCenterByName($processingcenterName,$center_id);
            if($processingcenterObj!=null)
	    	return true;
	    else
	    	return false;
	}
	
	function  validateProcessingcenter($data = array())
	{
		$msg = null;
		$error = array();
		if(isset($data['name']) && ( strlen($data['name']) < 1 || strlen($data['name'])> 50))
		{
			$msg = JText::_('There is error during the saving process: Name can be from 1 to 50 characters');
		}
                else if($this->checkDuplicatedCenter($data['id'])){
                    $msg = "Name Duplicate.";
                }
	
		if($msg != null)
		{
			$error['msg'] = $msg;
		}
		return $error;
	}
        
    function autoCompleteUserName(){
        $return_arr = array();
        $oModelPCenter= JModelLegacy::getInstance('processingcenter', 'enmasseModel');
        $listNameMerchant = $oModelPCenter->getAllUserName();
        
        echo json_encode($listNameMerchant);
        exit();
    }
}
?>