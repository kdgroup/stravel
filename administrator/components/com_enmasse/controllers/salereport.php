<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');
JTable::addIncludePath('components' . DS . 'com_enmasse' . DS . 'tables');
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "helpers" . DS . "EnmasseHelper.class.php");

class EnmasseControllerSalereport extends JControllerLegacy {

    function display($cachable = false, $urlparams = false) {
        JRequest::setVar('view', 'salereport');
        JRequest::setVar('layout', 'show');
        parent::display();
    }

    function searchSaleReport() {
        JRequest::setVar('view', 'salereport');
        JRequest::setVar('layout', 'show');
        parent::display();
    }

    function control() {
        $this->setRedirect('index.php?option=com_enmasse');
    }

    function generateReport() {
        $oModelSaleReport = JModelLegacy::getInstance('salereport', 'enmasseModel');
        $deal_name = JRequest::getVar('deal_name');

        $sale_person_name = JRequest::getVar('sale_person_name');
        $merchant_name = JRequest::getVar('merchant_name');
        $location_list = JRequest::getVar('location_list');
        $category_list = JRequest::getVar('category_list');
        $from_at = JRequest::getVar('from_at');
        $from_to = JRequest::getVar('from_to');
        $groupBy = JRequest::getVar('groupBy');
        $tmp = JRequest::getVar('tmpt');
        $isExport = JRequest::getVar('export');
        $deal_ids = "";
        if ($deal_name) {
            $deal_ids = JModelLegacy::getInstance('deal', 'enmasseModel')->getLikeName($deal_name);
        }
        if ($sale_person_name) {
            $sale_person_name = JModelLegacy::getInstance('salereports', 'enmasseModel')->getByName($sale_person_name);
        }
        if ($merchant_name) {
            $merchant_name = JModelLegacy::getInstance('merchant', 'enmasseModel')->getLikeName($merchant_name);
        }
        $result = JModelLegacy::getInstance('salereport', 'enmasseModel')->getForSalereprt
                ($deal_ids, $sale_person_name, $merchant_name, $location_list, $category_list, $from_at, $from_to);
//        $this->resultSearch = $result;
        if (count($result) > 0 && isset($isExport) && $isExport == "true") {
            $grBy = $groupBy;
            if (!$grBy) {
                $grBy = "Week";
            }
            $firstday = $result[0]->created_at;
            $fromEnd = $oModelSaleReport->setDateFromEnd($grBy, $firstday, 0, $result);
            $numOrder = 0;
            $id = 0;
//            $itemList = "";
            for ($i = 0; $i < count($result); $i++) {
                $firstday = $result[$i]->created_at;
                $flag = true;
                if ($firstday >= $fromEnd[0] && $firstday <= $fromEnd[1]) {
                    $flag = false;
                    $numOrder = $numOrder + 1;
                } else {
                    $flag = true;
                    $fromEnd = $oModelSaleReport->setDateFromEnd($grBy, $firstday, $numOrder, $result);
                }
                if ($flag || $i == 0) {
                    $itemList[$id]['dateFrom'] = $fromEnd[0] . "\t";
                    $itemList[$id]['dateEnd'] = $fromEnd[1] . "\t";
                    $itemList[$id]['numOrder'] = $fromEnd[2] . "\t";
                    $itemList[$id]['total'] = $fromEnd[3] . "\t";
                    $itemList[$id]['commission'] = $fromEnd[4] . "\t";
                    $itemList[$id]['totalAfterCommission'] = $fromEnd[5] . "</br>";
                    $id++;
                }
            }
            $rs = '<table style="border:1px solid black;border-collapse: collapse; font-size:12px;">';
            $rs .= "
				         <tr><td colspan='9' style='border:1px dotted #D5D5D5;font-size:16px; color:#0000FF; text-align:center;'>" . JTEXT::_('Sale Report') . "</td> </tr>
						<tr>
							<th width=\"15%\">" . JTEXT::_('Date from') . "</th>
							<th width=\"15%\">" . JTEXT::_('Date End') . "</th>
							<th width=\"15%\">" . JTEXT::_('No.Order') . "</th>
							<th width=\"15%\">" . JTEXT::_('Total') . "</th>
							<th width=\"15%\">" . JTEXT::_('Commission') . "</th>
							<th width=\"10%\">" . JTEXT::_('Total after Commission') . "</th>
						</tr>
					";
            $rs .= '</table>';
//            $rs = str_replace("\r", "", $rs);
            if ($tmp == "pdf") {
                require_once(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_enmasse' . DS . 'helpers' . DS . 'html2pdf' . DS . 'html2pdf.class.php');
                $html2pdf = new HTML2PDF('P', 'A4', 'en');
                $html2pdf->setDefaultFont('Arial');
                $html2pdf->writeHTML($rs);
                $outFileName = 'report-' . DatetimeWrapper::getDateOfNow() . '.pdf';
                ob_start();
                $html2pdf->Output($outFileName, 'I');
                ob_end_flush();
                die;
            } else {
                $filename = "Report" . date('Ymd') . ".xls";
                // $ab = enmasseHelper::reportGeneratorSaleReport($itemList);
                // covert from utf-8 without bom -> utf8
                $rss = chr(239) . chr(187) . chr(191) . $rs;
                echo $rss;
                header('Content-Encoding: UTF-8');
                header("Content-Disposition: attachment; filename=\"$filename\"");
                // header("Content-Type: application/vnd.ms-excel");
                header("Content-type: application/octet-stream;charset=UTF-8");
                header("Pragma: no-cache");
                header("Expires: 0");
                header("Lacation: excel.htm?id=yes");
                //print $rs;
                exit(0);
            }
        } else {
            $this->setRedirect(JURI::root() . 'administrator/index.php?option=com_enmasse&controller=salereport', JTEXT::_('REPORT_EMPTY_MSG'));
        }
    }

}

function replaceBr($strText) {
    $strText = str_replace('</br>', '', $strText);
    return $strText;
}
?>