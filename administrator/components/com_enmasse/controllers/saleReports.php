<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.controller');
JTable::addIncludePath('components'.DS.'com_enmasse'.DS.'tables');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse".DS."helpers". DS ."EnmasseHelper.class.php");

class EnmasseControllerSaleReports extends JControllerLegacy
{  
	function display($cachable = false, $urlparams = false)
	{
		JRequest::setVar('view', 'salereports');
		JRequest::setVar('layout', 'show');
		parent::display();
	}

	function save()
	{
		$data = JRequest::get( 'post' );
		$data['name'] = trim($data['name']);		
		$data['slug_name'] 		= EnmasseHelper::seoUrl($data['name']);
		$data['description'] 	= JRequest::getVar( 'description', '', 'post', 'string', JREQUEST_ALLOWRAW ); 
		$data['highlight'] 		= JRequest::getVar( 'highlight', '', 'post', 'string', JREQUEST_ALLOWRAW ); 
		$data['terms'] 			= JRequest::getVar( 'terms', '', 'post', 'string', JREQUEST_ALLOWRAW ); 
		
        if($data['slug_name']=='_' || $data['slug_name'] =='')
        {
           $now = str_replace(":"," ",DatetimeWrapper::getDatetimeOfNow());
           $data['slug_name'] = EnmasseHelper::seoUrl($now);
        }
        
		$model = JModelLegacy::getInstance('deal','enmasseModel');
		
		//---------------------------------------------------------------
		// if edit deal
		if($data['id'] > 0)
		{
			//---get deal data
			$deal = JModelLegacy::getInstance('deal','enmasseModel')->getById($data['id']);
			
			// get sold coupon qty for deal
			$soldCouponList = JModelLegacy::getInstance('invty','enmasseModel')->getSoldCouponByPdtId($deal->id);
			
		    //if from unlimited to limited
			if($deal->max_coupon_qty < 0  ) 
			{
				if($data['max_coupon_qty'] > 0)
				{
				    if($data['max_coupon_qty'] <= count($soldCouponList) )
					{
						$msg = JText::_('MSG_CURRENT_SOLD_GRATER_THAN_MODIFIED_COUPON');
						JFactory::getApplication()->redirect('index.php?option=com_enmasse&controller=deal&task=edit&cid='.$data['id'],$msg);
					}
					else
					{
						$numOfAddCoupon  = $data['max_coupon_qty']- count($soldCouponList);
					}
				}
			}
			else 
			{   //---------------- if change from limited to unlimited
				if($data['max_coupon_qty'] < 0 )
				 	$unlimit = true;
				 //-------------------------change the coupon qty to less
				else if($data['max_coupon_qty'] < $deal->max_coupon_qty)
				{
					//---------------------- if new coupon qty <= the sold coupon qty
					if( $data['max_coupon_qty'] <= count($soldCouponList) )
					{
						$msg = JText::_('MSG_CURRENT_SOLD_GRATER_THAN_MODIFIED_COUPON');
						JFactory::getApplication()->redirect('index.php?option=com_enmasse&controller=deal&task=edit&cid='.$data['id'],$msg);
					}
					else
					{
						$numOfRemoveCoupon = $deal->max_coupon_qty -  $data['max_coupon_qty'];
					}
				} //------------------ if change to coupon qty to greater qty
				else if($data['max_coupon_qty'] > $deal->max_coupon_qty)
					$numOfAddCoupon = $data['max_coupon_qty'] - $deal->max_coupon_qty;
			}
			
		}
		
			//------------------------
		//gemerate integration class
		 $integrateFileName = EnmasseHelper::getSubscriptionClassFromSetting().'.class.php';
		 $integrationClass = EnmasseHelper::getSubscriptionClassFromSetting();
		 require_once (JPATH_SITE . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."subscription". DS .$integrationClass. DS.$integrateFileName);
		 $integrationObject = new $integrationClass();
		 
		// store data
		$row = $model->store($data);
		
		if ($row->success)
		{ 
			if($data['id'] == 0)
				$integrationObject->integration($row,'newDeal');
			//--------------------------------------
			// store location and category
			JModelLegacy::getInstance('dealCategory','enmasseModel')->store($row->id,$data['pdt_cat_id']);
			JModelLegacy::getInstance('dealLocation','enmasseModel')->store($row->id,$data['location_id']);
			
			// if is new deal and limited the coupon then create coupon in invty
			if($data['id']==0 && $row->max_coupon_qty > 0)
			{
				for($i=0 ; $i < $row->max_coupon_qty; $i++)
				{
					
					$name = $i+1;
					JModelLegacy::getInstance('invty','enmasseModel')->generateCouponFreeStatus($row->id,$name,'Free');
				}
				
			}
			else if($data['id']!=0)
			{
				if(!empty($numOfRemoveCoupon))
				{
					$freeCouponList = JModelLegacy::getInstance('invty','enmasseModel')->getCouponFreeByPdtID($data['id']);
					// removed the coupons from invty
					for($i=0; $i < $numOfRemoveCoupon ; $i++)
					{
						JModelLegacy::getInstance('invty','enmasseModel')->removeById($freeCouponList[$i]->id);
					}
				}
				else if(!empty($numOfAddCoupon))
				{
					// add more coupon to invty
					for($i=0; $i < $numOfAddCoupon ; $i++)
					{
						$name = $i+1;
						JModelLegacy::getInstance('invty','enmasseModel')->generateCouponFreeStatus($data['id'],$name,'Free');
					}
				}
				else if($unlimit)
				{
					//remove all free coupon 
					JModelLegacy::getInstance('invty','enmasseModel')->removeCouponByPdtIdAndStatus($data['id'],'Free');
					
				}
				
			}
			$msg = JText::_('SAVE_SUCCESS_MSG');
			$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller'), $msg);
		}
		else
		{
			$msg = JText::_('SAVE_ERROR_MSG') .": " . $model->getError();
			if($data['id'] == null)
				$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller').'&task=add', $msg, 'error');
			else
				$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller').'&task=edit&cid[0]='. $data['id'], $msg, 'error');
		}
	}

	function control()
	{
		$this->setRedirect('index.php?option=com_enmasse');
	}

	function publish()
	{
		EnmasseHelper::changePublishState(1,'enmasse_deal','deal','deal');
	}
	function unpublish()
	{
		EnmasseHelper::changePublishState(0,'enmasse_deal','deal','deal');
	}

	function refreshOrder($by=null)
	{
		JModelLegacy::getInstance('deal','enmasseModel')->refreshOrder($by=null);
	}
	function upPosition()
	{
		EnmasseControllerDeal::changePosition('com_enmasse', true);
	}
	function downPosition()
	{
		EnmasseControllerDeal::changePosition('com_enmasse', false);
	}
	
	function changePosition($option, $up)
	{
		// get current item
		$id = JRequest::getVar('id');
		$cur = JModelLegacy::getInstance('deal','enmasseModel')->getCurrentPosition($id);
		
		// get other item
		if ($up)
			$temp = $cur->position - 1;
		else
			$temp = $cur->position + 1;
		$other = JModelLegacy::getInstance('deal','enmasseModel')->getNextPosition($temp);

		// change position
		if ($up)
		{
			$cur->position --;
			$other->position ++;
		}
		else
		{
			$cur->position ++;
			$other->position --;
		}

		JModelLegacy::getInstance('deal','enmasseModel')->store($cur);
		if ($other->id)
			JModelLegacy::getInstance('deal','enmasseModel')->store($other);

		// redirect
		$msg = JText::_( "ORDER_POSITION_UPDATED");
		JFactory::getApplication()->redirect("index.php?option=com_enmasse&controller=deal", $msg);

	}
	
	function updatePosition()
	{
		$id = JRequest::getVar('id');
		$updatePosition = JRequest::getVar('updatePosition');
		$listPosition = JModelLegacy::getInstance('deal','enmasseModel')->getListPosition($id);
		$cur = JModelLegacy::getInstance('deal','enmasseModel')->getCurrentPosition($id);
		$other = JModelLegacy::getInstance('deal','enmasseModel')->getOtherPosition($id);
       
		// Check value of the position was updated, max value of it shouldn`t greater than total deals
		$nTotal = count($other) + 1;
		if($updatePosition >$nTotal) $updatePosition = $nTotal;
		
		if($updatePosition > $cur->position)
		{
			for($i=0; $i<count($other);$i++)
			{
				if($other[$i]->position <= $updatePosition && $other[$i]->position > $cur->position)
				{
					$other[$i]->position -=1;
					JModelLegacy::getInstance('deal','enmasseModel')->store($other[$i]);
				}
			}
		}elseif ($updatePosition < $cur->position)
		{
			for($z=0; $z<count($other);$z++)
			{
			if($other[$z]->position >= $updatePosition && $other[$z]->position < $cur->position)
				{
					$other[$z]->position +=1;
					JModelLegacy::getInstance('deal','enmasseModel')->store($other[$z]);
				}
			}
		}
		
		$cur->position = $updatePosition;
		JModelLegacy::getInstance('deal','enmasseModel')->store($cur);
		
		// redirect
		$msg = JText::_( "ORDER_POSITION_UPDATED");
		JFactory::getApplication()->redirect("index.php?option=com_enmasse&controller=deal", $msg);
	}
	
	/*public static function printPdf(){
		?>
			<script type="text/javascript">
			<!--
				window.location.href = "index.php?option=com_enmasse&view=salereports";
				window.print();
			//-->
			</script>
		<?php
	}*/
	
	public static function pdf(){	
		$filter = JRequest::getVar('filter', array('name' => "", 'code' => "", 'merchant_id' => "", 'fromdate' => "", 'todate' => ""));
		$dealList 			= JModelLegacy::getInstance('salereports','enmasseModel')->search($filter['code'], $filter['name'],$filter['saleperson_id'],$filter['merchant_id'],$filter['fromdate'], $filter['todate']);
		
		$currency = JModelLegacy::getInstance('setting','enmasseModel')->getCurrencyPrefix();
		
		if(empty($dealList)){
			$msg = JText::_( "NO_SALE_REPORT");
			JFactory::getApplication()->redirect("index.php?option=com_enmasse&controller=saleReports", $msg,'notice');
			return false;
		}
		
		$result = '<table style="border:1px dotted #D5D5D5; border-collapse: collapse;"><tr valign="middle"><th style="border:1px dotted #D5D5D5;" align="center" width="30">'
						.JText::_("No").'</th><th style="border:1px dotted #D5D5D5;" align="center" width="80">'
						.JText::_("Deal Code").'</th><th style="border:1px dotted #D5D5D5;" align="center" width="80">'
						.JText::_("Deal Name").'</th><th style="border:1px dotted #D5D5D5;" align="center" width="60">'
						.JText::_("Merchant").'</th><th style="border:1px dotted #D5D5D5;" align="center" width="60">'
						.JText::_("Qty Sold").'</th><th style="border:1px dotted #D5D5D5;" align="center" width="80">'
						.JText::_("Unit Price").'</th><th style="border:1px dotted #D5D5D5;" align="center" width="80">'
						.JText::_("Total Sales").'</th><th style="border:1px dotted #D5D5D5;" align="center" width="80">'
						.JText::_("Commission Percentage").'</th><th style="border:1px dotted #D5D5D5;" align="left" width="150">'
						.JText::_("Total Commission Amount").'</th></tr>';
		$i = 0;
		$total_commission_amount = 0;
		foreach ($dealList as $row)
		{
			
			$i++;
			$merchant_name 	= JModelLegacy::getInstance('merchant','enmasseModel')->retrieveName($row->merchant_id);
			$total_sales = $row->price * $row->cur_sold_qty;
			$total_amount = ($total_sales * $row->commission_percent) / 100;
			$result .= '<tr>
				<td style="border:1px dotted #D5D5D5;" align="center">'.$i.'</td>
				<td style="border:1px dotted #D5D5D5;">'.$row->deal_code.'</td>
				<td style="border:1px dotted #D5D5D5;">'.$row->name.'</td>
				<td style="border:1px dotted #D5D5D5;">'.$merchant_name.'</td>
				<td style="border:1px dotted #D5D5D5;" align="center">'.$row->cur_sold_qty.'</td>
				<td style="border:1px dotted #D5D5D5;" align="center">'.$currency.$row->price.'</td>
				<td style="border:1px dotted #D5D5D5;" align="center">'.$currency.$total_sales.'</td>
				<td style="border:1px dotted #D5D5D5;" align="center">'.$row->commission_percent.' % </td>
				<td style="border:1px dotted #D5D5D5;" align="center">'.$currency.$total_amount.'</td></tr>';
			;
			
			$total_commission_amount += $total_amount;
		}
		$result .= '<tr><td style="border-right:1px dotted #D5D5D5; text-align:right" colspan="8"   >Total: </td>
					<td style="border:1px dotted #D5D5D5;" align="center" align="center">' .$currency.$total_commission_amount. '</td></tr></table>';
		//todo
	
		require_once(JPATH_ADMINISTRATOR. DS .'components' . DS .'com_enmasse' .DS .'helpers' .DS .'html2pdf' . DS .'html2pdf.class.php');
		
		$html2pdf = new HTML2PDF('P', 'A4', 'en');
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($result);
		$outFileName = 'report-' .DatetimeWrapper::getDateOfNow() .'.pdf';
		$html2pdf->Output($outFileName,'I');
		
		header("Content-type:application/pdf");
		// It will be called downloaded.pdf
		header("Content-Disposition:attachment;filename=downloaded.pdf");
		
		die();
	}
	public function cancel(){
		$this->setRedirect('index.php?option=com_enmasse');
	}
        
        function generateReport() {
            $oModelSaleReport = JModelLegacy::getInstance('salereport', 'enmasseModel');
            $deal_name = JRequest::getVar('deal_name');
            $from_at = JRequest::getVar('from_at');
            $from_to = JRequest::getVar('from_to');
            $saleperson_id = JRequest::getVar('sale_person_name');
            $tmp = JRequest::getVar('tmpt');
            $currency_prefix	= JModelLegacy::getInstance('setting','enmasseModel')->getCurrencyPrefix();
            $dealList 		= JModelLegacy::getInstance('salereports','enmasseModel')->search("", $deal_name,$saleperson_id,"",$from_at, $from_to);
            for($i=0; $i < count($dealList); $i++)
            {
                    $dealCategoryIdList = JModelLegacy::getInstance('dealcategory','enmasseModel')->getCategoryByDealId($dealList[$i]->id);
                    $dealLocationIdList = JModelLegacy::getInstance('deallocation','enmasseModel')->getLocationByDealId($dealList[$i]->id);

                    //----------------------------------------------
                    // get list of category name
                    if(count($dealCategoryIdList)!=0)
                            $categoryList = JModelLegacy::getInstance('category','enmasseModel')->getCategoryListInArrId($dealCategoryIdList);
                    else
                       $categoryList = null;
                     //----------------------------------------------
                    // get list of location name
                    if(count($dealLocationIdList)!=0)
                    $locationList = JModelLegacy::getInstance('location','enmasseModel')->getLocationListInArrId($dealLocationIdList);
                    else
                       $locationList = null;

                    if(count($locationList)!=0 && $locationList!=null)
                            $dealList[$i]->location_name 		= $locationList;
                    else 
                            $dealList[$i]->location_name 		= null;

                    if(count($categoryList)!=0 && $categoryList!=null)
                            $dealList[$i]->category_name 		= $categoryList;
                    else
                        $dealList[$i]->category_name 		= null;
                        $dealList[$i]->sales_person_name 	= JModelLegacy::getInstance('salesPerson','enmasseModel')->retrieveName($dealList[$i]->sales_person_id);
                        $dealList[$i]->merchant_name 		= JModelLegacy::getInstance('merchant','enmasseModel')->retrieveName($dealList[$i]->merchant_id);
            }
            $result = $dealList;
            
            
            
            $total_commission_amount = 0;
            $total_sales_amount = 0;
            if (count($result) > 0) {
                $id = 0;
                for ($i = 0; $i < count($result); $i++) {
                    $row = $result[$i];
                    $total_sales = $row->total_sales;
                    $total_commission = ($total_sales * $row->commission_percent) / 100;
                    $itemList[$id]['deal_code'] = $row->deal_code. "\t";
                    $itemList[$id]['name'] = $row->name."\t";
                    $itemList[$id]['sales_person_name'] = $row->sales_person_name. "\t";
                    $itemList[$id]['merchant_name'] = $row->merchant_name. "\t";
                    $itemList[$id]['cur_sold_qty'] = $row->cur_sold_qty. "\t";
                    $itemList[$id]['min_price'] = $row->min_price. "\t";
                    $itemList[$id]['max_price'] = $row->max_price. "\t";
                    $itemList[$id]['commission_percent'] = $row->commission_percent. "\t";
                    $itemList[$id]['total_commission'] = $total_commission. "\t";
                    $itemList[$id]['total_sales'] = $total_sales. "</br>";
                    $id++;
                    $total_commission_amount += $total_commission;
                    $total_sales_amount += $total_sales;
                }
                $rs = '<table style="border:1px solid black;border-collapse: collapse; font-size:9px;">';
                $rs .= "
                                             <tr><td colspan='9' style='border:1px dotted #D5D5D5;font-size:16px; color:#0000FF; text-align:center;'>" . JTEXT::_('Commission Report') . "</td> </tr>
                                                    <tr>
                                                            <th width=\"10%\" style='align=center'>" . JTEXT::_('DEAL_CODE') . "</th>
                                                            <th width=\"15%\">" . JTEXT::_('DEAL_NAME') . "</th>
                                                            <th width=\"15%\">" . JTEXT::_('DEAL_SALE_PERSON') . "</th>
                                                            <th width=\"10%\">" . JTEXT::_('DEAL_MERCHANT') . "</th>
                                                            <th width=\"10%\">" . JTEXT::_('DEAL_QUANTITY_SOLD') . "</th>
                                                            <th width=\"10%\">" . JTEXT::_('DEAL_UNIT_PRICE') . "</th>
                                                            <th style='width:100px;'>" . JTEXT::_('COMMISSION_TOTAL_SALES') . "</th>
                                                            <th width=\"10%\">" . JTEXT::_('Commission') . "</th>
                                                            <th width=\"10%\">" . JTEXT::_('COMMISSION_TOTAL_AMOUNT') . "</th>    
                                                    </tr>
                                            ";
                for ($i = 0; $i < count($itemList); $i++) {
                    $itemOrder = $itemList[$i];
                    $rs .= "<tr>   
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['deal_code']) . "</td>" . "
                 <td style='border:1px dotted #D5D5D5;width:130px;' align='center'>" . replaceBr($itemOrder['name']) . " </td>" . "
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['sales_person_name']) . " </td>" . "
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['merchant_name']) . "</td>" . "
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . $currency_prefix.replaceBr($itemOrder['cur_sold_qty']) . "</td>" . "
                 <td style='border:1px dotted #D5D5D5;width:85px;' align='center'>From" . $currency_prefix.replaceBr($itemOrder['min_price'])."to ".$currency_prefix.replaceBr($itemOrder['max_price']). "</td>" . "
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . $currency_prefix.replaceBr($itemOrder['total_sales']) . "</td>" . "
                 <td style='border:1px dotted #D5D5D5;width:50px;' align='center'>" . replaceBr($itemOrder['commission_percent']) . "% </td>" . "
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . $currency_prefix.replaceBr($itemOrder['total_commission']) . "</td>" . "
                 </tr> ";
                }
                $rs .= "<tr>
                            <td colspan='6' align='right'>". JText::_( 'COMMISSION_TOTAL' )."</td>
                            <td  align='center'>".$currency_prefix.number_format($total_sales_amount,2)."</td>
                            <td></td>
                            <td align='center'>".$currency_prefix.$total_commission_amount."</td>
                        </tr>";
                $rs .= '</table>';
    //            $rs = str_replace("\r", "", $rs);
                if ($tmp == "pdf") {
                    require_once(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_enmasse' . DS . 'helpers' . DS . 'html2pdf' . DS . 'html2pdf.class.php');
                    $html2pdf = new HTML2PDF('P', 'A4', 'en');
                    $html2pdf->setDefaultFont('Arial');
                    $html2pdf->writeHTML($rs);
                    $outFileName = 'report-' . DatetimeWrapper::getDateOfNow() . '.pdf';
                    ob_start();
                    $html2pdf->Output($outFileName, 'I');
                    ob_end_flush();
                    die;
                } else {
                    $filename = "Report" . date('Ymd') . ".xls";
                    // $ab = enmasseHelper::reportGeneratorSaleReport($itemList);
                    $rss = chr(239) . chr(187) . chr(191) . $rs;
                    echo $rss;
                    header('Content-Encoding: UTF-8');
                    header("Content-Disposition: attachment; filename=\"$filename\"");
                    // header("Content-Type: application/vnd.ms-excel");
                    header("Content-type: application/octet-stream;charset=UTF-8");
                    header("Pragma: no-cache");
                    header("Expires: 0");
                    header("Lacation: excel.htm?id=yes");
                    //print $rs;
                    exit(0);
                }
            } else {
                $this->setRedirect(JURI::root() . 'administrator/index.php?option=com_enmasse&controller=saleReports', JTEXT::_('REPORT_EMPTY_MSG'));
            }
        }
        
   function generateSaleReport() {
        $oModelSaleReport = JModelLegacy::getInstance('salereport', 'enmasseModel');
        $deal_name = JRequest::getVar('deal_name');

        $sale_person_name = JRequest::getVar('sale_person_name');
        $merchant_name = JRequest::getVar('merchant_name');
        $location_list = JRequest::getVar('location_list');
        $category_list = JRequest::getVar('category_list');
        $from_at = JRequest::getVar('from_at');
        $from_to = JRequest::getVar('from_to');
        $groupBy = JRequest::getVar('groupBy');
        $tmp = JRequest::getVar('tmpt');
        $isExport = JRequest::getVar('export');
        $deal_ids = "";
        if ($deal_name) {
            $deal_ids = JModelLegacy::getInstance('deal', 'enmasseModel')->getLikeName($deal_name);
        }
        if ($sale_person_name) {
            $sale_person_name = JModelLegacy::getInstance('salereports', 'enmasseModel')->getByName($sale_person_name);
        }
        if ($merchant_name) {
            $merchant_name = JModelLegacy::getInstance('merchant', 'enmasseModel')->getLikeName($merchant_name);
        }
        $result = JModelLegacy::getInstance('salereport', 'enmasseModel')->getForSalereprt
                ($deal_ids, $sale_person_name, $merchant_name, $location_list, $category_list, $from_at, $from_to);
//        $this->resultSearch = $result;
        if (count($result) > 0 && isset($isExport) && $isExport == "true") {
            $grBy = $groupBy;
            if (!$grBy) {
                $grBy = "Week";
            }
            $firstday = $result[0]->created_at;
            $fromEnd = $oModelSaleReport->setDateFromEnd($grBy, $firstday, 0, $result);
            $numOrder = 0;
            $id = 0;
//            $itemList = "";
            for ($i = 0; $i < count($result); $i++) {
                $firstday = $result[$i]->created_at;
                $flag = true;
                if ($firstday >= $fromEnd[0] && $firstday <= $fromEnd[1]) {
                    $flag = false;
                    $numOrder = $numOrder + 1;
                } else {
                    $flag = true;
                    $fromEnd = $oModelSaleReport->setDateFromEnd($grBy, $firstday, $numOrder, $result);
                }
                if ($flag || $i == 0) {
                    $itemList[$id]['dateFrom'] = $fromEnd[0] . "\t";
                    $itemList[$id]['dateEnd'] = $fromEnd[1] . "\t";
                    $itemList[$id]['numOrder'] = $fromEnd[2] . "\t";
                    $itemList[$id]['total'] = $fromEnd[3] . "\t";
                    $itemList[$id]['commission'] = $fromEnd[4] . "\t";
                    $itemList[$id]['totalAfterCommission'] = $fromEnd[5] . "</br>";
                    $id++;
                }
            }
            $rs = '<table style="border:1px solid black;border-collapse: collapse; font-size:12px;">';
            $rs .= "
				         <tr>
                                                <td colspan='9' style='border:1px dotted #D5D5D5;font-size:16px; color:#0000FF; text-align:center;'>" . JTEXT::_('Sale Report') . "</td> 
                                         </tr>
                                        <tr>
                                                <th width=\"20%\"  style='border:1px dotted #D5D5D5;' align='center'>" . JTEXT::_('Date from') . "</th>
                                                <th width=\"20%\"  style='border:1px dotted #D5D5D5;' align='center'>" . JTEXT::_('Date End') . "</th>
                                                <th width=\"15%\"  style='border:1px dotted #D5D5D5;' align='center'>" . JTEXT::_('No.Order') . "</th>
                                                <th width=\"15%\"  style='border:1px dotted #D5D5D5;' align='center'>" . JTEXT::_('Total') . "</th>
                                                <th width=\"15%\"  style='border:1px dotted #D5D5D5;' align='center'>" . JTEXT::_('Commission') . "</th>
                                                <th width=\"15%\"  style='border:1px dotted #D5D5D5;' align='center'>" . JTEXT::_('Total after Commission') . "</th>
                                        </tr>
					";
            for ($i = 0; $i < count($itemList); $i++) {
                $itemOrder = $itemList[$i];
                $rs .= "<tr>   
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['dateFrom']) . "</td>" . "
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['dateEnd']) . " </td>" . "
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['numOrder']) . " </td>" . "
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['total']) . "</td>" . "
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['commission']) . "</td>" . "
                 <td style='border:1px dotted #D5D5D5;' align='center'>" .replaceBr($itemOrder['totalAfterCommission']). "</td>" . "
                 </tr> ";
           }
            $rs .= '</table>';
//            $rs = str_replace("\r", "", $rs);
            if ($tmp == "pdf") {
                require_once(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_enmasse' . DS . 'helpers' . DS . 'html2pdf' . DS . 'html2pdf.class.php');
                $html2pdf = new HTML2PDF('P', 'A4', 'en');
                $html2pdf->setDefaultFont('Arial');
                $html2pdf->writeHTML($rs);
                $outFileName = 'report-' . DatetimeWrapper::getDateOfNow() . '.pdf';
//                ob_start();
                ob_end_clean();
                $html2pdf->Output($outFileName, 'I');
//                ob_end_flush();
                die;
            } else {
                $filename = "Report" . date('Ymd') . ".xls";
                // $ab = enmasseHelper::reportGeneratorSaleReport($itemList);
                $rss = chr(239) . chr(187) . chr(191) . $rs;
                echo $rss;
                header('Content-Encoding: UTF-8');
                header("Content-Disposition: attachment; filename=\"$filename\"");
                // header("Content-Type: application/vnd.ms-excel");
                header("Content-type: application/octet-stream;charset=UTF-8");
                header("Pragma: no-cache");
                header("Expires: 0");
                header("Lacation: excel.htm?id=yes");
                //print $rs;
                exit(0);
            }
        } else {
            $this->setRedirect(JURI::root() . 'administrator/index.php?option=com_enmasse&controller=salereport', JTEXT::_('REPORT_EMPTY_MSG'));
        }
    }
    
}

        function replaceBr($strText) {
            $strText = str_replace('</br>', '', $strText);
            return $strText;
        }
?>