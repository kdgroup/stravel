<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.controller');
JTable::addIncludePath('components'.DS.'com_enmasse'.DS.'tables');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse".DS."helpers". DS ."EnmasseHelper.class.php");

class EnmasseControllerinsurance extends JControllerLegacy
{

	function display($cachable = false, $urlparams = false)
	{
		JRequest::setVar('view', 'insurance');
		JRequest::setVar('layout', 'show');
		parent::display();
	}
	function edit()
	{
		JRequest::setVar('view', 'insurance');
		JRequest::setVar('layout', 'edit');
		parent::display();
	}

	function add()
	{
		JRequest::setVar('view', 'insurance');
		JRequest::setVar('layout', 'edit');
		parent::display();
	}
	function save()
	{
		$data = JRequest::get('post');
		// trim space
		$data['insurance_name'] = trim ($data['insurance_name']);
		
		
		$model = JModelLegacy::getInstance('insurance','enmasseModel');
		if(strlen($data['insurance_name']) > 50 || strlen($data['insurance_name']) < 1)
		{
			$msg = JText::_('SAVE_ERROR_MSG'). ": " .JText::_('Insurance name save error');
			if($data['id'] == null)
				$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller').'&task=add', $msg, 'error');
			else
				$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller').'&task=edit&cid[0]='. $data['id'], $msg, 'error');
		}
		
		else if ($model->store($data))
		{
			$msg = JText::_('SAVE_SUCCESS_MSG');
			$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller'), $msg);
		}
		else
		{
			$msg = JText::_('SAVE_ERROR_MSG') .": " . $model->getError();
			if($data['id'] == null)
				$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller').'&task=add', $msg, 'error');
			else
				$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller').'&task=edit&cid[0]='. $data['id'], $msg, 'error');
		}
	}
	
	function control()
	{
		$this->setRedirect('index.php?option=com_enmasse');
	}

	function remove()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );
		
		$model = JModelLegacy::getInstance('insurance','enmasseModel');
                $oModelDealinsurance = JModelLegacy::getInstance('dealinsurance','enmasseModel');
		$msg = "";
        
                
                $cids = array_merge($cids, $subList);
		if($model->deleteList($cids))
		{
			// to remove insurance from deal
			/* remove at 18/05/2011
			 * for($i=0; $i < count($cids); $i++)
			{
				JModelLegacy::getInstance('dealinsurance','enmasseModel')->removeByinsurance($cids[$i]);
			}*/
			// Commented on July 27, 2011
			//$msg = JText::_('DELETE_SUCCESS_MSG');
			//$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller'), $msg );
		}
		else
		{ 
			$msg .= JText::_('DELETE_ERROR_MSG') .": " . $model->getError();
			$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller'), $msg, 'error');
		}
		JFactory::getApplication()->redirect('index.php?option=com_enmasse&controller=insurance', $msg , 'error');
	}

	function publish()
	{
		EnmasseHelper::changePublishState(1,'enmasse_insurance','insurance','insurance');
	}
	function unpublish()
	{
		EnmasseHelper::changePublishState(0,'enmasse_insurance','insurance','insurance');
	}
	
	function checkDuplicatedinsurance()
	{
		
		$insuranceName = addslashes(JRequest::getVar("insuranceName"));
		$insuranceObj = JModelLegacy::getInstance('insurance','enmasseModel')->getinsuranceByName($insuranceName);
		if($insuranceObj != null)
		 echo 'true';
		else
		 echo 'false';
		die;
		
	}
	
	
}
?>