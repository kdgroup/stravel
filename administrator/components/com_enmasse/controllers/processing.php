<?php
/*------------------------------------------------------------------------
# En Masse - Social Buying Extension 2010
# ------------------------------------------------------------------------
# By Matamko.com
# Copyright (C) 2010 Matamko.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.matamko.com
# Technical Support:  Visit our forum at www.matamko.com
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.controller');
JTable::addIncludePath('components'.DS.'com_enmasse'.DS.'tables');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse".DS."helpers". DS ."EnmasseHelper.class.php");

class EnmasseControllerProcessing extends JControllerLegacy
{

	function display($cachable = false, $urlparams = false)
	{
		JRequest::setVar('view', 'processing');
		JRequest::setVar('layout', 'show');
		parent::display();
	}

	function edit()
	{
		JRequest::setVar('view', 'processing');
		JRequest::setVar('layout', 'edit');
		parent::display();
	}

	function control()
	{
		$this->setRedirect('index.php?option=com_enmasse');
	}
	
	
	function save()
	{
		$data = JRequest::get( 'post' );
		$data['status'] = $data['delivery_status'];
		$model = JModelLegacy::getInstance('processing','enmasseModel');
                
		$model->store($data);
                
                $msg = JTEXT::_('Record has being saved');
                $this->setRedirect('index.php?option=com_enmasse&controller=processing', $msg);
	}
        function popupNoteSubmit(){
                JRequest::setVar('view', 'processing');
		JRequest::setVar('layout', 'popupnote');
                parent::display();
        }
        function submitPopUpNote(){
                $processID = JRequest::getVar('processID');
                $oProcess = JModelLegacy::getInstance('processing','enmasseModel');
		$note = JRequest::getVar('note');
		$oProcess->updateSubmitNote($processID,$note);
		
                echo '<script type="text/javascript">
                    window.parent.location.reload();                                
                    window.parent.SqueezeBox.close();
                   </script>';
                exit();
        }
}
