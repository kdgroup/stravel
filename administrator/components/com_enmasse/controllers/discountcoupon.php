<?php

/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');
JTable::addIncludePath('components' . DS . 'com_enmasse' . DS . 'tables');
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "helpers" . DS . "EnmasseHelper.class.php");

class EnmasseControllerDiscountcoupon extends JControllerLegacy {

    function display($cachable = false, $urlparams = false) {
        JRequest::setVar('view', 'discountcoupon');
        JRequest::setVar('layout', 'show');
        parent::display();
    }

    function edit() {
        JRequest::setVar('view', 'discountcoupon');
        JRequest::setVar('layout', 'edit');
        parent::display();
    }

    function add() {
        JRequest::setVar('view', 'discountcoupon');
        JRequest::setVar('layout', 'edit');
        parent::display();
    }

    function viewCoupon($cachable = false, $urlparams = false) {
        JRequest::setVar('view', 'discountcoupon');
        JRequest::setVar('layout', 'viewcoupon');
        parent::display();
    }

    function reportcoupon() {
        JRequest::setVar('view', 'discountcoupon');
        JRequest::setVar('layout', 'reportcoupon');
        parent::display();
    }

    function save() {
        $data = JRequest::get('post');
        $model = JModelLegacy::getInstance('discountcoupon', 'enmasseModel');
        if ($model->store($data)) {
            $msg = JText::_('SAVE_SUCCESS_MSG');
            $this->setRedirect('index.php?option=com_enmasse&controller=' . JRequest::getVar('controller'), $msg);
        } else {
            $msg = JText::_('SAVE_ERROR_MSG') . ": " . $model->getError();
            if ($data['id'] == null)
                $this->setRedirect('index.php?option=com_enmasse&controller=' . JRequest::getVar('controller') . '&task=add', $msg, 'error');
            else
                $this->setRedirect('index.php?option=com_enmasse&controller=' . JRequest::getVar('controller') . '&task=edit&cid[0]=' . $data['id'], $msg, 'error');
        }
    }

    function control() {
        $this->setRedirect('index.php?option=com_enmasse');
    }

    function remove() {
        $cids = JRequest::getVar('cid', array(0), 'post', 'array');
        $model = JModelLegacy::getInstance('discountcoupon', 'enmasseModel');
        $msg = "";
        $subList = array();
        if ($model->deleteList($cids)) {
            
        } else {
            $msg .= JText::_('DELETE_ERROR_MSG') . ": " . $model->getError();
            $this->setRedirect('index.php?option=com_enmasse&controller=' . JRequest::getVar('controller'), $msg, 'error');
        }
        JFactory::getApplication()->redirect('index.php?option=com_enmasse&controller=discountcoupon', $msg, 'error');
    }

    function checkDuplicatedCode() {
        $couponCode = addslashes(JRequest::getVar("couponCode"));
        $couponObj = JModelLegacy::getInstance('discountcoupon', 'enmasseModel')->getCouponByCode($couponCode);
        if ($couponObj != null)
            echo 'true';
        else
            echo 'false';
        die;
    }

    function generateReport() {
        $coupon_name = JRequest::getVar('coupon_name');
        $coupon_code = JRequest::getVar('coupon_code');
        $discount_at = JRequest::getVar('discount_at');
        $to_discount = JRequest::getVar('to_discount');
        $type = JRequest::getVar('type');
        $start_at = JRequest::getVar('start_at');
        $start_to = JRequest::getVar('start_to');
        $end_at = JRequest::getVar('end_at');
        $end_to = JRequest::getVar('end_to');
        $use_at = JRequest::getVar('use_at');
        $use_to = JRequest::getVar('use_to');
        $tmp = JRequest::getVar('tmpt');
        $isExport = JRequest::getVar('export'); //
        if ($coupon_name || $coupon_code || $discount_at || $to_discount || $type || $start_at || $start_to || $end_at || $end_to || $use_at || $use_to) {
            $discountCouponList =
                    JModelLegacy::getInstance('discountcoupon', 'enmasseModel')->getReportVoucher(
                    $coupon_name, $coupon_code, $discount_at, $to_discount, $type, $start_at, $start_to, $end_at, $end_to, $use_at, $use_to);
            for ($i = 0; $i < count($discountCouponList); $i++) {
                $useTime = JModelLegacy::getInstance('discountcoupon', 'enmasseModel')->getUseTime($discountCouponList[$i]->id);
                $discountCouponList[$i]->usedTimes = $useTime->useTime;
                $discountCouponList[$i]->lastTimeuse = $useTime->lastUsed;
            }
            //$this->assignRef('discountCouponList', $discountCouponList);
        }
        $result = $discountCouponList;
        if (count($result) > 0 && isset($isExport) && $isExport == "true") {
            $id = 0;
            for ($i = 0; $i < count($result); $i++) {
                $row = $result[$i];
                $itemList[$id]['name'] = $row->name . "\t";
                $itemList[$id]['voucher_code'] = $row->voucher_code . "\t";
                $itemList[$id]['discount_amount'] = $row->discount_amount . "\t";
                $itemList[$id]['start_at'] = $row->start_at . "\t";
                $itemList[$id]['end_at'] = $row->end_at . "\t";
                $itemList[$id]['usedTimes'] = $row->usedTimes . "\t";
                $itemList[$id]['lastTimeuse'] = $row->lastTimeuse . "\t";
                $itemList[$id]['status'] = $row->status . "</br>";
                $id++;
            }

            $rs = '<table style="border:1px solid black;border-collapse: collapse; font-size:12px;">';
            $rs .= "
				         <tr>
                                                <td colspan='9' style='border:1px dotted #D5D5D5;font-size:16px; color:#0000FF; text-align:center;'>" . JTEXT::_('Discount Coupon Report') . "</td> 
                                         </tr>
                                        <tr>
                                                <th width=\"10%\"  style='border:1px dotted #D5D5D5;' align='center'>" . JTEXT::_('Coupon Name') . "</th>
                                                <th width=\"10%\"  style='border:1px dotted #D5D5D5;' align='center'>" . JTEXT::_('Coupon Code') . "</th>
                                                <th width=\"10%\"  style='border:1px dotted #D5D5D5;' align='center'>" . JTEXT::_('Discount') . "</th>
                                                <th width=\"15%\"  style='border:1px dotted #D5D5D5;' align='center'>" . JTEXT::_('Date Start') . "</th>
                                                <th width=\"15%\"  style='border:1px dotted #D5D5D5;' align='center'>" . JTEXT::_('Date End') . "</th>
                                                <th width=\"15%\"  style='border:1px dotted #D5D5D5;' align='center'>" . JTEXT::_('Used Times') . "</th>
                                                <th width=\"15%\"  style='border:1px dotted #D5D5D5;' align='center'>" . JTEXT::_('Last Time Used') . "</th>
                                                <th width=\"10%\"  style='border:1px dotted #D5D5D5;' align='center'>" . JTEXT::_('Status') . "</th>
                                        </tr>
					";
            for ($i = 0; $i < count($itemList); $i++) {
                $itemOrder = $itemList[$i];
                $rs .= "<tr>   
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['name']) . "</td>" . "
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['voucher_code']) . " </td>" . "
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['discount_amount']) . " </td>" . "
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['start_at']) . "</td>" . "
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['end_at']) . "</td>" . "
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['usedTimes']) . "</td>" . "
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['lastTimeuse']) . "</td>" . "
                 <td style='border:1px dotted #D5D5D5;' align='center'>" . replaceBr($itemOrder['status']) . "</td>" . "
                 </tr> ";
            }
            $rs .= '</table>';
//            echo "<pre>";
//            print_r(mb_detect_encoding($rs));
//            die;
//            $rs = str_replace("\r", "", $rs);
            if ($tmp == "pdf") {
                require_once(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_enmasse' . DS . 'helpers' . DS . 'html2pdf' . DS . 'html2pdf.class.php');
                $html2pdf = new HTML2PDF('P', 'A4', 'en'); //, true, 'ISO-8859-15'
                $html2pdf->setDefaultFont('Arial');
                $html2pdf->writeHTML($rs);
                $outFileName = 'report-' . DatetimeWrapper::getDateOfNow() . '.pdf';
//                ob_start();
                ob_end_clean();
                $html2pdf->Output($outFileName, 'I');
//                ob_end_flush();
                die;
            } else {
                $filename = "Report" . date('Ymd') . ".xls";
                // $ab = enmasseHelper::reportGeneratorSaleReport($itemList);
//                $rs = mb_convert_encoding($rs, "ISO-8859-13", "UTF-8");
                $rss = chr(239) . chr(187) . chr(191) . $rs;
                header('Content-Encoding: UTF-8');
                header("Content-Disposition: attachment; filename=\"$filename\"");
//                header("Content-Type: application/vnd.ms-excel","UTF-8");
                header("Content-type: application/octet-stream;charset=UTF-8");
                header("Pragma: no-cache");
                header("Expires: 0");
                header("Lacation: excel.htm?id=yes");
                print $rss;
                exit(0);
            }
        } else {
            $this->setRedirect(JURI::root() . 'administrator/index.php?option=com_enmasse&controller=discountcoupon&task=reportcoupon', JTEXT::_('REPORT_EMPTY_MSG'));
        }
    }

    function autoCompleteCouponName() {
        $return_arr = array();
        $oDiscountCoupon = JModelLegacy::getInstance('discountcoupon', 'enmasseModel');
        $listCouponName = $oDiscountCoupon->getAllCouponName();
        for ($i = 0; $i < count($listCouponName); $i++) {
            $return_arr[] = $listCouponName[$i]->name;
        }
        echo json_encode($return_arr);
        exit();
    }

    function autoCompleteCouponCode() {
        $return_arr = array();
        $oDiscountCoupon = JModelLegacy::getInstance('discountcoupon', 'enmasseModel');
        $listCouponCode = $oDiscountCoupon->getAllCouponCode();
        for ($i = 0; $i < count($listCouponCode); $i++) {
            $return_arr[] = $listCouponCode[$i]->voucher_code;
        }
        echo json_encode($return_arr);
        exit();
    }

    function autoCompleteCustomerCouppon() {
        $return_arr = array();
        $oDiscountCoupon = JModelLegacy::getInstance('discountcoupon', 'enmasseModel');
        $listCustomer = $oDiscountCoupon->getAllCustomer();
        for ($i = 0; $i < count($listCustomer); $i++) {
            $return_arr[] = $listCustomer[$i]->username;
        }
        echo json_encode($return_arr);
        exit();
    }

}

function replaceBr($strText) {
    $strText = str_replace('</br>', '', $strText);
    return $strText;
}

function UTF8toiso8859_11($string) {

    if (!ereg("[\241-\377]", $string))
        return $string;

    $UTF8 = array(
        "\xe0\xb8\x81" => "\xa1",
        "\xe0\xb8\x82" => "\xa2",
        "\xe0\xb8\x83" => "\xa3",
        "\xe0\xb8\x84" => "\xa4",
        "\xe0\xb8\x85" => "\xa5",
        "\xe0\xb8\x86" => "\xa6",
        "\xe0\xb8\x87" => "\xa7",
        "\xe0\xb8\x88" => "\xa8",
        "\xe0\xb8\x89" => "\xa9",
        "\xe0\xb8\x8a" => "\xaa",
        "\xe0\xb8\x8b" => "\xab",
        "\xe0\xb8\x8c" => "\xac",
        "\xe0\xb8\x8d" => "\xad",
        "\xe0\xb8\x8e" => "\xae",
        "\xe0\xb8\x8f" => "\xaf",
        "\xe0\xb8\x90" => "\xb0",
        "\xe0\xb8\x91" => "\xb1",
        "\xe0\xb8\x92" => "\xb2",
        "\xe0\xb8\x93" => "\xb3",
        "\xe0\xb8\x94" => "\xb4",
        "\xe0\xb8\x95" => "\xb5",
        "\xe0\xb8\x96" => "\xb6",
        "\xe0\xb8\x97" => "\xb7",
        "\xe0\xb8\x98" => "\xb8",
        "\xe0\xb8\x99" => "\xb9",
        "\xe0\xb8\x9a" => "\xba",
        "\xe0\xb8\x9b" => "\xbb",
        "\xe0\xb8\x9c" => "\xbc",
        "\xe0\xb8\x9d" => "\xbd",
        "\xe0\xb8\x9e" => "\xbe",
        "\xe0\xb8\x9f" => "\xbf",
        "\xe0\xb8\xa0" => "\xc0",
        "\xe0\xb8\xa1" => "\xc1",
        "\xe0\xb8\xa2" => "\xc2",
        "\xe0\xb8\xa3" => "\xc3",
        "\xe0\xb8\xa4" => "\xc4",
        "\xe0\xb8\xa5" => "\xc5",
        "\xe0\xb8\xa6" => "\xc6",
        "\xe0\xb8\xa7" => "\xc7",
        "\xe0\xb8\xa8" => "\xc8",
        "\xe0\xb8\xa9" => "\xc9",
        "\xe0\xb8\xaa" => "\xca",
        "\xe0\xb8\xab" => "\xcb",
        "\xe0\xb8\xac" => "\xcc",
        "\xe0\xb8\xad" => "\xcd",
        "\xe0\xb8\xae" => "\xce",
        "\xe0\xb8\xaf" => "\xcf",
        "\xe0\xb8\xb0" => "\xd0",
        "\xe0\xb8\xb1" => "\xd1",
        "\xe0\xb8\xb2" => "\xd2",
        "\xe0\xb8\xb3" => "\xd3",
        "\xe0\xb8\xb4" => "\xd4",
        "\xe0\xb8\xb5" => "\xd5",
        "\xe0\xb8\xb6" => "\xd6",
        "\xe0\xb8\xb7" => "\xd7",
        "\xe0\xb8\xb8" => "\xd8",
        "\xe0\xb8\xb9" => "\xd9",
        "\xe0\xb8\xba" => "\xda",
        "\xe0\xb8\xbf" => "\xdf",
        "\xe0\xb9\x80" => "\xe0",
        "\xe0\xb9\x81" => "\xe1",
        "\xe0\xb9\x82" => "\xe2",
        "\xe0\xb9\x83" => "\xe3",
        "\xe0\xb9\x84" => "\xe4",
        "\xe0\xb9\x85" => "\xe5",
        "\xe0\xb9\x86" => "\xe6",
        "\xe0\xb9\x87" => "\xe7",
        "\xe0\xb9\x88" => "\xe8",
        "\xe0\xb9\x89" => "\xe9",
        "\xe0\xb9\x8a" => "\xea",
        "\xe0\xb9\x8b" => "\xeb",
        "\xe0\xb9\x8c" => "\xec",
        "\xe0\xb9\x8d" => "\xed",
        "\xe0\xb9\x8e" => "\xee",
        "\xe0\xb9\x8f" => "\xef",
        "\xe0\xb9\x90" => "\xf0",
        "\xe0\xb9\x91" => "\xf1",
        "\xe0\xb9\x92" => "\xf2",
        "\xe0\xb9\x93" => "\xf3",
        "\xe0\xb9\x94" => "\xf4",
        "\xe0\xb9\x95" => "\xf5",
        "\xe0\xb9\x96" => "\xf6",
        "\xe0\xb9\x97" => "\xf7",
        "\xe0\xb9\x98" => "\xf8",
        "\xe0\xb9\x99" => "\xf9",
        "\xe0\xb9\x9a" => "\xfa",
        "\xe0\xb9\x9b" => "\xfb",
    );

    $string = strtr($string, $UTF8);
    return $string;
}

?>