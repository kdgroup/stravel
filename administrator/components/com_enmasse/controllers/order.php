<?php
/*------------------------------------------------------------------------
# En Masse - Social Buying Extension 2010
# ------------------------------------------------------------------------
# By Matamko.com
# Copyright (C) 2010 Matamko.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.matamko.com
# Technical Support:  Visit our forum at www.matamko.com
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.controller');
JTable::addIncludePath('components'.DS.'com_enmasse'.DS.'tables');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse".DS."helpers". DS ."EnmasseHelper.class.php");

class EnmasseControllerOrder extends JControllerLegacy
{

	function display($cachable = false, $urlparams = false)
	{
		JRequest::setVar('view', 'order');
		JRequest::setVar('layout', 'show');
		parent::display();
	}

	function edit()
	{
		JRequest::setVar('view', 'order');
		JRequest::setVar('layout', 'edit');
		parent::display();
	}

	function control()
	{
		$this->setRedirect('index.php?option=com_enmasse');
	}
	
	public function exportExcel()
	{
		JRequest::setVar('view', 'order');
		JRequest::setVar('layout', 'excel_export');
		parent::display();
	}
	
	function save()
	{
		$status = JRequest::getVar( 'status', '', 'post');
		$partial = JRequest::getVar('partial', 0, 'method', 'int');
		$orderData = new JObject();
		$orderData->id 				= 	JRequest::getInt( 'id', '', 'post');
		$orderData->description 		= 	JRequest::getVar( 'description', '', 'post', 'text', JREQUEST_ALLOWRAW );
		//
		$orderData->buyerid 		= 	JRequest::getInt( 'buyerid', '', 'post');
		
		$model = JModelLegacy::getInstance('order','enmasseModel');
		
		if(empty($status))//admin dont update order status, so we just save order information
		{
			if ($model->store($orderData))
			{
				$msg = JText::_('SAVE_SUCCESS_MSG');
				if($partial)
				{
					$this->setRedirect('index.php?option=com_enmasse&controller=partialOrder', $msg);
				}else 
				{
					$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller'), $msg);
				}
				
			}
			else
			{
				$msg = JText::_('SAVE_ERROR_MSG') .": " . $model->getError();
				if($orderData->id == null)
					$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller').'&task=add', $msg, 'error');
				else
					$this->setRedirect('index.php?option=com_enmasse&controller='.JRequest::getVar('controller').'&task=edit&cid[0]='. $orderData->id .'&partial=' .$partial, $msg, 'error');
			}
		}
		else
		{
			//------------------------------
			//  send email and update total sold qty for each case of when change status of order
			
		    if( $status == EnmasseHelper::$ORDER_STATUS_LIST["Paid"])
		    {
				//// update deal quality sold
			//	JModelLegacy::getInstance('deal', 'enmasseModel')->addQtySold($orderItem->pdt_id, $orderItem->qty);
				$orderItemList = JModelLegacy::getInstance('orderItem', 'enmasseModel')->listByOrderId($orderData->id 	);
				for ($count = 0; $count < count($orderItemList); $count++)
				{
            				$orderItem = $orderItemList[$count];
					JModelLegacy::getInstance('deal', 'enmasseModel')->addQtySold($orderItem->pdt_id, $orderItem->qty);
				}
	    			EnmasseHelper::doNotify($orderData->id); 	
                                $msg = JTEXT::_('SAVE_SUCCESS_MSG_AND_SEND_RECEIPT');
		    }
		    else if( $status == EnmasseHelper::$ORDER_STATUS_LIST["Cancelled"])
		    {
				$orderId = $orderData->id;
                                $order = JModelLegacy::getInstance('order', 'enmasseModel')->getById($orderId);
				JModelLegacy::getInstance('order', 'enmasseModel')->updateStatus($order->id, 'Cancelled');
				$orderItemList = JModelLegacy::getInstance('orderItem', 'enmasseModel')->listByOrderId($orderId);
				for ($count = 0; $count < count($orderItemList); $count++)
				{
                                        $orderItem = $orderItemList[$count];
					JModelLegacy::getInstance('orderItem', 'enmasseModel')->updateStatus($orderItem->id, 'Unpaid');
                                        JModelLegacy::getInstance('invty', 'enmasseModel')->freeCouponByOrderItemId($orderItem->id, 'Free');
				}
	  	    	$msg = JTEXT::_('SAVE_SUCCESS_MSG');
		    }            
		    else if($status == EnmasseHelper::$ORDER_STATUS_LIST["Waiting_For_Refund"])
		    {
		    	$orderItemList = JModelLegacy::getInstance('orderItem','enmasseModel')->listByOrderId($orderData->id);
		   		foreach($orderItemList as $orderItem)
				{
					EnmasseHelper::orderItemWaitingForRefund($orderItem);
					sleep(1);
				}
				$msg = JTEXT::_('SAVE_SUCCESS_MSG_AND_SEND_REFUND');	    
	
		    }
		    
			else if($status == EnmasseHelper::$ORDER_STATUS_LIST["Refunded"])
		    {
		    	$orderItemList = JModelLegacy::getInstance('orderItem','enmasseModel')->listByOrderId($orderData->id);
		   		foreach($orderItemList as $orderItem)
				{
					// reduce deal quality sold
					// only reduce when order item was delivered
					$isDelivered = intVal($orderItem->is_delivered);
					if ($isDelivered) {
						JModelLegacy::getInstance('deal', 'enmasseModel')->reduceQtySold($orderItem->pdt_id, $orderItem->qty);
					}
					
					EnmasseHelper::orderItemRefunded($orderItem);
					sleep(1);
				}
				$msg = JTEXT::_('SAVE_SUCCESS_MSG');	    
	
		    }
		    else if($status == 'Delivered')
		    {
		    	$orderItemList = JModelLegacy::getInstance('orderItem','enmasseModel')->listByOrderId($orderData->id);
		    	//update paid_amount of coupon to 100%
		    	$model->updateToFullPaid($orderData->id);
		    	
		    	JModelLegacy::getInstance("orderDeliverer", 'EnmasseModel')->updateStatus($orderData->id, "Delivered");
		    	
		   		foreach($orderItemList as $orderItem)
				{
					// update deal quality sold
					//JModelLegacy::getInstance('deal', 'enmasseModel')->addQtySold($orderItem->pdt_id, $orderItem->qty);
					
					EnmasseHelper::orderItemDelivered($orderItem);
					sleep(1);
				}
				$msg = JTEXT::_('SAVE_SUCCESS_MSG_AND_SEND_DELIVERY');
		    }
		    
		    if($partial)
		    {
		    	$this->setRedirect('index.php?option=com_enmasse&controller=partialOrder', $msg);
		    }else
		    {
		    	$this->setRedirect('index.php?option=com_enmasse&controller=order', $msg);
		    }
			
		}
	}
	function apply()
	{
		$status = JRequest::getVar( 'status', '', 'post');
		$partial = JRequest::getVar('partial', 0, 'method', 'int');
		$orderData = new JObject();
		$orderData->id 				= 	JRequest::getInt( 'id', '', 'post');
		$orderData->description 		= 	JRequest::getVar( 'description', '', 'post', 'text', JREQUEST_ALLOWRAW );
		//
		$orderData->buyerid 		= 	JRequest::getInt( 'buyerid', '', 'post');
		
		$model = JModelLegacy::getInstance('order','enmasseModel');
		
		if(empty($status))//admin dont update order status, so we just save order information
		{
			if ($model->store($orderData))
			{
				$msg = JText::_('SAVE_SUCCESS_MSG');
				$this->setRedirect('index.php?option=com_enmasse&controller=order&task=edit&orderId='.$orderData->id, $msg);
			}
			else
			{
				$msg = JText::_('SAVE_ERROR_MSG') .": " . $model->getError();
				$this->setRedirect('index.php?option=com_enmasse&controller=order&task=edit&orderId='.$orderData->id, $msg);
			}
		}
	}
        
        function ajaxGetLastDeal(){
            $dealName = JRequest::getVar( 'deal_name');
            $model = JModelLegacy::getInstance('order','enmasseModel');
            $rowsOrder = $model->get10LastestOrder($dealName);
            $rs = "<table class='adminlist  table table-striped' width='100%'>
                    <thead>
                        <tr>
                            <th width='65'>".JText::_('DEAL_ID')."</th>
                            <th width='120'>".JText::_('Buyer Name')."</th>
                            <th width='150'>".JText::_('Buyer Email')."</th>
                            <th width='120'>".JText::_('Delivery Name')."</th>
                            <th width='150'>".JText::_('Delivery Email')."</th>
                            <th width='129'>".JText::_('Date Purchased')."</th>
                            <th width='100'>".JText::_('ORDER_QUANTITY')."</th>
                            <th width='90'>".JText::_('ORDER_TOTAL_PAID')." </th>
                            <th>".JText::_('ORDER_STATUS') ."</th>
                        </tr>
                    </thead>";
            for ($i = 0; $i < count($rowsOrder); $i++) {
                $k = $i % 2;
                $row = $rowsOrder[$i];
                $arrBuyer = json_decode($row->buyer_detail);
                $arrDeliver = json_decode($row->delivery_detail);
                $rs .= "<tr class='row$k'>
                                <td >$row->id</td>
                                <td >$arrBuyer->name</td>
                                <td >$arrBuyer->email</td>
                                <td >$arrDeliver->name</td>
                                <td >$arrDeliver->email</td>
                                <td >$row->created_at</td>
                                <td >$row->qty</td>
                                <td >$row->paid_amount</td>
                                <td >$row->status</td>
                        </tr>";           
             }
             $rs .= "</table>";
            echo $rs;
            exit();
        }

}
