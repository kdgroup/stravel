<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com 
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 
if(isset($this->insurance))
{
    $row = $this->insurance;
}
else
{
    $row->id = '';
    $row->insurance_name = '';
    
    $row->published = 1;
    $row->adult_price = '';  
    $row->child_price = '';  
    $row->adult_offer_price = '';  
	  $row->child_offer_price = '';  
    $row->created = '';
   
}

$option = 'com_enmasse';
JHTML::_('behavior.tooltip');

$version = new JVersion;
$joomla = $version->getShortVersion();
if(substr($joomla,0,3) >= 1.6){
?>
    <script language="javascript" type="text/javascript">
        <!--
        Joomla.submitbutton = function(pressbutton)
<?php
}else{
?>
    <script language="javascript" type="text/javascript">
        <!--
        submitbutton = function(pressbutton)
<?php
}
?>
        {
            var form = document.adminForm;
            if (pressbutton == 'cancel')
            {
                submitform( pressbutton );
                return;
            }
            sName = jQuery.trim(form.insurance_name.value.replace(/(<.*?>)/ig,""));
            // do field validation
            if (sName == "")
            {
                alert( "<?php echo JText::_( 'INVALID_NAME', true ); ?>" );
            }
            else
            {
              console.log(1)
            	 jQuery.post("index.php?option=com_enmasse&tmpl=component&controller=insurance&task=checkDuplicatedinsurance", { cityName: sName },function(data) {
                 	   if(data == 'true' && sName.toLowerCase() != form.tempName.value.toLowerCase()){
                 		  alert("<?php echo JText::_('INSURANCE_NAME_DUPLICATED', true); ?>");
                      	   }
                 	   else
                 	   {
                 		  submitform( pressbutton );
                     	}
               	   
                  });
            }
        }        
        </script>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="width-100 fltrt">
<fieldset class="adminform"><legend><?php echo JText::_('DETAIL')?></legend>
<table class="admintable" style="width: 100%">
	<tr>
		<td width="170" align="right" class="key" ><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_INSURANCE_NAME'), JTEXT::_(''), 
                    '', JTEXT::_('Insurance Name') . ' *');?></td>
		<td><input class="text_area" type="text" name="insurance_name" id="insurance_name"
			size="50" maxlength="250" value="<?php echo htmlentities($row->insurance_name, ENT_QUOTES,"UTF-8");?>" />
			<input type="hidden"  name="tempName" value="<?php echo htmlentities($row->insurance_name, ENT_QUOTES,"UTF-8");?>" />
			</td>
	</tr>

	

	<tr>
		<td width="170" align="right" class="key"><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_INSURANCE_PUBLISH'), JTEXT::_('TOOLTIP_INSURANCE_PUBLISH_TITLE'), 
                    '', JTEXT::_('PUBLISHED'));?></td>
		<td><?php
		if ($row->published == null)
		{
			echo JHTML::_('select.booleanlist', 'published',
                          'class="inputbox"', 1);
		}
		else
		{
		echo JHTML::_('select.booleanlist', 'published',
                          'class="inputbox"', $row->published);
		}
		?></td>
	</tr>
	<tr>
    <td width="170" align="right" class="key" ><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_ADULT_PRICE'), JTEXT::_(''), 
                    '', JTEXT::_('Adult Price') . ' *');?></td>
    <td><input  type="text" name="adult_price" id="adult_price"
      size="50" maxlength="250" value="<?php echo htmlentities($row->adult_price, ENT_QUOTES,"UTF-8");?>" />
      
      </td>
  </tr>
  <tr>
    <td width="170" align="right" class="key" ><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_ADULT_PRICE'), JTEXT::_(''), 
                    '', JTEXT::_('Children Price') . ' *');?></td>
    <td><input  type="text" name="child_price" id="child_price"
      size="50" maxlength="250" value="<?php echo htmlentities($row->child_price, ENT_QUOTES,"UTF-8");?>" />
      
      </td>
  </tr>
  <tr>
    <td width="170" align="right" class="key" ><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_ADULT_PRICE'), JTEXT::_(''), 
                    '', JTEXT::_('Adult Offer Price') . ' *');?></td>
    <td><input type="text" name="adult_offer_price" id="adult_offer_price"
      size="50" maxlength="250" value="<?php echo htmlentities($row->adult_offer_price, ENT_QUOTES,"UTF-8");?>" />
      
      </td>
  </tr>
  <tr>
    <td width="170" align="right" class="key" ><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_ADULT_PRICE'), JTEXT::_(''), 
                    '', JTEXT::_('Chilren Offer Price') . ' *');?></td>
    <td><input type="text" name="child_offer_price" id="child_offer_price"
      size="50" maxlength="250" value="<?php echo htmlentities($row->child_offer_price, ENT_QUOTES,"UTF-8");?>" />
      
      </td>
  </tr>
	
	<tr>
		<td width="170" align="right" class="key"><span><?php echo JText::_('Created')?></span></td>
		<td ><?php echo DatetimeWrapper::getDisplayDatetime($row->created); ?> </td>
	</tr>
	
</table>
</fieldset>
<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="controller" value="insurance" /> 
<input type="hidden" name="task" value="" />
</div>
</form>