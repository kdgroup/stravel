<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com 
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 
include_once JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."views".DS."headerMenu".DS
        ."menuDeal.php";
$rows = $this->insuranceList;
$option = 'com_enmasse';

JHTML::_( 'behavior.modal' );
$filter = $this->filter;

$publishedJOptList = array();
array_push($publishedJOptList, JHTML::_('select.option', null, JText::_('') ));
array_push($publishedJOptList, JHTML::_('select.option', 1, JText::_('PUBLISHED') ));
array_push($publishedJOptList, JHTML::_('select.option', 0, JText::_('NOT_PUBLISHED') ));

?>
<div>
    <form action="index.php">
    <table>
            <tr>
                    <td>
                            <b><?php echo JText::_('Insurance Name')?>: </b>
                    </td>
                    <td>
                            <input type="text" name="filter[name]" value="<?php echo $filter['name']; ?>"  placeholder=""/>
                    </td>
                    <td>
                            <b><?php echo JText::_(' Published')?>: </b>
                    </td>
                    <td>
                             <?php echo JHTML::_('select.genericList', $publishedJOptList, 'filter[published]', null , 'value', 'text', $filter['published']);?>
                    </td>
                    <td>
                            <input class="btn" style="margin:7px 0px;" type="submit" value="<?php echo JText::_('Search')?>" />
                            <input class="btn" type="button" value="<?php echo JText::_('Reset');?>" onClick="location.href='index.php?option=com_enmasse&controller=insurance'" />
                    </td>
            </tr>
    </table>
    <input type="hidden" name="controller" value="insurance" />
    <input type="hidden" name="option" value="com_enmasse" />
</form>
<form action="index.php" method="post" name="adminForm"  id="adminForm">
<table class="adminlist  table table-striped">
	<thead>
		<tr>
			<th width="5">
				<input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
			</th>
			<th width=""><?php  echo JHTML::_( 'grid.sort', JText::_('NAME'), 'insurance_name', $this->order['order_dir'], $this->order['order']); ?></th>
			
			<th width="5" nowrap="nowrap"><?php echo JText::_('PUBLISHED')?></th>
			
			<th ><?php  echo JHTML::_( 'grid.sort', JText::_('CREATED'), 'created', $this->order['order_dir'], $this->order['order']); ?></th>
			<th ><?php  echo JHTML::_( 'grid.sort', JText::_('Adult Price'), 'adult_price', $this->order['order_dir'], $this->order['order']); ?></th>
			<th ><?php  echo JHTML::_( 'grid.sort', JText::_('Children Price'), 'child_price', $this->order['order_dir'], $this->order['order']); ?></th>
			<th ><?php  echo JHTML::_( 'grid.sort', JText::_('Adult Offer Price'), 'adult_offer_price', $this->order['order_dir'], $this->order['order']); ?></th>
			<th ><?php  echo JHTML::_( 'grid.sort', JText::_('Children Offer Price'), 'child_offer_price', $this->order['order_dir'], $this->order['order']); ?></th>
			
			<th width="10">Id</th>
		</tr>
	</thead>
	<?php
	for ($i=0; $i < count( $rows ); $i++)
	{
		$k = $i % 2;
		$row = &$rows[$i];
		$checked = JHTML::_('grid.id', $i, $row->id );
		$published = JHTML::_('grid.published', $row, $i );
		$hot = JModelLegacy::getInstance('insurance','enmasseModel')->hot($row->hot, $i, $img1 = 'tick.png', $img0 = 'publish_x.png', $prefix='');
		$link =  JRoute::_('index.php?option=' . $option .'&controller=insurance'.'&task=edit&cid[]='. $row->id) ;
	?>
	<tr class="<?php echo "row$k"; ?>">
		<td><?php echo $checked; ?></td>
		<td><a href="<?php echo $link?>"><?php echo $row->insurance_name; ?></a></td>
		
		<td align="center"><?php echo $published;?></td>
		
		<td><?php echo DatetimeWrapper::getDisplayDatetime($row->created); ?></td>
		<td><a href="<?php echo $link?>"><?php echo $row->adult_price; ?></a></td>
		<td><a href="<?php echo $link?>"><?php echo $row->child_price; ?></a></td>
		<td><a href="<?php echo $link?>"><?php echo $row->adult_offer_price; ?></a></td>
		<td><a href="<?php echo $link?>"><?php echo $row->child_offer_price; ?></a></td>
		
		
		<td><?php echo $row->id; ?></td>
	</tr>
	<?php
	} 
	?>
	<tfoot>
    <tr>
      <td colspan="9"><?php echo $this->pagination->getListFooter(); ?></td>
    </tr>
  </tfoot>
</table>
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="controller" value="insurance" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $this->order['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $this->order['order_dir']; ?>" />
</form>
</div>
