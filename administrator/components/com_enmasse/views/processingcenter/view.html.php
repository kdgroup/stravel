<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."toolbar.enmasse.html.php");

class EnmasseViewProcessingcenter extends JViewLegacy
{
	function display($tpl = null)
	{
                $oProcessingCenter = JModelLegacy::getInstance('processingcenter','enmasseModel');
                $oSetting = JModelLegacy::getInstance('setting','enmasseModel');
                $userGroupId = $oSetting->getSetting(1);
                if($userGroupId->processing_center_group=="0"){
                    $userGroupId = null;
                }else{
                    $userGroupId = $userGroupId->processing_center_group;
                }
                $listUserInCharge =  $oProcessingCenter->getUserInCharge($userGroupId);
		$task = JRequest::getWord('task');
		if($task == 'edit')
		{
                    TOOLBAR_enmasse::_SMENU();
			TOOLBAR_enmasse::_PROCESSINGCENTER_NEW();
			$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
                        //$row->user_in_charge_name = JFactory::getUser($row->user_in_charge)->name;
                        $row = $oProcessingCenter->getById($cid[0]);
                        $this->assignRef( 'listUserInCharge', $listUserInCharge );
			$this->assignRef( 'processingcenter', $row );
		}
		elseif($task == 'add')
		{
                        TOOLBAR_enmasse::_SMENU();
			TOOLBAR_enmasse::_PROCESSINGCENTER_NEW();
                        $this->assignRef( 'listUserInCharge', $listUserInCharge );
		}		
		else
		{			 
			/// load pagination
			$pagination =& $this->get('Pagination');
			$state =& $this->get( 'state' );
			// get order values
			$order['order_dir'] = $state->get( 'filter_order_dir' );
                        $order['order']     = $state->get( 'filter_order' );
                        $filter = JRequest::getVar('filter');
                        $filter['name'] = isset($filter['name']) ? $filter['name'] : '';
                        $filter['published'] = isset($filter['published']) ? $filter['published'] : '';
                        $this->filter = $filter;
			TOOLBAR_enmasse::_SMENU();
                        TOOLBAR_enmasse::_PROCESSINGCENTER();
			
			$centerList = JModelLegacy::getInstance('processingcenter','enmasseModel')->search($filter['name'],$filter['published']);
                        foreach($centerList as $center){
                            $center->user_in_charge_name = JFactory::getUser($center->user_in_charge)->name;
                        }
			$this->assignRef( 'centerList', $centerList );
			$this->assignRef('pagination', $pagination);
			$this->assignRef( 'order', $order );
			
		}
		parent::display($tpl);
	}

}
?>