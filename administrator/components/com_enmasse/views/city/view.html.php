<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com 
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."toolbar.enmasse.html.php");

class EnmasseViewCity extends JViewLegacy
{
	function display($tpl = null)
	{
        $this->form = $this->get('Form');
		$task = JRequest::getWord('task');
		if($task == 'edit')
		{
            TOOLBAR_enmasse::_SMENU();
			$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
			TOOLBAR_enmasse::_CITY_NEW();
			
			$city = JModelLegacy::getInstance('city','enmasseModel')->getById($cid[0]);
			$this->assignRef('city', $city);

		}
		elseif($task == 'add')
		{
            TOOLBAR_enmasse::_SMENU();
			TOOLBAR_enmasse::_CITY_NEW();
		}
		else
		{
			TOOLBAR_enmasse::_SMENU();
			$nNumberOfCities = JModelLegacy::getInstance('city','enmasseModel')->countAll();
			if($nNumberOfCities==0)
			{
				TOOLBAR_enmasse::_CITY_EMPTY();
			}
			else
			{
				TOOLBAR_enmasse::_CITY();
			}
			/// load pagination
			$pagination = $this->get('Pagination');
		
			$state = $this->get( 'state' );
			// get order values
			$order['order_dir'] = $state->get( 'filter_order_dir' );
                        $order['order']     = $state->get( 'filter_order' );
                        $filter = JRequest::getVar('filter');
                        $filter['name'] = isset($filter['name']) ? $filter['name'] : '';
                        $filter['published'] = isset($filter['published']) ? $filter['published'] : '';
                        $this->filter = $filter;
                        
			$cityList = JModelLegacy::getInstance('city','enmasseModel')->search($filter['name'],$filter['published']);
			$this->assignRef( 'cityList', $cityList);
			$this->assignRef('pagination', $pagination);
			$this->assignRef( 'order', $order );
		}
			
		parent::display($tpl);
	}

}
?>