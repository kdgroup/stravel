<?php

/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "toolbar.enmasse.html.php");
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "helpers" . DS . "DatetimeWrapper.class.php");
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "helpers" . DS . "EnmasseHelper.class.php");

class EnmasseViewReport extends JViewLegacy {

    function display($tpl = null) {
        TOOLBAR_enmasse::_SMENU();
        TOOLBAR_enmasse::_REPORT();

        $task = JRequest::getVar('task');

        $dealList = JModelLegacy::getInstance('deal', 'enmasseModel')->listConfirmed();
        $this->assignRef('dealList', $dealList);

        $filter = JRequest::getVar('filter');
        $this->assignRef('filter', $filter);

        $dealId = $filter['deal_id'];
        $from_at = "";
        $from_to = "";
        $type = "";
        if (!empty($dealId)) {
            if($dealId=='0'){
                $orderItemList =array();
                die;
                foreach ($dealList as $key => $value) {
                    $dealId = $value->id;
                    $deal = JModelLegacy::getInstance('deal', 'enmasseModel')->getById($dealId);
                    $this->assignRef('deal', $deal);
                    
                    $from_at = JRequest::getVar('from_at');
                    $from_to = JRequest::getVar('from_to');
                    $type = JRequest::getVar('type');
                    $orderItemListall = JModelLegacy::getInstance('orderItem', 'enmasseModel')->listByPdtIdAndStatusAndDate($dealId, "Delivered",$from_at,$from_to);

                    for ($count = 0; $count < count($orderItemListall); $count++) {
                        $orderItemListall[$count]->invtyList = JModelLegacy::getInstance('invty', 'enmasseModel')->listByOrderItemIdAndStatus($orderItemList[$count]->id,$type);
                        $orderItemListall[$count]->order = JModelLegacy::getInstance('order', 'enmasseModel')->getById($orderItemList[$count]->order_id);
                        $orderI->invtyList = JModelLegacy::getInstance('invty', 'enmasseModel')->listByOrderItemIdAndStatus($orderItemList[$count]->id,$type);
                        $orderI->order = JModelLegacy::getInstance('order', 'enmasseModel')->getById($orderItemList[$count]->order_id);
                        $orderItemList[] = $orderI;
                    }
                }
            }else{
                $deal = JModelLegacy::getInstance('deal', 'enmasseModel')->getById($dealId);
                    $this->assignRef('deal', $deal);
                    
                    $from_at = JRequest::getVar('from_at');
                    $from_to = JRequest::getVar('from_to');
                    $type = JRequest::getVar('type');
                    $orderItemList = JModelLegacy::getInstance('orderItem', 'enmasseModel')->listByPdtIdAndStatusAndDate($dealId, "Delivered",$from_at,$from_to);

                    for ($count = 0; $count < count($orderItemList); $count++) {
                        $orderItemList[$count]->invtyList = JModelLegacy::getInstance('invty', 'enmasseModel')->listByOrderItemIdAndStatus($orderItemList[$count]->id,$type);
                        $orderItemList[$count]->order = JModelLegacy::getInstance('order', 'enmasseModel')->getById($orderItemList[$count]->order_id);
                    }
            }
           
            $this->assignRef('orderItemList', $orderItemList);
        } else {
            $orderItemList = array();
            $this->assignRef('orderItemList', $orderItemList);
        }
        /// load pagination
        $pagination = JModelLegacy::getInstance('orderItem', 'enmasseModel')->getPagination();
        $this->assignRef('pagination', $pagination);
        parent::display($tpl);
    }

}

?>