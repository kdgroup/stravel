<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."toolbar.enmasse.html.php");

class EnmasseViewMerchant extends JViewLegacy
{
	function display($tpl = null)
	{
		$task = JRequest::getWord('task');
		if($task == 'edit' || $task == 'add')
		{
                    TOOLBAR_enmasse::_SMENU();
			JRequest::setVar('hidemainmenu', true);
			TOOLBAR_enmasse::_MERCHANT_NEW();
			
			$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
			$merchant = JTable::getInstance('merchant','Table');
			
			if(!$merchant->load($cid['0']))
			{
				JError::raiseError( 500, $merchant->getError() );
				return;
			}
			
			//populate pre-value was save in session if the data exist
			$data = JFactory::getApplication()->getUserState('merchant.add.data');
			if(!empty($data))
			{$merchant->bind($data);}
			
			$this->merchant = $merchant;
			
			$salesPersonList 	= JModelLegacy::getInstance('salesPerson','enmasseModel')->listAllPublished();
			$this->salesPersonList =  $salesPersonList ;
			
                        //get form user
                        JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
                        $form = JForm::getInstance('com_enmasse.merchant', 'merchant');
                        $user_id = '';
                        if($merchant->user_name){
                            $user = EnmasseHelper::getUserByName($merchant->user_name);
                            $user_id = $user->id;
                        }
                        
                        $form->setValue('user_id',null,$user_id);
                        $form->setValue('suppervisor_id',null,$merchant->suppervisor_id);
                        $this->form = $form;
		}
		else
		{
			TOOLBAR_enmasse::_SMENU();
			$nNumberOfMerchants = JModelLegacy::getInstance('merchant','enmasseModel')->countAll();
			if($nNumberOfMerchants==0)
			{
				TOOLBAR_enmasse::_MERCHANT_EMPTY();
			}
			else
			{
				TOOLBAR_enmasse::_MERCHANT();
			}
			
			$filter = JRequest::getVar('filter');
			
			$merchantList = JModelLegacy::getInstance('merchant','enmasseModel')->search($filter['name']);
			/// load pagination
			$pagination = $this->get( 'pagination' );
			$state = $this->get( 'state' );
			// get order values
			$order['order_dir'] = $state->get( 'filter_order_dir' );
            $order['order']     = $state->get( 'filter_order' );
            
			for($count=0; $count < count($merchantList); $count++)
			{
				$salesPerson = JModelLegacy::getInstance('salesPerson','enmasseModel')->getById($merchantList[$count]->sales_person_id);
				$merchantList[$count]->sales_person_name = $salesPerson->name;
			}
			$this->assignRef( 'filter', $filter );
			$this->assignRef( 'merchantList', $merchantList );
			$this->assignRef('pagination', $pagination);
			$this->assignRef( 'order', $order );
		}
		parent::display($tpl);
	}

}
?>