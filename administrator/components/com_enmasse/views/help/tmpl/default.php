<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$sWebsite = '<a href="http://www.matamko.com/" target="_blank">www.matamko.com</a>'; 
?>
<br>
<div>
    <center>
        <table>
            <tr>
                <td style="vertical-align: top;padding-right: 10px;"><img src="components/com_enmasse/images/help_icon.png" /></td>
                <td>
                    <b><?php echo JTEXT::_('HELP_LINE_1')?></b><br/><br>
                    <?php echo sprintf(JTEXT::_('HELP_LINE_2'), $sWebsite); ?>
                </td>
            </tr>
        </table>
    </center>
</div>