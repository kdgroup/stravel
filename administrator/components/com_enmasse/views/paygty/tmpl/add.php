<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$option = 'com_enmasse';
$version = new JVersion;
$joomla = $version->getShortVersion();
if(substr($joomla,0,3) >= '1.6'){
?>
    <script language="javascript" type="text/javascript">
        <!--
        Joomla.submitbutton = function(pressbutton)
<?php
}else{
?>
    <script language="javascript" type="text/javascript">
        <!--
        submitbutton = function(pressbutton)
<?php
}
?>
        {
            var form = document.adminForm;
            if (pressbutton == 'cancel')
            {
                submitform( pressbutton );
                return;
            }
            // do field validation
            if (form.name.value == "")
            {
                alert( "<?php echo JText::_( 'FILL_IN_MERCHANT_PAYMENT_GATEWAY_NAME', true ); ?>" );
            }
            else
            {
                submitform( pressbutton );
            }
        }
        //-->
        </script>
<script>

function addRow(nTableId)
{
	var txtCountAttribute = document.getElementById('count');
	var nIdOfNewAttribute = parseInt(txtCountAttribute.value);
	nIdOfNewAttribute = nIdOfNewAttribute + 1;
	txtCountAttribute.value = nIdOfNewAttribute;
	var attributeTable = document.getElementById(nTableId);
	var tblTr = document.createElement("tr");
	var tblTd1 = document.createElement("td");
	var txtAttributeName=document.createElement("input");
	txtAttributeName.type="text";
	txtAttributeName.name="attribute_name[" + nIdOfNewAttribute + "]";
	txtAttributeName.id="attribute_name[" + nIdOfNewAttribute + "]";
	txtAttributeName.size="50";
	txtAttributeName.maxlength="250";
	tblTd1.appendChild(txtAttributeName);
	var tblTd2 = document.createElement("td");
	var txtAttributeValue=document.createElement("input");
	txtAttributeValue.type="text";
	txtAttributeValue.name="attribute_value[" + nIdOfNewAttribute + "]";
	txtAttributeValue.id="attribute_value[" + nIdOfNewAttribute + "]";
	txtAttributeValue.size="50";
	txtAttributeValue.maxlength="250";
	tblTd2.appendChild(txtAttributeValue);
	tblTr.appendChild(tblTd1);
	tblTr.appendChild(tblTd2);
	attributeTable.appendChild(tblTr);
}

function addField(tableId){

	var payTable = document.getElementById(tableId);
	var rowCounter = payTable.getElementsByTagName('tr').length;
	var newRow = payTable.insertRow(rowCounter);
	var label = newRow.insertCell(0);
	var name = newRow.insertCell(1);
	var type = newRow.insertCell(2);
	var require = newRow.insertCell(3);
	var validate = newRow.insertCell(4);
	var actionBtn = newRow.insertCell(5);
	

		label.innerHTML = '<input type="text" value="" maxlength="250" size="50" name="payDetailFields[label][]" class="text_area pay_fields">';
		name.innerHTML = '<input type="text" value="" maxlength="250" size="50" name="payDetailFields[name][]" class="text_area pay_fields">';
		type.innerHTML = '<select name="payDetailFields[type][]" class="text_area pay_fields"><option value="">- Select one -</option><option value="text">Text</option><option value="datepicker">Date</option><option value="file" >File</option></select>';
		require.innerHTML = '<input type="checkbox" name="payDetailFields[require][]"  value="1"/>';
		validate.innerHTML = '<select name="payDetailFields[validate][]" class="text_area pay_fields"><option value="">No Validation</option><option value="numeric">Numeric</option><option value="email" >Email Address</option><option value="phone" >Phone Number (123-456-7890)</option><option value="credit" >Credit Card (AMEX/Diners/Dicover/Master/Visa)</option></select>';
		actionBtn.innerHTML = "<td><a onclick=\"addField('payFieldTable')\" href=\"javascript:void(0)\" class=\"btnAdd btn\">Add</a></td>";

	addDelBtn();		
} 

function addDelBtn()
{
	jQuery('.btnAdd').click(function(){
		jQuery(this).removeClass('btnAdd').addClass('btnDel').text('Delete').removeAttr('onclick');
		jQuery('.btnDel').click(function(){
			jQuery(this).closest('tr').remove();

		});			
	});	
	
}

jQuery(document).ready(function(){
	//jQuery('#adminForm').reset();
	document.getElementById('adminForm').reset();
	
	jQuery('.btnAdd').click(function(){
		jQuery(this).text('Delete');
	});
	
	jQuery('#class_name').change(function(){
		var gateway = jQuery(this).val();
		jQuery('#gatewayNote, #detailNote').empty();
		getNoteByGateway(gateway);
	});
	
	getNoteByGateway('agd') // Default gateway is authorizenet
	
	function getNoteByGateway(gateway){
		jQuery.ajax({
			type: "GET",
			url: "<?php echo JURI::root(0)?>/components/com_enmasse/helpers/payGty/"+gateway+"/note.xml",
			dataType: "xml",
			success: function(note) {
				var gatewayNote = '';
				var payDetailNote = '';
				
				var attrLength = jQuery(note).find('Gateway').find('AttributeName').length;
				var labelLength = jQuery(note).find('PaymentDetail').find('Label').length;
				
				var i = j = 0;
				jQuery(note).find('Gateway').each(function(){

					jQuery(this).find('AttributeName').each(function(){
						gatewayNote += jQuery(this).text();
						
						if(++i == attrLength)
							gatewayNote += '.';
						else
							gatewayNote += ', ';						
					});


				});
				
				jQuery(note).find('PaymentDetail').each(function(){
					jQuery(this).find('Label').each(function(){
						payDetailNote += jQuery(this).text();
						
						if(++j == labelLength)
							payDetailNote += '.';
						else
							payDetailNote += ', ';						
					});						
				});
				
				if(gatewayNote != '')
					jQuery('#gatewayNote').html('<b>Note:</b> ' + gatewayNote);
				if(payDetailNote != '')  
					jQuery('#detailNote').html('<b>Note:</b> ' + payDetailNote);
			},
			error: function(){
			}
		});			
	}

	
	
})


</script>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="width-100 fltrt">
<fieldset class="adminform">
<legend><?php echo JText::_('PAY_DETAIL')?></legend>
<table class="admintable">
	<tr>
		<td width="100" align="right" class="key"><?php echo Jtext::_('PAY_NAME');?></td>
		<td><input class="text_area" type="text" name="name" id="name"
			size="50" maxlength="250" value="" /></td>
	</tr>
	<tr>
		<td width="100" align="right" class="key"><?php echo Jtext::_('PUBLISHED');?></td>
		<td><?php
			echo JHTML::_('select.booleanlist', 'published',
                          'class="inputbox"');
		?></td>
	</tr>
	<tr>
		<td width="100" align="right" class="key">Payment type</td>
		<td>
			
			<label class="radio">
			
			<input name="payment_type" value="0" checked="checked" class="inputbox selectPayType" type="radio">Offline
			</label>
			<label class="radio">
			
			<input name="payment_type" value="1" class="inputbox selectPayType" type="radio">Direct
			</label>
		</td>
	</tr>	
	<tr>
		<td width="100" align="right" class="key">Gateway</td>
		<td>
		<?php
			$arr = array();
			$subscriptionList = '';
			foreach (EnmasseHelper::gatewayList() as $key=>$value)
			{
                if($value != 'index.html'){
					array_push($arr, JHTML::_('select.option', $value, JText::_($value) ));
					$gatewayName = $value;
                }
			}
			echo JHTML::_('select.genericlist', $arr, 'class_name',  '', 'value', 'text', '');
		?>			
			
		</td>
	</tr>		
</table>
</fieldset>
<div id="paymentDetail">
	<legend><?php echo JText::_('Payment detail')?></legend>
	<div class="wrap-fields">
		<table id="payFieldTable">
		<tbody >
			<tr>
				<td colspan="6" id="detailNote"></td>
			</tr>		
			<tr>
				<th>Label</th>
				<th>Name</th>
				<th>Field type</th>
				<th>Require</th>
				<th>Validate</th>
				<th></th>
			</tr>

			<tr>
				<td><input type="text" value="" maxlength="250" size="50" name="payDetailFields[label][]" class="text_area pay_fields"></td>
				<td><input type="text" value="" maxlength="250" size="50" name="payDetailFields[name][]" class="text_area pay_fields"></td>
				<td>	
					<select name="payDetailFields[type][]" class="text_area pay_fields">
						<option value="">- Select one -</option>
						<option value="text" >Text</option>
						<option value="datepicker" >Date</option>
						<option value="file" >File</option>
					</select>	
				</td>
				<td><input type="checkbox" name="payDetailFields[require][]" value="1"/></td>
				<td>
					<select name="payDetailFields[validate][]" class="text_area pay_fields">
						<option value="">No Validation</option>
						<option value="numeric">Numeric</option>
						<option value="email" >Email Address</option>
						<option value="phone" >Phone Number (123-456-7890)</option>
						<option value="credit" >Credit Card (AMEX/Diners/Dicover/Master/Visa)</option>
					</select>				
				
				</td>
				<td><a onclick="addField('payFieldTable'); " href="javascript:void(0)" class="btnAdd btn">Add</a></td>
			</tr>			

		</table>

	</div>
	
</div>
<fieldset class="adminform">
<legend><?php echo JText::_('GATEWAY_SETTING');?></legend>
<input type="hidden" id="count" value="1"/>
<table class="admintable">
<tbody id="admintable">
	<tr>
		<td colspan="6" id="gatewayNote"></td>
	</tr>
	<tr>
		<th><?php echo JText::_('ATTRIBUTE_NAME'); ?></th>
		<th><?php echo JText::_('ATTRIBUTE_VALUE'); ?></th>
	</tr>
	<tr>
		<td><input class="text_area" type="text" name="attribute_name[1]" 
           id="attribute_name[1]" size="50" maxlength="250" 
           value="" /></td>
		<td><input class="text_area" type="text" name="attribute_value[1]" 
           id="attribute_value[1]" size="50" maxlength="250" 
           value="" /></td>
	</tr>
</tbody>	
</table>
<a href="#" onclick="addRow('admintable'); return false;"><?php echo JText::_('ADD_ATTRIBUTE'); ?></a><br/>
<?php echo JText::_('ATTRIBUTE_NOTICE'); ?>
</fieldset>
<input type="hidden" name="id" value="" />
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="controller" value="payGty" />
<input type="hidden" name="task" value="" />
</div>
</form>