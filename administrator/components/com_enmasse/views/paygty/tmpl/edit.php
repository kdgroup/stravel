<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$row 	= $this->payGty;
$option = 'com_enmasse';
$version = new JVersion;
$joomla = $version->getShortVersion();

$pay_detail_fields = json_decode($row->pay_detail_fields, true);

if(substr($joomla,0,3) >= '1.6'){
?>
    <script language="javascript" type="text/javascript">
        <!--
        Joomla.submitbutton = function(pressbutton)
<?php
}else{
?>
    <script language="javascript" type="text/javascript">
        <!--
        submitbutton = function(pressbutton)
<?php
}
?>
        {
            var form = document.adminForm;
            if (pressbutton == 'cancel')
            {
                submitform( pressbutton );
                return;
            }
            // do field validation
            if (form.name.value == "")
            {
                alert( "<?php echo JText::_( 'FILL_IN_MERCHANT_PAYMENT_GATEWAY_NAME', true ); ?>" );
            }
            else
            {
                submitform( pressbutton );
            }
        }
        //-->
        </script>
<script>
function addRow(nTableId)
{
	var txtCountAttribute = document.getElementById('count');
	var nIdOfNewAttribute = parseInt(txtCountAttribute.value);
	nIdOfNewAttribute = nIdOfNewAttribute + 1;
	txtCountAttribute.value = nIdOfNewAttribute;
	var attributeTable = document.getElementById(nTableId);
	var tblTr = document.createElement("tr");
	var tblTd1 = document.createElement("td");
	var txtAttributeName=document.createElement("input");
	txtAttributeName.type="text";
	txtAttributeName.name="attribute_name[" + nIdOfNewAttribute + "]";
	txtAttributeName.id="attribute_name[" + nIdOfNewAttribute + "]";
	txtAttributeName.size="50";
	txtAttributeName.maxlength="250";
	tblTd1.appendChild(txtAttributeName);
	var tblTd2 = document.createElement("td");
	var txtAttributeValue=document.createElement("input");
	txtAttributeValue.type="text";
	txtAttributeValue.name="attribute_value[" + nIdOfNewAttribute + "]";
	txtAttributeValue.id="attribute_value[" + nIdOfNewAttribute + "]";
	txtAttributeValue.size="50";
	txtAttributeValue.maxlength="250";
	tblTd2.appendChild(txtAttributeValue);
	tblTr.appendChild(tblTd1);
	tblTr.appendChild(tblTd2);
	attributeTable.appendChild(tblTr);
}	

function addField(tableId){

	var payTable = document.getElementById(tableId);
	var rowCounter = payTable.getElementsByTagName('tr').length;
	var newRow = payTable.insertRow(rowCounter);
	var label = newRow.insertCell(0);
	var name = newRow.insertCell(1);
	var type = newRow.insertCell(2);
	var require = newRow.insertCell(3);
	var validate = newRow.insertCell(4);
	var actionBtn = newRow.insertCell(5);

	label.innerHTML = '<input type="text" value="" maxlength="250" size="50" name="payDetailFields[label][]" class="text_area pay_fields">';
	name.innerHTML = '<input type="text" value="" maxlength="250" size="50" name="payDetailFields[name][]" class="text_area pay_fields">';
	type.innerHTML = '<select name="payDetailFields[type][]" class="text_area pay_fields"><option value="">- Select one -</option><option value="text">Text</option><option value="datepicker">Date</option><option value="file" >File</option></select>';
	require.innerHTML = '<input type="checkbox" name="payDetailFields[require][]"  value="1"/>';
	validate.innerHTML = '<select name="payDetailFields[validate][]" class="text_area pay_fields"><option value="">No Validation</option><option value="numeric">Numeric</option><option value="email" >Email Address</option><option value="phone" >Phone Number (123-456-7890)</option><option value="credit" >Credit Card (AMEX/Diners/Dicover/Master/Visa)</option></select>';
	actionBtn.innerHTML = "<td><a onclick=\"addField('payFieldTable')\" href=\"javascript:void(0)\" class=\"btnAdd btn\">Add</a></td>";

	addDelBtn();
		
} 

function addDelBtn()
{
	jQuery('.btnAdd').click(function(){
		jQuery(this).removeClass('btnAdd').addClass('btnDel').text('Delete').removeAttr('onclick');
		jQuery('.btnDel').click(function(){
			jQuery(this).closest('tr').remove();

		});			
	});	
	jQuery('.btnDel').click(function(){
		jQuery(this).closest('tr').remove();

	});		
	
}

jQuery(document).ready(function(){

	document.getElementById('adminForm').reset();
	addDelBtn();
	
	jQuery('#class_name').change(function(){
		var gateway = jQuery(this).val();
		jQuery('#gatewayNote, #detailNote').empty();
		getNoteByGateway(gateway);
	});
	
	getNoteByGateway(jQuery('#class_name').val()) // Default gateway is authorizenet
	
	function getNoteByGateway(gateway){
		jQuery.ajax({
			type: "GET",
			url: "<?php echo JURI::root(0)?>/components/com_enmasse/helpers/payGty/"+gateway+"/note.xml",
			dataType: "xml",
			success: function(note) {
				var gatewayNote = '';
				var payDetailNote = '';
				
				var attrLength = jQuery(note).find('Gateway').find('AttributeName').length;
				var labelLength = jQuery(note).find('PaymentDetail').find('Label').length;
				
				var i = j = 0;
				jQuery(note).find('Gateway').each(function(){

					jQuery(this).find('AttributeName').each(function(){
						gatewayNote += jQuery(this).text();
						
						if(++i == attrLength)
							gatewayNote += '.';
						else
							gatewayNote += ', ';						
					});


				});
				
				jQuery(note).find('PaymentDetail').each(function(){
					jQuery(this).find('Label').each(function(){
						payDetailNote += jQuery(this).text();
						
						if(++j == labelLength)
							payDetailNote += '.';
						else
							payDetailNote += ', ';						
					});						
				});
				
				if(gatewayNote != '')
					jQuery('#gatewayNote').html('<b>Note:</b> ' + gatewayNote);
				if(payDetailNote != '')  
					jQuery('#detailNote').html('<b>Note:</b> ' + payDetailNote);
			},
			error: function(){
			}
		});			
	}	
		
})
</script>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="width-100 fltrt">
<fieldset class="adminform">
<legend><?php echo JText::_('PAY_DETAIL')?></legend>
<table class="admintable">
	<tr>
		<td width="100" align="right" class="key"><?php echo Jtext::_('PAY_NAME');?></td>
		<td><input class="text_area" type="text" name="name" id="name"
			size="50" maxlength="250" value="<?php echo $row->name;?>" /></td>
		<td rowspan="4" style="vertical-align:top"><?php echo $row->description; ?></td>			
	</tr>	
	<tr>
		<td width="100" align="right" class="key"><?php echo Jtext::_('PUBLISHED');?></td>
		<td><?php
		if ($row->published == null)
		{
			echo JHTML::_('select.booleanlist', 'published',
                          'class="inputbox"', 1);
		}
		else
		{
			echo JHTML::_('select.booleanlist', 'published',
                          'class="inputbox"', $row->published);
		}
		?></td>
		
	</tr>
	
	<tr>
		<td width="100" align="right" class="key">Payment type</td>
		<td>
			
			<label class="radio">
			
			<input name="payment_type" value="0" <?php if($row->payment_type == 0) echo 'checked="checked"'?> class="inputbox selectPayType" type="radio">Offline
			</label>
			<label class="radio">
			
			<input name="payment_type" value="1" <?php if($row->payment_type == 1) echo 'checked="checked"'?> class="inputbox selectPayType" type="radio">Direct
			</label>
		</td>
	</tr>		
	
	<tr>
		<td width="100" align="right" class="key">Gateway</td>
		<td>
		<?php
			$arr = array();
			$subscriptionList = '';
			foreach (EnmasseHelper::gatewayList() as $key=>$value)
			{
                if($value != 'index.html'){
					array_push($arr, JHTML::_('select.option', $value, JText::_($value) ));
					$gatewayName = $value;
                }
			}
			echo JHTML::_('select.genericlist', $arr, 'class_name',  '', 'value', 'text', $row->class_name);
		?>				
		</td>
	</tr>		
	<tr>
		<td width="100" align="right" class="key"><?php echo Jtext::_('CREATED_AT');?></td>
		<td><?php echo DatetimeWrapper::getDisplayDatetime($row->created_at); ?></td>
	</tr>
	<tr>
		<td width="100" align="right" class="key"><?php echo Jtext::_('UPDATED_AT');?></td>
		<td><?php echo DatetimeWrapper::getDisplayDatetime($row->updated_at); ?></td>
	</tr>
</table>
</fieldset>

<div>
	<legend><?php echo JText::_('Payment detail')?></legend>	
	<div class="wrap-fields">
		<table id="payFieldTable">
		<tbody >
			<tr>
				<td colspan="6" id="detailNote"></td>
			</tr>		
			<tr>
				<th>Label</th>
				<th>Name</th>
				<th>Field type</th>
				<th>Require</th>
				<th>Validate</th>
				<th></th>
			</tr>
			<?php 
			if(count($pay_detail_fields) > 0):  // Offline payment
				$count = 0;
				foreach($pay_detail_fields as $field):
				?>
				<tr>
					<td><input type="text" value="<?php echo $field[label]?>" maxlength="250" size="50" name="payDetailFields[label][]" class="text_area pay_fields"></td>
					<td><input type="text" value="<?php echo $field[name]?>" maxlength="250" size="50" name="payDetailFields[name][]" class="text_area pay_fields"></td>
					<td>
						<select name="payDetailFields[type][]" class="text_area pay_fields">
							<option value="">- Select one -</option>
							<option value="text" <?php if($field[type] == 'text') echo 'selected'?>>Text</option>
							<option value="datepicker" <?php if($field[type] == 'datepicker') echo 'selected'?>>Date</option>
							<option value="file" <?php if($field[type] == 'file') echo 'selected'?>>File</option>
						</select>
					</td>
					<td><input type="checkbox" name="payDetailFields[require][]" value="1" <?php if($field['require'] == 1) echo 'checked="true"' ?>/></td>
					<td>
						<select name="payDetailFields[validate][]" class="text_area pay_fields">
							<option value="">No Validation</option>
							<option value="numeric" <?php if($field['validate'] == 'numeric') echo 'selected' ?>>Numeric</option>
							<option value="email" <?php if($field['validate'] == 'email') echo 'selected' ?>>Email Address</option>
							<option value="phone" <?php if($field['validate'] == 'phone') echo 'selected' ?>>Phone Number (123-456-7890)</option>
							<option value="credit" <?php if($field['validate'] == 'credit') echo 'selected' ?>>Credit Card (AMEX/Diners/Dicover/Master/Visa)</option>
						</select>								
					</td>					
				<?php if(++$count == count($pay_detail_fields)):?>
					<td><a onclick="addField('payFieldTable'); " href="javascript:void(0)" class="btnAdd btn">Add</a></td>						
				<?php else: ?>
					<td><a href="javascript:void(0)" class="btnDel btn">Delete</a></td>	
				<?php endif?>
				</tr>		
				<?php
				endforeach;
			else: ?>
			<tr>
				<td><input type="text" value="" maxlength="250" size="50" name="payDetailFields[label][]" class="text_area pay_fields"></td>
				<td><input type="text" value="" maxlength="250" size="50" name="payDetailFields[name][]" class="text_area pay_fields" disabled="true"></td>
				<td>	
					<select name="payDetailFields[type][]" class="text_area pay_fields">
						<option value="">- Select one -</option>
						<option value="text" >Text</option>
						<option value="datepicker" >Date</option>
						<option value="file" >File</option>
					</select>	
				</td>
				<td><input type="checkbox" name="payDetailFields[require][]" value="1"/></td>
				<td>
					<select name="payDetailFields[validate][]" class="text_area pay_fields">
						<option value="">No Validation</option>
						<option value="numeric">Numeric</option>
						<option value="email" >Email Address</option>
						<option value="phone" >Phone Number (123-456-7890)</option>
						<option value="credit" >Credit Card (AMEX/Diners/Dicover/Master/Visa)</option>
					</select>								
				</td>
				<td><a onclick="addField('payFieldTable'); " href="javascript:void(0)" class="btnAdd btn">Add</a></td>
			</tr>				
			<?php endif?>
		</table>

	</div>
	
</div>

<fieldset class="adminform">
<legend><?php echo JText::_('GATEWAY_SETTING');?></legend>
<table class="admintable">
<tbody id="admintable">
		<tr>
			<td colspan="6" id="gatewayNote"></td>
		</tr>
<?php
		$attribute_list 	= explode(",",$row->attributes);
		$attribute_obj 		= json_decode($row->attribute_config);
		if($row->class_name == 'cash' || $row->class_name == 'point')
		{
			$title = $attribute_list[0];
			$title == '' ? $value = '' : $value = $attribute_obj->$title;
		    $editor = JFactory::getEditor();
			echo $editor->display('attribute_config['.$title.']', $value, '800', '300', '40', '3');
	    }
	    else
	    {
			for ($i=0; $i < count($attribute_list); $i++)
			{
				$count = $i + 1;
				$title = $attribute_list[$i];
				$title == '' ? $value = '' : $value = $attribute_obj->$title;
?>	
	<tr>
		<th><?php echo JText::_('ATTRIBUTE_NAME'); ?></th>
		<th><?php echo JText::_('ATTRIBUTE_VALUE'); ?></th>
	</tr>
	<tr>
		<td><input class="text_area" type="text" name="attribute_name[<?php echo $count; ?>]" 
           id="attribute_name[<?php echo $count; ?>]" size="50" maxlength="250" 
           value="<?php echo $title; ?>" /></td>
		<td><input class="text_area" type="text" name="attribute_value[<?php echo $count; ?>]" 
           id="attribute_value[<?php echo $count; ?>]" size="50" maxlength="250" 
           value="<?php echo $value; ?>" /></td>
	</tr>
<?php
			}
	    }
?>	
</tbody>	
</table>
<?php 
if($row->class_name != 'cash' && $row->class_name != 'point')
{
?>

<a href="#" onclick="addRow('admintable'); return false;"><?php echo JText::_('ADD_ATTRIBUTE'); ?></a><br/>
<?php echo JText::_('ATTRIBUTE_NOTICE'); ?>
<?php 
}
?>
<input type="hidden" id="count" value="<?php echo $count; ?>"/>
</fieldset>
<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="controller" value="payGty" />
<input type="hidden" name="task" value="" />
</div>
</form>