<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 
include_once JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."views".DS."headerMenu".DS."menuSystem.php";
$setting = $this->setting;
$row = $this->setting;

$oDefault = new JObject();
$oDefault->id = "";
$oDefault->name = JText::_(" ");

if(count($this->taxList) != 0)
{
	$emptyJOpt = JHTML::_('select.option', '', JText::_('') );
	$taxJOptList = array();
	array_unshift($this->taxList, $oDefault);
}
//add an empty select option for usergroup select
$oDefault = new JObject();
$oDefault->id = "";
$oDefault->name = JText::_("(Not in used)");
array_unshift($this->userGroupList, $oDefault);

$countryJOptList = $this->countryJOptList;

$option = 'com_enmasse';
$custom_field = json_decode($row->dl_custom_field, true);
JHTML::_( 'behavior.modal' );
JHTML::_('behavior.tooltip');
?>

<script language="javascript" type="text/javascript">
        Joomla.submitbutton = function(pressbutton)
        {
            var form = document.adminForm;
            if (pressbutton == 'cancel')
            {
                submitform( pressbutton );
                return;
            }
            // do field validation
            if (form.company_name.value == "")
            {
                alert( "<?php echo JText::_( 'FILL_IN_COMPANY_NAME', true ); ?>" );
            }
            else if (form.address1.value == "")
            {
                alert( "<?php echo JText::_( 'FILL_IN_COMPANY_ADDRESS', true ); ?>" );
            }
            else if (form.city.value == "")
            {
                alert( "<?php echo JText::_( 'FILL_IN_COMPANY_CITY', true ); ?>" );
            }
            else if (form.state.value == "")
            {
                alert( "<?php echo JText::_( 'FILL_IN_COMPANY_STATE', true ); ?>" );
            }
            else if (form.country.value == "")
            {
                alert( "<?php echo JText::_( 'FILL_IN_COMPANY_COUNTRY', true ); ?>" );
            }
            else if (form.postal_code.value == "")
            {
                alert( "<?php echo JText::_( 'FILL_IN_COMPANY_POSTAL_CODE', true ); ?>" );
            }
            else if (isNaN(form.contact_number.value))
            {
                alert( "<?php echo JText::_( 'PHONE_CONTACT_SHOULD_BE_NUM', true ); ?>" );
            }
            else if (isNaN(form.contact_fax.value))
            {
                alert( "<?php echo JText::_( 'FAX_CONTACT_SHOULD_BE_NUM', true ); ?>" );
            }
            else if (!isEmail(form.customer_support_email.value))
            {
                alert( "<?php echo JText::_( 'PLEASE_ENTER_VALID_EMAIL', true ); ?>" );
            }
            else if (form.currency_prefix.value == "" && form.currency_postfix.value == "")
            {
                alert( "<?php echo JText::_( 'FILL_IN_DEFAULT_CURRENCY_PREFIX_POSTFIX', true ); ?>" );
            }
            else if (form.currency_separator.value.length && !form.currency_separator.value.match(/^[ ]$|^[\.]$|^[\,]$/) )
            {
                alert("<?php echo JText::_('INVALID_CURRENCY_THOUSANDS_SEPARATOR')?>");
            }
            else if (form.currency_decimal.value.length && !form.currency_decimal.value.match(/^[0-9]$/) )
            {
                alert("<?php echo JText::_('INVALID_CURRENCY_DECIMAL')?>");
            }
            else if (form.currency_decimal_separator.value.length && !form.currency_decimal_separator.value.match(/^[\.]$|^[\,]$/) )
            {
                alert("<?php echo JText::_('INVALID_DECIMAL_POINT_SEPARATOR')?>");
            }
            else if (form.currency_decimal_separator.value == form.currency_separator.value )
            {
                alert("<?php echo JText::_('THOUSANDS_SEPARATOR_IS_THE_SAME_WITH_DECIMAL_POINT_SEPARATOR')?>");
            }
            else if (form.order_min_item.value.length && !form.order_min_item.value.match(/^[0-9]+$/) )
            {
                alert("<?php echo JText::_('Invalid min quantity')?>");
            }
            else if (form.order_max_item.value.length && !form.order_max_item.value.match(/^[0-9]+$/) )
            {
                alert("<?php echo JText::_('Invalid max quantity')?>");
            }
            else if (form.order_min_value.value.length && !form.order_min_value.value.match(/^[0-9]+$/) )
            {
                alert("<?php echo JText::_('Invalid min value')?>");
            }
            else if (form.order_max_value.value.length && !form.order_max_value.value.match(/^[0-9]+$/) )
            {
                alert("<?php echo JText::_('Invalid max value')?>");
            }
            else if (form.order_min_item.value.length && form.order_max_item.value.length && (form.order_min_item.value >= form.order_max_item.value))
            {
                alert("<?php echo JText::_('Max quantity must greater than min quantity')?>");
            }
            else if (form.order_min_value.value.length && form.order_max_value.value.length && (form.order_min_value.value >= form.order_max_value.value))
            {
                alert("<?php echo JText::_('Max value must greater than min value')?>");
            }
            else
            {
                submitform( pressbutton );
            }
        }
        
        function isEmail(strEmail){
          validRegExp = /^[^@]+@[^@]+.[a-z]{2,}$/i;
           if (strEmail.search(validRegExp) == -1)
           {
              return false;
           }
           return true;
        }
        //-->
        </script>
        <script>
function addRow(nTableId)
    {
        var txtCountAttribute = document.getElementById('count');
        var nIdOfNewAttribute = parseInt(txtCountAttribute.value);
        nIdOfNewAttribute = nIdOfNewAttribute + 1;
        txtCountAttribute.value = nIdOfNewAttribute;
        var attributeTable = document.getElementById(nTableId);
        var tblTr = document.createElement("tr");
        var tblTd1 = document.createElement("td");
        var txtAttributeName = document.createElement("input");
        txtAttributeName.type = "text";
        txtAttributeName.name = "attribute_name[" + nIdOfNewAttribute + "]";
        txtAttributeName.id = "attribute_name[" + nIdOfNewAttribute + "]";
        txtAttributeName.size = "50";
        txtAttributeName.maxlength = "250";
        tblTd1.appendChild(txtAttributeName);
        var tblTd2 = document.createElement("td");
        var txtAttributeValue = document.createElement("input");
        txtAttributeValue.type = "text";
        txtAttributeValue.name = "attribute_value[" + nIdOfNewAttribute + "]";
        txtAttributeValue.id = "attribute_value[" + nIdOfNewAttribute + "]";
        txtAttributeValue.size = "50";
        txtAttributeValue.maxlength = "250";
        tblTd2.appendChild(txtAttributeValue);
        tblTr.appendChild(tblTd1);
        tblTr.appendChild(tblTd2);
        attributeTable.appendChild(tblTr);
    }

    function addField() {

        var payTable = document.getElementById('payFieldTable');
        var rowCounter = payTable.getElementsByTagName('tr').length;
        var newRow = payTable.insertRow(rowCounter);
        var label = newRow.insertCell(0);
        var type = newRow.insertCell(1);
        var del = newRow.insertCell(2);
        label.innerHTML = '<input type="text" value="" maxlength="250" size="50" name="customFields[label][]" class="text_area pay_fields">';
        type.innerHTML = '<select name="customFields[type][]" class="text_area pay_fields"><option value="">- Select one -</option><option value="text">Text</option><option value="datepicker">Date</option></select>';
        del.innerHTML = '<a onclick="delField(this)" class="del_field" href="javascript:void(0)">Delete</a>';
    }

    function delField(field) {
        jQuery(field).parent().parent().remove();
    }
</script>
 <body >
<div class="headerAttTag">
    <a class="active"><?php echo JText::_('COMPANY_DETAIL');?></a>
    <a><?php echo JText::_('SYSTEM_DETAIL');?></a>
    <a><?php echo JText::_('Shopping Cart');?></a>
    <a><?php echo JText::_('Delivery Setting');?></a>
    <hr>
</div>
<form action="index.php" method="post" name="adminForm" id="adminForm">
    <div>
<div class="att_tag tag0" style="display:block;">
<fieldset class="adminform">
    <!--<legend><?php echo JText::_('COMPANY_INFO');?></legend>-->
<table class="admintable">
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_COMPANY_NAME'),JTEXT::_(''), 
                    '', JTEXT::_('C_NAME'));?> *</span></td>
		<td><input class="text_area" type="text" name="company_name"
			id="company_name" size="30" maxlength="50"
			value="<?php echo $row->company_name;?>" /></td>
	</tr>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_ADDRESS1'),JTEXT::_(''), 
                    '', JTEXT::_('ADDRESS_1'));?> *</span></td>
		<td><input class="text_area" type="text" name="address1" id="address1"
			size="50" maxlength="255" value="<?php echo $row->address1;?>" /></td>
	</tr>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_ADDRESS2'),JTEXT::_(''),  
                    '', JTEXT::_('ADDRESS_2'));?></span></td>
		<td><input class="text_area" type="text" name="address2" id="address2"
			size="50" maxlength="255" value="<?php echo $row->address2;?>" /></td>
	</tr>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_CITY'),JTEXT::_(''), 
                    '', JTEXT::_('CITY'));?> *</span></td>
		<td><input class="text_area" type="text" name="city" id="city"
			size="50" maxlength="50" value="<?php echo $row->city;?>" /></td>
	</tr>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_STATE'),JTEXT::_(''),  
                    '', JTEXT::_('STATE'));?> *</span></td>
		<td><input class="text_area" type="text" name="state" id="state"
			size="50" maxlength="50" value="<?php echo $row->state;?>" /></td>
	</tr>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_COUNTRY'),JTEXT::_(''), 
                    '', JTEXT::_('COUNTRY'));?> *</span></td>
		<td><?php echo $countryJOptList?></td>
	</tr>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_PCODE'),JTEXT::_(''), 
                    '', JTEXT::_('POSTAL_CODE'));?> *</span></td>
		<td><input class="text_area" type="text" name="postal_code"
			id="postal_code" size="20" maxlength="250"
			value="<?php echo $row->postal_code;?>" /></td>
	</tr>
</table>
</fieldset>
<fieldset class="adminform"><legend><?php echo JText::_('TAX_DETAILS')?></legend>
<table class="admintable">
<?php 
if(count($this->taxList)!= 0)
{
?>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT('TOOLTIP_SETTING_TAX'),JTEXT::_(''), 
                    '', JTEXT::_('TAX'));?></span></td>
		<td><?php
		echo JHTML::_('select.genericList',$taxList, 'tax', null , 'text','text', $row->tax );
		?></td>
	</tr>
<?php 
}
?>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_TAX'),JTEXT::_(''), 
                    '', JTEXT::_('TAX_1'));?></span></td>
		<td><input class="text_area" type="text" name="tax_number1"
			id="tax_number1" size="30" maxlength="250"
			value="<?php echo $row->tax_number1;?>" /></td>
	</tr>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_TAX'),JTEXT::_(''), 
                    '', JTEXT::_('TAX_2'));?></span></td>
		<td><input class="text_area" type="text" name="tax_number2"
			id="tax_number2" size="30" maxlength="250"
			value="<?php echo $row->tax_number2;?>" /></td>
	</tr>
</table>
</fieldset>
<fieldset class="adminform"><legend><?php echo JText::_('STORE_DETAIL');?></legend>
<table class="admintable">
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_PHONE'),JTEXT::_(''), 
                    '', JTEXT::_('STORE_CONTACT_NUMBER'));?></span></td>
		<td><input class="text_area" type="text" name="contact_number"
			id="contact_number" size="30" maxlength="250"
			value="<?php echo $row->contact_number;?>" /></td>
	</tr>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_FAX'),JTEXT::_(''), 
                    '', JTEXT::_('STORE_CONTACT_FAX'));?></span></td>
		<td><input class="text_area" type="text" name="contact_fax"
			id="contact_fax" size="30" maxlength="250"
			value="<?php echo $row->contact_fax;?>" /></td>
	</tr>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_EMAIL'),JTEXT::_(''), 
                    '', JTEXT::_('CUMTOMER_SUPPORT_EMAIL'));?> *</span></td>
		<td><input class="text_area" type="text" name="customer_support_email"
			id="customer_support_email" size="50" maxlength="250"
			value="<?php echo $row->customer_support_email;?>" /></td>
	</tr>
</table>
</fieldset>
</div>
    
<div class="att_tag tag1" style="display:none;">
<fieldset class="adminform">
    <!--<legend><?php echo JText::_('Default Background Image')?></legend>-->
        <table class="admintable">
        <tr>
            <td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_MAIN_I'),JTEXT::_(''), 
                    '', JTEXT::_('MAIN_IMAGE'));?></span>
            </td>
            <td>
                    <?php echo $this->form->getInput('main_image'); ?>
            </td>
         </tr>
         <tr>
            <td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_DATE_TYPE'),JTEXT::_(''), 
                    '', JTEXT::_('SETTING_DATE_TYPE'));?></span>
            </td>
            <td><?php
                    $dateTypeJOptList = array();
                    array_push($dateTypeJOptList, JHTML::_('select.option', null, JText::_('--Selected--')));
                    array_push($dateTypeJOptList, JHTML::_('select.option', '0', JText::_('Date')));
                    array_push($dateTypeJOptList, JHTML::_('select.option', '1', JText::_('Date and Time')));
                    echo JHTML::_('select.genericList', $dateTypeJOptList, 'date_type', 'null', 'value', 'text', $row->date_type);
                    ?>
                   <?php // echo JHtml::_('select.genericList', $this->userGroupList, 'date_type', 'style="width: 105px;"', 'id', 'name', $row->date_type)?>
            </td>
         </tr> 
        </table>
</fieldset>    
 
<fieldset class="adminform"><legend><?php echo JText::_('TERM_CONDITION')?></legend>
<table class="admintable">

	<tr>
		<td colspan='2'><span><?php echo JText::_('TERM_CONDITION_MESSAGE')?></span></td>
	</tr>
	<tr>
		<td width="200" align="right" class="key"><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_ARTICLE'),JTEXT::_('TOOLTIP_SETTING_ARTICLE_TITLE'), 
                    '', JTEXT::_('ARTICLE'));?></td>
		<td>
		  <input class="text_area" type="hidden" name="article_id" id="article_id" size="10" maxlength="250" value="<?php echo $row->article_id;?>" />
		  <input class="text_area" type="hidden" name="article_title" id="article_title" size="10" maxlength="250" value="<?php if(isset($row->article_id) && $row->article_id !=0)echo EnmasseHelper::getArticleTitleById($row->article_id);?>" />
          <?php
                    
                $version = new JVersion;
                $joomla = $version->getShortVersion();
                if(substr($joomla,0,3) >= '1.6')
                {
                    include_once JPATH_ADMINISTRATOR.DS.'components/com_enmasse/helpers/articles.php';
                    if (class_exists( 'JFormFieldArticles' ))
                    {
                        $articleSelectBox = new JFormFieldArticles();
                        echo $articleSelectBox->getInput();
                    }                  
                }
                else
                { 
                    include_once JPATH_ADMINISTRATOR.DS.'components/com_content/elements/article.php';
                    if (class_exists( 'JElementArticle' ))
                    {
                        $articleSelectBox = new JElementArticle();
                        echo $articleSelectBox->fetchElement('name', 'value',$a=null, 'control_name');
                    }
                }    
            ?>
		</td>
	</tr>
</table>
</fieldset>
    
<fieldset class="adminform"><legend><?php echo JText::_('CURRENCY_DETAILS')?></legend>
<table class="admintable">
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_CUR'),JTEXT::_(''),
                    '', JTEXT::_('DEFAULT_CURRENCY'));?></span> </td>
		<td><input class="text_area" type="text" name="default_currency"
			id="default_currency" size="30" maxlength="250"
			value="<?php echo $row->default_currency;?>" /></td>
	</tr>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_CUR_PRE'),JTEXT::_(''), 
                    '', JTEXT::_('CURRENCY_PREFIX'));?> </span></td>
		<td><input class="text_area" type="text" name="currency_prefix"
			id="currency_prefix" size="30" maxlength="250"
			value="<?php echo $row->currency_prefix;?>" /></td>
	</tr>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_CUR_POST'),JTEXT::_(''), 
                    '', JTEXT::_('CURRENCY_POSTFIX'));?> </span></td>
		<td><input class="text_area" type="text" name="currency_postfix"
			id="currency_postfix" size="30" maxlength="250"
			value="<?php echo $row->currency_postfix;?>" /></td>
	</tr>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_THOUSANDS_SEPARATOR'),JTEXT::_(''), 
                    '', JTEXT::_('THOUSANDS_SEPARATOR'));?></span></td>
		<td><input class="text_area" type="text" name="currency_separator"
			id="currency_separator" size="30" maxlength="250"
			value="<?php echo $row->currency_separator;?>" /></td>
	</tr>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_NUMBER_OF_DECIMAL'),JTEXT::_(''), 
                    '', JTEXT::_('NUMBER_OF_DECIMAL'));?> *</span></td>
		<td><input class="text_area" type="text" name="currency_decimal"
			id="currency_decimal" size="30" maxlength="250"
			value="<?php echo $row->currency_decimal;?>" /></td>
	</tr>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_DECIMAL_SEPARATOR'),JTEXT::_(''), 
                    '', JTEXT::_('DECIMAL_SEPARATOR'));?> </span></td>
		<td><input class="text_area" type="text" name="currency_decimal_separator"
			id="currency_decimal_separator" size="30" maxlength="250"
			value="<?php echo $row->currency_decimal_separator;?>" /></td>
	</tr>
</table>

</fieldset>
<fieldset class="adminform"><legend><?php echo JTEXT::_('PRODUCT_IMG_DETAIL');?></legend>
<table class="admintable">
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_H'),JTEXT::_(''), 
                    '', JTEXT::_('IMG_HEIGHT'));?></span></td>
		<td><input class="text_area" type="text" name="image_height"
			id="image_height" size="10" maxlength="250"
			value="<?php echo $row->image_height;?>" /></td>
	</tr>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_W'),JTEXT::_(''),
                    '', JTEXT::_('IMG_WIDTH'));?></span></td>
		<td><input class="text_area" type="text" name="image_width"
			id="image_width" size="10" maxlength="250"
			value="<?php echo $row->image_width;?>" /></td>
	</tr>
</table>
</fieldset>

<!-- Begin - group for sale and merchant -->
<fieldset class="adminform"><legend><?php echo JText::_('Frontend User Access Function')?></legend>
<table class="admintable">
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_SALE_GROUP'),JTEXT::_(''), 
                    '', JTEXT::_('SALE_GROUP'));?></span>
        </td>
		<td>
			<?php echo JHtml::_('select.genericList', $this->userGroupList, 'sale_group', null, 'id', 'name', $row->sale_group)?>
		</td>
	</tr>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_MERCHANT_GROUP'),JTEXT::_(''), 
                    '', JTEXT::_('MERCHANT_GROUP'));?></span>
        </td>
		<td>
			<?php echo JHtml::_('select.genericList', $this->userGroupList, 'merchant_group', null, 'id', 'name', $row->merchant_group)?>
		</td>
	</tr>
	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_PROCESSING_CENTER_GROUP'),JTEXT::_(''), 
                    '', JTEXT::_('Processing Center Group:'));?></span>
                </td>
		<td>
			<?php echo JHtml::_('select.genericList', $this->userGroupList, 'processing_center_group', null, 'id', 'name', $row->processing_center_group)?>
		</td>
	</tr>
</table>

</fieldset>

<fieldset class="adminform"><legend><?php echo JText::_('HEAD_LOCATION_POP_UP')?></legend>
<script language="javascript" type="text/javascript">
function showDescription(subscriptionList){
	selectedSubscription = subscriptionList.options[subscriptionList.selectedIndex].value;
	description = document.getElementById(selectedSubscription).innerHTML;
	if (description!='')
	{
		document.getElementById("subscriptionDescription").innerHTML = description;
	}
}
</script>
<table class="admintable">

	<tr>
		<td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_LOCATION_POP_UP'),JTEXT::_(''), 
                    '', JTEXT::_('LOCATION_POP_UP'));?></span>
        </td>
		<td>
		<?php
//		if ($row->active_popup_location == null){
//		  echo JHTML::_('select.booleanlist', 'active_popup_location', 'class="inputbox"', 1);
//		}else{
//		  echo JHTML::_('select.booleanlist', 'active_popup_location', 'class="inputbox"', $row->active_popup_location);
//		}
			$arr = array();
			$subscriptionList = '';
			foreach (EnmasseHelper::subscriptionList() as $key=>$value)
			{
                            if($value != 'index.html'){
				array_push($arr, JHTML::_('select.option', $value, JText::_($value) ));
				$subscriptionName = $value;
				$subscriptionDesc = strtoupper($value."_DESC");
				$selectedSubscription = strtoupper($row->subscription_class."_DESC");
				if($subscriptionDesc==$selectedSubscription)
				{
					$defaultDescription = JText::_($subscriptionDesc);
				}
				$subscriptionList .= "<div id=\"$subscriptionName\" style=\"display: none\">".JText::_($subscriptionDesc)."</div>";
                            }
			}
			echo JHTML::_('select.genericlist', $arr, 'subscription_class',  'onChange="showDescription(this);"', 'value', 'text', $row->subscription_class);
		?>
		</td>
		<td>
			<?php echo $subscriptionList; ?>
			<div id="subscriptionDescription" style="color: red;"><?php echo $defaultDescription;?></div>
		</td>
	</tr>
</table>
</fieldset>
     
</div>
<div class="att_tag tag2" style="display:none;">
    <!-- ---------------- Start - Buy without registration ---------------- -->

    <fieldset class="adminform">
        <!--<legend><?php echo JText::_('HEAD_GUEST_BUY')?></legend>-->
    <table class="admintable">

            <tr>
                    <td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_SETTING_GUEST_BUY'),JTEXT::_('TOOLTIP_SETTING_GUEST_BUY_TITLE'), 
                        '', JTEXT::_('GUEST_BUY'));?></span>
            </td>
                    <td>
                    <?php
                    if ($row->active_guest_buying == null){
                      echo JHTML::_('select.booleanlist', 'active_guest_buying', 'class="inputbox"', 1);
                    }else{
                      echo JHTML::_('select.booleanlist', 'active_guest_buying', 'class="inputbox"', $row->active_guest_buying);
                    }
                    ?>
                    </td>
            </tr>
    </table>

    </fieldset>

    <!-- ---------------- End - Buy without registration ---------------- -->
    <fieldset class="adminform"><legend><?php echo JText::_('MINUTE_TO_RELEASE_INVTY')?></legend>
        <table class="admintable">

                <tr>
                        <td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_MINUTE_TO_RELEASE_INVTY'),JTEXT::_('TOOLTIP_MINUTE_TO_RELEASE_INVTY_TITLE'), 
                            '', JTEXT::_('MINUTE_TO_RELEASE'));?></span></td>
                        <td><input class="text_area" type="text" name="minute_release_invty"
                                id="minute_release_invty" size="10"  style="width:40px"
                                value="<?php echo $row->minute_release_invty;?>" /> Minute(s)
                        </td>
                </tr>
                <tr>
                        <td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_CASH_MINUTE_TO_RELEASE_INVTY'),JTEXT::_('TOOLTIP_CASH_MINUTE_TO_RELEASE_INVTY_TITLE'), 
                            '', JTEXT::_('CASH_MINUTE_TO_RELEASE'));?></span></td>
                        <td><input class="text_area" type="text" name="cash_minute_release_invty"
                                id="cash_minute_release_invty" size="10" style="width:40px"
                                value="<?php echo $row->cash_minute_release_invty;?>" /> Minute(s)
                        </td>
                </tr>
        </table>
    </fieldset>

    

    <fieldset class="adminform"><legend><?php echo JText::_('Order Requirement')?></legend>
        <table class="admintable">
            <tr>
                    <td width="200" align="right" class="key">
                        <span><?php echo JTEXT::_('Min quantity'); ?></span>
                    </td>
                    <td>
                        <input style="width:100px" type="text" id="order_min_item" name="order_min_item" size="10" value="<?php echo $row->order_min_item != '0' ? $row->order_min_item : ''; ?>" />
                    </td>
            </tr>
            <tr>
                    <td width="200" align="right" class="key">
                        <span><?php echo JTEXT::_('Max quantity'); ?></span>
                    </td>
                    <td>
                        <input style="width:100px" type="text" id="order_max_item" name="order_max_item" size="10" value="<?php echo $row->order_max_item != '0' ? $row->order_max_item : ''; ?>" />
                    </td>
            </tr>
            <tr>
                    <td width="200" align="right" class="key">
                        <span><?php echo JTEXT::_('Min Value'); ?></span>
                    </td>
                    <td>
                        <input style="width:90px" type="text" id="order_min_value" name="order_min_value" size="10" value="<?php echo $row->order_min_value;?>" /> <?php echo $row->currency_prefix;?>
                    </td>
            </tr>
            <tr>
                    <td width="200" align="right" class="key">
                        <span><?php echo JTEXT::_('Max Value'); ?></span>
                    </td>
                    <td>
                        <input style="width:90px" type="text" id="order_max_value" name="order_max_value" size="10" value="<?php echo $row->order_max_value;?>" /> <?php echo $row->currency_prefix;?>
                    </td>
            </tr>
        </table>
    </fieldset>
    <fieldset class="adminform"><legend><?php echo JText::_('Deal - Passenger Info')?></legend>
        <table class="admintable">

                <tr>
                        <td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('Passenger Title'),JTEXT::_('Passenger Title'), 
                            '', JTEXT::_('Passenger Title'));?></span></td>
                        <td><input class="text_area" type="text" name="passenger_title"
                                id="passenger_title" size="10" 
                                value="<?php echo $row->passenger_title;?>" />
                        </td>
                </tr>
                <tr>
                        <td width="200" align="right" class="key"><span><?php echo JHTML::tooltip(JTEXT::_('Passenger Description'),JTEXT::_('Passenger Description'), 
                            '', JTEXT::_('Passenger Description'));?></span></td>
                        <td>
                             <?php 
                    $editor = JFactory::getEditor();
                    echo $editor->display('passenger_description', $row->passenger_description, '200', '200', '50', '3');
                    ?> 
                        
                        </td>
                </tr>
        </table>
    </fieldset>
</div>
<div class="att_tag tag3" style="display:none;">
    <fieldset class="adminform">
        <!--<legend><?php echo JText::_('Delivery Setting')?></legend>-->
            <div>
                <table id="payFieldTable">
                    <tbody >
                        <tr>
                            <th>Field label</th>
                            <th>Type</th>
                        </tr>
                        <?php
                        if (count($custom_field) > 0):
                            foreach ($custom_field as $field):
                                ?>
                                <tr>
                                    <td><input type="text" value="<?php echo $field[label] ?>" maxlength="250" size="50" name="customFields[label][]" class="text_area pay_fields"></td>
                                    <td>
                                        <select name="customFields[type][]" class="text_area pay_fields">
                                            <option value="">- Select one -</option>
                                            <option value="text" <?php if ($field[type] == 'text') echo 'selected' ?>>Text</option>
                                            <option value="datepicker" <?php if ($field[type] == 'datepicker') echo 'selected' ?>>Date</option>
                                        </select>
                                    </td>
                                    <td>
                                         <a onclick="delField(this)" class="del_field" href="javascript:void(0)">Delete</a>
                                    </td>
                                </tr>		
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td><input type="text" value="<?php echo $field[label] ?>" maxlength="250" size="50" name="customFields[label][]" class="text_area pay_fields"></td>
                                <td>	
                                    <select name="customFields[type][]" class="text_area pay_fields">
                                        <option value="">- Select one -</option>
                                        <option value="text" >Text</option>
                                        <option value="datepicker" >Date</option>
                                    </select>	
                                </td>
                            </tr>			
                        <?php endif ?>
                </table>
                <a onclick="addField()" href="javascript:void(0)">Add more field</a>
            </div>
    </fieldset>
</div>

<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="controller" value="setting" />
<input type="hidden" name="task" value="" />
</div>
</form>
</body>
<script>
    jQuery(document).ready(function(){
//        jQuery('.toggle_bt').click(function(e){
//            e.preventDefault();
//
//            if (!jQuery(this).parent().hasClass('active')){
//                    jQuery('.toggle_mct').slideUp();
//                    jQuery(this).parent().find('.toggle_mct').slideToggle();
//                    jQuery('.toggle_bt').parent().removeClass('active');
//                    jQuery(this).parent().addClass('active');
//            }else{
//                    jQuery(this).parent().find('.toggle_mct').slideUp();
//                    jQuery(this).parent().removeClass('active');
//            }
//        });
        
        var tag = jQuery('.headerAttTag a');

        tag.each(function(index, tab){
            jQuery(tab).unbind("click");
            jQuery(tab).click(function(){
                jQuery('.att_tag').hide(1000);
                jQuery(tag).removeClass('active');

                jQuery('.tag'+index).show(1000);
                jQuery(tab).addClass('active');
            });
        });
    });
</script>
    