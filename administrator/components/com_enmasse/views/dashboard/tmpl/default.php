<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined('_JEXEC') or die('Restricted access');
?>
<div id="dashBoard" class="row-fluid">
    <div class="span7" id="main_feature">
        <!-- Deal -->
        <div class="row-fluid">
            <div class="span3">
                <a href="<?php echo JRoute::_("index.php?option=com_enmasse&controller=deal"); ?>"><img src="components/com_enmasse/images/deal_management.png" /></a>
                <br/>
                <b><?php echo JText::_('S_DEAL'); ?></b>
            </div>
			
			<div class="span3">
                <a href="<?php echo JRoute::_("index.php?option=com_enmasse&controller=insurance"); ?>"><img src="components/com_enmasse/images/deal_management.png" /></a>
                <br/>
                <b><?php echo JText::_('S_INSURANCE'); ?></b>
            </div>
            <div class="span3">
                <a href="<?php echo JRoute::_("index.php?option=com_enmasse&controller=discountcoupon"); ?>"><img src="components/com_enmasse/images/Discount_coupon.png" /></a>
                <br/>
                <b><?php echo JText::_('S_DISCOUNT_COUPON'); ?></b>
            </div>
            <div class="span3">
                <a href="<?php echo JRoute::_("index.php?option=com_enmasse&controller=order"); ?>"><img src="components/com_enmasse/images/order_management.png" /></a>
                <br/>
                <b><?php echo JText::_('S_ORDER'); ?></b>
            </div>
            
        </div>
        <div class="row-fluid">
<!--             <div class="span3">
                <a href="<?php echo JRoute::_("index.php?option=com_enmasse&controller=processingcenter"); ?>"><img src="components/com_enmasse/images/locations.png" /></a>
                <br/>
                <b><?php echo JText::_('Processing Center'); ?></b>
            </div>-->
            <div class="span3">
                <a href="<?php echo JRoute::_("index.php?option=com_enmasse&controller=report"); ?>"><img src="components/com_enmasse/images/sale_report.png" /></a>
                <br/>
                <b><?php echo JText::_('S_COUPON_REPORT'); ?></b>
            </div>
            <div class="span3">
                <a href="<?php echo JRoute::_("index.php?option=com_enmasse&controller=comment"); ?>"><img src="components/com_enmasse/images/comment.png" /></a>
                <br/>
                <b><?php echo JText::_('S_COMMENT'); ?></b>
            </div>
<!--            <div class="span3">
                <a href="<?php echo JRoute::_("index.php?option=com_enmasse&controller=salesPerson"); ?>"><img src="components/com_enmasse/images/salesperson.png" /></a>
                <br/>
                <b><?php echo JText::_('S_SALE_PERSON'); ?></b>
            </div>
            <div class="span3">
                <a href="<?php echo JRoute::_("index.php?option=com_enmasse&controller=merchant"); ?>"><img src="components/com_enmasse/images/merchant_management.png" /></a>
                <br/>
                <b><?php echo JText::_('S_MERCHANT'); ?></b>
            </div>-->
            <div class="span3">
                <a href="<?php echo JRoute::_("index.php?option=com_enmasse&controller=help"); ?>"><img src="components/com_enmasse/images/help.png" /></a>
                <br/>
                <b><?php echo JText::_('S_HELP'); ?></b>
            </div>
			<div class="span3">
                <a href="<?php echo JRoute::_("index.php?option=com_enmasse&controller=setting&cid=1"); ?>"><img src="components/com_enmasse/images/setting.png" /></a>
                <br/>
                <b><?php echo JText::_('S_SETTING'); ?></b>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span3">
                <a href="<?php echo JRoute::_("index.php?option=com_enmasse&controller=city"); ?>"><img src="components/com_enmasse/images/deal_management.png" /></a>
                <br/>
                <b><?php echo JText::_('S_CITY'); ?></b>
            </div>
        </div>
    </div>
    <!-- end div leftDashBoard -->
    <div class="span5">
        <div class="overView">
            <div class="header_overView">
                OverView
            </div>
            <div class="contain_overView">
                <div class="detail_overView">
                    <div class="leftDetail_overView">
                        <p>Total Merchants:</p><span><?php echo $this->totalMerchant ?></span><br/>
                        <div class="underLineOverView"></div>
                        <p>Total Sales Person:</p><span><?php echo $this->totalSalePerson ?></span><br/>
                        <div class="underLineOverView"></div>
                        <p>Total Product On Sale:</p><span><?php echo $this->DealOnSale ?></span><br/>
                        <div class="underLineOverView"></div>
                        <p>Total Product:</p><span><?php echo $this->totalDeal ?></span><br/>
                    </div>
                </div>
            </div>
        </div>
        <!-- end div overView for overview  -->
        <div class="overView">
            <div class="header_overView">
                Product Overview
                <div class="right_header_overView">
                    <input type="text" class="auto" onclick="return autoDealName();" name="dealName" id="dealNameOverView" value="" style="width: 76%;" placeholder="Product Name">
                    <a onclick="return getDealOverView();"><img src="components/com_enmasse/images/search-icon.png" /></a>
                </div>
            </div>
            <div class="contain_overView">
                <div class="detail_overView">
                    <div class="leftDetail_overView">
                        <table cellpadding="7" width="100%">
<!--                            <tr>
                                <td colspan="2">No. of views:</td>
                                <td style="text-align:center;" class="DOV1"><?php echo $this->numViewDeal; ?></td>
                            </tr>-->
                            <tr>
                                <td>&nbsp;</td>
                                <td style="text-align:center;">Up to now</td>
                                <td  style="text-align:center;">Today(<?php echo date('Y-m-d'); ?>)</td>
                            </tr>
                            <tr>
                                <td>Total order:</td>
                                <td style="text-align:center;" class="DOV2"><?php echo $this->orderAmount ?></td>
                                <td style="text-align:center;" class="DOV3"><?php echo $this->countOrderToday ?></td>
                            </tr>
                            <tr>
                                <td>No. of customer ordered:</td>
                                <td style="text-align:center;" class="DOV4"><?php echo $this->countCustomerOrder; ?></td>
                                <td style="text-align:center;" class="DOV5"><?php echo $this->countCustomerOrderToday; ?></td>
                            </tr>
                            <tr>
                                <td>Total amount sold:</td>
                                <td style="text-align:center;" class="DOV6"><?php echo $this->countDealSold ?></td>
                                <td style="text-align:center;" class="DOV7"><?php echo $this->countDealSoldToday; ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- end div overview for order  -->    
    </div>
    <!-- end div rightDastBoard  -->
    <div class="clear"></div>
    <?php $rows = $this->list5Deals; ?>
    <div class="lastDeal">
        <div class="header_overView_table">
            Latest 5 Products
        </div>
        <div class="tableOV">
            <table class="adminlist  table table-striped" width="100%">
                <thead>
                    <tr>
                        <th width="2%"><?php echo JText::_('DEAL_ID'); ?></th>
                        <!--<th width="90"><?php echo JText::_('DEAL_CODE'); ?></th>-->
                        <th width="60%"  ><?php echo JText::_('DEAL_NAME'); ?></th>
                        <!--<th width="90"><?php echo JText::_('DEAL_MIN_QUANTITY'); ?></th>-->
                        <th width="20%"><?php echo JText::_('DEAL_TOTAL_SOLD'); ?></th>
                        <th width="18%"><?php echo JText::_('DEAL_STATUS'); ?></th>
                        <!--<th width="100"><?php echo JText::_('DEAL_SALE_PERSON'); ?></th>-->
                        <!--<th width="100"><?php echo JText::_('DEAL_CATEGORY'); ?></th>-->
                        <!--<th width="100"><?php echo JText::_('DEAL_LOCATION'); ?></th>-->
                    </tr>
                </thead>

                <?php
                for ($i = 0; $i < $n = count($rows); $i++) {
                    $k = $i % 2;
                    $row = &$rows[$i];
                    ?>
                    <tr class="<?php echo "row$k"; ?>">
                        <td style="text-align: center"><?php echo $i + 1; ?></td>
                        <!--<td ><?php echo $row->deal_code; ?></td>-->
                        <?php
                        if (strlen($row->name) > 122) {
                            $row->name = substr($row->name, 0, 122) . "...";
                        }
                        ?>
                        <td ><?php echo $row->name; ?></td>
                        <!--<td ><?php echo $row->min_needed_qty; ?></td>-->
                        <td style="text-align: center"><?php echo $row->cur_sold_qty; ?></td>
                        <td style="text-align: center"><?php echo JTEXT::_('DEAL_' . strtoupper(str_replace(' ', '_', $row->status))); ?></td>
    <!--                                <td align="center" >
                        <?php echo $row->sales_person_name; ?>
                        </td>
                        <td align="center" >
                        <?php
                        if ($row->category_name != null) {
                            foreach ($row->category_name as $item)
                                echo '- ' . $item->name . '<br/>';
                            ;
                        }
                        ?>
                        </td>-->
    <!--                                <td align="center" >
                        <?php
                        if ($row->location_name != null) {
                            foreach ($row->location_name as $item)
                                echo '- ' . $item->name . '<br>';
                        }
                        ?>
                        </td>-->
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
        <!-- end div overView last 5 deals -->
    </div>
    <!-- end div lastDeal  -->
    <!-- begin for lastest 10 oders --> 
    <?php $rowsOrder = $this->list10LastestOrder; ?>
    <div class="header_overView_table">
        Latest 10 Orders
    </div>
    <div class="tableOV" id="last10order">
        <table class="adminlist  table table-striped" width="100%">
            <thead>
                <tr>
                    <th width="2%"><?php echo JText::_('DEAL_ID'); ?></th>
                    <th width="20%"><?php echo JText::_('Buyer Name'); ?></th>
                    <!--<th width="150"><?php echo JText::_('Buyer Email'); ?></th>-->
                    <!--<th width="120"><?php echo JText::_('Delivery Name'); ?></th>-->
                    <!--<th width="150"><?php echo JText::_('Delivery Email'); ?></th>-->
                    <th width="20%"><?php echo JText::_('Date Purchased'); ?></th>
                    <th width="20%"><?php echo JText::_('ORDER_QUANTITY') ?></th>
                    <th width='20%'><?php echo JText::_('ORDER_TOTAL_PAID') ?> </th>
                    <th width='18%'><?php echo JText::_('ORDER_STATUS') ?></th>
                </tr>
            </thead>
            <tbody >
                <?php
                for ($i = 0; $i < count($rowsOrder); $i++) {
                    $k = $i % 2;
                    $row = &$rowsOrder[$i];
                    $arrBuyer = json_decode($row->buyer_detail);
                    $arrDeliver = json_decode($row->delivery_detail);
                    ?>
                    <tr class="<?php echo "row$k"; ?>">
                        <td style="text-align: center"><?php echo $row->id; ?></td>
                        <td ><?php echo $arrBuyer->name; ?></td>
                        <!--<td ><?php echo $arrBuyer->email; ?></td>-->
                        <!--<td ><?php echo $arrDeliver->name; ?></td>-->
                        <!--<td ><?php echo $arrDeliver->email; ?></td>-->
                        <td style="text-align: center"><?php echo $row->created_at; ?></td>
                        <td style="text-align: center"><?php echo $row->qty; ?></td>
                        <td style="text-align: center"><?php echo $row->paid_amount; ?></td>
                        <td style="text-align: center"><?php echo $row->status; ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- end div overView for latest 10 orders -->
</div>
<!-- end div style -->
<script type="text/javascript">
                        function getLastDeal()
                        {
                            var deal_name = jQuery('#dealNameOrder').val();
                            jQuery.ajax({
                                url: 'index.php?option=com_enmasse&controller=order&task=ajaxGetLastDeal',
                                type: 'POST',
                                dataType: 'html', //'json'
                                data: {
                                    'deal_name': deal_name
                                },
                                success: function(resp) {
                                    jQuery('#last10order').html(resp);
                                }
                            });
                        }

                        function getDealOverView()
                        {
                            var dealName = jQuery('#dealNameOverView').val();
                            jQuery.ajax({
                                url: 'index.php?option=com_enmasse&controller=deal&task=ajaxGetDealOverView',
                                type: 'POST',
                                dataType: 'html', //'json'
                                data: {
                                    'dealName': dealName
                                },
                                success: function(resp) {
                                    var chart = resp.split(',');
                                    jQuery('.DOV1').html(chart[0]);
                                    jQuery('.DOV2').html(chart[1]);
                                    jQuery('.DOV3').html(chart[2]);
                                    jQuery('.DOV4').html(chart[3]);
                                    jQuery('.DOV5').html(chart[4]);
                                    jQuery('.DOV6').html(chart[5]);
                                    jQuery('.DOV7').html(chart[6]);
                                }
                            });
                        }
</script>
