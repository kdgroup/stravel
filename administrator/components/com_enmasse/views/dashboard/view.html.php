<?php

/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "toolbar.enmasse.html.php");

class EnmasseViewDashBoard extends JViewLegacy {

    function display($tpl = null) {
        
        $oOrderModel = JModelLegacy::getInstance('order', 'enmasseModel');
        $orderAmount = $oOrderModel->getCountOrder("");
        $countOrderToday = $oOrderModel->getCountOrderToday("");
        $countDealSold = $oOrderModel->getCountDealSold("");
        $countDealSoldToday = $oOrderModel->getCountDealSoldToday("");
        if($countDealSoldToday == ""){
            $countDealSoldToday = 0;
        }
        $countCustomerOrder = $oOrderModel->getCountCustomerOrder("");
        $countCustomerOrderToday = $oOrderModel->getCountCustomerOrderToday("");
        $this->countCustomerOrderToday = count($countCustomerOrderToday);
        $this->countCustomerOrder = count($countCustomerOrder);
        $this->countDealSold = $countDealSold;
        $this->countDealSoldToday = $countDealSoldToday;
        $this->countOrderToday = count($countOrderToday);
        $this->orderAmount = count($orderAmount);
        $oMerchantModel = JModelLegacy::getInstance('merchant', 'enmasseModel');
        $this->totalMerchant = $oMerchantModel->countAll();
        $oSalePerson = JModelLegacy::getInstance('salesperson', 'enmasseModel');
        $this->totalSalePerson = $oSalePerson->countAll();
        $oDeal = JModelLegacy::getInstance('deal', 'enmasseModel');
        $this->DealOnSale = $oDeal->getCountDealOnSale();
        $this->totalDeal = $oDeal->countAll();
        $numViewDeal = $oDeal->getNumView("");
        $this->numViewDeal = ceil($numViewDeal / 3);
        $list5Deals = $oDeal->get5DealLastest();
        for ($i = 0; $i < count($list5Deals); $i++) {
            $dealCategoryIdList = JModelLegacy::getInstance('dealcategory', 'enmasseModel')->getCategoryByDealId($list5Deals[$i]->id);
            $dealLocationIdList = JModelLegacy::getInstance('deallocation', 'enmasseModel')->getLocationByDealId($list5Deals[$i]->id);

            //----------------------------------------------
            // get list of category name
            if (count($dealCategoryIdList) != 0)
                $categoryList = JModelLegacy::getInstance('category', 'enmasseModel')->getCategoryListInArrId($dealCategoryIdList);
            else
                $categoryList = null;
            //----------------------------------------------
            // get list of location name
            if (count($dealLocationIdList) != 0)
                $locationList = JModelLegacy::getInstance('location', 'enmasseModel')->getLocationListInArrId($dealLocationIdList);
            else
                $locationList = null;


            if (count($locationList) != 0 && $locationList != null)
                $list5Deals[$i]->location_name = $locationList;
            else
                $list5Deals[$i]->location_name = null;

            if (count($categoryList) != 0 && $categoryList != null)
                $list5Deals[$i]->category_name = $categoryList;
            else
                $list5Deals[$i]->category_name = null;
                $list5Deals[$i]->sales_person_name = 
                        JModelLegacy::getInstance('salesPerson', 'enmasseModel')->retrieveName($list5Deals[$i]->sales_person_id);
        }
        $this->list5Deals = $list5Deals;

        $list10LastestOrder = JModelLegacy::getInstance('order', 'enmasseModel')->get10LastestOrder("");
        $this->list10LastestOrder = $list10LastestOrder;
        TOOLBAR_enmasse::_DEFAULT();
        parent::display($tpl);
    }

}

?>