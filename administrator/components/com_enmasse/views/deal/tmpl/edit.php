<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

//add space for children category
$tmpCategory = array();
foreach($this->categoryList as $item){
    if($item->parent_id){ 
        $item->name = '&nbsp;&nbsp;-'.$item->name;
    }
    //get default array
    $tmpCategory[] = $item->id;
}
$tmpCategory = implode(',', $tmpCategory);

foreach($this->dealCategoryList as $item){
    if($item->parent_id){
        $item->name = '&nbsp;&nbsp;-'.$item->name;
    }
}
//--end add space--

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php"); 
$emptyJOpt = JHTML::_('select.option', '', JText::_('') );
$row            = $this->deal;
if(!$this->bNewDeal)
{
    $nCurrentSoldQty = $row->cur_sold_qty;
}
else
{
    $nCurrentSoldQty = 0;
}
$merchantList   = $this->merchantList;
$locationList   = $this->locationList;
$categoryList   = $this->categoryList;
$cityList   = $this->cityList;
$statusList     = $this->statusList;
$option         = 'com_enmasse';
$prepayList = array();
for($i = 0; $i <= 100; $i+=5)
{
    $prepayList[$i] = $i;
}
//Kayla add Commission
$commissionList = array();
for($i = 0; $i <= 100; $i+=5)
{
    $commissionList[$i] = $i;
}

global $mainframe;
JHtml::_('behavior.framework');
//JHTML::_('behavior.calendar');
JHTML::_('behavior.tooltip');
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Locale.en-US.DatePicker.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.Attach.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.Date.js");
JFactory::getDocument()->addStyleSheet("components/com_enmasse/script/datepicker/datepicker_dashboard/datepicker_dashboard.css");
?>
<script src="components/com_enmasse/script/jquery-ui.js"></script>
<?php 
if(!$row->end_at){
                            $row->start_at = date('Y-m-d H:i:s',time());
                           
                            }
if(!$row->end_at){
                            $row->end_at = date('Y-m-d H:i:s',time()+60*24*60);
                            
                            }?>
<script type="text/javascript">
<!--
window.onerror = function(){
    return true;    
};
addEvent('domready',function(){
    new Picker.Date($$('.calendar'), {
    timePicker: true,
    draggable: false,
    positionOffset: {x: 5, y: 0},
    pickerClass: 'datepicker_dashboard',
    format: 'db',
    useFadeInOut: !Browser.ie
    });
});
//-->
</script>
<style> 
    table.adminlist tbody tr.unapply-deal td {text-decoration: line-through;}
    table.adminlist thead th.cellTools {width:180px;}
        .editor iframe {
            max-height: 110px;
        }
</style>

<script language="javascript" type="text/javascript">        
        function moveOptions(from,to) {           
              // Move them over
              for (var i=0; i<from.options.length; i++) {
                var o = from.options[i];
                if (o.selected) {
                  to.options[to.options.length] = new Option( o.text, o.value, false, false);
                }
              }
              // Delete them from original
              for (var i=(from.options.length-1); i>=0; i--) {
                var o = from.options[i];
                if (o.selected) {
                  from.options[i] = null;
                }
              }
              from.selectedIndex = -1;
              to.selectedIndex = -1;
        }
        
        function moveOptionsArrange(from,to) {           
            // Move them over
            for (var i=0; i<from.options.length; i++) {
                  var o = from.options[i];
                  if (o.selected) {
                    to.options[to.options.length] = new Option( o.text, o.value, false, false);
                  }
            }
            // Delete them from original
            for (var i=(from.options.length-1); i>=0; i--) {
                  var o = from.options[i];
                  if (o.selected) {
                    from.options[i] = null;
                  }
            }
            from.selectedIndex = -1;
            to.selectedIndex = -1;

            //arrange destination array
            arrangeCategory(to);
        }
        
        function arrangeCategory(cate)
        {
            var tmp = '<?php echo $tmpCategory; ?>';
            tmp = tmp.split(',');

            var n = 0;
            for(var i=0; i < tmp.length; i++){
                for(var j=0; j<cate.options.length; j++){
                    if(tmp[i] == cate.options[j].value){
                        var swapText1 = cate.options[n].text;
                        var swapValue1 = cate.options[n].value;
                        var swapText2 = cate.options[j].text;
                        var swapValue2 = cate.options[j].value;
                        cate.options[j].value = 99999;
                        
                        cate.options[n].text = swapText2;
                        cate.options[n].value = swapValue2;
                        
                        cate.options[j].text = swapText1;
                        cate.options[j].value = swapValue1;
                        n++;
                    }
                }
            }
        }
        
        function dateGen(str)
        {
            var year  = parseInt(str.substring(0,4),10);
            var month  = parseInt(str.substring(5,7),10);
            var day  = parseInt(str.substring(8,10),10);
            var date = new Date(year, month, day);
            return date.getTime();
        }
        function currDate()
        {
             var currentTime = new Date();
             var year = currentTime.getFullYear();
             var month = currentTime.getMonth() + 1;
             var day = currentTime.getDate();
             var today = "";
             if(month > 9 && day > 9 )
             {today = year+"-"+month+"-"+day;}
             else if(month > 9)
             {today = year+"-"+month+"-0"+day;}
             else if(day > 9)
             {today = year+"-0"+month+"-"+day;}
             else
             {today = year+"-0"+month+"-0"+day;}
             return today;
        }
        
<?php        
$version = new JVersion;
$joomla = $version->getShortVersion();
if(substr($joomla,0,3) >= '1.6'){
?>
        Joomla.submitbutton = function(pressbutton)
<?php
}else{
?>
        submitbutton = function(pressbutton)
<?php
}
?>
        
        {
            
            var StartSateDate = '<?php  if(isset($row->start_at)) {echo $row->start_at;} else { echo '';} ?>';
            var EndDate       = '<?php  if(isset($row->start_at)) {echo $row->end_at;} else { echo '';} ?>';
            var form = document.adminForm;
            if (pressbutton != 'save')
            {
                
                submitform( pressbutton );
                return;
            }
            // do field validation
           // sName = jQuery.trim(form.name.value.replace(/(<.*?>)/ig,""));
            sName = form.name.value;
            var timePt = /\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/;
            var min_needed = parseInt(jQuery.trim(form.min_needed_qty.value));
            var max_needed = parseInt(jQuery.trim(form.max_coupon_qty.value));
            var max_buy    = parseInt(jQuery.trim(form.max_buy_qty.value));
            var lastMinQtyStep    = parseInt(jQuery.trim(form.lastMinQtyStep.value));
            var tierPricing = jQuery("input:radio[name=tier_pricing]:checked").val();
            
            if (form.cur_sold_qty.value <0)
        {
              alert( "<?php echo JText::_( 'INVALID_CURRENT_SOLD', true ); ?>" );
        }
        
            if (sName == "")
            {
                alert( "<?php echo JText::_( 'INVALID_NAME', true ); ?>" );
            }
            else if (form.origin_price.value == "" || form.origin_price.value == 0)
            {
                alert( "<?php echo JText::_( 'FILL_IN_DEAL_ORIG_PRICE', true ); ?>" );
            }
            else if (min_needed < 0)
            {
                alert( "<?php echo JText::_( 'DEAL_MIN_QTY_NOT_NEGATIVE', true ); ?>" );
            }             
            else if (form.price.value == "")
            {
                alert( "<?php echo JText::_( 'FILL_IN_DEAL_PRICE', true ); ?>" );
            }
            else if (parseFloat(form.price.value) > parseFloat(form.origin_price.value))
            {
                alert( "<?php echo JText::_( 'PRICE_NOT_GREATER_ORIG_PRICE', true ); ?>" );
            }   
            else if (isNaN(form.origin_price.value))
            {
                alert( "<?php echo JText::_( 'NUMBERIC_DEAL_ORIG_PRICE', true ); ?>" );
            }                     
            else if (isNaN(form.price.value))
            {
                alert( "<?php echo JText::_( 'NUMBERIC_DEAL_PRICE', true ); ?>" );
            }
            else if (form.price.value < 0)
            {
                alert( "<?php echo JText::_( 'DEAL_PRICE_NOT_NEGATIVE', true ); ?>" );
            }
            else if (form.origin_price.value < 0)
            {
                alert( "<?php echo JText::_( 'DEAL_ORIG_PRICE_NOT_NEGATIVE', true ); ?>" );
            }
            else if (form.start_at.value != "" && !timePt.test(form.start_at.value))
            {
                alert( "<?php echo JText::_( 'DEAL_START_AT_INVALID_FORMAT', true ); ?>" );
            }
            else if (form.end_at.value != "" && !timePt.test(form.end_at.value))
            {
                alert( "<?php echo JText::_( 'DEAL_END_AT_INVALID_FORMAT', true ); ?>" );
            }
            else if(form.start_at.value != "" && form.end_at.value != "" && dateGen(form.start_at.value) > dateGen(form.end_at.value))
            {
                alert( "<?php echo JText::_( 'DEAL_END_GREATER_START', true ); ?>" ); 
            }
            else if (min_needed == "")
            {
                alert( "<?php echo JText::_( 'FILL_IN_DEAL_MIN_QTY', true ); ?>" );
            }
            else if(max_buy < -1){
                alert("<?php echo JText::_('MAX_BUY_QTY_LESS_THAN_ZERO', true)?>");
            }
            else if(max_needed < -1){
                alert("<?php echo JText::_('MAX_COUPON_QTY_LESS_THAN_ZERO', true)?>");
            }            
            else if(isNaN(max_buy)){
                alert("<?php echo JText::_('NUMBER_MAX_BUY_QTY', true)?>");
            }
            else if((max_needed != -1)&&((max_buy > max_needed)||(max_buy == -1))){
                alert("<?php echo JText::_('MAX_COUPON_QTY_LESS_MAX_BUY', true)?>");
            }           
            else if((tierPricing == 1)&&(max_needed != -1)&&(max_needed < lastMinQtyStep)){                 
                alert("<?php echo JText::_('MIN_STEP_QTY_LESS_MAX_BUY', true)?>");
            } 
            else if(jQuery.trim(form.max_buy_qty.value) == "")   
            {
                alert("<?php echo JText::_('EMPTY_MAX_BUY_QTY', true)?>");                
            }  
            else if(isNaN(max_needed)){
                alert("<?php echo JText::_('NUMBER_MAX_COUPON_QTY', true)?>");
            }
            
            else if(jQuery.trim(form.max_coupon_qty.value) == "")   
            {
                alert("<?php echo JText::_('EMPTY_MAX_COUPON_QTY', true)?>");                
            }
            
            else if(max_needed > 0 && form.cur_sold_qty.value > max_needed)
            {
                 alert("<?php echo JText::_('CURRENT_SOLD_GREATER_THAN_MAX_COUPON_QTY', true)?>");  
            }
            else if((max_needed > 0) && (min_needed > max_needed))
            {
                alert("<?php echo JText::_('NEEDED_QTY_GREATER_THAN_MAX_COUPON_QTY', true)?>");
            }
            else if(min_needed >0 && min_needed >100 )
            {   
                alert("<?php echo JText::_('DOMINATION_MINIMUM_QTY', true)?>");
            }    
            else if(max_buy >0 && max_buy >100)
            {
                alert("<?php echo JText::_('DOMINATION_PURCHASE_BY_USER_QTY', true)?>");
            }                   
            else if (isNaN(min_needed))
            {
                alert( "<?php echo JText::_( 'NUMBERIC_DEAL_MIN_QTY', true ); ?>" );
            }
            else if(document.adminForm['location_id[]'].options.length ==0)
            {
                  alert ("<?php echo JText::_('PLEASE_CHOOSE_LOCATION_FOR_DEAL', true);?>");
            }
            else if(document.adminForm['pdt_cat_id[]'].options.length ==0)
            {
                  alert ("<?php echo JText::_('PLEASE_CHOOSE_CATEGORY_FOR_DEAL', true);?>");
            }
            else
            {
//              allSelected(document.adminForm['location_id[]']);
//              allSelected(document.adminForm['pdt_cat_id[]']);
//                submitform( pressbutton );
//                return;
             jQuery.post("index.php?option=com_enmasse&tmpl=component&controller=deal&task=checkDuplicatedDeal", { dealName: sName },function(data) {
                   if(data == 'true' && sName!=form.tempName.value){
                          alert("<?php echo JText::_('DEAL_NAME_DUPLICATED', true); ?>");
                       }
                           else
                       {
                           allSelected(document.adminForm['location_id[]']);
                           allSelected(document.adminForm['pdt_cat_id[]']);
                           submitform( pressbutton );
                           return;
                        }
                   
                 });
            }
            
        }

        function allSelected(element) {
               for (var i=0; i<element.options.length; i++) {
                    var o = element.options[i];
                    o.selected = true;

                }
             }
        
        </script>

<style>
    .toggle-editor .btn-group{
            display:none !important;
    }
    .btn-toolbar a{
        margin: 0px;
        display: none;
    }
    .btn-toolbar a:nth-child(2){
        display: block;
    }
    #editor-xtd-buttons {
        margin: 0px;
    }
    .radio {
        float: left;
        margin-right: 15px;
    }
    label{
        /*line-height: 21px;*/
    }
</style>
<div class="headerAttTag">
    <a class="active"><?php echo JText::_('DEAL_GENERAL_DETAIL');?></a>
    <a><?php echo JText::_('DEAL_PRIMARY_DISPLAY');?></a>
   
    <a class="attribute-tab" ><?php echo JText::_('DEAL_SPECIAL_PROMOTION');?></a>
     <a><?php echo JText::_('Insurance');?></a>
    <!--<a><?php echo JText::_('DEAL_SETTINGS_INFO');?></a>-->
    <hr>
</div>
<form action="index.php" method="post" name="adminForm" id="adminForm" class="form-validate">
<div class="width-100 fltrt">
<div class="col100">
<div class="att_tag tag0" style="display:block;">
        <table class="admintable" style="width: 100%; margin-top:20px;" border="0">
            <!--<tr>
                    <td width="15%" align="right" class="key"><label for="prepay_percent"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_PREPAY_PERCENT'),JTEXT::_(''), '', JTEXT::_('DEAL_PREPAY_PERCENT')); ?>
                    * </label></td>
                    <td><?php echo JHtml::_('select.genericList', $prepayList, 'prepay_percent', null, 'value', 'text', is_null($row->prepay_percent)? 10 : $row->prepay_percent, 'prepay_percent')?><span>%</span></td>
            </tr>-->
            <tr>
                    <td width="15%" align="right" class="key"><label for="name"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_NAME'), JTEXT::_(''), '', JTEXT::_('DEAL_NAME')); ?>
                    * </label></td>
                    <td><input class="required" type="text" name="name" id="name"
                            size="50" maxlength="250" value="<?php echo strip_tags($row->name);?>" />
                            <input type="hidden"  name="tempName" value="<?php echo $row->name;?>" />
                            </td>
                             <td width="15%" align="right" class="key"><label for="quantity_odd"><?php echo JHTML::tooltip(JTEXT::_(''),JTEXT::_(''), '', JTEXT::_('Quantity Odd')); ?>:</label>
                </td>
                <td>
                        <?php 
                        if(!isset($row->quantity_odd)){
                            $row->quantity_odd =1;
                        }
                       
                        echo JHtml::_('select.booleanlist', 'quantity_odd','class="inputbox"', $row->quantity_odd)?>
                </td>
            </tr>
            <tr>
                    <td width="15%" align="right" class="key"><label for="name"> <?php echo JHTML::tooltip(JTEXT::_('Product Subtitle'), JTEXT::_(''), '', JTEXT::_('Product Subtitle')); ?>
                    * </label></td>
                    <td><input class="required" type="text" name="subTitle"
                            size="50" maxlength="250" value="<?php echo strip_tags($row->subTitle);?>" />
                           
                            </td>
                    <td width="15%" align="right" class="key"><label for="showinsurance"><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_SHOWINSURANCE'),JTEXT::_(''), '', JTEXT::_('DEAL_SHOWINSURANCE')); ?>:</label>
                    </td>
                    <td><?php 
                    if ($row->showinsurance == null)
                    echo JHTML::_('select.booleanlist', 'showinsurance', 'class="inputbox"', 1);
                    else
                    echo JHTML::_('select.booleanlist', 'showinsurance', 'class="inputbox"', $row->showinsurance);
                    ?></td>
            </tr>
            <tr>
                <td width="15%" align="right" class="key"><label for="published"><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_PUBLISHED'),JTEXT::_(''), '', JTEXT::_('PUBLISHED')); ?>:</label>
                    </td>
                <td><?php 
                if ($row->published == null)
                echo JHTML::_('select.booleanlist', 'published', 'class="inputbox"', 1);
                else
                echo JHTML::_('select.booleanlist', 'published', 'class="inputbox"', $row->published);
                ?></td>
                
                <td width="15%" align="right" class="key"><label for="auto_confirm"><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_AUTO_CONFIRM'),JTEXT::_(''), '', JTEXT::_('DEAL_AUTO_CONFIRM')); ?>:</label>
                </td>
                <td>
                        <?php 
                        if(!isset($row->auto_confirm)){
                            $row->auto_confirm =1;
                        }
                       
                        echo JHtml::_('select.booleanlist', 'auto_confirm','class="inputbox"', $row->auto_confirm)?>
                </td>
            </tr>
            
            <tr>
                    <td width="15%" align="right" class="key"><label for="price"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_C_PRICE'),JTEXT::_(''), '', JTEXT::_('DEAL_PRICE')); ?>
                    (<?php echo $this->currencyPrefix;?>) * </label></td>
                    <td width="35%"><input class="text_area"
                            type="text" name="price" id="price" size="50" maxlength="250"
                            value="<?php echo $row->price;?>" /> <?php echo $this->currencyPostfix;?>
                    </td>
                    <td width="15%" align="right" class="key"><label for="price"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_O_PRICE'),JTEXT::_(''), '', JTEXT::_('DEAL_ORIGINAL_PRICE')); ?>
                    (<?php echo $this->currencyPrefix;?>) * </label></td>
                    <td  width="35%"><input class="text_area"
                            type="text" name="origin_price" id="origin_price" size="50"
                            maxlength="250" value="<?php echo $row->origin_price;?>" /> <?php echo $this->currencyPostfix;?>
                    </td>
            </tr>
             <tr>
                    <td width="15%" align="right" class="key"><label for="deposit"> <?php echo JHTML::tooltip(JTEXT::_('DEPOSIT'),JTEXT::_(''), '', JTEXT::_('DEPOSIT')); ?>
                     </label></td>
                    <td width="35%"><input class="text_area"
                            type="text" name="deposit" id="deposit" size="50" maxlength="250"
                            value="<?php echo $row->deposit;?>" /> 
                    </td>
                    <td width="15%" align="right" class="key"><label for="frontpage"> <?php echo JHTML::tooltip(JTEXT::_('FRONTPAGE'),JTEXT::_(''), '', JTEXT::_('FRONTPAGE')); ?>
                     </label></td>
                    <td  width="35%"><input class="text_area"
                            type="text" name="frontpage" id="frontpage" size="50" maxlength="250"
                            value="<?php echo $row->frontpage;?>" /></td>
            </tr>
            <tr>
                    <td width="15%" align="right" class="key"><label for="start_at"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_START_DATE'), JTEXT::_(''), '', JTEXT::_('DEAL_START_AT')); ?>
                    </label></td>
                    <td>
                    <!--
                        <input type="text" name="start_at" id="start_at" class="calendar" value="<?php echo $row->start_at ?>" size="50" readonly/> 
                        <button id="" class="btn calendar" onclick="return false;"><i class="icon-calendar"></i></button>
                    -->
                        <?php echo JHTML::calendar($row->start_at ,'start_at','start_at','%Y-%m-%d '.date ("H:i:s"), 'readonly'); ?> 
                    </td>
                    <td width="15%" align="right" class="key"><label for="end_at"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_END_DATE'), JTEXT::_(''), '', JTEXT::_('DEAL_END_AT')); ?>
                    </label></td>
                    <td>
                    <!--
                        <input type="text" name="end_at" id="end_at" class="calendar"
                                    value="<?php echo $row->end_at ?>" size="50" readonly> 
                        <button id="" class="btn calendar" onclick="return false;"><i class="icon-calendar"></i></button>
                    -->   

                        <?php echo JHTML::calendar($row->end_at ,'end_at','end_at','%Y-%m-%d '.date ("H:i:s"), 'readonly required'); ?> 
                    </td>
            </tr>
        </table>
    <hr style="border-bottom:1px solid #CCC;">
        <table class="admintable" style="width: 100%">
            <tr>
                    <td width="15%" align="right" class="key"><label for="max_coupon_qty"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_MAX_COUPON_QTY'), JTEXT::_(''), '', JTEXT::_('DEAL_MAX_COUPON_QUANTITY')); ?>
                    </label></td>
                    <td  width="35%"><input type="text" name="max_coupon_qty" id="max_coupon_qty"
                            value="<?php if(!empty($row->max_coupon_qty) && $row->max_coupon_qty!=0) echo $row->max_coupon_qty ; else echo '-1'; ?>">
                        <br><i><?php  echo JText::_('MSG_MAX_COUPON_QTY_DES');?></i>
                    </td>
                    
                    <td width="15%" align="left" class="key"><label for="max_buy_qty"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_MAX_BUY_QTY'), JTEXT::_(''), '', JTEXT::_('DEAL_MAX_BUY_QUANTITY')); ?>
                    </label></td>
                    <td  width="35%"><input type="text" name="max_buy_qty" id="max_buy_qty"
                            value="<?php if(!empty($row->max_buy_qty) && $row->max_buy_qty!=0) echo $row->max_buy_qty ; else echo '-1'; ?>">
                        <br><i><?php  echo JText::_('MSG_MAX_BUY_QTY_DES');?></i>
                            </td>
                            
            </tr>
            <tr>
                    <td width="15%" align="right" class="key"><label for="min_need_qty"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_MIN_QTY'), JTEXT::_(''), '', JTEXT::_('DEAL_MIN_QUANTITY')); ?>
                    * </label></td>
                    <td><input type="text" name="min_needed_qty" id="min_needed_qty"
                            value="<?php echo $row->min_needed_qty ?>"></td>
            
                    <td width="15%" align="right" class="key"><label for="cur_sold_qty"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_CUR_SOLD_QTY'), JTEXT::_(''), '', JTEXT::_('DEAL_CUR_SOLD_QTY')); ?>
                    </label></td>
                    <td><input type="text" name="cur_sold_qty" id="cur_sold_qty" 
                               value="<?php echo $nCurrentSoldQty; ?>" disabled>           
                    </td>
            </tr>
            <tr>
                <td align="right" class="key">
                    <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_MIN_QUANTITY_ORDER'),JTEXT::_(''), '', JTEXT::_('DEAL_MIN_QUANTITY_ORDER')); ?>
                </td>
                <td>
                    <input type="text" name="min_item_per_order" id="min_item_per_order"
                            value="<?php echo $row->min_item_per_order; ?>">
                </td>
                <td align="right" class="key">
                    <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_MAX_QUANTITY_ORDER'),JTEXT::_(''), '', JTEXT::_('DEAL_MAX_QUANTITY_ORDER')); ?>
                </td>
                <td>
                    <input type="text" name="max_item_per_order" id="max_item_per_order"
                            value="<?php echo $row->max_item_per_order; ?>">
                </td>
            </tr>
        </table>
    <hr style="border-bottom:1px solid #CCC;">
        <table class="admintable" style="width: 100%">
            <tr>
                    <td width="15%" align="right" class="key"><label for="pdt_cat_id"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_CITY'),JTEXT::_(''), '', JTEXT::_('DEAL_CITY')); ?>
                     * </label></td>
                    <td colspan="3">
                         <div>
                                   <div style="float: left;">
                                        <div><b><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_AVAILABLE_CITY'),JTEXT::_(''), '', JTEXT::_('AVAILABLE_CITY'));?></b></div>
                                        <div>
                                        
                                            <select name="city_id" >
                                                <option>--------Please select-------</option>
                                            <?php 
                                            for($i=0;$i<count($cityList);$i++){
                                                if($row->city_id == $cityList[$i]->id){
                                                    $s = "selected";
                                                }else{
                                                    $s='';
                                                }
                                                echo '<option value="'.$cityList[$i]->id.'" '.$s.">";
                                                echo $cityList[$i]->name;
                                                echo '</option>';
                                            }
                                            ?>
                                            </select>
                                        </div>

                                            
                             </div>
                    </td>
            </tr>
            <tr>
                    <td width="15%" align="right" class="key"><label for="pdt_cat_id"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_CATEGORY'),JTEXT::_(''), '', JTEXT::_('DEAL_CATEGORY')); ?>
                     * </label></td>
                    <td colspan="3">
                         <div>
                                   <div style="float: left;">
                                        <div><b><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_AVAILABLE_CATEGORY'),JTEXT::_(''), '', JTEXT::_('AVAILABLE_CATEGORY'));?></b></div>
                                        <div>
                                                    <?php
                                                    // create list location for combobox
                                                    $categoryJOptList = array();

                                                            if(count($this->dealCategoryList)!=0)
                                                            {
                                                                    $categoryList = $this->categoryList;
                                                                    $dealCategoryList = $this->dealCategoryList;
                                                                    for ( $i=0; $i < count($categoryList); $i++)
                                                                    {
                                                                            $available = false;
                                                                            for( $x=0 ; $x < count($dealCategoryList); $x++)
                                                                            {
                                                                                    if($categoryList[$i]->id == $dealCategoryList[$x]->id )
                                                                                    {
                                                                                            $available = true;
                                                                                    }
                                                                            }
                                                                            if(!$available)
                                                                            {
                                                                            $var = JHTML::_('select.option', $categoryList[$i]->id, $categoryList[$i]->name );
                                                                            array_push($categoryJOptList, $var);
                                                                            }
                                                                    }
                                                            }
                                                            else
                                                            {
                                                                    foreach ($this->categoryList as $item)
                                                                    {
                                                                            $var = JHTML::_('select.option', $item->id, $item->name );
                                                                            array_push($categoryJOptList, $var);
                                                                    }
                                                            }


                                                    echo JHTML::_('select.genericList',$categoryJOptList, 'pdt_cat_list', 'class="inputbox" size="10"  onDblClick="moveOptionsArrange(document.adminForm.pdt_cat_list, document.adminForm[\'pdt_cat_id[]\'])" multiple="multiple"', 'value','text',null );
                                                    ?>
                                                    </div>
                                            </div>
                                            <div style="float: left; padding: 40px 10px 0px 10px">
                                                            <input style="width: 50px" type="button" name="Button" value="&gt;" onClick="moveOptionsArrange(document.adminForm.pdt_cat_list, document.adminForm['pdt_cat_id[]']);" />
                                        <br /><br />
                                    <input style="width: 50px" type="button" name="Button" value="&lt;" onClick="moveOptionsArrange(document.adminForm['pdt_cat_id[]'],document.adminForm.pdt_cat_list)" />
                                        <br /><br />
                                            </div>
                                            <div style="float: left;">
                                                    <div><b><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_AVAILABLE_CATEGORY2'),JTEXT::_(''), '', JTEXT::_('CURRENT_CATEGORY'));?></b> </div>
                                                    <div>
                                                    <?php 
                                                        $dealCategoryJOptList = array();
                                                        if(!empty($this->dealCategoryList))
                                                        {
                                                            foreach($this->dealCategoryList as $item)
                                                            {
                                                                    $var = JHTML::_('select.option', $item->id, $item->name );
                                                                    array_push($dealCategoryJOptList, $var);
                                                            }
                                                        }
                                                            echo JHTML::_('select.genericList',$dealCategoryJOptList, 'pdt_cat_id[]', 'class="inputbox" size="10" onDblClick="moveOptionsArrange(document.adminForm[\'pdt_cat_id[]\'],document.adminForm.pdt_cat_list)" multiple="multiple"', 'value','text',null );
                                                    ?>
                                                 </div>
                                            </div>
                             </div>
                    </td>
            </tr>
            
            <tr>
                    <td width="15%" align="right" class="key"><label for="location_id"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_LOCATION'), JTEXT::_(''), '', JTEXT::_('DEAL_LOCATION')); ?>
                     * </label></td>
                    <td  colspan="3">
                            <div>
                                            <div style="float: left; ">
                                                    <div><b><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_AVAILABLE_LOCATION'), JTEXT::_(''), '', JTEXT::_('AVAILABLE_LOCATION'));?></b></div>
                                                    <div>
                                                    <?php
                                                    // create list location for combobox
                                                    $locationJOptList = array();

                                                            if(!empty($this->dealLocationList))
                                                            {
                                                                    $locationList = $this->locationList;
                                                                    $dealLocationList = $this->dealLocationList;
                                                                    for ($i=0 ; $i < count($locationList); $i++)
                                                                    {
                                                                            $locationAvailable = false;
                                                                            for($x = 0 ; $x < count($dealLocationList); $x++)
                                                                            {
                                                                                    if($locationList[$i]->id == $dealLocationList[$x]->id)
                                                                                    {
                                                                                            $locationAvailable = true;
                                                                                    }
                                                                            }
                                                                            if(!$locationAvailable)
                                                                            {
                                                                                    $var = JHTML::_('select.option', $locationList[$i]->id, $locationList[$i]->name );
                                                                                    array_push($locationJOptList, $var);
                                                                            }
                                                                    }
                                                            }
                                                            else
                                                            {
                                                                    foreach ($this->locationList as $item)
                                                                    {
                                                                            $var = JHTML::_('select.option', $item->id, $item->name );
                                                                            array_push($locationJOptList, $var);
                                                                    }
                                                            }

                                                    echo JHTML::_('select.genericList',$locationJOptList, 'location_list', 'class="inputbox" size="10" onDblClick="moveOptions(document.adminForm.location_list, document.adminForm[\'location_id[]\'])" multiple="multiple"' , 'value','text',null);
                                                    ?>
                                                    </div>
                                            </div>
                                            <div style="float: left; padding: 40px 10px 0px 10px">

                                                            <input style="width: 50px" type="button" name="Button" value="&gt;" onClick="moveOptions(document.adminForm.location_list, document.adminForm['location_id[]'])" />
                                        <br /><br />
                                    <input style="width: 50px" type="button" name="Button" value="&lt;" onClick="moveOptions(document.adminForm['location_id[]'],document.adminForm.location_list)" />
                                        <br /><br />

                                            </div>
                                            <div style="float: left; ">
                                                    <div><b><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_AVAILABLE_LOCATION2'), JTEXT::_(''), '', JTEXT::_('CURRENT_LOCATION'));?></b></div>
                                                    <div>
                                                            <?php 
                                                            $dealLocationJOptList = array();
                                                            foreach($this->dealLocationList as $item)
                                                            {
                                                                    $var = JHTML::_('select.option', $item->id, $item->name );
                                                                    array_push($dealLocationJOptList, $var);
                                                            }
                                                            echo JHTML::_('select.genericList',$dealLocationJOptList, 'location_id[]', 'class="inputbox" size="10" onDblClick="moveOptions(document.adminForm[\'location_id[]\'],document.adminForm.location_list)" multiple="multiple"' , 'value','text',null );?>
                                                    </div>
                                        </div>
                            </div>
                    </td>
            </tr>
            
        </table>
    <hr style="border-bottom:1px solid #CCC;">
        <table class="admintable" style="width: 100%">
            <tr>
                    <td width="15%" align="right" class="key"><label for="salesPerson"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_SALE_PERSON'),JTEXT::_(''), '', JTEXT::_('DEAL_SALE_PERSON')); ?>
                    </label></td>
                    <td width="35%"><?php
                    $salesPersonJOptList = array();
                    array_push($salesPersonJOptList, $emptyJOpt);
                    foreach ($this->salesPersonList as $item)
                    {
                            $var = JHTML::_('select.option', $item->id, $item->name );
                            array_push($salesPersonJOptList, $var);
                    }

                    echo JHTML::_('select.genericList',$salesPersonJOptList, 'sales_person_id', null , 'value','text',$row->sales_person_id );
                    ?></td>
                    
                    <td width="15%" align="right" class="key"><label for="merchant_id"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_MERCHANT'), JTEXT::_(''), '', JTEXT::_('DEAL_MERCHANT')); ?>
                    </label></td>
                    <td width="35%"><?php
                    $merchantJOptList = array();
                    array_push($merchantJOptList, $emptyJOpt);
                    foreach ($this->merchantList as $item)
                    {
                            $var = JHTML::_('select.option', $item->id, $item->name );
                            array_push($merchantJOptList, $var);
                    }

                    echo JHTML::_('select.genericList', $merchantJOptList, 'merchant_id', null , 'value','text',$row->merchant_id );
                    ?></td>
            </tr>
            <tr>
                    <td width="15%" align="right" class="key"><label for="commission_percent"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_COMMISSION_PERCENT'),JTEXT::_(''), '', JTEXT::_('DEAL_COMMISSION_PERCENT')); ?>
                    * </label></td>
                    <td><?php echo JHtml::_('select.genericList', $commissionList, 'commission_percent', null, 'value', 'text', is_null($row->commission_percent)? 10 : $row->commission_percent, 'commission_percent')?><span>%</span></td>
            </tr>
        </table>
    <hr style="border-bottom:1px solid #CCC;">
    <table class="admintable" style="width: 100%">
       <?php if(!empty($row->id)):?>
            <tr>
                    <td width="15%" align="right" class="key"><label><?php echo JText::_('CREATED_AT');?>:</label></td>
                    <td style="padding-bottom: 5px;"><?php echo DatetimeWrapper::getDisplayDatetime($row->created_at); ?></td>
                    <td width="15%" align="right" class="key"><label><?php echo JText::_('UPDATED_AT');?>:</label></td>
                    <td style="padding-bottom: 5px;"><?php echo DatetimeWrapper::getDisplayDatetime($row->updated_at); ?></td>
            </tr>
       <?php endif;?>
    </table>
</div>
<div class="att_tag tag1" style="display:none;">
        <table class="admintable tb_product" style="width: 100%; margin-top:20px; ">
            <tr>
                    <td align="right" valign="top" class="key"><label for="description"> <?php echo JHTML::tooltip(JTEXT::_('Product Information'),JTEXT::_(''), '', JTEXT::_('Product Information')); ?>
                    </label></td>
                    <td width="42%"><?php 
                    $editor = JFactory::getEditor();
                    echo $editor->display('description', $row->description, '100%', '200', '50', '3');
                    ?></td>
                    <td align="right" valign="top" class="key"><label for="description1"> <?php echo JHTML::tooltip(JTEXT::_('Flight Information'),JTEXT::_(''), '', JTEXT::_('Flight Information')); ?>
                    </label></td>
                    <td width="42%"><?php 
                    $editor1 = JFactory::getEditor();
                    echo $editor1->display('description1', $row->description1, '100%', '200', '50', '3');
                    ?></td>
            </tr>
            <tr>
                    <td align="right" valign="top" class="key"><label for="description2"> <?php echo JHTML::tooltip(JTEXT::_('Hotel Information'),JTEXT::_(''), '', JTEXT::_('Hotel Information')); ?>
                    </label></td>
                    <td width="42%"><?php 
                    $editor2 = JFactory::getEditor();
                    echo $editor2->display('description2', $row->description2, '100%', '200', '50', '3');
                    ?></td>
                    <td align="right" valign="top" class="key"><label for="description3"> <?php echo JHTML::tooltip(JTEXT::_('Order Method'),JTEXT::_(''), '', JTEXT::_('Order Method ')); ?>
                    </label></td>
                    <td width="42%"><?php 
                    $editor3 = JFactory::getEditor();
                    echo $editor3->display('description3', $row->description3, '100%', '200', '50', '3');
                    ?></td>
            </tr>
            <tr>    
                    <td align="right" valign="top" class="key"><label for="short_desc"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_S_DESCRIPTION'),JTEXT::_(''), '', JTEXT::_('DEAL_SHORT_DESC')); ?>
                    </label></td>
                    <td width="36%"><textarea style="width: 96%;height:230px;margin: 0px;" name="short_desc" 
                               id="short_desc" ><?php echo $row->short_desc;?></textarea></td>
                    <td></td>
                    <td></td>
            </tr>
            <tr>
                    <td align="right" style="vertical-align: middle;" class5="key"><label for="pic_dir"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_URL'),JTEXT::_(''), '', JTEXT::_('DEAL_PIC_URL')); ?>
                    </label></td>
                    <td style="padding:15px 0 25px 0;" colspan="3">
                        <ul class="deleteImage" id="deleteImage" style="margin:0px;">
                            <?php
                                $list_images = $row->pic_dir;
                                if (!EnmasseHelper::is_urlEncoded($list_images)) {
                                        $imageUrlArr = '';
                                    } else {
                                        $imageUrlArr = unserialize(urldecode($list_images));
                                    }
                            ?>
                            <?php 
                            if($imageUrlArr){
                                foreach ($imageUrlArr as $i => $imageUrl){ 
                                    $imageUrl = str_replace("\\", "/", $imageUrl);
                            ?>
                                    <li style="padding: 0;list-style: none;float:left;margin-right:10px; ">
                                        <img style="height:70px;" src="<?php echo JURI::root(); ?><?php echo $imageUrl; ?>" class="img-polaroid" /> 
                                        <img style="vertical-align: top; margin-left: -22px; margin-top: -1px;"
                                            src="<?php echo JURI::root(); ?>components/com_enmasse/theme/dark_blue/images/icon_delete.png" onclick="deleteImage('<?php echo $i; ?>');">
                                    </li>
                            <?php } } ?>
                        </ul>
                        
                        <input class="text_area" type="hidden" name="pic_dir" id="pic_dir"
                            size="50" value="<?php echo $row->pic_dir;?>" readonly />
                        <a rel="{handler: 'iframe', size: {x: 715, y: 300}}"
                                href="<?php echo 'index.php?option=com_enmasse&controller=uploader&task=displayall&tmpl=component&parentId=pic_dir&parent=deal'; ?>"
                                class="modal btn btnview" style='margin-top:54px;'>
                            <?php echo JText::_('DEAL_PIC_URL_LINK');?>
                        </a>
                    </td>
            </tr>
            <tr>
                    <td align="right" class="key"><label for="highlight"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_HIGHTLIGHT'),JTEXT::_(''), '', JTEXT::_('DEAL_HIGHLIHGTS')); ?>
                    </label></td>
                    <td><?php 
                    $editor = JFactory::getEditor();
                    echo $editor->display('highlight', $row->highlight, '100%', '200', '50', '3');
                    ?></td>
                    <td align="right" class="key"><label for="terms"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_CONDITION'),JTEXT::_(''), '', JTEXT::_('DEAL_TERMS_CONDITIONS')); ?>
                    </label></td>
                    <td><?php 
                    $editor = JFactory::getEditor();
                    echo $editor->display('terms', $row->terms, '100%', '200', '50', '3');
                    ?>
                    </td>
            </tr>
             <tr>
                    <td align="right" class="key"><label for="price_description"> <?php echo JHTML::tooltip(JTEXT::_('Price Description'),JTEXT::_(''), '', JTEXT::_('Price Description')); ?>
                    </label></td>
                    <td><?php 
                    $editor = JFactory::getEditor();
                    echo $editor->display('price_description', $row->price_description, '100%', '200', '50', '3');
                    ?></td>
                    <td></td>
                <td></td>
            </tr>       
            <tr>
                <td></td>
                <td></td>
                <td  class="key"><label for="master_term"> <?php echo JHTML::tooltip(JTEXT::_('Master T&C Name: '), JTEXT::_(''), '', JTEXT::_('Master T&C Name: ')); ?>
                    </label></td>
                    <td><input  size="300" type="text" name="master_term" id="master_term" 
                               value="<?php echo ($row->master_term)?$row->master_term:"除上述產品使用條款外，心程旅遊使用條款同時適用。"; ?>">           
                    </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td  class="key"><label for="master_term_url"> <?php echo JHTML::tooltip(JTEXT::_('URL'), JTEXT::_(''), '', JTEXT::_('URL')); ?>
                    </label></td>
                    <td><input size="300"type="text" name="master_term_url" id="master_term_url" 
                               value="<?php echo ($row->master_term_url)?$row->master_term_url:"http://www.stravel.com.hk/使用條款"; ?>" >           
                    </td>
            </tr>
        </table>
</div>
<div class="att_tag tag3" style="display:none;">
        <table class="admintable tb_product" style="width: 100%; margin-top:20px; ">
            <tr>
                    <td align="right" class="key"><label for="description"> <?php echo JHTML::tooltip(JTEXT::_('Insurance description'),JTEXT::_(''), '', JTEXT::_('Insurance description')); ?>
                    </label></td>
                    <td width="42%"><?php 
                    $editor = JFactory::getEditor();
                    echo $editor->display('insurance_description', $row->insurance_description, '100%', '200', '50', '3');
                    ?></td>
                   
            </tr>
          <tr>
                    <td width="15%" align="right" class="key"><label for="pdt_cat_id"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_CATEGORY'),JTEXT::_(''), '', JTEXT::_('Deal Insurance')); ?>
                     * </label></td>
                    <td colspan="3">
                         <div>
                                   <div style="float: left;">
                                        <div><b><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_AVAILABLE_CATEGORY'),JTEXT::_(''), '', JTEXT::_('Available Insurances'));?></b></div>
                                        <div>
                                                    <?php
                                                    // create list location for combobox
                                                    $selectedInList =explode(",", $row->insurances);
                                                    $insuranceJOptList = array();

                                                            if(count($this->dealInsuranceList)!=0)
                                                            {
                                                                    $insuranceList = $this->insuranceList;
                                                                    $dealInsuranceList = $this->dealInsuranceList;
                                                                    for ( $i=0; $i < count($insuranceList); $i++)
                                                                    {
                                                                            $available = false;
                                                                            for( $x=0 ; $x < count($dealInsuranceList); $x++)
                                                                            {
                                                                                    if($insuranceList[$i]->id == $dealInsuranceList[$x]->id )
                                                                                    {
                                                                                            $available = true;
                                                                                    }
                                                                            }
                                                                            if(!$available)
                                                                            {
                                                                            $var = JHTML::_('select.option', $insuranceList[$i]->id, $insuranceList[$i]->name,$selectedInList );
                                                                            array_push($insuranceJOptList, $var);
                                                                            }
                                                                    }
                                                            }
                                                            else
                                                            {
                                                                    foreach ($this->insuranceList as $item)
                                                                    {
                                                                            $var = JHTML::_('select.option', $item->id, $item->insurance_name,$selectedInList );
                                                                            array_push($insuranceJOptList, $var);
                                                                    }
                                                            }


                                                    echo JHTML::_('select.genericList',$insuranceJOptList, 'insurances[]', 'class="inputbox" size="10"  onDblClick="moveOptionsArrange(document.adminForm.insurances, document.adminForm[\'insurances[]\'])" multiple="multiple"', 'value','text',$selectedInList );
                                                    ?>
                                                    </div>
                                            </div>
                                           
                             </div>
                    </td>
            </tr>
            <tr>
                    <td align="right" class="key"><label for="description"> <?php echo JHTML::tooltip(JTEXT::_('Policy Date'),JTEXT::_(''), '', JTEXT::_('Policy Date')); ?>
                    </label></td>
                    <td width="42%">
                         <input class="text_area tierprice" type="text" name="policy_date" size="50" maxlength="250" value="<?php   echo $row->policy_date;  ?>" />
                   <!--  <?php 
                    $editor = JFactory::getEditor();
                    echo $editor->display('policy_date', $row->policy_date, '100%', '200', '50', '3');
                    ?> --></td>
                   
            </tr>
        </table>
</div>
<div class="att_tag tag2" style="display:none;">
        <table class="admintable" style="width: 100%; ">
            <tr>
        <td  width="15%" align="right"><?php echo JText::_('DEAL_DEAL_TYPE');?></td>
        <td>
                    <br/>
                        <label style="margin-right:25px;">
                                <input class="chkPriceDealType" type="radio" name="tier_pricing" value="1" id="PriceDealNormal"  style="border:none; margin:0;" <?php if($row->tier_pricing) echo 'checked="checked"';?> />
                                Tier pricing by inventory</label>                <label>
                                <input class="chkPriceDealType" type="radio" name="tier_pricing" value="0" id="PriceDealDynamic" style="border:none;  margin:0;" <?php if(!($row->tier_pricing)) echo 'checked="checked"';?> />
                                Price Options (Attributes / add-on)</label>
                    <br/>
                    Max. Total Qty: <span class="maxTotalTypes"><?php if(!empty($row->max_coupon_qty) && $row->max_coupon_qty!=0) echo $row->max_coupon_qty ; else echo '-1'; ?></span>
                    <br/>
        </td>
    </tr>
    <?php 
        $deal_price_step = unserialize($row->price_step);
        $root = JURI::root();
        $prefix = $this->currencyPrefix;
        $i = 0;
    ?>
    <!--Tier Pricing--> 
        <?php
                if($deal_price_step){       
                foreach ($deal_price_step as $k=>$v) { 
                        $min_quantity_arr  = $v;
                        $price_step_arr  = $k;
                        if($min_quantity_arr != 0)
                        {
        ?>
                                <tr class="normalDeal">
                                        <td width="15%" align="right" class="key"><label>Step</label></td>
                                        <td>
                                                <?php echo $this->currencyPrefix;?>
                                                <input class="text_area tierprice" type="text" name="price_step[]" size="50" maxlength="250" value="<?php   echo $price_step_arr;  ?>" readonly/>
                                                <span style="margin:5px 10px 0 10px;">Min Quantity</span>
                                                <input class="tierqty" type="text" name="min_quantity_step[]" size="10" maxlength="50" value="<?php echo $min_quantity_arr; ?>" readonly/>
                                                <img class='delete' onclick='deletetQuestion(this);' width='20' height='20' src="<?php echo JURI::root(); ?>administrator/components/com_enmasse/images/delete.png"/>
                                        </td>
                                </tr>
        <?php   } } }?> 
                <tr id ="addStep"  class="normalDeal">
                        <td width="15%" align="right" class="key"><label>If more than </label></td>
                        <td>            
                                <input type="hidden" name="lastMinQtyStep" id="lastMinQtyStep" value="0" />
                                <input class="text_area" type="text" name="min_quantity_step[]" id="min_quantity_step" size="10" maxlength="50"/>       
                                <span style="margin:5px 10px 0 10px;"> the price will be</span>
                                <?php echo $this->currencyPrefix;?>
                                <input class="text_area add_price" type="text" name="price_step[]" id="price_step" size="50" maxlength="250"/>
                                <img id="add" width="20" height="20" src="<?php echo JURI::root() ?>administrator/components/com_enmasse/images/add.png"/>
                        </td>
                </tr>
        
                <!--Tier Pricing-->
                <tr id="DynamicAttributeDeal">
                        <td width="15%" align="right" class="key"><label for="name"> <?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_TYPE'), JTEXT::_('TOOLTIP_DEAL_TYPE_TITLE'), '', JTEXT::_('DEAL_TYPE')); ?></label></td>
                        <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="adminlist" id="dealTypeTbl">
                                        <thead>
                                        <tr>
                                                <th>Attribute Name</th>
                                                <th><span title="">Original Price</span></th>
                                                <th><span title="">Actual Price</span></th>
                                                <th><span title="">Quantity</span></th>
                                                <th>Attribute Code</th>
                                                <th>Sold Quantity</th>
                                                <th class="cellTools">&nbsp;</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                                <!-- tin -->
                                        <?php
                                                $i_row = 0;
                                                if($this->dealTypeList){
                                                foreach($this->dealTypeList as $dealType)
                                                {
                                                        //set style of table row
                                                        if($dealType->status == 0)
                                                                $class = 'class="row'.$i_row.' unapply-deal"';
                                                        else
                                                                $class = 'class="row'.$i_row.'"';
                                                        $i_row++;

                                                        //display deal type
                                                        ?>
                                                        <tr id="deal-type-1" <?php echo ($class); ?>>
                                                        <td>
                                                        <input type="hidden" name="idType[]" value="<?php echo($dealType->id) ?>">
                                                        <span><?php echo($dealType->name)?></span><input class="type-edit-name" type="hidden" name="txtTypeName[]" value="<?php echo($dealType->name)?>" maxlength="45">
                                                                                                        </td>
                                                        <td><span><?php echo($dealType->origin_price); ?> </span><input class="type-edit-origin-price"  type="hidden" name="txtOriginalPrice[]" value="<?php echo($dealType->origin_price); ?>"></td>
                                                        <td><span><?php echo($dealType->price);?> </span><input class="type-edit-price" type="hidden" name="txtPrice[]" value="<?php echo($dealType->price);?>"></td>
                                                         <td><span><?php echo($dealType->qty); ?> </span><input type="hidden"  class="typeQtyInput type-edit-qty" name="txtQty[]" value="<?php echo($dealType->qty); ?>" maxlength="45"></td>
                                                      
                                                        <td><?php echo($dealType->code); ?> <input type="hidden" name="txtTypeCode[]" value="<?php echo($dealType->code); ?>" maxlength="45"></td>
                                                        <td><?php echo($dealType->sold_qty); ?> </td>
                                                         <td>
                                                                <input class="btnEdit"  type="submit" name="btn-edit[]" id="btn-delete-<?php echo($dealType->id) ?>"  value="Edit" />
                                                                <input class="btnApply" type="submit" id="btn-apply-1" name="btn-apply[]" value="Apply" <?php if($dealType->status == 1){echo ('style="display:none;"');}?>  />
                                                                <input class="btnUnApply" type="submit" id="btn-apply-1" name="btn-apply[]" value="Unapply" <?php if($dealType->status == 0){echo ('style="display:none;"');}?> />
                                                                <input class="btnDelete"  type="submit" name="btn-delete[]" id="btn-delete-1"  value="Delete" />
                                                                
                                                                <input type="hidden"  name="type-apply[]" class="isApply" value="<?php
                                                                echo($dealType->status);
                                                                 ?>" />
                                                        </td>
                                                </tr>
                                                        <?php   
                                                }
                                                }
                                        ?>
                                        </tbody>
                                        <tfoot class="addTypeForm">
                                                <tr>
                                                        <td><input class="txtTypeName" type="text" name="txtTypeNameAdd" id="txtTypeName-1" title="Enter type name" value="" maxlength="45" /></td>
                                                        <td><input class="txtOriginalPrice" type="text" name="txtOriginalPriceAdd" id="txtOriginalPrice-1" title="Enter original price" value="" /></td>
                                                        <td><input class="txtPrice" type="text" name="txtPriceAdd" id="txtPrice-1" title="Enter price" value="" /></td>
                                                        <td><input class="txtQty" type="text" name="txtQtyAdd" id="txtQty" title="Enter Quantity" value="" /></td>
                                                        <td></td>
                                                        <td><input type="submit" id="btnEditSubmit" name="btnEditSubmit" value="Save Edit" style="display:none"/></td>
                                                        <td><input type="submit" id="btn-add-type" name="btn-add-type" value="Add Type" /></td>
                                                        
                                                </tr>
                                        </tfoot>
                                </table>

                        </td>
                </tr>
                <tr class="normalDeal">
                    <td colspan="2">
                        <br><br>
                        <i>
                        **Tier pricing: This feature allows you to set the deals price based on the quality that has been sold. For example a deal’s price is $100 if 0- 10 deals are sold, $90 if 11-20 deals are sold, $80 if 21-30 deals are sold and so on and so forth. It can also be programmed to work backwards where the price starts low and moves higher as the quantity sold of the deal gets higher. 
<!--                        <br><br>
                        Ex: A particular product if 1-10pcs sold, the price is $80 each, 11-20pcs, the price is $90 each, 21 and above, the price is $100.-->
                        </i>
                    </td>
                </tr>
                <tr class="DynamicAttributeDeal">
                    <td colspan="2">
                        <br><br>
                        <i>
                        **deal pricing options: This feature allows you to give multiple combo options or pricing options for a particular deal.
                        For example a dinner for 2 is sold for $30. Now this dinner deal can have 3 options to choose from, chicken for $30, steak for $40, and Salmon for $50.
                        </i>
                    </td>
                </tr>
        </table>
</div>
<!--<div class="att_tag tag3" style="display:none;">    
        <table class="admintable" style="width: 100%; margin-top:20px; ">
            <tr>
                    <td width="15%" align="right" class="key"><label for="published"><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_PUBLISHED'),JTEXT::_(''), '', JTEXT::_('PUBLISHED')); ?></label>
                    </td>
                    <td><?php 
                    if ($row->published == null)
                    echo JHTML::_('select.booleanlist', 'published', 'class="inputbox"', 1);
                    else
                    echo JHTML::_('select.booleanlist', 'published', 'class="inputbox"', $row->published);
                    ?></td>
            </tr>
            <tr>
                    <td width="15%" align="right" class="key"><label for="auto_confirm"><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_DEAL_AUTO_CONFIRM'),JTEXT::_(''), '', JTEXT::_('DEAL_AUTO_CONFIRM')); ?>:</label>
            </td>
                    <td>
                            <?php echo JHtml::_('select.booleanlist', 'auto_confirm','class="inputbox"', $row->auto_confirm)?>
                    </td>
            </tr>
            
            <?php if(!empty($row->id)):?>
                    <tr>
                            <td width="15%" align="right" class="key"><label><?php echo JText::_('CREATED_AT');?>:</label></td>
                            <td style="padding-bottom: 5px;"><?php echo DatetimeWrapper::getDisplayDatetime($row->created_at); ?></td>
                    </tr>
                    <tr>
                            <td width="15%" align="right" class="key"><label><?php echo JText::_('UPDATED_AT');?>:</label></td>
                            <td style="padding-bottom: 5px;"><?php echo DatetimeWrapper::getDisplayDatetime($row->updated_at); ?></td>
                    </tr>
            <?php endif;?>
        </table>
</div>-->
</div>

<input type="hidden" name="position" value="<?php echo $row->position; ?>" />
<input type="hidden" name="id"  value="<?php echo $row->id; ?>" />
<input type="hidden" name="option"  value="<?php echo $option;?>" />
<input type="hidden" name="controller" value="deal" />
<input type="hidden" name="cid[]"   value="<?php echo $row->id; ?>" />
<input type="hidden" name="task" value="" />
</div>
</form>
<script language="javascript" type="text/javascript"> 
jQuery.noConflict();
jQuery(document).ready(function(){
    toggleApplyDeal();  
    deleteDealRow();
    jQuery('#btn-add-type').unbind().bind('click', function(e){
        e.preventDefault();
        addDealTypes();
        return false;
    });
    checkPriceTyleDeal();
    
    arrangeCategory(document.adminForm['pdt_cat_id[]']);
    checkMinQtyStep();
    setTypeQty();
    jQuery('.attribute-tab').click(function(){
        setTypeQty();
    });
    jQuery('#txtQty').change(function(){
        var max = jQuery('#txtQty').attr('rel');
        if(jQuery('.editing').length>0){
            var qty = jQuery('.editing .type-edit-qty').val();
            max = parseInt(max) + parseInt(qty);
        }
        if(parseInt(jQuery('#txtQty').val())>parseInt(max)){
            alert("Qty cannot greater than "+max);

            jQuery('#txtQty').val(max);
        }
    })
    jQuery('#max_coupon_qty').change(function(){
        jQuery('.maxTotalTypes').text(jQuery(this).val());
    });
});
function setTypeQty(){
   
        var max_coupon_qty = jQuery('#max_coupon_qty').val();
        // console.log('max_coupon_qty:'+max_coupon_qty);
        jQuery('.typeQtyInput:not(.delete)').each(function(){
            var val = jQuery(this).val();
            max_coupon_qty-= val;
            // console.log('val:'+val);

        })
        jQuery('#txtQty').val(max_coupon_qty);
        jQuery('#txtQty').attr('rel',max_coupon_qty);
        if(max_coupon_qty<=0){
            jQuery('.addTypeForm').hide();
        }else{
            jQuery('.addTypeForm').show();
        }
   
}

function checkMinQtyStep(){
            var lastMinQtyStep = jQuery("#lastMinQtyStep").val();   
            lastMinQtyStep = parseInt(lastMinQtyStep);  
            var all_qty = jQuery("input.tierqty[type=text]");
            
            var max = 0;
            jQuery.each(all_qty, function(i, val) {
                if(val.value != lastMinQtyStep && parseInt(val.value) > max){
                    max = val.value;
                }
            });
            document.getElementById('lastMinQtyStep').value = max;
}
function checkPriceTyleDeal() {
    jQuery('.chkPriceDealType').each(function(i, chk){
        chk = jQuery(chk);
        chk.bind('change', function(){
            showHideType();
        });
    });
    function showHideType(){
        if(jQuery('#PriceDealNormal').is(':checked')) {
            jQuery('.normalDeal').attr('style', '');
            document.getElementById('DynamicAttributeDeal').style.display = 'none';
                        jQuery('.DynamicAttributeDeal').hide();
        } else {
            jQuery('.normalDeal').attr('style', 'display:none;');
            document.getElementById('DynamicAttributeDeal').style.display = '';
                        jQuery('.DynamicAttributeDeal').show();
        }
    }
    showHideType();
}
function addDealTypes(){
    var dealTypeTbl = jQuery('#dealTypeTbl');
    if(dealTypeTbl.length > 0){
        var arrTRs  = jQuery('#dealTypeTbl tbody tr');
        var strVal  = jQuery('#dealTypeTbl tfoot tr');
        var strTypeName = strVal.find('input.txtTypeName').val();
        var strOriginalPrice = strVal.find('input.txtOriginalPrice').val();
        var strPrice = strVal.find('input.txtPrice').val();
        var strQty = strVal.find('input.txtQty').val();
        //var strTypeCode = strVal.find('input.txtTypeCode').val();     
        /*validate empty field*/
        strTypeName = htmlEntities(strTypeName);
        /*strTypeCode = htmlEntities(strTypeCode);*/
        
        if(jQuery.trim(strTypeName) == "") {
            alert("Please enter type name!");
            strVal.find('input.txtTypeName').focus();
            return false;
        }
        
        if(jQuery.trim(strOriginalPrice) == "") {
            alert("Please enter original price!");
            strVal.find('input.txtOriginalPrice').focus();
            return false;
        } else {
            var re = /^[-+]?\d*\.?\d+(?:[eE][-+]?\d+)?$/;
            if(!re.test(strOriginalPrice)) {
                alert("Original Price value is numeric!");
                strVal.find('input.txtOriginalPrice').focus();
                strVal.find('input.txtOriginalPrice').select();
                return false;
            }
        }
        
        if(jQuery.trim(strPrice) == "") {
            alert("Please enter price!");
            strVal.find('input.txtPrice').focus();
            return false;
        }  else {
            var re = /^[-+]?\d*\.?\d+(?:[eE][-+]?\d+)?$/;
            if(!re.test(strPrice)) {
                alert("Price value is numeric!");
                strVal.find('input.txtPrice').focus();
                strVal.find('input.txtPrice').select();
                return false;
            }
        }
        
        /*if(jQuery.trim(strTypeCode) == "") {
            alert("Please enter type code!");
            strVal.find('input.txtTypeCode').focus();
            return false;
        }*/
        
        //check duplicate
        
        if(parseFloat(strPrice) > parseFloat(strOriginalPrice))
        {
            alert("Actual price can't be greater than orginal price");
            return false;
        }
        if(parseFloat(strPrice) < 0 )
        {
            alert("Actual price must be greater than 0");
            return false;
        }
        if(parseFloat(strOriginalPrice) < 0 )
        {
            alert("Orginal price must be greater than 0");
            return false;
        }
        if((arrTRs.length + 1) % 2 == 0) {
            var strClass = 'row1';
        } else {
            var strClass = 'row0';
        }
        
        addDealTypeRow(arrTRs.length + 1, strClass, strTypeName, strOriginalPrice, strPrice,strQty);
        setTypeQty();

        return false;
    }
}
function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}
function addDealTypeRow(i, strClass, strTypeName, strOriginalPrice, strPrice,strQty) {
    var tbody  = jQuery('#dealTypeTbl tbody');
    if(tbody.length > 0) {
        var strRow = '<tr id="deal-type-'+ i +'" class="'+ strClass +'">';
        strRow += '<td> <input type="hidden" name="idType[]" value=""><span>'+ strTypeName +'</span><input class="type-edit-name" type="hidden" name="txtTypeName[]" value="'+ strTypeName  + '"></td>';
        strRow += '<td><span>'+ strOriginalPrice +'</span><input class="type-edit-origin-price" type="hidden" name="txtOriginalPrice[]" value="'+ strOriginalPrice  + '"></td>';
        strRow += '<td><span>'+ strPrice +'</span><input type="hidden" class="type-edit-price" name="txtPrice[]" value="'+ strPrice  + '"></td>';
        strRow += '<td><span>'+ strQty +'</span><input type="hidden" class="typeQtyInput type-edit-qty" name="txtQty[]" value="'+ strQty  + '"></td>';
        strRow += '<td></td>';
        strRow += '<td>0</td>';
        strRow += '<td> <input class="btnEdit"  type="submit" name="btn-edit[]" id="btn-delete-"  value="Edit" /><input class="btnApply" type="submit" id="btn-apply-'+ i +'" name="btn-apply[]" value="Apply" style="display:none;"/><input class="btnUnApply" type="submit" id="btn-apply-'+ i +'" name="btn-apply[]" value="Unapply"  style="display:inline;"/><input type="submit" class="btnDelete" name="btn-delete[]" id="btn-delete-'+ i +'"  value="Delete" /><input type="hidden"  name="type-apply[]" class="isApply" value="1" /></td></tr>';
        
        
        
        tbody.append(strRow);
        
        deleteDealRow();
        
        var strVal  = jQuery('#dealTypeTbl tfoot tr');
        var strTypeName = strVal.find('input.txtTypeName').val('');
        var strOriginalPrice = strVal.find('input.txtOriginalPrice').val('');
        var strPrice = strVal.find('input.txtPrice').val('');
        /*var strTypeCode = strVal.find('input.txtTypeCode').val('');       */
        
    }
}
function toggleApplyDeal(){
    jQuery('#dealTypeTbl .btnApply').live({
        'click': function(e) {
            jQuery(this).css('display', 'none');
            jQuery(this).parent().find('.btnUnApply').css('display', 'inline');
            jQuery(this).parent().find('.isApply').val(1);
            jQuery(this).parent().parent().toggleClass('unapply-deal');
            return false;
        }
    });
    
    jQuery('#dealTypeTbl .btnUnApply').live({
        'click': function(e) {
            jQuery(this).css('display', 'none');
            jQuery(this).parent().find('.btnApply').css('display', 'inline');
            jQuery(this).parent().find('.isApply').val(0);
            jQuery(this).parent().parent().toggleClass('unapply-deal');
            return false;
        }
    });
    jQuery('#dealTypeTbl .btnEdit').live({
        'click': function(e) {
            jQuery('.editing').removeClass('editing');
            jQuery(this).parent().parent().addClass('editing');
            var oPrice = jQuery(this).parent().parent().find('.type-edit-origin-price').val();
            var price = jQuery(this).parent().parent().find('.type-edit-price').val();
            var name = jQuery(this).parent().parent().find('.type-edit-name').val();
            var qty = jQuery(this).parent().parent().find('.type-edit-qty').val();

            jQuery('#txtTypeName-1').val(name);
            jQuery('#txtOriginalPrice-1').val(oPrice);
            jQuery('#txtPrice-1').val(price);
            jQuery('#txtQty').val(qty);

            

            jQuery('.addTypeForm').show();
            jQuery('#btnEditSubmit').show();
            return false;
        }
    });
    jQuery('#btnEditSubmit').live({
        'click': function(e) {
            var oPrice =  jQuery('#txtOriginalPrice-1').val();
            var price =  jQuery('#txtPrice-1').val();
            var name = jQuery('#txtTypeName-1').val();
            var qty = jQuery('#txtQty').val();

            if(parseFloat(price) > parseFloat(oPrice))
            {
                alert("Actual price can't be greater than orginal price");
                return false;
            }
            if(parseFloat(price) < 0 )
            {
                alert("Actual price must be greater than 0");
                return false;
            }
            if(parseFloat(oPrice) < 0 )
            {
                alert("Orginal price must be greater than 0");
                return false;
            }

            jQuery(this).parent().parent().addClass('editing');
            jQuery('.editing .type-edit-origin-price').val(oPrice);
            jQuery('.editing .type-edit-origin-price').parent().find('span').text(oPrice);

            jQuery('.editing .type-edit-price').val(price);
            jQuery('.editing .type-edit-price').parent().find('span').text(price);

            jQuery('.editing .type-edit-name').val(name);
            jQuery('.editing .type-edit-name').parent().find('span').text(name);

            jQuery('.editing .type-edit-qty').val(qty);
            jQuery('.editing .type-edit-qty').parent().find('span').text(qty);

            jQuery('.editing').removeClass('editing');
           
            jQuery('#txtTypeName-1').val('');
            jQuery('#txtOriginalPrice-1').val('');
            jQuery('#txtPrice-1').val('');
            jQuery('#txtQty').val(0);

            // jQuery('.addTypeForm').show();
            jQuery('#btnEditSubmit').hide();
            setTypeQty();
            return false;
        }
    });
}

function deleteDealRow(){
    jQuery('#dealTypeTbl .btnDelete').each(function(index, btnDel){
        btnDel = jQuery(btnDel);
        btnDel.unbind('click').bind({
            'click': function(e) {
                if(confirm('Are you want delete this row?')) {
                    jQuery(btnDel.parent().parent()).css('display', 'none');
                    jQuery(btnDel.parent().parent()).find('input').attr('name','removeNameInput');
                    jQuery(btnDel.parent().parent()).find('input').addClass('delete');
                    setTypeQty();
                }
                return false;
            }
        });
    });
}

function addImg(imgs){
    jQuery("#pic_dir").val(imgs);
    return true;
}

</script>
<script>
    jQuery(document).ready(function(){
        var tag = jQuery('.headerAttTag a');

        tag.each(function(index, tab){
            jQuery(tab).unbind("click");
            jQuery(tab).click(function(){
                jQuery('.att_tag').hide();
                jQuery(tag).removeClass('active');

                jQuery('.tag'+index).show();
                jQuery(tab).addClass('active');
            });
        });
    });
</script>
<script>
    function deletetQuestion(img) {
                    checkMinQtyStep();

                    var img = jQuery(img);
            jQuery(img.parent().parent()).remove();
        }
    function isNumber(value){
            var str = /^[0-9]*$/;
            return str.test(value);
    }   
    jQuery(function(){         
                    jQuery("#add").click(function(){    
                            var price = jQuery("#price_step").val();
                            var min_quantity = jQuery("#min_quantity_step").val();
                            var lastMinQtyStep = jQuery("#lastMinQtyStep").val();           
                            var first_price = jQuery("#price").val();

                            var all_price = jQuery("input.tierprice[type=text]");
                            var all_qty = jQuery("input.tierqty[type=text]");



                            if(parseInt(first_price) == price){
                                    alert('Price can not be duplicate');
                                    return false;
                            }

                            var checkDuplicate = false;
                            jQuery.each(all_price, function(i, val) {
                                    if(val.value == price){
                                            alert('Price can not be duplicate');
                                            checkDuplicate = true;
                                            return false;
                                    }
                            });
                            if(checkDuplicate == true)
                            {
                                    return false;
                            }

                            if(!price || price == '0') {
                                    alert('Price is not null');
                                    return false;
                            }
                            else if(!isNumber(price)){
                                    alert('Price is integer');
                                    return false;
                            }
                            if(parseInt(min_quantity) <= parseInt(lastMinQtyStep)) {
                                    alert('Quantity could not smaller than last one');
                                    jQuery("#min_quantity_step").val('');
                                    return false;
                            }
                            else if(!min_quantity || min_quantity == '0'){
                                    alert('Price is not null');
                                    return false;
                            }   
                            else if(!isNumber(min_quantity)){
                                    alert('Quantity is integer');
                                    return false;
                            }           

                            var root = <?php echo "'".$root."'"; ?>;
                            var prefix = <?php echo "'".$prefix."'"; ?>;
                            var strHTML = "<tr class='normalDeal'><td width='100' align='right' class='key'><label>If more than </label></td>";
                            strHTML += "<td>";
                            strHTML += "<input class='tierqty' type='text' name='min_quantity_step[]' size='10' maxlength='50' value='"+ min_quantity +"' readonly/>";
                            strHTML += "<span style='margin:5px 10px 0 10px;'> the price will be </span>";
                            strHTML += prefix+' ';
                            strHTML += "<input class='text_area tierprice' type='text' name='price_step[]' size='50' maxlength='250' value='"+ price +"' readonly/>";
                            strHTML += ' ';
                            strHTML += "<img class='delete' onclick='deletetQuestion(this);'  width='20' height='20' src ='" + root + "administrator/components/com_enmasse/images/delete.png'/>";
                            strHTML += "</td></tr>";
                            jQuery("#addStep").before(strHTML);
                            jQuery("#price_step").val(''); jQuery("#min_quantity_step").val('');
                            document.getElementById('lastMinQtyStep').value = min_quantity;
                    });
            });
</script>

<script>
    function deleteImage(index_img){
        jQuery.ajax({
            url: 'index.php?option=com_enmasse&controller=uploader&task=ajaxRemoveImg',
            type: 'POST',
            dataType: 'html', //'json'
            data: {
                'index_img':index_img
            },
            success: function(resp){
                jQuery('#deleteImage').html(resp);
            }
        });
    }

    function showImg(){
        deleteImage('999');
        return true;
    }
</script>