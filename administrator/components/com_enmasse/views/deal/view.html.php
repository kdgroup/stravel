<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."toolbar.enmasse.html.php");

class EnmasseViewDeal extends JViewLegacy
{
	function display($tpl = null)
	{

		 
		$task = JRequest::getWord('task');
					
		if($task == 'edit' || $task == 'add')
		{
                        TOOLBAR_enmasse::_SMENU();
			$cid 	= JRequest::getVar( 'cid', array(0), '', 'array' );
			$row = JModelLegacy::getInstance('deal','enmasseModel')->getById($cid[0]);
			if(!empty($row->id))
			{
				TOOLBAR_enmasse::_DEAL_DETAIL($row->status);
			}
			else
			{
				TOOLBAR_enmasse::_DEAL_NEW();
			}
                        //set null if date start_at, date end_at is default
                        if($row->start_at == '0000-00-00 00:00:00'){
                            $row->start_at = '';
                        }
                        if($row->end_at == '2049-12-30 00:00:00'){
                            $row->end_at = '';
                        }
                        
			$this->deal = $row ;
                        $oSession = JFactory::getSession();
                        $oSession->set('deal_list_images',$row->pic_dir);
                        
			$this->currencyPrefix = JModelLegacy::getInstance('setting','enmasseModel')->getCurrencyPrefix();
			$this->currencyPostfix = JModelLegacy::getInstance('setting','enmasseModel')->getCurrencyPostfix();
			$this->statusList = EnmasseHelper::$DEAL_STATUS_LIST;
			$this->locationList = JModelLegacy::getInstance('location','enmasseModel')->listAllPublished();
			$this->categoryList = JModelLegacy::getInstance('category','enmasseModel')->listAllPublished();
			$this->cityList = JModelLegacy::getInstance('city','enmasseModel')->listAllPublished();
			$this->insuranceList = JModelLegacy::getInstance('insurance','enmasseModel')->listAllPublished();
			$this->salesPersonList = JModelLegacy::getInstance('salesPerson','enmasseModel')->listAllPublished();
			$this->merchantList = JModelLegacy::getInstance('merchant','enmasseModel')->listAllPublished();	
			if($row->id)
			{
								
				$dealCategoryIdList = JModelLegacy::getInstance('dealcategory','enmasseModel')->getCategoryByDealId($row->id);
				//------------------------------
				// get category list of deal
				if(count($dealCategoryIdList)!=0)
					$dealCategoryList = JModelLegacy::getInstance('category','enmasseModel')->getCategoryListInArrId($dealCategoryIdList);
				else
				    $dealCategoryList = array();
				 // assign category list to template  
			    $this->assignRef('dealCategoryList',$dealCategoryList);
			    
			    $dealLocationIdList = JModelLegacy::getInstance('deallocation','enmasseModel')->getLocationByDealId($row->id);
			    //------------------------------
				// get location list of deal
			    if(count($dealLocationIdList)!=0)
			    	$dealLocationList = JModelLegacy::getInstance('location','enmasseModel')->getLocationListInArrId($dealLocationIdList);
			    else 
			       $dealLocationList = array();
			    $this->assignRef('dealLocationList',$dealLocationList);
			    
			    // Set a flag for the template to recognize we are creating a new deal
                            $this->bNewDeal = false;   
				
				//get type list of deal
				$dealTypeList = JModelLegacy::getInstance('dealtype','enmasseModel')->getDealTypeListByIdDeal($row->id);
				$this->assignRef('dealTypeList',$dealTypeList);
			}
			else
			{
                               // $this->categoryList = array();
                               // $this->locationList = array();
                                if(!$this->categoryList){
                                    echo('<script>
                                    alert("\t\t There is no data of category.\n please create information before creating deals.");
                                    window.location.href = "'.JURI::root().'administrator/index.php?option=com_enmasse&controller=category&task=add";
                                    </script>');
                                }else if(!$this->locationList){
                                    echo('<script>
                                    alert("\t\t There is no data of location.\n please create information before creating deals.");
                                    window.location.href = "'.JURI::root().'administrator/index.php?option=com_enmasse&controller=location&task=add";
                                    </script>');
                                }
				$dealCategoryList = array();
				$this->assignRef('dealCategoryList',$dealCategoryList);
				$dealLocationList = array();
				$this->assignRef('dealLocationList',$dealLocationList);

				
				
				
				$this->bNewDeal = true;
			}
                                             
		}
		else //Task == Show
		{
                        $oSession = JFactory::getSession();
                        $oSession->set('deal_list_images','');
                        
			TOOLBAR_enmasse::_SMENU();
			$nNumberOfDeals = JModelLegacy::getInstance('deal','enmasseModel')->countAll();
			if($nNumberOfDeals==0)
			{
				TOOLBAR_enmasse::_DEAL_EMPTY();
			}
			else
			{
				TOOLBAR_enmasse::_DEAL();
			}
			
            $filter 	= JRequest::getVar('filter');
            $filter['code'] = isset($filter['code']) ? $filter['code'] : '';
            $filter['location_id'] = isset($filter['location_id']) ? $filter['location_id'] : '';
            $filter['category_id'] = isset($filter['category_id']) ? $filter['category_id'] : '';
            $filter['name'] = isset($filter['name']) ? $filter['name'] : '';
            $filter['published'] = isset($filter['published']) ? $filter['published'] : '';
            $filter['status'] = isset($filter['status']) ? $filter['status'] : '';
            
			$dealFromLocation = null;
			$dealFromCategory = null;

			if(!empty($filter['location_id']) && trim($filter['location_id'])!= '')
			 	$dealFromLocation = JModelLegacy::getInstance('dealLocation','enmasseModel')->getDealByLocationId($filter['location_id']);
			if(!empty($filter['category_id']) && trim($filter['category_id'])!= '')

                        {
                            //get children category 
                            $listChildren = JModelLegacy::getInstance('dealCategory','enmasseModel')->getChildrenCategory($filter['category_id']);
                            if($listChildren){
                                $filter['category_id'] = $filter['category_id'].','.implode(',', $listChildren);
                            }
                            //get all deal belong selected category
                            $dealFromCategory = JModelLegacy::getInstance('dealCategory','enmasseModel')->getDealByListCategoryId($filter['category_id']);
                        }

			$dealList 		= JModelLegacy::getInstance('deal','enmasseModel')->search($filter['code'], $filter['name'],$dealFromLocation,$dealFromCategory,$filter['published'], $filter['status']);
			if(($filter['location_id'] && !$dealFromLocation) || ($filter['category_id'] && !$dealFromCategory))
                        {
                            $dealList = array();
                        }
                        /// load pagination
			$pagination =JModelLegacy::getInstance('deal','enmasseModel')->getPagination($filter['code'], $filter['name'],$dealFromLocation,$dealFromCategory,$filter['published'], $filter['status']);
			$state = $this->get( 'state' );
			// get order values
			$order['order_dir'] = $state->get( 'filter_order_dir' );
            $order['order']     = $state->get( 'filter_order' );
			for($i=0; $i < count($dealList); $i++)
			{
				$dealCategoryIdList = JModelLegacy::getInstance('dealcategory','enmasseModel')->getCategoryByDealId($dealList[$i]->id);
				$dealLocationIdList = JModelLegacy::getInstance('deallocation','enmasseModel')->getLocationByDealId($dealList[$i]->id);
				
				//----------------------------------------------
				// get list of category name
				if(count($dealCategoryIdList)!=0)
					$categoryList = JModelLegacy::getInstance('category','enmasseModel')->getCategoryListInArrId($dealCategoryIdList);
				else
				   $categoryList = null;

				   
				 //----------------------------------------------
				// get list of location name
				if(count($dealLocationIdList)!=0)
			    	$locationList = JModelLegacy::getInstance('location','enmasseModel')->getLocationListInArrId($dealLocationIdList);
				else
				   $locationList = null;
				   
				   
				if(count($locationList)!=0 && $locationList!=null)
					$dealList[$i]->location_name 		= $locationList;
				else 
					$dealList[$i]->location_name 		= null;
					
				if(count($categoryList)!=0 && $categoryList!=null)
					$dealList[$i]->category_name 		= $categoryList;
				else
				    $dealList[$i]->category_name 		= null;
				$dealList[$i]->sales_person_name 	= JModelLegacy::getInstance('salesPerson','enmasseModel')->retrieveName($dealList[$i]->sales_person_id);
			}

			$this->assignRef( 'filter', $filter);
			$this->statusList = EnmasseHelper::$DEAL_STATUS_LIST;
			$this->locationList = JModelLegacy::getInstance('location','enmasseModel')->listAllPublished();
			$categoryList = JModelLegacy::getInstance('category','enmasseModel')->listAllPublished();
                        foreach($categoryList as $item){
                            if($item->parent_id){ 
                                $item->name = '&nbsp;&nbsp;-'.$item->name;
                            }
                        }
                        $this->categoryList = $categoryList;
			$this->assignRef( 'dealList', $dealList );
			$this->assignRef('pagination', $pagination);
			$this->assignRef( 'order', $order );
		}
		
		parent::display($tpl);
	}

}
?>