<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$rows = $this -> orderList;
$option = 'com_enmasse';
$archived = JRequest::getVar('archived');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse".DS."helpers". DS ."EnmasseHelper.class.php");

$filter = $this->filter;

$emptyJOpt = JHTML::_('select.option', '', '--- All ---' );

$statusJOptList = array();
array_push($statusJOptList, $emptyJOpt);
foreach ($this->statusList as $key=>$name)
{
	$var = JHTML::_('select.option', JText::_('ORDER_'.strtoupper($key)), $name );
	array_push($statusJOptList, $var);
}
JHtml::_('behavior.framework');
JHTML::_('behavior.tooltip');
JHTML::_('behavior.modal');
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Locale.en-US.DatePicker.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.Attach.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.Date.js");
JFactory::getDocument()->addStyleSheet("components/com_enmasse/script/datepicker/datepicker_dashboard/datepicker_dashboard.css");

?>
<script type="text/javascript">
    addEvent('domready', function() {
        new Picker.Date($$('.calendar'), {
            timePicker: true,
            draggable: false,
            positionOffset: {x: 5, y: 0},
            pickerClass: 'datepicker_dashboard',
            format: 'db',
            useFadeInOut: !Browser.ie
        });
    });

</script>
<form action="index.php">
    <table class="tableSearch">
	<tr>
		<td>
                        <b><?php echo JText::_('Order ID')?>: </b>
                </td>
                <td>
                        <input type="text" name="filter[order_id]"	value="<?php echo $filter['order_id']; ?>"  placeholder=""/>
                </td>
                <td>
                        <b><?php echo JText::_('Processing Center')?>: </b>
                </td>
                <td>
                        <input type="text" name="filter[center]"	value="<?php echo $filter['center']; ?>" placeholder=""/>
		</td>
                <td>&nbsp;</td>
        </tr>
        <tr>
		<td>
                        <b><?php echo JText::_('From Date')?>: </b>
                </td>
                <td>
                        <?php echo JHTML::_('calendar', $filter[from_date], 'filter[from_date]','filter[from_date]','%Y-%m-%d','size="20" readonly style="width:168px;"'); ?>
                </td>
                <td>
                        <b><?php echo JText::_('To Date')?>: </b>
                </td>
                <td>
                        <?php echo JHTML::_('calendar', $filter[from_to], 'filter[from_to]','filter[from_to]','%Y-%m-%d','size="20" readonly style="width:168px;"'); ?>
                        <!--<input type="text" name="filter[from_to]" class="calendar" value="<?php echo $filter['from_to']; ?>" readonly placeholder=""/>-->
                </td>
                <td>
                        <input class="btn" style="margin:7px 0px;" type="submit" value="<?php echo JText::_('ORDER_SEARCH')?>" />
                        <input class="btn" type="button" value="<?php echo JText::_('MERCHANT_SETTLEMENT_RESET');?>" onClick="location.href='index.php?option=com_enmasse&controller=processing'" />
		</td>
	</tr>
</table>
<input type="hidden" name="controller" value="processing" />
<input type="hidden" name="option" value="com_enmasse" />
</form>

<form action="index.php" method="post" name="adminForm"  id="adminForm">
<table class="adminlist  table table-striped">
	<thead>
		<tr>
			<th class="title" width='5px'><?php echo JText::_('Order ID')?></th>
			<th><?php echo JText::_('Client Name')?></th>
                        <th><?php echo JText::_('Processing Center')?></th>
			<th><?php echo JText::_('Address Delivery')?></th>
			<th>Items Detail<br>(Product | Quantity)</th>
			<th><?php echo JText::_('Date Delivery')?></th>
			<th><?php echo JText::_('Delivery Status')?></th>
			<th><?php echo JText::_('Update At')?></th>
			<th><?php echo JText::_('Comment')?></th>
		</tr>
	</thead>
	<?php
	for ($i=0; $i < count( $rows ); $i++)
	{
		$k = $i % 2;		
		$row = &$rows[$i];
		$buyer = json_decode($row->buyer_detail); 
                $delivery_detail = (array)json_decode($row->delivery_detail); 
                
		$link =  JRoute::_('index.php?option=' . $option .'&controller=processing'.'&task=edit&orderId='. $row->id.'&order_item_id='. $row->order_item_id) ;
	?>
            <tr class="<?php echo "row$k"; ?>">
		<td align="center">
			<a href="<?php echo $link?>"><?php echo EnmasseHelper::displayOrderDisplayId($row->id) ; ?></a>
		</td>
		<td>
			<?php echo $buyer->name; ?>
		</td>
                <td>
			<?php echo $row->delivery_location; ?>
		</td>
                <td>
			<?php echo $delivery_detail['address']; ?>
		</td>
                <td class="innerTableOrder">
                    <table width="100%">
                        <?php foreach ($row->orderItem as $orderItem) { ?>
                                <tr style="background: none;">
                                    <td class="innerTableOrderSub"><?php echo $orderItem->description ?></td>
                                    <td width="30" class="innerTableOrderSub"><?php echo $orderItem->qty ?></td>
                                </tr>

                        <?php } ?>
                    </table>
		</td>
                <td>
			<?php echo DatetimeWrapper::getDisplayDatetime($delivery_detail['delivery date']); ?>
		</td>
		<td>
			<?php echo $row->process_status; ?>
		</td>
                <td>
			<?php echo DatetimeWrapper::getDisplayDatetime($row->process_update); ?>
		</td>
                <td>
                    <a class="modal btn <?php echo $row->note?'btnview':'btnadd'; ?>"
                       href="index.php?option=com_enmasse&controller=processing&task=popupNoteSubmit&processID=<?php echo $row->process_id; ?>&tmpl=component" 
                       rel="{handler: 'iframe', size: {x: 351, y: 179}, onClose: function() {}}">
                        <?php echo $row->note?'view':'add'; ?>
                    </a>
		</td>
	</tr>
	<?php
	} 
	?>
	<tfoot>
    <tr>
      <td colspan="12"><?php echo $this->pagination->getListFooter(); ?></td>
    </tr>
  </tfoot>
</table>
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="controller" value="order" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter[status]" value="<?php echo $filter['status']; ?>" />
<input type="hidden" name="filter[deal_name]" value="<?php echo $filter['deal_name']; ?>" />
</form>
