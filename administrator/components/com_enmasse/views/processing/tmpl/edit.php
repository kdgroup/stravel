<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
$order_row = $this->order;

$option = 'com_enmasse';
$filter = $this->filter;
$emptyJOpt = JHTML::_('select.option', '', JText::_('') );
	
// create list location for combobox
$locationJOptList = array();
array_push($locationJOptList, $emptyJOpt);
foreach ($this->centerList as $item)
{
	$var = JHTML::_('select.option', $item->id, JText::_($item->name) );
	array_push($locationJOptList, $var);
}

$statusJOptList = array();
$statusJOptList[] = JHTML::_('select.option', 'Pending', 'Pending' );
$statusJOptList[] = JHTML::_('select.option', 'Schedule', 'Schedule' );
$statusJOptList[] = JHTML::_('select.option', 'Delivered', 'Delivered' );
$statusJOptList[] = JHTML::_('select.option', 'Cancel', 'Cancel' );
//-------------------

?>
<script language="javascript" type="text/javascript">
 function setTask()
 {
    document.adminForm.task.value = 'save';
 }
</script>

<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="width-100 fltrt">
<fieldset class="adminform"><legend><?php echo JText::_('Processing');?></legend>
<table class="admintable orderTable">
	<tr>
		<td width="100" align="left" class="key" valign="top"><?php echo JText::_('Order ID');?></td>
		<td class="order-Id"><?php echo EnmasseHelper::displayOrderDisplayId($order_row->id);?></td>
	</tr>
        <tr style="padding: 7px 0px;">
		<td width="100" align="left" class="key" valign="top"><?php echo JText::_('Order Item');?></td>
                <td align="left" class="innerTableOrder">
                    <table>
                        <tr style="display: inherit;">
                            <th style="text-align: left; padding-right: 10px;">Product name</th>
                            <th style="text-align: left; padding-left: 10px;">Quantity</th>
                        </tr>
                        <?php foreach ($order_row->orderItem as $orderItem) { ?>
                                <tr style="display: inherit;">
                                    <td style="width:auto; padding-right: 10px;"><?php echo $orderItem->description ?></td>
                                    <td style="width:auto; padding-left: 10px;"><?php echo $orderItem->qty ?></td>
                                </tr>

                        <?php } ?>
                    </table>
                </td>
	</tr>
	<tr>
		<td width="100" align="left" class="key" valign="top"><?php echo JText::_('Grand Total');?></td>
		<td align="left"><?php 
		echo EnmasseHelper::displayCurrency($order_row->total_buyer_paid);
		?></td>
	</tr>
        <tr>
            <td width="100" align="left" class="key" valign="top"><?php echo JText::_('Processing Center');?></td>
            <td>
                <?php echo JHTML::_('select.genericList', $locationJOptList, 'processing_center_id', null , 'value', 'text', $order_row->processing_center_id);?>
            </td>
        </tr>
        <tr>
            <td width="100" align="left" class="key" valign="top"><?php echo JText::_('Status');?></td>
            <td>
                <?php echo JHTML::_('select.genericList', $statusJOptList, 'delivery_status', null , 'value', 'text', $order_row->process_status);?>
            </td>
        </tr>
	<tr>
		<td width="100" align="left" class="key" valign="top"><?php echo JText::_('Comment');?></td>
		<td><textarea name="note" cols=40 rows=3><?php echo $order_row->note;?></textarea>
		</td>
	</tr>
</table>
<table class="admintable orderTable">
	<tr>
		<td width="100" align="left" class="key" valign="top"><?php echo JText::_('ORDER_DELIVERY_DETAIL');?></td>
	</tr>
	<tr>
		<td align="left"><?php 
		echo EnmasseHelper::displayJson($order_row->delivery_detail);
		?></td>
	</tr>
	<tr>
		<td width="100" align="left" class="key"><?php echo JText::_('UPDATED_AT');?></td>
		<td><?php echo DatetimeWrapper::getDisplayDatetime($order_row->process_update); ?></td>
	</tr>
</table>
</fieldset>

<input type="hidden" name="buyerid" value="<?php echo EnmasseHelper::getBuyerId(json_decode($order_row->buyer_detail)); ?>" />
<input type="hidden" name="id" value="<?php echo $order_row->process_id; ?>" />
<input type="hidden" name="order_id" value="<?php echo $order_row->id; ?>" />
<input type="hidden" name="option" value="<?php echo $option;?>" /> 
<input type="hidden" name="controller" value="processing" />
<input type="hidden" name="task" value="" />
</div>
</form>