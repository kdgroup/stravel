<?php
$processing = $this->processing;
?>
<form>
    <div id="popUpNote">
        <textarea name="note" cols=40 rows=3><?php echo $processing->note; ?></textarea>
        <div class="btModelQuotation"><input class="em_btn btn btn-success" type="submit" value="Submit" /></div>
    </div>
    <input type="hidden" name="processID" value="<?php echo $processing->id; ?>" />
    <input type="hidden" name="option" value="com_enmasse" /> 
    <input type="hidden" name="controller" value="processing" /> 
    <input type="hidden" name="task" value="submitPopUpNote" />
</form>
<style type="text/css">
    .btModelQuotation input{
        font-size: 20px;
        font-weight: bold;
        height: 35px;
        margin-left: -20px;
        margin-top: 3px;
        width: 351px;
    }
    .shadow{
        padding: 2px;
    }
    .sbox-content-iframe{
        height: 91%;
    }
    #sbox-content{
        height: 91% !important;
    }
    form{
        margin: 0;
    }
    #popUpNote textarea{
        height: 118px;
        margin-left: -20px;
        width: 337px;
        resize: none;
    }
</style>