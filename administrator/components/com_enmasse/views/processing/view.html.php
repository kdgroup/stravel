<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."toolbar.enmasse.html.php");

class EnmasseViewProcessing extends JViewLegacy
{
	function display($tpl = null)
	{
			
		$task = JRequest::getWord('task');

		if($task == 'edit') // display order with its item
		{		
                        TOOLBAR_enmasse::_SMENU();
                        //JToolBarHelper::apply('apply','Save');
                        JToolBarHelper::save();	
                        JToolBarHelper::cancel();
		
			$orderId = JRequest::getVar('orderId');
                        
			$modelOrderItem = JModelLegacy::getInstance('orderItem','enmasseModel');
			$order 	= JModelLegacy::getInstance('processing','enmasseModel')->getProcessingOrderById($orderId);
                        $order->orderItem 	= $modelOrderItem->listByOrderId($order->id);
                        
                        $this->centerList = JModelLegacy::getInstance('processingcenter','enmasseModel')->listAllPublished();
			$this->assignRef( 'statusList', EnmasseHelper::$ORDER_STATUS_LIST);
			$this->assignRef( 'order', $order );    
		}if($task == 'popupNoteSubmit'){
                        $processID = JRequest::getVar('processID');
                        $modelProcess= JModelLegacy::getInstance('processing','enmasseModel');
                        $this->processing = $modelProcess->getById($processID);
                }
		else // display list of orders
		{
			TOOLBAR_enmasse::_SMENU();
			TOOLBAR_enmasse::_PROCESSING();
			
			$filter 	= JRequest::getVar('filter');
            
			// Weird that only this will caused warning...
			if(!isset($filter['order_id'])){
				$filter['order_id'] = "";
                        }
			if(!isset($filter['from_date'])){
				$filter['from_date'] = "";
                        }
                        if(!isset($filter['from_to'])){
				$filter['from_to'] = "";
                        }
                        if(!isset($filter['center'])){
				$filter['center'] = "";
                        }
                        
			$oProcessingModel = JModelLegacy::getInstance('processing','enmasseModel');
			$orderList 	= $oProcessingModel->search($filter['order_id'],$filter['from_date'],$filter['from_to'],$filter['center'], "DESC");
                        
                        for($count =0; $count < count($orderList); $count++)
                        {
                                $orderItemList = JModelLegacy::getInstance('orderItem','enmasseModel')->listByOrderId($orderList[$count]->id);
                                $orderList[$count]->orderItem 	= $orderItemList;
                                $orderList[$count]->delivery_location = $oProcessingModel->getDeliveryCenterById($orderList[$count]->processing_center_id)->name;
                                if(!$orderList[$count]->process_status){
                                    $orderList[$count]->process_status = 'Pending';
                                }
                        }
                        
                        $pagination = $oProcessingModel->getPagination();
                        
                        $this->statusList = EnmasseHelper::$ORDER_STATUS_LIST;
			$this->filter = $filter;
			$this->orderList = $orderList ;
			$this->pagination = $pagination;
		}
		parent::display($tpl);
	}

}
?>