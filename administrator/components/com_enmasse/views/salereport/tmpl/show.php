<?php
// No direct access 
defined('_JEXEC') or die('Restricted access');
include_once JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "views" . DS . "headerMenu" . DS
        . "menuReport.php";

$option = 'com_enmasse';
$oModelSaleReport = JModelLegacy::getInstance('salereport', 'enmasseModel');

foreach ($this->categoryList as $item) {
    if ($item->parent_id) {
        $item->name = '&nbsp;&nbsp;-' . $item->name;
    }
}
$locationList = $this->locationList;
$categoryList = $this->categoryList;
//set select box for category
$categoryJOptList = array();
array_push($categoryJOptList, JHTML::_('select.option', null, JText::_('--Selected--')));
foreach ($categoryList as $item) {
    $var = JHTML::_('select.option', $item->id, $item->name);
    array_push($categoryJOptList, $var);
}
//set select box for location
$locationJOptList = array();
array_push($locationJOptList, JHTML::_('select.option', null, JText::_('--Selected--')));
foreach ($locationList as $item) {
    $var = JHTML::_('select.option', $item->id, $item->name);
    array_push($locationJOptList, $var);
}
$groupBy = JRequest::getVar('groupBy');
JHtml::_('behavior.framework');
JHTML::_('behavior.calendar');
JHTML::_('behavior.tooltip');
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Locale.en-US.DatePicker.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.Attach.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.Date.js");
JFactory::getDocument()->addStyleSheet("components/com_enmasse/script/datepicker/datepicker_dashboard/datepicker_dashboard.css");
?>
<script src="components/com_enmasse/script/highcharts.js"></script>
<script type="text/javascript">
    addEvent('domready', function() {
        new Picker.Date($$('.calendar'), {
            timePicker: true,
            draggable: false,
            positionOffset: {x: 5, y: 0},
            pickerClass: 'datepicker_dashboard',
            format: 'db',
            useFadeInOut: !Browser.ie
        });
    });

</script>
<script language="javascript" type="text/javascript">
    Joomla.submitbutton = function(pressbutton)
    {
        var form = document.adminForm;
        if(pressbutton == 'excel')
        {
            document.getElementById("tmpt").value = "";
            document.getElementById("ExportFile").submit();
//            submitform( pressbutton );
        }
        if(pressbutton == 'pdf'){
            document.getElementById("tmpt").value = "pdf";
            document.getElementById("ExportFile").submit();
        }
        return false;
    }       
</script>
<div>    
    <form action="index.php" method="post" name="adminForm"  id="adminForm">
        <table class="tableSearch">
            <tr>
                <td>
                    <span class="titleCouponRe" style="width: 94px;"><?php echo JText::_('DEAL_NAME');?>:</span>
                </td>
                <td>
                    <input type="text" class="auto" onclick="return autoDealName();" name="deal_name" id="deal_name" class="" value="<?php echo JRequest::getVar('deal_name'); ?>" size="50"/>
                </td>
                <td>
                    <span class="titleCouponRe2" style="width: 63px;">Category:</span>
                </td>
                <td>
                    <?php echo JHTML::_('select.genericList', $categoryJOptList, 'category_list', null, 'value', 'text', JRequest::getVar('category_list')); ?>
                </td>
                <td>
                     <b>Group by: <?php
                        $groupByJOptList = array();
                        array_push($groupByJOptList, JHTML::_('select.option', null, JText::_('--Selected--')));
                        array_push($groupByJOptList, JHTML::_('select.option', 'Day', JText::_('Day')));
                        array_push($groupByJOptList, JHTML::_('select.option', 'Week', JText::_('Week')));
                        array_push($groupByJOptList, JHTML::_('select.option', 'Month', JText::_('Month')));
                        array_push($groupByJOptList, JHTML::_('select.option', 'Year', JText::_('Year')));
                        echo JHTML::_('select.genericList', $groupByJOptList, 'groupBy', 'style="width: 105px;"', 'value', 'text', JRequest::getVar('groupBy'));
                        ?>
                    </b>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="titleCouponRe" style="width: 94px;">From:</span>
                </td>
                <td>
                     <?php echo JHTML::_('calendar', JRequest::getVar('from_at'), 'from_at','from_at','%Y-%m-%d','size="20" readonly style="width:168px;"'); ?>
                </td>
                <td>
                    <b class="flRight marTop5">to:</b>
                </td>
                <td>
                    <?php echo JHTML::_('calendar', JRequest::getVar('from_to'), 'from_to','from_to','%Y-%m-%d','size="20" readonly style="width:168px;"'); ?>
                </td>
                <td>
                    <input type="submit" class="btn btnview" style="padding-top:6px; padding-bottom:6px;" id="search_sale_report" value="<?php echo JTEXT::_('Generate') ?>">
                    <input class="btn" type="button" value="<?php echo JText::_('MERCHANT_SETTLEMENT_RESET');?>" onClick="location.href='index.php?option=com_enmasse&controller=salereport'" />
                </td>
            </tr>
            
    <!--        <span class="titleCouponRe">Sale Person Name:</span>
            <input type="text" name="sale_person_name" id="sale_person_name" onclick="return autoSalePersonName();" class="auto2" value="<?php echo JRequest::getVar('sale_person_name'); ?>" size="50"/>
            <span class="titleCouponRe2">Merchant name:</span>
            <input type="text" name="merchant_name" id="merchant_name" onclick="return autoMerchantName();" class="auto1" value="<?php echo JRequest::getVar('merchant_name'); ?>" size="50"/> <br/>-->

            <!--<span class="titleCouponRe">Location:</span>-->
            <?php //echo JHTML::_('select.genericList', $locationJOptList, 'location_list', null, 'value', 'text', JRequest::getVar('location_list')); ?>

            <!--<input type="text" name="from_at" id="from_at" class="calendar" value="<?php echo JRequest::getVar('from_at') ?>" size="50" readonly/>--> 
           
        </table>
<!--        <div style="margin-left: 454px;">
            <input type="button" class="btn" id="" onClick="exportSubmit();" value="<?php echo JTEXT::_('Export to Excel') ?>" style="margin-bottom: 9px;">
            <input type="button" class="btn" id="" onClick="PDFSubmit();" value="<?php echo JTEXT::_('Export to PDF') ?>" style="margin-bottom: 9px;">
        </div>-->
        <?php
        //if have any result ==> show them
        if (isset($this->resultSearch) && count($this->resultSearch) > 0) {
            $result = $this->resultSearch;
            $grBy = $groupBy;
            if (!$grBy) {
                $grBy = "Week";
            }
            $firstday = $result[0]->created_at;
            $fromEnd = $oModelSaleReport->setDateFromEnd($grBy, $firstday, 0, $result);
            $dateStartChart = $fromEnd[0];
            $dateStartChart = date("Y,m,d", strtotime($dateStartChart));
            ?>

            <div id="graphSaleTrending" style="height: 400px; width: 1000px"></div>
            <input type="hidden" id="groupByShap" name="groupByShap" value="<?php echo $grBy; ?>"/>
            <table class="adminlist  table table-striped">
                <thead>
                    <tr>
                        <th width="100"><?php echo JText::_('Date from'); ?></th>
                        <th width="100"><?php echo JText::_('Date End'); ?></th>	
                        <th width="100"><?php echo JText::_('No.Order'); ?></th>	
                        <th width="100"><?php echo JText::_('Total'); ?></th>	
                        <th width="100"><?php echo JText::_('Commission'); ?></th>	
                        <th><?php echo JText::_('Total after Commission'); ?></th>	
                    </tr>
                </thead>
                <?php
                $numOrder = 0;
                $forChart = "";
                $listResult = array();
                $listResult[] = $fromEnd;
                for ($i = 0; $i < count($result); $i++) {
                    $firstday = $result[$i]->created_at;
                    $firstday = date("Y-m-d", strtotime($firstday));
                    $flag = true;
                    if ($firstday >= $fromEnd[0] && $firstday <= $fromEnd[1]) {
                        $flag = false;
                        $numOrder = $numOrder + 1;
                    } else {
                        $flag = true;
                        $fromEnd = $oModelSaleReport->setDateFromEnd($grBy, $firstday, $numOrder, $result);
                        $listResult[] = $fromEnd;
                    }
                }

                $max_index = count($listResult) - 1;
                foreach ($listResult as $index => $fromEnd) {
                    ?>
                    <tr>
                        <td width="100"><?php echo $fromEnd[0]; ?></td>
                        <td width="100"><?php echo $fromEnd[1]; ?></td>
                        <td width="100"><?php echo $fromEnd[2]; ?></td>
                        <td width="100"><?php echo $fromEnd[3]; ?></td>
                        <td width="100"><?php echo $fromEnd[4]; ?></td>
                        <td><?php echo $fromEnd[5]; ?></td>
                    </tr>
                    <?php
                    $forChart .= $fromEnd[5] . ",";
                    if ($index < $max_index) {
                        $miss_space = $oModelSaleReport->getSpace($fromEnd[0], $listResult[$index + 1][0], $grBy);
                        $i = 0;
                        while ($i < $miss_space) {
                            //have n space for loop, adding value = 0 for that space
                            $forChart .= '0,';
                            $i++;
                        }
                    }
                }
                ?>
                <tfoot>
                    <tr>

                    </tr>
                </tfoot>
            </table>
        <?php }else if(!isset($this->resultSearch)){ ?>
            <div class="beforeGenerate">Please input your report criteria to generate the report</div>
        <?php }else{ ?>
            <div class="beforeGenerate">Have Not Result.</div>
        <?php } ?>
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="controller" value="salereport" />
        <input type="hidden" name="task" value="searchSaleReport" />
        <input type="hidden" name="export" value="true" />
    </form>

    <form id='ExportFile' name='ExportFile' method="post">
        <input type="hidden" name="option" value="com_enmasse" />
        <input type="hidden" name="controller" value="saleReports" />
        <input type="hidden" name="task" value="generateSaleReport" />
        <input type="hidden" id="tmpt" name="tmpt" value="" />
        <input type="hidden" name="deal_name" value="<?php echo JRequest::getVar('deal_name') ?>" />
        <input type="hidden" name="sale_person_name" value="<?php echo JRequest::getVar('sale_person_name') ?>" />
        <input type="hidden" name="merchant_name" value="<?php echo JRequest::getVar('merchant_name') ?>" />
        <input type="hidden" name="location_list" value="<?php echo JRequest::getVar('location_list') ?>" />
        <input type="hidden" name="category_list" value="<?php echo JRequest::getVar('category_list') ?>" />
        <input type="hidden" name="from_at" value="<?php echo JRequest::getVar('from_at') ?>" />
        <input type="hidden" name="from_to" value="<?php echo JRequest::getVar('from_to') ?>" />
        <input type="hidden" name="export" value="<?php echo JRequest::getVar('export') ?>" />
    </form>
    <script>
    jQuery(document).ready(function() {
        var datas = "<?php echo $forChart; ?>";
        datas = datas.substr(0, datas.length - 1);
        var chart = datas.split(',');
        for (i = 0; i < chart.length; i++) {
            chart[i] = parseInt(chart[i]);
        }

        var groupBy = ('<?php echo $grBy; ?>');
        if (groupBy == 'Week') {
            groupBy = 7;
        } else if (groupBy == 'Day') {
            groupBy = 1;
        } else if (groupBy == "Month") {
            groupBy = 31;
        } else {
            groupBy = 365;
        }
        var dateStart = new Date('<?php echo $dateStartChart; ?>');
//        alert(dateStart);
        jQuery('#graphSaleTrending').highcharts({
            xAxis: {
                startOfWeek: 0,
                type: 'datetime',
                labels: {
                    formatter: function() {
                        return Highcharts.dateFormat('%e %b', this.value);
                    }
                }
            },
            series: [{
                    data: chart,
                    pointStart: Date.UTC(dateStart.getFullYear(), dateStart.getMonth(), dateStart.getDate()),
                    pointInterval: groupBy * 24 * 3600 * 1000
                }]
        });
    });
    </script>

    <script>
//        function exportSubmit()
//        {
//            document.getElementById("tmpt").value = "";
//            document.getElementById("ExportFile").submit();
//        }
//        function PDFSubmit()
//        {
//            document.getElementById("tmpt").value = "pdf";
//            document.getElementById("ExportFile").submit();
//        }
    </script>

</div>
