<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined('_JEXEC') or die('Restricted access');
include_once JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "views" . DS . "headerMenu" . DS
        . "menuReport.php";
$orderItemList = $this->orderItemList;
$emptyJOpt = JHTML::_('select.option', '', JText::_('DEAL_COUPON_SELECTION_MSG'));
$dealJOptList = array();
array_push($dealJOptList, $emptyJOpt);
foreach ($this->dealList as $item) {
    $var = JHTML::_('select.option', $item->id, JText::_($item->name));
    array_push($dealJOptList, $var);
}
JHtml::_('behavior.framework');
JHTML::_('behavior.tooltip');
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Locale.en-US.DatePicker.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.Attach.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.Date.js");
JFactory::getDocument()->addStyleSheet("components/com_enmasse/script/datepicker/datepicker_dashboard/datepicker_dashboard.css");
?>
<script type="text/javascript">
    addEvent('domready', function() {
        new Picker.Date($$('.calendar'), {
            timePicker: true,
            draggable: false,
            positionOffset: {x: 5, y: 0},
            pickerClass: 'datepicker_dashboard',
            format: 'db',
            useFadeInOut: !Browser.ie
        });
    });

</script>

<form action="index.php">
    <table>
        <tr>
            <td>
                <div style="float: left; margin-right: 10px;">
                    <b class="titleCouponRe" style="width: 74px;"><?= JText::_('DEAL_NAME'); ?>: </b>
                    <?php echo JHTML::_('select.genericList', $dealJOptList, 'filter[deal_id]', null, 'value', 'text', $this->filter['deal_id']); ?>
                </div>
            </td>
        </tr>
        <tr>
            <td style="float: left; margin-right: 10px;" >
                <span class="titleCouponRe" style="width: 74px;">From:</span>
                <input type="text" name="from_at" id="from_at" class="calendar" value="<?php echo JRequest::getVar('from_at') ?>" size="50" readonly/> 
                <span class="">to:</span>
                <input type="text" name="from_to" id="from_to" class="calendar" value="<?php echo JRequest::getVar('from_to') ?>" size="50" readonly/> 
            </td>
        </tr>
        <tr>
            <td>
                <span class="titleCouponRe" style="width: 74px;"><?php echo JText::_('Status'); ?>:</span>
                <?php
                $typeJOptList = array();
                array_push($typeJOptList, JHTML::_('select.option', null, JText::_('--Selected--')));
                array_push($typeJOptList, JHTML::_('select.option', 'Used', JText::_('Used')));
                array_push($typeJOptList, JHTML::_('select.option', 'Taken', JText::_('Taken')));
                echo JHTML::_('select.genericList', $typeJOptList, 'type', null, 'value', 'text',JRequest::getVar('type'));
                ?>


                <input type="submit" class="btn" id="search_Voucher" value="<?php echo JTEXT::_('REPORT_SEARCH_BUTTON') ?>" style="margin-bottom: 9px;">
                <input type="button" class="btn" id="" value="<?php echo JTEXT::_('Export to Excel') ?>"
                       onClick="exportSubmit();" style="margin-bottom: 9px;">
                <input type="button" class="btn" id="search_coupon" value="<?php echo JTEXT::_('Export to PDF') ?>" 
                        onClick="PDFSubmit();" style="margin-bottom: 9px;">
            </td>
        </tr>
    </table>
    <input type="hidden" name="controller" value="report" />
    <input type="hidden" name="option" value="com_enmasse" />
</form>

<form action="index.php" name="adminForm"  id="adminForm">
    <table class="adminlist  table table-striped">
        <thead>
            <tr>
                <th width="5%"><?php echo JTEXT::_('REPORT_SERIAL'); ?></th>
                <th width="15%"><?php echo JTEXT::_('REPORT_BUYER_NAME'); ?></th>
                <th width="15%"><?php echo JTEXT::_('REPORT_BUYER_MAIL'); ?></th>
                <th width="15%"><?php echo JTEXT::_('REPORT_DELIVERY_NAME'); ?></th>
                <th width="15%"><?php echo JTEXT::_('REPORT_DELIVERY_MAIL'); ?></th>
                <th width="15%"><?php echo JTEXT::_('REPORT_ORDER_COMMENT'); ?></th>
                <th width="10%"><?php echo JTEXT::_('REPORT_PURCHASE_DATE'); ?></th>
                <th width="5%"><?php echo JTEXT::_('REPORT_COUPON_SERIAL'); ?></th>
                <th width="5%"><?php echo JTEXT::_('REPORT_COUPON_STATUS'); ?></th>
            </tr>
        </thead>
        <?php
        $count = 1;
        for ($i = 0; $i < count($orderItemList); $i++) {
            $orderItem = $orderItemList[$i];
            $buyerDetail = json_decode($orderItem->order->buyer_detail);
            $deliveryDetail = json_decode($orderItem->order->delivery_detail);

            for ($j = 0; $j < count($orderItem->invtyList); $j++) {
                $invty = $orderItem->invtyList[$j];
                ?>
                <tr>
                    <td><?php echo $count++; ?>
                    <td><?php echo $buyerDetail->name; ?></td>
                    <td><?php echo $buyerDetail->email; ?></td>
                    <td><?php echo $deliveryDetail->name; ?></td>
                    <td><?php echo $deliveryDetail->email; ?></td>
                    <td><?php echo $orderItem->order->description; ?></td>
                    <td align="center"><?php echo DatetimeWrapper::getDisplayDatetime($orderItem->created_at); ?></td>
                    <td align="center"><?php echo $invty->name; ?></td>
                    <td align="center"><?php echo JTEXT::_('COUPON_' . strtoupper($invty->status)); ?></td>
                </tr>
                <?php
            }
        }
        ?>
        <tfoot>
            <tr>
                <td colspan="16"><?php echo $this->pagination->getListFooter(); ?></td>
            </tr>
        </tfoot>
    </table>
    <input type="hidden" name="option" value="com_enmasse" />
    <input type="hidden" name="controller" value="report" />
    <input type="hidden" name="task" value="dealList" />
    <input type="hidden" name="filter[deal_id]" value="<?php echo $this->filter['deal_id']; ?>" />
</form>

<br>

<form id='excelExport' name='excelExport' method="post">
    <input type="hidden" name="option" value="com_enmasse" />
    <input type="hidden" name="controller" value="report" />
    <input type="hidden" name="task" value="generateReport" />
    <input type="hidden" name="dealId" value='<?php echo $this->filter['deal_id']; ?>' />
</form>
<form id='PDFExport' name='PDFExport' method="post">
    <input type="hidden" name="option" value="com_enmasse" />
    <input type="hidden" name="controller" value="report" />
    <input type="hidden" name="task" value="generateReport" />
    <input type="hidden" name="tmpt" value="pdf" />
    <input type="hidden" name="dealId" value='<?php echo $this->filter['deal_id']; ?>' />
</form>
<script>
    function exportSubmit()
    {
        document.getElementById("excelExport").submit();
    }
    function PDFSubmit()
    {
        document.getElementById("PDFExport").submit();
    }
</script>