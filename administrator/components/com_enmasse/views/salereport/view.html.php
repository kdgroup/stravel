<?php

/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "toolbar.enmasse.html.php");
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "helpers" . DS . "DatetimeWrapper.class.php");
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "helpers" . DS . "EnmasseHelper.class.php");

class EnmasseViewSalereport extends JViewLegacy {

    function display($tpl = null) {
        TOOLBAR_enmasse::_SMENU();
        TOOLBAR_enmasse::_SALE_REPORT();

        $task = JRequest::getVar('task');
        if ($task == 'searchSaleReport') {
            $deal_name = JRequest::getVar('deal_name');
            $sale_person_name = JRequest::getVar('sale_person_name');
            $merchant_name = JRequest::getVar('merchant_name');
            $location_list = JRequest::getVar('location_list');
            $category_list = JRequest::getVar('category_list');
            $from_at = JRequest::getVar('from_at');
            $from_to = JRequest::getVar('from_to');
            $groupBy = JRequest::getVar('groupBy');
            $deal_ids = "";
            if ($deal_name) {
                $deal_ids = JModelLegacy::getInstance('deal', 'enmasseModel')->getLikeName($deal_name);
            }
            if ($sale_person_name) {
                $sale_person_name = JModelLegacy::getInstance('salereports', 'enmasseModel')->getByName($sale_person_name);
            }
            if ($merchant_name) {
                $merchant_name = JModelLegacy::getInstance('merchant', 'enmasseModel')->getLikeName($merchant_name);
            }
            $result = JModelLegacy::getInstance('salereport', 'enmasseModel')->getForSalereprt
                    ($deal_ids, $sale_person_name, $merchant_name, $location_list, $category_list, $from_at, $from_to);
            $this->resultSearch = $result; 
//             $this->groupBy = $groupBy;
        }
        $this->locationList = JModelLegacy::getInstance('location', 'enmasseModel')->listAllPublished();
        $this->categoryList = JModelLegacy::getInstance('category', 'enmasseModel')->listAllPublished();
        parent::display($tpl);
    }

}

?>