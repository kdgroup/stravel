<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 


jimport( 'joomla.application.component.view');

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."toolbar.enmasse.html.php");

class EnmasseViewSalesPerson extends JViewLegacy
{
	function display($tpl = null)
	{
		$task = JRequest::getWord('task');
		if($task == 'edit' || $task == 'add')
		{
                    TOOLBAR_enmasse::_SMENU();
			JRequest::setVar('hidemainmenu', true);
			TOOLBAR_enmasse::_SALESPERSON_NEW();
			$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
			$oSale = JTable::getInstance('SalesPerson','Table');
			if($cid[0] > 0 && !$oSale->load($cid[0]))
			{
				JError::raiseError( 500, $oSale->getError() );
				return;
			}
			//ovveride the object with data was saved in the session
			$data = JFactory::getApplication()->getUserState('salesperson.add.data');
			if (!empty($data))
			{$oSale->bind($data);}
			$this->salesPerson = $oSale;
                        
                        //get form user
                        JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
                        $form = JForm::getInstance('com_enmasse.salesperson', 'salesperson');
                        $user_id = '';
                        if($oSale->user_name){
                            $user = EnmasseHelper::getUserByName($oSale->user_name);
                            $user_id = $user->id;
                        }
                        
                        $form->setValue('user_id',null,$user_id);
                        $this->form = $form;
		}
		else // show
		{
			TOOLBAR_enmasse::_SMENU();
			$nNumberOfSales = JModelLegacy::getInstance('salesPerson','enmasseModel')->countAll();
			if($nNumberOfSales==0)
			{
				TOOLBAR_enmasse::_SALESPERSON_EMPTY();
			}
			else
			{
				TOOLBAR_enmasse::_SALESPERSON();
			}
			$filter = JRequest::getVar('filter');
			$salesPersonList = JModelLegacy::getInstance('salesPerson','enmasseModel')->search($filter['name']);
			// load pagination
			$pagination = JModelLegacy::getInstance('salesPerson','enmasseModel')->getPagination($filter['name']);
			$state = $this->get( 'state' );
			// get order values
			$order['order_dir'] = $state->get( 'filter_order_dir' );
			$order['order']     = $state->get( 'filter_order' );
			$this->assignRef( 'filter', $filter );
			$this->assignRef( 'salesPersonList', $salesPersonList );
			$this->assignRef('pagination', $pagination);
			$this->assignRef( 'order', $order );
		}
		parent::display($tpl);
	}
}
?>