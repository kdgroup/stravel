<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 


require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse".DS."helpers". DS ."DatetimeWrapper.class.php");
if(isset($this->location))
{
    $row = $this->location;
}
else
{
    $row->id = '';
    $row->name = '';
    $row->description = '';
    $row->published = 1;
    $row->created_at = '';
    $row->updated_at = '';
}
$option = 'com_enmasse';
JHTML::_('behavior.tooltip');
$document = JFactory::getDocument();
$document->addScript('https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false');
$document->addScript('components/com_enmasse/script/googlemapjsv3.js');
?>
<script language="javascript" type="text/javascript">
    Joomla.submitbutton = function(pressbutton)
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel')
        {
            submitform( pressbutton );
            return;
        }
        sName = jQuery.trim(form.name.value.replace(/(<.*?>)/ig,""));
        // do field validation
        if (sName == "")
        {
            alert( "<?php echo JText::_( 'INVALID_NAME', true ); ?>" );
        }
        else
        {
             jQuery.post("index.php?option=com_enmasse&tmpl=component&controller=location&task=checkDuplicatedLocation", { locationName: sName },function(data) {
                       if(data == 'true' && sName!=form.tempName.value){
                              alert("<?php echo JText::_('LOCATION_NAME_DUPLICATED', true); ?>");
                       }
                       else
                       {
                              submitform( pressbutton );
                    }

              });
        }
    }       
</script>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="width-100 fltrt">
<fieldset class="adminform"><legend><?php echo JText::_('DETAIL')?></legend>
<table class="admintable" style="width: 100%">
	<tr>
		<td width="170" align="right" class="key"><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_LOCATION_NAME'),JTEXT::_(''), 
                    '', JTEXT::_('NAME'). ' *');?></td>
		<td><input class="text_area" type="text" name="name" id="name"
			size="50" maxlength="250" value="<?php echo htmlentities($row->name, ENT_QUOTES,"UTF-8");?>" />
			<input type="hidden"  name="tempName" value="<?php echo htmlentities($row->name, ENT_QUOTES,"UTF-8");?>" />
			</td>
	</tr>
	<tr>
		<td width="170" align="right" class="key"><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_LOCATION_DESC'),JTEXT::_(''), 
                    '', JTEXT::_('DESC'));?></td>
		<td><textarea style="width: auto" type="text" name="description"
			id="description" maxlength="250" cols="36" rows="3"><?php echo $row->description;?></textarea>
		</td>
	</tr>
	<tr>
		<td width="170" align="right" class="key"><span><?php echo JText::_('PUBLISHED')?></span></td>
		<td><?php
		if ($row->published == null)
		{
			echo JHTML::_('select.booleanlist', 'published',
                          'class="inputbox"', 1);
		}
		else
		{
		echo JHTML::_('select.booleanlist', 'published',
                          'class="inputbox"', $row->published);
		}
		?></td>
	</tr>
        <tr>
            <td colspan="2">
                    <table class="admintable table" width="100%">
                      <tr>
                        <td>
                            <input type="hidden" name="latitude" id="loc_x"
                            value="<?php if(isset($row->latitude)) echo $row->latitude ?>" size="40" />
                            <input type="hidden" name="longitude" id="loc_y"
                            value="<?php if(isset($row->longitude)) echo $row->longitude ?>" size="40" />
                            <input type="hidden" name="default_zoom" id="zoom_rate"
                              value="12" size="40" />
                            Address
                            <input type="text" value="<?php echo $row->address ?>" name="address" id="address" style="width: 350px;" onkeydown="if (event.keyCode == 13) document.getElementById('addressSearch').click()"/> 
                            <input type="button" id="addressSearch" value="Go" onclick="codeAddress()" class="btn" /></td>
                      </tr>
                      <tr>
                        <td>
                                            <div align="right" style="font-style:italic; font-size:11px;" id="latlng">
                                                    <b>Lat: </b><span id="lat"><?php  echo $row->latitude ?></span>&nbsp;&nbsp;&nbsp;
                                                    <b>Lng: </b><span id="lng"><?php  echo $row->longitude ?></span>&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div class="map_canvas" id="map-canvas" style="width: auto; height: 400px;"></div>
                        </td>
                      </tr>
                    </table>
            </td>
        </tr>
	<tr>
		<td width="170" align="right" class="key"><span><?php echo JText::_('CREATED_AT');?></span></td>
		<td><?php echo DatetimeWrapper::getDisplayDatetime($row->created_at); ?></td>
	</tr>
	<tr>
		<td width="170" align="right" class="key"><span><?php echo JText::_('UPDATED_AT');?></span></td>
		<td><?php echo DatetimeWrapper::getDisplayDatetime($row->updated_at); ?></td>
	</tr>
</table>
</fieldset>
<input type="hidden" name="oldname" value="<?php echo $row->name;?>" />
<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="controller" value="location" />
<input type="hidden" name="task" value="" />
</div>
</form>