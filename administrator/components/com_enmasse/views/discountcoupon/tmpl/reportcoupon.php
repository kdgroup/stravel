<?php
defined('_JEXEC') or die('Restricted access');
include_once JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "views" . DS . "headerMenu" . DS
        . "menuReport.php";
$option = 'com_enmasse';
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "helpers" . DS . "EnmasseHelper.class.php");
JHtml::_('behavior.framework');
JHTML::_('behavior.tooltip');
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Locale.en-US.DatePicker.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.Attach.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.Date.js");
JFactory::getDocument()->addStyleSheet("components/com_enmasse/script/datepicker/datepicker_dashboard/datepicker_dashboard.css");
?>
<script type="text/javascript">
    addEvent('domready', function() {
        new Picker.Date($$('.calendar'), {
            timePicker: true,
            draggable: false,
            positionOffset: {x: 5, y: 0},
            pickerClass: 'datepicker_dashboard',
            format: 'db',
            useFadeInOut: !Browser.ie
        });
    });

</script>
<script language="javascript" type="text/javascript">
    Joomla.submitbutton = function(pressbutton)
    {
        var form = document.adminForm;
        if(pressbutton == 'excel')
        {
            document.getElementById("tmpt").value = "";
            document.getElementById("ExportFile").submit();
        }
        if(pressbutton == 'pdf'){
            document.getElementById("tmpt").value = "pdf";
            document.getElementById("ExportFile").submit();
        }
        return false;
    }       
</script>

<form action="index.php" method="post" id="adminForm">
    <table class="tableSearch">
       <tr>
            <td>
                <b>Voucher Name:</b>
            </td>
            <td>
                <input type="text" name="coupon_name" id="coupon_name" class="auto3" onclick="return autoCouponName();" value="<?php echo JRequest::getVar('coupon_name') ?>" size="50"/>
            </td>
            <td colspan="4">
                <b>Voucher Code:</b>
                <input type="text" name="coupon_code" id="coupon_code" class="auto4" onclick="return autoCouponCode();" value="<?php echo JRequest::getVar('coupon_code') ?>" size="50"/>
            </td>
        </tr>    
        <tr>
            <td>
                <b>Discount between:</b>
            </td>
            <td>
                <input type="text" name="discount_at" id="discount_at" class="" value="<?php echo JRequest::getVar('discount_at') ?>" size="50"/> 
            </td>
            <td>
                <b>to:</b>
            </td>
            <td>
                <input type="text" name="to_discount" id="to_discount" class="" value="<?php echo JRequest::getVar('to_discount') ?>" size="50"/> 
            </td>
            <td>
                <b><?php echo JText::_('COUPON_TYPE'); ?>:</b>
            </td>
            <td>
                <?php
                $typeJOptList = array();
                array_push($typeJOptList, JHTML::_('select.option', null, JText::_('--Selected--')));
                array_push($typeJOptList, JHTML::_('select.option', 'Percentage', JText::_('Percentage')));
                array_push($typeJOptList, JHTML::_('select.option', 'Fixed Amount', JText::_('Fixed Amount')));
                echo JHTML::_('select.genericList', $typeJOptList, 'type', null, 'value', 'text', JRequest::getVar('type'));
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <b>Start between:</b>
            </td>
            <td>
                <?php echo JHTML::_('calendar', JRequest::getVar('start_at'), 'start_at','start_at','%Y-%m-%d','size="20" readonly style="width:168px;"'); ?>
            </td>
            <td>
                <b>to:</b>
            </td>
            <td>
                <?php echo JHTML::_('calendar', JRequest::getVar('start_to'), 'start_to','start_to','%Y-%m-%d','size="20" readonly style="width:168px;"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <b>End between:</b>
            </td>
            <td>
                <?php echo JHTML::_('calendar', JRequest::getVar('end_at'), 'end_at','end_at','%Y-%m-%d','size="20" readonly style="width:168px;"'); ?>
            </td>
            <td>
                <b>to:</b>
            </td>
            <td>
                <?php echo JHTML::_('calendar', JRequest::getVar('end_to'), 'end_to','end_to','%Y-%m-%d','size="20" readonly style="width:168px;"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <b>Use between:</b>
            </td>
            <td>
                <?php echo JHTML::_('calendar', JRequest::getVar('use_at'), 'use_at','use_at','%Y-%m-%d','size="20" readonly style="width:168px;"'); ?>
            </td>
            <td>
                <b>to:</b>
            </td>
            <td>
                <?php echo JHTML::_('calendar', JRequest::getVar('use_to'), 'use_to','use_to','%Y-%m-%d','size="20" readonly style="width:168px;"'); ?>
            </td>
            <td colspan="2">
                <input type="submit" class="btn" id="search_coupon" value="<?php echo JTEXT::_('Search') ?>" >
                <input class="btn" type="button" value="<?php echo JText::_('MERCHANT_SETTLEMENT_RESET');?>" onClick="location.href='index.php?option=com_enmasse&controller=discountcoupon&task=reportcoupon'" />
                <!--    <input type="button" class="btn" onClick="exportSubmit();" id="" value="<?php echo JTEXT::_('Export to Excel') ?>" >
                <input type="button" class="btn" onClick="PDFSubmit();" id="search_coupon" value="<?php echo JTEXT::_('Export to PDF') ?>" >-->
            </td>
        </tr>
        <!--<span class="titleCouponRe"></span>-->
    </table>
    <?php
    if (isset($this->discountCouponList) && count($this->discountCouponList) > 0) {
        $discountCouponList = $this->discountCouponList;
        ?>
        <table class="adminlist  table table-striped">
            <thead>
                <tr>
                    <th><?php echo JText::_('Voucher Name') ?></th>
                    <th><?php echo JText::_('Voucher Code') ?></th>
                    <th><?php echo JText::_('Discount') ?></th>
                    <th><?php echo JText::_('Date Start') ?></th>
                    <th><?php echo JText::_('Date End') ?></th>
                    <th><?php echo JText::_('Used Times') ?></th>
                    <th><?php echo JText::_('Last Time Used') ?></th>
                    <th><?php echo JText::_('Status') ?></th>
                </tr>
            </thead>
            <?php
            for ($i = 0; $i < count($discountCouponList); $i++) {
                $k = $i % 2;
                $row = $discountCouponList[$i];
                ?>
                <tr class="<?php echo "row$k"; ?>">
                    <td align="center">
                        <?php echo $row->name ?>
                    </td>
                    <td align="right">
                        <?php echo $row->voucher_code ?>
                    </td>
                    <td>
                        <?php echo $row->discount_amount . "%"; ?>
                    </td>
                    <td>
                        <?php echo DatetimeWrapper::getDisplayDatetime($row->start_at); ?>
                    </td>
                    <td>
                        <?php echo DatetimeWrapper::getDisplayDatetime($row->end_at); ?>
                    </td>
                    <td nowrap="true">
                        <?php echo $row->usedTimes ?>	
                    </td>
                    <td align="center">
                        <?php
                        echo $row->lastTimeuse;
                        ?>
                    </td>
                    <td align="left">
                        <?php echo $row->status; ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
    <?php } ?>
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="controller" value="discountcoupon" />
    <input type="hidden" name="task" value="reportcoupon" />
    <input type="hidden" name="export" value="true" />
</form>

<form id='ExportFile' name='ExportFile' method="post">
    <input type="hidden" name="option" value="com_enmasse" />
    <input type="hidden" name="controller" value="discountcoupon" />
    <input type="hidden" name="task" value="generateReport" />
    <input type="hidden" id="tmpt" name="tmpt" value="" />
    <input type="hidden" name="coupon_name" value="<?php echo JRequest::getVar('coupon_name') ?>" />
    <input type="hidden" name="coupon_code" value="<?php echo JRequest::getVar('coupon_code') ?>" />
    <input type="hidden" name="discount_at" value="<?php echo JRequest::getVar('discount_at') ?>" />
    <input type="hidden" name="to_discount" value="<?php echo JRequest::getVar('to_discount') ?>" />
    <input type="hidden" name="type" value="<?php echo JRequest::getVar('type') ?>" />
    <input type="hidden" name="start_at" value="<?php echo JRequest::getVar('start_at') ?>" />
    <input type="hidden" name="start_to" value="<?php echo JRequest::getVar('start_to') ?>" />
    <input type="hidden" name="end_at" value="<?php echo JRequest::getVar('end_at') ?>" />
    <input type="hidden" name="end_to" value="<?php echo JRequest::getVar('end_to') ?>" />
    <input type="hidden" name="use_at" value="<?php echo JRequest::getVar('use_at') ?>" />
    <input type="hidden" name="use_to" value="<?php echo JRequest::getVar('use_to') ?>" />
    <input type="hidden" name="export" value="<?php echo JRequest::getVar('export') ?>" />
</form>

<script>
//   function exportSubmit()
//   {
//       document.getElementById("tmpt").value = "";
//       document.getElementById("ExportFile").submit();
//   }
//   function PDFSubmit()
//   {
//       document.getElementById("tmpt").value = "pdf";
//       document.getElementById("ExportFile").submit();
//   }
</script>
