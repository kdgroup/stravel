<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined('_JEXEC') or die('Restricted access');
$rows = $this->discountCouponList;
$option = 'com_enmasse';

require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "helpers" . DS . "EnmasseHelper.class.php");

//$filter = $this->filter;
//$emptyJOpt = JHTML::_('select.option', '', '--- All ---' );
//
//$statusJOptList = array();
//array_push($statusJOptList, $emptyJOpt);
//foreach ($this->statusList as $key=>$name)
//{
//	$var = JHTML::_('select.option', $name, JText::_('ORDER_'.strtoupper($key)) );
//	array_push($statusJOptList, $var);
//}
JHtml::_('behavior.framework');
JHTML::_('behavior.tooltip');
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Locale.en-US.DatePicker.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.Attach.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.Date.js");
JFactory::getDocument()->addStyleSheet("components/com_enmasse/script/datepicker/datepicker_dashboard/datepicker_dashboard.css");
?>
<script type="text/javascript">
    addEvent('domready', function() {
        new Picker.Date($$('.calendar'), {
            timePicker: true,
            draggable: false,
            positionOffset: {x: 5, y: 0},
            pickerClass: 'datepicker_dashboard',
            format: 'db',
            useFadeInOut: !Browser.ie
        });
    });

</script>
<form action="index.php">
    <table class="tableSearch">
        <tr>
            <td>
                <b>From:</b>
            </td>
            <td>
                <?php echo JHTML::_('calendar', JRequest::getVar('from_at'), 'from_at','from_at','%Y-%m-%d','size="20" readonly style="width:168px;"'); ?>
            </td>
            <td>
                <b>to:</b>
            </td>
            <td>
                <?php echo JHTML::_('calendar', JRequest::getVar('from_to'), 'from_to','from_to','%Y-%m-%d','size="20" readonly style="width:168px;"'); ?>
            </td>
            <td>
                <span class="titleCouponRe" style="margin: 0 -2px 0 10px;float: none;"><?php echo JText::_('Status'); ?>:</span>
                <?php
                $statusJOptList = array();
                array_push($statusJOptList, JHTML::_('select.option', null, JText::_('--Selected--')));
                array_push($statusJOptList, JHTML::_('select.option', 'Actived', JText::_('Published')));
                array_push($statusJOptList, JHTML::_('select.option', 'Deactived', JText::_('Unpublished')));
                echo JHTML::_('select.genericList', $statusJOptList, 'vc_status', null, 'value', 'text', $row->status);
                ?>
                <input type="submit" class="btn" id="search_Voucher" value="<?php echo JTEXT::_('REPORT_SEARCH_BUTTON') ?>" >
                <input class="btn" type="button" value="<?php echo JText::_('MERCHANT_SETTLEMENT_RESET');?>" onClick="location.href='index.php?option=com_enmasse&controller=discountcoupon'" />
            </td>
        </tr>
    </table>
    <input type="hidden" name="controller" value="discountcoupon" />
    <input type="hidden" name="option" value="com_enmasse" />
</form>

<form action="index.php" method="post" name="adminForm"  id="adminForm">
    <table class="adminlist  table table-striped">
        <thead>
            <tr>
                <th width="5">
                    <input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
                </th>
                <th width="120"><?php echo JHTML::_('grid.sort', JText::_('Voucher Name'), 'name', $this->order['order_dir'], $this->order['order']); ?></th>
                <th><?php echo JText::_('Code') ?></th>
                <th><?php echo JText::_('Discount') ?></th>
                <th ><?php echo JHTML::_('grid.sort',JTEXT::_('Start At'), 'start_at', $this->order['order_dir'], $this->order['order']); ?></th>
                <th ><?php echo JHTML::_('grid.sort', JText::_('End At'), 'end_at', $this->order['order_dir'], $this->order['order']); ?></th>
                <th width="5" nowrap="nowrap"><?php echo JText::_('Status') ?></th>
                <th><?php echo JText::_('Action') ?></th>
                <th><?php echo JText::_('History') ?></th>
            </tr>
        </thead>
        <?php
        for ($i = 0; $i < count($rows); $i++) {
            $k = $i % 2;
            $row = &$rows[$i];
            $checked = JHTML::_('grid.id', $i, $row->id);
            $linkView = JRoute::_('index.php?option=' . $option . '&controller=discountcoupon' . '&task=viewCoupon&cid[]=' . $row->id);
            $linkEdit = JRoute::_('index.php?option=' . $option . '&controller=discountcoupon' . '&task=edit&cid[]=' . $row->id);
            ?>
            <tr class="<?php echo "row$k"; ?>">
                <td align="center">
                    <?php echo $checked ?>
                </td>
                <td align="right">
                    <?php echo $row->name ?>
                </td>
                <td>
                    <?php echo $row->voucher_code; ?>
                </td>
                <td>
                    <?php echo $row->discount_amount; ?>
                </td>
                <td>
                    <?php echo DatetimeWrapper::getDisplayDatetime($row->start_at); ?>
                </td>
                <td>
                    <?php echo DatetimeWrapper::getDisplayDatetime($row->end_at); ?>
                </td>
                <td nowrap="true">
                    <?php echo ($row->status == 'Actived' ? 'Published' : 'Unpublished'); ?>	
                </td>
                <td align="center">
                    <?php
                    echo "<a href='$linkEdit'>Edit</a>";
                    ?>
                </td>
                <td align="left">
                    <?php echo "<a href='$linkView'> View </a>"; ?>
                </td>
            </tr>
            <?php
        }
        ?>
        <tfoot>
            <tr>
                <td colspan="12"><?php echo $this->pagination->getListFooter();        ?></td>
            </tr>
        </tfoot>
    </table>
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="controller" value="discountcoupon" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="filter_order" value="<?php echo $this->order['order']; ?>" />
    <input type="hidden" name="filter_order_Dir" value="<?php echo $this->order['order_dir']; ?>" />
</form>
