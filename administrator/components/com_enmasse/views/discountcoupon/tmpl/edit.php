<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined('_JEXEC') or die('Restricted access');

require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "helpers" . DS . "EnmasseHelper.class.php");
$row = $this->coupon;
$option = 'com_enmasse';
JHtml::_('behavior.framework');
JHTML::_('behavior.tooltip');
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Locale.en-US.DatePicker.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.Attach.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.Date.js");
JFactory::getDocument()->addStyleSheet("components/com_enmasse/script/datepicker/datepicker_dashboard/datepicker_dashboard.css");
?>
<script type="text/javascript">
    addEvent('domready', function() {
        new Picker.Date($$('.calendar'), {
            timePicker: true,
            draggable: false,
            positionOffset: {x: 5, y: 0},
            pickerClass: 'datepicker_dashboard',
            format: 'db',
            useFadeInOut: !Browser.ie
        });
    });
    jQuery(document).ready(function() {

        var generate = document.getElementById('generateBt');
        generate.onclick = function() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for (var i = 0; i < 7; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            var d = new Date();
            var n = d.getDay() + d.getMonth()
                    + d.getFullYear() + d.getHours()
                    + d.getMinutes() + d.getSeconds();
            n = n + "";
            var rs = "";
            rs = text.substr(0, 3) + n.substr(n.length - 1, 1) + text.substr(3, 1) + n.substr(n.length - 2, 1)
                    + text.substr(4, 1) + n.substr(n.length - 3, 1) + text.substr(5, 2) + n.substr(n.length - 4, 1);
            document.getElementById('voucher_code').value = rs;
        };
    });
//-->
    function dateGen(str)
    {
        var year = parseInt(str.substring(0, 4), 10);
        var month = parseInt(str.substring(5, 7), 10);
        var day = parseInt(str.substring(8, 10), 10);
        var date = new Date(year, month, day);
        return date.getTime();
    }
    function currDate()
    {
        var currentTime = new Date();
        var year = currentTime.getFullYear();
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var today = "";
        if (month > 9 && day > 9)
        {
            today = year + "-" + month + "-" + day;
        }
        else if (month > 9)
        {
            today = year + "-" + month + "-0" + day;
        }
        else if (day > 9)
        {
            today = year + "-0" + month + "-" + day;
        }
        else
        {
                    today = year + "-0" + month + "-0" + day;
                }
                return today;
            }
</script>
<script language="javascript" type="text/javascript">
<?php
$version = new JVersion;
$joomla = $version->getShortVersion();
if (substr($joomla, 0, 3) >= '1.6') {
    ?>
                Joomla.submitbutton = function(pressbutton)
    <?php
} else {
    ?>
                submitbutton = function(pressbutton)
    <?php
}
?>
            {
                var StartSateDate = '<?php
if (isset($row->start_at)) {
    echo $row->start_at;
} else {
    echo '';
}
?>';
                var EndDate = '<?php
if (isset($row->end_at)) {
    echo $row->end_at;
} else {
    echo '';
}
?>';
                var form = document.adminForm;
                if (pressbutton != 'save')
                {
                    submitform(pressbutton);
<?php ?>return;
                }
                sName = form.name.value;
                sCouponCode = form.voucher_code.value;
                sCouponType = form.type.value;
                sStatus = form.vc_status.value;
                sStartAt = form.start_at.value;
                sEndAt = form.end_at.value;
                sDiscountCoupon = form.discount_amount.value;
                sUse_per_user = form.use_per_user.value;
                sUse_per_coupon = form.use_per_coupon.value;
                //sUser_id = form.user_id.value;
                var numbers = /^[0-9]+$/;
                var timePt = /\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/;
                if (sName == "") {
                    alert("<?php echo JText::_('INVALID_NAME', true); ?>");
                } else if (sCouponCode == "") {
                    alert("<?php echo JText::_('INVALID_COUPON_CODE', true); ?>");
                } else if (sCouponType == "") {
                    alert("<?php echo JText::_('INVALID_COUPON_TYPE', true); ?>");
                } else if (!sDiscountCoupon.match(numbers)) {
                    alert("<?php echo JText::_('INVALID_DISCOUNT_COUPON', true); ?>");
                } else if (sStartAt == "") {
                    alert("<?php echo JText::_('FILL_IN_DEAL_START_DATE', true); ?>");
                } else if (!timePt.test(sStartAt)) {
                    alert("<?php echo JText::_('DEAL_START_AT_INVALID_FORMAT', true); ?>");
                } else if (sEndAt == "") {
                    alert("<?php echo JText::_('FILL_IN_DEAL_END_DATE', true); ?>");
                } else if (!timePt.test(sEndAt)) {
                    alert("<?php echo JText::_('DEAL_END_AT_INVALID_FORMAT', true); ?>");
                } else if (!sUse_per_user.match(numbers)&&sUse_per_user != -1) {
                    alert("<?php echo JText::_('INVALID_USES_PER_CUSTOMER', true); ?>");
                } else if (!sUse_per_coupon.match(numbers)&&sUse_per_coupon != -1) {
                    alert("<?php echo JText::_('INVALID_USES_PER_COUPON', true); ?>");
                } else if (sStatus == "") {
                    alert("<?php echo JText::_('INVALID_COUPON_STATUS', true); ?>");
                } else if (dateGen(sStartAt) > dateGen(sEndAt)) {
                    alert("<?php echo JText::_('DEAL_END_GREATER_START', true); ?>");
                } else if (form.id == '' && dateGen(sStartAt) < dateGen(currDate())) {
                    alert("<?php echo JText::_('DEAL_START_LESS_TODAY', true); ?>");
                } else if (form.id != '' && sStartAt != StartSateDate && dateGen(sStartAt) < dateGen(currDate())) {
                    alert("<?php echo JText::_('DEAL_START_LESS_TODAY', true); ?>");
                } else {
                    var flag = false;
                    jQuery.post("index.php?option=com_enmasse&tmpl=component&controller=discountcoupon&task=checkDuplicatedCode",
                            {couponCode: sCouponCode}, function(data) {
                        flag = data;
                        if (flag == 'true' && sCouponCode != form.tempCode.value) {
                            alert("<?php echo JText::_('COUPON_CODE_DUPLICATED', true); ?>");
                        } else {
                            submitform(pressbutton);
<?php ?>return;
                        }
                    });
                }
            }
</script>
<form action = "index.php" method = "post" name = "adminForm" id = "adminForm">
    <div class = "width-100 fltrt">
        <fieldset class = "adminform"><legend><?php echo JText::_('COUPON_DETAIL');
?></legend>
            <table class="admintable">
                <tr>
                    <td width="100" align="right" class="key" valign="middle"><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_COUPON_NAME'), JTEXT::_(''), '', JTEXT::_('COUPON_NAME')); ?></td>
                    <td><input class="required" type="text" name="name" id="name"
                               size="50" maxlength="250" value="<?php echo strip_tags($row->name); ?>" />
                    </td>
                </tr>
                <tr>
                    <td width="100" align="right" class="key" valign="middle"><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_COUPON_CODE'), JTEXT::_(''), '', JTEXT::_('COUPON_CODE')); ?></td>
                    <td><input class="required" type="text" name="voucher_code" id="voucher_code"
                               size="50" maxlength="250" value="<?php echo strip_tags($row->voucher_code); ?>" />
                        <input type="hidden"  name="tempCode" value="<?php echo $row->voucher_code; ?>" />
                        <input type="button" class="btn" id="generateBt" value="<?php echo JTEXT::_('Generate') ?>">
                    </td>
                </tr>
                <tr>
                    <td width="100" align="right" class="key" valign="middle"><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_COUPON_TYPE'), JTEXT::_(''), '', JTEXT::_('COUPON_TYPE')); ?></td>
                    <td>
                        <?php
                        $typeJOptList = array();
                        array_push($typeJOptList, JHTML::_('select.option', null, JText::_('--Selected--')));
                        array_push($typeJOptList, JHTML::_('select.option', 'Percentage', JText::_('Percentage')));
                        array_push($typeJOptList, JHTML::_('select.option', 'Fixed Amount', JText::_('Fixed Amount')));
                        echo JHTML::_('select.genericList', $typeJOptList, 'type', null, 'value', 'text', $row->type);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td width="100" align="right" class="key" valign="middle"><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_COUPON_DISCOUNT'), JTEXT::_(''), '', JTEXT::_('COUPON_DISCOUNT')); ?></td>
                    <td><input class="required" type="text" name="discount_amount" id="discount_amount"
                               size="50" maxlength="250" value="<?php echo strip_tags($row->discount_amount); ?>" />
                    </td>
                </tr>
                <tr>
                    <td width="100" align="right" class="key" valign="middle"><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_COUPON_ONLY_FOR_CUSTOMER'), JTEXT::_(''), '', JTEXT::_('COUPON_ONLY_FOR_CUSTOMER')); ?></td>
                    <td><input onclick="return autoCustomerCoupon();" class="required auto5" type="text" name="user_name" id="user_name"
                               size="50" maxlength="250" value="<?php echo $row->user_name; ?>" />
                    </td>
                </tr>
                <tr>
                    <td width="100" align="right" class="key" valign="middle"><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_COUPON_DATE_START'), JTEXT::_(''), '', JTEXT::_('COUPON_DATE_START')); ?></td>
                    <td>
                        <input type="text" name="start_at" id="start_at" class="calendar" value="<?php echo $row->start_at ?>" size="50" readonly/> 
                    </td>
                </tr>
                <tr>
                    <td width="100" align="right" class="key" valign="middle"><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_COUPON_DATE_END'), JTEXT::_(''), '', JTEXT::_('COUPON_DATE_END')); ?></td>
                    <td>
                        <input type="text" name="end_at" id="end_at" class="calendar" value="<?php echo $row->end_at ?>" size="50" readonly/> 
                    </td>
                </tr>
                <tr>
                    <td width="100" align="right" class="key" valign="middle"><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_COUPON_USES_PER_COUPON'), JTEXT::_(''), '', JTEXT::_('COUPON_USES_PER_COUPON')); ?></td>
                    <td><input class="required" type="text" name="use_per_coupon" id="use_per_coupon"
                               size="50" maxlength="250" value="<?php if(isset($row->use_per_coupon)){echo strip_tags($row->use_per_coupon);}else{echo '-1';} ?>" />
                    </td>
                </tr>
                <tr>
                    <td width="120" align="right" class="key" valign="middle"><?php echo JHTML::tooltip(JTEXT::_('TOOLTIP_COUPON_USES_PER_CUSTOMER'), JTEXT::_(''), '', JTEXT::_('COUPON_USES_PER_CUSTOMER')); ?></td>
                    <td><input class="required" type="text" name="use_per_user" id="use_per_user"
                               size="50" maxlength="250" value="<?php if(isset($row->use_per_user)){echo strip_tags($row->use_per_user);}  else {echo '-1';} ?>" />
                    </td>
                </tr>
                <tr>
                    <td width="100" align="right" class="key" valign="middle"><?php echo  JHTML::tooltip(JTEXT::_('TOOLTIP_COUPON_STATUS'), JTEXT::_(''), '', JTEXT::_('COUPON_STATUS')); ?></td>
                    <td>
                        <?php
                        $statusJOptList = array();
                        array_push($statusJOptList, JHTML::_('select.option', null, JText::_('--Selected--')));
                        array_push($statusJOptList, JHTML::_('select.option', 'Actived', JText::_('Published')));
                        array_push($statusJOptList, JHTML::_('select.option', 'Deactived', JText::_('Unpublished')));
                        echo JHTML::_('select.genericList', $statusJOptList, 'vc_status', null, 'value', 'text', $row->status);
                        ?>
                    </td>
                </tr>
            </table>

        </fieldset>
        <input type="hidden" name="id" value="<?php echo $row->id; ?>" />
        <input type="hidden" name="option" value="<?php echo $option; ?>" /> 
        <input type="hidden" name="controller" value="discountcoupon" />
        <input type="hidden" name="task" value="" />
    </div>
</form>