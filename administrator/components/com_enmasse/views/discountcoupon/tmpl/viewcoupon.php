<?php
defined('_JEXEC') or die('Restricted access');
$orderVoucher = $this->orderVoucher;
$option = 'com_enmasse';
$cid = $this->cid;
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "helpers" . DS . "EnmasseHelper.class.php");
JHtml::_('behavior.framework');
JHTML::_('behavior.tooltip');
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Locale.en-US.DatePicker.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.Attach.js");
JFactory::getDocument()->addScript("components/com_enmasse/script/datepicker/Picker.Date.js");
JFactory::getDocument()->addStyleSheet("components/com_enmasse/script/datepicker/datepicker_dashboard/datepicker_dashboard.css");
?>
<script type="text/javascript">
    addEvent('domready', function() {
        new Picker.Date($$('.calendar'), {
            timePicker: true,
            draggable: false,
            positionOffset: {x: 5, y: 0},
            pickerClass: 'datepicker_dashboard',
            format: 'db',
            useFadeInOut: !Browser.ie
        });
    });

</script>
<form action="index.php" method="post" >
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="controller" value="discountcoupon" />
    <input type="hidden" name="task" value="viewCoupon" />
    <table class="tableSearch">
        <tr>
            <td>
                <b>From:</b>
            </td>
            <td>
                <?php echo JHTML::_('calendar', JRequest::getVar('start_at'), 'start_at','start_at','%Y-%m-%d','size="20" readonly style="width:168px;"'); ?>
            </td>
            <td>
                <b>To:</b>
            </td>
            <td>
                <?php echo JHTML::_('calendar', JRequest::getVar('end_at'), 'end_at','end_at','%Y-%m-%d','size="20" readonly style="width:168px;"'); ?>
            </td>
            <td>
                <input type="submit" class="btn" id="filterCoupon" value="<?php echo JTEXT::_('Filter') ?>">
                <input type="hidden" name ="cid[]" id="cid[]" value="<?php echo $cid[0] ?>" >
            </td>
        </tr>
    </table>
</form>
<form action="index.php" method="post" name="adminForm"  id="adminForm">
    <table class="adminlist  table table-striped">
        <thead>
            <tr>
                <th width="120"><?php echo JText::_('Order ID') ?></th>
                <th ><?php echo JHTML::_('grid.sort', JText::_('Customer'), 'customer', $this->order['order_dir'], $this->order['order']); ?></th>
                <th><?php echo JText::_('Amount') ?></th>
                <th ><?php echo JHTML::_('grid.sort', JText::_('Date used'), 'created_at', $this->order['order_dir'], $this->order['order']); ?></th>
            </tr>
        </thead>
        <?php
        for ($i = 0; $i < count($orderVoucher); $i++) {
            $k = $i % 2;
            $row = &$orderVoucher[$i];
            ?>
            <tr class="<?php echo "row$k"; ?>">
                <td align="right">
                    <?php echo $row->order_id ?>
                </td>
                <td>
                    <?php echo $row->customer; ?>
                </td>
                <td>
                    <?php echo $row->discount_amount; ?>
                </td>
                <td nowrap="true">
                    <?php echo $row->created_at ?>	
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="controller" value="discountcoupon" />
    <input type="hidden" name="task" value="viewCoupon" />
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="filter_order" value="<?php echo $this->order['order']; ?>" />
    <input type="hidden" name="filter_order_Dir" value="<?php echo $this->order['order_dir']; ?>" />
    <input type="hidden" name ="cid[]" id="cid[]" value="<?php echo $cid[0] ?>" >
</form>
