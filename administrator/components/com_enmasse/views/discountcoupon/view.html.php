<?php

// No direct access 
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "toolbar.enmasse.html.php");

class EnmasseViewDiscountcoupon extends JViewLegacy {

    function display($tpl = null) {
        $this->form = $this->get('Form');
        $task = JRequest::getWord('task');
        if ($task == 'edit' || $task == 'add') {
            TOOLBAR_enmasse::_SMENU();
            $cid = JRequest::getVar('cid', array(0), '', 'array');
            if ($task == 'edit') {
                TOOLBAR_enmasse::_DISCOUNT_COUPON_EDIT();
            } else {
                TOOLBAR_enmasse::_DISCOUNT_COUPON_NEW();
            }
            $coupon = JModelLegacy::getInstance('discountcoupon', 'enmasseModel')->getById($cid[0]);
            if($coupon->user_id){
                $coupon->user_name = JFactory::getUser($coupon->user_id)->username;
            }
            else{
                $coupon->user_name = "";
            }
            $this->assignRef('coupon', $coupon);
        } else if ($task == 'viewCoupon') {
            TOOLBAR_enmasse::_DISCOUNT_COUPON_VIEW();
            TOOLBAR_enmasse::_SMENU();
            $cid = JRequest::getVar('cid', array(0), '', 'array');
            $start_at = JRequest::getVar('start_at');
            $end_at = JRequest::getVar('end_at');
            $coupon = JModelLegacy::getInstance('discountcoupon', 'enmasseModel')->getById($cid[0]);
            $orderVoucher = null;
            if (isset($start_at) && $start_at != "" && $end_at != '') {
                if ($start_at < $end_at) {
                    $orderVoucher = JModelLegacy::getInstance('ordervoucher', 'enmasseModel')
                            ->getByIdAndDate($coupon->id, $start_at, $end_at);
                }
            } else {
                $orderVoucher = JModelLegacy::getInstance('ordervoucher', 'enmasseModel')->getByIdVoucher($coupon->id);
            }
            for ($i = 0; $i < count($orderVoucher); $i++) {
                $orders = JModelLegacy::getInstance('order', 'enmasseModel')->getById($orderVoucher[$i]->order_id);
                $tmpp = json_decode($orders->buyer_detail)->name;
                $orderVoucher[$i]->customer = $tmpp;
            }
            $state = $this->get('state');
            // get order values
            $order['order_dir'] = $state->get('filter_order_dir');
            $order['order'] = $state->get('filter_order');
            $this->assignRef('order', $order);
            $this->assignRef('cid', $cid);
            $this->assignRef('orderVoucher', $orderVoucher);
        } else if ($task == 'reportcoupon') {
            TOOLBAR_enmasse::_DISCOUNT_COUPON_REPORT();
            TOOLBAR_enmasse::_SMENU();
            $coupon_name = JRequest::getVar('coupon_name');
            $coupon_code = JRequest::getVar('coupon_code');
            $discount_at = JRequest::getVar('discount_at');
            $to_discount = JRequest::getVar('to_discount');
            $type = JRequest::getVar('type');
            $start_at = JRequest::getVar('start_at');
            $start_to = JRequest::getVar('start_to');
            $end_at = JRequest::getVar('end_at');
            $end_to = JRequest::getVar('end_to');
            $use_at = JRequest::getVar('use_at');
            $use_to = JRequest::getVar('use_to');
            if ($coupon_name || $coupon_code || $discount_at || $to_discount || $type || $start_at || $start_to || $end_at || $end_to || $use_at || $use_to) {
                $discountCouponList =
                        JModelLegacy::getInstance('discountcoupon', 'enmasseModel')->getReportVoucher(
                        $coupon_name, $coupon_code, $discount_at, $to_discount, $type, $start_at, $start_to, $end_at, $end_to, $use_at, $use_to);
                for ($i = 0; $i < count($discountCouponList); $i++) {
                    $useTime = JModelLegacy::getInstance('discountcoupon', 'enmasseModel')->getUseTime($discountCouponList[$i]->id);
                    $discountCouponList[$i]->usedTimes = $useTime->useTime;
                    $discountCouponList[$i]->lastTimeuse = $useTime->lastUsed;
                }
                $this->assignRef('discountCouponList', $discountCouponList);
            }
        } else {
            TOOLBAR_enmasse::_SMENU();
            TOOLBAR_enmasse::_DISCOUNT_COUPON();
            /// load pagination
            $pagination = $this->get('Pagination');
            
            $state = $this->get('state');
            // get order values
            $order['order_dir'] = $state->get('filter_order_dir');
            $order['order'] = $state->get('filter_order');
            $from_at = JRequest::getVar('from_at');
            $from_to = JRequest::getVar('from_to');
            $status = JRequest::getVar('vc_status');
            
            $discountCouponList = JModelLegacy::getInstance('discountcoupon', 'enmasseModel')->searchByParam($from_to,$from_at,$status);
            $this->assignRef('discountCouponList', $discountCouponList);
            $this->assignRef('pagination', $pagination);
            $this->assignRef('order', $order);
        }

        parent::display($tpl);
    }

}

?>