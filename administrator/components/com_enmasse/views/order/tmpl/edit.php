<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");
$enmasseConfigs = EnmasseHelper::getSetting();
$order_row = $this->order;

$option = 'com_enmasse';
//-------------------
// to re-define the link of server root

$temp_uri_arr =explode ('/',$_SERVER['REQUEST_URI'])  ;
$link_server = "";
 for($count = 0; $count < count($temp_uri_arr); $count++)
 {
 	if($temp_uri_arr[$count]== '')
 	{ }
 	else if($temp_uri_arr[$count] == 'administrator' )
 	{
 		break ;
 	}
 	else
 	{
 	$link_server.= '/';
 	$link_server.=$temp_uri_arr[$count];	
 	}
 }
?>
<script language="javascript" type="text/javascript">
 function setTask()
 {
    document.adminForm.task.value = 'save';
 }
 function submitForm()
 {
	 var sOrderStatus = document.adminForm.status.value;
	 var sMsg = "";
	 switch (sOrderStatus)
	 {
	 	case '<?php echo EnmasseHelper::$ORDER_STATUS_LIST['Paid']?>':
		 	sMsg = "<?php echo JTEXT::_('ORDER_PAID');?>";
		 	break;
	 	case '<?php echo EnmasseHelper::$ORDER_STATUS_LIST['Cancelled']?>':
		 	sMsg = "<?php echo JTEXT::_('ORDER_CANCELLED');?>";
		 	break;            
	 	case '<?php echo EnmasseHelper::$ORDER_STATUS_LIST['Refunded']?>':
	 		sMsg = "<?php echo JTEXT::_('ORDER_REFUNDED');?>";
		 	break;
	 	case '<?php echo EnmasseHelper::$ORDER_STATUS_LIST['Waiting_For_Refund']?>':
	 		sMsg = "<?php echo JTEXT::_('ORDER_WAITING_FOR_REFUND');?>";
		 	break;
	 	case '<?php echo EnmasseHelper::$ORDER_STATUS_LIST['Delivered']?>':
	 		sMsg = "<?php echo JTEXT::_('ORDER_DELIVERED');?>";
		 	break;
	 	
	 }
	 var confirmmation = confirm("<?php echo JTEXT::_('CHANGE_STATUS_CONFIRM_MSG');?> " +'"'+ sMsg + '" ?');
	 if (confirmmation == true)
	 {
		 document.adminForm.submit();
	 }else{
		 document.adminForm.status.value = "";
	 }
		 
	
 }
	function setOrderStatus(orderStatus)
	{ 
		 document.adminForm.status.value = orderStatus;
	}

</script>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="width-100 fltrt">
<fieldset class="adminform"><legend><?php echo JText::_('ORDER_DETAIL');?></legend>
<table class="admintable orderTable">
	<tr>
		<td width="100" align="left" class="key" valign="top"><?php echo JText::_('ORDER_ID');?></td>
		<td class="order-Id"><?php echo EnmasseHelper::displayOrderDisplayId($order_row->id);?></td>
	</tr>
        <tr style="padding: 7px 0px;">
		<td width="100" align="left" class="key" valign="top"><?php echo JText::_('Order Item');?></td>
                <td align="left" class="innerTableOrder">
                    <table>
                        <tr style="display: inherit;">
                            <th style="text-align: left; padding-right: 10px;">Product name</th>
                            <th style="text-align: left; padding-left: 10px;">Quantity</th>
                        </tr>
                        <?php foreach ($order_row->orderItem as $orderItem) { ?>
                                <tr style="display: inherit;">
                                    <td style="width:auto; padding-right: 10px;"><?php echo $orderItem->description ?></td>
                                    <td style="width:auto; padding-left: 10px;"><?php echo $orderItem->qty ?></td>
                                </tr>

                        <?php } ?>
                        <?php 

                                if($orderItem->order_types){
                                	?>
									<tr style="display: inherit;">
			                            <th style="text-align: left; padding-right: 10px;">Product Type</th>
			                            <th style="text-align: left; padding-left: 10px;">Quantity</th>
			                        </tr>
                                	<?php
                                    $orderTypes = json_decode($orderItem->order_types);
                                    foreach ($orderTypes as $key => $orderType) {
                                       echo '<tr style="display: inherit;">';
                                       echo '<td style="width:auto; padding-right: 10px;">'.$orderType->name.'</td>';
                                       echo '<td style="width:auto; padding-left: 10px;"> '.$orderType->orderQty.'</td>';
                                       echo '</tr>';
                                    }
                                }
                        ?>
                    </table>
                </td>
	</tr>
	<tr>
		<td width="100" align="left" class="key" valign="top"><?php echo JText::_('Grand Total');?></td>
		<td align="left"><?php 
		echo EnmasseHelper::displayCurrency($order_row->total_buyer_paid);
		?></td>
	</tr>
	<tr>
		<td width="100" align="left" class="key" valign="top"><?php echo JText::_('ORDER_BUYER_DETAIL');?></td>
		<td align="left"><?php 
		echo EnmasseHelper::displayBuyer(json_decode($order_row->buyer_detail));
		?></td>
	</tr>
	<tr>
		<td width="100" align="left" class="key" valign="top"><?php echo JText::_('ORDER_PAYMENT_DETAIL');?></td>
		<td align="left">
		<?php
		if(!empty($order_row->pay_detail))
		{
			$pay_detail = json_decode($order_row->pay_detail, true);
			echo '<pre>';
			foreach($pay_detail as $key=>$val){
				echo '<b>'.$val[label].':</b> '.$val[value].'<br>';
			}
			echo '</pre>';
			echo "<br />";
		}
		if($order_row->point_used_to_pay>0)
		{
			echo '<strong>'.JText::_('POINTS').':</strong> ' . $order_row->point_used_to_pay;
		}
		?>		
		</td>
	</tr>
	<tr>
		<td width="100" align="left" class="key" valign="top"><?php echo JText::_('Insurance Fee Detail');?></td>
		<td align="left"><?php 
		$insurance_fee = 0;
		
		$insuranceArray = json_decode($order_row->insurance_fee);

		if($order_row->insurance_fee){
			$insuranceArray = json_decode($order_row->insurance_fee);

			foreach ($insuranceArray as $key => $value) {
				echo "Insurance Name: ".$value->insurance_name.'';
				if($value->childFee){
					$insurance_fee+=$value->childFee;
					echo " - Child: ".$value->childVal.'';
				}
				if($value->adultFee){
					$insurance_fee+=$value->adultFee;
					echo " - Adult: ".$value->adultVal.'';
				}
				echo '<br/>';
			}
		}
		
		?></td>
	</tr>
	<tr>
		<td width="100" align="left" class="key" valign="top"><?php echo JText::_('Insurance Fee');?></td>
		<td align="left"><?php 
		
		echo "$".$insurance_fee;
		?></td>
	</tr>
	<tr>
		<td width="100" align="left" class="key" valign="top"><?php echo JText::_('Branch');?></td>
		<td align="left"><?php 
		
		echo $order_row->branch;
		?></td>
	</tr>
	<tr>
		<td width="100" align="left" class="key" valign="top"><?php echo JText::_('ORDER_COMMENT');?></td>
		<td><textarea name="description" cols=40 rows=3><?php echo $order_row->description;?></textarea>
		</td>
	</tr>
        <tr style="padding-bottom: 17px;">
		<td width="100" align="left" class="key" valign="top"><?php echo JText::_('ORDER_STATUS');?></td>
		<td>
			<input type="hidden" name="status" value="" />
			<div class="status" style="width: 150px;">
				<b>
				<?php 
                                if($order_row->status == "Paid")
				{
                                    echo "Confirmed";
                                }
//                                else if($order_row->status == 'Pending' && $order_row->payment_status){
//                                    echo 'Verifying Payment';
//                                }
                                else{
                                    echo JText::_('ORDER_'.strtoupper($order_row->status));
                                }
                                ?>
				</b>
			</div>
		</td>
	</tr>
</table>
<table class="admintable orderTable">
	<tr>
		<td width="100" align="left" class="key" valign="top"><?php echo JText::_('ORDER_DELIVERY_DETAIL');?></td>
	</tr>
	<tr>
		<td align="left"><?php 
		echo EnmasseHelper::displayJson($order_row->delivery_detail);
		?></td>
	</tr>
	<tr>
		<td width="100" align="left" class="key"><?php echo JText::_('CREATED_AT');?></td>
		<td><?php echo DatetimeWrapper::getDisplayDatetime($order_row->created_at); ?></td>
	</tr>
	<tr>
		<td width="100" align="left" class="key"><?php echo JText::_('UPDATED_AT');?></td>
		<td><?php echo DatetimeWrapper::getDisplayDatetime($order_row->updated_at); ?></td>
	</tr>
</table>
<div class="button_list">
	<?php if ($order_row->status == EnmasseHelper::$ORDER_STATUS_LIST['Pending'] || $order_row->status == EnmasseHelper::$ORDER_STATUS_LIST['Unpaid'] ):?>
                <input type="button" class="button" value="<?php echo JTEXT::_('ORDER_PAID')?>" onclick="setTask();setOrderStatus('<?php echo EnmasseHelper::$ORDER_STATUS_LIST['Paid']?>');submitForm();" title="<?php echo JText::_('PAID_ORDER');?>">
                <input type="button" class="button" value="<?php echo JTEXT::_('ORDER_CANCELLED')?>" onclick="setTask();setOrderStatus('<?php echo EnmasseHelper::$ORDER_STATUS_LIST['Cancelled']?>');submitForm();" title="<?php echo JText::_('CANCELLED_ORDER'); ?>"/>                    
	<?php endif;?>
        <?php if($order_row->status == "Paid"){ ?>
                <input type="button" class="button" value="<?php echo JTEXT::_('ORDER_DELIVERED')?>" onclick="setTask();setOrderStatus('<?php echo EnmasseHelper::$ORDER_STATUS_LIST['Delivered']?>');submitForm();" title="<?php echo JText::_('DELIVERED_ORDER');?>">
        <?php } ?>
    <?php if($order_row->status == 'Paid' || $order_row->status == EnmasseHelper::$ORDER_STATUS_LIST['Delivered']) : ?>
            <input type="button" class="button" value="<?php echo JTEXT::_('ORDER_REFUNDED')?>" onclick="setTask();setOrderStatus('<?php echo EnmasseHelper::$ORDER_STATUS_LIST['Refunded']?>');submitForm();" title="<?php echo JText::_('ORDER_REFUNDED');?>">
    <?php endif; ?>
</div>
</fieldset>

<fieldset class="adminform">
	<legend><?php echo JText::_('Passenger Info');
	
	?></legend>
<table class="adminlist  table table-striped">
	<thead>
		<tr>
			<th class="title" width=""><?php echo JText::_('#');?></th>
			<th class="title" width=""><?php echo JText::_('Name');?></th>
			<th class="title" width=""><?php echo JText::_('Surname');?></th>
			<th class="title" width=""><?php echo JText::_('Sex');?></th>
			<th class="title" width=""><?php echo JText::_('Birthday');?></th>
			<th class="title" width=""><?php echo JText::_('Phone');?></th>
			<th class="title" width=""><?php echo JText::_('Email');?></th>
			
			
		</tr>
	</thead>
	
	<tr>
		<td>Contact Passenger Info</td>
		<td><?php echo $order_row->passengerInfo->name3?></td>
		<td><?php echo $order_row->passengerInfo->surname3?></td>
		<td></td>
		<td></td>
		<td><?php echo $order_row->passengerInfo->phone3?></td>
		<td><?php echo $order_row->passengerInfo->email3?></td>
	

	</tr>
	
</table>
</fieldset>

<fieldset class="adminform">
	<legend><?php echo JText::_('ORDER_COUPON_DETAIL');?></legend>
<table class="adminlist  table table-striped">
	<thead>
		<tr>
			<th class="title" width=""><?php echo JText::_('ORDER_COUPON_SERIAL');?></th>
			<th width=""><?php echo JText::_('ORDER_COUPON_STATUS');?></th>
			<?php
			if ( !empty($enmasseConfigs->enable_security_code) )
			{
			?>
			<th width=""><?php echo JText::_('ORDER_COUPON_SECURITY_CODE');?></th>
			<?php } ?>
			<th></th>
			
		</tr>
	</thead>
	<?php
        foreach ($order_row->orderItem as $orderItem){
            $invtyList = $orderItem->invtyList;
            $base_url='http://';
            $base_url.= $_SERVER["SERVER_NAME"].$link_server;
            for ($i=0; $i < count( $invtyList ); $i++)
            {
                    $k = $i % 2;
                    $link = $base_url."/index.php?option=com_enmasse&controller=coupon&task=generate&invtyName=".$invtyList[$i]->name
                      ."&token=".EnmasseHelper::generateCouponToken($invtyList[$i]->name);
            ?>
            <tr class="<?php echo "row$k"; ?>">
                    <td align="center"><?php echo $invtyList[$i]->name; ?></td>
                    <td align="center"><?php echo JTEXT::_('COUPON_'.strtoupper($invtyList[$i]->status));?></td>

                    <?php
                    if ( !empty($enmasseConfigs->enable_security_code) )
                    {
                            ?>
                    <td align="center"><?php echo $invtyList[$i]->security_code; ?></td>
                    <?php } ?>
                    <td align="center"><a href='<?php echo $link ;?>' target="_blank"><?php echo JTEXT::_('REPORT_COUPON_REVIEW');?></a></td>
            </tr>
            <?php
            } 
        }
	?>
</table>
</fieldset>
<input type="hidden" name="buyerid" value="<?php echo EnmasseHelper::getBuyerId(json_decode($order_row->buyer_detail)); ?>" />
<input type="hidden" name="id" value="<?php echo $order_row->id; ?>" />
<input type="hidden" name="option" value="<?php echo $option;?>" /> 
<input type="hidden" name="controller" value="order" />
<input type="hidden" name="partial" value="<?php echo $this->partial;?>" />
<input type="hidden" name="task" value="" />
</div>
</form>