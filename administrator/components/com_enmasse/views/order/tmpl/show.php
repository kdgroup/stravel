<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$rows = $this -> orderList;
$listPaymentMethod = $this->listPaymentMethod;
$option = 'com_enmasse';
$archived = JRequest::getVar('archived');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse".DS."helpers". DS ."EnmasseHelper.class.php");

$filter = $this->filter;

$emptyJOpt = JHTML::_('select.option', '', '--- All ---' );

$statusJOptList = array();
array_push($statusJOptList, $emptyJOpt);
foreach ($this->statusList as $key=>$name)
{
	$var = JHTML::_('select.option', JText::_('ORDER_'.strtoupper($key)), $name );
	array_push($statusJOptList, $var);
}
$paymentJOptList = array();
array_push($paymentJOptList, $emptyJOpt);
foreach ($listPaymentMethod as $paymentMethod){
    $var = JHTML::_('select.option', JText::_($paymentMethod->id), $paymentMethod->name );
    array_push($paymentJOptList, $var);
}
?>
<form action="index.php">
<table border="0">
	<tr>
                <td>
                    <div style="float: left;">
                        <b><?php echo JText::_('Product Name')?>:</b>
                    </div>
                </td>
                <td>
                    <input type="text" name="filter[deal_name]"	value="<?php echo $filter['deal_name']; ?>" />
                </td>
		<td>
			<div style="float: left;">
                            <b><?php echo JText::_('ORDER_STATUS')?>: </b>
			</div>
		</td>
                <td>
                    <?php echo JHTML::_('select.genericList', $statusJOptList, 'filter[status]', null , 'value', 'text', $filter['status']);?>
                </td>
	</tr>
        <tr>
             <td>
                <div style="float: left;">
                        <b><?php echo JText::_('ORDER_DEAL_CODE')?>:</b>
                </div>
            </td>
            <td>
                <input type="text" name="filter[deal_code]"	value="<?php echo $filter['deal_code']; ?>" />
            </td>
            <td>
                <div style="float: left;">
                    <b><?php echo JText::_('Payment Method')?>:</b>
                </div>
            </td>
            <td>
                <?php echo JHTML::_('select.genericList', $paymentJOptList, 'filter[payment_method]', null , 'value', 'text', $filter['payment_method']);?>
                <!--<input type="text" name="filter[deal_name]"	value="<?php echo $filter['payment_method']; ?>" />-->
                <input class="btn" style="margin:7px 0px;" type="submit" value="<?php echo JText::_('ORDER_SEARCH')?>" />
                <input class="btn" style="margin:7px 0px;" type="button" value="<?php echo JText::_('ORDER_RESET')?>"
                       onClick="location.href='index.php?option=com_enmasse&controller=order&<?php if (isset($archived) && $archived != null) echo "task=orderArchived&";?>filter= '" />
            </td>
        </tr>
</table>
<input type="hidden" name="controller" value="order" />
<?php 
if (isset($archived) && $archived != null){
    echo '<input type="hidden" name="task" value="orderArchived" />';
}?>
<input type="hidden" name="option" value="com_enmasse" />
</form>

<form action="index.php" method="post" name="adminForm"  id="adminForm">
<table class="adminlist  table table-striped">
	<thead>
		<tr>
			<th class="title" width='5px'><?php echo JText::_('ORDER_ID')?></th>
			<th width='80px'><?php echo JText::_('ORDER_TOTAL_PAID')?> </th>
                        <!--<th><?php echo JText::_('ORDER_DEAL_CODE')?></th>-->
			<th width="181px;"><?php echo JText::_('ORDER_DEAL_NAME')?>
                            &nbsp | &nbsp <?php echo JText::_('ORDER_QUANTITY'); ?></th>
			<!--<th><?php echo JText::_('ORDER_QUANTITY')?></th>-->
			<th><?php echo JText::_('ORDER_BUYER_DETAIL')?></th>
			<th><?php echo JText::_('ORDER_PAYMENT_DETAIL')?></th>
			<th width="5" nowrap="nowrap"><?php echo JText::_('ORDER_STATUS')?></th>
			<th><?php echo JText::_('ORDER_COMMENT')?></th>
			<th><?php echo JText::_('CREATED_AT')?></th>
			<th><?php echo JText::_('UPDATED_AT')?></th>
		</tr>
	</thead>
	<?php
	for ($i=0; $i < count( $rows ); $i++)
	{
		$k = $i % 2;		
		$row = &$rows[$i];
		
		$link =  JRoute::_('index.php?option=' . $option .'&controller=order'.'&task=edit&orderId='. $row->id.'&order_item_id='. $row->order_item_id) ;
	?>
	<tr class="<?php echo "row$k"; ?>">
		<td align="center">
			<a href="<?php echo $link?>"><?php echo EnmasseHelper::displayOrderDisplayId($row->id) ; ?></a>
		</td>
		<td align="right">
			<?php echo EnmasseHelper::displayCurrency($row->total_buyer_paid); ?>
		</td>
                <td class="innerTableOrder">
                    <table width="100%">
                        <?php foreach ($row->orderItem as $orderItem) { ?>
                                <tr style="background: none;">
                                    <td class="innerTableOrderSub"><?php echo $orderItem->description ?></td>
                                    <td width="30" class="innerTableOrderSub"><?php echo $orderItem->qty ?></td>
                                </tr>

                        <?php } ?>
                    </table>
		</td>
		<td>
			<?php echo EnmasseHelper::displayBuyer( json_decode($row->buyer_detail) ); ?>
		</td>
		<td nowrap="true">
			<?php 
			if(!empty($row->payment_method))
			{
				echo $row->payment_method;
			}
			if($row->point_used_to_pay>0)
			{
				echo '<strong>'.JText::_('POINTS').':</strong> ' . $row->point_used_to_pay;
			}		
			?>	
		</td>
		<td align="center">
			<?php 
				if($row->status == "Paid" && $row->total_buyer_paid > $row->paid_amount)
				{
					//echo JText::_('ORDER_PARTIAL_PAID');
                                    echo "Confirmed";
				}else if($row->status == "Paid" && $row->total_buyer_paid == $row->paid_amount)
				{
					//echo JText::_('ORDER_FULL_PAID');
                                    echo "Confirmed";
				}else
				{
                                    if($row->status == 'Unpaid'){
                                        //echo "Verifying Payment";
                                        echo "Unpaid";
                                    }
                                    else {
					echo JTEXT::_('ORDER_'.strtoupper($row->status));
                                    }
				}
				 
			?>
		</td>
		<td align="left">
			<?php echo $row->description; ?>
		</td>
		<td align="center"><?php echo DatetimeWrapper::getDisplayDatetime($row->created_at); ?></td>
		<td align="center"><?php echo DatetimeWrapper::getDisplayDatetime($row->updated_at); ?></td>
	</tr>
	<?php
	} 
	?>
	<tfoot>
    <tr>
      <td colspan="12"><?php echo $this->pagination->getListFooter(); ?></td>
    </tr>
  </tfoot>
</table>
<input type="hidden" name="archived" value="<?php echo $archived; ?>" />
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="controller" value="order" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter[status]" value="<?php echo $filter['status']; ?>" />
<input type="hidden" name="filter[deal_name]" value="<?php echo $filter['deal_name']; ?>" />
</form>
