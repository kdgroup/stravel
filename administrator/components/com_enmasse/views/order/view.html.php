<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."toolbar.enmasse.html.php");

class EnmasseViewOrder extends JViewLegacy
{
	function display($tpl = null)
	{
			
		$task = JRequest::getWord('task');

		if($task == 'edit') // display order with its item
		{		
                    TOOLBAR_enmasse::_SMENU();
                        JToolBarHelper::apply('apply','Save');
			TOOLBAR_enmasse::_ORDER_NEW();
		
			$orderId = JRequest::getVar('orderId');
                        $order_item_id = JRequest::getVar('order_item_id');
			//$modelOrder 	= JModelLegacy::getInstance('order','enmasseModel');
			//$modelOrderItem = JModelLegacy::getInstance('orderItem','enmasseModel');
                        $modelInvty = JModelLegacy::getInstance('invty','enmasseModel');
			$modelOrderItem = JModelLegacy::getInstance('orderItem','enmasseModel');
			$order 				= JModelLegacy::getInstance('order','enmasseModel')->getById($orderId);
			$order->passengerInfo 				= JModelLegacy::getInstance('order','enmasseModel')->getPassengerById($orderId);
			                       
                        $order->orderItem 	= $modelOrderItem->listByOrderId($order->id);
//			$orderItem		= $modelOrderItem->getOrderItemById($order_item_id);
                        for($count =0; $count < count($order->orderItem); $count++)
                        {
                            $order->orderItem[$count]->invtyList = $modelInvty->listByOrderItemId($order->orderItem[$count]->id);
                        }
			//$order->orderItem->invtyList = $modelInvty->listByOrderItemId($order->orderItem->id);
			
			$oDeal = JModelLegacy::getInstance('deal','enmasseModel')->getById($order->orderItem->pdt_id);
			$this->sDealStatus = $oDeal->status;
			
			$this->assignRef( 'statusList', EnmasseHelper::$ORDER_STATUS_LIST);
			$this->assignRef( 'order', $order );    
			$this->partial = JRequest::getVar('partial', 0, 'method', 'int');
		}
		elseif($task == 'exportExcel')
		{
			$filter 	= JRequest::getVar('filter');
			if(!isset($filter['deal_name']))
				$filter['deal_name'] = "";
                        if(!isset($filter['deal_code']))
				$filter['deal_code'] = "";
			if(!isset($filter['status']))
				$filter['status'] = "";
			if(!isset($filter['year']))
				$filter['year'] = "";
			if(!isset($filter['month']))
				$filter['month'] = "";
				
			$oOrderModel = JModelLegacy::getInstance('order','enmasseModel');
			$orderList 	= $oOrderModel->search($filter['status'], $filter['deal_code'], $filter['deal_name'], "created_at", "DESC");
			$this->filter = $filter;
			$this->orderList = $orderList ;
		}elseif ($task == 'orderArchived' || JRequest::getVar('archived')) {
                        TOOLBAR_enmasse::_SMENU();
			TOOLBAR_enmasse::_ORDER_ARCHIVED();
                        
                        $filter 	= JRequest::getVar('filter');
			// Weird that only this will caused warning...
			if(!isset($filter['deal_name']))
				$filter['deal_name'] = "";
                        if(!isset($filter['deal_code']))
				$filter['deal_code'] = "";
			if(!isset($filter['status']))
				$filter['status'] = "";
			if(!isset($filter['year']))
				$filter['year'] = "";
			if(!isset($filter['month']))
				$filter['month'] = "";
			
			$oOrderModel = JModelLegacy::getInstance('order','enmasseModel');
			$orderList 	= $oOrderModel->search($filter['status'], $filter['deal_code'], $filter['deal_name'], "created_at", "DESC",FALSE,TRUE);
                        JRequest::setVar("archived", true);
                        $pagination = $oOrderModel->getPagination();
                        $this->statusList = EnmasseHelper::$ORDER_STATUS_LIST;
			$this->filter = $filter;
			$this->orderList = $orderList ;
			$this->pagination = $pagination;
                }
		else // display list of orders
		{
			TOOLBAR_enmasse::_SMENU();
			TOOLBAR_enmasse::_ORDER();
			
			$filter 	= JRequest::getVar('filter');
            
			// Weird that only this will caused warning...
			if(!isset($filter['deal_name']))
				$filter['deal_name'] = "";
                        if(!isset($filter['payment_method']))
				$filter['payment_method'] = "";
                        if(!isset($filter['deal_code']))
				$filter['deal_code'] = "";
			if(!isset($filter['status']))
				$filter['status'] = "";
			if(!isset($filter['year']))
				$filter['year'] = "";
			if(!isset($filter['month']))
				$filter['month'] = "";
			
			$oOrderModel = JModelLegacy::getInstance('order','enmasseModel');
			$orderList 	= $oOrderModel->search($filter['status'], $filter['deal_code'], $filter['payment_method'],$filter['deal_name'], "created_at", "DESC");
                        for($count =0; $count < count($orderList); $count++)
                        {
                                $orderItemList = JModelLegacy::getInstance('orderItem','enmasseModel')->listByOrderId($orderList[$count]->id);
                                $orderList[$count]->orderItem 	= $orderItemList;
                        }
                        $oPaygty = JModelLegacy::getInstance('paygty','enmasseModel');
                        $listPaymentMethod = $oPaygty->listAll();
                        $pagination = $oOrderModel->getPagination();
                        $this->listPaymentMethod = $listPaymentMethod;
                        $this->statusList = EnmasseHelper::$ORDER_STATUS_LIST;
			$this->filter = $filter;
			$this->orderList = $orderList ;
			$this->pagination = $pagination;
		}
		parent::display($tpl);
	}

}
?>