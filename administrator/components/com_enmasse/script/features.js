/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.isis
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @since       3.0
 */

jQuery( document ).ready(function() {
        var subMenu = jQuery('#sidebar #submenu');
        if(subMenu.html()){
            addSubMenuClass(subMenu);
        }
});

function addSubMenuClass(subMenu){
    subMenu.find('li').addClass('child_menu');
    subMenu.find('li:nth-child(1)').removeClass('child_menu');
    subMenu.find('li:nth-child(2)').removeClass('child_menu');
    subMenu.find('li:nth-child(6)').removeClass('child_menu');
    subMenu.find('li:nth-child(7)').removeClass('child_menu');
    //subMenu.find('li:nth-child(8)').removeClass('child_menu');
    subMenu.find('li:nth-child(8)').removeClass('child_menu');
    subMenu.find('li:nth-child(16)').removeClass('child_menu');
    subMenu.find('li:nth-child(22)').removeClass('child_menu');
    subMenu.find('li:nth-child(25)').removeClass('child_menu');
}