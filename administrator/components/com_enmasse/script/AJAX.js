$(document).ready(function() {
	
	load_data();
	function load_data() {
		$('#loading').html("<img src='images/loading.gif'/>").fadeIn('fast'); 
		$.ajax({
			type: "POST",
			url: "data.php",
			success: function(data_log) {
				$('#loading').fadeOut('fast');
				$("#data").html(data_log);
			}
		})
	}


		$("nav[role='tab'] ul li").click(function() {
			var id_cat = $(this).attr("id_cat");
			$('#loading').html("<img src='images/loading.gif'/>").fadeIn('fast');
			$("#main").slideUp().delay(400).slideDown();
			$.ajax({
				type: "POST",
				url: "get_data.php",
				data: "id_cat="+ id_cat,
				success: function(get_data) {
					$('#loading').fadeOut('fast');
					$("#data").empty().append(get_data);
				}
			});
		});

});