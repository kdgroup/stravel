var geocoder;
var map;
var marker;
var infowindow;




function initialize() {
	//get lat, lng and zoom form the view
	lat =  parseFloat(document.getElementById('loc_x').value);
	lng =  parseFloat(document.getElementById('loc_y').value);
	zoom =  parseInt(document.getElementById('zoom_rate').value);	
	
	//check lat and lng was initialized or not
	if(!lat & !lng) {
		//if not: show the map with current position
		//getCurrentPosition();
                showMap(1.34982,103.824692, 10);
	} else {
		//if yet: show the map with lat, lng, zoom get form view
		showMap(lat, lng, zoom);
	}	
}

  (function () {
    google.maps.Map.prototype.markers = new Array();
    google.maps.Map.prototype.getMarkers = function() {
      return this.markers
    };
    google.maps.Map.prototype.clearMarkers = function() {
      for(var i=0; i<this.markers.length; i++){
        this.markers[i].setMap(null);
      }
      this.markers = new Array();
    };
    google.maps.Marker.prototype._setMap = google.maps.Marker.prototype.setMap;
    google.maps.Marker.prototype.setMap = function(map) {
      if (map) {
        map.markers[map.markers.length] = this;
      }
      this._setMap(map);
    }
  })();

//get the current Position on the google map			
function getCurrentPosition() {
	if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(successCallback, errorCallback, {maximumAge:Infinity, timeout:5000});
	}
}

// to do when getCurrentPosition() success
function successCallback(position) {
	var lat = position.coords.latitude;
	var lng = position.coords.longitude;
	//show the google map with (lat, lng)
	showMap(lat,lng, 12);
}

// to do when getCurrentPosition() error
function errorCallback() {
}

// show google map with lat, lng, zoom
function showMap(lat, lng, zoom) {
	//init postion	(lat, lng)
	var latlng = new google.maps.LatLng(lat,lng);
	//init map option
	var mapOptions = {
		zoom: zoom,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	//init map with position and map option
	map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
	//init marker at (lat, lng) on map
	marker = new google.maps.Marker({
		map: map,
		draggable: true,
		position: latlng
	});	
	map.setCenter(latlng);
	addMarkerListener(marker);
	addMapListener(map);
	defaultProperties();
	addInfowindow(contentString, map, marker);
}

function codeAddress() {
	//clear all marker on the map
	map.clearMarkers();
	//get the address form the view
	var address = document.getElementById("address").value;
	//check address was inputed or not
	if(!address) {
		alert("Address can't be empty");
		return;
	}
	//init geocoder
	geocoder = new google.maps.Geocoder();	
	// get the lat and lng by geocde the address inputed
	geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			var lat = results[0].geometry.location.lat();
			var lng = results[0].geometry.location.lng();
			showMap(lat, lng, 12);
		} else {
				if(status == google.maps.GeocoderStatus.ZERO_RESULTS) 
					alert("Address is not exist.");
				else if(status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) 
					alert("You are over your quota");
				else if(status == google.maps.GeocoderStatus.REQUEST_DENIED ) 
					alert("Your request was denied");
				else if(status == google.maps.GeocoderStatus.INVALID_REQUEST ) 
					alert("Address is missing");
		}
	});
}

// add action for maker's event
function addMarkerListener(_marker) {
	google.maps.event.addListener(_marker, 'drag', function() {
		document.getElementById('lat').innerHTML  = _marker.getPosition().lat();
		document.getElementById('lng').innerHTML  = _marker.getPosition().lng();
	});
	google.maps.event.addListener(_marker, 'dragend', function() {
		document.getElementById('loc_x').value = _marker.getPosition().lat();
		document.getElementById('loc_y').value = _marker.getPosition().lng();
		map.setCenter(_marker.getPosition());
	});
	if(document.getElementById('infowindow')) {
		var name = document.getElementById('name').value;
		var address = document.getElementById('address').value;
		var description = document.getElementById('description').value;
		var string = "";
		var value = $$('input[type=checkbox]:checked').each(
		function(e) { 
			var id = "service_" + e.value;
			var service = document.getElementById(id).innerHTML;
				string = string + "<p>" + service + "</p>";
		});
		var contentString = '<h4 id="firstHeading" class="firstHeading" style="font-size:16px;margin-bottom:10px;">' +
												name + 
											'</h4>' +
											'<div id="bodyContent">' +
											'<p style="margin-bottom:10px;">' +
											'<b>Address: </b>' +
												address +
											'</p>' + 
											'<p style="margin-bottom:10px;">' +
											'<b>Service:</b>' +
												string +
											'<p>' + description  + '</p>' +
											'</div>'
											;

		addInfowindow(contentString, map, marker);
	}	
}
// add action for map's event
function addMapListener(_map) {
	google.maps.event.addListener(_map, 'zoom_changed', function() {
		document.getElementById('zoom_rate').value = _map.getZoom();
		document.getElementById('zoom').innerHTML  = _map.getZoom();		
	});
}

// add default
function defaultProperties() {
		document.getElementById('loc_x').value = marker.getPosition().lat();
		document.getElementById('loc_y').value = marker.getPosition().lng();	
		document.getElementById('zoom_rate').value = map.getZoom();
		document.getElementById('lat').innerHTML  = marker.getPosition().lat();
		document.getElementById('lng').innerHTML  = marker.getPosition().lng();
		document.getElementById('zoom').innerHTML  = map.getZoom();		
}

function addInfowindow(_contentString, _map, _marker) {
	var infowindow = new google.maps.InfoWindow({
    content: _contentString
	});
	google.maps.event.addListener(_marker, 'click', function() {
		infowindow.open(_map,_marker);
	});
}


function zoneChange(_this) {
	var url = 'index.php?option=com_googlemaplocator&controller=location&task=ajaxZoneChange';
	var id = _this.value;
	
	jQuery.ajax({
            url: url,
            type: "POST",
			data : {
				id : id
			},
			success: function(result) {
               // return result;
            }
        });
}

//initialize the map when window load
google.maps.event.addDomListener(window, 'load', initialize);
  
















