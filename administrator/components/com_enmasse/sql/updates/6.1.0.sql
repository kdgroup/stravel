ALTER TABLE `#__enmasse_order` ADD `payment_status` TINYINT( 2 ) NOT NULL DEFAULT '0' AFTER `archived` ;

ALTER TABLE `#__enmasse_pay_gty` ADD `pay_detail_fields` longtext;
ALTER TABLE `#__enmasse_pay_gty` ADD `payment_type` INT(1) NOT NULL DEFAULT '0'

ALTER TABLE `#__enmasse_setting` ADD `dl_custom_field` text COLLATE utf8_unicode_ci;
ALTER TABLE `#__enmasse_setting` ADD `order_min_item` INT NULL ;
ALTER TABLE `#__enmasse_setting` ADD `order_max_item` INT NULL ;
ALTER TABLE `#__enmasse_setting` ADD `order_min_value` VARCHAR( 255 ) COLLATE utf8_unicode_ci;
ALTER TABLE `#__enmasse_setting` ADD `order_max_value` VARCHAR( 255 ) COLLATE utf8_unicode_ci ;
ALTER TABLE `#__enmasse_setting` ADD `date_type` INT( 2 ) ;

ALTER TABLE `#__enmasse_deal` ADD `min_item_per_order` VARCHAR( 10 ) COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `#__enmasse_deal` ADD `max_item_per_order` VARCHAR( 10 ) COLLATE utf8_unicode_ci NULL DEFAULT NULL;

ALTER TABLE `#__enmasse_location` ADD `latitude` VARCHAR( 255 ) COLLATE utf8_unicode_ci,
ALTER TABLE `#__enmasse_location` ADD `longitude` VARCHAR( 255 ) COLLATE utf8_unicode_ci;
ALTER TABLE `#__enmasse_location` ADD `address` VARCHAR( 255 ) COLLATE utf8_unicode_ci ;