ALTER TABLE `#__enmasse_deal` ADD `tier_pricing` TINYINT( 3 ) NOT NULL DEFAULT '0' AFTER `price` ,
ADD `price_step` MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `tier_pricing` ;

ALTER TABLE `#__enmasse_setting` ADD `enable_rtl` TINYINT( 3 ) DEFAULT NULL AFTER `enable_security_code`;
INSERT IGNORE INTO `#__enmasse_coupon_element` (`id`, `name`, `x`, `y`, `font_size`, `width`, `height`, `published`) VALUES
(8, 'security_code', 200, 47, 12, 140, 60, 1);