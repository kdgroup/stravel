CREATE TABLE IF NOT EXISTS `#__enmasse_voucher` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `voucher_code` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `discount_amount` varchar(255) NOT NULL,
  `use_per_coupon` int(11) NOT NULL,
  `use_per_user` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `start_at` datetime NOT NULL,
  `end_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__enmasse_order_voucher` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `discount_amount` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `#__enmasse_deal` ADD `num_view` INT( 11 ) NOT NULL DEFAULT '0' AFTER `cur_sold_qty` ;

ALTER TABLE `#__enmasse_order` ADD `archived` INT( 1 ) NOT NULL DEFAULT '0' AFTER `paid_amount` ;

ALTER TABLE `#__enmasse_category` ADD `background_image` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `published` ;

ALTER TABLE `#__enmasse_setting` ADD `main_image` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `currency_decimal_separator` ;