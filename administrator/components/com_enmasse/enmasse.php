<?php
/*------------------------------------------------------------------------
# En Masse - Social Buying Extension 2010
# ------------------------------------------------------------------------
# By Matamko.com
# Copyright (C) 2010 Matamko.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.matamko.com
# Technical Support:  Visit our forum at www.matamko.com
-------------------------------------------------------------------------*/

// No direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 if(!defined('DS')){
define('DS',DIRECTORY_SEPARATOR);
}
?>
<?php
    $e_task = JRequest::getVar('task');
    if($e_task != 'ajaxGetDealOverView' && $e_task != 'autoCompleteDealName'
        && $e_task != 'autoCompleteMerchantName' && $e_task != 'autoCompleteSalePersonName'
        && $e_task != 'autoCompleteDealCode' && $e_task != 'autoCompleteCouponCode'
        && $e_task != 'autoCompleteCouponName' && $e_task != 'autoCompleteCustomerCouppon'
        && $e_task != 'generateReport' && $e_task != 'autoCompleteUserName') {?>
    <script type="text/javascript" src="components/com_enmasse/script/jquery.cookie.js"></script>
    <script type="text/javascript" src="components/com_enmasse/script/features.js"></script>
    <!-- auto complete with jquey -->
    <link rel="stylesheet" href="components/com_enmasse/css/jquery-ui.css">
    <!-- <script src="components/com_enmasse/script/jquery-ui.js"></script> -->
<?php } ?>
<?php

// Require the base controller
 
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR .'controller.php' );

// Require specific controller if requested
if($controller = JRequest::getWord('controller')) {
    $path = JPATH_COMPONENT.DIRECTORY_SEPARATOR .'controllers'.DIRECTORY_SEPARATOR .$controller.'.php';
    if (file_exists($path)) {
        require_once $path;
    } else {
        $controller = '';
    }
}
 
// Create the controller
$classname    = 'EnmasseController'.ucFirst($controller);
$controller   = new $classname();

// load language
$language = JFactory::getLanguage();
$base_dir = JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR .'components'.DIRECTORY_SEPARATOR .'com_enmasse';
$version = new JVersion;
$joomla = $version->getShortVersion();
if(substr($joomla,0,3) >= 1.6){
    $extension = 'com_enmasse16';
}else{
    $extension = 'com_enmasse';
}
if($language->load($extension, $base_dir, $language->getTag(), true) == false)
{
	 $language->load($extension, $base_dir, 'en-GB', true);
}

// Perform the Request task
$controller->execute(JRequest::getCmd('task'));
//$controller->execute( JRequest::getVar( 'task' ) );
 
JFactory::getDocument()->addStyleSheet('components/com_enmasse/css/style_be.css');


// Redirect if set by the controller
$controller->redirect();

if(substr($joomla,0,3) >= 1.6){
?>
<style>
fieldset.adminform label, fieldset.adminform span.faux-label {
    min-width: 5px;
    padding: 0 5px 0 0;
}
fieldset label, fieldset span.faux-label {
    clear: none;
    display: block;
    float: left;
    margin: 5px 0;
}

.controls > .radio:first-child, .controls > .checkbox:first-child
{
	padding-top:0px;
}
fieldset.adminform label, fieldset.adminform span.faux-label
{
padding-left: 18px !important ;
}
</style>
<?php
}

$doc = JFactory::getDocument();


$style = '.width-100{width:100%}'; 
$doc->addStyleDeclaration( $style );
?>
<script type="text/javascript">
//jQuery(function() {   
       function autoDealName(){
         jQuery.ajax({
               url: 'index.php?option=com_enmasse&controller=deal&task=autoCompleteDealName',
               type: 'POST',
               dataType: 'JSON',
               success: function(data){
//                   alert(data);
                     jQuery('.auto').autocomplete(
                     {
                           source: data,
                           minLength: 1
                     });
                
               }
          });        
       }
       function autoMerchantName(){
          jQuery.ajax({
               url: 'index.php?option=com_enmasse&controller=merchant&task=autoCompleteMerchantName',
               type: 'GET',
               dataType: 'JSON',
               success: function(data){
                     jQuery('.auto1').autocomplete(
                     {
                           source: data,
                           minLength: 1
                     });
                
               }
          });    
       }
       
       function autoSalePersonName(){
          jQuery.ajax({
               url: 'index.php?option=com_enmasse&controller=salesPerson&task=autoCompleteSalePersonName',
               type: 'GET',
               dataType: 'JSON',
               success: function(data){
                     jQuery('.auto2').autocomplete(
                     {
                           source: data,
                           minLength: 1
                     });
                
               }
          });    
       }
       
       function autoCouponName(){
          jQuery.ajax({
               url: 'index.php?option=com_enmasse&controller=discountcoupon&task=autoCompleteCouponName',
               type: 'GET',
               dataType: 'JSON',
               success: function(data){
                     jQuery('.auto3').autocomplete(
                     {
                           source: data,
                           minLength: 1
                     });
                
               }
          });    
       }
       
       function autoCouponCode(){
          jQuery.ajax({
               url: 'index.php?option=com_enmasse&controller=discountcoupon&task=autoCompleteCouponCode',
               type: 'GET',
               dataType: 'JSON',
               success: function(data){
                     jQuery('.auto4').autocomplete(
                     {
                           source: data,
                           minLength: 1
                     });
                
               }
          });    
       }
       
        function autoCustomerCoupon(){
            jQuery.ajax({
               url: 'index.php?option=com_enmasse&controller=discountcoupon&task=autoCompleteCustomerCouppon',
               type: 'GET',
               dataType: 'JSON',
               success: function(data){
                     jQuery('.auto5').autocomplete(
                     {
                           source: data,
                           minLength: 1
                     });
                
               }
            });    
        }
//});
</script>