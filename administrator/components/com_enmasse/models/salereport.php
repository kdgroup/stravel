<?php

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "helpers" . DS . "EnmasseHelper.class.php");
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "helpers" . DS . "DatetimeWrapper.class.php");
jimport('joomla.application.component.model');

class EnmasseModelSaleReport extends JModelLegacy {

    function getForSalereprt($deal_ids, $sale_person_name, $merchant_name, $location_list, $category_list, $from_at, $from_to) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        $query->select('d.commission_percent,oi.total_price,o.created_at,oi.id,o.id as oid,d.name,d.id as iddeal');
        $query->from('#__enmasse_order o');
        $query->join('left', '#__enmasse_order_item oi ON o.id = oi.order_id');
        $query->join('left', '#__enmasse_deal d ON d.id=oi.pdt_id');
        if ($deal_ids) {
            $query->where("d.id IN(" . implode(',', $deal_ids) . ")");
        }
        if ($sale_person_name) {
            $query->where("d.sales_person_id IN(" . implode(',', $sale_person_name) . ")");
        }
        if ($merchant_name) {
            $query->where("d.merchant_id IN(" . implode(',', $merchant_name) . ")");
        }
        if ($location_list) {
            $query->join('left', '#__enmasse_deal_location dl ON d.id=dl.deal_id');
            $query->where("dl.location_id IN(" . $location_list . ")");
        }
        if ($category_list) {
            $query->join('left', '#__enmasse_deal_category dc ON d.id=dc.deal_id');
            $query->where("dc.category_id IN(" . $category_list . ")");
        }
        if ($from_at) {
            $query->where("o.created_at >= '" . $from_at . "'");
        }
        if ($from_to) {
            $query->where("o.created_at <= '" . $from_to . "'");
        }
        $query->order('o.created_at asc');
        //$query->group("o.id");
        $db->setQuery($query);
        $result = $db->loadObjectList();
        if ($this->_db->getErrorNum()) {
            JError::raiseError(500, $this->_db->stderr());
            return false;
        }
        return $result;
    }

    function setDateFromEnd($grBy, $firstday, $index, $listExamine) {
        $dateFrom = "";
        $dateEnd = "";
        $numOrder = "";
        $total = "";
        $commission = "";
        $totalAfterCommission = "";
        if ($grBy == "Day") {
            $dateFrom = new DateTime($firstday);
            $dateFrom->setTime(0, 0, 0);
            $dateFrom = $dateFrom->format('Y-m-d');

            $dateEnd = new DateTime($firstday);
            $dateEnd = $dateEnd->modify('+1 day');
            $dateEnd->setTime(23, 59, 59);
            $dateEnd = $dateEnd->format('Y-m-d');
        } else if ($grBy == "Week") {
            $rmp = strtotime($firstday.' +1 days');
            $rmp1 = strtotime($firstday.' -1 days');
            $custom_date = strtotime(date('Y-m-d', $rmp));
            $custom_date1 = strtotime(date('Y-m-d', $rmp1));
            $dateFrom = date('Y-m-d', strtotime('this week last sunday', $custom_date));
            $dateEnd = date('Y-m-d', strtotime('this week next saturday', $custom_date1));
        } else if ($grBy == "Month") {
            $d = new DateTime($firstday);
            $d->modify('first day of this month');
            $dateFrom = $d->format('Y-m-d');
            $dateEnd = date("Y-m-t", strtotime($firstday));
            //echo $dateFrom;echo "====";echo $dateEnd;echo "<br/>";
        } else if ($grBy == "Year") {
            $d = new DateTime($firstday);
            $year = $d->format("Y");
            $dateFrom = "{$year}-01-01";
            $dateEnd = "{$year}-12-31";
        }
        for ($i = $index; $i < count($listExamine); $i++) {
            $tmp = $listExamine[$i]->created_at;
            if ($tmp >= $dateFrom && $tmp <= $dateEnd) {
                $numOrder = $numOrder + 1;
                $total = $total + $listExamine[$i]->total_price;
                $commission += ($listExamine[$i]->total_price * $listExamine[$i]->commission_percent / 100);
            }
        }
        $totalAfterCommission = $total - $commission;
        $result = array();
        array_push($result, $dateFrom);
        array_push($result, $dateEnd);
        array_push($result, $numOrder);
        array_push($result, $total);
        array_push($result, $commission);
        array_push($result, $totalAfterCommission);
        return $result;
    }

    function getSpace($from, $to, $grBy) {
        $rs = 0;
        $to = (is_string($to) ? strtotime($to) : $to);
        $from = (is_string($from) ? strtotime($from) : $from);
        $numDate = abs($to - $from);
        $days = $numDate / 86400;
        if ($grBy == "Day") {
            $rs = $days;
        } else if ($grBy == "Week") {
            $rs = $days / 7;
        } else if ($grBy == "Month") {
            $rs = round($days/28);
        } else if ($grBy == "Year") {
            $rs = round($days/364);
        }
        return $rs - 1;
    }

}

?>