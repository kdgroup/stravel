<?php
/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */
// No direct access 
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.model' );
require_once( JPATH_ADMINISTRATOR . DS ."components". DS ."com_enmasse". DS ."helpers". DS ."EnmasseHelper.class.php");

class EnmasseModelOrderItem extends JModelLegacy
{
	 var $_total = null;
	  var $_pagination = null;
	  function __construct()
	  {
	        parent::__construct();
	 
	        global $mainframe, $option;
	 
	        // define values need to pagination
	        $limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
	        $limitstart = JRequest::getVar('limitstart', 0, '', 'int');
	        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
	        $this->setState('limit', $limit);
	        $this->setState('limitstart', $limitstart);
	        
	  }
	 
	  
	   function getTotal()
	   {
	   // 	 global $mainframe, $option;
	   // 	 $status = '';
	   // 	 $pdtId = '';
	   // 	 $filter = $mainframe->getUserStateFromRequest( $option.'filter', 'filter', '', 'array' );
	   // 	 if($filter != null && count($filter)!=0)
	   // 	 {
	   	 	
		  //   $status = 'Delivered';
		  //  	if(isset($filter['deal_id']) && $filter['deal_id']!=null)
		  //  	 	$pdtId = $filter['deal_id'];
	   // 	 }
	   //      // Load total records
	   //      if($pdtId == '')
	   //      {$this->_total=0;}
	   //      else if (empty($this->_total)) {
	   //          $query =  "	SELECT
				// 		* 
				// 	FROM 
				// 		#__enmasse_order_item 
				// 	WHERE";
				// if($status!=null)						
				// 	$query .= "	status = '".$status."' AND";
				// $query .= "		pdt_id = $pdtId";
			
	   //          $this->_total = $this->_getListCount($query);    
	   //      }
	        return $this->_total;
	  }
	   function getPagination($total)
	  {
	        //create pagination
	        if (empty($this->_pagination)) {
	            jimport('joomla.html.pagination');
	            $this->_pagination = new JPagination($total, $this->getState('limitstart'), $this->getState('limit') );
	       }
	          
	        return $this->_pagination;
	  }
	
	function getById($id)
	{
		$db = JFactory::getDBO();
		$query = "	SELECT
						* 
					FROM 
						#__enmasse_order_item 
					WHERE
	              		id = $id
	              ";
		$db->setQuery( $query );
		$orderItem = $db->loadObject();

		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return $orderItem;
	}
	
	function listByOrderId($orderId)
	{
		$db = JFactory::getDBO();
		$query = "	SELECT
						* 
					FROM 
						#__enmasse_order_item 
					WHERE
	              		order_id = $orderId
	              ";
		$db->setQuery( $query );
		$orderItemList = $db->loadObjectList();

		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return $orderItemList;
	}
        
        function getOrderItemById($orderItemId)
	{
		$db = JFactory::getDBO();
		$query = "	SELECT
						* 
					FROM 
						#__enmasse_order_item 
					WHERE
	              		id = $orderItemId
	              ";
		$db->setQuery( $query );
		$rs = $db->loadObject();

		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return $rs;
	}

	function listByPdtIdAndStatus($pdtId, $status=null)
	{
		$db = JFactory::getDBO();
		$query = "	SELECT
						* 
					FROM 
						#__enmasse_order_item 
					WHERE";
		if($status!=null)						
			$query .= "	status = '".$status."' AND";
		$query .= "		pdt_id = $pdtId";
		$db->setQuery( $query );
		$orderItemList = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return $orderItemList;
	}
        
        function listByPdtIdAndStatusAndDate($pdtId, $status=null,$from_at,$from_to)
	{
		$db = JFactory::getDBO();
		$query = "	SELECT
						* 
					FROM 
						#__enmasse_order_item 
					WHERE";
		if($status!=null)						
			$query .= "	status = '".$status."' AND";
		$query .= "		pdt_id = $pdtId";
                if($from_at != ""){
                    $query .= " AND created_at >= '".$from_at."'"	;
                }
                if($from_to != ""){
                    $query .= " AND created_at <= '".$from_to."'"	;
                }
		$db->setQuery( $query );

		$orderItemList = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return $orderItemList;
	}
	
	function listByPdtIdAndStatusAndDateCustom($pdtId, $status=null,$from_at,$from_to,$orderby,$branch="")
	{
		$db = JFactory::getDBO();
		$query = "	SELECT a.created_at,a.status, a.total_price,b.status as statusIn,b.name as nameIn,c.branch,d.deal_code,d.name,c.buyer_detail
					FROM #__enmasse_order_item a 
					left join #__enmasse_invty b on a.id = b.order_item_id
					left join #__enmasse_order c  on a.order_id = c.id
					left join #__enmasse_deal d on a.pdt_id = d.id
					WHERE ";
		if($status!=null)						
			$query .= "	(a.status = '".$status."')";
		//$query .= "		pdt_id = $pdtId";
                if($from_at != ""){
                    $query .= " AND a.created_at >= '".$from_at."' "	;
                }
                if($from_to != ""){
                    $query .= " AND a.created_at <= '".$from_to."' "	;
                }
          if($pdtId!='all'){
         	$query .= "	AND a.pdt_id = ".$pdtId." ";
         }
          if($branch){
         	$query .= "	AND c.branch = '".$branch."' ";
         }
         if($orderby){
         	$query .= " ".$orderby." "	;
         }

		$db->setQuery( $query );
		 $this->_total = $this->_getListCount($query); 
		$orderItemList = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		// $this->_total = $this->_getListCount($query); var_dump($this->_total);die;
		$data['orderItemList'] = $orderItemList;
		$data['_total'] = $this->_total;
		return $data;
	}
	
	function updateStatus($id,$value)
	{
		$db = JFactory::getDBO();
		$query = '	UPDATE 
						#__enmasse_order_item 
					SET 
						status ="'.$value.'", 
						updated_at = "'. DatetimeWrapper::getDatetimeOfNow() .'" 
					WHERE 
						id ='.$id;
		$db->setQuery( $query );
		$db-> query();
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return true;
	}
	
	function updateIsDelivered($id,$value)
	{
		$db = JFactory::getDBO();
		$query = '	UPDATE 
						#__enmasse_order_item 
					SET 
						is_delivered ='.$value.'  
					WHERE 
						id ='.$id;
		$db->setQuery( $query );
		$db-> query();
		
		if ($this->_db->getErrorNum()) {
			JError::raiseError( 500, $this->_db->stderr() );
			return false;
		}
		return true;
	}
	
	public function getDealIdCannotDelete()
	{
		$db = JFactory::getDBO();
		$query = "SELECT DISTINCT pdt_id
					FROM #__enmasse_order_item
					WHERE status= 'Paid' OR status = 'Delivered' OR status = 'Waiting_For_Refund'";
		$db->setQuery( $query );
		return $db->loadColumn();
	}
}
?>