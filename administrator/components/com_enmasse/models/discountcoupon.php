<?php

/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */


// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();
jimport('joomla.application.component.model');
jimport('joomla.html.pagination');

class EnmasseModelDiscountcoupon extends JModelAdmin {

    var $_total = null;
    var $_pagination = null;

    function __construct() {
        parent::__construct();

        global $mainframe, $option;

        $version = new JVersion;
        $joomla = $version->getShortVersion();
        if (substr($joomla, 0, 3) >= 1.6) {
            $mainframe = JFactory::getApplication();
        }

        // define values need to pagination
        $limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
        $limitstart = JRequest::getVar('limitstart', 0, '', 'int');
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);

        // define value need for sort
        $filter_order = $mainframe->getUserStateFromRequest($option . 'filter_order', 'filter_order', 'name', 'cmd');
        $filter_order_dir = $mainframe->getUserStateFromRequest($option . 'filter_order_Dir', 'filter_order_Dir', 'asc', 'word');
        $this->setState('filter_order', $filter_order);
        $this->setState('filter_order_dir', $filter_order_dir);
    }

    public function getForm($data = array(), $loadData = true) {
        // Get the form.
        $form = $this->loadForm('com_enmasse.discountcoupon', 'discountcoupon', array('control' => 'jform', 'load_data' => $loadData));
        if (empty($form)) {
            return false;
        }
        return $form;
    }

    public function loadFormData() {
        $cid = JRequest::getVar('cid');
        $id = $cid[0];
        $app = JFactory::getApplication();
        $data = $app->getUserState('discountcoupon.data');
        if (empty($data)) {
            $data = $this->getById($id);
        }
        return $data;
    }

    function _buildContentOrderBy() { // to generate order query content
        $orderby = '';
        $filter_order = $this->getState('filter_order');
        $filter_order_dir = $this->getState('filter_order_dir');

        if (!empty($filter_order) && !empty($filter_order_dir)) {
            if ($filter_order == 'name' || $filter_order == 'start_at' || $filter_order == 'end_at')
                $orderby = ' ORDER BY ' . $filter_order . ' ' . $filter_order_dir;
        }

        return $orderby;
    }

    function getTotal() {
        // Load total records
        if (empty($this->_total)) {
            $query = "SELECT * FROM #__enmasse_voucher" . $this->_buildContentOrderBy();
            $this->_total = $this->_getListCount($query);
        }
        return $this->_total;
    }

    function getPagination() {
        //create pagination
        if (empty($this->_pagination)) {
            jimport('joomla.html.pagination');
            $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
        }
        return $this->_pagination;
    }

    function search() {
        $db = JFactory::getDBO();
        $query = "SELECT * FROM #__enmasse_voucher" . $this->_buildContentOrderBy();
        $db->setQuery($query);
        $rows = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));

        if ($this->_db->getErrorNum()) {
            JError::raiseError(500, $this->_db->stderr());
            return false;
        }
        return $rows;
    }

    function getById($id) {
        $row = JTable::getInstance('discountcoupon', 'Table');
        $row->load($id);
        if ($this->_db->getErrorNum()) {
            JError::raiseError(500, $this->_db->stderr());
            return false;
        }
        return $row;
    }

    public function getCouponByCode($code) {
        $db = JFactory::getDBO();
        $query = ' SELECT
		*
		FROM
		#__enmasse_voucher
		WHERE
		voucher_code = "' . $code . '"';
        $db->setQuery($query);
        $coupon = $db->loadObject();
        return $coupon;
    }

    function store($data) {
        $arrayJform = $data['jform'];
        
        $row = $this->getTable();
        $data['user_id'] = $this->getUserByName($data['user_name'])->id;
        if(!$data['user_id']){
            $data['user_id'] = '0';
        }
        
        $data['name'] = trim($data['name']);
        $data['status'] = trim($data['vc_status']);
        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
        return true;
    }
    
    function getUserByName($user_name)
    {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__users');
        $query->where('username = '.$db->quote($user_name));
        $db->setQuery($query);
        $result = $db->loadObject();
        if ($this->_db->getErrorNum()) {
            JError::raiseError(500, $this->_db->stderr());
            return false;
        }
        return $result;
    }
    
    function deleteList($cids) {
        $row = $this->getTable();
        foreach ($cids as $cid) {
            if (!$row->delete($cid)) {
                $this->setError($row->getErrorMsg());
                return false;
            }
        }
        return true;
    }

    function getReportVoucher($coupon_name, $coupon_code, $discount_at, $to_discount, $type, $start_at, $start_to, $end_at, $end_to, $use_at, $use_to) {
        $oDb = JFactory::getDbo();
//        $sql_utf8 = "set names 'utf8'"; 
//        $oDb->setQuery( $sql_utf8 ); 
        $oQuery = $oDb->getQuery(true);
        
        $oQuery->select('v.*');
        $oQuery->from('#__enmasse_voucher AS v ,#__enmasse_order_voucher AS ov ');
        if ($coupon_name) {
            $oQuery->where('v.name Like "%' . $coupon_name . '%"');
        }
        if ($coupon_code) {
            $oQuery->where('v.voucher_code Like    "%' . $coupon_code . '%"');
        }
        if ($discount_at) {
            $oQuery->where('v.discount_amount >=' . $discount_at);
        }
        if ($to_discount) {
            $oQuery->where('v.discount_amount <=' . $to_discount);
        }
        if ($type) {
            $oQuery->where('v.type = "' . $type.'"');
        }
        if ($start_at) {
            $oQuery->where('v.start_at >= "' . $start_at . '"');
        }
        if ($start_to) {
            $oQuery->where('v.start_at <= "' . $start_to . '"');
        }
        if ($end_at) {
            $oQuery->where('v.end_at >= "' . $end_at . '"');
        }
        if ($end_to) {
            $oQuery->where('v.end_at <= "' . $end_to . '"');
        }
        if ($use_at) {
            $oQuery->where('ov.created_at >= "' . $use_at . '"');
        }
        if ($use_to) {
            $oQuery->where('ov.created_at <= "' . $use_to . '"');
        }
        $oQuery->where('v.id = ov.voucher_id');
        $oQuery->group('v.id');
        $oDb->setQuery($oQuery);
        return $oDb->loadObjectList();
    }

    function getUseTime($id) {
        $db = JFactory::getDBO();
        $query = '  SELECT COUNT(v.voucher_id) as useTime, v.created_at as lastUsed
        FROM (
        SELECT *
        FROM #__enmasse_order_voucher WHERE voucher_id = ' . $id . '  ORDER BY created_at DESC
        ) as v
        GROUP BY v.voucher_id';
        $db->setQuery($query);
        $coupon = $db->loadObject();
        return $coupon;
    }
    
    function getAllCouponName(){
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('v.name');
        $query->from('#__enmasse_voucher v');
        $db->setQuery($query);
        $result = $db->loadObjectList();
        if ($this->_db->getErrorNum()) {
            JError::raiseError(500, $this->_db->stderr());
            return false;
        }
        return $result;
    }
    
    function getAllCouponCode(){
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('v.voucher_code');
        $query->from('#__enmasse_voucher v');
        $db->setQuery($query);
        $result = $db->loadObjectList();
        if ($this->_db->getErrorNum()) {
            JError::raiseError(500, $this->_db->stderr());
            return false;
        }
        return $result;
    }
    
    function getAllCustomer(){
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('u.username');
        $query->from('#__users u');
        $db->setQuery($query);
        $result = $db->loadObjectList();
        if ($this->_db->getErrorNum()) {
            JError::raiseError(500, $this->_db->stderr());
            return false;
        }
        return $result;
    }
    
    function searchByParam($from_to='',$from_at='',$status=''){
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__enmasse_voucher v');
        $db->setQuery($query);
        if ($from_at) {
            $query->where("v.start_at >= '" . $from_at . "'");
        }
        if($from_to){
            $query->where("v.end_at >= '" . $from_to . "'");
        }
        if($status){
            $query->where("v.status like '" . $status . "'");
        }
        
        $rows = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
        
        if ($this->_db->getErrorNum()) {
            JError::raiseError(500, $this->_db->stderr());
            return false;
        }
        return $rows;
    }

}

?>