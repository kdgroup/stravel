<?php

/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();
jimport('joomla.application.component.model');
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "helpers" . DS . "DatetimeWrapper.class.php");
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "helpers" . DS . "EnmasseHelper.class.php");

class EnmasseModelOrder extends JModelLegacy {

    var $_total = null;
    var $_pagination = null;

    function __construct() {
        parent::__construct();

        global $mainframe, $option;

        $version = new JVersion;
        $joomla = $version->getShortVersion();
        if (substr($joomla, 0, 3) >= '1.6') {
            $mainframe = JFactory::getApplication();
        }

        // define values need to pagination
        $limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
        $limitstart = JRequest::getVar('limitstart', 0, '', 'int');
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);
    }

    function getTotal() {
        global $mainframe, $option;
        $status = '';
        $dealName = '';
        $dealCode = '';
        $bPartial = false;
        $filter = $mainframe->getUserStateFromRequest($option . 'filter', 'filter', '', 'array');
        $db = JFactory::getDBO();
        if ($filter != null) {
            if (isset($filter['status']) && $filter['status'] != null)
                $status = $filter['status'];
            if (isset($filter['deal_name']) && $filter['deal_name'] != null)
                $dealName = $filter['deal_name'];
            if (isset($filter['deal_code']) && $filter['deal_code'] != null)
                $dealCode = $filter['deal_code'];
            if (isset($filter['partial']) && $filter['partial'] != null)
                $bPartial = $filter['partial'];
            if(isset($filter['payment_method']))
                $payment_method = $filter['payment_method'];
        }
        $archived = JRequest::getVar('archived');
        // Load total records
        if (empty($this->_total)) {
            $query = "	SELECT 
						o.*,
						oi.description as deal_name,
						oi.qty as qty,
                        d.deal_code as deal_code
					FROM 
						#__enmasse_order AS o,
						#__enmasse_order_item AS oi,
                                                #__enmasse_deal AS d
					WHERE 
						o.id = oi.order_id AND oi.pdt_id = d.id";
            if (!empty($status))
                $query .="	AND o.status = '$status'";

            if (!empty($dealCode)) {
                $dealCode = EnmasseHelper::escapeSqlLikeSpecialChar($db->escape($dealCode));
                $query .=" AND d.deal_code like '%$dealCode%'";
            }
            if (!empty($dealName)) {
                $dealName = EnmasseHelper::escapeSqlLikeSpecialChar($db->escape($dealName));
                $query .=" AND d.name like '%$dealName%'";
            }
            if ($bPartial) {
                $query .=" AND d.prepay_percent < 100.0 ";
            }
            if($payment_method){
                 $query .=" AND o.pay_gty_id = '$payment_method' ";
            }
            if (!empty($filter['year'])) {
                $query .="	AND YEAR(o.created_at) = " . intval($filter['year']);
            }

            if (!empty($filter['month'])) {
                $query .="	AND MONTH(o.created_at) = " . intval($filter['month']);
            }
            
            if (isset($archived) && $archived != null){
                $query .= " and o.archived = 1";
            }else{
                $query .= " and o.archived = 0";
            }
            $this->_total = $this->_getListCount($query);
        }

        return $this->_total;
    }

    public function getPagination() {
        //create pagination
        if (empty($this->_pagination)) {
            jimport('joomla.html.pagination');
            $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
        }
        return $this->_pagination;
    }

    function search($status = null, $orderCode = null, $payment_method = null,$dealName= null, $orderBy = null, $orderType = null, $bPartial = false,$archived = false) {
        global $option;
        //set pagination limitstart again from 0 for archive
        $oSession = JFactory::getSession();
        $archive_session = $oSession->get('archive_session');
        if(($archive_session && !$archived) || (!$archive_session && $archived)){
            $this->setState('limitstart','0');
        }
        $oSession->set('archive_session', $archived);
        
        $app = JFactory::getApplication();
        $filter = $app->getUserStateFromRequest($option . 'filter', 'filter', array(), 'array');
        $db = JFactory::getDBO();
        $query = "	SELECT 
						o.*,
						oi.description as deal_name,
						oi.qty as qty,
                                                oi.id as order_item_id,
                                                p.name as payment_method
					FROM 
						#__enmasse_order AS o,
						#__enmasse_order_item AS oi,
                                                #__enmasse_pay_gty AS p
					WHERE 
						o.id = oi.order_id AND p.id = o.pay_gty_id";
        if (!empty($status))
            $query .="	AND o.status = '$status'";

        if($dealName){
            $query .="	AND oi.description LIKE '%$dealName%'";
        }
        if (!empty($orderCode)) {
            $orderCode = EnmasseHelper::escapeSqlLikeSpecialChar($db->escape($orderCode));
            $query .=" AND o.id like '%$orderCode%'";
        }
        if (!empty($payment_method)) {
            $query .=" AND o.pay_gty_id = '$payment_method' ";
       }
            
        if ($bPartial) {
            $query .=" AND d.prepay_percent < 100.0 ";
        }

        if (!empty($filter['year'])) {
            $query .="	AND YEAR(o.created_at) = " . intval($filter['year']);
        }

        if (!empty($filter['month'])) {
            $query .="	AND MONTH(o.created_at) = " . intval($filter['month']);
        }
        if($archived){
            $query .= " and o.archived = 1";
        }else{
            $query .= " and o.archived = 0";
        }
        $query .= " GROUP BY o.id ";

        if (!empty($orderBy))
            $query .=" ORDER BY $orderBy ";
        if (!empty($orderBy) && !empty($orderType))
            $query .="	$orderType ";
        $db->setQuery($query);

        $rows = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));

        if ($this->_db->getErrorNum()) {
            JError::raiseError(500, $this->_db->stderr());
            return false;
        }
        // Assign total records for pagination
        $this->_total = $this->_getListCount($query);

        return $rows;
    }

    function getById($id) {
        $order_row = JTable::getInstance('order', 'Table');
        $order_row->load($id);
        return $order_row;
    }

     function getPassengerById($id) {
            $db = JFactory::getDBO();
            $query = $db->getQuery(true);
            $query->select("*");
            $query->where("order_id = ".$id);
            $query->from("#__enmasse_order_passenger");
            
            $db->setQuery($query);
            $object = $db->loadObject();
            
            return $object;
    }
    function updateStatus($id, $value) {
        $db = JFactory::getDBO();
        $query = 'UPDATE #__enmasse_order SET status ="' . $value . '", updated_at = "' . DatetimeWrapper::getDatetimeOfNow() . '" where id =' . $id;
        $db->setQuery($query);
        $db->query();

        if ($this->_db->getErrorNum()) {
            JError::raiseError(500, $this->_db->stderr());
            return false;
        }
        return true;
    }

    public function updateToFullPaid($id) {
        $db = JFactory::getDBO();
        $query = 'UPDATE #__enmasse_order SET paid_amount = total_buyer_paid, updated_at = "' . DatetimeWrapper::getDatetimeOfNow() . '" where id =' . $id;
        $db->setQuery($query);
        $db->query();

        if ($this->_db->getErrorNum()) {
            JError::raiseError(500, $this->_db->stderr());
            return false;
        }
        return true;
    }

    function store($data) {
        $row = JTable::getInstance('order', 'Table');
        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }

        if ($row->id <= 0)
            $row->created_at = DatetimeWrapper::getDatetimeOfNow();
        $row->updated_at = DatetimeWrapper::getDatetimeOfNow();

        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }

        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }

        return true;
    }

    public function getListIdUndelivered($arId) {
        $db = JFactory::getDbo();
        $query = "SELECT id
					FROM #__enmasse_order
					WHERE status != 'Delivered'
					AND status != 'Holding_By_Deliverer'
					AND id IN (" . implode(',', $arId) . ")";

        $db->setQuery($query);

        return $db->loadColumn();
    }

    public function getCountOrder($dealName) {
        $db = JFactory::getDbo();
        $query = "SELECT o.id FROM #__enmasse_order AS o,
            #__enmasse_order_item AS oi,#__enmasse_deal d WHERE o.id = oi.order_id and d.id = oi.pdt_id ";
        if($dealName != ""){
            $dealName = EnmasseHelper::escapeSqlLikeSpecialChar($db->escape($dealName));
            $query .= " and d.name like '%$dealName%' ";
        }
        $query .= "GROUP BY o.id";
        $db->setQuery($query);
        $count = $db->loadRowList();
        return $count;
    }

    public function getCountOrderToday($dealName) {
        $db = JFactory::getDbo();
        $today = date("Y-m-d");
        $query = "SELECT o.id FROM #__enmasse_order AS o,
            #__enmasse_order_item AS oi,#__enmasse_deal d WHERE o.id = oi.order_id and d.id = oi.pdt_id 
            and o.updated_at like '".$today."%' ";
        if($dealName != ""){
            $dealName = EnmasseHelper::escapeSqlLikeSpecialChar($db->escape($dealName));
            $query .= " and d.name like '%$dealName%' ";
        }
        $query .= "GROUP BY o.id";
        $db->setQuery($query);
        $count = $db->loadRowList();
        return $count;
    }
    
    public function getCountCustomerOrder($dealName) {
        $db = JFactory::getDbo();
        $query = "SELECT o.id FROM #__enmasse_order AS o,
            #__enmasse_order_item AS oi,#__enmasse_deal d WHERE o.id = oi.order_id and d.id = oi.pdt_id ";
        if($dealName != ""){
            $dealName = EnmasseHelper::escapeSqlLikeSpecialChar($db->escape($dealName));
            $query .= " and d.name like '%$dealName%' ";
        }
        $query .= "GROUP BY o.buyer_id";
        $db->setQuery($query);
        $count = $db->loadRowList();
        return $count;
    }
    
     public function getCountCustomerOrderToday($dealName) {
        $db = JFactory::getDbo();
        $today = date("Y-m-d");
        $query = "SELECT o.id FROM #__enmasse_order AS o,
            #__enmasse_order_item AS oi,#__enmasse_deal d WHERE o.id = oi.order_id and d.id = oi.pdt_id 
            and o.updated_at like '".$today."%' ";
        if($dealName != ""){
            $dealName = EnmasseHelper::escapeSqlLikeSpecialChar($db->escape($dealName));
            $query .= " and d.name like '%$dealName%' ";
        }
        $query .= "GROUP BY o.buyer_id";
        $db->setQuery($query);
        $count = $db->loadRowList();
        return $count;
    }

    public function getCountDealSold($dealName) {
        $db = JFactory::getDbo();
        $query = "SELECT sum(oi.qty) FROM #__enmasse_order_item AS oi,#__enmasse_deal d WHERE d.id = oi.pdt_id 
            and oi.status IN ('paid','Delivered')";
        if($dealName != ""){
            $dealName = EnmasseHelper::escapeSqlLikeSpecialChar($db->escape($dealName));
            $query .= " and d.name like '%$dealName%' ";
        }
//        $query .= "GROUP BY o.id";
        $db->setQuery($query);
        $count = $db->loadResult();
        return $count;
    }
    
     public function getCountDealSoldToday($dealName) {
        $db = JFactory::getDbo();
        $today = date("Y-m-d");
        $query = "SELECT sum(oi.qty) FROM #__enmasse_order_item AS oi,#__enmasse_deal d WHERE d.id = oi.pdt_id 
            and oi.status IN ('paid','Delivered') and oi.updated_at like '".$today."%' ";
         if($dealName != ""){
            $dealName = EnmasseHelper::escapeSqlLikeSpecialChar($db->escape($dealName));
            $query .= " and d.name like '%$dealName%' ";
        }
//        $query .= "GROUP BY o.id";
        $db->setQuery($query);
        $count = $db->loadResult();
        return $count;
    }

    public function get10LastestOrder($dealName) {
        $db = JFactory::getDBO();
        $query = "SELECT o. * , Sum(oi.qty) AS qty FROM #__enmasse_order AS o,
           #__enmasse_order_item AS oi,#__enmasse_deal d WHERE o.id = oi.order_id and d.id = oi.pdt_id ";
        if($dealName != ""){
            $dealName = EnmasseHelper::escapeSqlLikeSpecialChar($db->escape($dealName));
            $query .= " and d.name like '%$dealName%'";
        }
        $query .= "GROUP BY oi.order_id ORDER BY o.id DESC 
            LIMIT 10  ";
        $db->setQuery($query);
        $orders = $db->loadObjectList();
        return $orders;
    }

}

?>