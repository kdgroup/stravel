<?php

/* ------------------------------------------------------------------------
  # En Masse - Social Buying Extension 2010
  # ------------------------------------------------------------------------
  # By Matamko.com
  # Copyright (C) 2010 Matamko.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.matamko.com
  # Technical Support:  Visit our forum at www.matamko.com
  ------------------------------------------------------------------------- */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();
jimport('joomla.application.component.model');
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "helpers" . DS . "DatetimeWrapper.class.php");
require_once( JPATH_ADMINISTRATOR . DS . "components" . DS . "com_enmasse" . DS . "helpers" . DS . "EnmasseHelper.class.php");

class EnmasseModelProcessing extends JModelLegacy {

    var $_total = null;
    var $_pagination = null;

    function __construct() {
        parent::__construct();

        global $mainframe, $option;

        $version = new JVersion;
        $joomla = $version->getShortVersion();
        if (substr($joomla, 0, 3) >= '1.6') {
            $mainframe = JFactory::getApplication();
        }

        // define values need to pagination
        $limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
        $limitstart = JRequest::getVar('limitstart', 0, '', 'int');
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);
    }

    function getTotal() {
        global $mainframe, $option;
        $order_id = null; 
        $from_date = null; 
        $from_to = null; 
        $center = null;
        $filter = $mainframe->getUserStateFromRequest($option . 'filter', 'filter', '', 'array');
        $db = JFactory::getDBO();
        if ($filter != null) {
            if (isset($filter['order_id']) && $filter['order_id'] != null)
                $order_id = $filter['order_id'];
            if (isset($filter['from_date']) && $filter['from_date'] != null)
                $from_date = $filter['from_date'];
            if (isset($filter['from_to']) && $filter['from_to'] != null)
                $from_to = $filter['from_to'];
            if (isset($filter['center']) && $filter['center'] != null)
                $center = $filter['center'];
        }
        
        // Load total records
        if (empty($this->_total)) {
            $query = "	SELECT 
                                                    o.*,
                                                    p.id as process_id,
                                                    p.processing_center_id as processing_center_id,
                                                    p.status as process_status,
                                                    p.note as note,
                                                    p.updated_at as process_update,
                                                    oi.description as deal_name,
                                                    oi.qty as qty,
                                                    oi.id as order_item_id
                                            FROM 
                                                    #__enmasse_order AS o
                                            JOIN 	
                                                    #__enmasse_order_item AS oi ON o.id = oi.order_id
                                            LEFT JOIN 
                                                    #__enmasse_processing AS p ON p.order_id = o.id
                                            LEFT JOIN 
                                                    #__enmasse_processing_center AS pc ON p.processing_center_id = pc.id
                                            WHERE 
                                                    o.status = 'Delivered' ";
            if (!empty($order_id)){
                $order_id = (int)$order_id;
                $query .="	AND o.id = '$order_id'";
            }
            if(!empty($from_date)){
                $query .= " and o.created_at >= '" . $from_date . "'";
            }
            if(!empty($from_to)){
                $query .= " and o.created_at <= '" . $from_to . "'";
            }
            if(!empty($center)){
                $query .= " and pc.name LIKE " . $db->quote('%'.$center.'%');
            }
            $query .= " GROUP BY o.id ";
            
            $this->_total = $this->_getListCount($query);
        }

        return $this->_total;
    }

    public function getPagination() {
        //create pagination
        if (empty($this->_pagination)) {
            jimport('joomla.html.pagination');
            $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
        }
        return $this->_pagination;
    }

    function getProcessingOrderById($order_id){
        $db = JFactory::getDBO();
        $query = "	SELECT 
						o.*,
                                                p.id as process_id,
                                                p.processing_center_id as processing_center_id,
                                                p.status as process_status,
                                                p.note as note,
                                                p.updated_at as process_update
					FROM 
						#__enmasse_order AS o
                                        LEFT JOIN 
                                                #__enmasse_processing AS p ON p.order_id = o.id
					WHERE 
						o.id = '$order_id' ";
        $db->setQuery($query);
        $row = $db->loadObject();
        
        if ($this->_db->getErrorNum()) {
            JError::raiseError(500, $this->_db->stderr());
            return false;
        }
        // Assign total records for pagination
        return $row;
    }
    
    function search($order_id = null, $from_date = null, $from_to = null, $center = null, $orderType = null) {
        $db = JFactory::getDBO();
        $query = "	SELECT 
						o.*,
                                                p.id as process_id,
                                                p.processing_center_id as processing_center_id,
                                                p.status as process_status,
                                                p.note as note,
                                                p.updated_at as process_update,
						oi.description as deal_name,
						oi.qty as qty,
                                                oi.id as order_item_id
					FROM 
						#__enmasse_order AS o
					JOIN 	
                                                #__enmasse_order_item AS oi ON o.id = oi.order_id
                                        LEFT JOIN 
                                                #__enmasse_processing AS p ON p.order_id = o.id
                                        LEFT JOIN 
                                                #__enmasse_processing_center AS pc ON p.processing_center_id = pc.id
					WHERE 
						o.status = 'Delivered' ";
        if (!empty($order_id)){
            $order_id = (int)$order_id;
            $query .="	AND o.id = '$order_id'";
        }
        if(!empty($from_date)){
            $query .= " and o.created_at >= '" . $from_date . "'";
        }
        if(!empty($from_to)){
            $query .= " and o.created_at <= '" . $from_to . "'";
        }
        if(!empty($center)){
            $query .= " and pc.name LIKE " . $db->quote('%'.$center.'%');
        }
        $query .= " GROUP BY o.id ";

        $query .=" ORDER BY o.id ".$orderType;
        $db->setQuery($query);

        $rows = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));

        if ($this->_db->getErrorNum()) {
            JError::raiseError(500, $this->_db->stderr());
            return false;
        }
        // Assign total records for pagination
        $this->_total = $this->_getListCount($query);

        return $rows;
    }

    function getById($id) {
        $order_row = JTable::getInstance('processing', 'Table');
        $order_row->load($id);
        return $order_row;
    }

    function updateStatus($id, $value) {
        $db = JFactory::getDBO();
        $query = 'UPDATE #__enmasse_order SET status ="' . $value . '", updated_at = "' . DatetimeWrapper::getDatetimeOfNow() . '" where id =' . $id;
        $db->setQuery($query);
        $db->query();

        if ($this->_db->getErrorNum()) {
            JError::raiseError(500, $this->_db->stderr());
            return false;
        }
        return true;
    }

    public function updateToFullPaid($id) {
        $db = JFactory::getDBO();
        $query = 'UPDATE #__enmasse_order SET paid_amount = total_buyer_paid, updated_at = "' . DatetimeWrapper::getDatetimeOfNow() . '" where id =' . $id;
        $db->setQuery($query);
        $db->query();

        if ($this->_db->getErrorNum()) {
            JError::raiseError(500, $this->_db->stderr());
            return false;
        }
        return true;
    }

    function store($data) {
        $row = JTable::getInstance('processing', 'Table');
        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }

        if ($row->id <= 0)
            $row->updated_at = DatetimeWrapper::getDatetimeOfNow();

        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }

        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }

        return true;
    }

    public function getListIdUndelivered($arId) {
        $db = JFactory::getDbo();
        $query = "SELECT id
					FROM #__enmasse_order
					WHERE status != 'Delivered'
					AND status != 'Holding_By_Deliverer'
					AND id IN (" . implode(',', $arId) . ")";

        $db->setQuery($query);

        return $db->loadColumn();
    }

    public function getDeliveryCenterById($id = ''){
        if(!$id){
            return '';
        }
        
        $db = JFactory::getDbo();
        $query = "SELECT * FROM #__enmasse_processing_center";
        
        $query .= ' WHERE id = '.$id;
        
        $query .= ' ORDER BY name asc ';
        
        $db->setQuery($query);
        return $db->loadObject();
    }
    
    public function updateSubmitNote($processID=null,$note=null){
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->update('#__enmasse_processing p');
        $query->set('p.note ="'.$note.'"');
        $query->where('p.id = "'.$processID.'"' );
        $db->setQuery($query);
        $db->query();
        if ($this->_db->getErrorNum()) {
            JError::raiseError(500, $this->_db->stderr());
            return false;
        }
        return true;
    }
}

?>