<?php
defined('_JEXEC') or die;
$layout = $params->get('layout', 'default');
if(JRequest::getVar('option')!=='com_stravel') {
    require_once JPATH_BASE.DS.'components'.DS.'com_stravel'.DS.'helper'.DS.'apihelper.php';
    JFactory::getLanguage()->load('com_stravel',JPATH_BASE.DS.'components'.DS.'com_stravel');
}
if(preg_match('/default/',$layout)) {    
    $list	= apiHelper::getContentById($params->get('contentid',null));    
}

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

require JModuleHelper::getLayoutPath('mod_kjapimenu', $layout );
