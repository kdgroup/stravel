<?php
defined('_JEXEC') or die;
$region_name_asia = apiHelper::getContentById(1263);
$asia_cities = apiHelper::getContent(array('CONTENT','B2C','PACKAGE','PACKAGEPULLDOWNMENU','ASIA','REGIONCITY'));

$region_name_china = apiHelper::getContentById(1264);
$china_cities = apiHelper::getContent(array('CONTENT','B2C','PACKAGE','PACKAGEPULLDOWNMENU','CHINA','REGIONCITY'));

$region_name_longtrip = apiHelper::getContentById(1265);
$longtrip_cities = apiHelper::getContent(array('CONTENT','B2C','PACKAGE','PACKAGEPULLDOWNMENU','LONGTRIP','REGIONCITY'));
//Japan Taiwan Korea
$region_name_twjpkr = apiHelper::getContentById(1257);
$twjpkr_cities = apiHelper::getContent(array('CONTENT','B2C','PACKAGE','PACKAGEPULLDOWNMENU','TWJPKR','REGIONCITY'));
?>
<div class="hidden-xs hidden-sm packagesubmenu">
    <div class="pksubmenu-arrow">&nbsp;</div>
    <div class="pksubmenu-wrapper">
        <div class="twjpkr pksection">
            <span class="subtitle"><?php echo $region_name_twjpkr->Content ?></span>
            <?php 
                $i=0;
                foreach($twjpkr_cities as $city) :
                if($i%5==0) echo '<ul class="ulpksub">'; 
            ?>
                <li><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packagehome&city='.$city->Keyword.'&cityname='.$city->Content) ?>"><?php echo $city->Content ?></a></li>
            <?php
                if($i%5==4) echo '</ul>';
                $i++; 
                endforeach; 
            ?>
            <a class="pkmorebtn" href="<?php echo JRoute::_('index.php?option=com_stravel&view=packagehome'); ?>"><?php echo JText::_('COM_STRAVEL_MORE') ?></a>
        </div>
        <div class="asia pksection">
            <span class="subtitle"><?php echo $region_name_asia->Content ?></span>
            <?php 
                $i=0;
                foreach($asia_cities as $city) :
                if($i%5==0) echo '<ul class="ulpksub">'; 
            ?>
                <li><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packagehome&city='.$city->Keyword.'&cityname='.$city->Content) ?>"><?php echo $city->Content ?></a></li>
            <?php
                if($i%5==4) echo '</ul>';
                $i++; 
                endforeach; 
            ?>
            <a class="pkmorebtn" href="<?php echo JRoute::_('index.php?option=com_stravel&view=packagehome'); ?>"><?php echo JText::_('COM_STRAVEL_MORE') ?></a>
        </div>
        <div class="china pksection">
            <span class="subtitle"><?php echo $region_name_china->Content ?></span>
            <?php 
                $i=0;
                foreach($china_cities as $city) :
                if($i%5==0) echo '<ul class="ulpksub">'; 
            ?>
                <li><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packagehome&city='.$city->Keyword.'&cityname='.$city->Content) ?>"><?php echo $city->Content ?></a></li>
            <?php
                if($i%5==4) echo '</ul>';
                $i++; 
                endforeach; 
            ?>
            <a class="pkmorebtn" href="<?php echo JRoute::_('index.php?option=com_stravel&view=packagehome'); ?>"><?php echo JText::_('COM_STRAVEL_MORE') ?></a>
        </div>
        <div class="longtrip pksection last">
            <span class="subtitle"><?php echo $region_name_longtrip->Content ?></span>
            <?php 
                $i=0;
                foreach($longtrip_cities as $city) :
                if($i%5==0) echo '<ul class="ulpksub">'; 
            ?>
                <li><a href="<?php echo JRoute::_('index.php?option=com_stravel&view=packagehome&city='.$city->Keyword.'&cityname='.$city->Content) ?>"><?php echo $city->Content ?></a></li>
            <?php
                if($i%5==4) echo '</ul>';
                $i++; 
                endforeach; 
            ?>
            <a class="pkmorebtn" href="<?php echo JRoute::_('index.php?option=com_stravel&view=packagehome'); ?>"><?php echo JText::_('COM_STRAVEL_MORE') ?></a>
        </div>
    </div>    
</div>